.class public final Lcom/google/android/apps/plus/phone/FIFEUtil;
.super Ljava/lang/Object;
.source "FIFEUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/FIFEUtil$1;,
        Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;,
        Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;
    }
.end annotation


# static fields
.field private static final FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

.field private static final JOIN_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;

.field private static final SPLIT_ON_EQUALS:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

.field private static final SPLIT_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 22
    const-string v0, "="

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->on(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->omitEmptyStrings()Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_EQUALS:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    .line 24
    const-string v0, "/"

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->on(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->omitEmptyStrings()Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    .line 26
    const-string v0, "/"

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;->on(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil;->JOIN_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;

    .line 28
    const-string v0, "^((http(s)?):)?\\/\\/((((lh[3-6]\\.((ggpht)|(googleusercontent)|(google)))|([1-4]\\.bp\\.blogspot)|(bp[0-3]\\.blogger))\\.com)|(www\\.google\\.com\\/visualsearch\\/lh))\\/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil;->FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static getImageUrlSize(Ljava/lang/String;)Landroid/graphics/Point;
    .registers 14
    .parameter "url"

    .prologue
    const/4 v12, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 133
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 134
    .local v1, imageSize:Landroid/graphics/Point;
    if-eqz p0, :cond_10

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_11

    .line 159
    :cond_10
    return-object v1

    .line 137
    :cond_11
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 138
    .local v5, proxyUri:Landroid/net/Uri;
    sget-object v6, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/phone/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    if-le v10, v7, :cond_105

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v10, "image"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_105

    add-int/lit8 v6, v9, -0x1

    :goto_3d
    if-lt v6, v12, :cond_b4

    const/4 v9, 0x6

    if-gt v6, v9, :cond_b4

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    sget-object v6, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    invoke-virtual {v6, v9}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/phone/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_67

    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v11, "image"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_67

    invoke-interface {v10, v8}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_67
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v6

    const-string v11, "/"

    invoke-virtual {v9, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_ad

    const/4 v9, 0x5

    if-ne v6, v9, :cond_ad

    move v9, v7

    :goto_77
    if-ne v6, v12, :cond_af

    move v6, v7

    :goto_7a
    if-nez v9, :cond_b1

    if-nez v6, :cond_b1

    invoke-interface {v10, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    :goto_84
    move-object v4, v6

    .line 139
    .local v4, options:Ljava/lang/String;
    :goto_85
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_10

    .line 143
    const-string v6, "-"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 144
    .local v3, optionArray:[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_92
    array-length v6, v3

    if-ge v0, v6, :cond_10

    .line 145
    aget-object v2, v3, v0

    .line 147
    .local v2, opt:Ljava/lang/String;
    :try_start_97
    const-string v6, "w"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d9

    .line 148
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Landroid/graphics/Point;->x:I
    :try_end_aa
    .catch Ljava/lang/NumberFormatException; {:try_start_97 .. :try_end_aa} :catch_ed

    .line 144
    :cond_aa
    :goto_aa
    add-int/lit8 v0, v0, 0x1

    goto :goto_92

    .end local v0           #i:I
    .end local v2           #opt:Ljava/lang/String;
    .end local v3           #optionArray:[Ljava/lang/String;
    .end local v4           #options:Ljava/lang/String;
    :cond_ad
    move v9, v8

    .line 138
    goto :goto_77

    :cond_af
    move v6, v8

    goto :goto_7a

    :cond_b1
    const-string v6, ""

    goto :goto_84

    :cond_b4
    if-ne v6, v7, :cond_d5

    sget-object v6, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_EQUALS:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/phone/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-le v8, v7, :cond_d2

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    :goto_d0
    move-object v4, v6

    goto :goto_85

    :cond_d2
    const-string v6, ""

    goto :goto_d0

    :cond_d5
    const-string v6, ""

    move-object v4, v6

    goto :goto_85

    .line 149
    .restart local v0       #i:I
    .restart local v2       #opt:Ljava/lang/String;
    .restart local v3       #optionArray:[Ljava/lang/String;
    .restart local v4       #options:Ljava/lang/String;
    :cond_d9
    :try_start_d9
    const-string v6, "h"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_ef

    .line 150
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Landroid/graphics/Point;->y:I

    goto :goto_aa

    :catch_ed
    move-exception v6

    goto :goto_aa

    .line 151
    :cond_ef
    const-string v6, "s"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_aa

    .line 152
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Landroid/graphics/Point;->y:I

    iput v6, v1, Landroid/graphics/Point;->x:I
    :try_end_104
    .catch Ljava/lang/NumberFormatException; {:try_start_d9 .. :try_end_104} :catch_ed

    goto :goto_aa

    .end local v0           #i:I
    .end local v2           #opt:Ljava/lang/String;
    .end local v3           #optionArray:[Ljava/lang/String;
    .end local v4           #options:Ljava/lang/String;
    :cond_105
    move v6, v9

    goto/16 :goto_3d
.end method

.method public static isFifeHostedUrl(Ljava/lang/String;)Z
    .registers 3
    .parameter "url"

    .prologue
    .line 287
    if-nez p0, :cond_4

    .line 288
    const/4 v1, 0x0

    .line 292
    :goto_3
    return v1

    .line 291
    :cond_4
    sget-object v1, Lcom/google/android/apps/plus/phone/FIFEUtil;->FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 292
    .local v0, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    goto :goto_3
.end method

.method private static makeUriString(Landroid/net/Uri;)Ljava/lang/String;
    .registers 10
    .parameter "uri"

    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    .line 171
    .local v6, scheme:Ljava/lang/String;
    if-eqz v6, :cond_14

    .line 172
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175
    :cond_14
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, encodedAuthority:Ljava/lang/String;
    if-eqz v1, :cond_23

    .line 178
    const-string v7, "//"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_23
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 182
    .local v5, path:Ljava/lang/String;
    const-string v7, "/="

    invoke-static {v5, v7}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 183
    .local v3, encodedPath:Ljava/lang/String;
    if-eqz v3, :cond_32

    .line 184
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    :cond_32
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v4

    .line 188
    .local v4, encodedQuery:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_45

    .line 189
    const/16 v7, 0x3f

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_45
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v2

    .line 193
    .local v2, encodedFragment:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_58

    .line 194
    const/16 v7, 0x23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private static newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Ljava/util/ArrayList",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 428
    .local p0, elements:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+TE;>;"
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 429
    .local v0, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<+TE;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 430
    .local v1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TE;>;"
    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 431
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 433
    :cond_17
    return-object v1
.end method

.method public static setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 11
    .parameter "options"
    .parameter "url"

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 208
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v1, :cond_fa

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "image"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_fa

    add-int/lit8 v0, v4, -0x1

    :goto_2f
    if-lt v0, v8, :cond_bb

    const/4 v4, 0x6

    if-gt v0, v4, :cond_bb

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_f7

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v6, "image"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f7

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    :goto_5a
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const-string v7, "/"

    invoke-virtual {v4, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_b3

    const/4 v4, 0x5

    if-ne v6, v4, :cond_b3

    move v4, v1

    :goto_6a
    if-ne v6, v8, :cond_b5

    :goto_6c
    if-eqz v4, :cond_75

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_75
    if-eqz v1, :cond_b7

    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7a
    if-eqz v0, :cond_81

    const-string v0, "image"

    invoke-interface {v5, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_81
    if-eqz v7, :cond_88

    const-string v0, ""

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_88
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/phone/FIFEUtil;->JOIN_ON_SLASH:Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;->appendTo(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :goto_b2
    return-object v0

    :cond_b3
    move v4, v2

    goto :goto_6a

    :cond_b5
    move v1, v2

    goto :goto_6c

    :cond_b7
    invoke-interface {v5, v8, p0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_7a

    :cond_bb
    if-ne v0, v1, :cond_f5

    sget-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil;->SPLIT_ON_EQUALS:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_b2

    :cond_f5
    move-object v0, v3

    goto :goto_b2

    :cond_f7
    move v0, v2

    goto/16 :goto_5a

    :cond_fa
    move v0, v4

    goto/16 :goto_2f
.end method

.method public static setImageUrlSize(IILjava/lang/String;ZZ)Ljava/lang/String;
    .registers 9
    .parameter "width"
    .parameter "height"
    .parameter "url"
    .parameter "crop"
    .parameter "includeMetadata"

    .prologue
    .line 108
    if-eqz p2, :cond_8

    invoke-static {p2}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    :cond_8
    move-object v1, p2

    .line 126
    :goto_9
    return-object v1

    .line 112
    :cond_a
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 113
    .local v0, options:Ljava/lang/StringBuffer;
    const-string v3, "w"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 114
    const-string v3, "-h"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 115
    const-string v3, "-d"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/google/android/apps/plus/phone/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 124
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/FIFEUtil;->makeUriString(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 126
    .local v1, returnUrl:Ljava/lang/String;
    goto :goto_9
.end method

.method public static setImageUrlSize(ILjava/lang/String;Z)Ljava/lang/String;
    .registers 5
    .parameter "size"
    .parameter "url"
    .parameter "crop"

    .prologue
    .line 62
    if-eqz p1, :cond_8

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .end local p1
    :cond_8
    :goto_8
    return-object p1

    .restart local p1
    :cond_9
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "-d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p2, :cond_23

    const-string v1, "-c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_23
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/phone/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil;->makeUriString(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p1

    goto :goto_8
.end method
