.class public final Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;
.super Lcom/android/common/widget/CompositeCursorAdapter;
.source "HostNavigationBarAdapter.java"


# static fields
.field private static final DESTINATIONS_PROJECTION:[Ljava/lang/String;

.field private static sEmptyCursor:Landroid/database/MatrixCursor;


# instance fields
.field private mColorRead:I

.field private mColorUnread:I

.field private mDestinationsCursor:Landroid/database/MatrixCursor;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mNotificationProgressIndicator:Landroid/view/View;

.field private mNotificationRefreshButton:Landroid/view/View;

.field private mUnreadNotificationCount:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "icon"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0, p1, v2}, Lcom/android/common/widget/CompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    .line 65
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 68
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addPartition(ZZ)V

    .line 71
    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addPartition(ZZ)V

    .line 74
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addPartition(ZZ)V

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 77
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0a00fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mColorRead:I

    .line 78
    const v1, 0x7f0a00ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mColorUnread:I

    .line 79
    return-void
.end method


# virtual methods
.method public final addDestination(III)V
    .registers 6
    .parameter "destinationId"
    .parameter "iconResId"
    .parameter "textResId"

    .prologue
    .line 86
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 87
    .local v0, text:Ljava/lang/CharSequence;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(IILjava/lang/CharSequence;Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public final addDestination(IILjava/lang/CharSequence;Ljava/lang/String;)V
    .registers 9
    .parameter "destinationId"
    .parameter "iconResId"
    .parameter "text"
    .parameter "gaiaId"

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationsCursor:Landroid/database/MatrixCursor;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v2, 0x3

    aput-object p4, v1, v2

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method protected final bindHeaderView$3ab248f1(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    .prologue
    .line 193
    const v4, 0x7f09004f

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 194
    .local v3, text:Landroid/widget/TextView;
    const v4, 0x7f090108

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 195
    .local v2, separator:Landroid/view/View;
    const v4, 0x7f0900fe

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationRefreshButton:Landroid/view/View;

    .line 196
    const v4, 0x7f0900ff

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationProgressIndicator:Landroid/view/View;

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 198
    .local v1, res:Landroid/content/res/Resources;
    iget v4, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mUnreadNotificationCount:I

    if-lez v4, :cond_3c

    const v4, 0x7f0a00fd

    :goto_31
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 201
    .local v0, color:I
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 202
    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 203
    return-void

    .line 198
    .end local v0           #color:I
    :cond_3c
    const v4, 0x7f0a00fc

    goto :goto_31
.end method

.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;I)V
    .registers 14
    .parameter "view"
    .parameter "partition"
    .parameter "cursor"
    .parameter "position"

    .prologue
    const v7, 0x7f020118

    const/4 v6, 0x3

    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 259
    packed-switch p2, :pswitch_data_13c

    .line 268
    :goto_b
    return-void

    .line 261
    :pswitch_c
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v0, 0x7f09009c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f090048

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v5, :cond_43

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_36
    const v0, 0x7f09004f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    :cond_43
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v8}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_36

    .line 265
    :pswitch_4d
    const/16 v0, 0xb

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_b0

    move v3, v2

    :goto_56
    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f090048

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    if-nez v3, :cond_b2

    move v5, v2

    :goto_6e
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    invoke-virtual {v1, v8}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    const/16 v5, 0xc

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v5, v2, :cond_b4

    :goto_7c
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/16 v6, 0xf

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    packed-switch v5, :pswitch_data_144

    :pswitch_89
    const v1, 0x7f02011e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_8f
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_135

    iget v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mColorRead:I

    :goto_a4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_b

    :cond_b0
    move v3, v4

    goto :goto_56

    :cond_b2
    move v5, v4

    goto :goto_6e

    :cond_b4
    move v2, v4

    goto :goto_7c

    :pswitch_b6
    if-nez v2, :cond_be

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsNotificationData;->isEventNotificationType(I)Z

    move-result v1

    if-eqz v1, :cond_c2

    :cond_be
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_8f

    :cond_c2
    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsNotificationData;->isCommentNotificationType(I)Z

    move-result v1

    if-eqz v1, :cond_cf

    const v1, 0x7f020115

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_8f

    :cond_cf
    const v1, 0x7f020124

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_8f

    :pswitch_d6
    const v1, 0x7f020124

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_8f

    :pswitch_dd
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x6

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    if-eqz v2, :cond_139

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbDataActor;->deserializeDataActorList([B)Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_139

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_139

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_139

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v5, v2, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_10a
    invoke-virtual {v1, v5, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setDimmed(Z)V

    goto/16 :goto_8f

    :pswitch_118
    const v1, 0x7f020121

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_8f

    :pswitch_120
    const v1, 0x7f02011b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_8f

    :pswitch_128
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_8f

    :pswitch_12d
    const v1, 0x7f020111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_8f

    :cond_135
    iget v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mColorUnread:I

    goto/16 :goto_a4

    :cond_139
    move-object v2, v5

    move-object v5, v6

    goto :goto_10a

    .line 259
    :pswitch_data_13c
    .packed-switch 0x0
        :pswitch_c
        :pswitch_4d
    .end packed-switch

    .line 265
    :pswitch_data_144
    .packed-switch 0x1
        :pswitch_b6
        :pswitch_dd
        :pswitch_118
        :pswitch_120
        :pswitch_12d
        :pswitch_89
        :pswitch_89
        :pswitch_d6
        :pswitch_89
        :pswitch_128
    .end packed-switch
.end method

.method public final getDestinationId(I)I
    .registers 5
    .parameter "position"

    .prologue
    const/4 v1, -0x1

    .line 132
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getPartitionForPosition(I)I

    move-result v2

    if-eqz v2, :cond_8

    .line 141
    :cond_7
    :goto_7
    return v1

    .line 136
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 137
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_7

    .line 141
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    goto :goto_7
.end method

.method protected final getItemViewType(II)I
    .registers 3
    .parameter "partition"
    .parameter "position"

    .prologue
    .line 157
    return p1
.end method

.method public final getItemViewTypeCount()I
    .registers 2

    .prologue
    .line 149
    const/4 v0, 0x3

    return v0
.end method

.method public final getUnreadNotificationCount()I
    .registers 2

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mUnreadNotificationCount:I

    return v0
.end method

.method public final hideProgressIndicator()V
    .registers 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationRefreshButton:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationRefreshButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 224
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationProgressIndicator:Landroid/view/View;

    if-eqz v0, :cond_15

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationProgressIndicator:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 227
    :cond_15
    return-void
.end method

.method public final isEnabled(I)Z
    .registers 3
    .parameter "position"

    .prologue
    .line 165
    invoke-super {p0, p1}, Lcom/android/common/widget/CompositeCursorAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 166
    const/4 v0, 0x1

    .line 169
    :goto_7
    return v0

    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->isNotificationHeader(I)Z

    move-result v0

    goto :goto_7
.end method

.method protected final isEnabled$255f299(I)Z
    .registers 3
    .parameter "partition"

    .prologue
    const/4 v0, 0x1

    .line 177
    if-eqz p1, :cond_5

    if-ne p1, v0, :cond_6

    :cond_5
    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final isNotificationHeader(I)Z
    .registers 5
    .parameter "position"

    .prologue
    const/4 v0, 0x1

    .line 230
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getPartitionForPosition(I)I

    move-result v1

    if-ne v1, v0, :cond_f

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getOffsetInPartition(I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_f

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method protected final newHeaderView$4ac0fa28(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "partition"
    .parameter "parent"

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03004c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final newView$54126883(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "context"
    .parameter "partition"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    const/4 v2, 0x0

    .line 240
    packed-switch p2, :pswitch_data_24

    .line 251
    const/4 v0, 0x0

    :goto_5
    return-object v0

    .line 242
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03004b

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 245
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030067

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 248
    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030066

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_5

    .line 240
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_6
        :pswitch_10
        :pswitch_1a
    .end packed-switch
.end method

.method public final removeAllDestinations()V
    .registers 3

    .prologue
    .line 82
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationsCursor:Landroid/database/MatrixCursor;

    .line 83
    return-void
.end method

.method public final setNotifications(Landroid/database/Cursor;)V
    .registers 9
    .parameter "cursor"

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 99
    invoke-virtual {p0, v3, p1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 101
    if-eqz p1, :cond_d

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_4f

    :cond_d
    move v0, v3

    .line 102
    .local v0, empty:Z
    :goto_e
    const/4 v4, 0x2

    if-eqz v0, :cond_51

    sget-object v1, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->sEmptyCursor:Landroid/database/MatrixCursor;

    if-nez v1, :cond_2b

    new-instance v1, Landroid/database/MatrixCursor;

    new-array v5, v3, [Ljava/lang/String;

    const-string v6, "empty"

    aput-object v6, v5, v2

    invoke-direct {v1, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->sEmptyCursor:Landroid/database/MatrixCursor;

    new-array v5, v3, [Ljava/lang/Object;

    const-string v6, "empty"

    aput-object v6, v5, v2

    invoke-virtual {v1, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_2b
    sget-object v1, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->sEmptyCursor:Landroid/database/MatrixCursor;

    :goto_2d
    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 104
    iput v2, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mUnreadNotificationCount:I

    .line 105
    if-eqz p1, :cond_4e

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4e

    .line 107
    :cond_3a
    const/16 v1, 0xb

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eq v1, v3, :cond_48

    .line 108
    iget v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mUnreadNotificationCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mUnreadNotificationCount:I

    .line 110
    :cond_48
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3a

    .line 112
    :cond_4e
    return-void

    .end local v0           #empty:Z
    :cond_4f
    move v0, v2

    .line 101
    goto :goto_e

    .line 102
    .restart local v0       #empty:Z
    :cond_51
    const/4 v1, 0x0

    goto :goto_2d
.end method

.method public final showDestinations()V
    .registers 3

    .prologue
    .line 95
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationsCursor:Landroid/database/MatrixCursor;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 96
    return-void
.end method

.method public final showProgressIndicator()V
    .registers 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationProgressIndicator:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationProgressIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationRefreshButton:Landroid/view/View;

    if-eqz v0, :cond_15

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mNotificationRefreshButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    :cond_15
    return-void
.end method
