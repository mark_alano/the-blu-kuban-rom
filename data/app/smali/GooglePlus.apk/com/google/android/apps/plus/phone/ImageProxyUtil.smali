.class public final Lcom/google/android/apps/plus/phone/ImageProxyUtil;
.super Ljava/lang/Object;
.source "ImageProxyUtil.java"


# static fields
.field private static final PROXY_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

.field static sProxyIndex:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 22
    const-string v0, "^(((http(s)?):)?\\/\\/images(\\d)?-.+-opensocial\\.googleusercontent\\.com\\/gadgets\\/proxy\\?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->PROXY_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    return-void
.end method

.method private static createProxyUrl()Ljava/lang/String;
    .registers 3

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 127
    .local v0, proxy:Ljava/lang/StringBuffer;
    const-string v1, "http://images"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->getNextProxyIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-esmobile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-opensocial.googleusercontent.com/gadgets/proxy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 134
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getImageUrlSize(Ljava/lang/String;)Landroid/graphics/Point;
    .registers 7
    .parameter "url"

    .prologue
    const/4 v4, 0x0

    .line 100
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 101
    .local v1, imageSize:Landroid/graphics/Point;
    if-eqz p0, :cond_e

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->isProxyHostedUrl(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 119
    :cond_e
    :goto_e
    return-object v1

    .line 105
    :cond_f
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 107
    .local v2, proxyUri:Landroid/net/Uri;
    :try_start_13
    const-string v5, "resize_w"

    invoke-virtual {v2, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, width:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_33

    move v5, v4

    :goto_20
    iput v5, v1, Landroid/graphics/Point;->x:I
    :try_end_22
    .catch Ljava/lang/NumberFormatException; {:try_start_13 .. :try_end_22} :catch_3d

    .line 113
    .end local v3           #width:Ljava/lang/String;
    :goto_22
    :try_start_22
    const-string v5, "resize_h"

    invoke-virtual {v2, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, height:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_38

    :goto_2e
    iput v4, v1, Landroid/graphics/Point;->y:I
    :try_end_30
    .catch Ljava/lang/NumberFormatException; {:try_start_22 .. :try_end_30} :catch_31

    goto :goto_e

    .end local v0           #height:Ljava/lang/String;
    :catch_31
    move-exception v4

    goto :goto_e

    .line 108
    .restart local v3       #width:Ljava/lang/String;
    :cond_33
    :try_start_33
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_36
    .catch Ljava/lang/NumberFormatException; {:try_start_33 .. :try_end_36} :catch_3d

    move-result v5

    goto :goto_20

    .line 114
    .end local v3           #width:Ljava/lang/String;
    .restart local v0       #height:Ljava/lang/String;
    :cond_38
    :try_start_38
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3b
    .catch Ljava/lang/NumberFormatException; {:try_start_38 .. :try_end_3b} :catch_31

    move-result v4

    goto :goto_2e

    .line 109
    .end local v0           #height:Ljava/lang/String;
    :catch_3d
    move-exception v5

    goto :goto_22
.end method

.method private static declared-synchronized getNextProxyIndex()I
    .registers 3

    .prologue
    .line 141
    const-class v2, Lcom/google/android/apps/plus/phone/ImageProxyUtil;

    monitor-enter v2

    :try_start_3
    sget v1, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->sProxyIndex:I

    add-int/lit8 v0, v1, 0x1

    sput v0, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->sProxyIndex:I

    .line 142
    .local v0, toReturn:I
    sget v1, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->sProxyIndex:I

    rem-int/lit8 v1, v1, 0x3

    sput v1, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->sProxyIndex:I
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_11

    .line 143
    monitor-exit v2

    return v0

    .line 141
    :catchall_11
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static isProxyHostedUrl(Ljava/lang/String;)Z
    .registers 3
    .parameter "url"

    .prologue
    .line 266
    if-nez p0, :cond_4

    .line 267
    const/4 v1, 0x0

    .line 271
    :goto_3
    return v1

    .line 270
    :cond_4
    sget-object v1, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->PROXY_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 271
    .local v0, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    goto :goto_3
.end method

.method public static setImageUrlSize(IILjava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "width"
    .parameter "height"
    .parameter "url"

    .prologue
    .line 80
    if-nez p2, :cond_4

    move-object v2, p2

    .line 92
    :goto_3
    return-object v2

    .line 85
    :cond_4
    invoke-static {p2}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->isProxyHostedUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 86
    invoke-static {}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->createProxyUrl()Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, proxyUrl:Ljava/lang/String;
    :goto_e
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 92
    .local v0, proxyUri:Landroid/net/Uri;
    invoke-static {p0, p1, v0, p2}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->setImageUrlSizeOptions(IILandroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 88
    .end local v0           #proxyUri:Landroid/net/Uri;
    .end local v1           #proxyUrl:Ljava/lang/String;
    :cond_1b
    move-object v1, p2

    .line 89
    .restart local v1       #proxyUrl:Ljava/lang/String;
    const/4 p2, 0x0

    goto :goto_e
.end method

.method public static setImageUrlSize(ILjava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "size"
    .parameter "url"

    .prologue
    .line 55
    if-nez p1, :cond_4

    move-object v2, p1

    .line 67
    :goto_3
    return-object v2

    .line 60
    :cond_4
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->isProxyHostedUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 61
    invoke-static {}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->createProxyUrl()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, proxyUrl:Ljava/lang/String;
    :goto_e
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 67
    .local v0, proxyUri:Landroid/net/Uri;
    invoke-static {p0, p0, v0, p1}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->setImageUrlSizeOptions(IILandroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 63
    .end local v0           #proxyUri:Landroid/net/Uri;
    .end local v1           #proxyUrl:Ljava/lang/String;
    :cond_1b
    move-object v1, p1

    .line 64
    .restart local v1       #proxyUrl:Ljava/lang/String;
    const/4 p1, 0x0

    goto :goto_e
.end method

.method private static setImageUrlSizeOptions(IILandroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .registers 19
    .parameter "width"
    .parameter "height"
    .parameter "proxyUri"
    .parameter "imageUrl"

    .prologue
    .line 158
    sget-object v9, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 159
    .local v6, proxyUriBuilder:Landroid/net/Uri$Builder;
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 160
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 161
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 163
    const/4 v9, -0x1

    if-eq p0, v9, :cond_3c

    const/4 v9, -0x1

    move/from16 v0, p1

    if-eq v0, v9, :cond_3c

    .line 164
    const-string v9, "resize_w"

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 165
    const-string v9, "resize_h"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 166
    const-string v9, "no_expand"

    const-string v10, "1"

    invoke-virtual {v6, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 169
    :cond_3c
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 171
    .local v4, newProxyUri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->isOpaque()Z

    move-result v9

    if-eqz v9, :cond_4e

    new-instance v9, Ljava/lang/UnsupportedOperationException;

    const-string v10, "This isn\'t a hierarchical URI."

    invoke-direct {v9, v10}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_4e
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v12

    if-nez v12, :cond_8c

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v5

    .line 172
    .local v5, paramNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :goto_58
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5c
    :goto_5c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_fd

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 173
    .local v3, key:Ljava/lang/String;
    invoke-virtual {v4, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_5c

    .line 174
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 178
    const-string v9, "url"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c3

    .line 180
    const-string v9, "url"

    const-string v10, "url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 195
    :cond_87
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    goto :goto_5c

    .line 171
    .end local v3           #key:Ljava/lang/String;
    .end local v5           #paramNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_8c
    new-instance v13, Ljava/util/LinkedHashSet;

    invoke-direct {v13}, Ljava/util/LinkedHashSet;-><init>()V

    const/4 v9, 0x0

    :cond_92
    const/16 v10, 0x26

    invoke-virtual {v12, v10, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_9f

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v10

    :cond_9f
    const/16 v11, 0x3d

    invoke-virtual {v12, v11, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v11

    if-gt v11, v10, :cond_aa

    const/4 v14, -0x1

    if-ne v11, v14, :cond_ab

    :cond_aa
    move v11, v10

    :cond_ab
    invoke-virtual {v12, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v13, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v10, 0x1

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v10

    if-lt v9, v10, :cond_92

    invoke-static {v13}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    goto :goto_58

    .line 183
    .restart local v3       #key:Ljava/lang/String;
    .restart local v5       #paramNames:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_c3
    const/4 v9, -0x1

    if-eq p0, v9, :cond_cb

    const/4 v9, -0x1

    move/from16 v0, p1

    if-ne v0, v9, :cond_e3

    :cond_cb
    const-string v9, "resize_w"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5c

    const-string v9, "resize_h"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5c

    const-string v9, "no_expand"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5c

    .line 187
    :cond_e3
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 191
    .local v8, values:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_ed
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_87

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 192
    .local v7, value:Ljava/lang/String;
    invoke-virtual {v6, v3, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_ed

    .line 199
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #key:Ljava/lang/String;
    .end local v7           #value:Ljava/lang/String;
    .end local v8           #values:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_fd
    if-eqz p3, :cond_116

    const-string v9, "url"

    invoke-virtual {v4, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_116

    .line 200
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 201
    const-string v9, "url"

    move-object/from16 v0, p3

    invoke-virtual {v6, v9, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 202
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 204
    :cond_116
    const-string v9, "container"

    invoke-virtual {v4, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_12d

    .line 205
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 206
    const-string v9, "container"

    const-string v10, "esmobile"

    invoke-virtual {v6, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 207
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 209
    :cond_12d
    const-string v9, "gadget"

    invoke-virtual {v4, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_144

    .line 210
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 211
    const-string v9, "gadget"

    const-string v10, "a"

    invoke-virtual {v6, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 212
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 214
    :cond_144
    const-string v9, "rewriteMime"

    invoke-virtual {v4, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_15b

    .line 215
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 216
    const-string v9, "rewriteMime"

    const-string v10, "image/*"

    invoke-virtual {v6, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 217
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 220
    :cond_15b
    return-object v4
.end method
