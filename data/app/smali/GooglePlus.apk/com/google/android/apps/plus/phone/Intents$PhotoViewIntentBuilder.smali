.class public final Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
.super Ljava/lang/Object;
.source "Intents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/Intents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoViewIntentBuilder"
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAllowPlusOne:Ljava/lang/Boolean;

.field private mDisplayName:Ljava/lang/String;

.field private mEventId:Ljava/lang/String;

.field private mForceLoadId:Ljava/lang/Long;

.field private mGaiaId:Ljava/lang/String;

.field private final mIntent:Landroid/content/Intent;

.field private mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private mNotificationId:Ljava/lang/String;

.field private mPageHint:Ljava/lang/Integer;

.field private mPhotoId:Ljava/lang/Long;

.field private mPhotoOfUserId:Ljava/lang/String;

.field private mPhotoOnly:Ljava/lang/Boolean;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPhotoUrl:Ljava/lang/String;

.field private mRefreshAlbumId:Ljava/lang/String;

.field private mStreamId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .registers 4
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2377
    .local p2, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    .line 2379
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Class;B)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 2335
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final build()Landroid/content/Intent;
    .registers 8

    .prologue
    .line 2497
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_c

    .line 2498
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Account must be set"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2501
    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2502
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2504
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAlbumId:Ljava/lang/String;

    if-eqz v1, :cond_29

    .line 2505
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "album_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2508
    :cond_29
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAlbumName:Ljava/lang/String;

    if-eqz v1, :cond_36

    .line 2509
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "album_name"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2512
    :cond_36
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAllowPlusOne:Ljava/lang/Boolean;

    if-eqz v1, :cond_47

    .line 2513
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "allow_plusone"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAllowPlusOne:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2516
    :cond_47
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mDisplayName:Ljava/lang/String;

    if-eqz v1, :cond_54

    .line 2517
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "display_name"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2520
    :cond_54
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mEventId:Ljava/lang/String;

    if-eqz v1, :cond_61

    .line 2521
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "event_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mEventId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2524
    :cond_61
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mForceLoadId:Ljava/lang/Long;

    if-eqz v1, :cond_72

    .line 2525
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "force_load_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mForceLoadId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2528
    :cond_72
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mRefreshAlbumId:Ljava/lang/String;

    if-eqz v1, :cond_7f

    .line 2529
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "refresh_album_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mRefreshAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2532
    :cond_7f
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_8c

    .line 2533
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "mediarefs"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2536
    :cond_8c
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mNotificationId:Ljava/lang/String;

    if-eqz v1, :cond_99

    .line 2537
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "notif_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2540
    :cond_99
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_a6

    .line 2541
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "owner_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2544
    :cond_a6
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPageHint:Ljava/lang/Integer;

    if-eqz v1, :cond_117

    .line 2545
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "page_hint"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPageHint:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2550
    :goto_b7
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoId:Ljava/lang/Long;

    if-eqz v1, :cond_c8

    .line 2551
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photo_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2554
    :cond_c8
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    if-eqz v1, :cond_d5

    .line 2559
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photos_of_user_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2562
    :cond_d5
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoOnly:Ljava/lang/Boolean;

    if-eqz v1, :cond_e1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoOnly:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_e5

    :cond_e1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_ed

    .line 2563
    :cond_e5
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "show_photo_only"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2566
    :cond_ed
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_120

    .line 2567
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photo_ref"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2577
    :cond_fa
    :goto_fa
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    if-eqz v1, :cond_107

    .line 2578
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photo_url"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2581
    :cond_107
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mStreamId:Ljava/lang/String;

    if-eqz v1, :cond_114

    .line 2582
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "stream_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mStreamId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2585
    :cond_114
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    return-object v1

    .line 2547
    :cond_117
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "page_hint"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_b7

    .line 2570
    :cond_120
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoId:Ljava/lang/Long;

    if-eqz v1, :cond_fa

    .line 2571
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mGaiaId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 2573
    .local v0, photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "photo_ref"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_fa
.end method

.method public final setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "account"

    .prologue
    .line 2383
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 2384
    return-object p0
.end method

.method public final setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "albumId"

    .prologue
    .line 2389
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAlbumId:Ljava/lang/String;

    .line 2390
    return-object p0
.end method

.method public final setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "albumName"

    .prologue
    .line 2395
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAlbumName:Ljava/lang/String;

    .line 2396
    return-object p0
.end method

.method public final setAllowPlusOne(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "allowPlusOne"

    .prologue
    .line 2401
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mAllowPlusOne:Ljava/lang/Boolean;

    .line 2402
    return-object p0
.end method

.method public final setDisplayName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "displayName"

    .prologue
    .line 2407
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mDisplayName:Ljava/lang/String;

    .line 2408
    return-object p0
.end method

.method public final setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "eventId"

    .prologue
    .line 2413
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mEventId:Ljava/lang/String;

    .line 2414
    return-object p0
.end method

.method public final setForceLoadId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "forceLoadId"

    .prologue
    .line 2419
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mForceLoadId:Ljava/lang/Long;

    .line 2420
    return-object p0
.end method

.method public final setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "gaiaId"

    .prologue
    .line 2431
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mGaiaId:Ljava/lang/String;

    .line 2432
    return-object p0
.end method

.method public final setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "mediaRefs"

    .prologue
    .line 2437
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    .line 2438
    return-object p0
.end method

.method public final setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "notificationId"

    .prologue
    .line 2443
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mNotificationId:Ljava/lang/String;

    .line 2444
    return-object p0
.end method

.method public final setPageHint(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "pageHint"

    .prologue
    .line 2449
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPageHint:Ljava/lang/Integer;

    .line 2450
    return-object p0
.end method

.method public final setPhotoId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "photoId"

    .prologue
    .line 2455
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoId:Ljava/lang/Long;

    .line 2456
    return-object p0
.end method

.method public final setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "photoOfUserId"

    .prologue
    .line 2467
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    .line 2468
    return-object p0
.end method

.method public final setPhotoOnly(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "photoOnly"

    .prologue
    .line 2473
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoOnly:Ljava/lang/Boolean;

    .line 2474
    return-object p0
.end method

.method public final setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "photoRef"

    .prologue
    .line 2479
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 2480
    return-object p0
.end method

.method public final setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "photoUrl"

    .prologue
    .line 2485
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mPhotoUrl:Ljava/lang/String;

    .line 2486
    return-object p0
.end method

.method public final setRefreshAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "refreshAlbumId"

    .prologue
    .line 2425
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mRefreshAlbumId:Ljava/lang/String;

    .line 2426
    return-object p0
.end method

.method public final setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .registers 2
    .parameter "streamId"

    .prologue
    .line 2491
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->mStreamId:Ljava/lang/String;

    .line 2492
    return-object p0
.end method
