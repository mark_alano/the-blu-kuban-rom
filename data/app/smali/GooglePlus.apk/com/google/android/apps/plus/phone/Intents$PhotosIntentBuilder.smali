.class public final Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
.super Ljava/lang/Object;
.source "Intents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/Intents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotosIntentBuilder"
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumType:Ljava/lang/String;

.field private mAllowCrop:Ljava/lang/Boolean;

.field private mDoNotificationRefresh:Ljava/lang/Boolean;

.field private mGaiaId:Ljava/lang/String;

.field private final mIntent:Landroid/content/Intent;

.field private mNotificationId:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;

.field private mPhotoOfUserId:Ljava/lang/String;

.field private mPhotoPickerMediaRefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mPhotoPickerMode:Ljava/lang/Integer;

.field private mPhotoPickerShareOnZeroSelected:Ljava/lang/Boolean;

.field private mPhotoPickerTitleResourceId:Ljava/lang/Integer;

.field private mPhotosHome:Ljava/lang/Boolean;

.field private mShowCameraAlbum:Ljava/lang/Boolean;

.field private mStreamId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .registers 4
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2659
    .local p2, cls:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2660
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    .line 2661
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Class;B)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 2596
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public final build()Landroid/content/Intent;
    .registers 5

    .prologue
    .line 2834
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v2, :cond_c

    .line 2835
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Account must be set"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2838
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2839
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2840
    .local v0, extras:Landroid/os/Bundle;
    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2842
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumId:Ljava/lang/String;

    if-eqz v2, :cond_2a

    .line 2843
    const-string v2, "album_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2846
    :cond_2a
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumName:Ljava/lang/String;

    if-eqz v2, :cond_35

    .line 2847
    const-string v2, "album_name"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2850
    :cond_35
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumType:Ljava/lang/String;

    if-eqz v2, :cond_40

    .line 2851
    const-string v2, "album_type"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumType:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2854
    :cond_40
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mNotificationId:Ljava/lang/String;

    if-eqz v2, :cond_4b

    .line 2875
    const-string v2, "notif_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2878
    :cond_4b
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mDoNotificationRefresh:Ljava/lang/Boolean;

    if-eqz v2, :cond_5a

    .line 2879
    const-string v2, "do_notif_refresh"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mDoNotificationRefresh:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2882
    :cond_5a
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_65

    .line 2883
    const-string v2, "owner_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2886
    :cond_65
    const-string v2, "page_hint"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2892
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPersonId:Ljava/lang/String;

    if-eqz v2, :cond_76

    .line 2893
    const-string v2, "person_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPersonId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2896
    :cond_76
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    if-eqz v2, :cond_81

    .line 2905
    const-string v2, "photos_of_user_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2908
    :cond_81
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotosHome:Ljava/lang/Boolean;

    if-eqz v2, :cond_90

    .line 2928
    const-string v2, "photos_home"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotosHome:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2931
    :cond_90
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mShowCameraAlbum:Ljava/lang/Boolean;

    if-eqz v2, :cond_9f

    .line 2932
    const-string v2, "photos_show_camera_album"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mShowCameraAlbum:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2935
    :cond_9f
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mStreamId:Ljava/lang/String;

    if-eqz v2, :cond_aa

    .line 2936
    const-string v2, "stream_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mStreamId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2939
    :cond_aa
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMode:Ljava/lang/Integer;

    if-eqz v2, :cond_b9

    .line 2940
    const-string v2, "photo_picker_mode"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMode:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2943
    :cond_b9
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerTitleResourceId:Ljava/lang/Integer;

    if-eqz v2, :cond_c8

    .line 2944
    const-string v2, "photo_picker_title"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerTitleResourceId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2947
    :cond_c8
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerShareOnZeroSelected:Ljava/lang/Boolean;

    if-eqz v2, :cond_d7

    .line 2948
    const-string v2, "photo_picker_share_on_zero"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerShareOnZeroSelected:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2952
    :cond_d7
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    if-eqz v2, :cond_ed

    .line 2953
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v1, v2, [Lcom/google/android/apps/plus/api/MediaRef;

    .line 2954
    .local v1, mediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2955
    const-string v2, "photo_picker_selected"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 2958
    .end local v1           #mediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;
    :cond_ed
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAllowCrop:Ljava/lang/Boolean;

    if-eqz v2, :cond_fc

    .line 2959
    const-string v2, "allow_crop"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAllowCrop:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2962
    :cond_fc
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2963
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mIntent:Landroid/content/Intent;

    return-object v2
.end method

.method public final setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "account"

    .prologue
    .line 2665
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 2666
    return-object p0
.end method

.method public final setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "albumId"

    .prologue
    .line 2671
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumId:Ljava/lang/String;

    .line 2672
    return-object p0
.end method

.method public final setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "albumName"

    .prologue
    .line 2677
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumName:Ljava/lang/String;

    .line 2678
    return-object p0
.end method

.method public final setAlbumType(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "albumType"

    .prologue
    .line 2683
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAlbumType:Ljava/lang/String;

    .line 2684
    return-object p0
.end method

.method public final setAllowCrop(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "enabled"

    .prologue
    .line 2828
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mAllowCrop:Ljava/lang/Boolean;

    .line 2829
    return-object p0
.end method

.method public final setDoNotificationRefresh(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 3
    .parameter "enabled"

    .prologue
    .line 2734
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mDoNotificationRefresh:Ljava/lang/Boolean;

    .line 2735
    return-object p0
.end method

.method public final setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "gaiaId"

    .prologue
    .line 2713
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mGaiaId:Ljava/lang/String;

    .line 2714
    return-object p0
.end method

.method public final setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "notificationId"

    .prologue
    .line 2725
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mNotificationId:Ljava/lang/String;

    .line 2726
    return-object p0
.end method

.method public final setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "personId"

    .prologue
    .line 2746
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPersonId:Ljava/lang/String;

    .line 2747
    return-object p0
.end method

.method public final setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "photoOfUserId"

    .prologue
    .line 2764
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoOfUserId:Ljava/lang/String;

    .line 2765
    return-object p0
.end method

.method public final setPhotoPickerInitiallySelected(Ljava/util/ArrayList;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)",
            "Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;"
        }
    .end annotation

    .prologue
    .line 2817
    .local p1, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMediaRefs:Ljava/util/ArrayList;

    .line 2818
    return-object p0
.end method

.method public final setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "mode"

    .prologue
    .line 2806
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerMode:Ljava/lang/Integer;

    .line 2807
    return-object p0
.end method

.method public final setPhotoPickerShareOnZeroSelected(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 3
    .parameter "shareOnZeroSelected"

    .prologue
    .line 2822
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerShareOnZeroSelected:Ljava/lang/Boolean;

    .line 2823
    return-object p0
.end method

.method public final setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "id"

    .prologue
    .line 2812
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotoPickerTitleResourceId:Ljava/lang/Integer;

    .line 2813
    return-object p0
.end method

.method public final setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "photosHome"

    .prologue
    .line 2788
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mPhotosHome:Ljava/lang/Boolean;

    .line 2789
    return-object p0
.end method

.method public final setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "showCameraAlbum"

    .prologue
    .line 2794
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mShowCameraAlbum:Ljava/lang/Boolean;

    .line 2795
    return-object p0
.end method

.method public final setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    .registers 2
    .parameter "streamId"

    .prologue
    .line 2800
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->mStreamId:Ljava/lang/String;

    .line 2801
    return-object p0
.end method
