.class public Lcom/google/android/apps/plus/phone/InviteContactActivity;
.super Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;
.source "InviteContactActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/InviteContactActivity$EmailPickerDialog;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final ENTITY_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 43
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->ENTITY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;-><init>()V

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mHandler:Landroid/os/Handler;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/InviteContactActivity;Ljava/util/ArrayList;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 38
    :try_start_0
    new-instance v1, Lcom/google/android/apps/plus/phone/InviteContactActivity$EmailPickerDialog;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/phone/InviteContactActivity$EmailPickerDialog;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "pick_email"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/plus/phone/InviteContactActivity$EmailPickerDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_17} :catch_18

    :goto_17
    return-void

    :catch_18
    move-exception v0

    const-string v1, "InviteContactActivity"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_29

    const-string v1, "InviteContactActivity"

    const-string v2, "Cannot show dialog"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_29
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    goto :goto_17
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 7
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 264
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2b

    .line 265
    const/4 v1, 0x0

    .line 266
    .local v1, success:Z
    const/4 v2, -0x1

    if-ne p2, v2, :cond_25

    .line 267
    const-string v2, "person_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mPersonId:Ljava/lang/String;

    .line 268
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mPersonId:Ljava/lang/String;

    if-eqz v2, :cond_25

    .line 269
    const-string v2, "person_data"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/PersonData;

    .line 271
    .local v0, person:Lcom/google/android/apps/plus/content/PersonData;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mPersonName:Ljava/lang/String;

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->showCirclePicker()V

    .line 273
    const/4 v1, 0x1

    .line 277
    .end local v0           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_25
    if-nez v1, :cond_2a

    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    .line 283
    .end local v1           #success:Z
    :cond_2a
    :goto_2a
    return-void

    .line 281
    :cond_2b
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_2a
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsProfileGatewayActivity;->onCreate(Landroid/os/Bundle;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 139
    :goto_9
    return-void

    .line 129
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 130
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 131
    .local v2, uri:Landroid/net/Uri;
    if-nez v2, :cond_18

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    goto :goto_9

    .line 136
    :cond_18
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 137
    .local v0, args:Landroid/os/Bundle;
    const-string v3, "data_uri"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_9
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 11
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 146
    const-string v0, "data_uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    .line 147
    .local v7, contactUri:Landroid/net/Uri;
    const-string v0, "entities"

    invoke-static {v7, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 148
    .local v2, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/phone/InviteContactActivity;->ENTITY_PROJECTION:[Ljava/lang/String;

    const-string v4, "mimetype IN (\'vnd.android.cursor.item/name\',\'vnd.android.cursor.item/email_v2\')"

    move-object v1, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v3, 0x1

    .line 38
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    if-nez p2, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->finish()V

    :cond_8
    :goto_8
    return-void

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mRedirected:Z

    if-nez v0, :cond_8

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mRedirected:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_14
    :goto_14
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_44

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_23

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mPersonName:Ljava/lang/String;

    :cond_23
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3a

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :cond_3a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_14

    :cond_44
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_58

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/phone/InviteContactActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity$1;-><init>(Lcom/google/android/apps/plus/phone/InviteContactActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_8

    :cond_58
    if-ne v0, v3, :cond_65

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/phone/InviteContactActivity$2;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/plus/phone/InviteContactActivity$2;-><init>(Lcom/google/android/apps/plus/phone/InviteContactActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_8

    :cond_65
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/phone/InviteContactActivity$3;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/plus/phone/InviteContactActivity$3;-><init>(Lcom/google/android/apps/plus/phone/InviteContactActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_8
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final showCirclePicker(Ljava/lang/String;)V
    .registers 4
    .parameter "email"

    .prologue
    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "e:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mPersonId:Ljava/lang/String;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->showCirclePicker()V

    .line 257
    return-void
.end method

.method protected final showSearchActivity()V
    .registers 11

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 228
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/InviteContactActivity;->mPersonName:Ljava/lang/String;

    const/4 v4, -0x1

    move-object v0, p0

    move v5, v3

    move v7, v3

    move v8, v6

    move v9, v6

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/phone/Intents;->getPeopleSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZIZZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/phone/InviteContactActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 231
    return-void
.end method
