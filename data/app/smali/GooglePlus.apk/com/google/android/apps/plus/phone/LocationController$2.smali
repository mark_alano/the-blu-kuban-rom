.class final Lcom/google/android/apps/plus/phone/LocationController$2;
.super Ljava/lang/Thread;
.source "LocationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/LocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/LocationController;

.field final synthetic val$location:Landroid/location/Location;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 393
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 12

    .prologue
    const/4 v3, 0x0

    .line 400
    new-instance v0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$1300(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/LocationController;->access$1400(Lcom/google/android/apps/plus/phone/LocationController;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-direct {v5, v4, v3}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/LocationQuery;Z)V

    .line 403
    .local v0, op:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->start()V

    .line 404
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 405
    .local v8, extras:Landroid/os/Bundle;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mDisplayDebugToast:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$200(Lcom/google/android/apps/plus/phone/LocationController;)Z

    move-result v1

    if-eqz v1, :cond_44

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_44

    .line 406
    const-string v1, "location_source"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "location_source"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_44
    const/4 v7, 0x0

    .line 411
    .local v7, description:Ljava/lang/String;
    const/4 v10, 0x0

    .line 412
    .local v10, locationValue:Lcom/google/android/apps/plus/content/DbLocation;
    const/4 v9, 0x0

    .line 413
    .local v9, key:Ljava/lang/String;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->hasPreciseLocation()Z

    move-result v1

    if-eqz v1, :cond_7b

    .line 414
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->getPreciseLocation()Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v10

    .line 415
    const-string v9, "finest_location"

    .line 424
    :cond_53
    :goto_53
    if-eqz v10, :cond_67

    .line 425
    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 426
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v7

    .line 427
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_67

    .line 428
    const-string v1, "location_description"

    invoke-virtual {v8, v1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_67
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-virtual {v1, v8}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$1200(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/phone/LocationController$2$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/LocationController$2$1;-><init>(Lcom/google/android/apps/plus/phone/LocationController$2;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 452
    return-void

    .line 416
    :cond_7b
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->hasPlaceLocation()Z

    move-result v1

    if-eqz v1, :cond_88

    .line 417
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->getPlaceLocation()Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v10

    .line 418
    const-string v9, "finest_location"

    goto :goto_53

    .line 419
    :cond_88
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->hasCoarseLocation()Z

    move-result v1

    if-eqz v1, :cond_53

    .line 420
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->getCoarseLocation()Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v10

    .line 421
    const-string v9, "coarse_location"

    goto :goto_53
.end method
