.class final Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;
.super Ljava/lang/Object;
.source "LocationController.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/LocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/LocationController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/phone/LocationController;)V
    .registers 2
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/phone/LocationController;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;-><init>(Lcom/google/android/apps/plus/phone/LocationController;)V

    return-void
.end method

.method private triggerLocationListener()V
    .registers 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$500(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$500(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-ne v0, p0, :cond_18

    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$600(Lcom/google/android/apps/plus/phone/LocationController;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_77

    .line 129
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$700(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$700(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->areSameLocations(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_77

    .line 130
    :cond_32
    const-string v0, "LocationController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 131
    const-string v1, "LocationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "----> onLocationChanged: triggering location change because "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$600(Lcom/google/android/apps/plus/phone/LocationController;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_7d

    const-string v0, "only this location listener was registered"

    :goto_4e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_59
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mReverseGeo:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$800(Lcom/google/android/apps/plus/phone/LocationController;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$900(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)V

    .line 143
    :cond_6c
    :goto_6c
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    #setter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$702(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)Landroid/location/Location;

    .line 147
    :cond_77
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #setter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;
    invoke-static {v0, p0}, Lcom/google/android/apps/plus/phone/LocationController;->access$502(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/LocationListener;)Landroid/location/LocationListener;

    .line 148
    return-void

    .line 131
    :cond_7d
    const-string v0, "a previous location listener was successful"

    goto :goto_4e

    .line 139
    :cond_80
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_6c

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_6c
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .registers 8
    .parameter "location"

    .prologue
    const/4 v5, 0x3

    .line 65
    const-string v1, "LocationController"

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 66
    const-string v1, "LocationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "====> onLocationChanged: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from provider: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_2f
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/google/android/apps/plus/phone/LocationController;->access$100(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;Landroid/location/Location;)Z

    move-result v1

    if-nez v1, :cond_49

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_48

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->triggerLocationListener()V

    .line 90
    :cond_48
    :goto_48
    return-void

    .line 80
    :cond_49
    const-string v1, "LocationController"

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_69

    .line 81
    const-string v1, "LocationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "----> onLocationChanged: new location: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_69
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #setter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/phone/LocationController;->access$002(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)Landroid/location/Location;

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mDisplayDebugToast:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$200(Lcom/google/android/apps/plus/phone/LocationController;)Z

    move-result v1

    if-eqz v1, :cond_8f

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$300(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v1

    if-ne p0, v1, :cond_93

    const-string v0, "net: "

    .line 87
    .local v0, source:Ljava/lang/String;
    :goto_80
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "location_source"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .end local v0           #source:Ljava/lang/String;
    :cond_8f
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->triggerLocationListener()V

    goto :goto_48

    .line 86
    :cond_93
    const-string v0, "gps: "

    goto :goto_80
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .registers 3
    .parameter "provider"

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderDisabled(Ljava/lang/String;)V

    .line 100
    :cond_11
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .registers 3
    .parameter "provider"

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderEnabled(Ljava/lang/String;)V

    .line 110
    :cond_11
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 5
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    #getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/location/LocationListener;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 120
    :cond_11
    return-void
.end method
