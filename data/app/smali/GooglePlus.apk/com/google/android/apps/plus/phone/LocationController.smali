.class public final Lcom/google/android/apps/plus/phone/LocationController;
.super Ljava/lang/Object;
.source "LocationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mContext:Landroid/content/Context;

.field private final mDisplayDebugToast:Z

.field private mGpsListener:Landroid/location/LocationListener;

.field private final mHandler:Landroid/os/Handler;

.field private mLastSentLocation:Landroid/location/Location;

.field private mLastSuccessfulLocationListener:Landroid/location/LocationListener;

.field private mListener:Landroid/location/LocationListener;

.field private mLocation:Landroid/location/Location;

.field private mLocationAcquisitionTimer:Ljava/lang/Runnable;

.field private final mLocationManager:Landroid/location/LocationManager;

.field private mNetworkListener:Landroid/location/LocationListener;

.field private final mReverseGeo:Z

.field private final mTimeout:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V
    .registers 18
    .parameter "context"
    .parameter "account"
    .parameter "reverseGeo"
    .parameter "timeout"
    .parameter "lastLocation"
    .parameter "listener"

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mContext:Landroid/content/Context;

    .line 164
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/LocationController;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 165
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;

    .line 166
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;

    .line 167
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mReverseGeo:Z

    .line 168
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;

    .line 169
    const-wide/16 v1, 0xbb8

    iput-wide v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mTimeout:J

    .line 171
    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v9

    .line 173
    .local v9, providers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v8

    .line 174
    .local v8, providerCount:I
    const/4 v7, 0x0

    .local v7, i:I
    :goto_33
    if-ge v7, v8, :cond_55

    .line 175
    const-string v1, "network"

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_87

    .line 176
    new-instance v1, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;-><init>(Lcom/google/android/apps/plus/phone/LocationController;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    const-string v2, "network"

    const-wide/16 v3, 0xbb8

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;

    invoke-virtual/range {v1 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 183
    :cond_55
    const/4 v7, 0x0

    :goto_56
    if-ge v7, v8, :cond_78

    .line 184
    const-string v1, "gps"

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8a

    .line 185
    new-instance v1, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;-><init>(Lcom/google/android/apps/plus/phone/LocationController;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mGpsListener:Landroid/location/LocationListener;

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    const-string v2, "gps"

    const-wide/16 v3, 0xbb8

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/LocationController;->mGpsListener:Landroid/location/LocationListener;

    invoke-virtual/range {v1 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 192
    :cond_78
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->LOCATION_DEBUGGING:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TRUE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mDisplayDebugToast:Z

    .line 193
    return-void

    .line 174
    :cond_87
    add-int/lit8 v7, v7, 0x1

    goto :goto_33

    .line 183
    :cond_8a
    add-int/lit8 v7, v7, 0x1

    goto :goto_56
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)Landroid/location/Location;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;Landroid/location/Location;)Z
    .registers 15
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    const/4 v11, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 30
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/apps/plus/phone/LocationController;->mTimeout:J

    sub-long/2addr v5, v7

    const-wide/32 v7, 0x493e0

    sub-long/2addr v5, v7

    cmp-long v0, v3, v5

    if-gez v0, :cond_3d

    const-string v0, "LocationController"

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3b

    const-string v0, "LocationController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "----> isMoreAccurateLocation: stale: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3b
    move v1, v2

    :cond_3c
    :goto_3c
    return v1

    :cond_3d
    if-nez p2, :cond_4f

    const-string v0, "LocationController"

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3c

    const-string v0, "LocationController"

    const-string v2, "----> isMoreAccurateLocation: no old location"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3c

    :cond_4f
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    sub-float/2addr v0, v3

    float-to-int v0, v0

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/phone/LocationController;->areSameLocations(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v3

    if-eqz v3, :cond_70

    const-string v0, "LocationController"

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6e

    const-string v0, "LocationController"

    const-string v1, "----> isMoreAccurateLocation: same location"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6e
    move v1, v2

    goto :goto_3c

    :cond_70
    if-lez v0, :cond_b1

    move v5, v1

    :goto_73
    if-gez v0, :cond_b3

    move v4, v1

    :goto_76
    const/16 v3, 0xc8

    if-le v0, v3, :cond_b5

    move v0, v1

    :goto_7b
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v9

    cmp-long v3, v7, v9

    if-lez v3, :cond_b7

    move v3, v1

    :goto_94
    if-nez v4, :cond_3c

    if-eqz v3, :cond_9a

    if-eqz v5, :cond_3c

    :cond_9a
    if-eqz v3, :cond_a0

    if-nez v0, :cond_a0

    if-nez v6, :cond_3c

    :cond_a0
    const-string v0, "LocationController"

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_af

    const-string v0, "LocationController"

    const-string v1, "----> isMoreAccurateLocation: less accurate location"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_af
    move v1, v2

    goto :goto_3c

    :cond_b1
    move v5, v2

    goto :goto_73

    :cond_b3
    move v4, v2

    goto :goto_76

    :cond_b5
    move v0, v2

    goto :goto_7b

    :cond_b7
    move v3, v2

    goto :goto_94
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/phone/LocationController;)J
    .registers 3
    .parameter "x0"

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mTimeout:J

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/phone/LocationController;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/LocationController;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mDisplayDebugToast:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/LocationListener;)Landroid/location/LocationListener;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/LocationController;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)Landroid/location/Location;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/phone/LocationController;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mReverseGeo:Z

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/phone/LocationController$2;-><init>(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController$2;->start()V

    return-void
.end method

.method public static areSameLocations(Landroid/location/Location;Landroid/location/Location;)Z
    .registers 6
    .parameter "location1"
    .parameter "location2"

    .prologue
    .line 497
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2a

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2a

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2a

    const/4 v0, 0x1

    :goto_29
    return v0

    :cond_2a
    const/4 v0, 0x0

    goto :goto_29
.end method

.method private canFindLocation()Z
    .registers 3

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private static canFindLocation(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    .prologue
    .line 274
    const-string v1, "location"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 276
    .local v0, locationManager:Landroid/location/LocationManager;
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    :cond_18
    const/4 v1, 0x1

    :goto_19
    return v1

    :cond_1a
    const/4 v1, 0x0

    goto :goto_19
.end method

.method public static getCityLevelLocation(Landroid/location/Location;)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 3
    .parameter "location"

    .prologue
    .line 480
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/LocationController;->getExtras(Landroid/location/Location;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "coarse_location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method private static getExtras(Landroid/location/Location;)Landroid/os/Bundle;
    .registers 2
    .parameter "location"

    .prologue
    .line 460
    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 461
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_7

    .end local v0           #extras:Landroid/os/Bundle;
    :goto_6
    return-object v0

    .restart local v0       #extras:Landroid/os/Bundle;
    :cond_7
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_6
.end method

.method public static getFormattedAddress(Landroid/location/Location;)Ljava/lang/String;
    .registers 3
    .parameter "location"

    .prologue
    .line 471
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/LocationController;->getExtras(Landroid/location/Location;)Landroid/os/Bundle;

    move-result-object v0

    .line 472
    .local v0, extras:Landroid/os/Bundle;
    const-class v1, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 473
    const-string v1, "location_description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getStreetLevelLocation(Landroid/location/Location;)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 3
    .parameter "location"

    .prologue
    .line 487
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/LocationController;->getExtras(Landroid/location/Location;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finest_location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method public static isProviderEnabled(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    .prologue
    .line 250
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_b

    .line 251
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/LocationController;->canFindLocation(Landroid/content/Context;)Z

    move-result v0

    .line 264
    :goto_a
    return v0

    .line 255
    :cond_b
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_1a

    .line 264
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/LocationController;->canFindLocation(Landroid/content/Context;)Z

    move-result v0

    goto :goto_a

    .line 257
    :pswitch_17
    const/4 v0, 0x0

    goto :goto_a

    .line 255
    nop

    :pswitch_data_1a
    .packed-switch 0x0
        :pswitch_17
    .end packed-switch
.end method


# virtual methods
.method public final init()V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    if-eqz v0, :cond_e

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 201
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    .line 204
    :cond_e
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;

    .line 206
    iget-wide v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mTimeout:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_30

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mGpsListener:Landroid/location/LocationListener;

    if-eqz v0, :cond_30

    .line 207
    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/LocationController$1;-><init>(Lcom/google/android/apps/plus/phone/LocationController;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/LocationController;->mTimeout:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 239
    :cond_30
    return-void
.end method

.method public final isProviderEnabled()Z
    .registers 3

    .prologue
    .line 288
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_b

    .line 289
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/LocationController;->canFindLocation()Z

    move-result v0

    .line 302
    :goto_a
    return v0

    .line 293
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_1c

    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/LocationController;->canFindLocation()Z

    move-result v0

    goto :goto_a

    .line 295
    :pswitch_19
    const/4 v0, 0x0

    goto :goto_a

    .line 293
    nop

    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_19
    .end packed-switch
.end method

.method public final release()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mGpsListener:Landroid/location/LocationListener;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController;->mGpsListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 317
    :cond_24
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;

    .line 318
    return-void
.end method
