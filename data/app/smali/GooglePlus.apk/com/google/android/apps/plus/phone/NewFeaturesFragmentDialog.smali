.class public Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "NewFeaturesFragmentDialog.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 4
    .parameter "account"

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 39
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 41
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    .line 42
    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 19
    .parameter "savedInstanceState"

    .prologue
    .line 46
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 47
    .local v3, context:Landroid/support/v4/app/FragmentActivity;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "account"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    .line 49
    .local v1, account:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 52
    .local v2, builder:Landroid/app/AlertDialog$Builder;
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    const v14, 0x7f0300d7

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 54
    .local v12, view:Landroid/view/View;
    const v13, 0x7f09005a

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 55
    .local v8, message:Landroid/widget/TextView;
    const v13, 0x7f0802bd

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 57
    .local v5, hyperlinkMessage:Ljava/lang/String;
    new-instance v10, Landroid/text/SpannableString;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Lcom/google/android/apps/plus/util/AndroidUtils;->hasTelephony(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_df

    const v13, 0x7f0802ba

    :goto_43
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f0802bc

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 65
    .local v10, s:Landroid/text/SpannableString;
    new-instance v13, Landroid/text/style/BulletSpan;

    const/16 v14, 0x10

    invoke-direct {v13, v14}, Landroid/text/style/BulletSpan;-><init>(I)V

    const/4 v14, 0x0

    invoke-virtual {v10}, Landroid/text/SpannableString;->length()I

    move-result v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v10, v13, v14, v15, v0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f080413

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 68
    .local v4, helpUrlParam:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 69
    .local v6, link:Landroid/net/Uri;
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 70
    .local v11, scheme:Ljava/lang/String;
    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    .line 71
    .local v9, pattern:Ljava/util/regex/Pattern;
    invoke-static {v10, v9, v11}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    .line 72
    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 74
    invoke-virtual {v2, v12}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 77
    const v13, 0x7f0803b9

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 78
    new-instance v7, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$1;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v3, v1}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$1;-><init>(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 102
    .local v7, listener:Landroid/content/DialogInterface$OnClickListener;
    const v13, 0x7f0803ba

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 103
    const v13, 0x7f0803bb

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 104
    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 105
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    return-object v13

    .line 57
    .end local v4           #helpUrlParam:Ljava/lang/String;
    .end local v6           #link:Landroid/net/Uri;
    .end local v7           #listener:Landroid/content/DialogInterface$OnClickListener;
    .end local v9           #pattern:Ljava/util/regex/Pattern;
    .end local v10           #s:Landroid/text/SpannableString;
    .end local v11           #scheme:Ljava/lang/String;
    :cond_df
    const v13, 0x7f0802bb

    goto/16 :goto_43
.end method
