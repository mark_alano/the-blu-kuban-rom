.class public abstract Lcom/google/android/apps/plus/phone/OobDeviceActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "OobDeviceActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v0, 0x0

    .line 114
    packed-switch p1, :pswitch_data_14

    .line 125
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 126
    :cond_7
    :goto_7
    return-void

    .line 116
    :pswitch_8
    if-eqz p2, :cond_7

    .line 117
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->setResult(I)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->finish()V

    .line 119
    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->overridePendingTransition(II)V

    goto :goto_7

    .line 114
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 74
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->isInitialOobIntent$755b117a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 75
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    .line 78
    :cond_11
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->setResult(I)V

    .line 79
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    .line 80
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    .line 58
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_10

    .line 66
    :goto_7
    return-void

    .line 60
    :pswitch_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onBackPressed()V

    goto :goto_7

    .line 63
    :pswitch_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinuePressed()V

    goto :goto_7

    .line 58
    :pswitch_data_10
    .packed-switch 0x1020019
        :pswitch_8
        :pswitch_c
    .end packed-switch
.end method

.method public onContinue()V
    .registers 5

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 96
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "plus_pages"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AccountSettingsData;

    .line 97
    .local v2, settings:Lcom/google/android/apps/plus/content/AccountSettingsData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {p0, v3, v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNextOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    .line 98
    .local v1, nextIntent:Landroid/content/Intent;
    if-eqz v1, :cond_1b

    .line 100
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 107
    :goto_1a
    return-void

    .line 103
    :cond_1b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->setOobComplete(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 104
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->setResult(I)V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->finish()V

    goto :goto_1a
.end method

.method public onContinuePressed()V
    .registers 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    .line 88
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    .prologue
    const v6, 0x102001a

    .line 35
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 38
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 39
    .local v2, intent:Landroid/content/Intent;
    const v4, 0x7f090116

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/BottomActionBar;

    .line 40
    .local v1, bottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;
    const-string v4, "plus_pages"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/AccountSettingsData;

    .line 42
    .local v3, settings:Lcom/google/android/apps/plus/content/AccountSettingsData;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->isInitialOobIntent$755b117a(Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_2e

    .line 43
    const v4, 0x1020019

    const v5, 0x7f080035

    invoke-virtual {v1, v4, v5, p0}, Lcom/google/android/apps/plus/views/BottomActionBar;->addButton(IILandroid/view/View$OnClickListener;)Lcom/google/android/apps/plus/views/ActionButton;

    .line 46
    :cond_2e
    const-string v4, "oob_intents"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/phone/OobIntents;

    if-nez v4, :cond_42

    const/4 v4, 0x1

    :goto_39
    if-nez v4, :cond_47

    .line 47
    const v4, 0x7f080033

    invoke-virtual {v1, v6, v4, p0}, Lcom/google/android/apps/plus/views/BottomActionBar;->addButton(IILandroid/view/View$OnClickListener;)Lcom/google/android/apps/plus/views/ActionButton;

    .line 51
    :goto_41
    return-void

    .line 46
    :cond_42
    invoke-virtual {v4, p0, v0, v3}, Lcom/google/android/apps/plus/phone/OobIntents;->isLastIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)Z

    move-result v4

    goto :goto_39

    .line 49
    :cond_47
    const v4, 0x7f080034

    invoke-virtual {v1, v6, v4, p0}, Lcom/google/android/apps/plus/views/BottomActionBar;->addButton(IILandroid/view/View$OnClickListener;)Lcom/google/android/apps/plus/views/ActionButton;

    goto :goto_41
.end method
