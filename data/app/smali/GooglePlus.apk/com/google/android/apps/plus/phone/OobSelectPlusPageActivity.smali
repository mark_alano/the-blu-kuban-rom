.class public Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;
.super Lcom/google/android/apps/plus/phone/OobDeviceActivity;
.source "OobSelectPlusPageActivity.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mContinueButton:Lcom/google/android/apps/plus/views/ActionButton;

.field private mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :goto_6
    return-object v0

    :cond_7
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    goto :goto_6
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 103
    packed-switch p1, :pswitch_data_14

    .line 115
    :cond_3
    :goto_3
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 116
    return-void

    .line 105
    :pswitch_7
    if-nez p2, :cond_3

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 109
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    .line 110
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_3

    .line 103
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 3
    .parameter "fragment"

    .prologue
    .line 67
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    if-eqz v0, :cond_8

    .line 68
    check-cast p1, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    .line 70
    :cond_8
    return-void
.end method

.method public final onContinue()V
    .registers 2

    .prologue
    .line 91
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 93
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    .line 94
    return-void
.end method

.method public final onContinuePressed()V
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    if-eqz v0, :cond_9

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->activateAccount()V

    .line 86
    :cond_9
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f03006e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->setContentView(I)V

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->showTitlebar(Z)V

    .line 39
    const v0, 0x7f080029

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 42
    if-eqz p1, :cond_23

    .line 43
    const-string v0, "active_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 45
    :cond_23
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 59
    const v0, 0x102001a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ActionButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mContinueButton:Lcom/google/android/apps/plus/views/ActionButton;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    if-eqz v0, :cond_1b

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isAccountSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->setContinueButtonEnabled(Z)V

    .line 63
    :cond_1b
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_e

    .line 52
    const-string v0, "active_account"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 54
    :cond_e
    return-void
.end method

.method public final setContinueButtonEnabled(Z)V
    .registers 3
    .parameter "enabled"

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mContinueButton:Lcom/google/android/apps/plus/views/ActionButton;

    if-eqz v0, :cond_9

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mContinueButton:Lcom/google/android/apps/plus/views/ActionButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ActionButton;->setEnabled(Z)V

    .line 79
    :cond_9
    return-void
.end method
