.class public Lcom/google/android/apps/plus/phone/OutOfBoxActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "OutOfBoxActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 11
    .parameter "savedInstanceState"

    .prologue
    const/4 v8, 0x0

    .line 29
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v6, 0x7f030072

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->setContentView(I)V

    .line 35
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->showTitlebar(Z)V

    .line 38
    const v6, 0x7f080029

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 41
    const/4 v6, 0x1

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->setHasVisitedOob(Landroid/content/Context;Z)V

    .line 43
    if-nez p1, :cond_5a

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 45
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "network_oob"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    .line 47
    .local v2, responseWrapper:Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;
    if-eqz v2, :cond_5b

    invoke-virtual {v2}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v1

    .line 49
    .local v1, oobResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    :goto_33
    if-eqz v0, :cond_5d

    if-eqz v1, :cond_5d

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "oob_origin"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 51
    .local v5, upgradeOrigin:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 52
    .local v3, t:Landroid/support/v4/app/FragmentTransaction;
    invoke-static {}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->createInitialTag()Ljava/lang/String;

    move-result-object v4

    .line 53
    .local v4, tag:Ljava/lang/String;
    const v6, 0x7f090152

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->newInstance(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    move-result-object v7

    invoke-virtual {v3, v6, v7, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 55
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 61
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #oobResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .end local v2           #responseWrapper:Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;
    .end local v3           #t:Landroid/support/v4/app/FragmentTransaction;
    .end local v4           #tag:Ljava/lang/String;
    .end local v5           #upgradeOrigin:Ljava/lang/String;
    :cond_5a
    :goto_5a
    return-void

    .line 47
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    .restart local v2       #responseWrapper:Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;
    :cond_5b
    const/4 v1, 0x0

    goto :goto_33

    .line 57
    .restart local v1       #oobResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    :cond_5d
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->setResult(I)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->finish()V

    goto :goto_5a
.end method
