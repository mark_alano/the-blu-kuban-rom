.class public final Lcom/google/android/apps/plus/phone/ParticipantHelper;
.super Ljava/lang/Object;
.source "ParticipantHelper.java"


# direct methods
.method public static inviteMoreParticipants(Landroid/app/Activity;Ljava/util/Collection;ZLcom/google/android/apps/plus/content/EsAccount;Z)V
    .registers 24
    .parameter "activity"
    .parameter
    .parameter "isGroup"
    .parameter "account"
    .parameter "fromHangoutTile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;Z",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, participantList:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v18, users:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, i$:Ljava/util/Iterator;
    :goto_9
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_57

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 33
    .local v16, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual/range {v16 .. v16}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v17

    .line 34
    .local v17, participantId:Ljava/lang/String;
    const/4 v12, 0x0

    .line 35
    .local v12, gaiaId:Ljava/lang/String;
    const/4 v11, 0x0

    .line 36
    .local v11, email:Ljava/lang/String;
    const-string v2, "g:"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 37
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 43
    :cond_29
    :goto_29
    invoke-virtual/range {v16 .. v16}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v15

    .line 44
    .local v15, name:Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v2, v12, v15, v11}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 38
    .end local v15           #name:Ljava/lang/String;
    :cond_38
    const-string v2, "e:"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 39
    const/4 v2, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_29

    .line 40
    :cond_4a
    const-string v2, "p:"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 41
    move-object/from16 v11, v17

    goto :goto_29

    .line 46
    .end local v11           #email:Ljava/lang/String;
    .end local v12           #gaiaId:Ljava/lang/String;
    .end local v16           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v17           #participantId:Ljava/lang/String;
    :cond_57
    new-instance v5, Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-direct {v5, v0, v2}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 47
    .local v5, audience:Lcom/google/android/apps/plus/content/AudienceData;
    if-eqz p2, :cond_7e

    .line 48
    const v2, 0x7f080283

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x6

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v14

    .line 53
    .local v14, intent:Landroid/content/Intent;
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 63
    :goto_7d
    return-void

    .line 55
    .end local v14           #intent:Landroid/content/Intent;
    :cond_7e
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/phone/Intents;->getNewConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v14

    .line 57
    .restart local v14       #intent:Landroid/content/Intent;
    if-eqz p4, :cond_93

    .line 58
    const-string v2, "tile"

    const-class v3, Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    :cond_93
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 61
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto :goto_7d
.end method
