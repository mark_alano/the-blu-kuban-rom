.class public final Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;
.super Landroid/support/v4/widget/CursorAdapter;
.source "PeopleCursorAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private mAlwaysHideLetterSections:Z

.field private mAvatarUrlColumnIndex:I

.field protected mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private final mGaiaIdColumnIndex:I

.field private mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

.field private final mNameColumnIndex:I

.field private mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

.field private final mPackedCircleIdsColumnIndex:I

.field private final mPersonIdColumnIndex:I

.field private mShowAddButtonIfNeeded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIIIILcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .registers 10
    .parameter "context"
    .parameter "personIdColumnIndex"
    .parameter "gaiaIdColumnIndex"
    .parameter "nameColumnIndex"
    .parameter "packedCircleIdsColumnIndex"
    .parameter "avatarUrlColumnIndex"
    .parameter "circleNameResolver"

    .prologue
    const/4 v1, 0x0

    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPersonIdColumnIndex:I

    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mGaiaIdColumnIndex:I

    .line 45
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    .line 46
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPackedCircleIdsColumnIndex:I

    .line 47
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAvatarUrlColumnIndex:I

    .line 48
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAlwaysHideLetterSections:Z

    .line 49
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mShowAddButtonIfNeeded:Z

    .line 50
    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 51
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 15
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    const/4 v9, 0x0

    .line 90
    move-object v3, p1

    check-cast v3, Lcom/google/android/apps/plus/views/PeopleListItemView;

    .line 91
    .local v3, item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    .line 92
    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPersonIdColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPersonId(Ljava/lang/String;)V

    .line 93
    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mGaiaIdColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, gaiaId:Ljava/lang/String;
    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAvatarUrlColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, avatarUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v2, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 97
    .local v4, name:Ljava/lang/String;
    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactName(Ljava/lang/String;)V

    .line 98
    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPackedCircleIdsColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 99
    .local v5, packedCircleIds:Ljava/lang/String;
    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIds(Ljava/lang/String;)V

    .line 100
    iget-boolean v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mShowAddButtonIfNeeded:Z

    if-eqz v8, :cond_4b

    .line 101
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setAddButtonVisible(Z)V

    .line 102
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    if-eqz v8, :cond_4b

    .line 103
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    .line 107
    :cond_4b
    iget-boolean v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAlwaysHideLetterSections:Z

    if-nez v8, :cond_70

    if-eqz p3, :cond_70

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v8

    const/16 v10, 0x14

    if-le v8, v10, :cond_70

    const/4 v8, 0x1

    :goto_5a
    if-eqz v8, :cond_86

    .line 108
    invoke-static {v4}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v1

    .line 109
    .local v1, firstLetter:C
    invoke-interface {p3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v8

    if-nez v8, :cond_72

    .line 110
    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeader(C)V

    .line 120
    :goto_69
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 124
    .end local v1           #firstLetter:C
    :goto_6c
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    .line 125
    return-void

    :cond_70
    move v8, v9

    .line 107
    goto :goto_5a

    .line 112
    .restart local v1       #firstLetter:C
    :cond_72
    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 113
    .local v7, previousName:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v6

    .line 114
    .local v6, previousFirstLetter:C
    if-eq v6, v1, :cond_82

    .line 115
    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeader(C)V

    goto :goto_69

    .line 117
    :cond_82
    invoke-virtual {v3, v9}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderVisible(Z)V

    goto :goto_69

    .line 122
    .end local v1           #firstLetter:C
    .end local v6           #previousFirstLetter:C
    .end local v7           #previousName:Ljava/lang/String;
    :cond_86
    invoke-virtual {v3, v9}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderVisible(Z)V

    goto :goto_6c
.end method

.method public final convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "cursor"

    .prologue
    .line 167
    iget v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPositionForSection(I)I
    .registers 3
    .parameter "section"

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public final getSectionForPosition(I)I
    .registers 3
    .parameter "position"

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    .line 82
    invoke-static {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;

    move-result-object v0

    return-object v0
.end method

.method public final setAlwaysHideLetterSections(Z)V
    .registers 3
    .parameter "show"

    .prologue
    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAlwaysHideLetterSections:Z

    .line 64
    return-void
.end method

.method public final setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V
    .registers 2
    .parameter "onActionButtonClickListener"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    .line 60
    return-void
.end method

.method public final setShowAddButtonIfNeeded(Z)V
    .registers 3
    .parameter "show"

    .prologue
    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mShowAddButtonIfNeeded:Z

    .line 55
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 4
    .parameter "cursor"

    .prologue
    .line 71
    if-eqz p1, :cond_b

    .line 72
    new-instance v0, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    iget v1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;-><init>(Landroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    .line 74
    :cond_b
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
