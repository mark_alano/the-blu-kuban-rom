.class final Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;
.super Ljava/lang/Object;
.source "PhotoOneUpActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

.field final synthetic val$data:Landroid/database/Cursor;

.field final synthetic val$loader:Landroid/support/v4/content/Loader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Landroid/support/v4/content/Loader;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 575
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->val$data:Landroid/database/Cursor;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->val$loader:Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 580
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$200(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Z

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->val$data:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 581
    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$302(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z

    .line 605
    :goto_17
    return-void

    .line 584
    :cond_18
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$402(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z

    .line 587
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$500(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    if-eqz v1, :cond_7b

    .line 589
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->val$data:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$500(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I

    move-result v0

    .line 593
    .local v0, itemIndex:I
    :goto_33
    if-gez v0, :cond_4b

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$800(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    if-eqz v1, :cond_4b

    .line 594
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->val$data:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$800(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I

    move-result v0

    .line 596
    :cond_4b
    if-gez v0, :cond_4e

    .line 598
    const/4 v0, 0x0

    .line 601
    :cond_4e
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->val$loader:Landroid/support/v4/content/Loader;

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->setPageable(Lcom/google/android/apps/plus/phone/Pageable;)V

    .line 602
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->val$data:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 603
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1000(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Landroid/view/View;

    move-result-object v2

    #calls: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/view/View;)V

    .line 604
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1200(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setCurrentItem(IZ)V

    goto :goto_17

    .line 591
    .end local v0           #itemIndex:I
    :cond_7b
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$700(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)I

    move-result v0

    .restart local v0       #itemIndex:I
    goto :goto_33
.end method
