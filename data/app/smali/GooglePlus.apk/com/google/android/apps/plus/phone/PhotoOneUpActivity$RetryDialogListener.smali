.class final Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;
.super Ljava/lang/Object;
.source "PhotoOneUpActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RetryDialogListener"
.end annotation


# instance fields
.field final mTag:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "tag"

    .prologue
    .line 959
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 960
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->mTag:Ljava/lang/String;

    .line 961
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 965
    packed-switch p2, :pswitch_data_1e

    .line 979
    :cond_3
    :goto_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 980
    return-void

    .line 967
    :pswitch_7
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 968
    .local v0, frag:Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_3

    .line 969
    check-cast v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    .end local v0           #frag:Landroid/support/v4/app/Fragment;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doDownload(Landroid/content/Context;Z)V

    goto :goto_3

    .line 965
    :pswitch_data_1e
    .packed-switch -0x1
        :pswitch_7
    .end packed-switch
.end method
