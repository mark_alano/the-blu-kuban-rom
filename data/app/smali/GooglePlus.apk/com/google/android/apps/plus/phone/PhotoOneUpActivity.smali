.class public Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "PhotoOneUpActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;
.implements Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;
.implements Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;,
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$AlbumDetailsQuery;,
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;,
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/analytics/InstrumentedActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/support/v4/view/ViewPager$OnPageChangeListener;",
        "Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;",
        "Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;",
        "Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;"
    }
.end annotation


# static fields
.field private static final EVENT_NAME_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

.field private mAlbumCount:I

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mCurrentIndex:I

.field private mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mEventId:Ljava/lang/String;

.field private mFailedListener:Landroid/content/DialogInterface$OnClickListener;

.field private mFragmentIsLoading:Z

.field private mFullScreen:Z

.field private mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

.field private mIsEmpty:Z

.field private mIsPaused:Z

.field private mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private mMenuItemListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOwnerGaiaId:Ljava/lang/String;

.field private mPageHint:I

.field private mPhotoOfUserGaiaId:Ljava/lang/String;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPhotoUrl:Ljava/lang/String;

.field private mRestartLoader:Z

.field private mRootView:Landroid/view/View;

.field private mScreenListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamId:Ljava/lang/String;

.field private mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 127
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->EVENT_NAME_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    .line 172
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPageHint:I

    .line 176
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    .line 188
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    .line 190
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    .line 208
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFailedListener:Landroid/content/DialogInterface$OnClickListener;

    .line 955
    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .registers 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->EVENT_NAME_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/views/PhotoViewPager;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRestartLoader:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I
    .registers 9
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    const/4 v1, -0x1

    .line 55
    invoke-virtual {p2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2b

    const/4 v0, 0x0

    :goto_c
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_30

    :cond_15
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4a

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    :goto_2a
    return v0

    :cond_2b
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    :cond_30
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4a

    :cond_36
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_36

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    goto :goto_2a

    :cond_4a
    move v0, v1

    goto :goto_2a
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    return-object v0
.end method

.method private updateTitleAndSubtitle()V
    .registers 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 870
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v5

    add-int/lit8 v1, v5, 0x1

    .line 872
    .local v1, position:I
    iget v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    if-ltz v5, :cond_30

    move v0, v3

    .line 874
    .local v0, hasAlbumCount:Z
    :goto_f
    iget-boolean v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    if-nez v5, :cond_17

    if-eqz v0, :cond_17

    if-gtz v1, :cond_3e

    .line 875
    :cond_17
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    if-eqz v3, :cond_32

    .line 876
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    .line 884
    .local v2, title:Ljava/lang/String;
    :goto_1d
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    instance-of v3, v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    if-eqz v3, :cond_2a

    .line 885
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    check-cast v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->setTitle(Ljava/lang/String;)V

    .line 887
    :cond_2a
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/HostActionBar;->invalidateActionBar()V

    .line 888
    return-void

    .end local v0           #hasAlbumCount:Z
    .end local v2           #title:Ljava/lang/String;
    :cond_30
    move v0, v4

    .line 872
    goto :goto_f

    .line 878
    .restart local v0       #hasAlbumCount:Z
    :cond_32
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08007f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2       #title:Ljava/lang/String;
    goto :goto_1d

    .line 881
    .end local v2           #title:Ljava/lang/String;
    :cond_3e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08007e

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    iget v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2       #title:Ljava/lang/String;
    goto :goto_1d
.end method

.method private updateView(Landroid/view/View;)V
    .registers 9
    .parameter "view"

    .prologue
    const v6, 0x7f09016b

    const v5, 0x7f09016a

    const v4, 0x7f090169

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 786
    if-nez p1, :cond_f

    .line 799
    :goto_e
    return-void

    .line 790
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    if-nez v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_35

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    if-nez v0, :cond_35

    .line 791
    :cond_1f
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e

    .line 793
    :cond_35
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    if-nez v0, :cond_41

    .line 794
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e

    .line 796
    :cond_41
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e
.end method


# virtual methods
.method public final addMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 473
    return-void
.end method

.method public final addScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 463
    return-void
.end method

.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 771
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 766
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final isFragmentActive(Landroid/support/v4/app/Fragment;)Z
    .registers 5
    .parameter "fragment"

    .prologue
    const/4 v0, 0x0

    .line 687
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    if-nez v1, :cond_a

    .line 690
    :cond_9
    :goto_9
    return v0

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v2

    if-ne v1, v2, :cond_9

    const/4 v0, 0x1

    goto :goto_9
.end method

.method public final onActionBarInvalidated()V
    .registers 3

    .prologue
    .line 738
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/HostedFragment;->attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    .line 739
    :cond_19
    return-void
.end method

.method public final onActionButtonClicked(I)V
    .registers 3
    .parameter "actionId"

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_9

    .line 755
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onActionButtonClicked(I)V

    .line 757
    :cond_9
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 356
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    if-eqz v0, :cond_8

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->toggleFullScreen()V

    .line 361
    :goto_7
    return-void

    .line 359
    :cond_8
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onBackPressed()V

    goto :goto_7
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 20
    .parameter "savedInstanceState"

    .prologue
    .line 219
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 221
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    .line 222
    .local v14, mIntent:Landroid/content/Intent;
    const-string v2, "account"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 224
    const/16 v17, 0x0

    .line 225
    .local v17, refreshAlbumId:Ljava/lang/String;
    const/4 v12, -0x1

    .line 226
    .local v12, currentItem:I
    if-eqz p1, :cond_9c

    .line 227
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.ITEM"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v12

    .line 228
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.FULLSCREEN"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    .line 229
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.CURRENT_REF"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 246
    :cond_3c
    :goto_3c
    const-string v2, "event_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 247
    const-string v2, "event_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    .line 250
    :cond_4e
    const-string v2, "album_name"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_bc

    .line 251
    const-string v2, "album_name"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    .line 256
    :cond_60
    :goto_60
    const-string v2, "owner_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_72

    .line 257
    const-string v2, "owner_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    .line 260
    :cond_72
    const-string v2, "mediarefs"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d2

    .line 261
    const-string v2, "mediarefs"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v16

    .line 262
    .local v16, parcelables:[Landroid/os/Parcelable;
    move-object/from16 v0, v16

    array-length v2, v0

    new-array v2, v2, [Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    .line 263
    const/4 v13, 0x0

    .local v13, i:I
    :goto_8a
    move-object/from16 v0, v16

    array-length v2, v0

    if-ge v13, v2, :cond_d2

    .line 264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v2, v16, v13

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    aput-object v2, v3, v13

    .line 263
    add-int/lit8 v13, v13, 0x1

    goto :goto_8a

    .line 231
    .end local v13           #i:I
    .end local v16           #parcelables:[Landroid/os/Parcelable;
    :cond_9c
    const-string v2, "notif_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 232
    .local v15, notificationId:Ljava/lang/String;
    if-eqz v15, :cond_ad

    .line 236
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v15}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    .line 240
    :cond_ad
    const-string v2, "refresh_album_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 241
    const-string v2, "refresh_album_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    goto :goto_3c

    .line 252
    .end local v15           #notificationId:Ljava/lang/String;
    :cond_bc
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    if-nez v2, :cond_60

    .line 253
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08007f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    goto :goto_60

    .line 268
    :cond_d2
    const-string v2, "album_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e4

    .line 269
    const-string v2, "album_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    .line 272
    :cond_e4
    const-string v2, "stream_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f6

    .line 273
    const-string v2, "stream_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mStreamId:Ljava/lang/String;

    .line 276
    :cond_f6
    const-string v2, "photos_of_user_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_108

    .line 277
    const-string v2, "photos_of_user_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoOfUserGaiaId:Ljava/lang/String;

    .line 280
    :cond_108
    const-string v2, "photo_url"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11a

    .line 281
    const-string v2, "photo_url"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoUrl:Ljava/lang/String;

    .line 284
    :cond_11a
    const-string v2, "photo_ref"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_130

    if-gez v12, :cond_130

    .line 285
    const-string v2, "photo_ref"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 288
    :cond_130
    const-string v2, "page_hint"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_145

    if-gez v12, :cond_145

    .line 289
    const-string v2, "page_hint"

    const/4 v3, -0x1

    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPageHint:I

    .line 293
    :cond_145
    const-string v2, "photo_index"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_156

    if-gez v12, :cond_156

    .line 294
    const-string v2, "photo_index"

    const/4 v3, -0x1

    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 296
    :cond_156
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I

    .line 299
    if-eqz v17, :cond_171

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_171

    .line 300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v2, v1, v3}, Lcom/google/android/apps/plus/service/EsService;->getAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_171
    const v2, 0x7f030086

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->setContentView(I)V

    .line 305
    const v2, 0x7f0900e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/HostActionBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    const v3, 0x7f0803e3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->setUpButtonContentDescription(Ljava/lang/String;)V

    .line 311
    const v2, 0x7f090167

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    .line 313
    const-string v2, "force_load_id"

    invoke-virtual {v14, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23e

    const-string v2, "force_load_id"

    const-wide/16 v3, 0x0

    invoke-virtual {v14, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 316
    .local v7, forceLoadId:Ljava/lang/Long;
    :goto_1cb
    const-string v2, "allow_plusone"

    const/4 v3, 0x1

    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 317
    .local v11, allowPlusOne:Z
    new-instance v2, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mStreamId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->setFragmentPagerListener(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;)V

    .line 321
    const v2, 0x7f090168

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    .line 322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setOnInterceptTouchListener(Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;)V

    .line 327
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const v3, 0x7f090029

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    .line 330
    return-void

    .line 313
    .end local v7           #forceLoadId:Ljava/lang/Long;
    .end local v11           #allowPlusOne:Z
    :cond_23e
    const/4 v7, 0x0

    goto :goto_1cb
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 10
    .parameter "id"
    .parameter "args"

    .prologue
    const/4 v5, 0x0

    .line 382
    const-string v4, "tag"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 383
    .local v3, tag:Ljava/lang/String;
    packed-switch p1, :pswitch_data_5e

    .line 410
    const/4 v1, 0x0

    :goto_b
    return-object v1

    .line 385
    :pswitch_c
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 386
    .local v1, progressDialog:Landroid/app/ProgressDialog;
    const-string v4, "dialog_message"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 387
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 388
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_b

    .line 393
    .end local v1           #progressDialog:Landroid/app/ProgressDialog;
    :pswitch_21
    new-instance v2, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Ljava/lang/String;)V

    .line 394
    .local v2, retryListener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 395
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v4, 0x7f080085

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0801c6

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0801c8

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 398
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_b

    .line 402
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    .end local v2           #retryListener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;
    :pswitch_44
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 403
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v4, 0x7f080086

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0801c4

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFailedListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 405
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_b

    .line 383
    nop

    :pswitch_data_5e
    .packed-switch 0x7f09002e
        :pswitch_c
        :pswitch_44
        :pswitch_21
    .end packed-switch
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 15
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 527
    sparse-switch p1, :sswitch_data_80

    move-object v0, v4

    .line 562
    :goto_7
    return-object v0

    .line 529
    :sswitch_8
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    .line 531
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v4, :cond_40

    array-length v0, v4

    if-ne v0, v1, :cond_40

    aget-object v0, v4, v3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_25

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_25

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_25
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3e

    const-string v4, "content:"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    move v0, v1

    :goto_34
    if-eqz v0, :cond_42

    .line 532
    new-instance v0, Lcom/google/android/apps/plus/phone/CameraPhotoLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/CameraPhotoLoader;-><init>(Landroid/content/Context;)V

    goto :goto_7

    :cond_3e
    move v0, v3

    .line 531
    goto :goto_34

    :cond_40
    move v0, v3

    goto :goto_34

    .line 534
    :cond_42
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoOfUserGaiaId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mStreamId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoUrl:Ljava/lang/String;

    iget v10, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPageHint:I

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_7

    .line 541
    :sswitch_5b
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 544
    .local v11, albumUri:Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v11, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 545
    .local v2, loaderUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$AlbumDetailsQuery;->PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 550
    .end local v2           #loaderUri:Landroid/net/Uri;
    .end local v11           #albumUri:Landroid/net/Uri;
    :sswitch_7a
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/content/Context;)V

    goto :goto_7

    .line 527
    :sswitch_data_80
    .sparse-switch
        0x511d1df3 -> :sswitch_7a
        0x7f090029 -> :sswitch_8
        0x7f09002a -> :sswitch_5b
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100017

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 425
    const/4 v0, 0x1

    return v0
.end method

.method public final onFragmentVisible(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    if-nez v0, :cond_9

    .line 702
    :cond_8
    :goto_8
    return-void

    .line 698
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v1

    if-ne v0, v1, :cond_1a

    .line 699
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    .line 701
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    goto :goto_8
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 10
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_a8

    :cond_d
    :goto_d
    return-void

    :sswitch_e
    if-eqz p2, :cond_16

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_43

    :cond_16
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    :goto_1f
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_51

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_51

    move v0, v1

    :goto_28
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    if-ne v4, v5, :cond_53

    :goto_32
    if-eqz v3, :cond_55

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    if-eqz v2, :cond_55

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x511d1df3

    invoke-virtual {v0, v1, v6, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_d

    :cond_43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Landroid/support/v4/content/Loader;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1f

    :cond_51
    move v0, v2

    goto :goto_28

    :cond_53
    move v1, v2

    goto :goto_32

    :cond_55
    if-eqz v0, :cond_66

    if-nez v3, :cond_5b

    if-eqz v1, :cond_66

    :cond_5b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f09002a

    invoke-virtual {v0, v1, v6, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_d

    :cond_66
    if-nez v3, :cond_d

    :cond_68
    :goto_68
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateTitleAndSubtitle()V

    goto :goto_d

    :sswitch_6c
    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    if-nez v2, :cond_7e

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    :cond_7e
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    if-ne v0, v5, :cond_8b

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_90

    const/4 v0, -0x2

    :goto_89
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    :cond_8b
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateTitleAndSubtitle()V

    goto/16 :goto_d

    :cond_90
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_89

    :sswitch_95
    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    if-nez v0, :cond_68

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    goto :goto_68

    :sswitch_data_a8
    .sparse-switch
        0x511d1df3 -> :sswitch_95
        0x7f090029 -> :sswitch_e
        0x7f09002a -> :sswitch_6c
    .end sparse-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 658
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    .line 450
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;

    .line 451
    .local v1, listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 453
    const/4 v2, 0x1

    .line 457
    .end local v1           #listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;
    :goto_19
    return v2

    :cond_1a
    const/4 v2, 0x0

    goto :goto_19
.end method

.method public final onPageActivated(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 676
    instance-of v0, p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_22

    .line 677
    check-cast p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    .line 678
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onViewActivated()V

    goto :goto_e

    .line 679
    :cond_1e
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateTitleAndSubtitle()V

    .line 683
    :goto_21
    return-void

    .line 681
    .restart local p1
    :cond_22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    goto :goto_21
.end method

.method public final onPageScrollStateChanged(I)V
    .registers 2
    .parameter "state"

    .prologue
    .line 672
    return-void
.end method

.method public final onPageScrolled$486775f1(IF)V
    .registers 3
    .parameter "position"
    .parameter "positionOffset"

    .prologue
    .line 662
    return-void
.end method

.method public final onPageSelected(I)V
    .registers 3
    .parameter "position"

    .prologue
    .line 666
    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I

    .line 667
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getMediaRef(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 668
    return-void
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    .line 350
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onPause()V

    .line 351
    return-void
.end method

.method public final onPhotoRemoved$1349ef()V
    .registers 7

    .prologue
    .line 505
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 506
    .local v0, data:Landroid/database/Cursor;
    if-nez v0, :cond_9

    .line 523
    :goto_8
    return-void

    .line 511
    :cond_9
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 512
    .local v1, dataCount:I
    const/4 v3, 0x1

    if-gt v1, v3, :cond_22

    .line 514
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    .line 516
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 517
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->startActivity(Landroid/content/Intent;)V

    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->finish()V

    goto :goto_8

    .line 522
    .end local v2           #intent:Landroid/content/Intent;
    :cond_22
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const v4, 0x7f090029

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_8
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 6
    .parameter "id"
    .parameter "dialog"
    .parameter "args"

    .prologue
    .line 365
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 366
    packed-switch p1, :pswitch_data_18

    .line 378
    :cond_6
    :goto_6
    return-void

    .line 370
    :pswitch_7
    instance-of v1, p2, Landroid/app/ProgressDialog;

    if-eqz v1, :cond_6

    move-object v0, p2

    .line 372
    check-cast v0, Landroid/app/ProgressDialog;

    .line 373
    .local v0, pd:Landroid/app/ProgressDialog;
    const-string v1, "dialog_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 366
    :pswitch_data_18
    .packed-switch 0x7f09002e
        :pswitch_7
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 8
    .parameter "menu"

    .prologue
    .line 431
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v4

    .line 432
    .local v4, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_5
    if-ge v0, v4, :cond_12

    .line 433
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 434
    .local v2, item:Landroid/view/MenuItem;
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 432
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 440
    .end local v2           #item:Landroid/view/MenuItem;
    :cond_12
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;

    .line 441
    .local v3, listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;
    invoke-interface {v3, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    goto :goto_18

    .line 444
    .end local v3           #listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;
    :cond_28
    const/4 v5, 0x1

    return v5
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .registers 2
    .parameter "selectedPosition"

    .prologue
    .line 743
    return-void
.end method

.method public final onRefreshButtonClicked()V
    .registers 2

    .prologue
    .line 747
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_9

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    .line 750
    :cond_9
    return-void
.end method

.method protected onResume()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 334
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onResume()V

    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_5a

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_58

    const-string v0, "PhotoOneUp"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_41

    const-string v0, "PhotoOneUp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity finished because it is associated with a signed-out account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_41
    move v0, v1

    :goto_42
    if-eqz v0, :cond_5c

    .line 336
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    .line 337
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRestartLoader:Z

    if-eqz v0, :cond_57

    .line 338
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRestartLoader:Z

    .line 339
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f090029

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 344
    :cond_57
    :goto_57
    return-void

    .line 335
    :cond_58
    const/4 v0, 0x1

    goto :goto_42

    :cond_5a
    move v0, v1

    goto :goto_42

    .line 342
    :cond_5c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->finish()V

    goto :goto_57
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 415
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 417
    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.ITEM"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 418
    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.FULLSCREEN"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 419
    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.CURRENT_REF"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 420
    return-void
.end method

.method public final onTouchIntercept(FF)Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;
    .registers 8
    .parameter "origX"
    .parameter "origY"

    .prologue
    .line 706
    const/4 v1, 0x0

    .line 707
    .local v1, interceptLeft:Z
    const/4 v2, 0x0

    .line 709
    .local v2, interceptRight:Z
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_24

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    .line 710
    .local v3, listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
    if-nez v1, :cond_1a

    .line 711
    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onInterceptMoveLeft$2548a39()Z

    move-result v1

    .line 713
    :cond_1a
    if-nez v2, :cond_20

    .line 714
    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onInterceptMoveRight$2548a39()Z

    move-result v2

    .line 716
    :cond_20
    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onViewActivated()V

    goto :goto_8

    .line 719
    .end local v3           #listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
    :cond_24
    if-eqz v1, :cond_2e

    .line 720
    if-eqz v2, :cond_2b

    .line 721
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->BOTH:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    .line 727
    :goto_2a
    return-object v4

    .line 723
    :cond_2b
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->LEFT:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_2a

    .line 724
    :cond_2e
    if-eqz v2, :cond_33

    .line 725
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->RIGHT:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_2a

    .line 727
    :cond_33
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->NONE:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_2a
.end method

.method public final onUpButtonClick()V
    .registers 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->onBackPressed()V

    .line 762
    return-void
.end method

.method public final removeMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 478
    return-void
.end method

.method public final removeScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 468
    return-void
.end method

.method public final toggleFullScreen()V
    .registers 4

    .prologue
    .line 495
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    if-nez v2, :cond_1f

    const/4 v2, 0x1

    :goto_5
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    .line 498
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    .line 499
    .local v1, listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onFullScreenChanged$25decb5(Z)V

    goto :goto_d

    .line 495
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
    :cond_1f
    const/4 v2, 0x0

    goto :goto_5

    .line 501
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_21
    return-void
.end method
