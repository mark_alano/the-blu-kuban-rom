.class final Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;
.super Ljava/lang/Object;
.source "PhotoOneUpAnimationController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V
    .registers 2
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 5
    .parameter "animation"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$000(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)I

    move-result v0

    packed-switch v0, :pswitch_data_36

    .line 68
    :goto_9
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    #calls: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V

    .line 69
    return-void

    .line 56
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x2

    #setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x0

    #setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$202(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;F)F

    goto :goto_9

    .line 62
    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x0

    #setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->getHideOffset(Z)I

    move-result v1

    int-to-float v1, v1

    #setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$202(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;F)F

    goto :goto_9

    .line 54
    nop

    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_f
        :pswitch_9
        :pswitch_1c
    .end packed-switch
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 2
    .parameter "animation"

    .prologue
    .line 73
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 4
    .parameter "animation"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$000(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)I

    move-result v0

    packed-switch v0, :pswitch_data_1e

    .line 49
    :goto_9
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    #calls: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V

    .line 50
    return-void

    .line 39
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    goto :goto_9

    .line 44
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x3

    #setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    goto :goto_9

    .line 37
    nop

    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_f
        :pswitch_9
        :pswitch_16
    .end packed-switch
.end method
