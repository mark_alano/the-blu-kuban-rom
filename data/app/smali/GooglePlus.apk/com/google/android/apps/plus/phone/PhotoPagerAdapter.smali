.class public final Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;
.source "PhotoPagerAdapter.java"


# instance fields
.field final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field final mAllowPlusOne:Z

.field private final mDefaultAlbumName:Ljava/lang/String;

.field final mEventId:Ljava/lang/String;

.field final mForceLoadId:Ljava/lang/Long;

.field private mPageable:Lcom/google/android/apps/plus/phone/Pageable;

.field final mStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11
    .parameter "context"
    .parameter "fm"
    .parameter "c"
    .parameter "account"
    .parameter "forceLoadId"
    .parameter "streamId"
    .parameter "eventId"
    .parameter "defaultAlbumName"
    .parameter "allowPlusOne"

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;)V

    .line 38
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 39
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mForceLoadId:Ljava/lang/Long;

    .line 40
    iput-object p6, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mStreamId:Ljava/lang/String;

    .line 41
    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mEventId:Ljava/lang/String;

    .line 42
    iput-object p8, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mDefaultAlbumName:Ljava/lang/String;

    .line 43
    iput-boolean p9, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAllowPlusOne:Z

    .line 44
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/phone/Pageable;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 49
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 51
    :goto_12
    return v0

    :cond_13
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->getCount()I

    move-result v0

    goto :goto_12
.end method

.method public final getItem(I)Landroid/support/v4/app/Fragment;
    .registers 4
    .parameter "position"

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->isDataValid()Z

    move-result v1

    if-eqz v1, :cond_23

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 84
    .local v0, cursor:Landroid/database/Cursor;
    :goto_a
    if-eqz v0, :cond_25

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_18

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_25

    .line 86
    :cond_18
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1}, Lcom/google/android/apps/plus/phone/Pageable;->loadMore()V

    .line 87
    new-instance v1, Lcom/google/android/apps/plus/fragments/LoadingFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/LoadingFragment;-><init>()V

    .line 89
    :goto_22
    return-object v1

    .line 83
    .end local v0           #cursor:Landroid/database/Cursor;
    :cond_23
    const/4 v0, 0x0

    goto :goto_a

    .line 89
    .restart local v0       #cursor:Landroid/database/Cursor;
    :cond_25
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_22
.end method

.method public final getItem$2282a066(Landroid/database/Cursor;)Landroid/support/v4/app/Fragment;
    .registers 13
    .parameter "cursor"

    .prologue
    .line 56
    const/4 v8, 0x1

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 57
    .local v4, photoId:J
    const/4 v8, 0x2

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 58
    .local v7, photoUrl:Ljava/lang/String;
    const/4 v8, 0x3

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, ownerId:Ljava/lang/String;
    const/4 v8, 0x4

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 62
    .local v6, photoTitle:Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    const-class v9, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    const/4 v10, 0x0

    invoke-direct {v0, v8, v9, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;B)V

    .line 64
    .local v0, builder:Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setDisplayName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mDefaultAlbumName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mStreamId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mEventId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAllowPlusOne:Z

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAllowPlusOne(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mForceLoadId:Ljava/lang/Long;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setForceLoadId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 75
    new-instance v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-direct {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;-><init>()V

    .line 76
    .local v3, photoFragment:Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 77
    .local v1, extras:Landroid/os/Bundle;
    invoke-virtual {v3, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 78
    return-object v3
.end method

.method public final getMediaRef(I)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 10
    .parameter "position"

    .prologue
    const/4 v5, 0x0

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->isDataValid()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v7

    .line 98
    .local v7, cursor:Landroid/database/Cursor;
    :goto_b
    if-eqz v7, :cond_19

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_19

    invoke-interface {v7, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_1d

    :cond_19
    move-object v0, v5

    .line 105
    :goto_1a
    return-object v0

    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_1b
    move-object v7, v5

    .line 97
    goto :goto_b

    .line 102
    .restart local v7       #cursor:Landroid/database/Cursor;
    :cond_1d
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 103
    .local v2, photoId:J
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, photoUrl:Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, ownerId:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    goto :goto_1a
.end method

.method public final setPageable(Lcom/google/android/apps/plus/phone/Pageable;)V
    .registers 2
    .parameter "pageable"

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    .line 113
    return-void
.end method
