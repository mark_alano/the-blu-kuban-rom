.class public Lcom/google/android/apps/plus/phone/PhotoPickerActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PhotoPickerActivity.java"


# instance fields
.field private mAllowCrop:Z

.field private mDisplayName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 10
    .parameter "savedInstanceState"

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0xb

    const/4 v5, 0x1

    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v3, 0x7f03008a

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->setContentView(I)V

    .line 35
    if-nez p1, :cond_2e

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 38
    .local v1, ft:Landroid/support/v4/app/FragmentTransaction;
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 39
    .local v2, intent:Landroid/content/Intent;
    const v3, 0x7f090186

    new-instance v4, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;

    invoke-direct {v4, v2}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 40
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 43
    .end local v1           #ft:Landroid/support/v4/app/FragmentTransaction;
    .end local v2           #intent:Landroid/content/Intent;
    :cond_2e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "display_name"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mDisplayName:Ljava/lang/String;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "allow_crop"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mAllowCrop:Z

    .line 48
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v6, :cond_6b

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 50
    .local v0, actionBar:Landroid/app/ActionBar;
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 56
    .end local v0           #actionBar:Landroid/app/ActionBar;
    :goto_51
    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mAllowCrop:Z

    if-eqz v3, :cond_74

    const v3, 0x7f08008b

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_5c
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v6, :cond_77

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 57
    :goto_6a
    return-void

    .line 52
    :cond_6b
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->showTitlebar(Z)V

    .line 53
    const/high16 v3, 0x7f10

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->createTitlebarButtons(I)V

    goto :goto_51

    .line 56
    :cond_74
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mDisplayName:Ljava/lang/String;

    goto :goto_5c

    :cond_77
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->setTitlebarTitle(Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    goto :goto_6a
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_e

    .line 90
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 85
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->finish()V

    .line 86
    const/4 v0, 0x1

    goto :goto_8

    .line 83
    :pswitch_data_e
    .packed-switch 0x102002c
        :pswitch_9
    .end packed-switch
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 5
    .parameter "menu"

    .prologue
    .line 76
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_12

    .line 77
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 79
    :cond_12
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->finish()V

    .line 66
    :cond_c
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->finish()V

    .line 71
    return-void
.end method
