.class final Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;
.super Ljava/lang/Object;
.source "PhotoViewActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

.field final synthetic val$data:Landroid/database/Cursor;

.field final synthetic val$loader:Landroid/support/v4/content/Loader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Landroid/database/Cursor;Landroid/support/v4/content/Loader;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 622
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->val$data:Landroid/database/Cursor;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->val$loader:Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 627
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsPaused:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$000(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 628
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$102(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Z)Z

    .line 652
    :goto_f
    return-void

    .line 631
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$202(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Z)Z

    .line 634
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$300(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    if-eqz v1, :cond_73

    .line 636
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->val$data:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$300(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$400(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I

    move-result v0

    .line 640
    .local v0, itemIndex:I
    :goto_2b
    if-gez v0, :cond_43

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$600(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    if-eqz v1, :cond_43

    .line 641
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->val$data:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$600(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$400(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I

    move-result v0

    .line 643
    :cond_43
    if-gez v0, :cond_46

    .line 645
    const/4 v0, 0x0

    .line 648
    :cond_46
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$700(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->val$loader:Landroid/support/v4/content/Loader;

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->setPageable(Lcom/google/android/apps/plus/phone/Pageable;)V

    .line 649
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$700(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->val$data:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 650
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$800(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Landroid/view/View;

    move-result-object v2

    #calls: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->updateView(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$900(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Landroid/view/View;)V

    .line 651
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$1000(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setCurrentItem(IZ)V

    goto :goto_f

    .line 638
    .end local v0           #itemIndex:I
    :cond_73
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;->this$0:Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    #getter for: Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentIndex:I
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->access$500(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)I

    move-result v0

    .restart local v0       #itemIndex:I
    goto :goto_2b
.end method
