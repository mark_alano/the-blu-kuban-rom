.class public Lcom/google/android/apps/plus/phone/PhotoViewActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PhotoViewActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;
.implements Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;
.implements Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;,
        Lcom/google/android/apps/plus/phone/PhotoViewActivity$RetryDialogListener;,
        Lcom/google/android/apps/plus/phone/PhotoViewActivity$AlbumDetailsQuery;,
        Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;,
        Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/support/v4/view/ViewPager$OnPageChangeListener;",
        "Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;",
        "Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;",
        "Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionBarHeight:I

.field private mActionBarLayoutListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;

.field private mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

.field private mAlbumCount:I

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mCurrentIndex:I

.field private mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mEventId:Ljava/lang/String;

.field private mFailedListener:Landroid/content/DialogInterface$OnClickListener;

.field private mFragmentIsLoading:Z

.field private mFullScreen:Z

.field private mIsEmpty:Z

.field private mIsPaused:Z

.field private mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private mMenuItemListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOwnerGaiaId:Ljava/lang/String;

.field private mPageHint:I

.field private mPhotoOfUserGaiaId:Ljava/lang/String;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPhotoUrl:Ljava/lang/String;

.field private mRestartLoader:Z

.field private mRootView:Landroid/view/View;

.field private mScreenListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;",
            ">;"
        }
    .end annotation
.end field

.field private mShowPhotoOnly:Z

.field private mStreamId:Ljava/lang/String;

.field private mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

.field private mViewScrolling:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 180
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPageHint:I

    .line 184
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumCount:I

    .line 200
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    .line 202
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMenuItemListeners:Ljava/util/Set;

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsPaused:Z

    .line 221
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFailedListener:Landroid/content/DialogInterface$OnClickListener;

    .line 1001
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsPaused:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/views/PhotoViewPager;
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRestartLoader:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/plus/phone/PhotoViewActivity;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 53
    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarHeight:I

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Ljava/util/Set;
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->clearListener()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsEmpty:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I
    .registers 9
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    const/4 v1, -0x1

    .line 53
    invoke-virtual {p2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2b

    const/4 v0, 0x0

    :goto_c
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_30

    :cond_15
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4a

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    :goto_2a
    return v0

    :cond_2b
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    :cond_30
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4a

    :cond_36
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_36

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    goto :goto_2a

    :cond_4a
    move v0, v1

    goto :goto_2a
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->updateView(Landroid/view/View;)V

    return-void
.end method

.method private clearListener()V
    .registers 3

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_f

    .line 946
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarLayoutListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 948
    :cond_f
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarLayoutListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;

    .line 949
    return-void
.end method

.method private setFullScreen(ZZ)V
    .registers 11
    .parameter "fullScreen"
    .parameter "animate"

    .prologue
    const/16 v7, 0xb

    const/4 v5, 0x1

    .line 777
    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    if-eq p1, v6, :cond_2f

    move v1, v5

    .line 778
    .local v1, fullScreenChanged:Z
    :goto_8
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    .line 780
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v6, v7, :cond_35

    .line 781
    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    if-eqz v6, :cond_31

    .line 782
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->hideTitlebar(Z)V

    .line 806
    :goto_15
    if-eqz v1, :cond_5f

    .line 807
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;

    .line 808
    .local v3, listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    iget-boolean v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;->onFullScreenChanged$25decb5()V

    goto :goto_1d

    .line 777
    .end local v1           #fullScreenChanged:Z
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    :cond_2f
    const/4 v1, 0x0

    goto :goto_8

    .line 784
    .restart local v1       #fullScreenChanged:Z
    :cond_31
    invoke-virtual {p0, p2, v5}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->showTitlebar(ZZ)V

    goto :goto_15

    .line 787
    :cond_35
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 788
    .local v0, actionBar:Landroid/app/ActionBar;
    iget-boolean v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    if-eqz v5, :cond_41

    .line 789
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_15

    .line 795
    :cond_41
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v7, :cond_5b

    iget v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarHeight:I

    if-nez v5, :cond_5b

    .line 796
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    .line 797
    .local v4, observer:Landroid/view/ViewTreeObserver;
    new-instance v5, Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;-><init>(Lcom/google/android/apps/plus/phone/PhotoViewActivity;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarLayoutListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;

    .line 798
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarLayoutListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 802
    .end local v4           #observer:Landroid/view/ViewTreeObserver;
    :cond_5b
    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_15

    .line 811
    .end local v0           #actionBar:Landroid/app/ActionBar;
    :cond_5f
    return-void
.end method

.method private setViewActivated()V
    .registers 4

    .prologue
    .line 817
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;

    .line 818
    .local v1, listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    invoke-interface {v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;->onViewActivated()V

    goto :goto_6

    .line 820
    .end local v1           #listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    :cond_16
    return-void
.end method

.method private updateTitleAndSubtitle()V
    .registers 11

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 911
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v6

    add-int/lit8 v2, v6, 0x1

    .line 913
    .local v2, position:I
    iget v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumCount:I

    if-ltz v6, :cond_2b

    move v1, v4

    .line 915
    .local v1, hasAlbumCount:Z
    :goto_f
    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsEmpty:Z

    if-nez v6, :cond_17

    if-eqz v1, :cond_17

    if-gtz v2, :cond_2d

    .line 916
    :cond_17
    const/4 v3, 0x0

    .line 921
    .local v3, subtitle:Ljava/lang/String;
    :goto_18
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_4a

    .line 922
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 924
    .local v0, actionBar:Landroid/app/ActionBar;
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 925
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 931
    .end local v0           #actionBar:Landroid/app/ActionBar;
    :goto_2a
    return-void

    .end local v1           #hasAlbumCount:Z
    .end local v3           #subtitle:Ljava/lang/String;
    :cond_2b
    move v1, v5

    .line 913
    goto :goto_f

    .line 918
    .restart local v1       #hasAlbumCount:Z
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08007e

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    iget v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v4

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3       #subtitle:Ljava/lang/String;
    goto :goto_18

    .line 927
    :cond_4a
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 928
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    .line 929
    const v4, 0x7f100017

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->createTitlebarButtons(I)V

    goto :goto_2a
.end method

.method private updateView(Landroid/view/View;)V
    .registers 9
    .parameter "view"

    .prologue
    const v6, 0x7f09016b

    const v5, 0x7f09016a

    const v4, 0x7f090169

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 827
    if-nez p1, :cond_f

    .line 840
    :goto_e
    return-void

    .line 831
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFragmentIsLoading:Z

    if-nez v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_35

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsEmpty:Z

    if-nez v0, :cond_35

    .line 832
    :cond_1f
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e

    .line 834
    :cond_35
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsEmpty:Z

    if-nez v0, :cond_41

    .line 835
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e

    .line 837
    :cond_41
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e
.end method


# virtual methods
.method public final addMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 538
    return-void
.end method

.method public final addScreenListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 528
    return-void
.end method

.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getActionBarHeight()I
    .registers 2

    .prologue
    .line 938
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarHeight:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 964
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final isFragmentActive(Landroid/support/v4/app/Fragment;)Z
    .registers 5
    .parameter "fragment"

    .prologue
    const/4 v0, 0x0

    .line 719
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    if-nez v1, :cond_a

    .line 722
    :cond_9
    :goto_9
    return v0

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v2

    if-ne v1, v2, :cond_9

    const/4 v0, 0x1

    goto :goto_9
.end method

.method public final isFragmentFullScreen(Landroid/support/v4/app/Fragment;)Z
    .registers 4
    .parameter "fragment"

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_13

    .line 548
    :cond_10
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    .line 550
    :goto_12
    return v0

    :cond_13
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    if-nez v0, :cond_25

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v1

    if-eq v0, v1, :cond_27

    :cond_25
    const/4 v0, 0x1

    goto :goto_12

    :cond_27
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final isShowPhotoOnly()Z
    .registers 2

    .prologue
    .line 555
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mShowPhotoOnly:Z

    return v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 6
    .parameter "fragment"

    .prologue
    .line 388
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 389
    const/4 v0, 0x0

    .line 390
    .local v0, photoFragment:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;
    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    if-eqz v2, :cond_b

    move-object v0, p1

    .line 391
    check-cast v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    .line 396
    :cond_b
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_22

    .line 397
    const v2, 0x7f09023d

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 402
    .local v1, progressView:Landroid/widget/ProgressBar;
    :goto_1a
    if-eqz v0, :cond_21

    if-eqz v1, :cond_21

    .line 403
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->onUpdateProgressView(Landroid/widget/ProgressBar;)V

    .line 405
    :cond_21
    return-void

    .line 399
    .end local v1           #progressView:Landroid/widget/ProgressBar;
    :cond_22
    const v2, 0x7f09004a

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .restart local v1       #progressView:Landroid/widget/ProgressBar;
    goto :goto_1a
.end method

.method public onAttachedToWindow()V
    .registers 6

    .prologue
    .line 409
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachedToWindow()V

    .line 411
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_2a

    .line 414
    const v3, 0x7f09023d

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 417
    .local v2, progressView:Landroid/widget/ProgressBar;
    if-eqz v2, :cond_2a

    .line 418
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;

    .line 419
    .local v1, listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;->onUpdateProgressView(Landroid/widget/ProgressBar;)V

    goto :goto_1a

    .line 423
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    .end local v2           #progressView:Landroid/widget/ProgressBar;
    :cond_2a
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mShowPhotoOnly:Z

    if-nez v0, :cond_c

    .line 380
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->toggleFullScreen()V

    .line 384
    :goto_b
    return-void

    .line 382
    :cond_c
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    goto :goto_b
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 21
    .parameter "savedInstanceState"

    .prologue
    .line 232
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 234
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v15

    .line 235
    .local v15, mIntent:Landroid/content/Intent;
    const-string v2, "account"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 236
    const-string v2, "show_photo_only"

    const/4 v3, 0x0

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mShowPhotoOnly:Z

    .line 238
    const/16 v18, 0x0

    .line 239
    .local v18, refreshAlbumId:Ljava/lang/String;
    const/4 v13, -0x1

    .line 240
    .local v13, currentItem:I
    if-eqz p1, :cond_95

    .line 241
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.ITEM"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 242
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.FULLSCREEN"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    .line 243
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.CURRENT_REF"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 261
    :cond_47
    :goto_47
    const-string v2, "album_name"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_bf

    .line 262
    const-string v2, "album_name"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    .line 267
    :goto_59
    const-string v2, "owner_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6b

    .line 268
    const-string v2, "owner_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mOwnerGaiaId:Ljava/lang/String;

    .line 271
    :cond_6b
    const-string v2, "mediarefs"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_cf

    .line 272
    const-string v2, "mediarefs"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v17

    .line 273
    .local v17, parcelables:[Landroid/os/Parcelable;
    move-object/from16 v0, v17

    array-length v2, v0

    new-array v2, v2, [Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    .line 274
    const/4 v14, 0x0

    .local v14, i:I
    :goto_83
    move-object/from16 v0, v17

    array-length v2, v0

    if-ge v14, v2, :cond_cf

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v2, v17, v14

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    aput-object v2, v3, v14

    .line 274
    add-int/lit8 v14, v14, 0x1

    goto :goto_83

    .line 245
    .end local v14           #i:I
    .end local v17           #parcelables:[Landroid/os/Parcelable;
    :cond_95
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mShowPhotoOnly:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    .line 246
    const-string v2, "notif_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 247
    .local v16, notificationId:Ljava/lang/String;
    if-eqz v16, :cond_b0

    .line 251
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    .line 255
    :cond_b0
    const-string v2, "refresh_album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 256
    const-string v2, "refresh_album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    goto :goto_47

    .line 264
    .end local v16           #notificationId:Ljava/lang/String;
    :cond_bf
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08007f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    goto :goto_59

    .line 279
    :cond_cf
    const-string v2, "album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e1

    .line 280
    const-string v2, "album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumId:Ljava/lang/String;

    .line 283
    :cond_e1
    const-string v2, "stream_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f3

    .line 284
    const-string v2, "stream_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mStreamId:Ljava/lang/String;

    .line 287
    :cond_f3
    const-string v2, "photos_of_user_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_105

    .line 288
    const-string v2, "photos_of_user_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoOfUserGaiaId:Ljava/lang/String;

    .line 291
    :cond_105
    const-string v2, "event_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_117

    .line 292
    const-string v2, "event_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mEventId:Ljava/lang/String;

    .line 295
    :cond_117
    const-string v2, "photo_url"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_129

    .line 296
    const-string v2, "photo_url"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoUrl:Ljava/lang/String;

    .line 299
    :cond_129
    const-string v2, "photo_ref"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13f

    if-gez v13, :cond_13f

    .line 300
    const-string v2, "photo_ref"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 303
    :cond_13f
    const-string v2, "page_hint"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_154

    if-gez v13, :cond_154

    .line 304
    const-string v2, "page_hint"

    const/4 v3, -0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPageHint:I

    .line 308
    :cond_154
    const-string v2, "photo_index"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_165

    if-gez v13, :cond_165

    .line 309
    const-string v2, "photo_index"

    const/4 v3, -0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 311
    :cond_165
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentIndex:I

    .line 314
    if-eqz v18, :cond_180

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_180

    .line 315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mOwnerGaiaId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v2, v1, v3}, Lcom/google/android/apps/plus/service/EsService;->getAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :cond_180
    const v2, 0x7f03007f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setContentView(I)V

    .line 319
    const v2, 0x7f090167

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    .line 321
    const-string v2, "force_load_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22a

    const-string v2, "force_load_id"

    const-wide/16 v3, 0x0

    invoke-virtual {v15, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 324
    .local v7, forceLoadId:Ljava/lang/Long;
    :goto_1a9
    const-string v2, "allow_plusone"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 325
    .local v11, allowPlusOne:Z
    new-instance v2, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mStreamId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mEventId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->setFragmentPagerListener(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;)V

    .line 329
    const v2, 0x7f090168

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setOnInterceptTouchListener(Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;)V

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const v3, 0x7f090029

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 337
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_22d

    .line 338
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v12

    .line 339
    .local v12, actionBar:Landroid/app/ActionBar;
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 345
    .end local v12           #actionBar:Landroid/app/ActionBar;
    :goto_220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->updateView(Landroid/view/View;)V

    .line 346
    return-void

    .line 321
    .end local v7           #forceLoadId:Ljava/lang/Long;
    .end local v11           #allowPlusOne:Z
    :cond_22a
    const/4 v7, 0x0

    goto/16 :goto_1a9

    .line 341
    .restart local v7       #forceLoadId:Ljava/lang/Long;
    .restart local v11       #allowPlusOne:Z
    :cond_22d
    const/4 v2, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->showTitlebar(ZZ)V

    .line 342
    const v2, 0x7f100017

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->createTitlebarButtons(I)V

    goto :goto_220
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 10
    .parameter "id"
    .parameter "args"

    .prologue
    const/4 v5, 0x0

    .line 444
    const-string v4, "tag"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 445
    .local v3, tag:Ljava/lang/String;
    packed-switch p1, :pswitch_data_5e

    .line 472
    const/4 v1, 0x0

    :goto_b
    return-object v1

    .line 447
    :pswitch_c
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 448
    .local v1, progressDialog:Landroid/app/ProgressDialog;
    const-string v4, "dialog_message"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 449
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 450
    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_b

    .line 455
    .end local v1           #progressDialog:Landroid/app/ProgressDialog;
    :pswitch_21
    new-instance v2, Lcom/google/android/apps/plus/phone/PhotoViewActivity$RetryDialogListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$RetryDialogListener;-><init>(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Ljava/lang/String;)V

    .line 456
    .local v2, retryListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$RetryDialogListener;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 457
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v4, 0x7f080085

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0801c6

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0801c8

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 460
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_b

    .line 464
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    .end local v2           #retryListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$RetryDialogListener;
    :pswitch_44
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 465
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v4, 0x7f080086

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0801c4

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFailedListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 467
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_b

    .line 445
    nop

    :pswitch_data_5e
    .packed-switch 0x7f09002e
        :pswitch_c
        :pswitch_44
        :pswitch_21
    .end packed-switch
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 15
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 587
    packed-switch p1, :pswitch_data_7a

    move-object v0, v4

    .line 609
    :goto_7
    return-object v0

    .line 589
    :pswitch_8
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFragmentIsLoading:Z

    .line 591
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v4, :cond_40

    array-length v0, v4

    if-ne v0, v1, :cond_40

    aget-object v0, v4, v3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_25

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_25

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_25
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3e

    const-string v4, "content:"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    move v0, v1

    :goto_34
    if-eqz v0, :cond_42

    .line 592
    new-instance v0, Lcom/google/android/apps/plus/phone/CameraPhotoLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/CameraPhotoLoader;-><init>(Landroid/content/Context;)V

    goto :goto_7

    :cond_3e
    move v0, v3

    .line 591
    goto :goto_34

    :cond_40
    move v0, v3

    goto :goto_34

    .line 594
    :cond_42
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mOwnerGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoOfUserGaiaId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mStreamId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mEventId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPhotoUrl:Ljava/lang/String;

    iget v10, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mPageHint:I

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_7

    .line 601
    :pswitch_5b
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 604
    .local v11, albumUri:Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v11, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 605
    .local v2, loaderUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewActivity$AlbumDetailsQuery;->PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 587
    :pswitch_data_7a
    .packed-switch 0x7f090029
        :pswitch_8
        :pswitch_5b
    .end packed-switch
.end method

.method public final onFragmentVisible(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    if-nez v0, :cond_9

    .line 734
    :cond_8
    :goto_8
    return-void

    .line 730
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v1

    if-ne v0, v1, :cond_1a

    .line 731
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFragmentIsLoading:Z

    .line 733
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->updateView(Landroid/view/View;)V

    goto :goto_8
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 9
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 53
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_80

    :cond_c
    :goto_c
    return-void

    :pswitch_d
    if-eqz p2, :cond_15

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_43

    :cond_15
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsEmpty:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFragmentIsLoading:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->updateView(Landroid/view/View;)V

    :goto_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_51

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_51

    move v0, v1

    :goto_27
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumCount:I

    if-ne v4, v5, :cond_53

    :goto_31
    if-eqz v0, :cond_55

    if-nez v3, :cond_37

    if-eqz v1, :cond_55

    :cond_37
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f09002a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_c

    :cond_43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$2;-><init>(Lcom/google/android/apps/plus/phone/PhotoViewActivity;Landroid/database/Cursor;Landroid/support/v4/content/Loader;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1e

    :cond_51
    move v0, v2

    goto :goto_27

    :cond_53
    move v1, v2

    goto :goto_31

    :cond_55
    if-nez v3, :cond_c

    :cond_57
    :goto_57
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->updateTitleAndSubtitle()V

    goto :goto_c

    :pswitch_5b
    if-eqz p2, :cond_c

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    if-nez v2, :cond_6d

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumName:Ljava/lang/String;

    :cond_6d
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumCount:I

    if-ne v0, v5, :cond_57

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_7b

    const/4 v0, -0x2

    :goto_78
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAlbumCount:I

    goto :goto_57

    :cond_7b
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_78

    :pswitch_data_80
    .packed-switch 0x7f090029
        :pswitch_d
        :pswitch_5b
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 692
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    .prologue
    const/4 v2, 0x1

    .line 508
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;

    .line 509
    .local v1, listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 522
    .end local v1           #listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;
    :goto_19
    return v2

    .line 516
    :cond_1a
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x102002c

    if-ne v3, v4, :cond_27

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->finish()V

    goto :goto_19

    .line 522
    :cond_27
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_19
.end method

.method public final onPageActivated(Landroid/support/v4/app/Fragment;)V
    .registers 2
    .parameter "fragment"

    .prologue
    .line 714
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setViewActivated()V

    .line 715
    return-void
.end method

.method public final onPageScrollStateChanged(I)V
    .registers 3
    .parameter "state"

    .prologue
    .line 709
    if-eqz p1, :cond_6

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewScrolling:Z

    .line 710
    return-void

    .line 709
    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final onPageScrolled$486775f1(IF)V
    .registers 3
    .parameter "position"
    .parameter "positionOffset"

    .prologue
    .line 696
    return-void
.end method

.method public final onPageSelected(I)V
    .registers 4
    .parameter "position"

    .prologue
    const/4 v1, 0x1

    .line 700
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewScrolling:Z

    if-eqz v0, :cond_1e

    :cond_9
    move v0, v1

    :goto_a
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setFullScreen(ZZ)V

    .line 701
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setViewActivated()V

    .line 702
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->updateTitleAndSubtitle()V

    .line 703
    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentIndex:I

    .line 704
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getMediaRef(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 705
    return-void

    .line 700
    :cond_1e
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsPaused:Z

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mActionBarLayoutListener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$ActionBarLayoutListener;

    if-eqz v0, :cond_a

    .line 370
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->clearListener()V

    .line 373
    :cond_a
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    .line 374
    return-void
.end method

.method public final onPhotoRemoved$1349ef()V
    .registers 7

    .prologue
    .line 565
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 566
    .local v0, data:Landroid/database/Cursor;
    if-nez v0, :cond_9

    .line 583
    :goto_8
    return-void

    .line 571
    :cond_9
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 572
    .local v1, dataCount:I
    const/4 v3, 0x1

    if-gt v1, v3, :cond_22

    .line 574
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    .line 576
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 577
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 578
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->finish()V

    goto :goto_8

    .line 582
    .end local v2           #intent:Landroid/content/Intent;
    :cond_22
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const v4, 0x7f090029

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_8
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 6
    .parameter "id"
    .parameter "dialog"
    .parameter "args"

    .prologue
    .line 427
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 428
    packed-switch p1, :pswitch_data_18

    .line 440
    :cond_6
    :goto_6
    return-void

    .line 432
    :pswitch_7
    instance-of v1, p2, Landroid/app/ProgressDialog;

    if-eqz v1, :cond_6

    move-object v0, p2

    .line 434
    check-cast v0, Landroid/app/ProgressDialog;

    .line 435
    .local v0, pd:Landroid/app/ProgressDialog;
    const-string v1, "dialog_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 428
    :pswitch_data_18
    .packed-switch 0x7f09002e
        :pswitch_7
    .end packed-switch
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 7
    .parameter "menu"

    .prologue
    .line 487
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    if-ge v0, v3, :cond_12

    .line 488
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 487
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 492
    :cond_12
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;

    .line 493
    .local v2, listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;
    invoke-interface {v2, p1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;->onPrepareTitlebarButtons(Landroid/view/Menu;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 498
    .end local v2           #listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;
    :cond_2a
    return-void
.end method

.method protected onResume()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 350
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 351
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->isIntentAccountActive()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 352
    const v0, 0x7f100017

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->createTitlebarButtons(I)V

    .line 353
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setFullScreen(ZZ)V

    .line 355
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mIsPaused:Z

    .line 356
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRestartLoader:Z

    if-eqz v0, :cond_28

    .line 357
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mRestartLoader:Z

    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f090029

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 363
    :cond_28
    :goto_28
    return-void

    .line 361
    :cond_29
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->finish()V

    goto :goto_28
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 477
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 479
    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.ITEM"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 480
    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.FULLSCREEN"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 481
    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.CURRENT_REF"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 482
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 502
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->finish()V

    .line 503
    return-void
.end method

.method public final onTouchIntercept(FF)Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;
    .registers 8
    .parameter "origX"
    .parameter "origY"

    .prologue
    .line 749
    const/4 v1, 0x0

    .line 750
    .local v1, interceptLeft:Z
    const/4 v2, 0x0

    .line 752
    .local v2, interceptRight:Z
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_24

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;

    .line 753
    .local v3, listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    if-nez v1, :cond_1a

    .line 754
    invoke-interface {v3, p1, p2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;->onInterceptMoveLeft(FF)Z

    move-result v1

    .line 756
    :cond_1a
    if-nez v2, :cond_20

    .line 757
    invoke-interface {v3, p1, p2}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;->onInterceptMoveRight(FF)Z

    move-result v2

    .line 759
    :cond_20
    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;->onViewActivated()V

    goto :goto_8

    .line 762
    .end local v3           #listener:Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;
    :cond_24
    if-eqz v1, :cond_2e

    .line 763
    if-eqz v2, :cond_2b

    .line 764
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->BOTH:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    .line 770
    :goto_2a
    return-object v4

    .line 766
    :cond_2b
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->LEFT:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_2a

    .line 767
    :cond_2e
    if-eqz v2, :cond_33

    .line 768
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->RIGHT:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_2a

    .line 770
    :cond_33
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->NONE:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_2a
.end method

.method public final removeMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 543
    return-void
.end method

.method public final removeScreenListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 533
    return-void
.end method

.method public final toggleFullScreen()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 560
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->mFullScreen:Z

    if-nez v0, :cond_a

    move v0, v1

    :goto_6
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->setFullScreen(ZZ)V

    .line 561
    return-void

    .line 560
    :cond_a
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final updateMenuItems()V
    .registers 3

    .prologue
    .line 738
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_a

    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->invalidateOptionsMenu()V

    .line 745
    :goto_9
    return-void

    .line 743
    :cond_a
    const v0, 0x7f100017

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoViewActivity;->createTitlebarButtons(I)V

    goto :goto_9
.end method
