.class public final Lcom/google/android/apps/plus/phone/PhotoViewLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PhotoViewLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoCommentQuery;,
        Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoPlusOneQuery;,
        Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoCaptionQuery;,
        Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoQuery;
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mForceLoadId:Ljava/lang/Long;

.field private final mNotificationUri:Landroid/net/Uri;

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private final mPhotoId:J

.field private final mPhotoUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/Long;)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "ownerGaiaId"
    .parameter "photoId"
    .parameter "photoUrl"
    .parameter "forceLoadId"

    .prologue
    .line 170
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p4, p5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 160
    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    .line 172
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 173
    iput-wide p4, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoId:J

    .line 174
    iput-object p6, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoUrl:Ljava/lang/String;

    .line 175
    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mForceLoadId:Ljava/lang/Long;

    .line 176
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mOwnerGaiaId:Ljava/lang/String;

    .line 177
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p4, p5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mNotificationUri:Landroid/net/Uri;

    .line 178
    return-void
.end method

.method private final doNetworkRequest(Z)V
    .registers 10
    .parameter "threaded"

    .prologue
    const/4 v3, 0x0

    .line 315
    new-instance v0, Lcom/google/android/apps/plus/api/GetPhotoOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v5, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoId:J

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mOwnerGaiaId:Ljava/lang/String;

    move-object v4, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/GetPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;JLjava/lang/String;)V

    .line 317
    .local v0, op:Lcom/google/android/apps/plus/network/HttpOperation;
    if-eqz p1, :cond_17

    .line 318
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    .line 325
    :goto_16
    return-void

    .line 320
    :cond_17
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    goto :goto_16
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 24

    .prologue
    .line 200
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 204
    .local v1, resolver:Landroid/content/ContentResolver;
    const/16 v16, 0x0

    .line 206
    .local v16, performedFetch:Z
    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoId:J

    const-wide/16 v9, 0x0

    cmp-long v3, v7, v9

    if-eqz v3, :cond_137

    .line 207
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mForceLoadId:Ljava/lang/Long;

    if-eqz v3, :cond_37

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoId:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mForceLoadId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v3, v7, v9

    if-nez v3, :cond_37

    .line 208
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->doNetworkRequest(Z)V

    .line 209
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mForceLoadId:Ljava/lang/Long;

    .line 210
    const/16 v16, 0x1

    .line 213
    :cond_37
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoId:J

    invoke-static {v3, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 217
    .local v2, photoUri:Landroid/net/Uri;
    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 218
    .local v17, photoCursor:Landroid/database/Cursor;
    if-nez v16, :cond_6b

    if-eqz v17, :cond_5c

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_6b

    .line 220
    :cond_5c
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->doNetworkRequest(Z)V

    .line 221
    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 224
    :cond_6b
    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoPlusOneQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "plusone_data NOT NULL AND plusone_count > 0"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 262
    .end local v2           #photoUri:Landroid/net/Uri;
    .local v18, plusOneCursor:Landroid/database/Cursor;
    :goto_75
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoId:J

    invoke-static {v3, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    .line 265
    .local v4, captionUri:Landroid/net/Uri;
    sget-object v5, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoCaptionQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "description NOT NULL AND description != \'\'"

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 268
    .local v11, captionCursor:Landroid/database/Cursor;
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoId:J

    invoke-static {v3, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    .line 272
    .local v6, commentQueryUri:Landroid/net/Uri;
    if-eqz v17, :cond_203

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_203

    .line 273
    const/16 v3, 0xb

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 274
    .local v12, commentCount:I
    const/4 v3, -0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 279
    :goto_ba
    sget-object v7, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoCommentQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "create_time"

    move-object v5, v1

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 281
    .local v13, commentCursor:Landroid/database/Cursor;
    if-eqz v13, :cond_206

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v14

    .line 284
    .local v14, commentCursorCount:I
    :goto_cb
    if-eq v12, v14, :cond_209

    .line 285
    new-instance v15, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoCommentQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v15, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .local v15, commentLoadCursor:Landroid/database/Cursor;
    move-object v3, v15

    .line 286
    check-cast v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    .line 300
    :goto_11d
    new-instance v19, Landroid/database/MergeCursor;

    const/4 v3, 0x5

    new-array v3, v3, [Landroid/database/Cursor;

    const/4 v5, 0x0

    aput-object v17, v3, v5

    const/4 v5, 0x1

    aput-object v11, v3, v5

    const/4 v5, 0x2

    aput-object v18, v3, v5

    const/4 v5, 0x3

    aput-object v13, v3, v5

    const/4 v5, 0x4

    aput-object v15, v3, v5

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 307
    .local v19, returnCursor:Landroid/database/MergeCursor;
    return-object v19

    .line 227
    .end local v4           #captionUri:Landroid/net/Uri;
    .end local v6           #commentQueryUri:Landroid/net/Uri;
    .end local v11           #captionCursor:Landroid/database/Cursor;
    .end local v12           #commentCount:I
    .end local v13           #commentCursor:Landroid/database/Cursor;
    .end local v14           #commentCursorCount:I
    .end local v15           #commentLoadCursor:Landroid/database/Cursor;
    .end local v17           #photoCursor:Landroid/database/Cursor;
    .end local v18           #plusOneCursor:Landroid/database/Cursor;
    .end local v19           #returnCursor:Landroid/database/MergeCursor;
    :cond_137
    new-instance v17, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 228
    .restart local v17       #photoCursor:Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoData(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/api/services/plusi/model/DataVideo;

    move-result-object v21

    .line 232
    .local v21, videoData:Lcom/google/api/services/plusi/model/DataVideo;
    if-eqz v21, :cond_1f9

    .line 233
    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v22

    .line 238
    .local v22, videoDataBytes:[B
    :goto_15c
    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    array-length v3, v3

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v20, v0

    .line 239
    .local v20, row:[Ljava/lang/Object;
    const/4 v3, 0x0

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v20, v3

    .line 240
    const/4 v3, 0x1

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 241
    const/4 v3, 0x2

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v20, v3

    .line 242
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mOwnerGaiaId:Ljava/lang/String;

    aput-object v5, v20, v3

    .line 243
    const/4 v3, 0x4

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 244
    const/4 v3, 0x5

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v20, v3

    .line 245
    const/4 v3, 0x6

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 246
    const/4 v3, 0x7

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 247
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mPhotoUrl:Ljava/lang/String;

    aput-object v5, v20, v3

    .line 248
    const/16 v3, 0x9

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 249
    const/16 v3, 0xa

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v20, v3

    .line 250
    const/16 v3, 0xb

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 251
    const/16 v3, 0xc

    const/4 v5, 0x0

    aput-object v5, v20, v3

    .line 252
    const/16 v3, 0xd

    aput-object v22, v20, v3

    .line 253
    const/16 v3, 0xe

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 254
    const/16 v3, 0xf

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 255
    const/16 v3, 0x10

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v20, v3

    .line 256
    const/16 v5, 0x11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v3, :cond_1fd

    const/4 v3, 0x0

    :goto_1ea
    aput-object v3, v20, v5

    move-object/from16 v3, v17

    .line 257
    check-cast v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 259
    const/16 v18, 0x0

    .restart local v18       #plusOneCursor:Landroid/database/Cursor;
    goto/16 :goto_75

    .line 235
    .end local v18           #plusOneCursor:Landroid/database/Cursor;
    .end local v20           #row:[Ljava/lang/Object;
    .end local v22           #videoDataBytes:[B
    :cond_1f9
    const/16 v22, 0x0

    .restart local v22       #videoDataBytes:[B
    goto/16 :goto_15c

    .line 256
    .restart local v20       #row:[Ljava/lang/Object;
    :cond_1fd
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_1ea

    .line 276
    .end local v20           #row:[Ljava/lang/Object;
    .end local v21           #videoData:Lcom/google/api/services/plusi/model/DataVideo;
    .end local v22           #videoDataBytes:[B
    .restart local v4       #captionUri:Landroid/net/Uri;
    .restart local v6       #commentQueryUri:Landroid/net/Uri;
    .restart local v11       #captionCursor:Landroid/database/Cursor;
    .restart local v18       #plusOneCursor:Landroid/database/Cursor;
    :cond_203
    const/4 v12, 0x0

    .restart local v12       #commentCount:I
    goto/16 :goto_ba

    .line 281
    .restart local v13       #commentCursor:Landroid/database/Cursor;
    :cond_206
    const/4 v14, 0x0

    goto/16 :goto_cb

    .line 297
    .restart local v14       #commentCursorCount:I
    :cond_209
    const/4 v15, 0x0

    .restart local v15       #commentLoadCursor:Landroid/database/Cursor;
    goto/16 :goto_11d
.end method

.method protected final onAbandon()V
    .registers 3

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_14

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mObserverRegistered:Z

    .line 196
    :cond_14
    return-void
.end method

.method protected final onStartLoading()V
    .registers 5

    .prologue
    .line 182
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->onStartLoading()V

    .line 183
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mObserverRegistered:Z

    if-nez v0, :cond_1a

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mNotificationUri:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;->mObserverRegistered:Z

    .line 188
    :cond_1a
    return-void
.end method
