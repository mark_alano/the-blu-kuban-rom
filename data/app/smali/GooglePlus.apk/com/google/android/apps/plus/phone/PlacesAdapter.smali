.class public final Lcom/google/android/apps/plus/phone/PlacesAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PlacesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PlacesAdapter$LocationQuery;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 43
    return-void
.end method

.method public static getLocation(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 3
    .parameter "cursor"

    .prologue
    .line 49
    const/4 v1, 0x2

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 50
    .local v0, locationData:[B
    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 10
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 58
    const v5, 0x1020006

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 59
    .local v2, iconView:Landroid/widget/ImageView;
    const v5, 0x1020016

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 60
    .local v4, nameView:Landroid/widget/TextView;
    const v5, 0x1020005

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 61
    .local v1, addressView:Landroid/widget/TextView;
    invoke-static {p3}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getLocation(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v3

    .line 64
    .local v3, location:Lcom/google/android/apps/plus/content/DbLocation;
    if-eqz v3, :cond_3a

    .line 65
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->isPrecise()Z

    move-result v5

    if-eqz v5, :cond_3b

    .line 66
    const v5, 0x7f020175

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 67
    const v5, 0x7f08018f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 68
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, address:Ljava/lang/String;
    :goto_37
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    .end local v0           #address:Ljava/lang/String;
    :cond_3a
    return-void

    .line 69
    :cond_3b
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->isCoarse()Z

    move-result v5

    if-eqz v5, :cond_52

    .line 70
    const v5, 0x7f0200cb

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 71
    const v5, 0x7f080190

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 72
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v0

    .restart local v0       #address:Ljava/lang/String;
    goto :goto_37

    .line 74
    .end local v0           #address:Ljava/lang/String;
    :cond_52
    const v5, 0x7f0200cc

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 75
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getBestAddress()Ljava/lang/String;

    move-result-object v0

    .restart local v0       #address:Ljava/lang/String;
    goto :goto_37
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    .line 87
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03005a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
