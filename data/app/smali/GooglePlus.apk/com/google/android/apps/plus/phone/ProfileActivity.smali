.class public Lcom/google/android/apps/plus/phone/ProfileActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "ProfileActivity.java"


# instance fields
.field private mCurrentSpinnerIndex:I

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    return-void
.end method

.method public static createSpinnerAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .registers 3
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f0300bf

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 39
    .local v0, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 42
    const v1, 0x7f0801d6

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 43
    const v1, 0x7f0801d7

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 45
    return-object v0
.end method


# virtual methods
.method protected final createHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;
    .registers 2

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;-><init>()V

    return-object v0
.end method

.method protected final getExtrasForLogging()Landroid/os/Bundle;
    .registers 5

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "person_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 156
    .local v1, personId:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 157
    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, gaiaId:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 159
    const-string v2, "extra_gaia_id"

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 162
    .end local v0           #gaiaId:Ljava/lang/String;
    :goto_20
    return-object v2

    :cond_21
    const/4 v2, 0x0

    goto :goto_20
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 4
    .parameter "actionBar"

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    .line 143
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 5
    .parameter "fragment"

    .prologue
    .line 58
    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    if-eqz v2, :cond_e

    move-object v1, p1

    .line 59
    check-cast v1, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    .line 60
    .local v1, profileFragment:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->relinquishPrimarySpinner()V

    .line 65
    .end local v1           #profileFragment:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    :cond_a
    :goto_a
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 66
    return-void

    .line 61
    :cond_e
    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    if-eqz v2, :cond_a

    move-object v0, p1

    .line 62
    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    .line 63
    .local v0, albumsFragment:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->relinquishPrimarySpinner()V

    goto :goto_a
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    .line 70
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->createSpinnerAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 71
    if-nez p1, :cond_29

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "profile_view_type"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 74
    .local v1, viewType:I
    packed-switch v1, :pswitch_data_34

    .line 83
    :goto_16
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "notif_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, notificationId:Ljava/lang/String;
    if-eqz v0, :cond_29

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {p0, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    .line 94
    .end local v0           #notificationId:Ljava/lang/String;
    .end local v1           #viewType:I
    :cond_29
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    return-void

    .line 76
    .restart local v1       #viewType:I
    :pswitch_2d
    iput v4, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    goto :goto_16

    .line 79
    :pswitch_30
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    goto :goto_16

    .line 74
    :pswitch_data_34
    .packed-switch 0x0
        :pswitch_2d
        :pswitch_30
    .end packed-switch
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .registers 4
    .parameter "position"

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onPrimarySpinnerSelectionChange(I)V

    .line 113
    iget v1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    if-ne v1, p1, :cond_8

    .line 133
    :cond_7
    :goto_7
    return-void

    .line 117
    :cond_8
    const/4 v0, 0x0

    .line 118
    .local v0, fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    packed-switch p1, :pswitch_data_20

    .line 129
    :goto_c
    if-eqz v0, :cond_7

    .line 130
    iput p1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    .line 131
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ProfileActivity;->replaceFragment(Lcom/google/android/apps/plus/phone/HostedFragment;)V

    goto :goto_7

    .line 120
    :pswitch_14
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    .end local v0           #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;-><init>()V

    .line 121
    .restart local v0       #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    goto :goto_c

    .line 124
    :pswitch_1a
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    .end local v0           #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;-><init>()V

    .restart local v0       #fragment:Lcom/google/android/apps/plus/phone/HostedFragment;
    goto :goto_c

    .line 118
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_14
        :pswitch_1a
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 106
    const-string v0, "spinnerIndex"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    .line 107
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 100
    const-string v0, "spinnerIndex"

    iget v1, p0, Lcom/google/android/apps/plus/phone/ProfileActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    return-void
.end method
