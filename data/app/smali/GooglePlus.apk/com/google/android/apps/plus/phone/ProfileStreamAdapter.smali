.class public final Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
.super Lcom/google/android/apps/plus/phone/StreamAdapter;
.source "ProfileStreamAdapter.java"


# instance fields
.field private mBlockRequestPending:Z

.field mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field mCircleNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

.field private mErrorText:Ljava/lang/String;

.field mFamilyName:Ljava/lang/String;

.field mFullName:Ljava/lang/String;

.field mGender:Ljava/lang/String;

.field mGivenName:Ljava/lang/String;

.field mHasProfile:Z

.field mIsBlocked:Z

.field private mIsLocalPlusPage:Z

.field mIsMuted:Z

.field mIsMyProfile:Z

.field private mIsPlusPage:Z

.field mIsSmsIntentRegistered:Z

.field private mIsUnclaimedLocalPlusPage:Z

.field mMapLocationBitmap:Landroid/graphics/Bitmap;

.field private mPackedCircleIds:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;

.field mPlusOneByMe:Z

.field mPlusOnes:I

.field private mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

.field mProfileLoadFailed:Z

.field private mProfileViewOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

.field mShowAddToCircles:Z

.field mShowBlocked:Z

.field mShowCircles:Z

.field mShowProgress:Z

.field private mViewIsExpanded:Z

.field private mViewingAsPlusPage:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)V
    .registers 20
    .parameter "context"
    .parameter "gridView"
    .parameter "account"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewUseListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"
    .parameter "floatingComposeBarView"

    .prologue
    .line 105
    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/phone/StreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)V

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    .line 107
    return-void
.end method

.method private bindProfileAboutView(Lcom/google/android/apps/plus/views/ProfileAboutView;)V
    .registers 59
    .parameter "profileView"

    .prologue
    .line 447
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    if-nez v5, :cond_7

    .line 865
    :goto_6
    return-void

    .line 451
    :cond_7
    const/16 v35, 0x0

    .line 453
    .local v35, intro:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    if-eqz v5, :cond_343

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    if-eqz v5, :cond_343

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBookEntry;->url:Ljava/lang/String;

    if-eqz v5, :cond_343

    .line 456
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->coverPhotoEntry:Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ScrapBookEntry;->url:Ljava/lang/String;

    move-object/from16 v44, v0

    .line 457
    .local v44, photoUrl:Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoUrl(Ljava/lang/String;)V

    .line 479
    .end local v44           #photoUrl:Ljava/lang/String;
    :goto_42
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v12, v5, Lcom/google/api/services/plusi/model/CommonContent;->photoUrl:Ljava/lang/String;

    .line 480
    .local v12, avatarUrl:Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3ba

    .line 481
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAvatarUrl(Ljava/lang/String;Z)V

    .line 486
    :goto_59
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGivenName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFamilyName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v8}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    if-eqz v5, :cond_3c5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->incomingConnections:Lcom/google/api/services/plusi/model/IntField;

    if-eqz v5, :cond_3c5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->incomingConnections:Lcom/google/api/services/plusi/model/IntField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/IntField;->value:Ljava/lang/Integer;

    if-eqz v5, :cond_3c5

    .line 491
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->incomingConnections:Lcom/google/api/services/plusi/model/IntField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/IntField;->value:Ljava/lang/Integer;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAddedByCount(Ljava/lang/Integer;)V

    .line 496
    :goto_97
    const/16 v38, 0x0

    .line 498
    .local v38, localPageAddress:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    if-eqz v5, :cond_3cd

    .line 499
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getFullAddress(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v38

    .line 500
    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocation(Ljava/lang/String;Z)V

    .line 530
    :cond_af
    :goto_af
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v5, :cond_465

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    const/4 v7, -0x1

    if-eq v5, v7, :cond_465

    .line 531
    const v5, 0x7f08039e

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    const/4 v10, 0x1

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    .line 533
    .local v45, plusOnesString:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOneByMe:Z

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setPlusOneData(Ljava/lang/String;Z)V

    .line 539
    .end local v45           #plusOnesString:Ljava/lang/String;
    :goto_e3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_46e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->tagLine:Lcom/google/api/services/plusi/model/StringField;

    if-eqz v5, :cond_46e

    .line 540
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->tagLine:Lcom/google/api/services/plusi/model/StringField;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/StringField;->value:Ljava/lang/String;

    move-object/from16 v52, v0

    .line 545
    .local v52, tagLine:Ljava/lang/String;
    :goto_101
    invoke-static/range {v52 .. v52}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_472

    .line 546
    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setTagLine(Ljava/lang/String;)V

    .line 551
    :goto_10e
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasProfile:Z

    if-nez v5, :cond_127

    .line 553
    const v5, 0x7f0801db

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    aput-object v9, v7, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    .line 556
    :cond_127
    if-nez v35, :cond_149

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_149

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->introduction:Lcom/google/api/services/plusi/model/StringField;

    if-eqz v5, :cond_149

    .line 558
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->introduction:Lcom/google/api/services/plusi/model/StringField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/StringField;->value:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v35

    .line 561
    :cond_149
    invoke-static/range {v35 .. v35}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_156

    .line 562
    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setIntroduction(Ljava/lang/String;)V

    .line 565
    :cond_156
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_47a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    if-eqz v5, :cond_47a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Links;->link:Ljava/util/List;

    if-eqz v5, :cond_47a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Links;->link:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_47a

    const/16 v27, 0x1

    .line 570
    .local v27, hasLinks:Z
    :goto_186
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    if-eqz v5, :cond_521

    .line 571
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    if-eqz v5, :cond_1a7

    .line 573
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-nez v7, :cond_47e

    const/4 v12, 0x0

    .line 574
    :goto_19b
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1a7

    .line 575
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAvatarUrl(Ljava/lang/String;Z)V

    .line 579
    :cond_1a7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->phone:Lcom/google/api/services/plusi/model/PhoneProto;

    if-nez v7, :cond_484

    const/16 v43, 0x0

    .line 580
    .local v43, phone:Ljava/lang/String;
    :goto_1b7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->placeInfo:Lcom/google/api/services/plusi/model/PlaceInfo;

    if-nez v7, :cond_4af

    const/16 v40, 0x0

    .line 582
    .local v40, mapsCid:Ljava/lang/String;
    :goto_1c7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    move-object/from16 v2, v40

    move-object/from16 v3, v38

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocalActions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v6, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    .line 587
    .local v6, scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;
    if-eqz v6, :cond_4bd

    const/4 v5, 0x1

    :goto_1e5
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalEditorialReviewsSection(Z)V

    .line 588
    if-eqz v6, :cond_23a

    .line 589
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->zagatEditorialReview:Lcom/google/api/services/plusi/model/Description;

    if-nez v7, :cond_4c0

    const/4 v7, 0x0

    :goto_1fb
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceLabel(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceValue(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v10, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/FrontendPaperProto;->reviewsHeadline:Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;

    if-eqz v10, :cond_4cc

    iget-object v10, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/FrontendPaperProto;->reviewsHeadline:Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;->aggregatedReviews:Lcom/google/api/services/plusi/model/AggregatedReviewsProto;

    if-eqz v10, :cond_4cc

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->reviewsHeadline:Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ReviewsHeadlineProto;->aggregatedReviews:Lcom/google/api/services/plusi/model/AggregatedReviewsProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/AggregatedReviewsProto;->numReviews:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v10

    :goto_235
    move-object/from16 v5, p1

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocalEditorialReviews(Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 597
    :cond_23a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->knownForTerms:Lcom/google/api/services/plusi/model/KnownForTermsProto;

    if-eqz v7, :cond_254

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->knownForTerms:Lcom/google/api/services/plusi/model/KnownForTermsProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/KnownForTermsProto;->term:Ljava/util/List;

    if-nez v7, :cond_4cf

    :cond_254
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v36

    .line 598
    .local v36, knownForTerms:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :goto_258
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getOpeningHoursSummary(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v42

    .line 599
    .local v42, openingHoursSummary:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getOpeningHoursFull(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;

    move-result-object v41

    .line 601
    .local v41, openingHoursFull:Ljava/lang/String;
    invoke-static/range {v42 .. v42}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_274

    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4dd

    :cond_274
    const/16 v30, 0x1

    .line 603
    .local v30, hasOpeningHours:Z
    :goto_276
    invoke-static/range {v43 .. v43}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_284

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v5

    if-gtz v5, :cond_284

    if-eqz v30, :cond_4e1

    :cond_284
    const/16 v28, 0x1

    .line 606
    .local v28, hasLocalDetails:Z
    :goto_286
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalDetailsSection(Z)V

    .line 607
    if-eqz v28, :cond_29c

    .line 608
    move-object/from16 v0, p1

    move-object/from16 v1, v36

    move-object/from16 v2, v43

    move-object/from16 v3, v42

    move-object/from16 v4, v41

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocalDetails(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    :cond_29c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->authorityPage:Lcom/google/api/services/plusi/model/AuthorityPageProto;

    if-nez v5, :cond_4e5

    const/16 v37, 0x0

    .line 613
    .local v37, link:Lcom/google/api/services/plusi/model/PlacePageLink;
    :goto_2ac
    if-eqz v37, :cond_4eb

    move-object/from16 v0, v37

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->url:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4eb

    const/16 v26, 0x1

    .line 614
    .local v26, hasHomepage:Z
    :goto_2ba
    if-eqz v26, :cond_4ef

    if-nez v27, :cond_4ef

    const/16 v50, 0x1

    .line 616
    .local v50, shouldShowHomepage:Z
    :goto_2c0
    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableHompageSection(Z)V

    .line 617
    if-eqz v50, :cond_2f1

    .line 618
    move-object/from16 v0, v37

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->url:Ljava/lang/String;

    move-object/from16 v0, v37

    iget-object v7, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "http://www.google.com/s2/u/0/favicons?domain="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v37

    iget-object v9, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->url:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v8}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setHomepage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    :cond_2f1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasYourActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v32

    .line 624
    .local v32, hasYourActivity:Z
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearAllReviews()V

    .line 625
    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalYourActivitySection(Z)V

    .line 626
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getYourReview(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-result-object v56

    .line 627
    .local v56, yourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    if-eqz v56, :cond_314

    .line 628
    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addYourReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    .line 632
    :cond_314
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasCircleActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v22

    .line 633
    .local v22, hasCircleActivity:Z
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalYourCirclesActivitySection(Z)V

    .line 634
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getCircleReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .local v34, i$:Ljava/util/Iterator;
    :goto_32f
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4f3

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    .line 635
    .local v46, review:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addCircleReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    goto :goto_32f

    .line 458
    .end local v6           #scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;
    .end local v12           #avatarUrl:Ljava/lang/String;
    .end local v22           #hasCircleActivity:Z
    .end local v26           #hasHomepage:Z
    .end local v27           #hasLinks:Z
    .end local v28           #hasLocalDetails:Z
    .end local v30           #hasOpeningHours:Z
    .end local v32           #hasYourActivity:Z
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v36           #knownForTerms:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v37           #link:Lcom/google/api/services/plusi/model/PlacePageLink;
    .end local v38           #localPageAddress:Ljava/lang/String;
    .end local v40           #mapsCid:Ljava/lang/String;
    .end local v41           #openingHoursFull:Ljava/lang/String;
    .end local v42           #openingHoursSummary:Ljava/lang/String;
    .end local v43           #phone:Ljava/lang/String;
    .end local v46           #review:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    .end local v50           #shouldShowHomepage:Z
    .end local v52           #tagLine:Ljava/lang/String;
    .end local v56           #yourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    :cond_343
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    if-eqz v5, :cond_3b5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->albumId:Ljava/lang/String;

    if-eqz v5, :cond_3b5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    if-eqz v5, :cond_3b5

    .line 461
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v51

    .line 463
    .local v51, size:I
    move/from16 v0, v51

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v54, v0

    .line 464
    .local v54, urls:[Ljava/lang/String;
    const/16 v33, 0x0

    .local v33, i:I
    :goto_37b
    move/from16 v0, v33

    move/from16 v1, v51

    if-ge v0, v1, :cond_39a

    .line 465
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->plusiEntry:Ljava/util/List;

    move/from16 v0, v33

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/ScrapBookEntry;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBookEntry;->url:Ljava/lang/String;

    aput-object v5, v54, v33

    .line 464
    add-int/lit8 v33, v33, 0x1

    goto :goto_37b

    .line 468
    :cond_39a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->scrapbook:Lcom/google/api/services/plusi/model/ScrapBook;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ScrapBook;->albumId:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setScrapbookAlbumUrls(Ljava/lang/Long;[Ljava/lang/String;)V

    goto/16 :goto_42

    .line 471
    .end local v33           #i:I
    .end local v51           #size:I
    .end local v54           #urls:[Ljava/lang/String;
    :cond_3b5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoToDefault()V

    goto/16 :goto_42

    .line 483
    .restart local v12       #avatarUrl:Ljava/lang/String;
    :cond_3ba
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAvatarToDefault(Z)V

    goto/16 :goto_59

    .line 493
    :cond_3c5
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAddedByCount(Ljava/lang/Integer;)V

    goto/16 :goto_97

    .line 501
    .restart local v38       #localPageAddress:Ljava/lang/String;
    :cond_3cd
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_af

    .line 502
    const/16 v17, 0x0

    .line 503
    .local v17, employer:Ljava/lang/String;
    const/16 v39, 0x0

    .line 504
    .local v39, location:Ljava/lang/String;
    const/16 v49, 0x0

    .line 506
    .local v49, school:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    if-eqz v5, :cond_40a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    if-eqz v5, :cond_40a

    .line 508
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/Employment;

    .line 509
    .local v18, employment:Lcom/google/api/services/plusi/model/Employment;
    if-eqz v18, :cond_40a

    .line 510
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 513
    .end local v18           #employment:Lcom/google/api/services/plusi/model/Employment;
    :cond_40a
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEmployer(Ljava/lang/String;)V

    .line 515
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    if-eqz v5, :cond_427

    .line 516
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    move-object/from16 v39, v0

    .line 518
    :cond_427
    const/4 v5, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocation(Ljava/lang/String;Z)V

    .line 520
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    if-eqz v5, :cond_45c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    if-eqz v5, :cond_45c

    .line 522
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/api/services/plusi/model/Education;

    .line 523
    .local v14, education:Lcom/google/api/services/plusi/model/Education;
    if-eqz v14, :cond_45c

    .line 524
    iget-object v0, v14, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    move-object/from16 v49, v0

    .line 527
    .end local v14           #education:Lcom/google/api/services/plusi/model/Education;
    :cond_45c
    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEducation(Ljava/lang/String;)V

    goto/16 :goto_af

    .line 535
    .end local v17           #employer:Ljava/lang/String;
    .end local v39           #location:Ljava/lang/String;
    .end local v49           #school:Ljava/lang/String;
    :cond_465
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setPlusOneData(Ljava/lang/String;Z)V

    goto/16 :goto_e3

    .line 542
    :cond_46e
    const/16 v52, 0x0

    .restart local v52       #tagLine:Ljava/lang/String;
    goto/16 :goto_101

    .line 548
    :cond_472
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setTagLine(Ljava/lang/String;)V

    goto/16 :goto_10e

    .line 565
    :cond_47a
    const/16 v27, 0x0

    goto/16 :goto_186

    .line 573
    .restart local v27       #hasLinks:Z
    :cond_47e
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v12, v5, Lcom/google/api/services/plusi/model/CommonContent;->photoUrl:Ljava/lang/String;

    goto/16 :goto_19b

    .line 579
    :cond_484
    iget-object v7, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->phone:Lcom/google/api/services/plusi/model/PhoneProto;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PhoneProto;->phoneNumber:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_498

    const/16 v43, 0x0

    goto/16 :goto_1b7

    :cond_498
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->phone:Lcom/google/api/services/plusi/model/PhoneProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PhoneProto;->phoneNumber:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/PlacePagePhoneNumber;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/PlacePagePhoneNumber;->formattedPhone:Ljava/lang/String;

    move-object/from16 v43, v0

    goto/16 :goto_1b7

    .line 580
    .restart local v43       #phone:Ljava/lang/String;
    :cond_4af
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->placeInfo:Lcom/google/api/services/plusi/model/PlaceInfo;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/PlaceInfo;->clusterId:Ljava/lang/String;

    move-object/from16 v40, v0

    goto/16 :goto_1c7

    .line 587
    .restart local v6       #scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;
    .restart local v40       #mapsCid:Ljava/lang/String;
    :cond_4bd
    const/4 v5, 0x0

    goto/16 :goto_1e5

    .line 589
    :cond_4c0
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->zagatEditorialReview:Lcom/google/api/services/plusi/model/Description;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/Description;->text:Ljava/lang/String;

    goto/16 :goto_1fb

    :cond_4cc
    const/4 v10, 0x0

    goto/16 :goto_235

    .line 597
    :cond_4cf
    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->knownForTerms:Lcom/google/api/services/plusi/model/KnownForTermsProto;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/KnownForTermsProto;->term:Ljava/util/List;

    move-object/from16 v36, v0

    goto/16 :goto_258

    .line 601
    .restart local v36       #knownForTerms:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v41       #openingHoursFull:Ljava/lang/String;
    .restart local v42       #openingHoursSummary:Ljava/lang/String;
    :cond_4dd
    const/16 v30, 0x0

    goto/16 :goto_276

    .line 603
    .restart local v30       #hasOpeningHours:Z
    :cond_4e1
    const/16 v28, 0x0

    goto/16 :goto_286

    .line 612
    .restart local v28       #hasLocalDetails:Z
    :cond_4e5
    iget-object v0, v5, Lcom/google/api/services/plusi/model/AuthorityPageProto;->authorityLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    move-object/from16 v37, v0

    goto/16 :goto_2ac

    .line 613
    .restart local v37       #link:Lcom/google/api/services/plusi/model/PlacePageLink;
    :cond_4eb
    const/16 v26, 0x0

    goto/16 :goto_2ba

    .line 614
    .restart local v26       #hasHomepage:Z
    :cond_4ef
    const/16 v50, 0x0

    goto/16 :goto_2c0

    .line 640
    .restart local v22       #hasCircleActivity:Z
    .restart local v32       #hasYourActivity:Z
    .restart local v34       #i$:Ljava/util/Iterator;
    .restart local v50       #shouldShowHomepage:Z
    .restart local v56       #yourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    :cond_4f3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;

    move-result-object v47

    .line 641
    .local v47, reviews:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/GoogleReviewProto;>;"
    invoke-interface/range {v47 .. v47}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_51f

    const/4 v5, 0x1

    :goto_502
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocalReviewsSection(Z)V

    .line 642
    invoke-interface/range {v47 .. v47}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :goto_50b
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_521

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    .line 643
    .restart local v46       #review:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLocalReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    goto :goto_50b

    .line 641
    .end local v46           #review:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    :cond_51f
    const/4 v5, 0x0

    goto :goto_502

    .line 647
    .end local v6           #scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;
    .end local v22           #hasCircleActivity:Z
    .end local v26           #hasHomepage:Z
    .end local v28           #hasLocalDetails:Z
    .end local v30           #hasOpeningHours:Z
    .end local v32           #hasYourActivity:Z
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v36           #knownForTerms:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v37           #link:Lcom/google/api/services/plusi/model/PlacePageLink;
    .end local v40           #mapsCid:Ljava/lang/String;
    .end local v41           #openingHoursFull:Ljava/lang/String;
    .end local v42           #openingHoursSummary:Ljava/lang/String;
    .end local v43           #phone:Ljava/lang/String;
    .end local v47           #reviews:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/GoogleReviewProto;>;"
    .end local v50           #shouldShowHomepage:Z
    .end local v56           #yourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    :cond_521
    const/16 v24, 0x0

    .line 648
    .local v24, hasEmails:Z
    const/16 v31, 0x0

    .line 649
    .local v31, hasPhoneNumbers:Z
    const/16 v20, 0x0

    .line 651
    .local v20, hasAddresses:Z
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-nez v5, :cond_5b3

    const/4 v13, 0x0

    .line 653
    .local v13, contacts:Lcom/google/api/services/plusi/model/Contacts;
    :goto_530
    if-eqz v13, :cond_55c

    .line 654
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    if-eqz v5, :cond_5bd

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5bd

    const/16 v24, 0x1

    .line 655
    :goto_540
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    if-eqz v5, :cond_5c0

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5c0

    const/16 v31, 0x1

    .line 656
    :goto_54e
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    if-eqz v5, :cond_5c3

    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5c3

    const/16 v20, 0x1

    .line 659
    :cond_55c
    :goto_55c
    if-nez v24, :cond_562

    if-nez v31, :cond_562

    if-eqz v20, :cond_5c6

    :cond_562
    const/16 v23, 0x1

    .line 661
    .local v23, hasContactInformation:Z
    :goto_564
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableContactSection(Z)V

    .line 662
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearEmails()V

    .line 663
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearPhoneNumbers()V

    .line 664
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearAddresses()V

    .line 666
    if-eqz v24, :cond_5c9

    .line 667
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .restart local v34       #i$:Ljava/util/Iterator;
    :goto_57c
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5c9

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/TaggedEmail;

    .line 668
    .local v16, email:Lcom/google/api/services/plusi/model/TaggedEmail;
    const/16 v53, 0x0

    .line 670
    .local v53, type:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-nez v5, :cond_59c

    .line 671
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/google/api/services/plusi/model/TaggedEmail;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForEmailType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v53

    .line 674
    :cond_59c
    if-nez v53, :cond_5a7

    .line 675
    const v5, 0x7f08022b

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v53

    .line 678
    :cond_5a7
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedEmail;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addEmail(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_57c

    .line 651
    .end local v13           #contacts:Lcom/google/api/services/plusi/model/Contacts;
    .end local v16           #email:Lcom/google/api/services/plusi/model/TaggedEmail;
    .end local v23           #hasContactInformation:Z
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v53           #type:Ljava/lang/String;
    :cond_5b3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v13, v5, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    goto/16 :goto_530

    .line 654
    .restart local v13       #contacts:Lcom/google/api/services/plusi/model/Contacts;
    :cond_5bd
    const/16 v24, 0x0

    goto :goto_540

    .line 655
    :cond_5c0
    const/16 v31, 0x0

    goto :goto_54e

    .line 656
    :cond_5c3
    const/16 v20, 0x0

    goto :goto_55c

    .line 659
    :cond_5c6
    const/16 v23, 0x0

    goto :goto_564

    .line 682
    .restart local v23       #hasContactInformation:Z
    :cond_5c9
    if-eqz v31, :cond_617

    .line 683
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .restart local v34       #i$:Ljava/util/Iterator;
    :goto_5d1
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_617

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lcom/google/api/services/plusi/model/TaggedPhone;

    .line 686
    .local v43, phone:Lcom/google/api/services/plusi/model/TaggedPhone;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v5, :cond_60a

    .line 687
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v43

    iget-object v7, v0, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForPlusPagePhoneType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v53

    .line 692
    .restart local v53       #type:Ljava/lang/String;
    :goto_5ef
    if-nez v53, :cond_5fa

    .line 693
    const v5, 0x7f08022c

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v53

    .line 696
    :cond_5fa
    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedPhone;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsSmsIntentRegistered:Z

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v5, v1, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addPhoneNumber(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_5d1

    .line 689
    .end local v53           #type:Ljava/lang/String;
    :cond_60a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v43

    iget-object v7, v0, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForPhoneType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v53

    .restart local v53       #type:Ljava/lang/String;
    goto :goto_5ef

    .line 700
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v43           #phone:Lcom/google/api/services/plusi/model/TaggedPhone;
    .end local v53           #type:Ljava/lang/String;
    :cond_617
    if-eqz v20, :cond_652

    .line 701
    iget-object v5, v13, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .restart local v34       #i$:Ljava/util/Iterator;
    :goto_61f
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_652

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/TaggedAddress;

    .line 702
    .local v11, address:Lcom/google/api/services/plusi/model/TaggedAddress;
    const/16 v53, 0x0

    .line 704
    .restart local v53       #type:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-nez v5, :cond_63d

    .line 705
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    iget-object v7, v11, Lcom/google/api/services/plusi/model/TaggedAddress;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForAddress(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;

    move-result-object v53

    .line 708
    :cond_63d
    if-nez v53, :cond_648

    .line 709
    const v5, 0x7f08024f

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v53

    .line 712
    :cond_648
    iget-object v5, v11, Lcom/google/api/services/plusi/model/TaggedAddress;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addAddress(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_61f

    .line 716
    .end local v11           #address:Lcom/google/api/services/plusi/model/TaggedAddress;
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v53           #type:Ljava/lang/String;
    :cond_652
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateContactSectionDividers()V

    .line 718
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    if-eqz v5, :cond_727

    const-string v5, "UNKNOWN"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_727

    const/16 v25, 0x1

    .line 721
    .local v25, hasGender:Z
    :goto_669
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_72b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->birthday:Lcom/google/api/services/plusi/model/BirthdayField;

    if-eqz v5, :cond_72b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->birthday:Lcom/google/api/services/plusi/model/BirthdayField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/BirthdayField;->value:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_72b

    const/16 v21, 0x1

    .line 725
    .local v21, hasBirthday:Z
    :goto_68d
    if-nez v25, :cond_691

    if-eqz v21, :cond_72f

    :cond_691
    const/4 v5, 0x1

    :goto_692
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enablePersonalSection(Z)V

    .line 727
    if-eqz v25, :cond_749

    .line 728
    const-string v55, ""

    .line 729
    .local v55, value:Ljava/lang/String;
    const-string v5, "MALE"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_732

    .line 730
    const v5, 0x7f080245

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v55

    .line 734
    :cond_6b0
    :goto_6b0
    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setGender(Ljava/lang/String;)V

    .line 739
    .end local v55           #value:Ljava/lang/String;
    :goto_6b7
    if-eqz v21, :cond_751

    .line 740
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->birthday:Lcom/google/api/services/plusi/model/BirthdayField;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/BirthdayField;->value:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setBirthday(Ljava/lang/String;)V

    .line 745
    :goto_6c8
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updatePersonalSectionDividers()V

    .line 747
    const/16 v19, 0x0

    .line 748
    .local v19, employmentText:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_763

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    if-eqz v5, :cond_763

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    if-eqz v5, :cond_763

    .line 753
    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    .line 754
    .local v48, sb:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .restart local v34       #i$:Ljava/util/Iterator;
    :cond_6fe
    :goto_6fe
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_759

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/Employment;

    .line 755
    .restart local v18       #employment:Lcom/google/api/services/plusi/model/Employment;
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    if-eqz v5, :cond_6fe

    .line 756
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_71d

    .line 757
    const-string v5, "\n"

    move-object/from16 v0, v48

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 759
    :cond_71d
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    move-object/from16 v0, v48

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6fe

    .line 718
    .end local v18           #employment:Lcom/google/api/services/plusi/model/Employment;
    .end local v19           #employmentText:Ljava/lang/String;
    .end local v21           #hasBirthday:Z
    .end local v25           #hasGender:Z
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v48           #sb:Ljava/lang/StringBuilder;
    :cond_727
    const/16 v25, 0x0

    goto/16 :goto_669

    .line 721
    .restart local v25       #hasGender:Z
    :cond_72b
    const/16 v21, 0x0

    goto/16 :goto_68d

    .line 725
    .restart local v21       #hasBirthday:Z
    :cond_72f
    const/4 v5, 0x0

    goto/16 :goto_692

    .line 731
    .restart local v55       #value:Ljava/lang/String;
    :cond_732
    const-string v5, "FEMALE"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6b0

    .line 732
    const v5, 0x7f080246

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v55

    goto/16 :goto_6b0

    .line 736
    .end local v55           #value:Ljava/lang/String;
    :cond_749
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setGender(Ljava/lang/String;)V

    goto/16 :goto_6b7

    .line 742
    :cond_751
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setBirthday(Ljava/lang/String;)V

    goto/16 :goto_6c8

    .line 763
    .restart local v19       #employmentText:Ljava/lang/String;
    .restart local v34       #i$:Ljava/util/Iterator;
    .restart local v48       #sb:Ljava/lang/StringBuilder;
    :cond_759
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_763

    .line 764
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 768
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v48           #sb:Ljava/lang/StringBuilder;
    :cond_763
    const/4 v15, 0x0

    .line 769
    .local v15, educationText:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_7c4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    if-eqz v5, :cond_7c4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    if-eqz v5, :cond_7c4

    .line 774
    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    .line 775
    .restart local v48       #sb:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .restart local v34       #i$:Ljava/util/Iterator;
    :cond_795
    :goto_795
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7ba

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/api/services/plusi/model/Education;

    .line 776
    .restart local v14       #education:Lcom/google/api/services/plusi/model/Education;
    iget-object v5, v14, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    if-eqz v5, :cond_795

    .line 777
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_7b2

    .line 778
    const-string v5, "\n"

    move-object/from16 v0, v48

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 780
    :cond_7b2
    iget-object v5, v14, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    move-object/from16 v0, v48

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_795

    .line 784
    .end local v14           #education:Lcom/google/api/services/plusi/model/Education;
    :cond_7ba
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_7c4

    .line 785
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 789
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v48           #sb:Ljava/lang/StringBuilder;
    :cond_7c4
    if-nez v19, :cond_7c8

    if-eqz v15, :cond_893

    :cond_7c8
    const/4 v5, 0x1

    :goto_7c9
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableWorkEducationSection(Z)V

    .line 791
    if-eqz v19, :cond_896

    .line 792
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEmploymentLocations(Ljava/lang/String;)V

    .line 797
    :goto_7d7
    if-eqz v15, :cond_89e

    .line 798
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEducationLocations(Ljava/lang/String;)V

    .line 804
    :goto_7de
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateWorkEducationDividers()V

    .line 806
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v5, :cond_8a6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    if-eqz v5, :cond_8a6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_81f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    if-eqz v5, :cond_8a6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8a6

    :cond_81f
    const/16 v29, 0x1

    .line 812
    .local v29, hasLocations:Z
    :goto_821
    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLocationsSection(Z)V

    .line 814
    if-eqz v29, :cond_8aa

    .line 815
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mMapLocationBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLocationMap(Landroid/graphics/Bitmap;)V

    .line 817
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearLocations()V

    .line 819
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    if-eqz v5, :cond_85c

    .line 820
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 821
    .local v11, address:Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_85c

    .line 822
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLocation(Ljava/lang/String;Z)V

    .line 826
    .end local v11           #address:Ljava/lang/String;
    :cond_85c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    if-eqz v5, :cond_8aa

    .line 827
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .restart local v34       #i$:Ljava/util/Iterator;
    :cond_876
    :goto_876
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8aa

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 828
    .restart local v11       #address:Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 829
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_876

    .line 830
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLocation(Ljava/lang/String;Z)V

    goto :goto_876

    .line 789
    .end local v11           #address:Ljava/lang/String;
    .end local v29           #hasLocations:Z
    .end local v34           #i$:Ljava/util/Iterator;
    :cond_893
    const/4 v5, 0x0

    goto/16 :goto_7c9

    .line 794
    :cond_896
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEmploymentLocations(Ljava/lang/String;)V

    goto/16 :goto_7d7

    .line 801
    :cond_89e
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setEducationLocations(Ljava/lang/String;)V

    goto/16 :goto_7de

    .line 806
    :cond_8a6
    const/16 v29, 0x0

    goto/16 :goto_821

    .line 836
    .restart local v29       #hasLocations:Z
    :cond_8aa
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateLocationsSectionDividers()V

    .line 837
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableLinksSection(Z)V

    .line 838
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearLinks()V

    .line 840
    if-eqz v27, :cond_8fe

    .line 841
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->links:Lcom/google/api/services/plusi/model/Links;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Links;->link:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .restart local v34       #i$:Ljava/util/Iterator;
    :cond_8c7
    :goto_8c7
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8fe

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Lcom/google/api/services/plusi/model/ProfilesLink;

    .line 842
    .local v37, link:Lcom/google/api/services/plusi/model/ProfilesLink;
    move-object/from16 v0, v37

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->url:Ljava/lang/String;

    if-eqz v5, :cond_8c7

    .line 843
    const/16 v53, 0x0

    .line 844
    .restart local v53       #type:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v5, :cond_8ea

    .line 845
    const v5, 0x7f080250

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v53

    .line 847
    :cond_8ea
    move-object/from16 v0, v37

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->url:Ljava/lang/String;

    move-object/from16 v0, v37

    iget-object v7, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->label:Ljava/lang/String;

    move-object/from16 v0, v37

    iget-object v8, v0, Lcom/google/api/services/plusi/model/ProfilesLink;->faviconImgUrl:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-virtual {v0, v5, v7, v8, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addLink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8c7

    .line 852
    .end local v34           #i$:Ljava/util/Iterator;
    .end local v37           #link:Lcom/google/api/services/plusi/model/ProfilesLink;
    .end local v53           #type:Ljava/lang/String;
    :cond_8fe
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateLinksSectionDividers()V

    .line 854
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    if-eqz v5, :cond_912

    .line 855
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNames:Ljava/util/ArrayList;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCircles(Ljava/util/ArrayList;)V

    goto/16 :goto_6

    .line 856
    :cond_912
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    if-eqz v5, :cond_923

    .line 857
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showAddToCircles(Z)V

    goto/16 :goto_6

    .line 858
    :cond_923
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    if-eqz v5, :cond_92e

    .line 859
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showBlocked()V

    goto/16 :goto_6

    .line 860
    :cond_92e
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    if-eqz v5, :cond_939

    .line 861
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showProgress()V

    goto/16 :goto_6

    .line 863
    :cond_939
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showNone()V

    goto/16 :goto_6
.end method

.method private getString(I)Ljava/lang/String;
    .registers 3
    .parameter "resourceId"

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs getString(I[Ljava/lang/Object;)Ljava/lang/String;
    .registers 4
    .parameter "resourceId"
    .parameter "formatArgs"

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final beginBlockInProgress()V
    .registers 2

    .prologue
    .line 355
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mBlockRequestPending:Z

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->updateCircleList()V

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    .line 358
    return-void
.end method

.method public final bindStreamView(Landroid/view/View;Landroid/database/Cursor;)V
    .registers 8
    .parameter "view"
    .parameter "cursor"

    .prologue
    const/4 v4, 0x1

    .line 145
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    if-nez v1, :cond_7b

    .line 146
    const-string v1, "ProfileAdapter"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 147
    const-string v1, "ProfileAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bindView(); "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_24
    move-object v0, p1

    .line 149
    check-cast v0, Lcom/google/android/apps/plus/views/ProfileAboutView;

    .line 150
    .local v0, profileView:Lcom/google/android/apps/plus/views/ProfileAboutView;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewIsExpanded:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->init(Z)V

    .line 151
    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    invoke-direct {v1}, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;-><init>()V

    sget-object v2, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    packed-switch v2, :pswitch_data_80

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_77

    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showDetailsAlways:Z

    :goto_40
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    if-eqz v2, :cond_4a

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-nez v2, :cond_4a

    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showDetailsAlways:Z

    :cond_4a
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-eqz v2, :cond_54

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_54

    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->hideButtons:Z

    :cond_54
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setDisplayPolicies(Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;)V

    .line 152
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileLoadFailed:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mErrorText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->showError(ZLjava/lang/String;)V

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v1, :cond_6a

    .line 154
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->bindProfileAboutView(Lcom/google/android/apps/plus/views/ProfileAboutView;)V

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileViewOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V

    .line 160
    .end local v0           #profileView:Lcom/google/android/apps/plus/views/ProfileAboutView;
    :cond_6a
    :goto_6a
    return-void

    .line 151
    .restart local v0       #profileView:Lcom/google/android/apps/plus/views/ProfileAboutView;
    :pswitch_6b
    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_74

    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showDetailsAlways:Z

    goto :goto_40

    :cond_74
    iput-boolean v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showExpandButtonText:Z

    goto :goto_40

    :cond_77
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    goto :goto_40

    .line 158
    .end local v0           #profileView:Lcom/google/android/apps/plus/views/ProfileAboutView;
    :cond_7b
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->bindStreamView(Landroid/view/View;Landroid/database/Cursor;)V

    goto :goto_6a

    .line 151
    nop

    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_6b
    .end packed-switch
.end method

.method public final endBlockInProgress(Z)V
    .registers 4
    .parameter "success"

    .prologue
    const/4 v0, 0x0

    .line 361
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mBlockRequestPending:Z

    .line 362
    if-eqz p1, :cond_c

    .line 365
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    if-nez v1, :cond_a

    const/4 v0, 0x1

    :cond_a
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    .line 367
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->updateCircleList()V

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    .line 369
    return-void
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 178
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public final getFullName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGender()Ljava/lang/String;
    .registers 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    return-object v0
.end method

.method public final getGivenName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGivenName:Ljava/lang/String;

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 183
    if-nez p1, :cond_5

    .line 184
    const-wide/16 v0, 0x0

    .line 186
    :goto_4
    return-wide v0

    :cond_5
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_4
.end method

.method public final getItemViewType(I)I
    .registers 3
    .parameter "pos"

    .prologue
    .line 164
    if-nez p1, :cond_c

    .line 165
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    .line 167
    :goto_8
    return v0

    .line 165
    :cond_9
    const/16 v0, 0x9

    goto :goto_8

    .line 167
    :cond_c
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_8
.end method

.method public final getViewIsExpanded()Z
    .registers 2

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewIsExpanded:Z

    return v0
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 173
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final init(Ljava/lang/String;ZZZLcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .registers 7
    .parameter "personId"
    .parameter "isMyProfile"
    .parameter "hasProfile"
    .parameter "isSmsIntentRegistered"
    .parameter "circleNameResolver"

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    .line 112
    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    .line 113
    iput-boolean p3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mHasProfile:Z

    .line 114
    iput-boolean p4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsSmsIntentRegistered:Z

    .line 115
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewingAsPlusPage:Z

    .line 117
    return-void
.end method

.method public final isBlocked()Z
    .registers 2

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    return v0
.end method

.method public final isMuted()Z
    .registers 2

    .prologue
    .line 416
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMuted:Z

    return v0
.end method

.method public final isPlusOnedByMe()Z
    .registers 2

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOneByMe:Z

    return v0
.end method

.method public final isPlusPage()Z
    .registers 2

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    return v0
.end method

.method public final newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    const/4 v5, -0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 125
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    if-nez v2, :cond_7a

    .line 126
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 128
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v2, 0x7f030094

    const/4 v6, 0x0

    invoke-virtual {v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ProfileAboutView;

    .line 130
    .local v1, view:Lcom/google/android/apps/plus/views/ProfileAboutView;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_74

    move v2, v3

    :goto_20
    sget-object v6, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v6, v6, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    packed-switch v6, :pswitch_data_80

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-eqz v4, :cond_78

    sget-object v4, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v4, v4, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    int-to-double v4, v4

    const-wide v6, 0x3fe6666666666666L

    mul-double/2addr v4, v6

    double-to-int v4, v4

    :goto_37
    move v5, v4

    move v4, v3

    :goto_39
    new-instance v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-direct {v6, v2, v5, v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mLandscape:Z

    if-nez v2, :cond_45

    const/4 v2, -0x2

    iput v2, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    :cond_45
    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_56

    .line 132
    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 134
    :cond_56
    const-string v2, "ProfileAdapter"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_73

    .line 135
    const-string v2, "ProfileAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "newView() -> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    .end local v0           #inflater:Landroid/view/LayoutInflater;
    .end local v1           #view:Lcom/google/android/apps/plus/views/ProfileAboutView;
    :cond_73
    :goto_73
    return-object v1

    .restart local v0       #inflater:Landroid/view/LayoutInflater;
    .restart local v1       #view:Lcom/google/android/apps/plus/views/ProfileAboutView;
    :cond_74
    move v2, v4

    .line 130
    goto :goto_20

    :pswitch_76
    move v3, v4

    goto :goto_39

    :cond_78
    move v4, v5

    goto :goto_37

    .line 139
    .end local v0           #inflater:Landroid/view/LayoutInflater;
    .end local v1           #view:Lcom/google/android/apps/plus/views/ProfileAboutView;
    :cond_7a
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_73

    .line 130
    nop

    :pswitch_data_80
    .packed-switch 0x1
        :pswitch_76
    .end packed-switch
.end method

.method public final setLocationMapBitmap(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter "bitmap"

    .prologue
    .line 439
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mMapLocationBitmap:Landroid/graphics/Bitmap;

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    if-eqz v0, :cond_9

    .line 441
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    .line 443
    :cond_9
    return-void
.end method

.method public final setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileViewOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    .line 121
    return-void
.end method

.method public final setProfileData(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V
    .registers 10
    .parameter "data"

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 276
    if-nez p1, :cond_6

    .line 349
    :cond_5
    :goto_5
    return-void

    .line 280
    :cond_6
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    .line 281
    iget-object v3, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    .line 285
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v3, :cond_60

    .line 286
    new-instance v3, Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/SimpleProfile;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    .line 287
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    const-string v6, "USER"

    iput-object v6, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    .line 289
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    const-string v6, "e:"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e3

    .line 290
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    new-instance v6, Lcom/google/api/services/plusi/model/CommonContent;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/CommonContent;-><init>()V

    iput-object v6, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    .line 291
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    new-instance v6, Lcom/google/api/services/plusi/model/Contacts;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/Contacts;-><init>()V

    iput-object v6, v3, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    .line 292
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v3, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    .line 293
    new-instance v0, Lcom/google/api/services/plusi/model/TaggedEmail;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/TaggedEmail;-><init>()V

    .line 294
    .local v0, email:Lcom/google/api/services/plusi/model/TaggedEmail;
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/TaggedEmail;->value:Ljava/lang/String;

    .line 295
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    .end local v0           #email:Lcom/google/api/services/plusi/model/TaggedEmail;
    :cond_60
    :goto_60
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    if-eqz v3, :cond_96

    .line 307
    const-string v3, "USER"

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_128

    .line 308
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v3, :cond_94

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/User;->name:Lcom/google/api/services/plusi/model/Name;

    if-eqz v3, :cond_94

    .line 309
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/User;->name:Lcom/google/api/services/plusi/model/Name;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Name;->given:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGivenName:Ljava/lang/String;

    .line 310
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/User;->name:Lcom/google/api/services/plusi/model/Name;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Name;->family:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFamilyName:Ljava/lang/String;

    .line 312
    :cond_94
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    .line 325
    :cond_96
    :goto_96
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->displayName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_19b

    const v3, 0x7f0801dc

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_a7
    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mFullName:Ljava/lang/String;

    .line 327
    iget-boolean v3, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->blocked:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    .line 328
    iget-object v3, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->packedCircleIds:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPackedCircleIds:Ljava/lang/String;

    .line 329
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    if-eqz v3, :cond_1a1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    if-eqz v3, :cond_1a1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SocialGraphData;->muted:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_1a1

    :goto_cd
    iput-boolean v5, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMuted:Z

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->updateCircleList()V

    .line 334
    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-eqz v3, :cond_1a4

    .line 335
    const-string v3, "OTHER"

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    .line 346
    :goto_da
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    if-eqz v3, :cond_5

    .line 347
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    goto/16 :goto_5

    .line 296
    :cond_e3
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    const-string v6, "p:"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_60

    .line 297
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    new-instance v6, Lcom/google/api/services/plusi/model/CommonContent;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/CommonContent;-><init>()V

    iput-object v6, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    .line 298
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    new-instance v6, Lcom/google/api/services/plusi/model/Contacts;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/Contacts;-><init>()V

    iput-object v6, v3, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    .line 299
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v3, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    .line 300
    new-instance v1, Lcom/google/api/services/plusi/model/TaggedPhone;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/TaggedPhone;-><init>()V

    .line 301
    .local v1, phone:Lcom/google/api/services/plusi/model/TaggedPhone;
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPersonId:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/api/services/plusi/model/TaggedPhone;->value:Ljava/lang/String;

    .line 302
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_60

    .line 313
    .end local v1           #phone:Lcom/google/api/services/plusi/model/TaggedPhone;
    :cond_128
    const-string v3, "PLUSPAGE"

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_96

    .line 314
    iput-boolean v5, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    .line 315
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v3, :cond_169

    move v3, v4

    :goto_13b
    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    .line 316
    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsLocalPlusPage:Z

    if-eqz v3, :cond_151

    .line 317
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    const-string v6, "UNCLAIMED"

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/LocalEntityInfo;->type:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    .line 319
    :cond_151
    iget-object v3, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v2, v3, Lcom/google/api/services/plusi/model/Page;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    .line 320
    .local v2, plusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;
    iget-object v3, v2, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOnes:I

    .line 321
    iget-object v3, v2, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPlusOneByMe:Z

    goto/16 :goto_96

    .line 315
    .end local v2           #plusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;
    :cond_169
    const-string v6, "PLUSPAGE"

    iget-object v7, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_175

    move v3, v4

    goto :goto_13b

    :cond_175
    iget-object v6, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    if-nez v6, :cond_17b

    move v3, v4

    goto :goto_13b

    :cond_17b
    const-string v6, "LOCAL"

    iget-object v7, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_189

    move v3, v4

    goto :goto_13b

    :cond_189
    iget-object v6, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    if-eqz v6, :cond_197

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    if-nez v3, :cond_199

    :cond_197
    move v3, v4

    goto :goto_13b

    :cond_199
    move v3, v5

    goto :goto_13b

    .line 325
    :cond_19b
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->displayName:Ljava/lang/String;

    goto/16 :goto_a7

    :cond_1a1
    move v5, v4

    .line 329
    goto/16 :goto_cd

    .line 337
    :cond_1a4
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v3, :cond_1c8

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/User;->gender:Lcom/google/api/services/plusi/model/Gender;

    if-eqz v3, :cond_1c8

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/User;->gender:Lcom/google/api/services/plusi/model/Gender;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Gender;->value:Ljava/lang/String;

    if-eqz v3, :cond_1c8

    .line 340
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/User;->gender:Lcom/google/api/services/plusi/model/Gender;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Gender;->value:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    goto/16 :goto_da

    .line 342
    :cond_1c8
    const-string v3, "UNKNOWN"

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mGender:Ljava/lang/String;

    goto/16 :goto_da
.end method

.method public final setViewIsExpanded(Z)V
    .registers 2
    .parameter "isExpanded"

    .prologue
    .line 376
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewIsExpanded:Z

    .line 377
    return-void
.end method

.method public final showError(Ljava/lang/String;)V
    .registers 3
    .parameter "errorText"

    .prologue
    .line 380
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfileLoadFailed:Z

    .line 381
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mErrorText:Ljava/lang/String;

    .line 382
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    .line 383
    return-void
.end method

.method public final updateCircleList()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 386
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsMyProfile:Z

    if-nez v2, :cond_16

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsUnclaimedLocalPlusPage:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v2

    if-nez v2, :cond_1f

    .line 390
    :cond_16
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    .line 405
    :goto_1e
    return-void

    .line 391
    :cond_1f
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mBlockRequestPending:Z

    if-eqz v2, :cond_2c

    .line 392
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    .line 393
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    goto :goto_1e

    .line 394
    :cond_2c
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsBlocked:Z

    if-eqz v2, :cond_39

    .line 395
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    .line 396
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    goto :goto_1e

    .line 397
    :cond_39
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPackedCircleIds:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_53

    .line 398
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    .line 399
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mIsPlusPage:Z

    if-nez v2, :cond_4f

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mViewingAsPlusPage:Z

    if-nez v2, :cond_50

    :cond_4f
    move v0, v1

    :cond_50
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    goto :goto_1e

    .line 401
    :cond_53
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowBlocked:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowProgress:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowAddToCircles:Z

    .line 402
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mShowCircles:Z

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mPackedCircleIds:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNameListForPackedIds(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->mCircleNames:Ljava/util/ArrayList;

    goto :goto_1e
.end method
