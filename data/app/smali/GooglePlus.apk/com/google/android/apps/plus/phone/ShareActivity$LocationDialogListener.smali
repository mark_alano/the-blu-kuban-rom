.class final Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;
.super Ljava/lang/Object;
.source "ShareActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/ShareActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/ShareActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/phone/ShareActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 447
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;->this$0:Lcom/google/android/apps/plus/phone/ShareActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/phone/ShareActivity;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 447
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 450
    packed-switch p2, :pswitch_data_22

    .line 471
    :goto_3
    return-void

    .line 453
    :pswitch_4
    invoke-static {}, Lcom/google/android/apps/plus/phone/Intents;->getLocationSettingActivityIntent()Landroid/content/Intent;

    move-result-object v0

    .line 454
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;->this$0:Lcom/google/android/apps/plus/phone/ShareActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 460
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_e
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;->this$0:Lcom/google/android/apps/plus/phone/ShareActivity;

    iget-object v1, v1, Lcom/google/android/apps/plus/phone/ShareActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    goto :goto_3

    .line 466
    :pswitch_17
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;->this$0:Lcom/google/android/apps/plus/phone/ShareActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;->this$0:Lcom/google/android/apps/plus/phone/ShareActivity;

    iget-object v2, v2, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLocationDialogSeenPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    goto :goto_3

    .line 450
    :pswitch_data_22
    .packed-switch -0x3
        :pswitch_17
        :pswitch_e
        :pswitch_4
    .end packed-switch
.end method
