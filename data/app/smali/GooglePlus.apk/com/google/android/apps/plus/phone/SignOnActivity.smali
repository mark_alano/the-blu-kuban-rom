.class public Lcom/google/android/apps/plus/phone/SignOnActivity;
.super Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.source "SignOnActivity.java"


# instance fields
.field private mCallingActivity:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;-><init>()V

    return-void
.end method

.method public static finishIfNoAccount(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .registers 8
    .parameter "activity"
    .parameter "account"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 217
    if-eqz p1, :cond_47

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_47

    .line 218
    invoke-virtual {p0, v3}, Landroid/app/Activity;->setResult(I)V

    .line 220
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "from_signup"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 222
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 223
    .local v0, resultIntent:Landroid/content/Intent;
    const-string v4, "no_account"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 224
    invoke-virtual {p0, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 225
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 241
    .end local v0           #resultIntent:Landroid/content/Intent;
    :goto_2d
    return v2

    .line 229
    :cond_2e
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "intent"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 230
    .local v1, target:Landroid/content/Intent;
    if-eqz v1, :cond_43

    .line 231
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 232
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_2d

    .line 236
    :cond_43
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_2d

    .end local v1           #target:Landroid/content/Intent;
    :cond_47
    move v2, v3

    .line 241
    goto :goto_2d
.end method

.method private fireIntent(I)V
    .registers 5
    .parameter "resultCode"

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_16

    const/4 v1, 0x0

    :goto_9
    invoke-static {p0, v2, v1}, Lcom/google/android/apps/plus/phone/Intents;->getTargetIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 182
    .local v0, intent:Landroid/content/Intent;
    if-nez v0, :cond_1d

    .line 183
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->setResult(I)V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    .line 204
    :goto_15
    return-void

    .line 180
    .end local v0           #intent:Landroid/content/Intent;
    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_9

    .line 186
    .restart local v0       #intent:Landroid/content/Intent;
    :cond_1d
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_28

    .line 194
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->startActivity(Landroid/content/Intent;)V

    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    goto :goto_15

    .line 200
    :cond_28
    const-string v1, "from_signup"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 201
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x2000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 202
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_15
.end method

.method private recordEvent$7c4c9d3f(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 13
    .parameter "account"

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    .line 119
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    if-nez v2, :cond_32

    move-object v3, v1

    .line 120
    .local v3, pkg:Ljava/lang/String;
    :goto_9
    if-eqz v3, :cond_28

    .line 122
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    move-object v2, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .local v0, sourceInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    new-instance v4, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, v0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    .line 125
    .local v4, info:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    invoke-static {v4}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    .line 127
    .end local v0           #sourceInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    .end local v4           #info:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    :cond_28
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 129
    return-void

    .line 119
    .end local v3           #pkg:Ljava/lang/String;
    :cond_32
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    goto :goto_9
.end method


# virtual methods
.method protected final getUpgradeOrigin()Ljava/lang/String;
    .registers 5

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getTargetIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 87
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, name:Ljava/lang/String;
    const-class v2, Lcom/google/android/apps/plus/phone/PlusOneActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    const-string v2, "PLUS_ONE"

    :goto_1f
    return-object v2

    :cond_20
    const-string v2, "DEFAULT"

    goto :goto_1f
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 250
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V
    .registers 6
    .parameter "oobResponse"
    .parameter "account"
    .parameter "settings"

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getUpgradeOrigin()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p2, p1, p3, v1}, Lcom/google/android/apps/plus/phone/Intents;->getOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 103
    .local v0, nextIntent:Landroid/content/Intent;
    if-eqz p2, :cond_16

    .line 104
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->recordEvent$7c4c9d3f(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 105
    if-eqz p1, :cond_16

    .line 106
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->recordEvent$7c4c9d3f(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 110
    :cond_16
    if-eqz v0, :cond_1e

    .line 111
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 115
    :goto_1d
    return-void

    .line 113
    :cond_1e
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_1d
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 7
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 136
    packed-switch p1, :pswitch_data_3c

    .line 167
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 171
    :goto_6
    return-void

    .line 138
    :pswitch_7
    const/4 v1, -0x1

    if-ne p2, v1, :cond_e

    .line 139
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_6

    .line 142
    :cond_e
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->setResult(I)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    goto :goto_6

    .line 149
    :pswitch_15
    if-eqz p3, :cond_34

    const-string v1, "no_account"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 151
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 152
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_2c

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v1

    if-nez v1, :cond_30

    .line 153
    :cond_2c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->showAccountList()V

    goto :goto_6

    .line 155
    :cond_30
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_6

    .line 160
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_34
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/plus/phone/SignOnActivity;->setResult(ILandroid/content/Intent;)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finish()V

    goto :goto_6

    .line 136
    nop

    :pswitch_data_3c
    .packed-switch 0xa
        :pswitch_7
        :pswitch_15
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    if-eqz p1, :cond_1f

    .line 57
    const-string v1, "SignOnActivity#callingActivity"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    .line 62
    :goto_f
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 64
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_1b

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v1

    if-nez v1, :cond_26

    .line 66
    :cond_1b
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->showAccountSelectionOrUpgradeAccount(Landroid/os/Bundle;)V

    .line 72
    :cond_1e
    :goto_1e
    return-void

    .line 59
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    goto :goto_f

    .line 68
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_26
    if-nez p1, :cond_1e

    .line 69
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->fireIntent(I)V

    goto :goto_1e
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    const-string v0, "SignOnActivity#callingActivity"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnActivity;->mCallingActivity:Landroid/content/ComponentName;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 82
    return-void
.end method
