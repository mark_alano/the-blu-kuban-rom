.class final Lcom/google/android/apps/plus/phone/StreamAdapter$1;
.super Ljava/lang/Object;
.source "StreamAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/StreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/StreamAdapter;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Lcom/google/android/apps/plus/views/ColumnGridView;IIIII)V
    .registers 25
    .parameter "view"
    .parameter "firstItem"
    .parameter "visibleOffset"
    .parameter "viewItemCount"
    .parameter "totalItemCount"
    .parameter "scrollDelta"

    .prologue
    .line 217
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    #getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v1

    if-eqz v1, :cond_21

    .line 218
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    #getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v1

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/plus/phone/ComposeBarController;->onScroll(Lcom/google/android/apps/plus/views/ColumnGridView;IIIII)V

    .line 222
    :cond_21
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_29

    if-nez p4, :cond_2a

    .line 287
    :cond_29
    :goto_29
    return-void

    .line 226
    :cond_2a
    const/high16 v11, -0x8000

    .line 227
    .local v11, highestIndexSeen:I
    const/16 v10, 0x32

    .line 229
    .local v10, delay:I
    const/4 v12, 0x0

    .local v12, i:I
    :goto_2f
    move/from16 v0, p4

    if-ge v12, v0, :cond_f4

    .line 230
    add-int v13, p2, v12

    .line 231
    .local v13, index:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    #getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$100(Lcom/google/android/apps/plus/phone/StreamAdapter;)I

    move-result v1

    if-le v13, v1, :cond_e1

    .line 232
    invoke-static {v11, v13}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 234
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 235
    .local v9, childView:Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f090069

    if-eq v1, v2, :cond_e1

    .line 236
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v15

    .line 240
    .local v15, screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_e5

    const/4 v14, 0x1

    .line 243
    .local v14, landscape:Z
    :goto_6c
    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v1

    float-to-int v0, v1

    move/from16 v16, v0

    .line 244
    .local v16, x:I
    invoke-virtual {v9}, Landroid/view/View;->getTranslationY()F

    move-result v1

    float-to-int v0, v1

    move/from16 v17, v0

    .line 245
    .local v17, y:I
    if-eqz v14, :cond_e7

    iget v1, v15, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    div-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    :goto_81
    invoke-virtual {v9, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 246
    if-eqz v14, :cond_e9

    const/4 v1, 0x0

    :goto_87
    invoke-virtual {v9, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 247
    if-eqz v14, :cond_ef

    const/4 v1, 0x0

    :goto_8d
    invoke-virtual {v9, v1}, Landroid/view/View;->setRotationX(F)V

    .line 248
    if-eqz v14, :cond_f2

    const/high16 v1, -0x3ee0

    :goto_94
    invoke-virtual {v9, v1}, Landroid/view/View;->setRotationY(F)V

    .line 250
    invoke-virtual {v9}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->rotationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    move/from16 v0, v16

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    move/from16 v0, v17

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$200()Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    .line 254
    .local v8, anim:Landroid/view/ViewPropertyAnimator;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_d1

    .line 255
    new-instance v1, Lcom/google/android/apps/plus/phone/StreamAdapter$1$1;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v9}, Lcom/google/android/apps/plus/phone/StreamAdapter$1$1;-><init>(Lcom/google/android/apps/plus/phone/StreamAdapter$1;Landroid/view/View;)V

    invoke-virtual {v8, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 279
    :cond_d1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_df

    .line 280
    int-to-long v1, v10

    invoke-virtual {v8, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 282
    :cond_df
    add-int/lit8 v10, v10, 0x32

    .line 229
    .end local v8           #anim:Landroid/view/ViewPropertyAnimator;
    .end local v9           #childView:Landroid/view/View;
    .end local v14           #landscape:Z
    .end local v15           #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    .end local v16           #x:I
    .end local v17           #y:I
    :cond_e1
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2f

    .line 240
    .restart local v9       #childView:Landroid/view/View;
    .restart local v15       #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    :cond_e5
    const/4 v14, 0x0

    goto :goto_6c

    .line 245
    .restart local v14       #landscape:Z
    .restart local v16       #x:I
    .restart local v17       #y:I
    :cond_e7
    const/4 v1, 0x0

    goto :goto_81

    .line 246
    :cond_e9
    iget v1, v15, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    div-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    goto :goto_87

    .line 247
    :cond_ef
    const/high16 v1, 0x4120

    goto :goto_8d

    .line 248
    :cond_f2
    const/4 v1, 0x0

    goto :goto_94

    .line 286
    .end local v9           #childView:Landroid/view/View;
    .end local v13           #index:I
    .end local v14           #landscape:Z
    .end local v15           #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    .end local v16           #x:I
    .end local v17           #y:I
    :cond_f4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    #getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$100(Lcom/google/android/apps/plus/phone/StreamAdapter;)I

    move-result v2

    invoke-static {v2, v11}, Ljava/lang/Math;->max(II)I

    move-result v2

    #setter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$102(Lcom/google/android/apps/plus/phone/StreamAdapter;I)I

    goto/16 :goto_29
.end method

.method public final onScrollStateChanged(Lcom/google/android/apps/plus/views/ColumnGridView;I)V
    .registers 4
    .parameter "view"
    .parameter "scrollState"

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    #getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    #getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/ComposeBarController;->onScrollStateChanged(Lcom/google/android/apps/plus/views/ColumnGridView;I)V

    .line 212
    :cond_11
    return-void
.end method
