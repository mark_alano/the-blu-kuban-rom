.class public Lcom/google/android/apps/plus/phone/StreamAdapter;
.super Lcom/android/common/widget/CompositeCursorAdapter;
.source "StreamAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/TranslationAdapter$TranslationListAdapter;
.implements Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;,
        Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    }
.end annotation


# static fields
.field private static sDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field protected static sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mBoxLayout:[[I

.field private mCardTypes:[I

.field private mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

.field private mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field protected mLandscape:Z

.field private mMarkPostsAsRead:Z

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mStreamMediaClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;

.field private mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

.field private mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

.field private final mViewerHasReadPosts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibleIndex:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 68
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)V
    .registers 14
    .parameter "context"
    .parameter "gridView"
    .parameter "account"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewUseListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"
    .parameter "floatingComposeBarView"

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    invoke-direct {p0, p1}, Lcom/android/common/widget/CompositeCursorAdapter;-><init>(Landroid/content/Context;)V

    .line 84
    const/high16 v0, -0x8000

    iput v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    .line 173
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->addPartition(ZZ)V

    .line 174
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->addPartition(ZZ)V

    .line 176
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 177
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 178
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 179
    iput-object p7, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    .line 180
    iput-object p8, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;

    .line 181
    iput-object p6, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

    .line 182
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    .line 183
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mMarkPostsAsRead:Z

    .line 185
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_8f

    move v0, v1

    :goto_32
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    .line 188
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-nez v0, :cond_3e

    .line 189
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    .line 192
    :cond_3e
    if-eqz p9, :cond_53

    .line 193
    invoke-virtual {p9, v2}, Landroid/view/View;->setVisibility(I)V

    .line 194
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-direct {p0, p9, v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setComposeBarPadding(Landroid/view/View;I)V

    .line 195
    new-instance v0, Lcom/google/android/apps/plus/phone/ComposeBarController;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    invoke-direct {v0, p9, v2}, Lcom/google/android/apps/plus/phone/ComposeBarController;-><init>(Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    .line 198
    :cond_53
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v0, :cond_91

    move v0, v1

    :goto_58
    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    .line 200
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v0, :cond_93

    :goto_61
    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    .line 202
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    .line 203
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v1, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v2, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v3, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    .line 206
    new-instance v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/phone/StreamAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/StreamAdapter;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    .line 290
    new-instance v0, Lcom/google/android/apps/plus/phone/StreamAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/StreamAdapter$2;-><init>(Lcom/google/android/apps/plus/phone/StreamAdapter;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    .line 298
    return-void

    :cond_8f
    move v0, v2

    .line 185
    goto :goto_32

    :cond_91
    move v0, v3

    .line 198
    goto :goto_58

    :cond_93
    move v1, v3

    .line 200
    goto :goto_61
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;
    .registers 2
    .parameter "x0"

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/StreamAdapter;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/phone/StreamAdapter;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    return p1
.end method

.method static synthetic access$200()Landroid/view/animation/Interpolator;
    .registers 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method public static getScreenDisplayType()I
    .registers 1

    .prologue
    .line 718
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    return v0
.end method

.method private isBoxStart(I)Z
    .registers 10
    .parameter "position"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 318
    const/4 v4, -0x1

    .line 319
    .local v4, row:I
    const/4 v1, -0x1

    .line 320
    .local v1, col:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getColumnCount()I

    move-result v7

    if-ge v2, v7, :cond_27

    .line 321
    const/4 v3, 0x0

    .local v3, j:I
    :goto_c
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    aget-object v7, v7, v2

    array-length v7, v7

    if-ge v3, v7, :cond_1d

    .line 322
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    aget-object v7, v7, v2

    aget v0, v7, v3

    .line 323
    .local v0, boxPos:I
    if-ne v0, p1, :cond_22

    .line 324
    move v4, v2

    .line 325
    move v1, v3

    .line 332
    .end local v0           #boxPos:I
    :cond_1d
    if-gez v1, :cond_27

    .line 333
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 328
    .restart local v0       #boxPos:I
    :cond_22
    if-gt v0, p1, :cond_1d

    .line 329
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 337
    .end local v0           #boxPos:I
    .end local v3           #j:I
    :cond_27
    iget-boolean v7, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v7, :cond_32

    rem-int/lit8 v7, v1, 0x2

    if-nez v7, :cond_30

    :cond_2f
    :goto_2f
    return v5

    :cond_30
    move v5, v6

    goto :goto_2f

    :cond_32
    if-eqz v4, :cond_2f

    move v5, v6

    goto :goto_2f
.end method

.method private setComposeBarPadding(Landroid/view/View;I)V
    .registers 9
    .parameter "composeBarView"
    .parameter "extraPadding"

    .prologue
    const v5, 0x7f09006c

    const v4, 0x7f09006b

    const v3, 0x7f09006a

    const/4 v2, 0x0

    .line 731
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v1, :cond_2a

    .line 732
    sget-object v1, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    add-int v0, v1, p2

    .line 733
    .local v0, padding:I
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 734
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 735
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 742
    :goto_29
    return-void

    .line 737
    .end local v0           #padding:I
    :cond_2a
    const/4 v0, 0x0

    .line 738
    .restart local v0       #padding:I
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, p2, v2, p2}, Landroid/view/View;->setPadding(IIII)V

    .line 739
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, p2, v2, p2}, Landroid/view/View;->setPadding(IIII)V

    .line 740
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, p2, v2, p2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_29
.end method


# virtual methods
.method public bindStreamView(Landroid/view/View;Landroid/database/Cursor;)V
    .registers 19
    .parameter "view"
    .parameter "cursor"

    .prologue
    .line 370
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v14

    .line 371
    .local v14, position:I
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount(I)I

    move-result v2

    add-int v15, v14, v2

    .line 372
    .local v15, realPosition:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    aget v3, v2, v15

    .local v3, displaySizeType:I
    move-object/from16 v1, p1

    .line 373
    check-cast v1, Lcom/google/android/apps/plus/views/StreamCardView;

    .line 375
    .local v1, cardView:Lcom/google/android/apps/plus/views/StreamCardView;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_27

    const/4 v13, 0x1

    .line 379
    .local v13, orientation:I
    :goto_1e
    packed-switch v3, :pswitch_data_a6

    .line 405
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    .line 375
    .end local v13           #orientation:I
    :cond_27
    const/4 v13, 0x2

    goto :goto_1e

    .line 381
    .restart local v13       #orientation:I
    :pswitch_29
    const/4 v12, 0x1

    .line 382
    .local v12, minorSpan:I
    const/4 v11, 0x1

    .line 409
    .local v11, majorSpan:I
    :goto_2b
    new-instance v10, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v2, -0x3

    invoke-direct {v10, v13, v2, v12, v11}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    .line 411
    .local v10, lp:Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/plus/phone/StreamAdapter;->isBoxStart(I)Z

    move-result v2

    iput-boolean v2, v10, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    .line 413
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-nez v2, :cond_54

    sget-object v2, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v2, :cond_54

    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/apps/plus/views/TextCardView;

    if-nez v2, :cond_51

    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/apps/plus/views/EventStreamCardView;

    if-eqz v2, :cond_54

    .line 415
    :cond_51
    const/4 v2, -0x2

    iput v2, v10, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    .line 418
    :cond_54
    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/views/StreamCardView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 419
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;

    move-object/from16 v2, p2

    move-object/from16 v7, p0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/plus/views/StreamCardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 422
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

    if-eqz v2, :cond_7c

    .line 423
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

    invoke-interface {v2, v15}, Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;->onViewUsed(I)V

    .line 425
    :cond_7c
    return-void

    .line 387
    .end local v10           #lp:Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    .end local v11           #majorSpan:I
    .end local v12           #minorSpan:I
    :pswitch_7d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_8c

    const/4 v12, 0x2

    .line 388
    .restart local v12       #minorSpan:I
    :goto_84
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_8e

    const/4 v11, 0x1

    .line 389
    .restart local v11       #majorSpan:I
    :goto_8b
    goto :goto_2b

    .line 387
    .end local v11           #majorSpan:I
    .end local v12           #minorSpan:I
    :cond_8c
    const/4 v12, 0x1

    goto :goto_84

    .line 388
    .restart local v12       #minorSpan:I
    :cond_8e
    const/4 v11, 0x2

    goto :goto_8b

    .line 393
    .end local v12           #minorSpan:I
    :pswitch_90
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_9f

    const/4 v12, 0x1

    .line 394
    .restart local v12       #minorSpan:I
    :goto_97
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v2, :cond_a1

    const/4 v11, 0x2

    .line 395
    .restart local v11       #majorSpan:I
    :goto_9e
    goto :goto_2b

    .line 393
    .end local v11           #majorSpan:I
    .end local v12           #minorSpan:I
    :cond_9f
    const/4 v12, 0x2

    goto :goto_97

    .line 394
    .restart local v12       #minorSpan:I
    :cond_a1
    const/4 v11, 0x1

    goto :goto_9e

    .line 399
    .end local v12           #minorSpan:I
    :pswitch_a3
    const/4 v12, 0x2

    .line 400
    .restart local v12       #minorSpan:I
    const/4 v11, 0x2

    .line 401
    .restart local v11       #majorSpan:I
    goto :goto_2b

    .line 379
    :pswitch_data_a6
    .packed-switch 0x0
        :pswitch_29
        :pswitch_7d
        :pswitch_90
        :pswitch_a3
    .end packed-switch
.end method

.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;I)V
    .registers 9
    .parameter "view"
    .parameter "partition"
    .parameter "cursor"
    .parameter "position"

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 342
    packed-switch p2, :pswitch_data_48

    .line 353
    :goto_5
    return-void

    .line 344
    :pswitch_6
    const v0, 0x7f09006a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09006b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09006c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    if-eqz v0, :cond_3f

    move v0, v1

    :goto_2f
    sget-object v3, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v3, :cond_41

    :goto_35
    new-instance v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v0, v3, v1, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    :cond_3f
    move v0, v2

    goto :goto_2f

    :cond_41
    move v1, v2

    goto :goto_35

    .line 349
    :pswitch_43
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->bindStreamView(Landroid/view/View;Landroid/database/Cursor;)V

    goto :goto_5

    .line 342
    nop

    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_6
        :pswitch_43
    .end packed-switch
.end method

.method public final changeComposeBarCursor(Landroid/database/Cursor;)V
    .registers 3
    .parameter "cursor"

    .prologue
    .line 478
    const/4 v0, 0x0

    invoke-super {p0, v0, p1}, Lcom/android/common/widget/CompositeCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 479
    return-void
.end method

.method public final changeStreamCursor(Landroid/database/Cursor;)V
    .registers 15
    .parameter "cursor"

    .prologue
    .line 482
    const/4 v0, 0x1

    invoke-super {p0, v0, p1}, Lcom/android/common/widget/CompositeCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 483
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v4

    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getColumnCount()I

    move-result v0

    mul-int/lit8 v1, v4, 0x2

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getColumnCount()I

    move-result v2

    move v1, v0

    :goto_26
    if-ge v1, v2, :cond_3b

    const/4 v0, 0x0

    mul-int/lit8 v3, v4, 0x2

    :goto_2b
    if-ge v0, v3, :cond_37

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    aget-object v5, v5, v1

    const/4 v6, -0x1

    aput v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2b

    :cond_37
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_26

    :cond_3b
    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_8d

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_8d

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    :goto_4e
    if-ge v1, v2, :cond_8d

    sget-object v3, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v3, :cond_65

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    const/4 v5, 0x0

    aput v5, v3, v1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x0

    aget-object v3, v3, v5

    aput v1, v3, v1

    :goto_62
    add-int/lit8 v1, v1, 0x1

    goto :goto_4e

    :cond_65
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    const/4 v5, 0x3

    aput v5, v3, v1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x0

    aget-object v3, v3, v5

    aput v1, v3, v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x0

    aget-object v3, v3, v5

    add-int/lit8 v5, v0, 0x1

    aput v1, v3, v5

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x1

    aget-object v3, v3, v5

    aput v1, v3, v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x1

    aget-object v3, v3, v5

    add-int/lit8 v5, v0, 0x1

    aput v1, v3, v5

    add-int/lit8 v0, v0, 0x2

    goto :goto_62

    :cond_8d
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v5

    if-eqz v5, :cond_217

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_217

    move v3, v1

    move v12, v0

    move v0, v2

    move v2, v12

    :goto_9f
    if-ge v3, v4, :cond_217

    sget-object v1, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v1, :cond_b7

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    const/4 v6, 0x0

    aput v6, v1, v3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v6, 0x0

    aget-object v1, v1, v6

    aput v3, v1, v3

    :goto_b3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_9f

    :cond_b7
    const/4 v1, 0x0

    const/16 v6, 0xe

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v8, 0x200

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_27f

    if-nez v0, :cond_27f

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    const/4 v1, 0x3

    aput v1, v0, v3

    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x0

    aget-object v0, v0, v8

    aput v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x0

    aget-object v0, v0, v8

    add-int/lit8 v8, v2, 0x1

    aput v3, v0, v8

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x1

    aget-object v0, v0, v8

    aput v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x1

    aget-object v0, v0, v8

    add-int/lit8 v8, v2, 0x1

    aput v3, v0, v8

    const/4 v0, 0x1

    :goto_f1
    if-nez v0, :cond_11a

    const-wide/16 v8, 0x400

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_11a

    and-int/lit8 v8, v1, 0x3

    if-nez v8, :cond_1d1

    or-int/lit8 v1, v1, 0x3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x0

    aget-object v0, v0, v8

    aput v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x0

    aget-object v0, v0, v8

    add-int/lit8 v8, v2, 0x1

    aput v3, v0, v8

    const/4 v0, 0x1

    :cond_113
    :goto_113
    if-eqz v0, :cond_11a

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    const/4 v9, 0x2

    aput v9, v8, v3

    :cond_11a
    if-nez v0, :cond_141

    const-wide/16 v8, 0x800

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_141

    and-int/lit8 v6, v1, 0x5

    if-nez v6, :cond_1ea

    or-int/lit8 v1, v1, 0x5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v6, 0x0

    aget-object v0, v0, v6

    aput v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v6, 0x1

    aget-object v0, v0, v6

    aput v3, v0, v2

    const/4 v0, 0x1

    :cond_13a
    :goto_13a
    if-eqz v0, :cond_141

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    const/4 v7, 0x1

    aput v7, v6, v3

    :cond_141
    move v12, v0

    move v0, v1

    move v1, v12

    if-nez v1, :cond_162

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCardTypes:[I

    const/4 v6, 0x0

    aput v6, v1, v3

    const/4 v1, 0x0

    :goto_14c
    const/4 v6, 0x4

    if-ge v1, v6, :cond_162

    const/4 v6, 0x1

    shl-int/2addr v6, v1

    and-int/2addr v6, v0

    if-nez v6, :cond_205

    const/4 v6, 0x1

    shl-int/2addr v6, v1

    or-int/2addr v0, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    shr-int/lit8 v7, v1, 0x1

    aget-object v6, v6, v7

    and-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v2

    aput v3, v6, v1

    :cond_162
    const-string v1, "StreamAdapter"

    const/4 v6, 0x3

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1c4

    const-string v6, "StreamAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "Box: ["

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    and-int/lit8 v1, v0, 0x1

    const/4 v8, 0x1

    if-ne v1, v8, :cond_209

    const-string v1, "1"

    :goto_17b
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    and-int/lit8 v1, v0, 0x2

    const/4 v8, 0x2

    if-ne v1, v8, :cond_20d

    const-string v1, "1"

    :goto_186
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "]"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "StreamAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "     ["

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    and-int/lit8 v1, v0, 0x4

    const/4 v8, 0x4

    if-ne v1, v8, :cond_211

    const-string v1, "1"

    :goto_1a7
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    and-int/lit8 v1, v0, 0x8

    const/16 v8, 0x8

    if-ne v1, v8, :cond_214

    const-string v1, "1"

    :goto_1b3
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "]"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c4
    const/16 v1, 0xf

    if-ne v0, v1, :cond_27c

    const/4 v0, 0x0

    add-int/lit8 v1, v2, 0x2

    :goto_1cb
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move v2, v1

    goto/16 :goto_b3

    :cond_1d1
    and-int/lit8 v8, v1, 0xc

    if-nez v8, :cond_113

    or-int/lit8 v1, v1, 0xc

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x1

    aget-object v0, v0, v8

    aput v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v8, 0x1

    aget-object v0, v0, v8

    add-int/lit8 v8, v2, 0x1

    aput v3, v0, v8

    const/4 v0, 0x1

    goto/16 :goto_113

    :cond_1ea
    and-int/lit8 v6, v1, 0xa

    if-nez v6, :cond_13a

    or-int/lit8 v1, v1, 0xa

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v6, 0x0

    aget-object v0, v0, v6

    add-int/lit8 v6, v2, 0x1

    aput v3, v0, v6

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v6, 0x1

    aget-object v0, v0, v6

    add-int/lit8 v6, v2, 0x1

    aput v3, v0, v6

    const/4 v0, 0x1

    goto/16 :goto_13a

    :cond_205
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_14c

    :cond_209
    const-string v1, "0"

    goto/16 :goto_17b

    :cond_20d
    const-string v1, "0"

    goto/16 :goto_186

    :cond_211
    const-string v1, "0"

    goto :goto_1a7

    :cond_214
    const-string v1, "0"

    goto :goto_1b3

    :cond_217
    const-string v0, "StreamAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_27b

    const-string v0, "StreamAdapter"

    const-string v1, "BoxLayout:"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    mul-int/lit8 v1, v4, 0x2

    :goto_22a
    if-ge v0, v1, :cond_27b

    const-string v2, "StreamAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x0

    aget-object v4, v4, v5

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x0

    aget-object v4, v4, v5

    add-int/lit8 v5, v0, 0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "StreamAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x1

    aget-object v4, v4, v5

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    const/4 v5, 0x1

    aget-object v4, v4, v5

    add-int/lit8 v5, v0, 0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x2

    goto :goto_22a

    .line 484
    :cond_27b
    return-void

    :cond_27c
    move v1, v2

    goto/16 :goto_1cb

    :cond_27f
    move v12, v1

    move v1, v0

    move v0, v12

    goto/16 :goto_f1
.end method

.method public final getColumnCount()I
    .registers 2

    .prologue
    .line 704
    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x2

    goto :goto_7
.end method

.method protected final getItemViewType(II)I
    .registers 9
    .parameter "partition"
    .parameter "position"

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 648
    packed-switch p1, :pswitch_data_64

    .line 658
    :cond_6
    :goto_6
    :pswitch_6
    return v0

    .line 654
    :pswitch_7
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt p2, v1, :cond_6

    :cond_13
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    const-wide/16 v2, 0x1000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_28

    const/4 v0, 0x7

    goto :goto_6

    :cond_28
    const-wide/16 v2, 0x2000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_31

    const/4 v0, 0x6

    goto :goto_6

    :cond_31
    const-wide/16 v2, 0x4000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3a

    const/4 v0, 0x5

    goto :goto_6

    :cond_3a
    const-wide/32 v2, 0x10000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_45

    const/16 v0, 0x8

    goto :goto_6

    :cond_45
    const-wide/16 v2, 0xa0

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_58

    const-wide/32 v2, 0x8004

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_56

    const/4 v0, 0x3

    goto :goto_6

    :cond_56
    const/4 v0, 0x4

    goto :goto_6

    :cond_58
    const-wide/16 v2, 0xf

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_61

    const/4 v0, 0x2

    goto :goto_6

    :cond_61
    const/4 v0, 0x1

    goto :goto_6

    .line 648
    nop

    :pswitch_data_64
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final getLayoutArray()[[I
    .registers 2

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mBoxLayout:[[I

    return-object v0
.end method

.method public getViewTypeCount()I
    .registers 2

    .prologue
    .line 643
    const/16 v0, 0x9

    return v0
.end method

.method public hasStableIds()Z
    .registers 2

    .prologue
    .line 310
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 727
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount(I)I

    move-result v1

    if-nez v1, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isHorizontal()Z
    .registers 2

    .prologue
    .line 714
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mLandscape:Z

    return v0
.end method

.method public newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    const-wide/16 v4, 0x0

    .line 451
    const/16 v2, 0xe

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 453
    .local v0, contentFlags:J
    const-wide/16 v2, 0x1000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_15

    .line 454
    new-instance v2, Lcom/google/android/apps/plus/views/EventStreamCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/EventStreamCardView;-><init>(Landroid/content/Context;)V

    .line 473
    :goto_14
    return-object v2

    .line 455
    :cond_15
    const-wide/16 v2, 0x2000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_22

    .line 456
    new-instance v2, Lcom/google/android/apps/plus/views/HangoutCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/HangoutCardView;-><init>(Landroid/content/Context;)V

    goto :goto_14

    .line 457
    :cond_22
    const-wide/16 v2, 0x4000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2f

    .line 458
    new-instance v2, Lcom/google/android/apps/plus/views/SkyjamCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/SkyjamCardView;-><init>(Landroid/content/Context;)V

    goto :goto_14

    .line 459
    :cond_2f
    const-wide/32 v2, 0x10000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3d

    .line 460
    new-instance v2, Lcom/google/android/apps/plus/views/PlaceReviewCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/PlaceReviewCardView;-><init>(Landroid/content/Context;)V

    goto :goto_14

    .line 461
    :cond_3d
    const-wide/16 v2, 0xa0

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_58

    .line 463
    const-wide/32 v2, 0x8004

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_52

    .line 465
    new-instance v2, Lcom/google/android/apps/plus/views/LinksCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/LinksCardView;-><init>(Landroid/content/Context;)V

    goto :goto_14

    .line 467
    :cond_52
    new-instance v2, Lcom/google/android/apps/plus/views/ImageCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/ImageCardView;-><init>(Landroid/content/Context;)V

    goto :goto_14

    .line 468
    :cond_58
    const-wide/16 v2, 0xf

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_65

    .line 471
    new-instance v2, Lcom/google/android/apps/plus/views/TextCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/TextCardView;-><init>(Landroid/content/Context;)V

    goto :goto_14

    .line 473
    :cond_65
    new-instance v2, Lcom/google/android/apps/plus/views/DummyCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/DummyCardView;-><init>(Landroid/content/Context;)V

    goto :goto_14
.end method

.method protected final newView$54126883(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "partition"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    .line 430
    packed-switch p2, :pswitch_data_18

    .line 440
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 432
    :pswitch_5
    new-instance v0, Lcom/google/android/apps/plus/views/ComposeBarView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/views/ComposeBarView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setComposeBarPadding(Landroid/view/View;I)V

    goto :goto_4

    .line 436
    :pswitch_13
    invoke-virtual {p0, p1, p3, p4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_4

    .line 430
    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_5
        :pswitch_13
    .end packed-switch
.end method

.method public final onPause()V
    .registers 5

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Ljava/lang/Integer;

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 306
    :cond_26
    return-void
.end method

.method public final onStreamCardViewed(Ljava/lang/String;)V
    .registers 3
    .parameter "activityId"

    .prologue
    .line 695
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mMarkPostsAsRead:Z

    if-eqz v0, :cond_9

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 700
    :cond_9
    return-void
.end method

.method public final resetAnimationState()V
    .registers 2

    .prologue
    .line 722
    const/high16 v0, -0x8000

    iput v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    .line 723
    return-void
.end method

.method public final setMarkPostsAsRead(Z)V
    .registers 2
    .parameter "markPostsAsRead"

    .prologue
    .line 748
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mMarkPostsAsRead:Z

    .line 749
    return-void
.end method
