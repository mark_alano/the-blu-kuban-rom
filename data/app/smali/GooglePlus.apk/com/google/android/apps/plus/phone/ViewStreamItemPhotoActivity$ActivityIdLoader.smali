.class final Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "ViewStreamItemPhotoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActivityIdLoader"
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mStreamItemUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V
    .registers 4
    .parameter "context"
    .parameter "account"
    .parameter "streamItemUri"

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    .line 88
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mStreamItemUri:Landroid/net/Uri;

    .line 90
    return-void
.end method

.method private loadMediaFromDatabase$73948607(Landroid/content/ContentResolver;Ljava/lang/String;)[B
    .registers 11
    .parameter "resolver"
    .parameter "activityId"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 131
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 134
    .local v1, activityUri:Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "media"

    aput-object v0, v2, v4

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 137
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_1c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 138
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_26
    .catchall {:try_start_1c .. :try_end_26} :catchall_39

    move-result-object v7

    .line 139
    .local v7, mediaBytes:[B
    if-eqz v7, :cond_2d

    .line 146
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 149
    .end local v7           #mediaBytes:[B
    :goto_2c
    return-object v7

    .line 142
    .restart local v7       #mediaBytes:[B
    :cond_2d
    const/4 v0, 0x0

    :try_start_2e
    new-array v7, v0, [B
    :try_end_30
    .catchall {:try_start_2e .. :try_end_30} :catchall_39

    .line 146
    .end local v7           #mediaBytes:[B
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2c

    :cond_34
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v7, v3

    .line 149
    goto :goto_2c

    .line 146
    :catchall_39
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 16

    .prologue
    .line 97
    const/4 v12, 0x0

    .line 98
    .local v12, personId:Ljava/lang/String;
    const/4 v8, 0x0

    .line 99
    .local v8, activityId:Ljava/lang/String;
    const/4 v11, 0x0

    .line 100
    .local v11, mediaIndex:I
    const/4 v10, 0x0

    .line 101
    .local v10, mediaData:[B
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 102
    .local v0, resolver:Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mStreamItemUri:Landroid/net/Uri;

    invoke-static {}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->access$000()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 105
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_19
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_ac

    .line 106
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 107
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_28
    .catchall {:try_start_19 .. :try_end_28} :catchall_82

    move-result-object v4

    .line 108
    .end local v8           #activityId:Ljava/lang/String;
    .local v4, activityId:Ljava/lang/String;
    const/4 v1, 0x2

    :try_start_2a
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_aa

    move-result v11

    .line 111
    :goto_2e
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 114
    if-eqz v4, :cond_62

    .line 115
    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->loadMediaFromDatabase$73948607(Landroid/content/ContentResolver;Ljava/lang/String;)[B

    move-result-object v10

    .line 116
    if-nez v10, :cond_62

    .line 117
    new-instance v1, Lcom/google/android/apps/plus/api/GetActivityOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/GetActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivityOperation;->start()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v2

    if-eqz v2, :cond_88

    const-string v2, "ViewStreamItemActivity"

    const-string v3, "Cannot download activity"

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v14, 0x0

    .line 118
    .local v14, success:Z
    :goto_5c
    if-eqz v14, :cond_62

    .line 119
    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->loadMediaFromDatabase$73948607(Landroid/content/ContentResolver;Ljava/lang/String;)[B

    move-result-object v10

    .line 123
    .end local v14           #success:Z
    :cond_62
    new-instance v13, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-static {}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->access$100()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v13, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 124
    .local v13, result:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v12, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v10, v1, v2

    invoke-virtual {v13, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 125
    return-object v13

    .line 111
    .end local v4           #activityId:Ljava/lang/String;
    .end local v13           #result:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .restart local v8       #activityId:Ljava/lang/String;
    :catchall_82
    move-exception v1

    move-object v4, v8

    .end local v8           #activityId:Ljava/lang/String;
    .restart local v4       #activityId:Ljava/lang/String;
    :goto_84
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1

    .line 117
    :cond_88
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivityOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_a8

    const-string v2, "ViewStreamItemActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Cannot download activity: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getErrorCode()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x0

    goto :goto_5c

    :cond_a8
    const/4 v14, 0x1

    goto :goto_5c

    .line 111
    :catchall_aa
    move-exception v1

    goto :goto_84

    .end local v4           #activityId:Ljava/lang/String;
    .restart local v8       #activityId:Ljava/lang/String;
    :cond_ac
    move-object v4, v8

    .end local v8           #activityId:Ljava/lang/String;
    .restart local v4       #activityId:Ljava/lang/String;
    goto :goto_2e
.end method
