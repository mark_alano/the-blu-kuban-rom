.class public Lcom/google/android/apps/plus/phone/WriteReviewActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "WriteReviewActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;
    .registers 2

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;-><init>()V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "bundle"

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_REWIEWS:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-nez v0, :cond_15

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/WriteReviewActivity;->finish()V

    .line 32
    const-string v0, "WriteReviewActivity"

    const-string v1, "Writing reviews is not enabled yet, this activity should not be used."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    :cond_15
    return-void
.end method
