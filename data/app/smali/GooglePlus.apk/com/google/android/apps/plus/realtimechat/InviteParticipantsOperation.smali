.class public final Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "InviteParticipantsOperation.java"


# instance fields
.field mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field mConversationRowId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "audience"

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 27
    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mConversationRowId:J

    .line 28
    iput-object p5, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 29
    return-void
.end method


# virtual methods
.method public final execute()V
    .registers 7

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantListFromAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Ljava/util/List;

    move-result-object v4

    .line 41
    .local v4, participants:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mConversationRowId:J

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->inviteParticipantsLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    .line 43
    return-void
.end method
