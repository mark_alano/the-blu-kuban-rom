.class public final Lcom/google/android/apps/plus/service/AndroidContactsSync;
.super Ljava/lang/Object;
.source "AndroidContactsSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    }
.end annotation


# static fields
.field private static final ACTIVITY_PROJECTION:[Ljava/lang/String;

.field private static final ACTIVITY_STATE_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;",
            ">;"
        }
    .end annotation
.end field

.field private static final ACTIVITY_SUMMARY_PROJECTION:[Ljava/lang/String;

.field private static final AVATAR_URL_PROJECTION:[Ljava/lang/String;

.field private static final CONTACTS_PROJECTION:[Ljava/lang/String;

.field private static final DISPLAY_PHOTO_CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

.field private static final EMAIL_TYPE_CUSTOM:Ljava/lang/String;

.field private static final EMAIL_TYPE_HOME:Ljava/lang/String;

.field private static final EMAIL_TYPE_OTHER:Ljava/lang/String;

.field private static final EMAIL_TYPE_WORK:Ljava/lang/String;

.field private static final ENTITIES_PROJECTION:[Ljava/lang/String;

.field private static final GROUPS_PROJECTION:[Ljava/lang/String;

.field private static final LARGE_AVATAR_RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

.field private static final MY_PROFILE_PROJECTION:[Ljava/lang/String;

.field private static final PHONE_TYPE_ASSISTANT:Ljava/lang/String;

.field private static final PHONE_TYPE_CALLBACK:Ljava/lang/String;

.field private static final PHONE_TYPE_CAR:Ljava/lang/String;

.field private static final PHONE_TYPE_COMPANY_MAIN:Ljava/lang/String;

.field private static final PHONE_TYPE_CUSTOM:Ljava/lang/String;

.field private static final PHONE_TYPE_HOME:Ljava/lang/String;

.field private static final PHONE_TYPE_HOME_FAX:Ljava/lang/String;

.field private static final PHONE_TYPE_ISDN:Ljava/lang/String;

.field private static final PHONE_TYPE_MAIN:Ljava/lang/String;

.field private static final PHONE_TYPE_MOBILE:Ljava/lang/String;

.field private static final PHONE_TYPE_OTHER:Ljava/lang/String;

.field private static final PHONE_TYPE_OTHER_FAX:Ljava/lang/String;

.field private static final PHONE_TYPE_PAGER:Ljava/lang/String;

.field private static final PHONE_TYPE_RADIO:Ljava/lang/String;

.field private static final PHONE_TYPE_TELEX:Ljava/lang/String;

.field private static final PHONE_TYPE_TTY_TDD:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK_FAX:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK_MOBILE:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK_PAGER:Ljava/lang/String;

.field private static final POSTAL_TYPE_CUSTOM:Ljava/lang/String;

.field private static final POSTAL_TYPE_HOME:Ljava/lang/String;

.field private static final POSTAL_TYPE_OTHER:Ljava/lang/String;

.field private static final POSTAL_TYPE_WORK:Ljava/lang/String;

.field private static final PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

.field private static final PROFILE_ENTITIES_PROJECTION:[Ljava/lang/String;

.field private static final PROFILE_PROJECTION:[Ljava/lang/String;

.field public static final PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final RAW_CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final RAW_CONTACT_REFRESH_PROJECTION:[Ljava/lang/String;

.field private static final RAW_CONTACT_SOURCE_ID_PROJECTION:[Ljava/lang/String;

.field public static final STREAM_ITEMS_CONTENT_LIMIT_URI:Landroid/net/Uri;

.field public static final STREAM_ITEMS_PHOTO_URI:Landroid/net/Uri;

.field private static final STREAM_ITEMS_PROJECTION:[Ljava/lang/String;

.field public static final STREAM_ITEMS_URI:Landroid/net/Uri;

.field private static final THUMBNAILS_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

.field private static sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

.field private static sContactsProviderExists:Z

.field private static sContactsProviderStatusKnown:Z

.field private static sMaxStreamItemsPerRawContact:I

.field private static sPhoneTypeMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sThumbnailSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 102
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "profile/raw_contacts"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    .line 106
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "stream_items"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_URI:Landroid/net/Uri;

    .line 122
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "stream_items_limit"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_CONTENT_LIMIT_URI:Landroid/net/Uri;

    .line 183
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "stream_items/photo"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PHOTO_URI:Landroid/net/Uri;

    .line 190
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "photo_dimensions"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->DISPLAY_PHOTO_CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

    .line 332
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "sync1"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    .line 340
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "last_updated_time"

    aput-object v1, v0, v3

    const-string v1, "profile_proto"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->MY_PROFILE_PROJECTION:[Ljava/lang/String;

    .line 351
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "last_updated_time"

    aput-object v1, v0, v3

    const-string v1, "for_sharing"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->CONTACTS_PROJECTION:[Ljava/lang/String;

    .line 361
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "contact_proto"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_PROJECTION:[Ljava/lang/String;

    .line 372
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sourceid"

    aput-object v1, v0, v5

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "data_id"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ENTITIES_PROJECTION:[Ljava/lang/String;

    .line 388
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sync1"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    .line 396
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "data1"

    aput-object v1, v0, v4

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_ENTITIES_PROJECTION:[Ljava/lang/String;

    .line 410
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_HOME:Ljava/lang/String;

    .line 411
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_WORK:Ljava/lang/String;

    .line 412
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_OTHER:Ljava/lang/String;

    .line 413
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_CUSTOM:Ljava/lang/String;

    .line 415
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_HOME:Ljava/lang/String;

    .line 416
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_WORK:Ljava/lang/String;

    .line 417
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_OTHER:Ljava/lang/String;

    .line 418
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_CUSTOM:Ljava/lang/String;

    .line 420
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME:Ljava/lang/String;

    .line 421
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK:Ljava/lang/String;

    .line 422
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER:Ljava/lang/String;

    .line 423
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME_FAX:Ljava/lang/String;

    .line 424
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_FAX:Ljava/lang/String;

    .line 425
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MOBILE:Ljava/lang/String;

    .line 426
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_PAGER:Ljava/lang/String;

    .line 427
    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER_FAX:Ljava/lang/String;

    .line 428
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_COMPANY_MAIN:Ljava/lang/String;

    .line 429
    const/16 v0, 0x13

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ASSISTANT:Ljava/lang/String;

    .line 430
    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CAR:Ljava/lang/String;

    .line 431
    const/16 v0, 0xe

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_RADIO:Ljava/lang/String;

    .line 432
    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ISDN:Ljava/lang/String;

    .line 433
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CALLBACK:Ljava/lang/String;

    .line 434
    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TELEX:Ljava/lang/String;

    .line 435
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TTY_TDD:Ljava/lang/String;

    .line 436
    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_MOBILE:Ljava/lang/String;

    .line 437
    const/16 v0, 0x12

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_PAGER:Ljava/lang/String;

    .line 438
    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MAIN:Ljava/lang/String;

    .line 439
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    .line 447
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 448
    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 449
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 450
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER:Ljava/lang/String;

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 451
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME_FAX:Ljava/lang/String;

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 452
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_FAX:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 453
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MOBILE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 454
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_PAGER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 455
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER_FAX:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 456
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_COMPANY_MAIN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 457
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ASSISTANT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 458
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CAR:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 459
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_RADIO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 460
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ISDN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 461
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CALLBACK:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 462
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TELEX:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 463
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TTY_TDD:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_MOBILE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 465
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_PAGER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 466
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MAIN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 473
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->GROUPS_PROJECTION:[Ljava/lang/String;

    .line 492
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "sync2"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->LARGE_AVATAR_RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    .line 508
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "avatar"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    .line 518
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "data_id"

    aput-object v1, v0, v4

    const-string v1, "mimetype"

    aput-object v1, v0, v6

    const-string v1, "sync3"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->THUMBNAILS_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    .line 546
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_SOURCE_ID_PROJECTION:[Ljava/lang/String;

    .line 557
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "raw_contact_source_id"

    aput-object v1, v0, v3

    const-string v1, "stream_item_sync1"

    aput-object v1, v0, v4

    const-string v1, "timestamp"

    aput-object v1, v0, v6

    const-string v1, "stream_item_sync2"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PROJECTION:[Ljava/lang/String;

    .line 577
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "activity_id"

    aput-object v1, v0, v5

    const-string v1, "author_id"

    aput-object v1, v0, v3

    const-string v1, "created"

    aput-object v1, v0, v4

    const-string v1, "modified"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_SUMMARY_PROJECTION:[Ljava/lang/String;

    .line 592
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "activity_id"

    aput-object v1, v0, v5

    const-string v1, "media"

    aput-object v1, v0, v3

    const-string v1, "total_comment_count"

    aput-object v1, v0, v4

    const-string v1, "plus_one_data"

    aput-object v1, v0, v6

    const-string v1, "loc"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "original_author_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "annotation"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_PROJECTION:[Ljava/lang/String;

    .line 622
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "account_type"

    aput-object v1, v0, v3

    const-string v1, "account_name"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v6

    const-string v1, "sourceid"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sync2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sync4"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_REFRESH_PROJECTION:[Ljava/lang/String;

    .line 688
    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_STATE_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method private static addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .registers 5
    .parameter "state"
    .parameter "mimeType"
    .parameter "data"

    .prologue
    .line 1602
    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;-><init>(B)V

    .line 1603
    .local v0, dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-object p1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    .line 1604
    iput-object p2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    .line 1605
    iget-object v1, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1606
    return-object v0
.end method

.method private static appendImgTag(Landroid/content/Context;Ljava/lang/StringBuilder;I)V
    .registers 5
    .parameter "context"
    .parameter "sb"
    .parameter "resId"

    .prologue
    .line 3006
    const-string v0, "<img src=\'res://"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3008
    return-void
.end method

.method private static applyActivityChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 2683
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 2684
    .local v8, resolver:Landroid/content/ContentResolver;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2685
    .local v5, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2687
    .local v2, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    new-array v6, v10, [I

    aput v1, v6, v1

    .line 2688
    .local v6, totalImageBytes:[I
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 2689
    .local v9, size:I
    const/4 v3, 0x0

    .line 2690
    .local v3, offset:I
    :goto_1d
    if-ge v3, v9, :cond_46

    .line 2691
    add-int/lit8 v4, v3, 0x20

    .line 2692
    .local v4, to:I
    if-le v4, v9, :cond_24

    .line 2693
    move v4, v9

    .line 2696
    :cond_24
    const-string v0, "GooglePlusContactsSync"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 2697
    move v7, v3

    .local v7, i:I
    :goto_2e
    if-ge v7, v4, :cond_3c

    .line 2698
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->dumpPersonActivityState(Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;)V

    .line 2697
    add-int/lit8 v7, v7, 0x1

    goto :goto_2e

    .line 2702
    .end local v7           #i:I
    :cond_3c
    invoke-static {p1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteObsoleteStreamItems(Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;)V

    move-object v0, p0

    move-object v1, p1

    .line 2703
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;[I)V

    .line 2704
    move v3, v4

    .line 2705
    goto :goto_1d

    .line 2707
    .end local v4           #to:I
    :cond_46
    invoke-static {v8, v5, v10}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2708
    return-void
.end method

.method private static applyCircleChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 991
    .local p2, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 992
    .local v3, resolver:Landroid/content/ContentResolver;
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 993
    .local v1, groupsUri:Landroid/net/Uri;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 994
    .local v0, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_cd

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    .line 995
    .local v4, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    const-string v6, "GooglePlusContactsSync"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_6c

    .line 996
    iget-boolean v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->exists:Z

    if-nez v6, :cond_a0

    const-string v6, "[DELETE]"

    :goto_30
    const-string v7, "GooglePlusContactsSync"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " (group_id="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ")"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    :cond_6c
    iget-boolean v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->exists:Z

    if-eqz v6, :cond_ae

    iget-wide v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_ae

    .line 1000
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "sourceid"

    iget-object v8, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "title"

    iget-object v8, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "group_is_read_only"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_15

    .line 996
    :cond_a0
    iget-wide v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_ab

    const-string v6, "[INSERT]"

    goto :goto_30

    :cond_ab
    const-string v6, "[UPDATE]"

    goto :goto_30

    .line 1005
    :cond_ae
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_15

    .line 1019
    .end local v4           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    :cond_cd
    const/4 v6, 0x1

    invoke-static {v3, v0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 1023
    invoke-virtual {p2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_ef

    .line 1024
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1025
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "sync1"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1026
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v3, v6, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1028
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_ef
    return-void
.end method

.method private static buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Z)V
    .registers 17
    .parameter "context"
    .parameter "rawContactsUri"
    .parameter
    .parameter "state"
    .parameter "myProfile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1704
    .local p2, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v6, "GooglePlusContactsSync"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1705
    invoke-static {p3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->dumpRawContactState(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;)V

    .line 1708
    :cond_c
    const/4 v5, 0x0

    .line 1709
    .local v5, rawContactBackReference:I
    iget-wide v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_1d3

    .line 1710
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1711
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "sourceid"

    iget-object v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "sync1"

    iget-wide v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_is_read_only"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1717
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/name"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data1"

    iget-object v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1722
    if-nez p4, :cond_121

    .line 1723
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "conversation"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    const v8, 0x7f0802a2

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1732
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0xe

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "hangout"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    const v8, 0x7f0802a3

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1741
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0x14

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "addtocircle"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    const v8, 0x7f0802a4

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1750
    :cond_121
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0x1e

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "view"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    const v8, 0x7f0802a1

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1758
    iget-object v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1759
    .local v3, gaiaId:Ljava/lang/String;
    if-eqz v3, :cond_19d

    .line 1760
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/identity"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data2"

    const-string v8, "com.google"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data1"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "gprofile:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1776
    .end local v3           #gaiaId:Ljava/lang/String;
    :cond_19d
    :goto_19d
    iget-object v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :cond_1a3
    :goto_1a3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_266

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    .line 1778
    .local v2, dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iget-boolean v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    if-nez v6, :cond_202

    .line 1779
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    .line 1808
    .local v1, cpo:Landroid/content/ContentProviderOperation;
    :goto_1cf
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1a3

    .line 1768
    .end local v1           #cpo:Landroid/content/ContentProviderOperation;
    .end local v2           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_1d3
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "sync1"

    iget-wide v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_19d

    .line 1785
    .restart local v2       #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .restart local v4       #i$:Ljava/util/Iterator;
    :cond_202
    iget-wide v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_24a

    .line 1786
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1787
    .local v0, builder:Landroid/content/ContentProviderOperation$Builder;
    const-string v6, "mimetype"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1788
    iget-wide v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_23e

    .line 1789
    const-string v6, "raw_contact_id"

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1802
    :goto_224
    const-string v6, "data1"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1803
    const-string v6, "data2"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1804
    const-string v6, "data3"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1805
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    .restart local v1       #cpo:Landroid/content/ContentProviderOperation;
    goto :goto_1cf

    .line 1792
    .end local v1           #cpo:Landroid/content/ContentProviderOperation;
    :cond_23e
    const-string v6, "raw_contact_id"

    iget-wide v7, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_224

    .line 1794
    .end local v0           #builder:Landroid/content/ContentProviderOperation$Builder;
    :cond_24a
    iget-boolean v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->changed:Z

    if-eqz v6, :cond_1a3

    .line 1795
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1796
    .restart local v0       #builder:Landroid/content/ContentProviderOperation$Builder;
    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-wide v9, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_224

    .line 1810
    .end local v0           #builder:Landroid/content/ContentProviderOperation$Builder;
    .end local v2           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    :cond_266
    return-void
.end method

.method private static buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;[Ljava/lang/String;Ljava/util/HashMap;)V
    .registers 8
    .parameter "context"
    .parameter "rawContactsUri"
    .parameter
    .parameter "personIds"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1692
    .local p2, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p4, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    array-length v2, p3

    if-ge v0, v2, :cond_13

    .line 1693
    aget-object v2, p3, v0

    invoke-virtual {p4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1694
    .local v1, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    const/4 v2, 0x0

    invoke-static {p0, p1, p2, v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Z)V

    .line 1692
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1696
    .end local v1           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_13
    return-void
.end method

.method private static buildStreamItemContentHtml(Landroid/content/Context;[Lcom/google/android/apps/plus/content/DbMedia;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 18
    .parameter "context"
    .parameter "dbMedia"
    .parameter "location"
    .parameter "originalAuthorName"
    .parameter "annotation"
    .parameter "content"

    .prologue
    .line 2885
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2887
    .local v6, text:Ljava/lang/StringBuilder;
    if-eqz p2, :cond_1a

    .line 2888
    const-string v9, "<div><font color=\'#6e8ec6\'><b>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</b></font></div>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2893
    :cond_1a
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_31

    .line 2894
    const-string v9, "<div>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</div>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2897
    :cond_31
    const/4 v1, 0x0

    .line 2898
    .local v1, blockquoteStarted:Z
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_c8

    .line 2899
    const v9, 0x7f08018c

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p3, v10, v11

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2901
    .local v4, originalAuthor:Ljava/lang/String;
    const-string v9, "<div>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</div>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2902
    const-string v9, "<blockquote>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2903
    const/4 v1, 0x1

    .line 2905
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_71

    .line 2906
    const-string v9, "<div><font color=\'#777777\'>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</font></div>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2915
    .end local v4           #originalAuthor:Ljava/lang/String;
    :cond_71
    :goto_71
    if-eqz p1, :cond_bc

    .line 2916
    const/4 v3, 0x0

    .local v3, i:I
    array-length v5, p1

    .local v5, size:I
    :goto_75
    if-ge v3, v5, :cond_bc

    .line 2917
    aget-object v9, p1, v3

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v8

    .line 2918
    .local v8, type:I
    const/4 v9, 0x1

    if-eq v8, v9, :cond_86

    const/4 v9, 0x3

    if-eq v8, v9, :cond_86

    const/4 v9, 0x2

    if-ne v8, v9, :cond_e0

    .line 2921
    :cond_86
    aget-object v9, p1, v3

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbMedia;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 2922
    .local v7, title:Ljava/lang/String;
    aget-object v9, p1, v3

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbMedia;->getDescription()Ljava/lang/String;

    move-result-object v2

    .line 2923
    .local v2, description:Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a7

    .line 2924
    const-string v9, "<div><font color=\'#6e8ec6\'><b>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</b></font></div>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2927
    :cond_a7
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_bc

    .line 2928
    const-string v9, "<div><font color=\'#777777\'>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</font></div>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2935
    .end local v2           #description:Ljava/lang/String;
    .end local v3           #i:I
    .end local v5           #size:I
    .end local v7           #title:Ljava/lang/String;
    .end local v8           #type:I
    :cond_bc
    if-eqz v1, :cond_c3

    .line 2936
    const-string v9, "</blockquote>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2939
    :cond_c3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 2910
    :cond_c8
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_71

    .line 2911
    const-string v9, "<div>"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</div>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_71

    .line 2916
    .restart local v3       #i:I
    .restart local v5       #size:I
    .restart local v8       #type:I
    :cond_e0
    add-int/lit8 v3, v3, 0x1

    goto :goto_75
.end method

.method private static declared-synchronized cancelSync()V
    .registers 2

    .prologue
    .line 870
    const-class v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    if-eqz v0, :cond_c

    .line 871
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->cancel()V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_e

    .line 873
    :cond_c
    monitor-exit v1

    return-void

    .line 870
    :catchall_e
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static cleanUpActivityStateMap(Ljava/util/HashMap;)V
    .registers 11
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    const-wide/16 v8, 0x0

    .line 2659
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2662
    .local v5, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    .line 2663
    .local v4, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    const/4 v1, 0x0

    .line 2664
    .local v1, hasChanges:Z
    iget-object v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_22
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_47

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .line 2665
    .local v0, activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    iget-boolean v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-eqz v6, :cond_38

    iget-wide v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v6, v6, v8

    if-eqz v6, :cond_46

    :cond_38
    iget-boolean v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v6, :cond_42

    iget-wide v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_46

    :cond_42
    iget-boolean v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    if-eqz v6, :cond_22

    .line 2668
    :cond_46
    const/4 v1, 0x1

    .line 2672
    .end local v0           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    :cond_47
    if-nez v1, :cond_f

    .line 2673
    iget-object v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {p0, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .line 2676
    .end local v1           #hasChanges:Z
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :cond_4f
    return-void
.end method

.method private static clearAndroidCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    const/4 v3, 0x0

    .line 3266
    const-string v1, "Android:DeleteCircles"

    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 3267
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 3268
    .local v0, count:I
    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    .line 3269
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 3270
    return-void
.end method

.method private static clearAndroidContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    const/4 v1, 0x0

    .line 3256
    const-string v0, "Android:DeleteContacts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 3257
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, v1, v1, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 3258
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 3259
    return-void
.end method

.method private static clearAndroidContactsForOtherAccounts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 3168
    new-array v4, v11, [Ljava/lang/String;

    if-eqz p1, :cond_96

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_b
    aput-object v0, v4, v10

    .line 3170
    .local v4, accountName:[Ljava/lang/String;
    const-string v0, "Android:DeleteProfilesOtherAccounts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 3171
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v2, "data_set=\'plus\' AND account_name!=? AND account_type=\'com.google\'"

    invoke-static {p0, v0, v2, v4, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 3176
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 3178
    const-string v0, "Android:DeleteContactsOtherAccounts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 3179
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v2, "data_set=\'plus\' AND account_name!=? AND account_type=\'com.google\'"

    invoke-static {p0, v0, v2, v4, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 3184
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 3186
    const-string v0, "Android:DeleteCirclesOtherAccounts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 3190
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 3191
    .local v1, groupsUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v11, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v10

    const-string v3, "data_set=\'plus\' AND account_name!=? AND account_type=\'com.google\'"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 3197
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_9d

    .line 3199
    :goto_74
    :try_start_74
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 3200
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 3201
    .local v8, groupId:J
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 3203
    .local v6, count:I
    invoke-virtual {p2, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V
    :try_end_90
    .catchall {:try_start_74 .. :try_end_90} :catchall_91

    goto :goto_74

    .line 3206
    .end local v6           #count:I
    .end local v8           #groupId:J
    :catchall_91
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 3168
    .end local v1           #groupsUri:Landroid/net/Uri;
    .end local v4           #accountName:[Ljava/lang/String;
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_96
    const-string v0, ""

    goto/16 :goto_b

    .line 3206
    .restart local v1       #groupsUri:Landroid/net/Uri;
    .restart local v4       #accountName:[Ljava/lang/String;
    .restart local v7       #cursor:Landroid/database/Cursor;
    :cond_9a
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3209
    :cond_9d
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 3210
    return-void
.end method

.method private static clearAndroidProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    const/4 v3, 0x0

    .line 3137
    const-string v1, "Android:DeleteProfile"

    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 3138
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3139
    .local v0, resolver:Landroid/content/ContentResolver;
    const-wide/16 v8, 0x0

    .line 3140
    .local v8, rawContactId:J
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 3142
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_28

    .line 3144
    :try_start_1a
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 3145
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_24
    .catchall {:try_start_1a .. :try_end_24} :catchall_41

    move-result-wide v8

    .line 3148
    :cond_25
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3152
    :cond_28
    const-wide/16 v1, 0x0

    cmp-long v1, v8, v1

    if-eqz v1, :cond_3d

    .line 3153
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 3156
    .local v6, count:I
    invoke-virtual {p2, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    .line 3159
    .end local v6           #count:I
    :cond_3d
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 3160
    return-void

    .line 3148
    :catchall_41
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "context"
    .parameter
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "orderByAndLimit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3100
    .local p1, rawContactIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3102
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_26

    .line 3104
    :goto_f
    :try_start_f
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 3105
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1d
    .catchall {:try_start_f .. :try_end_1d} :catchall_1e

    goto :goto_f

    .line 3108
    :catchall_1e
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_23
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3111
    :cond_26
    return-void
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v2, 0x0

    .line 3117
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 3130
    :goto_7
    return-void

    .line 3121
    :cond_8
    invoke-static {}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->cancelSync()V

    .line 3122
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncCleanupStatus(Landroid/content/Context;Z)V

    .line 3123
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncCleanupStatus(Landroid/content/Context;Z)V

    .line 3124
    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    .line 3125
    .local v0, syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 3129
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    goto :goto_7
.end method

.method private static deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 16
    .parameter "context"
    .parameter "rawContactsUri"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "syncState"

    .prologue
    .line 3220
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3221
    .local v0, resolver:Landroid/content/ContentResolver;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 3222
    .local v9, ids:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 3224
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_32

    .line 3226
    :goto_1b
    :try_start_1b
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 3227
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_29
    .catchall {:try_start_1b .. :try_end_29} :catchall_2a

    goto :goto_1b

    .line 3230
    :catchall_2a
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_2f
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3234
    :cond_32
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 3249
    :goto_38
    return-void

    .line 3238
    :cond_39
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3239
    .local v6, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, i$:Ljava/util/Iterator;
    :goto_42
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_72

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 3240
    .local v10, rawContactId:Ljava/lang/String;
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3244
    const/4 v1, 0x0

    invoke-static {v0, v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 3245
    invoke-virtual {p4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    goto :goto_42

    .line 3248
    .end local v10           #rawContactId:Ljava/lang/String;
    :cond_72
    const/4 v1, 0x1

    invoke-static {v0, v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    goto :goto_38
.end method

.method private static deleteContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 14
    .parameter "resolver"
    .parameter "account"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p3, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1818
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 1819
    .local v1, rawContactsUri:Landroid/net/Uri;
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_42

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1820
    .local v2, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    const-string v3, "GooglePlusContactsSync"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 1821
    invoke-static {v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->dumpRawContactState(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;)V

    .line 1824
    :cond_22
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v9, [Ljava/lang/String;

    iget-wide v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 1831
    .end local v2           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_42
    invoke-static {p0, p2, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 1832
    return-void
.end method

.method private static deleteObsoleteStreamItems(Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;)V
    .registers 18
    .parameter "account"
    .parameter
    .parameter "offset"
    .parameter "to"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;II",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3018
    .local p1, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    .local p4, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v8

    .line 3019
    .local v8, uri:Landroid/net/Uri;
    move v2, p2

    .local v2, i:I
    :goto_5
    move/from16 v0, p3

    if-ge v2, v0, :cond_50

    .line 3020
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    .line 3021
    .local v6, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4d

    .line 3022
    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_21
    :goto_21
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .line 3023
    .local v1, activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    iget-boolean v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v9, :cond_21

    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-eqz v9, :cond_21

    .line 3024
    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    invoke-static {v8, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 3025
    .local v4, itemUri:Landroid/net/Uri;
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 3019
    .end local v1           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #itemUri:Landroid/net/Uri;
    :cond_4d
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 3032
    .end local v6           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :cond_50
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 3033
    .local v5, sb:Ljava/lang/StringBuilder;
    const-string v9, "_id IN ("

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3034
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3035
    .local v7, streamItemIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move v2, p2

    :goto_60
    move/from16 v0, p3

    if-ge v2, v0, :cond_b1

    .line 3036
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    .line 3037
    .restart local v6       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_ae

    .line 3038
    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_7c
    :goto_7c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_ae

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .line 3039
    .restart local v1       #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    iget-boolean v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v9, :cond_7c

    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-eqz v9, :cond_7c

    .line 3040
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_9f

    .line 3041
    const/16 v9, 0x2c

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3043
    :cond_9f
    const/16 v9, 0x3f

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3044
    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7c

    .line 3035
    .end local v1           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_ae
    add-int/lit8 v2, v2, 0x1

    goto :goto_60

    .line 3049
    .end local v6           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :cond_b1
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_b8

    .line 3056
    :goto_b7
    return-void

    .line 3052
    :cond_b8
    const/16 v9, 0x29

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3053
    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    invoke-virtual {v10, v11, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b7
.end method

.method private static deleteRemovedContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .registers 8
    .parameter "resolver"
    .parameter "account"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1283
    .local p2, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    .local p3, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1284
    .local v2, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_21

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1285
    .local v1, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    iget-boolean v3, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-nez v3, :cond_d

    .line 1286
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 1290
    .end local v1           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_21
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2a

    .line 1291
    invoke-static {p0, p1, p3, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1293
    :cond_2a
    return-void
.end method

.method private static downloadLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/HashMap;I)V
    .registers 27
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter
    .parameter "avatarSize"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2134
    .local p3, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    .line 2135
    .local v17, resolver:Landroid/content/ContentResolver;
    new-instance v20, Ljava/util/ArrayList;

    invoke-virtual/range {p3 .. p3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2136
    .local v20, states:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;>;"
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 2137
    .local v18, size:I
    const/4 v13, 0x0

    .line 2138
    .local v13, offset:I
    :goto_14
    move/from16 v0, v18

    if-ge v13, v0, :cond_d4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v4

    if-nez v4, :cond_d4

    .line 2139
    add-int/lit8 v21, v13, 0x8

    .line 2140
    .local v21, to:I
    move/from16 v0, v21

    move/from16 v1, v18

    if-le v0, v1, :cond_28

    .line 2141
    move/from16 v21, v18

    .line 2144
    :cond_28
    move v12, v13

    .local v12, i:I
    :goto_29
    move/from16 v0, v21

    if-ge v12, v0, :cond_d0

    .line 2145
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    .line 2146
    .local v19, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    move-object/from16 v0, v19

    iget v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_48

    .line 2147
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->saveAvatarSignature(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;)V

    .line 2144
    :cond_45
    :goto_45
    add-int/lit8 v12, v12, 0x1

    goto :goto_29

    .line 2148
    :cond_48
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    if-eqz v4, :cond_45

    .line 2149
    new-instance v6, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x2

    move/from16 v0, p4

    invoke-direct {v6, v4, v5, v7, v0}, Lcom/google/android/apps/plus/content/AvatarImageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2153
    .local v6, request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    new-instance v3, Lcom/google/android/apps/plus/api/DownloadImageOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/DownloadImageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 2155
    .local v3, op:Lcom/google/android/apps/plus/api/DownloadImageOperation;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->start()V

    .line 2156
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->hasError()Z

    move-result v4

    if-eqz v4, :cond_7c

    .line 2157
    const-string v4, "GooglePlusContactsSync"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->logError(Ljava/lang/String;)V

    .line 2160
    :cond_7c
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->getImageBytes()[B

    move-result-object v10

    .line 2161
    .local v10, bytes:[B
    if-eqz v10, :cond_45

    .line 2162
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, v19

    iget-wide v7, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    invoke-static {v4, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    .line 2164
    .local v16, rawContactUri:Landroid/net/Uri;
    const-string v4, "display_photo"

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 2167
    .local v15, rawContactPhotoUri:Landroid/net/Uri;
    :try_start_96
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v14

    .line 2168
    .local v14, os:Ljava/io/OutputStream;
    invoke-virtual {v14, v10}, Ljava/io/OutputStream;->write([B)V

    .line 2169
    invoke-virtual {v14}, Ljava/io/OutputStream;->close()V

    .line 2170
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->saveAvatarSignature(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;)V
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_96 .. :try_end_ab} :catch_ac

    goto :goto_45

    .line 2171
    .end local v14           #os:Ljava/io/OutputStream;
    :catch_ac
    move-exception v11

    .line 2172
    .local v11, e:Ljava/io/IOException;
    const-string v4, "GooglePlusContactsSync"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_45

    .line 2173
    const-string v4, "GooglePlusContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Could not store large avatar: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_45

    .line 2180
    .end local v3           #op:Lcom/google/android/apps/plus/api/DownloadImageOperation;
    .end local v6           #request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    .end local v10           #bytes:[B
    .end local v11           #e:Ljava/io/IOException;
    .end local v15           #rawContactPhotoUri:Landroid/net/Uri;
    .end local v16           #rawContactUri:Landroid/net/Uri;
    .end local v19           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :cond_d0
    move/from16 v13, v21

    .line 2181
    goto/16 :goto_14

    .line 2182
    .end local v12           #i:I
    .end local v21           #to:I
    :cond_d4
    return-void
.end method

.method private static dumpPersonActivityState(Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;)V
    .registers 10
    .parameter "state"

    .prologue
    const-wide/16 v7, 0x0

    .line 3512
    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[STREAM] Gaia ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  (raw_contact_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3514
    iget-object v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_30
    :goto_30
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .line 3516
    .local v0, activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v3, :cond_8d

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v3, v3, v7

    if-eqz v3, :cond_8d

    .line 3517
    const-string v2, "[DELETE]"

    .line 3526
    .local v2, op:Ljava/lang/String;
    :goto_48
    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (stream_item_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") created="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\', modified="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_30

    .line 3518
    .end local v2           #op:Ljava/lang/String;
    :cond_8d
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-eqz v3, :cond_9a

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v3, v3, v7

    if-nez v3, :cond_9a

    .line 3519
    const-string v2, "[INSERT]"

    .restart local v2       #op:Ljava/lang/String;
    goto :goto_48

    .line 3520
    .end local v2           #op:Ljava/lang/String;
    :cond_9a
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    if-eqz v3, :cond_30

    .line 3521
    const-string v2, "[UPDATE]"

    .restart local v2       #op:Ljava/lang/String;
    goto :goto_48

    .line 3530
    .end local v0           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v2           #op:Ljava/lang/String;
    :cond_a1
    return-void
.end method

.method private static dumpRawContactState(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;)V
    .registers 10
    .parameter "state"

    .prologue
    const-wide/16 v7, 0x0

    .line 3481
    iget-boolean v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-nez v3, :cond_b3

    .line 3482
    const-string v2, "[DELETE]"

    .line 3489
    .local v2, op:Ljava/lang/String;
    :goto_8
    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (raw_contact_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") last_updated="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3491
    iget-object v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_50
    :goto_50
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    .line 3492
    .local v0, dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    if-nez v3, :cond_c1

    .line 3493
    const-string v2, "[DELETE]"

    .line 3502
    :goto_62
    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (data_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\', type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_50

    .line 3483
    .end local v0           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #op:Ljava/lang/String;
    :cond_b3
    iget-wide v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    cmp-long v3, v3, v7

    if-nez v3, :cond_bd

    .line 3484
    const-string v2, "[INSERT]"

    .restart local v2       #op:Ljava/lang/String;
    goto/16 :goto_8

    .line 3486
    .end local v2           #op:Ljava/lang/String;
    :cond_bd
    const-string v2, "[UPDATE]"

    .restart local v2       #op:Ljava/lang/String;
    goto/16 :goto_8

    .line 3494
    .restart local v0       #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_c1
    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    cmp-long v3, v3, v7

    if-nez v3, :cond_ca

    .line 3495
    const-string v2, "[INSERT]"

    goto :goto_62

    .line 3496
    :cond_ca
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->changed:Z

    if-eqz v3, :cond_50

    .line 3497
    const-string v2, "[UPDATE]"

    goto :goto_62

    .line 3506
    .end local v0           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    :cond_d1
    return-void
.end method

.method private static findChangesInCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .registers 13
    .parameter "context"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 952
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 953
    .local v8, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 954
    .local v0, resolver:Landroid/content/ContentResolver;
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->GROUPS_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 955
    .local v6, cursor:Landroid/database/Cursor;
    if-nez v6, :cond_1b

    .line 983
    :goto_1a
    return-object v3

    .line 960
    :cond_1b
    :goto_1b
    :try_start_1b
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_47

    .line 961
    new-instance v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    const/4 v1, 0x0

    invoke-direct {v7, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;-><init>(B)V

    .line 962
    .local v7, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    .line 963
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    .line 964
    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    .line 965
    iget-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    invoke-virtual {v8, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_41
    .catchall {:try_start_1b .. :try_end_41} :catchall_42

    goto :goto_1b

    .line 968
    .end local v7           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    :catchall_42
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_47
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 971
    const-string v1, "plus"

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    .line 972
    .restart local v7       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    if-nez v7, :cond_6f

    .line 973
    new-instance v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    .end local v7           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    invoke-direct {v7, v9}, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;-><init>(B)V

    .line 974
    .restart local v7       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    const-string v1, "plus"

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    .line 975
    const v1, 0x7f0803b8

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    .line 976
    iput-boolean v10, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->exists:Z

    .line 977
    const-string v1, "plus"

    invoke-virtual {v8, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_6d
    move-object v3, v8

    .line 983
    goto :goto_1a

    .line 980
    :cond_6f
    const-string v1, "plus"

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6d
.end method

.method private static findChangesInContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .registers 16
    .parameter "context"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1164
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1165
    .local v0, resolver:Landroid/content/ContentResolver;
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 1167
    .local v1, rawContactsUri:Landroid/net/Uri;
    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1168
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_15

    .line 1169
    const/4 v13, 0x0

    .line 1218
    :goto_14
    return-object v13

    .line 1172
    :cond_15
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 1174
    .local v13, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    :goto_1a
    :try_start_1a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_46

    .line 1175
    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v2, 0x0

    invoke-direct {v12, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;-><init>(B)V

    .line 1176
    .local v12, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    .line 1177
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    .line 1178
    const/4 v2, 0x2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    .line 1179
    iget-object v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v13, v2, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_40
    .catchall {:try_start_1a .. :try_end_40} :catchall_41

    goto :goto_1a

    .line 1182
    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :catchall_41
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_46
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1185
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/service/AndroidContactsSync;->CONTACTS_PROJECTION:[Ljava/lang/String;

    const-string v5, "in_my_circles=1 AND profile_type!=2 AND for_sharing!=0"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1192
    if-eqz v8, :cond_99

    .line 1193
    :goto_5c
    :try_start_5c
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_99

    .line 1194
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1195
    .local v11, personId:Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 1196
    .local v9, lastUpdateTime:J
    invoke-virtual {v13, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1197
    .restart local v12       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    if-nez v12, :cond_8c

    .line 1198
    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    const/4 v2, 0x0

    invoke-direct {v12, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;-><init>(B)V

    .line 1199
    .restart local v12       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    iput-object v11, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    .line 1200
    iput-wide v9, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    .line 1201
    iget-object v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v13, v2, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1209
    :goto_83
    const/4 v2, 0x1

    iput-boolean v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z
    :try_end_86
    .catchall {:try_start_5c .. :try_end_86} :catchall_87

    goto :goto_5c

    .line 1213
    .end local v9           #lastUpdateTime:J
    .end local v11           #personId:Ljava/lang/String;
    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :catchall_87
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1203
    .restart local v9       #lastUpdateTime:J
    .restart local v11       #personId:Ljava/lang/String;
    .restart local v12       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_8c
    :try_start_8c
    iget-wide v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    cmp-long v2, v2, v9

    if-nez v2, :cond_96

    .line 1204
    invoke-virtual {v13, v11}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_83

    .line 1206
    :cond_96
    iput-wide v9, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J
    :try_end_98
    .catchall {:try_start_8c .. :try_end_98} :catchall_87

    goto :goto_83

    .line 1213
    .end local v9           #lastUpdateTime:J
    .end local v11           #personId:Ljava/lang/String;
    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_99
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1217
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_14
.end method

.method private static flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;
    .registers 9
    .parameter "resolver"
    .parameter
    .parameter "batchSize"
    .parameter "force"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;IZ)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    .local p1, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v1, 0x0

    .line 1847
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1848
    .local v2, size:I
    if-nez v2, :cond_8

    .line 1866
    :cond_7
    :goto_7
    return-object v1

    .line 1852
    :cond_8
    if-nez p3, :cond_c

    if-lt v2, p2, :cond_7

    .line 1856
    :cond_c
    const/4 v1, 0x0

    .line 1858
    .local v1, result:[Landroid/content/ContentProviderResult;
    :try_start_d
    const-string v3, "com.android.contacts"

    invoke-virtual {p0, v3, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_12} :catch_17

    move-result-object v1

    .line 1865
    :cond_13
    :goto_13
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    goto :goto_7

    .line 1859
    :catch_17
    move-exception v0

    .line 1860
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "GooglePlusContactsSync"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1861
    const-string v3, "GooglePlusContactsSync"

    const-string v4, "Cannot apply a batch of content provider operations"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_13
.end method

.method private static flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;
    .registers 4
    .parameter "resolver"
    .parameter
    .parameter "force"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;Z)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .prologue
    .line 1839
    .local p1, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/16 v0, 0x80

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v0

    return-object v0
.end method

.method private static getCircleIdMap(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .registers 10
    .parameter "resolver"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1300
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1301
    .local v6, circleIdMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->GROUPS_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1302
    .local v7, cursor:Landroid/database/Cursor;
    if-nez v7, :cond_16

    .line 1315
    :goto_15
    return-object v3

    .line 1307
    :cond_16
    :goto_16
    :try_start_16
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1308
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2d
    .catchall {:try_start_16 .. :try_end_2d} :catchall_2e

    goto :goto_16

    .line 1312
    :catchall_2e
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_33
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 1315
    goto :goto_15
.end method

.method private static getEntitiesUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .registers 4
    .parameter "account"

    .prologue
    .line 3332
    sget-object v0, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .registers 4
    .parameter "account"

    .prologue
    .line 3356
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getLimitedRawContactSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)[Ljava/lang/String;
    .registers 9
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v6, 0x0

    .line 3077
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 3078
    .local v1, rawContactIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "account_type"

    const-string v5, "com.google"

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "data_set"

    const-string v5, "plus"

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "account_name"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 3079
    .local v2, rawContactsUri:Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "raw_contact_id"

    aput-object v0, v3, v6

    .line 3080
    .local v3, projection:[Ljava/lang/String;
    const-string v4, "starred!=0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3083
    const-string v4, "starred=0 AND times_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const-string v5, "times_contacted DESC LIMIT 8"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3087
    const-string v4, "starred=0 AND last_time_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const-string v5, "last_time_contacted DESC LIMIT 8"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3091
    new-array v0, v6, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private static getMaxStreamItemsPerRawContact(Landroid/content/Context;)I
    .registers 9
    .parameter "context"

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2637
    sget v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sMaxStreamItemsPerRawContact:I

    if-eqz v0, :cond_9

    .line 2638
    sget v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sMaxStreamItemsPerRawContact:I

    .line 2651
    :goto_8
    return v7

    .line 2641
    :cond_9
    const/4 v7, 0x2

    .line 2642
    .local v7, limit:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_CONTENT_LIMIT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "max_items"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2645
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_1d
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 2646
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    sput v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sMaxStreamItemsPerRawContact:I
    :try_end_2a
    .catchall {:try_start_1d .. :try_end_2a} :catchall_2e

    .line 2649
    :cond_2a
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_8

    :catchall_2e
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .registers 10
    .parameter "resolver"
    .parameter "limitName"

    .prologue
    const/4 v2, 0x0

    .line 2380
    const/16 v6, 0x60

    .line 2381
    .local v6, avatarSize:I
    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->DISPLAY_PHOTO_CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2383
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_20

    .line 2385
    :try_start_f
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 2386
    invoke-interface {v7, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1c
    .catchall {:try_start_f .. :try_end_1c} :catchall_21

    move-result v6

    .line 2389
    :cond_1d
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2392
    :cond_20
    return v6

    .line 2389
    :catchall_21
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .registers 4
    .parameter "account"

    .prologue
    .line 3276
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .registers 4
    .parameter "account"

    .prologue
    .line 3311
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getStreamItemPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/content/DbMedia;)Ljava/util/ArrayList;
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "dbMedia"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "[",
            "Lcom/google/android/apps/plus/content/DbMedia;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v12, 0x6

    .line 2947
    const/4 v3, 0x0

    .line 2948
    .local v3, images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;>;"
    if-eqz p2, :cond_c0

    .line 2949
    const/4 v2, 0x0

    .local v2, i:I
    array-length v4, p2

    .local v4, size:I
    :goto_7
    if-ge v2, v4, :cond_c0

    .line 2950
    aget-object v8, p2, v2

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v5

    .line 2951
    .local v5, type:I
    aget-object v8, p2, v2

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8b

    const/4 v8, 0x3

    if-eq v8, v5, :cond_21

    const/4 v8, 0x2

    if-ne v8, v5, :cond_8b

    .line 2953
    :cond_21
    aget-object v8, p2, v2

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v6

    .line 2954
    .local v6, url:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_44

    const-string v8, "//"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_44

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "http:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .end local v6           #url:Ljava/lang/String;
    :cond_44
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d00ca

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-static {v8, v6}, Lcom/google/android/apps/plus/util/ImageUtils;->getCroppedAndResizedUrl(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;

    invoke-direct {v10, p0, p1, v9}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->start()V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->getException()Ljava/lang/Exception;

    move-result-object v9

    if-eqz v9, :cond_8f

    const-string v8, "GooglePlusContactsSync"

    invoke-static {v8, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_74

    const-string v8, "GooglePlusContactsSync"

    const-string v9, "Could not download image"

    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->getException()Ljava/lang/Exception;

    move-result-object v10

    invoke-static {v8, v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_74
    move-object v0, v7

    .line 2955
    .local v0, bytes:[B
    :goto_75
    if-eqz v0, :cond_8b

    .line 2956
    if-nez v3, :cond_7e

    .line 2957
    new-instance v3, Ljava/util/ArrayList;

    .end local v3           #images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2959
    .restart local v3       #images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;>;"
    :cond_7e
    new-instance v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;

    const/4 v8, 0x0

    invoke-direct {v1, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;-><init>(B)V

    .line 2960
    .local v1, container:Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;
    iput v2, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->mediaIndex:I

    .line 2961
    iput-object v0, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->imageBytes:[B

    .line 2962
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2949
    .end local v0           #bytes:[B
    .end local v1           #container:Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;
    :cond_8b
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7

    .line 2954
    :cond_8f
    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->hasError()Z

    move-result v9

    if-eqz v9, :cond_b7

    const-string v8, "GooglePlusContactsSync"

    invoke-static {v8, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_b5

    const-string v8, "GooglePlusContactsSync"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "Could not download image: "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->getErrorCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b5
    move-object v0, v7

    goto :goto_75

    :cond_b7
    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/DownloadPhotoOperation;->getBytes()[B

    move-result-object v9

    invoke-static {v9, v8}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeBitmapToHeight([BI)[B

    move-result-object v0

    goto :goto_75

    .line 2968
    .end local v2           #i:I
    .end local v4           #size:I
    .end local v5           #type:I
    :cond_c0
    return-object v3
.end method

.method private static getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .registers 4
    .parameter "account"

    .prologue
    .line 3377
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static insertNewContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1242
    .local p2, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    .local p3, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p4, circleIdMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1243
    .local v3, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1244
    .local v8, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    iget-boolean v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-eqz v0, :cond_d

    iget-wide v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_d

    .line 1245
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 1249
    .end local v8           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_29
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_38

    .line 1250
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V

    .line 1252
    :cond_38
    return-void
.end method

.method public static isAndroidSyncSupported(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    .prologue
    .line 3415
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_8

    const/4 v0, 0x0

    :goto_7
    return v0

    :cond_8
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isContactsProviderAvailable(Landroid/content/Context;)Z

    move-result v0

    goto :goto_7
.end method

.method public static isContactsProviderAvailable(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    .prologue
    const/4 v2, 0x1

    .line 3422
    sget-boolean v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderStatusKnown:Z

    if-eqz v3, :cond_8

    .line 3423
    sget-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderExists:Z

    .line 3438
    :goto_7
    return v2

    .line 3427
    :cond_8
    :try_start_8
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "com.android.contacts"

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v1

    .line 3429
    .local v1, provider:Landroid/content/ContentProviderClient;
    if-eqz v1, :cond_21

    :goto_14
    sput-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderExists:Z

    .line 3430
    if-eqz v1, :cond_1b

    .line 3431
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    .line 3433
    :cond_1b
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderStatusKnown:Z
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_1e} :catch_23

    .line 3438
    .end local v1           #provider:Landroid/content/ContentProviderClient;
    :goto_1e
    sget-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderExists:Z

    goto :goto_7

    .line 3429
    .restart local v1       #provider:Landroid/content/ContentProviderClient;
    :cond_21
    const/4 v2, 0x0

    goto :goto_14

    .line 3434
    .end local v1           #provider:Landroid/content/ContentProviderClient;
    :catch_23
    move-exception v0

    .line 3435
    .local v0, e:Ljava/lang/Throwable;
    const-string v2, "GooglePlusContactsSync"

    const-string v3, "Cannot determine availability of the contacts provider"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e
.end method

.method private static limitStreamItemsPerRawContact(Landroid/content/Context;Ljava/util/HashMap;)V
    .registers 9
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2619
    .local p1, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getMaxStreamItemsPerRawContact(Landroid/content/Context;)I

    move-result v3

    .line 2620
    .local v3, maxItems:I
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_38

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    .line 2621
    .local v4, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    iget-object v0, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    .line 2622
    .local v0, activities:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v3, :cond_c

    .line 2623
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_STATE_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2627
    move v1, v3

    .local v1, i:I
    :goto_26
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_c

    .line 2628
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    .line 2627
    add-int/lit8 v1, v1, 0x1

    goto :goto_26

    .line 2631
    .end local v0           #activities:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;>;"
    .end local v1           #i:I
    .end local v4           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :cond_38
    return-void
.end method

.method private static loadContactCircleMembership$57209d63([Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .registers 9
    .parameter "personIds"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1586
    .local p1, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    .local p2, circleIdMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    array-length v4, p0

    if-ge v2, v4, :cond_26

    .line 1587
    aget-object v4, p0, v2

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1588
    .local v3, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    const-string v4, "plus"

    invoke-virtual {p2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 1589
    .local v1, groupId:Ljava/lang/Long;
    if-eqz v1, :cond_23

    .line 1590
    const-string v4, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v0

    .line 1592
    .local v0, dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    .line 1586
    .end local v0           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    :cond_23
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1595
    .end local v1           #groupId:Ljava/lang/Long;
    .end local v3           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_26
    return-void
.end method

.method private static populateRawContactState(Landroid/content/Context;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;)V
    .registers 14
    .parameter "context"
    .parameter "state"
    .parameter "contactInfo"

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 1516
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    if-eqz v5, :cond_47

    .line 1517
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_47

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataEmail;

    .line 1518
    .local v2, email:Lcom/google/api/services/plusi/model/DataEmail;
    const-string v5, "vnd.android.cursor.item/email_v2"

    iget-object v7, v2, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    invoke-static {p1, v5, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    .line 1519
    .local v1, dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-boolean v10, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    .line 1520
    iget-object v5, v2, Lcom/google/api/services/plusi/model/DataEmail;->standardTag:Ljava/lang/Integer;

    iget-object v7, v2, Lcom/google/api/services/plusi/model/DataEmail;->customTag:Ljava/lang/String;

    if-nez v5, :cond_33

    move v5, v6

    :goto_29
    packed-switch v5, :pswitch_data_e6

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iput-object v7, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_c

    :cond_33
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_29

    :pswitch_38
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_c

    :pswitch_3d
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_c

    :pswitch_42
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_c

    .line 1524
    .end local v1           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v2           #email:Lcom/google/api/services/plusi/model/DataEmail;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_47
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    if-eqz v5, :cond_9f

    .line 1525
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_51
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataPhone;

    .line 1526
    .local v4, phone:Lcom/google/api/services/plusi/model/DataPhone;
    const-string v5, "vnd.android.cursor.item/phone_v2"

    iget-object v7, v4, Lcom/google/api/services/plusi/model/DataPhone;->value:Ljava/lang/String;

    invoke-static {p1, v5, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    .line 1527
    .restart local v1       #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-boolean v10, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    .line 1528
    iget-object v7, v4, Lcom/google/api/services/plusi/model/DataPhone;->standardTag:Ljava/lang/Integer;

    iget-object v8, v4, Lcom/google/api/services/plusi/model/DataPhone;->customTag:Ljava/lang/String;

    if-nez v7, :cond_73

    const/4 v5, 0x0

    :goto_6e
    if-eqz v5, :cond_80

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_51

    :cond_73
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto :goto_6e

    :cond_80
    if-eqz v7, :cond_98

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v7, 0x14

    if-ne v5, v7, :cond_98

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    const v5, 0x7f08023f

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_51

    :cond_98
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iput-object v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_51

    .line 1532
    .end local v1           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #phone:Lcom/google/api/services/plusi/model/DataPhone;
    :cond_9f
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->addresses:Ljava/util/List;

    if-eqz v5, :cond_e4

    .line 1533
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->addresses:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_a9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_e4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;

    .line 1534
    .local v0, address:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;
    const-string v5, "vnd.android.cursor.item/postal-address_v2"

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->value:Ljava/lang/String;

    invoke-static {p1, v5, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    .line 1536
    .restart local v1       #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-boolean v10, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    .line 1537
    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->standardTag:Ljava/lang/Integer;

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->customTag:Ljava/lang/String;

    if-nez v5, :cond_d0

    move v5, v6

    :goto_c6
    packed-switch v5, :pswitch_data_f0

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iput-object v7, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_a9

    :cond_d0
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_c6

    :pswitch_d5
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_a9

    :pswitch_da
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_a9

    :pswitch_df
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_a9

    .line 1540
    .end local v0           #address:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;
    .end local v1           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_e4
    return-void

    .line 1520
    nop

    :pswitch_data_e6
    .packed-switch 0x1
        :pswitch_38
        :pswitch_3d
        :pswitch_42
    .end packed-switch

    .line 1537
    :pswitch_data_f0
    .packed-switch 0x1
        :pswitch_d5
        :pswitch_da
        :pswitch_df
    .end packed-switch
.end method

.method private static populateRawContactState$5160c72c(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/api/services/plusi/model/Contacts;)V
    .registers 11
    .parameter "state"
    .parameter "contact"

    .prologue
    const/4 v8, 0x1

    .line 1548
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    if-eqz v5, :cond_67

    .line 1549
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_67

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/TaggedEmail;

    .line 1550
    .local v2, email:Lcom/google/api/services/plusi/model/TaggedEmail;
    const-string v5, "vnd.android.cursor.item/email_v2"

    iget-object v6, v2, Lcom/google/api/services/plusi/model/TaggedEmail;->value:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    .line 1551
    .local v1, dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-boolean v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    .line 1552
    iget-object v5, v2, Lcom/google/api/services/plusi/model/TaggedEmail;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    if-eqz v5, :cond_b

    .line 1553
    iget-object v5, v2, Lcom/google/api/services/plusi/model/TaggedEmail;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    const-string v6, "HOME"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_36

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_b

    :cond_36
    const-string v6, "WORK"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_45

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_b

    :cond_45
    const-string v6, "OTHER"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_54

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_b

    :cond_54
    const-string v6, "CUSTOM"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v6, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_b

    .line 1558
    .end local v1           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v2           #email:Lcom/google/api/services/plusi/model/TaggedEmail;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_67
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    if-eqz v5, :cond_cd

    .line 1559
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_71
    :goto_71
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_cd

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/TaggedPhone;

    .line 1560
    .local v4, phone:Lcom/google/api/services/plusi/model/TaggedPhone;
    const-string v5, "vnd.android.cursor.item/phone_v2"

    iget-object v6, v4, Lcom/google/api/services/plusi/model/TaggedPhone;->value:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    .line 1561
    .restart local v1       #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-boolean v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    .line 1562
    iget-object v5, v4, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    if-eqz v5, :cond_71

    .line 1563
    iget-object v5, v4, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    const-string v6, "HOME"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9c

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_71

    :cond_9c
    const-string v6, "WORK"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_ab

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_71

    :cond_ab
    const-string v6, "OTHER"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_ba

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_71

    :cond_ba
    const-string v6, "CUSTOM"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_71

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v6, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_71

    .line 1568
    .end local v1           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #phone:Lcom/google/api/services/plusi/model/TaggedPhone;
    :cond_cd
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    if-eqz v5, :cond_133

    .line 1569
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_d7
    :goto_d7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_133

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/TaggedAddress;

    .line 1570
    .local v0, address:Lcom/google/api/services/plusi/model/TaggedAddress;
    const-string v5, "vnd.android.cursor.item/postal-address_v2"

    iget-object v6, v0, Lcom/google/api/services/plusi/model/TaggedAddress;->value:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    .line 1572
    .restart local v1       #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-boolean v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    .line 1573
    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedAddress;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    if-eqz v5, :cond_d7

    .line 1574
    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedAddress;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    const-string v6, "HOME"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_102

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_d7

    :cond_102
    const-string v6, "WORK"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_111

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_d7

    :cond_111
    const-string v6, "OTHER"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_120

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_d7

    :cond_120
    const-string v6, "CUSTOM"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d7

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v6, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_d7

    .line 1578
    .end local v0           #address:Lcom/google/api/services/plusi/model/TaggedAddress;
    .end local v1           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_133
    return-void
.end method

.method private static queryRawContactsRequiringActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .registers 15
    .parameter "context"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2472
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getLimitedRawContactSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)[Ljava/lang/String;

    move-result-object v4

    .line 2473
    .local v4, rawContactIds:[Ljava/lang/String;
    array-length v0, v4

    if-nez v0, :cond_9

    .line 2507
    :goto_8
    return-object v5

    .line 2477
    :cond_9
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 2480
    .local v12, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 2481
    .local v10, sb:Ljava/lang/StringBuilder;
    const-string v0, "_id IN ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2482
    const/4 v8, 0x0

    .local v8, i:I
    :goto_19
    array-length v0, v4

    if-ge v8, v0, :cond_2b

    .line 2483
    if-eqz v8, :cond_23

    .line 2484
    const/16 v0, 0x2c

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2486
    :cond_23
    const/16 v0, 0x3f

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2482
    add-int/lit8 v8, v8, 0x1

    goto :goto_19

    .line 2488
    :cond_2b
    const/16 v0, 0x29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2490
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_SOURCE_ID_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2493
    .local v6, cursor:Landroid/database/Cursor;
    :cond_42
    :goto_42
    :try_start_42
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 2494
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2495
    .local v9, personId:Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2496
    .local v7, gaiaId:Ljava/lang/String;
    if-eqz v7, :cond_42

    .line 2497
    new-instance v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    const/4 v0, 0x0

    invoke-direct {v11, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;-><init>(B)V

    .line 2498
    .local v11, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    iput-object v7, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    .line 2499
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    .line 2500
    invoke-virtual {v12, v7, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_65
    .catchall {:try_start_42 .. :try_end_65} :catchall_66

    goto :goto_42

    .line 2504
    .end local v7           #gaiaId:Ljava/lang/String;
    .end local v9           #personId:Ljava/lang/String;
    .end local v11           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :catchall_66
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_6b
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v5, v12

    .line 2507
    goto :goto_8
.end method

.method private static queryRawContactsRequiringLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .registers 16
    .parameter "context"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2040
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getLimitedRawContactSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)[Ljava/lang/String;

    move-result-object v4

    .line 2041
    .local v4, rawContactIds:[Ljava/lang/String;
    array-length v1, v4

    if-nez v1, :cond_9

    .line 2088
    :cond_8
    :goto_8
    return-object v5

    .line 2045
    :cond_9
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 2048
    .local v13, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2049
    .local v0, resolver:Landroid/content/ContentResolver;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 2050
    .local v11, sb:Ljava/lang/StringBuilder;
    const-string v1, "_id IN ("

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2051
    const/4 v9, 0x0

    .local v9, i:I
    :goto_1d
    array-length v1, v4

    if-ge v9, v1, :cond_2f

    .line 2052
    if-eqz v9, :cond_27

    .line 2053
    const/16 v1, 0x2c

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2055
    :cond_27
    const/16 v1, 0x3f

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2051
    add-int/lit8 v9, v9, 0x1

    goto :goto_1d

    .line 2057
    :cond_2f
    const/16 v1, 0x29

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2060
    const-string v1, " OR sync2"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2061
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->LARGE_AVATAR_RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2063
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_8

    .line 2068
    :cond_4f
    :goto_4f
    :try_start_4f
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7f

    .line 2069
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2070
    .local v10, personId:Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2071
    .local v7, gaiaId:Ljava/lang/String;
    if-eqz v7, :cond_4f

    .line 2072
    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    const/4 v1, 0x0

    invoke-direct {v12, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;-><init>(B)V

    .line 2073
    .local v12, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    iput-object v7, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    .line 2074
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    .line 2075
    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    .line 2076
    invoke-virtual {v13, v7, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_79
    .catchall {:try_start_4f .. :try_end_79} :catchall_7a

    goto :goto_4f

    .line 2080
    .end local v7           #gaiaId:Ljava/lang/String;
    .end local v10           #personId:Ljava/lang/String;
    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :catchall_7a
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_7f
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2083
    invoke-virtual {v13}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_94

    .line 2084
    new-instance v8, Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2085
    .local v8, gaiaIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p0, p1, v13, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->retrieveAvatarSignatures(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    .end local v8           #gaiaIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_94
    move-object v5, v13

    .line 2088
    goto/16 :goto_8
.end method

.method private static queryRawContactsRequiringThumbnails(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .registers 16
    .parameter "resolver"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2321
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getEntitiesUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->THUMBNAILS_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    const-string v3, "(mimetype=\'vnd.android.cursor.item/photo\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\') AND (sync2=0 OR sync2 IS NULL)"

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2323
    .local v7, cursor:Landroid/database/Cursor;
    if-nez v7, :cond_12

    .line 2373
    :goto_11
    return-object v4

    .line 2327
    :cond_12
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 2329
    .local v13, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;>;"
    :cond_17
    :goto_17
    :try_start_17
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_63

    .line 2330
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2331
    .local v10, personId:Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2332
    .local v8, gaiaId:Ljava/lang/String;
    if-eqz v8, :cond_17

    .line 2333
    invoke-virtual {v13, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    .line 2334
    .local v12, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    if-nez v12, :cond_49

    .line 2335
    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    const/4 v0, 0x0

    invoke-direct {v12, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;-><init>(B)V

    .line 2336
    .restart local v12       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    iput-object v8, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    .line 2337
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    .line 2338
    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    .line 2339
    invoke-virtual {v13, v8, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2342
    :cond_49
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2343
    .local v9, mimetype:Ljava/lang/String;
    const-string v0, "vnd.android.cursor.item/photo"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2344
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->dataId:J
    :try_end_5d
    .catchall {:try_start_17 .. :try_end_5d} :catchall_5e

    goto :goto_17

    .line 2349
    .end local v8           #gaiaId:Ljava/lang/String;
    .end local v9           #mimetype:Ljava/lang/String;
    .end local v10           #personId:Ljava/lang/String;
    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :catchall_5e
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_63
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2352
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    const-string v3, "in_my_circles=1"

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2355
    :cond_76
    :goto_76
    :try_start_76
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 2356
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2357
    .restart local v8       #gaiaId:Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2358
    .local v6, avatarUrl:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarUrlSignature(Ljava/lang/String;)I

    move-result v11

    .line 2359
    .local v11, signature:I
    invoke-virtual {v13, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    .line 2360
    .restart local v12       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    if-eqz v12, :cond_76

    .line 2361
    iget v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    if-ne v0, v11, :cond_9f

    .line 2362
    invoke-virtual {v13, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_99
    .catchall {:try_start_76 .. :try_end_99} :catchall_9a

    goto :goto_76

    .line 2370
    .end local v6           #avatarUrl:Ljava/lang/String;
    .end local v8           #gaiaId:Ljava/lang/String;
    .end local v11           #signature:I
    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :catchall_9a
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2364
    .restart local v6       #avatarUrl:Ljava/lang/String;
    .restart local v8       #gaiaId:Ljava/lang/String;
    .restart local v11       #signature:I
    .restart local v12       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :cond_9f
    :try_start_9f
    iput v11, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    .line 2365
    iput-object v6, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;
    :try_end_a3
    .catchall {:try_start_9f .. :try_end_a3} :catchall_9a

    goto :goto_76

    .line 2370
    .end local v6           #avatarUrl:Ljava/lang/String;
    .end local v8           #gaiaId:Ljava/lang/String;
    .end local v11           #signature:I
    .end local v12           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :cond_a4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v4, v13

    .line 2373
    goto/16 :goto_11
.end method

.method private static queryStreamItemState$373650f4(Landroid/content/Context;Ljava/util/HashMap;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 16
    .parameter "context"
    .parameter
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2527
    .local p1, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2528
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2530
    .local v7, cursor:Landroid/database/Cursor;
    if-nez v7, :cond_12

    .line 2531
    const/4 v1, 0x0

    .line 2557
    :goto_11
    return v1

    .line 2535
    :cond_12
    :goto_12
    :try_start_12
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5c

    .line 2536
    const/4 v1, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2537
    .local v9, personId:Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2538
    .local v8, gaiaId:Ljava/lang/String;
    if-eqz v8, :cond_12

    .line 2539
    invoke-virtual {p1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    .line 2540
    .local v10, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    if-eqz v10, :cond_12

    .line 2541
    new-instance v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    const/4 v1, 0x0

    invoke-direct {v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;-><init>(B)V

    .line 2542
    .local v6, activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    iget-wide v1, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    .line 2543
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    .line 2544
    const/4 v1, 0x2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    .line 2546
    const/4 v1, 0x3

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    .line 2547
    const/4 v1, 0x4

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    .line 2549
    iget-object v1, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_56
    .catchall {:try_start_12 .. :try_end_56} :catchall_57

    goto :goto_12

    .line 2554
    .end local v6           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v8           #gaiaId:Ljava/lang/String;
    .end local v9           #personId:Ljava/lang/String;
    .end local v10           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :catchall_57
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_5c
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2557
    const/4 v1, 0x1

    goto :goto_11
.end method

.method private static reconcileActivitiesAndStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 23
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter "selection"
    .parameter "selectionArgs"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2567
    .local p2, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2569
    .local v1, resolver:Landroid/content/ContentResolver;
    if-eqz p3, :cond_86

    .line 2570
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2575
    .local v4, where:Ljava/lang/String;
    :goto_1d
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_SUMMARY_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 2579
    .local v12, cursor:Landroid/database/Cursor;
    :cond_2e
    :goto_2e
    :try_start_2e
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_a2

    .line 2580
    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 2581
    .local v13, gaiaId:Ljava/lang/String;
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2582
    .local v8, activityId:Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 2583
    .local v10, created:J
    const/4 v2, 0x3

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 2584
    .local v15, lastModified:J
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    .line 2585
    .local v17, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    if-eqz v17, :cond_2e

    .line 2586
    const/4 v9, 0x0

    .line 2587
    .local v9, activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, i$:Ljava/util/Iterator;
    :cond_5b
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_70

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .line 2588
    .local v7, aState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    iget-object v2, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 2589
    move-object v9, v7

    .line 2593
    .end local v7           #aState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    :cond_70
    if-eqz v9, :cond_88

    .line 2594
    iget-wide v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    cmp-long v2, v2, v15

    if-eqz v2, :cond_7d

    .line 2595
    const/4 v2, 0x1

    iput-boolean v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    .line 2596
    iput-wide v15, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    .line 2606
    :cond_7d
    :goto_7d
    const/4 v2, 0x1

    iput-boolean v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z
    :try_end_80
    .catchall {:try_start_2e .. :try_end_80} :catchall_81

    goto :goto_2e

    .line 2610
    .end local v8           #activityId:Ljava/lang/String;
    .end local v9           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v10           #created:J
    .end local v13           #gaiaId:Ljava/lang/String;
    .end local v14           #i$:Ljava/util/Iterator;
    .end local v15           #lastModified:J
    .end local v17           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :catchall_81
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    .line 2572
    .end local v4           #where:Ljava/lang/String;
    .end local v12           #cursor:Landroid/database/Cursor;
    :cond_86
    const/4 v4, 0x0

    .restart local v4       #where:Ljava/lang/String;
    goto :goto_1d

    .line 2599
    .restart local v8       #activityId:Ljava/lang/String;
    .restart local v9       #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .restart local v10       #created:J
    .restart local v12       #cursor:Landroid/database/Cursor;
    .restart local v13       #gaiaId:Ljava/lang/String;
    .restart local v14       #i$:Ljava/util/Iterator;
    .restart local v15       #lastModified:J
    .restart local v17       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :cond_88
    :try_start_88
    new-instance v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .end local v9           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    const/4 v2, 0x0

    invoke-direct {v9, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;-><init>(B)V

    .line 2600
    .restart local v9       #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    move-object/from16 v0, v17

    iget-wide v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    iput-wide v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    .line 2601
    iput-object v8, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    .line 2602
    iput-wide v10, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    .line 2603
    iput-wide v15, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    .line 2604
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_a1
    .catchall {:try_start_88 .. :try_end_a1} :catchall_81

    goto :goto_7d

    .line 2610
    .end local v8           #activityId:Ljava/lang/String;
    .end local v9           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v10           #created:J
    .end local v13           #gaiaId:Ljava/lang/String;
    .end local v14           #i$:Ljava/util/Iterator;
    .end local v15           #lastModified:J
    .end local v17           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :cond_a2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 2611
    return-void
.end method

.method private static reconcileContacts(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/util/HashMap;)Z
    .registers 21
    .parameter "resolver"
    .parameter "entitiesUri"
    .parameter "personIds"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1614
    .local p3, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    move-object/from16 v0, p2

    array-length v1, v0

    new-array v5, v1, [Ljava/lang/String;

    .line 1615
    .local v5, args:[Ljava/lang/String;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 1616
    .local v16, sb:Ljava/lang/StringBuilder;
    const-string v1, "_id IN ("

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1617
    const/4 v14, 0x0

    .local v14, i:I
    :goto_12
    move-object/from16 v0, p2

    array-length v1, v0

    if-ge v14, v1, :cond_3c

    .line 1618
    aget-object v1, p2, v14

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    iget-wide v1, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v14

    .line 1619
    if-eqz v14, :cond_32

    .line 1620
    const/16 v1, 0x2c

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1622
    :cond_32
    const/16 v1, 0x3f

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1617
    add-int/lit8 v14, v14, 0x1

    goto :goto_12

    .line 1624
    :cond_3c
    const-string v1, ") AND mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1626
    sget-object v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ENTITIES_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1627
    .local v13, cursor:Landroid/database/Cursor;
    if-nez v13, :cond_56

    .line 1628
    const/4 v1, 0x0

    .line 1645
    :goto_55
    return v1

    .line 1632
    :cond_56
    :goto_56
    :try_start_56
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_8b

    .line 1633
    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1634
    .local v15, personId:Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1635
    .local v6, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    const/4 v1, 0x2

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v1, 0x3

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v1, 0x4

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v1, 0x5

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {v6 .. v12}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_85
    .catchall {:try_start_56 .. :try_end_85} :catchall_86

    goto :goto_56

    .line 1642
    .end local v6           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    .end local v15           #personId:Ljava/lang/String;
    :catchall_86
    move-exception v1

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_8b
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1645
    const/4 v1, 0x1

    goto :goto_55
.end method

.method private static reconcileData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "state"
    .parameter "dataId"
    .parameter "mimetype"
    .parameter "data1"
    .parameter "data2"
    .parameter "data3"

    .prologue
    .line 1653
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1654
    const/4 p4, 0x0

    .line 1656
    :cond_7
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1657
    const/4 p5, 0x0

    .line 1659
    :cond_e
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1660
    const/4 p6, 0x0

    .line 1663
    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_1b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_59

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    .line 1664
    .local v0, dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    invoke-static {v2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1b

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1b

    iget-wide v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1b

    .line 1666
    iput-wide p1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    .line 1669
    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    invoke-static {v2, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_51

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    invoke-static {v2, p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_58

    .line 1671
    :cond_51
    iput-object p5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    .line 1672
    iput-object p6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    .line 1673
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->changed:Z

    .line 1683
    :cond_58
    :goto_58
    return-void

    .line 1680
    .end local v0           #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    :cond_59
    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;-><init>(B)V

    .line 1681
    .restart local v0       #dataState:Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    iput-wide p1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    .line 1682
    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_58
.end method

.method public static declared-synchronized requestSync(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 850
    const-class v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_4
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;Z)V
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_9

    .line 851
    monitor-exit v0

    return-void

    .line 850
    :catchall_9
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized requestSync(Landroid/content/Context;Z)V
    .registers 5
    .parameter "context"
    .parameter "immediate"

    .prologue
    .line 857
    const-class v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;

    monitor-enter v1

    :try_start_3
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_1e

    move-result v0

    if-nez v0, :cond_b

    .line 867
    :goto_9
    monitor-exit v1

    return-void

    .line 861
    :cond_b
    :try_start_b
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    if-nez v0, :cond_21

    .line 862
    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;-><init>(Landroid/content/Context;)V

    .line 863
    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->start()V
    :try_end_1d
    .catchall {:try_start_b .. :try_end_1d} :catchall_1e

    goto :goto_9

    .line 857
    :catchall_1e
    move-exception v0

    monitor-exit v1

    throw v0

    .line 865
    :cond_21
    :try_start_21
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->requestSync(Z)V
    :try_end_26
    .catchall {:try_start_21 .. :try_end_26} :catchall_1e

    goto :goto_9
.end method

.method private static resizeThumbnail([BI)[B
    .registers 8
    .parameter "bytes"
    .parameter "thumbnailSize"

    .prologue
    const/4 v3, 0x0

    .line 2288
    if-nez p0, :cond_5

    move-object p0, v3

    .line 2312
    .end local p0
    :goto_4
    return-object p0

    .line 2292
    .restart local p0
    :cond_5
    const/4 v4, 0x0

    array-length v5, p0

    invoke-static {p0, v4, v5}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2293
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_f

    move-object p0, v3

    .line 2294
    goto :goto_4

    .line 2296
    :cond_f
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-gt v4, p1, :cond_1f

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-gt v4, p1, :cond_1f

    .line 2297
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_4

    .line 2301
    :cond_1f
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2302
    .local v2, thumbnail:Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2303
    if-nez v2, :cond_2a

    move-object p0, v3

    .line 2306
    goto :goto_4

    .line 2309
    :cond_2a
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0xfa0

    invoke-direct {v1, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 2310
    .local v1, stream:Ljava/io/ByteArrayOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2311
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 2312
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    goto :goto_4
.end method

.method private static retrieveAvatarSignatures(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .registers 18
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2097
    .local p2, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;>;"
    .local p3, gaiaIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v5, v1, [Ljava/lang/String;

    .line 2098
    .local v5, args:[Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 2099
    .local v11, sb:Ljava/lang/StringBuilder;
    const-string v1, "gaia_id IN ("

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2100
    const/4 v10, 0x0

    .local v10, i:I
    :goto_11
    array-length v1, v5

    if-ge v10, v1, :cond_31

    .line 2101
    if-eqz v10, :cond_1b

    .line 2102
    const/16 v1, 0x2c

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2104
    :cond_1b
    const/16 v1, 0x3f

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2105
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v10

    .line 2100
    add-int/lit8 v10, v10, 0x1

    goto :goto_11

    .line 2107
    :cond_31
    const-string v1, ")"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2108
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2112
    .local v8, cursor:Landroid/database/Cursor;
    :goto_4b
    :try_start_4b
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7b

    .line 2113
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2114
    .local v9, gaiaId:Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2115
    .local v7, avatarUrl:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarUrlSignature(Ljava/lang/String;)I

    move-result v12

    .line 2116
    .local v12, signature:I
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    .line 2117
    .local v13, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    iget v1, v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    if-ne v1, v12, :cond_76

    .line 2118
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_70
    .catchall {:try_start_4b .. :try_end_70} :catchall_71

    goto :goto_4b

    .line 2125
    .end local v7           #avatarUrl:Ljava/lang/String;
    .end local v9           #gaiaId:Ljava/lang/String;
    .end local v12           #signature:I
    .end local v13           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :catchall_71
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1

    .line 2120
    .restart local v7       #avatarUrl:Ljava/lang/String;
    .restart local v9       #gaiaId:Ljava/lang/String;
    .restart local v12       #signature:I
    .restart local v13       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :cond_76
    :try_start_76
    iput v12, v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    .line 2121
    iput-object v7, v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;
    :try_end_7a
    .catchall {:try_start_76 .. :try_end_7a} :catchall_71

    goto :goto_4b

    .line 2125
    .end local v7           #avatarUrl:Ljava/lang/String;
    .end local v9           #gaiaId:Ljava/lang/String;
    .end local v12           #signature:I
    .end local v13           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :cond_7b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2126
    return-void
.end method

.method private static saveAvatarSignature(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;)V
    .registers 9
    .parameter "resolver"
    .parameter "account"
    .parameter "state"

    .prologue
    const/4 v5, 0x0

    .line 2189
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    iget-wide v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2191
    .local v0, rawContactUri:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2192
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "sync2"

    iget v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2193
    const-string v2, "sync3"

    iget v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2194
    invoke-virtual {p0, v0, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2195
    return-void
.end method

.method private static shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z
    .registers 4
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    .line 3405
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_16

    if-eqz p2, :cond_14

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private static syncActivitiesForRawContact$520c64bf(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "rawContactId"
    .parameter "gaiaId"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2422
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2423
    .local v1, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;-><init>(B)V

    .line 2424
    .local v0, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    iput-object p4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    .line 2425
    iput-wide p2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    .line 2426
    invoke-virtual {v1, p4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2428
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "stream_items"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2431
    .local v2, uri:Landroid/net/Uri;
    invoke-static {p0, v1, v2, v6, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryStreamItemState$373650f4(Landroid/content/Context;Ljava/util/HashMap;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_40

    .line 2432
    const-string v3, "author_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p4, v4, v5

    invoke-static {p0, p1, v1, v3, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileActivitiesAndStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    .line 2435
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->limitStreamItemsPerRawContact(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 2436
    invoke-static {v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->cleanUpActivityStateMap(Ljava/util/HashMap;)V

    .line 2438
    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_40

    .line 2439
    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->applyActivityChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V

    .line 2442
    :cond_40
    return-void
.end method

.method protected static syncContactsForCurrentAccount(Landroid/content/Context;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 12
    .parameter "context"
    .parameter "syncState"

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 818
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 843
    :cond_e
    :goto_e
    return-void

    .line 822
    :cond_f
    const-string v0, "Android contacts sync"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    .line 824
    :try_start_14
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    .line 825
    .local v6, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncClean(Landroid/content/Context;)Z
    :try_end_21
    .catchall {:try_start_14 .. :try_end_21} :catchall_44

    move-result v0

    if-nez v0, :cond_2b

    :try_start_24
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidContactsForOtherAccounts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncCleanupStatus(Landroid/content/Context;Z)V
    :try_end_2b
    .catchall {:try_start_24 .. :try_end_2b} :catchall_44
    .catch Ljava/lang/Throwable; {:try_start_24 .. :try_end_2b} :catch_3b

    :cond_2b
    :goto_2b
    :try_start_2b
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncClean(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_35

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncCleanupStatus(Landroid/content/Context;Z)V
    :try_end_35
    .catchall {:try_start_2b .. :try_end_35} :catchall_44

    .line 826
    :cond_35
    if-nez v6, :cond_49

    .line 842
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_e

    .line 825
    :catch_3b
    move-exception v0

    :try_start_3c
    const-string v1, "GooglePlusContactsSync"

    const-string v2, "Failed to clear out contacts"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_43
    .catchall {:try_start_3c .. :try_end_43} :catchall_44

    goto :goto_2b

    .line 842
    .end local v6           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :catchall_44
    move-exception v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0

    .line 830
    .restart local v6       #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_49
    :try_start_49
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5b

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_5b

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 831
    :cond_5b
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_15b

    .line 832
    :cond_67
    :goto_67
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_73

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_197

    .line 834
    :cond_73
    :goto_73
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_156

    .line 835
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_93

    const-string v0, "Android:Circles"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->findChangesInCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_8d

    invoke-static {p0, v6, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->applyCircleChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V

    :cond_8d
    if-nez v0, :cond_1ec

    move v0, v7

    :goto_90
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    .line 836
    :cond_93
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_ca

    const-string v0, "Android:Contacts"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->findChangesInContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getCircleIdMap(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v0, :cond_c4

    if-eqz v1, :cond_c4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2, v6, v0, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteRemovedContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-static {p0, v6, v0, v3, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->insertNewContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V

    invoke-static {p0, v6, v0, v3, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateChangedContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V

    const/4 v1, 0x1

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    :cond_c4
    if-nez v0, :cond_1f2

    move v0, v7

    :goto_c7
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    .line 837
    :cond_ca
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_125

    const-string v0, "Android:LargeAvatars"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "display_max_dim"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "large.avatar.size"

    const/16 v3, 0x100

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v0, v2, :cond_109

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "sync2"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v9, 0x0

    invoke-virtual {v3, v4, v2, v5, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_109
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "large.avatar.size"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryRawContactsRequiringLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_122

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1f8

    :cond_122
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 838
    :cond_125
    :goto_125
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_153

    const-string v0, "Android:Activities"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->hasPublicCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_23a

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryRawContactsRequiringActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_142

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_204

    :cond_142
    :goto_142
    if-eqz v0, :cond_14d

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14d

    invoke-static {p0, v6, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->applyActivityChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V

    :cond_14d
    if-nez v0, :cond_234

    move v0, v7

    :goto_150
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    .line 839
    :cond_153
    :goto_153
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncSmallAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    :try_end_156
    .catchall {:try_start_49 .. :try_end_156} :catchall_44

    .line 842
    :cond_156
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto/16 :goto_e

    .line 831
    :cond_15b
    :try_start_15b
    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryContactsSyncVersion(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    move-result v0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_67

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "contacts_sync_version"

    const/16 v3, 0xd

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "account_status"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_67

    .line 832
    :cond_197
    const-string v0, "Android:MyProfile"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->MY_PROFILE_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1b6
    .catchall {:try_start_15b .. :try_end_1b6} :catchall_44

    move-result-object v9

    :try_start_1b7
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1df

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1df

    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1df

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    move-object v0, p0

    move-object v1, v6

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)V
    :try_end_1df
    .catchall {:try_start_1b7 .. :try_end_1df} :catchall_1e7

    :cond_1df
    :try_start_1df
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    goto/16 :goto_73

    :catchall_1e7
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    .line 835
    :cond_1ec
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto/16 :goto_90

    .line 836
    :cond_1f2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto/16 :goto_c7

    .line 837
    :cond_1f8
    invoke-static {p0, v6, p1, v1, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->downloadLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/HashMap;I)V

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    goto/16 :goto_125

    .line 838
    :cond_204
    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=? AND data_set=?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "com.google"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "plus"

    aput-object v5, v3, v4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryStreamItemState$373650f4(Landroid/content/Context;Ljava/util/HashMap;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_227

    move-object v0, v8

    goto/16 :goto_142

    :cond_227
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v6, v0, v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileActivitiesAndStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->limitStreamItemsPerRawContact(Landroid/content/Context;Ljava/util/HashMap;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->cleanUpActivityStateMap(Ljava/util/HashMap;)V

    goto/16 :goto_142

    :cond_234
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto/16 :goto_150

    :cond_23a
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=? AND data_set=?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "com.google"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "plus"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V
    :try_end_25f
    .catchall {:try_start_1df .. :try_end_25f} :catchall_44

    goto/16 :goto_153
.end method

.method public static syncRawContact(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 4
    .parameter "context"
    .parameter "rawContactUri"

    .prologue
    .line 1055
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 1056
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_7

    .line 1065
    :cond_6
    :goto_6
    return-void

    .line 1060
    :cond_7
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1064
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncRawContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V

    goto :goto_6
.end method

.method private static syncRawContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V
    .registers 27
    .parameter "context"
    .parameter "account"
    .parameter "rawContactUri"

    .prologue
    .line 1071
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_REFRESH_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p2

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 1073
    .local v17, cursor:Landroid/database/Cursor;
    if-nez v17, :cond_12

    .line 1127
    :cond_11
    :goto_11
    return-void

    .line 1077
    :cond_12
    const-wide/16 v20, 0x0

    .line 1078
    .local v20, rawContactId:J
    const/16 v19, 0x0

    .line 1079
    .local v19, personId:Ljava/lang/String;
    const/16 v16, 0x0

    .line 1080
    .local v16, avatarSignature:I
    const-wide/16 v22, 0x0

    .line 1083
    .local v22, streamLastUpdated:J
    :try_start_1a
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_a4

    .line 1084
    const/4 v4, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1085
    .local v14, accountName:Ljava/lang/String;
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1086
    .local v15, accountType:Ljava/lang/String;
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1087
    .local v18, dataSet:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_51

    const-string v4, "com.google"

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_51

    const-string v4, "plus"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_88

    .line 1089
    :cond_51
    const-string v4, "GooglePlusContactsSync"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_84

    .line 1090
    const-string v4, "GooglePlusContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot refresh raw contact. It does not belong to the current account: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_84
    .catchall {:try_start_1a .. :try_end_84} :catchall_17a

    .line 1102
    :cond_84
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto :goto_11

    .line 1096
    :cond_88
    const/4 v4, 0x0

    :try_start_89
    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1097
    const/4 v4, 0x4

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1098
    const/4 v4, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1099
    const/4 v4, 0x6

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_a3
    .catchall {:try_start_89 .. :try_end_a3} :catchall_17a

    move-result-wide v22

    .line 1102
    .end local v14           #accountName:Ljava/lang/String;
    .end local v15           #accountType:Ljava/lang/String;
    .end local v18           #dataSet:Ljava/lang/String;
    :cond_a4
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1105
    const-wide/16 v4, 0x0

    cmp-long v4, v20, v4

    if-eqz v4, :cond_11

    .line 1109
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1110
    .local v8, gaiaId:Ljava/lang/String;
    if-eqz v8, :cond_11

    .line 1114
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c6

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1118
    :cond_c6
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;-><init>(B)V

    iput-object v8, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    move-wide/from16 v0, v20

    iput-wide v0, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    move/from16 v0, v16

    iput v0, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-virtual {v4, v8, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->retrieveAvatarSignatures(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_112

    new-instance v5, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v6, "Large avatar sync for a single raw contact"

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "display_max_dim"

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5, v4, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->downloadLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/HashMap;I)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    .line 1120
    :cond_112
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->hasPublicCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v22

    const-wide/32 v6, 0x493e0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1a0

    .line 1122
    new-instance v13, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v4, "Single raw contact sync"

    invoke-virtual {v13, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v4, "Activities:Sync"

    invoke-virtual {v13, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    :try_start_134
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getMaxStreamItemsPerRawContact(Landroid/content/Context;)I

    move-result v11

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v13}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_144
    .catchall {:try_start_134 .. :try_end_144} :catchall_198
    .catch Ljava/lang/Exception; {:try_start_134 .. :try_end_144} :catch_17f

    invoke-virtual {v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "sync4"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    move-wide/from16 v0, v20

    invoke-static {v6, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v4, v7, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v20

    invoke-static {v0, v1, v2, v3, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncActivitiesForRawContact$520c64bf(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)V

    goto/16 :goto_11

    .line 1102
    .end local v8           #gaiaId:Ljava/lang/String;
    :catchall_17a
    move-exception v4

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    throw v4

    .line 1122
    .restart local v8       #gaiaId:Ljava/lang/String;
    :catch_17f
    move-exception v4

    :try_start_180
    const-string v5, "GooglePlusContactsSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_190

    const-string v5, "GooglePlusContactsSync"

    const-string v6, "Could not refresh posts"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_190
    .catchall {:try_start_180 .. :try_end_190} :catchall_198

    :cond_190
    invoke-virtual {v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto/16 :goto_11

    :catchall_198
    move-exception v4

    invoke-virtual {v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v4

    .line 1124
    :cond_1a0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v20

    invoke-static {v0, v1, v2, v3, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncActivitiesForRawContact$520c64bf(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)V

    goto/16 :goto_11
.end method

.method private static syncSmallAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 24
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    .line 2201
    invoke-static/range {p0 .. p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2253
    :goto_6
    return-void

    .line 2205
    :cond_7
    const-string v3, "Android:Avatars"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 2207
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    .line 2208
    .local v13, resolver:Landroid/content/ContentResolver;
    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryRawContactsRequiringThumbnails(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v16

    .line 2210
    .local v16, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;>;"
    if-eqz v16, :cond_20

    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_24

    .line 2211
    :cond_20
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    goto :goto_6

    .line 2215
    :cond_24
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v11

    .line 2216
    .local v11, mediumAvatarSize:I
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2217
    .local v8, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2218
    .local v17, states:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;>;"
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 2219
    .local v14, size:I
    const/4 v12, 0x0

    .line 2220
    .local v12, offset:I
    :goto_3d
    if-ge v12, v14, :cond_13d

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v3

    if-nez v3, :cond_13d

    .line 2221
    add-int/lit8 v18, v12, 0x8

    .line 2222
    .local v18, to:I
    move/from16 v0, v18

    if-le v0, v14, :cond_4d

    .line 2223
    move/from16 v18, v14

    .line 2226
    :cond_4d
    move v9, v12

    .local v9, i:I
    :goto_4e
    move/from16 v0, v18

    if-ge v9, v0, :cond_133

    .line 2227
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    .line 2228
    .local v15, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    iget-object v3, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    if-eqz v3, :cond_101

    .line 2229
    new-instance v5, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    iget-object v3, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    iget-object v4, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    invoke-direct {v5, v3, v4, v6, v11}, Lcom/google/android/apps/plus/content/AvatarImageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2232
    .local v5, request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;)[B

    move-result-object v10

    .line 2233
    .local v10, image:[B
    if-nez v10, :cond_86

    .line 2234
    new-instance v2, Lcom/google/android/apps/plus/api/DownloadImageOperation;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/plus/api/DownloadImageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 2236
    .local v2, op:Lcom/google/android/apps/plus/api/DownloadImageOperation;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->start()V

    .line 2237
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/DownloadImageOperation;->getImageBytes()[B

    move-result-object v10

    .line 2240
    .end local v2           #op:Lcom/google/android/apps/plus/api/DownloadImageOperation;
    :cond_86
    if-eqz v10, :cond_101

    .line 2241
    sget v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    if-nez v3, :cond_98

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "thumbnail_max_dim"

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    :cond_98
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v0, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v6, v7

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "sync3"

    iget v6, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v3, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->dataId:J

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-eqz v3, :cond_105

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v0, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->dataId:J

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v6, v7

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data15"

    sget v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    invoke-static {v10, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->resizeThumbnail([BI)[B

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2226
    .end local v5           #request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    .end local v10           #image:[B
    :cond_101
    :goto_101
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4e

    .line 2241
    .restart local v5       #request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    .restart local v10       #image:[B
    :cond_105
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "raw_contact_id"

    iget-wide v6, v15, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "mimetype"

    const-string v6, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data15"

    sget v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    invoke-static {v10, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->resizeThumbnail([BI)[B

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_101

    .line 2246
    .end local v5           #request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    .end local v10           #image:[B
    .end local v15           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;
    :cond_133
    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-static {v13, v8, v3, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    .line 2248
    move/from16 v12, v18

    .line 2249
    goto/16 :goto_3d

    .line 2251
    .end local v9           #i:I
    .end local v18           #to:I
    :cond_13d
    const/4 v3, 0x1

    invoke-static {v13, v8, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2252
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    goto/16 :goto_6
.end method

.method private static updateChangedContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1261
    .local p2, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    .local p3, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p4, circleIdMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1263
    .local v3, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1266
    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_10
    :goto_10
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    .line 1267
    .local v8, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    iget-boolean v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-eqz v0, :cond_10

    iget-wide v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_10

    .line 1268
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 1272
    .end local v8           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    :cond_2c
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3b

    .line 1273
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V

    .line 1275
    :cond_3b
    return-void
.end method

.method private static updateContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V
    .registers 21
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter
    .parameter
    .parameter "knownNew"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1448
    .local p2, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local p3, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    .local p4, stateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;>;"
    .local p6, circleIdMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1449
    .local v2, resolver:Landroid/content/ContentResolver;
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v11

    .line 1450
    .local v11, rawContactsUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getEntitiesUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v8

    .line 1451
    .local v8, entitiesUri:Landroid/net/Uri;
    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v4, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    .line 1452
    .local v3, contactsUri:Landroid/net/Uri;
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 1453
    .local v12, size:I
    const/4 v10, 0x0

    .line 1454
    .local v10, offset:I
    :goto_17
    if-ge v10, v12, :cond_a8

    .line 1455
    add-int/lit8 v13, v10, 0x20

    .line 1456
    .local v13, to:I
    if-le v13, v12, :cond_1e

    .line 1457
    move v13, v12

    .line 1460
    :cond_1e
    sub-int v4, v13, v10

    new-array v6, v4, [Ljava/lang/String;

    .line 1461
    .local v6, personIds:[Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, i:I
    :goto_23
    array-length v4, v6

    if-ge v9, v4, :cond_37

    .line 1462
    add-int v4, v10, v9

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    iget-object v4, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    aput-object v4, v6, v9

    .line 1461
    add-int/lit8 v9, v9, 0x1

    goto :goto_23

    .line 1465
    :cond_37
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "person_id IN ("

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    :goto_42
    array-length v7, v6

    if-ge v4, v7, :cond_54

    if-eqz v4, :cond_4c

    const/16 v7, 0x2c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4c
    const/16 v7, 0x3f

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_42

    :cond_54
    const/16 v4, 0x29

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    :cond_64
    :goto_64
    :try_start_64
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_94

    const/4 v4, 0x0

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    if-eqz v7, :cond_64

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->deserializeContactInfo([B)Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    move-result-object v7

    if-eqz v7, :cond_64

    invoke-static {p0, v4, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->populateRawContactState(Landroid/content/Context;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;)V
    :try_end_8e
    .catchall {:try_start_64 .. :try_end_8e} :catchall_8f

    goto :goto_64

    :catchall_8f
    move-exception v4

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_94
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 1466
    move-object/from16 v0, p4

    move-object/from16 v1, p6

    invoke-static {v6, v0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->loadContactCircleMembership$57209d63([Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 1468
    if-nez p5, :cond_a9

    move-object/from16 v0, p4

    invoke-static {v2, v8, v6, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileContacts(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v4

    if-nez v4, :cond_a9

    .line 1475
    .end local v6           #personIds:[Ljava/lang/String;
    .end local v9           #i:I
    .end local v13           #to:I
    :cond_a8
    return-void

    .line 1471
    .restart local v6       #personIds:[Ljava/lang/String;
    .restart local v9       #i:I
    .restart local v13       #to:I
    :cond_a9
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {p0, v11, v0, v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;[Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1472
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-static {v2, v0, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 1473
    move v10, v13

    .line 1474
    goto/16 :goto_17
.end method

.method private static updateMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)V
    .registers 27
    .parameter "context"
    .parameter "account"
    .parameter "name"
    .parameter "lastUpdatedTime"
    .parameter "profileJson"

    .prologue
    .line 1362
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1439
    :cond_6
    :goto_6
    return-void

    .line 1368
    :cond_7
    const/4 v15, 0x0

    .line 1369
    .local v15, isProfileFresh:Z
    new-instance v19, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;-><init>(B)V

    .line 1370
    .local v19, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    const/4 v5, 0x1

    move-object/from16 v0, v19

    iput-boolean v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    .line 1371
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    .line 1372
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    .line 1373
    move-wide/from16 v0, p3

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    .line 1375
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1376
    .local v3, resolver:Landroid/content/ContentResolver;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    .line 1378
    .local v4, profileRawContactUri:Landroid/net/Uri;
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1380
    .local v13, cursor:Landroid/database/Cursor;
    if-eqz v13, :cond_6

    .line 1385
    :try_start_3c
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_59

    .line 1386
    const/4 v5, 0x0

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    .line 1387
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    const/4 v7, 0x1

    invoke-interface {v13, v7}, Landroid/database/Cursor;->getLong(I)J
    :try_end_53
    .catchall {:try_start_3c .. :try_end_53} :catchall_11c

    move-result-wide v7

    cmp-long v5, v5, v7

    if-nez v5, :cond_119

    const/4 v15, 0x1

    .line 1391
    :cond_59
    :goto_59
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1394
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_68

    if-nez v15, :cond_161

    .line 1395
    :cond_68
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1396
    .local v12, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    .line 1398
    if-eqz p5, :cond_9a

    .line 1399
    invoke-static {}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->getInstance()Lcom/google/api/services/plusi/model/SimpleProfileJson;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v5, v0}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/SimpleProfile;

    .line 1400
    .local v16, profile:Lcom/google/api/services/plusi/model/SimpleProfile;
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_9a

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    if-eqz v5, :cond_9a

    .line 1401
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    move-object/from16 v0, v19

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->populateRawContactState$5160c72c(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/api/services/plusi/model/Contacts;)V

    .line 1405
    .end local v16           #profile:Lcom/google/api/services/plusi/model/SimpleProfile;
    :cond_9a
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_124

    .line 1406
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    sget-object v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "data"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_type"

    const-string v7, "com.google"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "data_set"

    const-string v7, "plus"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_name"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "caller_is_syncadapter"

    const-string v7, "true"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_ENTITIES_PROJECTION:[Ljava/lang/String;

    const-string v8, "mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v3

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1408
    .local v14, dataCursor:Landroid/database/Cursor;
    if-eqz v14, :cond_6

    .line 1413
    :goto_ef
    :try_start_ef
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_121

    .line 1414
    const/4 v5, 0x1

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v5, 0x0

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v5, 0x2

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v5, 0x3

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v5, 0x4

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v5, v19

    invoke-static/range {v5 .. v11}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_113
    .catchall {:try_start_ef .. :try_end_113} :catchall_114

    goto :goto_ef

    .line 1421
    :catchall_114
    move-exception v5

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v5

    .line 1387
    .end local v12           #batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v14           #dataCursor:Landroid/database/Cursor;
    :cond_119
    const/4 v15, 0x0

    goto/16 :goto_59

    .line 1391
    :catchall_11c
    move-exception v5

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v5

    .line 1421
    .restart local v12       #batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v14       #dataCursor:Landroid/database/Cursor;
    :cond_121
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1425
    .end local v14           #dataCursor:Landroid/database/Cursor;
    :cond_124
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_180

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v20

    .line 1427
    .local v20, uri:Landroid/net/Uri;
    :goto_132
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-static {v0, v1, v12, v2, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Z)V

    .line 1428
    const/4 v5, 0x1

    invoke-static {v3, v12, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-result-object v18

    .line 1429
    .local v18, results:[Landroid/content/ContentProviderResult;
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_161

    if-eqz v18, :cond_161

    move-object/from16 v0, v18

    array-length v5, v0

    if-lez v5, :cond_161

    .line 1430
    const/4 v5, 0x0

    aget-object v5, v18, v5

    iget-object v0, v5, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    move-object/from16 v17, v0

    .line 1431
    .local v17, rawContactUri:Landroid/net/Uri;
    invoke-static/range {v17 .. v17}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v5

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    .line 1435
    .end local v12           #batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v17           #rawContactUri:Landroid/net/Uri;
    .end local v18           #results:[Landroid/content/ContentProviderResult;
    .end local v20           #uri:Landroid/net/Uri;
    :cond_161
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_6

    .line 1436
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v19

    iget-wide v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncRawContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V

    goto/16 :goto_6

    .line 1425
    .restart local v12       #batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_180
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v20

    goto :goto_132
.end method

.method private static updateStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;[I)V
    .registers 49
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter "offset"
    .parameter "to"
    .parameter
    .parameter "totalImageBytes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;II",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;[I)V"
        }
    .end annotation

    .prologue
    .line 2717
    .local p2, stateList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;>;"
    .local p5, batch:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 2720
    .local v13, activityStateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;>;"
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    .line 2721
    .local v30, sb:Ljava/lang/StringBuilder;
    const-string v4, "activity_id IN ("

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2722
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2723
    .local v11, activityIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move/from16 v17, p3

    .local v17, i:I
    :goto_18
    move/from16 v0, v17

    move/from16 v1, p4

    if-ge v0, v1, :cond_7a

    .line 2724
    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    .line 2725
    .local v31, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, i$:Ljava/util/Iterator;
    :cond_30
    :goto_30
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_77

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .line 2726
    .local v12, activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    iget-boolean v4, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-eqz v4, :cond_30

    iget-boolean v4, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    if-nez v4, :cond_4e

    iget-wide v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    move-wide/from16 v38, v0

    const-wide/16 v40, 0x0

    cmp-long v4, v38, v40

    if-nez v4, :cond_30

    .line 2728
    :cond_4e
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5b

    .line 2729
    const/16 v4, 0x2c

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2731
    :cond_5b
    const/16 v4, 0x3f

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2732
    iget-object v4, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2733
    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    move-wide/from16 v38, v0

    move-wide/from16 v0, v38

    iput-wide v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    .line 2734
    iget-object v4, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v13, v4, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_30

    .line 2723
    .end local v12           #activityState:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    :cond_77
    add-int/lit8 v17, v17, 0x1

    goto :goto_18

    .line 2739
    .end local v18           #i$:Ljava/util/Iterator;
    .end local v31           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;
    :cond_7a
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_81

    .line 2878
    :cond_80
    :goto_80
    return-void

    .line 2743
    :cond_81
    const/16 v4, 0x29

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2745
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v35

    .line 2746
    .local v35, streamItemsUri:Landroid/net/Uri;
    sget-object v4, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PHOTO_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v38, "account_type"

    const-string v39, "com.google"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v38, "data_set"

    const-string v39, "plus"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v38, "account_name"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v38, "caller_is_syncadapter"

    const-string v39, "true"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v34

    .line 2747
    .local v34, streamItemsPhotoUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v26

    .line 2748
    .local v26, packageName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v38, 0x7f02016f

    move/from16 v0, v38

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v19

    .line 2749
    .local v19, iconResourceName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v38, 0x7f080029

    move/from16 v0, v38

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v22

    .line 2750
    .local v22, labelResourceName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 2751
    .local v3, resolver:Landroid/content/ContentResolver;
    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v38, 0x0

    move/from16 v0, v38

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 2756
    .local v16, cursor:Landroid/database/Cursor;
    :cond_10d
    :goto_10d
    :try_start_10d
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_36d

    .line 2757
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2758
    .local v10, activityId:Ljava/lang/String;
    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v24

    .line 2759
    .local v24, mediaBytes:[B
    const/4 v4, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 2760
    .local v15, commentCount:I
    const/4 v4, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v28

    .line 2761
    .local v28, plusOneBytes:[B
    const/4 v5, 0x0

    .line 2762
    .local v5, dbMedia:[Lcom/google/android/apps/plus/content/DbMedia;
    if-eqz v24, :cond_136

    .line 2763
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v5

    .line 2766
    :cond_136
    invoke-virtual {v13, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    .line 2767
    .local v31, state:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v32

    .line 2769
    .local v32, streamItemBackReference:I
    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    move-wide/from16 v38, v0

    const-wide/16 v40, 0x0

    cmp-long v4, v38, v40

    if-nez v4, :cond_326

    .line 2770
    invoke-static/range {v35 .. v35}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/16 v38, 0x1

    move/from16 v0, v38

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "raw_contact_id"

    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    move-wide/from16 v39, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "stream_item_sync1"

    move-object/from16 v0, v38

    invoke-virtual {v4, v0, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "res_package"

    move-object/from16 v0, v38

    move-object/from16 v1, v26

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "icon"

    move-object/from16 v0, v38

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "label"

    move-object/from16 v0, v38

    move-object/from16 v1, v22

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    .line 2783
    .local v14, builder:Landroid/content/ContentProviderOperation$Builder;
    :goto_192
    const/4 v4, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v23

    .line 2784
    .local v23, locationData:[B
    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v6

    .line 2786
    .local v6, location:Lcom/google/android/apps/plus/content/DbLocation;
    const/4 v4, 0x5

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2788
    .local v7, originalAuthorName:Ljava/lang/String;
    const/4 v4, 0x6

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2789
    .local v8, annotation:Ljava/lang/String;
    const/4 v4, 0x7

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .local v9, content:Ljava/lang/String;
    move-object/from16 v4, p0

    .line 2791
    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->buildStreamItemContentHtml(Landroid/content/Context;[Lcom/google/android/apps/plus/content/DbMedia;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 2794
    .local v36, text:Ljava/lang/String;
    const-string v4, "text"

    move-object/from16 v0, v36

    invoke-virtual {v14, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2795
    const-string v4, "timestamp"

    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    move-wide/from16 v38, v0

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v14, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2796
    const-string v4, "stream_item_sync2"

    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    move-wide/from16 v38, v0

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v14, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2798
    const/4 v4, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2799
    if-eqz v15, :cond_206

    .line 2800
    const v4, 0x7f0200a7

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->appendImgTag(Landroid/content/Context;Ljava/lang/StringBuilder;I)V

    .line 2801
    const-string v4, " "

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2802
    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2803
    const-string v4, " "

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2806
    :cond_206
    if-eqz v28, :cond_23e

    .line 2807
    invoke-static/range {v28 .. v28}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v29

    .line 2808
    .local v29, plusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v37

    .line 2809
    .local v37, totalPlusoneCount:I
    if-eqz v37, :cond_23e

    .line 2810
    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_21f

    .line 2811
    const-string v4, "&nbsp;&nbsp;"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2813
    :cond_21f
    const v4, 0x7f02012f

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->appendImgTag(Landroid/content/Context;Ljava/lang/StringBuilder;I)V

    .line 2814
    const-string v4, " "

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2815
    move-object/from16 v0, v30

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2816
    const-string v4, " "

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2820
    .end local v29           #plusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    .end local v37           #totalPlusoneCount:I
    :cond_23e
    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_342

    .line 2821
    const-string v4, "comments"

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v14, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2827
    :goto_24f
    invoke-virtual {v14}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2829
    const/16 v33, 0x0

    .line 2830
    .local v33, streamItemPhotoDirUri:Landroid/net/Uri;
    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    move-wide/from16 v38, v0

    const-wide/16 v40, 0x0

    cmp-long v4, v38, v40

    if-eqz v4, :cond_28b

    .line 2831
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    move-wide/from16 v38, v0

    move-wide/from16 v0, v38

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    const-string v38, "photo"

    move-object/from16 v0, v38

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v33

    .line 2834
    invoke-static/range {v33 .. v33}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2837
    :cond_28b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/content/DbMedia;)Ljava/util/ArrayList;

    move-result-object v21

    .line 2838
    .local v21, images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;>;"
    if-eqz v21, :cond_10d

    .line 2839
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v38, 0x6

    move/from16 v0, v38

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v27

    .line 2840
    .local v27, photoCount:I
    const/16 v17, 0x0

    :goto_2a3
    move/from16 v0, v17

    move/from16 v1, v27

    if-ge v0, v1, :cond_357

    .line 2841
    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;

    .line 2842
    .local v20, image:Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;
    const/4 v4, 0x0

    aget v38, p6, v4

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->imageBytes:[B

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    array-length v0, v0

    move/from16 v39, v0

    add-int v38, v38, v39

    aput v38, p6, v4

    .line 2844
    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    move-wide/from16 v38, v0

    const-wide/16 v40, 0x0

    cmp-long v4, v38, v40

    if-nez v4, :cond_352

    .line 2845
    invoke-static/range {v34 .. v34}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "stream_item_id"

    move-object/from16 v0, v38

    move/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v25

    .line 2851
    .local v25, op:Landroid/content/ContentProviderOperation$Builder;
    :goto_2df
    const-string v4, "stream_item_photo_sync1"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v10}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "sort_index"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v39

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "stream_item_photo_sync2"

    move-object/from16 v0, v20

    iget v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->mediaIndex:I

    move/from16 v39, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v39

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v38, "photo"

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->imageBytes:[B

    move-object/from16 v39, v0

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2840
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_2a3

    .line 2778
    .end local v6           #location:Lcom/google/android/apps/plus/content/DbLocation;
    .end local v7           #originalAuthorName:Ljava/lang/String;
    .end local v8           #annotation:Ljava/lang/String;
    .end local v9           #content:Ljava/lang/String;
    .end local v14           #builder:Landroid/content/ContentProviderOperation$Builder;
    .end local v20           #image:Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;
    .end local v21           #images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;>;"
    .end local v23           #locationData:[B
    .end local v25           #op:Landroid/content/ContentProviderOperation$Builder;
    .end local v27           #photoCount:I
    .end local v33           #streamItemPhotoDirUri:Landroid/net/Uri;
    .end local v36           #text:Ljava/lang/String;
    :cond_326
    move-object/from16 v0, v31

    iget-wide v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    move-wide/from16 v38, v0

    move-object/from16 v0, v35

    move-wide/from16 v1, v38

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/16 v38, 0x1

    move/from16 v0, v38

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v14

    .restart local v14       #builder:Landroid/content/ContentProviderOperation$Builder;
    goto/16 :goto_192

    .line 2824
    .restart local v6       #location:Lcom/google/android/apps/plus/content/DbLocation;
    .restart local v7       #originalAuthorName:Ljava/lang/String;
    .restart local v8       #annotation:Ljava/lang/String;
    .restart local v9       #content:Ljava/lang/String;
    .restart local v23       #locationData:[B
    .restart local v36       #text:Ljava/lang/String;
    :cond_342
    const-string v4, "comments"

    const-string v38, " "

    move-object/from16 v0, v38

    invoke-virtual {v14, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    :try_end_34b
    .catchall {:try_start_10d .. :try_end_34b} :catchall_34d

    goto/16 :goto_24f

    .line 2870
    .end local v5           #dbMedia:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v6           #location:Lcom/google/android/apps/plus/content/DbLocation;
    .end local v7           #originalAuthorName:Ljava/lang/String;
    .end local v8           #annotation:Ljava/lang/String;
    .end local v9           #content:Ljava/lang/String;
    .end local v10           #activityId:Ljava/lang/String;
    .end local v14           #builder:Landroid/content/ContentProviderOperation$Builder;
    .end local v15           #commentCount:I
    .end local v23           #locationData:[B
    .end local v24           #mediaBytes:[B
    .end local v28           #plusOneBytes:[B
    .end local v31           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v32           #streamItemBackReference:I
    .end local v36           #text:Ljava/lang/String;
    :catchall_34d
    move-exception v4

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    throw v4

    .line 2849
    .restart local v5       #dbMedia:[Lcom/google/android/apps/plus/content/DbMedia;
    .restart local v6       #location:Lcom/google/android/apps/plus/content/DbLocation;
    .restart local v7       #originalAuthorName:Ljava/lang/String;
    .restart local v8       #annotation:Ljava/lang/String;
    .restart local v9       #content:Ljava/lang/String;
    .restart local v10       #activityId:Ljava/lang/String;
    .restart local v14       #builder:Landroid/content/ContentProviderOperation$Builder;
    .restart local v15       #commentCount:I
    .restart local v20       #image:Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;
    .restart local v21       #images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;>;"
    .restart local v23       #locationData:[B
    .restart local v24       #mediaBytes:[B
    .restart local v27       #photoCount:I
    .restart local v28       #plusOneBytes:[B
    .restart local v31       #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .restart local v32       #streamItemBackReference:I
    .restart local v33       #streamItemPhotoDirUri:Landroid/net/Uri;
    .restart local v36       #text:Ljava/lang/String;
    :cond_352
    :try_start_352
    invoke-static/range {v33 .. v33}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v25

    .restart local v25       #op:Landroid/content/ContentProviderOperation$Builder;
    goto :goto_2df

    .line 2859
    .end local v20           #image:Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;
    .end local v25           #op:Landroid/content/ContentProviderOperation$Builder;
    :cond_357
    const/4 v4, 0x0

    aget v4, p6, v4

    const/high16 v38, 0x4

    move/from16 v0, v38

    if-lt v4, v0, :cond_10d

    .line 2863
    const/4 v4, 0x1

    move-object/from16 v0, p5

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2865
    const/4 v4, 0x0

    const/16 v38, 0x0

    aput v38, p6, v4
    :try_end_36b
    .catchall {:try_start_352 .. :try_end_36b} :catchall_34d

    goto/16 :goto_10d

    .line 2870
    .end local v5           #dbMedia:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v6           #location:Lcom/google/android/apps/plus/content/DbLocation;
    .end local v7           #originalAuthorName:Ljava/lang/String;
    .end local v8           #annotation:Ljava/lang/String;
    .end local v9           #content:Ljava/lang/String;
    .end local v10           #activityId:Ljava/lang/String;
    .end local v14           #builder:Landroid/content/ContentProviderOperation$Builder;
    .end local v15           #commentCount:I
    .end local v21           #images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;>;"
    .end local v23           #locationData:[B
    .end local v24           #mediaBytes:[B
    .end local v27           #photoCount:I
    .end local v28           #plusOneBytes:[B
    .end local v31           #state:Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;
    .end local v32           #streamItemBackReference:I
    .end local v33           #streamItemPhotoDirUri:Landroid/net/Uri;
    .end local v36           #text:Ljava/lang/String;
    :cond_36d
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 2873
    const/4 v4, 0x0

    move-object/from16 v0, p5

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    .line 2875
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_80

    .line 2876
    const/4 v4, 0x0

    const/16 v38, 0x0

    aput v38, p6, v4

    goto/16 :goto_80
.end method
