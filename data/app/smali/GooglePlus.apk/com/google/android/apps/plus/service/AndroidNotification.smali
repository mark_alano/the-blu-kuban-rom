.class public final Lcom/google/android/apps/plus/service/AndroidNotification;
.super Ljava/lang/Object;
.source "AndroidNotification.java"


# static fields
.field private static final SUGGESTION_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "category"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidNotification;->SUGGESTION_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private static buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "account"

    .prologue
    .line 1122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":notifications:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 6
    .parameter "context"
    .parameter "account"

    .prologue
    .line 162
    const-class v3, Lcom/google/android/apps/plus/service/AndroidNotification;

    monitor-enter v3

    :try_start_3
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    .line 163
    .local v1, notificationTag:Ljava/lang/String;
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 165
    .local v0, notificationManager:Landroid/app/NotificationManager;
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_15

    .line 166
    monitor-exit v3

    return-void

    .line 162
    .end local v0           #notificationManager:Landroid/app/NotificationManager;
    .end local v1           #notificationTag:Ljava/lang/String;
    :catchall_15
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static cancelFindPeoplePromo(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 1072
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1075
    .local v0, notificationManager:Landroid/app/NotificationManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":notifications:findpeople"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1077
    return-void
.end method

.method private static createDigestNotification$78923c81(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;
    .registers 23
    .parameter "context"
    .parameter "account"
    .parameter "cursor"

    .prologue
    .line 306
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasOnlyHangoutNotifications(Landroid/database/Cursor;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 307
    const/16 v18, 0x0

    .line 361
    :goto_8
    return-object v18

    .line 310
    :cond_9
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v18

    if-nez v18, :cond_12

    .line 311
    const/16 v18, 0x0

    goto :goto_8

    .line 313
    :cond_12
    const-wide v16, 0x7fffffffffffffffL

    .line 314
    .local v16, when:J
    const/4 v9, 0x0

    .line 315
    .local v9, interestingCount:I
    new-instance v7, Landroid/app/Notification$InboxStyle;

    invoke-direct {v7}, Landroid/app/Notification$InboxStyle;-><init>()V

    .line 317
    .local v7, digest:Landroid/app/Notification$InboxStyle;
    :cond_1d
    const/16 v18, 0x3

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 318
    .local v4, category:I
    const/16 v18, 0x8

    move/from16 v0, v18

    if-eq v4, v0, :cond_4e

    .line 319
    const/16 v18, 0x4

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 321
    .local v11, snippetText:Ljava/lang/CharSequence;
    invoke-virtual {v7, v11}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 322
    const/16 v18, 0x5

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 324
    .local v13, timestamp:J
    const-wide/16 v18, 0x3e8

    div-long v18, v13, v18

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v16

    .line 325
    add-int/lit8 v9, v9, 0x1

    .line 327
    .end local v11           #snippetText:Ljava/lang/CharSequence;
    .end local v13           #timestamp:J
    :cond_4e
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v18

    if-nez v18, :cond_1d

    .line 328
    move v15, v9

    .line 330
    .local v15, unseenCount:I
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 331
    .local v10, res:Landroid/content/res/Resources;
    const v18, 0x7f0e0012

    move/from16 v0, v18

    invoke-virtual {v10, v0, v15}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v12

    .line 333
    .local v12, tickerText:Ljava/lang/CharSequence;
    move-object v6, v12

    .line 335
    .local v6, contentTitle:Ljava/lang/CharSequence;
    invoke-static/range {p0 .. p2}, Lcom/google/android/apps/plus/phone/Intents;->getNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v8

    .line 336
    .local v8, intent:Landroid/content/Intent;
    const/high16 v18, 0x1400

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 337
    const-string v18, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 339
    new-instance v3, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 340
    .local v3, builder:Landroid/app/Notification$Builder;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v8, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 342
    .local v5, contentIntent:Landroid/app/PendingIntent;
    invoke-virtual {v7, v6}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 343
    invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    move-result-object v18

    const v19, 0x7f020140

    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v18

    const v19, 0x7f0201c7

    move/from16 v0, v19

    invoke-static {v10, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v18

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/PendingIntent;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 355
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_f2

    .line 356
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    .line 361
    :goto_ec
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v18

    goto/16 :goto_8

    .line 358
    :cond_f2
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    goto :goto_ec
.end method

.method private static createNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/Notification;
    .registers 11
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldNotify(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 225
    :cond_7
    :goto_7
    return-object v1

    .line 183
    :cond_8
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsNotificationData;->getNotificationsToDisplay(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/database/Cursor;

    move-result-object v0

    .line 184
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_7

    .line 190
    :try_start_e
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 191
    .local v2, unreadCount:I
    if-lez v2, :cond_ea

    .line 192
    packed-switch v2, :pswitch_data_ee

    .line 203
    invoke-static {}, Lcom/google/android/apps/plus/service/AndroidNotification;->isRunningJellybeanOrLater()Z

    move-result v3

    if-eqz v3, :cond_7e

    .line 204
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->createDigestNotification$78923c81(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;
    :try_end_20
    .catchall {:try_start_e .. :try_end_20} :catchall_72

    move-result-object v1

    .line 222
    .local v1, notification:Landroid/app/Notification;
    :goto_21
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_7

    .line 194
    .end local v1           #notification:Landroid/app/Notification;
    :pswitch_25
    :try_start_25
    invoke-static {}, Lcom/google/android/apps/plus/service/AndroidNotification;->isRunningJellybeanOrLater()Z

    move-result v3

    if-eqz v3, :cond_30

    .line 195
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->createRichNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;

    move-result-object v1

    .restart local v1       #notification:Landroid/app/Notification;
    goto :goto_21

    .line 197
    .end local v1           #notification:Landroid/app/Notification;
    :cond_30
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_37

    .line 199
    .restart local v1       #notification:Landroid/app/Notification;
    :cond_36
    :goto_36
    goto :goto_21

    .line 197
    .end local v1           #notification:Landroid/app/Notification;
    :cond_37
    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_36

    const/4 v4, 0x4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    new-instance v1, Landroid/app/Notification;

    const v7, 0x7f020140

    invoke-direct {v1, v7, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const v5, 0xffff

    if-eq v3, v5, :cond_62

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_77

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    :cond_62
    :goto_62
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/PendingIntent;

    move-result-object v3

    const-string v5, ""

    invoke-virtual {v1, p0, v4, v5, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, v1, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;
    :try_end_71
    .catchall {:try_start_25 .. :try_end_71} :catchall_72

    goto :goto_36

    .line 222
    .end local v2           #unreadCount:I
    :catchall_72
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3

    .line 197
    .restart local v2       #unreadCount:I
    :cond_77
    :try_start_77
    iget v3, v1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/app/Notification;->defaults:I

    goto :goto_62

    .line 207
    :cond_7e
    invoke-static {v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasOnlyHangoutNotifications(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_85

    .line 210
    .restart local v1       #notification:Landroid/app/Notification;
    :goto_84
    goto :goto_21

    .line 207
    .end local v1           #notification:Landroid/app/Notification;
    :cond_85
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0012

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    new-instance v1, Landroid/app/Notification;

    const v7, 0x7f020140

    invoke-direct {v1, v7, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const v5, 0x7f0e0013

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x1400

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v6, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_e3

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v6

    iput-object v6, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    :goto_cf
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, 0x0

    invoke-static {p0, v6, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v1, p0, v4, v3, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, v1, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    goto :goto_84

    :cond_e3
    iget v6, v1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v1, Landroid/app/Notification;->defaults:I
    :try_end_e9
    .catchall {:try_start_77 .. :try_end_e9} :catchall_72

    goto :goto_cf

    .line 219
    :cond_ea
    const/4 v1, 0x0

    .restart local v1       #notification:Landroid/app/Notification;
    goto/16 :goto_21

    .line 192
    nop

    :pswitch_data_ee
    .packed-switch 0x1
        :pswitch_25
    .end packed-switch
.end method

.method private static createRichNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;
    .registers 66
    .parameter "context"
    .parameter "account"
    .parameter "cursor"

    .prologue
    .line 374
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_8

    .line 375
    const/4 v5, 0x0

    .line 627
    :goto_7
    return-object v5

    .line 378
    :cond_8
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v52

    .line 380
    .local v52, res:Landroid/content/res/Resources;
    const/4 v5, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 381
    .local v33, category:I
    const/16 v5, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v47

    .line 385
    .local v47, notificationType:I
    const/4 v5, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v58

    .line 387
    .local v58, tickerText:Ljava/lang/CharSequence;
    const v5, 0x7f0e0012

    const/4 v6, 0x1

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v57

    .line 389
    .local v57, stockTitle:Ljava/lang/CharSequence;
    const/4 v5, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long v60, v5, v7

    .line 392
    .local v60, when:J
    const/16 v5, 0xa

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 395
    .local v15, activityId:Ljava/lang/String;
    invoke-static/range {p0 .. p2}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/PendingIntent;

    move-result-object v35

    .line 396
    .local v35, contentIntent:Landroid/app/PendingIntent;
    move-object/from16 v37, v57

    .line 397
    .local v37, contentTitle:Ljava/lang/CharSequence;
    move-object/from16 v36, v58

    .line 398
    .local v36, contentText:Ljava/lang/CharSequence;
    move-object/from16 v29, v58

    .line 400
    .local v29, bigTitle:Ljava/lang/CharSequence;
    const/16 v48, 0x0

    .line 401
    .local v48, primaryActionIcon:I
    const/16 v50, 0x0

    .line 402
    .local v50, primaryActionString:Ljava/lang/String;
    const/16 v49, 0x0

    .line 404
    .local v49, primaryActionIntent:Landroid/app/PendingIntent;
    const/16 v54, 0x0

    .line 405
    .local v54, secondaryActionIcon:I
    const/16 v56, 0x0

    .line 406
    .local v56, secondaryActionString:Ljava/lang/String;
    const/16 v55, 0x0

    .line 408
    .local v55, secondaryActionIntent:Landroid/app/PendingIntent;
    const/16 v27, 0x0

    .line 409
    .local v27, bigPicture:Landroid/graphics/Bitmap;
    const/16 v28, 0x0

    .line 410
    .local v28, bigText:Ljava/lang/CharSequence;
    const v5, 0x7f0201c6

    move-object/from16 v0, v52

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v43

    .line 412
    .local v43, largeIcon:Landroid/graphics/Bitmap;
    const/16 v51, -0x1

    .line 414
    .local v51, priority:I
    sparse-switch v47, :sswitch_data_3c8

    .line 572
    const-string v5, "AndroidNotification"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_86

    .line 573
    const-string v5, "AndroidNotification"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unknown notification type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v47

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :cond_86
    :goto_86
    :sswitch_86
    new-instance v31, Landroid/app/Notification$Builder;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 582
    .local v31, builder:Landroid/app/Notification$Builder;
    move-object/from16 v0, v31

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    move-object/from16 v0, v37

    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    move-wide/from16 v0, v60

    invoke-virtual {v5, v0, v1}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v5

    move/from16 v0, v51

    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v5

    const v6, 0x7f020140

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v5

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 591
    if-eqz v27, :cond_3a5

    .line 592
    new-instance v5, Landroid/app/Notification$BigPictureStyle;

    invoke-direct {v5}, Landroid/app/Notification$BigPictureStyle;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Landroid/app/Notification$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Landroid/app/Notification$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/app/Notification$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 602
    :cond_e1
    :goto_e1
    if-eqz v43, :cond_ea

    .line 603
    move-object/from16 v0, v31

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 606
    :cond_ea
    if-eqz v48, :cond_f7

    .line 607
    move-object/from16 v0, v31

    move/from16 v1, v48

    move-object/from16 v2, v50

    move-object/from16 v3, v49

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 613
    :cond_f7
    if-eqz v54, :cond_104

    .line 614
    move-object/from16 v0, v31

    move/from16 v1, v54

    move-object/from16 v2, v56

    move-object/from16 v3, v55

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 619
    :cond_104
    const v5, 0xffff

    move/from16 v0, v33

    if-eq v0, v5, :cond_11a

    .line 620
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3bf

    .line 621
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    .line 627
    :cond_11a
    :goto_11a
    invoke-virtual/range {v31 .. v31}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    goto/16 :goto_7

    .line 418
    .end local v31           #builder:Landroid/app/Notification$Builder;
    :sswitch_120
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 422
    :sswitch_123
    const/4 v5, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 424
    .local v11, ownerGaiaId:Ljava/lang/String;
    const/16 v5, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 426
    .local v9, photoId:J
    const-string v5, "AndroidNotification"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_15b

    .line 427
    const-string v5, "AndroidNotification"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Got a "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v33

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " notification: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    :cond_15b
    new-instance v4, Lcom/google/android/apps/plus/api/GetPhotoOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/plus/api/GetPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;JLjava/lang/String;)V

    .line 432
    .local v4, op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 434
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v9, v10}, Lcom/google/android/apps/plus/content/EsPhotosData;->getImageDataFromCloud(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)[B

    move-result-object v30

    .line 435
    .local v30, bits:[B
    if-eqz v30, :cond_1cd

    .line 436
    const/4 v5, 0x0

    move-object/from16 v0, v30

    array-length v6, v0

    move-object/from16 v0, v30

    invoke-static {v0, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v38

    .line 437
    .local v38, full:Landroid/graphics/Bitmap;
    if-eqz v38, :cond_1c9

    .line 438
    const v5, 0x7f0d0030

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v45

    .line 439
    .local v45, maxWidth:F
    const v5, 0x7f0d0031

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v44

    .line 440
    .local v44, maxHeight:F
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v45, v5

    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float v46, v5, v6

    .line 442
    .local v46, newHeight:F
    move/from16 v0, v45

    float-to-int v5, v0

    move/from16 v0, v46

    float-to-int v6, v0

    const/4 v7, 0x1

    move-object/from16 v0, v38

    invoke-static {v0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v53

    .line 444
    .local v53, scaled:Landroid/graphics/Bitmap;
    const v5, 0x7f0d0032

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    move/from16 v40, v0

    .line 445
    .local v40, iconSize:I
    const/4 v5, 0x1

    move-object/from16 v0, v53

    move/from16 v1, v40

    move/from16 v2, v40

    invoke-static {v0, v1, v2, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v43

    .line 446
    cmpg-float v5, v46, v44

    if-gtz v5, :cond_1e3

    .line 447
    move-object/from16 v27, v53

    .line 456
    .end local v40           #iconSize:I
    .end local v44           #maxHeight:F
    .end local v45           #maxWidth:F
    .end local v46           #newHeight:F
    .end local v53           #scaled:Landroid/graphics/Bitmap;
    :cond_1c9
    :goto_1c9
    if-eqz v27, :cond_1cd

    .line 457
    const/16 v51, 0x1

    .line 460
    .end local v38           #full:Landroid/graphics/Bitmap;
    :cond_1cd
    const v5, 0x7f0800a4

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 461
    const v5, 0x7f0800a5

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 462
    move-object/from16 v29, v37

    .line 463
    goto/16 :goto_86

    .line 448
    .restart local v38       #full:Landroid/graphics/Bitmap;
    .restart local v40       #iconSize:I
    .restart local v44       #maxHeight:F
    .restart local v45       #maxWidth:F
    .restart local v46       #newHeight:F
    .restart local v53       #scaled:Landroid/graphics/Bitmap;
    :cond_1e3
    if-eqz v53, :cond_1c9

    .line 449
    move/from16 v0, v45

    float-to-int v0, v0

    move/from16 v59, v0

    .line 451
    .local v59, w:I
    move/from16 v0, v44

    float-to-int v0, v0

    move/from16 v39, v0

    .line 452
    .local v39, h:I
    const/4 v5, 0x0

    invoke-virtual/range {v53 .. v53}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int v6, v6, v39

    div-int/lit8 v6, v6, 0x2

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v62

    .line 453
    .local v62, y:I
    const/4 v5, 0x0

    move-object/from16 v0, v53

    move/from16 v1, v62

    move/from16 v2, v59

    move/from16 v3, v39

    invoke-static {v0, v5, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v27

    goto :goto_1c9

    .line 471
    .end local v4           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    .end local v9           #photoId:J
    .end local v11           #ownerGaiaId:Ljava/lang/String;
    .end local v30           #bits:[B
    .end local v38           #full:Landroid/graphics/Bitmap;
    .end local v39           #h:I
    .end local v40           #iconSize:I
    .end local v44           #maxHeight:F
    .end local v45           #maxWidth:F
    .end local v46           #newHeight:F
    .end local v53           #scaled:Landroid/graphics/Bitmap;
    .end local v59           #w:I
    .end local v62           #y:I
    :sswitch_20a
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityIsPublic(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    .line 472
    .local v42, isPublic:Ljava/lang/Boolean;
    if-nez v42, :cond_249

    .line 474
    new-instance v4, Lcom/google/android/apps/plus/api/GetActivityOperation;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v12, v4

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    invoke-direct/range {v12 .. v18}, Lcom/google/android/apps/plus/api/GetActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 476
    .local v4, op:Lcom/google/android/apps/plus/api/GetActivityOperation;
    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetActivityOperation;->start()V

    .line 477
    const-string v5, "AndroidNotification"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_241

    .line 478
    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v5

    if-eqz v5, :cond_358

    .line 479
    const-string v5, "AndroidNotification"

    const-string v6, "Cannot download activity"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v7

    invoke-static {v5, v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 484
    :cond_241
    :goto_241
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityIsPublic(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    .line 487
    .end local v4           #op:Lcom/google/android/apps/plus/api/GetActivityOperation;
    :cond_249
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsPostsData;->isActivityPlusOnedByViewer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z

    move-result v41

    .line 489
    .local v41, isPlusOned:Z
    if-nez v41, :cond_267

    .line 490
    const v48, 0x7f0200e8

    .line 491
    const v5, 0x7f0800a3

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v50

    .line 492
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/service/EsService;->getCreatePostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v49

    .line 496
    :cond_267
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityCanReshare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v32

    .line 498
    .local v32, canReshare:Ljava/lang/Boolean;
    if-eqz v32, :cond_2b8

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2b8

    .line 499
    const v54, 0x7f0200f1

    .line 500
    const v5, 0x7f0800a2

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v56

    .line 501
    invoke-virtual/range {v42 .. v42}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    const/4 v5, 0x0

    if-eqz v15, :cond_378

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15, v6}, Lcom/google/android/apps/plus/phone/Intents;->getReshareActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    :cond_292
    :goto_292
    if-eqz v5, :cond_2a0

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v6, 0x1400

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_2a0
    if-eqz v5, :cond_38a

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v6

    if-eqz v6, :cond_38a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v6, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v55

    .line 505
    :cond_2b8
    :goto_2b8
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityAuthorId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 506
    .local v23, authorId:Ljava/lang/String;
    const/16 v24, 0x0

    .line 507
    .local v24, authorName:Ljava/lang/String;
    const/16 v26, 0x0

    .line 509
    .local v26, avatarUrl:Ljava/lang/String;
    const/16 v34, 0x0

    .line 511
    .local v34, contactCursor:Landroid/database/Cursor;
    const/16 v18, 0x0

    const/16 v19, 0x0

    const/4 v5, 0x2

    :try_start_2cb
    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/4 v5, 0x0

    const-string v6, "name"

    aput-object v6, v20, v5

    const/4 v5, 0x1

    const-string v6, "avatar"

    aput-object v6, v20, v5

    const-string v21, "gaia_id=?"

    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v5, 0x0

    aput-object v23, v22, v5

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    invoke-static/range {v16 .. v22}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v34

    .line 520
    if-eqz v34, :cond_301

    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_301

    .line 521
    const/4 v5, 0x0

    move-object/from16 v0, v34

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 522
    const/4 v5, 0x1

    move-object/from16 v0, v34

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_300
    .catchall {:try_start_2cb .. :try_end_300} :catchall_39e

    move-result-object v26

    .line 525
    :cond_301
    if-eqz v34, :cond_306

    .line 526
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V

    .line 530
    :cond_306
    if-eqz v24, :cond_321

    .line 531
    move-object/from16 v29, v24

    .line 532
    move-object/from16 v37, v24

    .line 533
    if-eqz v26, :cond_321

    .line 534
    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2, v5, v6}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v25

    .line 536
    .local v25, avatar:Landroid/graphics/Bitmap;
    if-eqz v25, :cond_321

    .line 537
    move-object/from16 v43, v25

    .line 542
    .end local v25           #avatar:Landroid/graphics/Bitmap;
    :cond_321
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityImageData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v27

    .line 543
    if-nez v27, :cond_348

    .line 545
    const-string v5, "AndroidNotification"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_348

    .line 547
    const-string v5, "AndroidNotification"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "failed to decode media object for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    :cond_348
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityPostText(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v28

    .line 552
    const/16 v51, 0x1

    .line 554
    if-nez v28, :cond_86

    .line 555
    move-object/from16 v28, v58

    goto/16 :goto_86

    .line 480
    .end local v23           #authorId:Ljava/lang/String;
    .end local v24           #authorName:Ljava/lang/String;
    .end local v26           #avatarUrl:Ljava/lang/String;
    .end local v32           #canReshare:Ljava/lang/Boolean;
    .end local v34           #contactCursor:Landroid/database/Cursor;
    .end local v41           #isPlusOned:Z
    .restart local v4       #op:Lcom/google/android/apps/plus/api/GetActivityOperation;
    :cond_358
    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetActivityOperation;->hasError()Z

    move-result v5

    if-eqz v5, :cond_241

    .line 481
    const-string v5, "AndroidNotification"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Cannot download activity: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getErrorCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_241

    .line 501
    .end local v4           #op:Lcom/google/android/apps/plus/api/GetActivityOperation;
    .restart local v32       #canReshare:Ljava/lang/Boolean;
    .restart local v41       #isPlusOned:Z
    :cond_378
    const-string v6, "AndroidNotification"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_292

    const-string v6, "AndroidNotification"

    const-string v7, "Can\'t reshare: activity ID was null"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_292

    :cond_38a
    const-string v5, "AndroidNotification"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_39a

    const-string v5, "AndroidNotification"

    const-string v6, "reshare returning null"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_39a
    const/16 v55, 0x0

    goto/16 :goto_2b8

    .line 525
    .restart local v23       #authorId:Ljava/lang/String;
    .restart local v24       #authorName:Ljava/lang/String;
    .restart local v26       #avatarUrl:Ljava/lang/String;
    .restart local v34       #contactCursor:Landroid/database/Cursor;
    :catchall_39e
    move-exception v5

    if-eqz v34, :cond_3a4

    .line 526
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V

    :cond_3a4
    throw v5

    .line 596
    .end local v23           #authorId:Ljava/lang/String;
    .end local v24           #authorName:Ljava/lang/String;
    .end local v26           #avatarUrl:Ljava/lang/String;
    .end local v32           #canReshare:Ljava/lang/Boolean;
    .end local v34           #contactCursor:Landroid/database/Cursor;
    .end local v41           #isPlusOned:Z
    .end local v42           #isPublic:Ljava/lang/Boolean;
    .restart local v31       #builder:Landroid/app/Notification$Builder;
    :cond_3a5
    if-eqz v28, :cond_e1

    .line 597
    new-instance v5, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v5}, Landroid/app/Notification$BigTextStyle;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    goto/16 :goto_e1

    .line 623
    :cond_3bf
    const/4 v5, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    goto/16 :goto_11a

    .line 414
    nop

    :sswitch_data_3c8
    .sparse-switch
        0x2 -> :sswitch_86
        0xf -> :sswitch_86
        0x10 -> :sswitch_20a
        0x12 -> :sswitch_123
        0x18 -> :sswitch_20a
        0x21 -> :sswitch_120
    .end sparse-switch
.end method

.method public static getRingtone(Landroid/content/Context;)Landroid/net/Uri;
    .registers 6
    .parameter "context"

    .prologue
    .line 951
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 952
    .local v3, res:Landroid/content/res/Resources;
    const v4, 0x7f08000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 953
    .local v1, key:Ljava/lang/String;
    const v4, 0x7f08000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 955
    .local v0, defValue:Ljava/lang/String;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 957
    .local v2, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    return-object v4
.end method

.method private static hasOnlyHangoutNotifications(Landroid/database/Cursor;)Z
    .registers 4
    .parameter "cursor"

    .prologue
    .line 279
    const/4 v1, 0x1

    .line 280
    .local v1, returnValue:Z
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 281
    const/4 v2, 0x3

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 282
    .local v0, category:I
    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    .line 283
    const/4 v1, 0x0

    .line 287
    .end local v0           #category:I
    :cond_11
    return v1
.end method

.method public static hasRingtone(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    .prologue
    .line 969
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 970
    .local v3, res:Landroid/content/res/Resources;
    const v4, 0x7f08000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 971
    .local v1, key:Ljava/lang/String;
    const v4, 0x7f08000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 973
    .local v0, defValue:Ljava/lang/String;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 975
    .local v2, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_22

    const/4 v4, 0x1

    :goto_21
    return v4

    :cond_22
    const/4 v4, 0x0

    goto :goto_21
.end method

.method private static isRunningJellybeanOrLater()Z
    .registers 2

    .prologue
    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public static newViewNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;
    .registers 36
    .parameter "context"
    .parameter "account"
    .parameter "cursor"

    .prologue
    .line 713
    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 714
    .local v20, category:I
    const v2, 0xffff

    move/from16 v0, v20

    if-ne v0, v2, :cond_11

    .line 715
    const/16 v27, 0x0

    .line 849
    :cond_10
    :goto_10
    return-object v27

    .line 717
    :cond_11
    const/16 v2, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 719
    .local v5, notificationType:I
    const/4 v2, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 720
    .local v25, displayMessage:Ljava/lang/String;
    const v2, 0x7f0801e2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 721
    .local v31, photoDeletedMessage:Ljava/lang/String;
    const v2, 0x7f0801e1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 722
    .local v26, eventDeletedMessage:Ljava/lang/String;
    const v2, 0x7f0801e0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 726
    .local v32, postDeletedMessage:Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5b

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5b

    invoke-static/range {v25 .. v26}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5b

    move-object/from16 v0, v25

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5e

    .line 730
    :cond_5b
    const/16 v27, 0x0

    goto :goto_10

    .line 733
    :cond_5e
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 734
    .local v8, notificationId:Ljava/lang/String;
    const/16 v27, 0x0

    .line 737
    .local v27, intent:Landroid/content/Intent;
    const/16 v2, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 739
    .local v4, eventId:Ljava/lang/String;
    const/16 v2, 0xe

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 741
    .local v6, creatorId:Ljava/lang/String;
    const/16 v2, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_fd

    const/16 v28, 0x1

    .line 743
    .local v28, isEvent:Z
    :goto_84
    if-eqz v28, :cond_8f

    .line 744
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v27

    .line 748
    :cond_8f
    if-nez v27, :cond_94

    .line 749
    packed-switch v20, :pswitch_data_212

    .line 807
    :cond_94
    :goto_94
    :pswitch_94
    if-eqz v27, :cond_10

    .line 826
    const-string v2, "notif_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 827
    const/16 v2, 0xb

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_20e

    const/16 v29, 0x1

    .line 828
    .local v29, isRead:Z
    :goto_a9
    const-string v2, "com.google.plus.analytics.intent.extra.NOTIFICATION_READ"

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 831
    new-instance v30, Ljava/util/ArrayList;

    const/4 v2, 0x1

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 832
    .local v30, notificationTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 833
    const-string v2, "notif_types"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 836
    const/4 v2, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 838
    .local v22, coalescingCode:Ljava/lang/String;
    new-instance v23, Ljava/util/ArrayList;

    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 839
    .local v23, coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 840
    const-string v2, "coalescing_codes"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 843
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 847
    const/high16 v2, 0x1400

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/16 :goto_10

    .line 741
    .end local v22           #coalescingCode:Ljava/lang/String;
    .end local v23           #coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v28           #isEvent:Z
    .end local v29           #isRead:Z
    .end local v30           #notificationTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_fd
    const/16 v28, 0x0

    goto :goto_84

    .line 752
    .restart local v28       #isEvent:Z
    :pswitch_100
    const/16 v2, 0xa

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 754
    .local v16, activityId:Ljava/lang/String;
    if-eqz v16, :cond_94

    .line 755
    new-instance v27, Landroid/content/Intent;

    .end local v27           #intent:Landroid/content/Intent;
    const-class v2, Lcom/google/android/apps/plus/phone/StreamOneUpActivity;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account"

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "activity_id"

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v8, :cond_140

    const-string v2, "notif_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "notif_category"

    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_140
    const-string v2, "refresh"

    const/4 v3, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .restart local v27       #intent:Landroid/content/Intent;
    goto/16 :goto_94

    .line 762
    .end local v16           #activityId:Ljava/lang/String;
    :pswitch_14a
    const/4 v2, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v21

    .line 763
    .local v21, circleData:[B
    if-eqz v21, :cond_94

    .line 764
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbDataActor;->deserializeDataActorList([B)Ljava/util/List;

    move-result-object v17

    .line 765
    .local v17, actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    if-eqz v17, :cond_94

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_94

    .line 766
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v24

    .line 767
    .local v24, count:I
    const/4 v2, 0x1

    move/from16 v0, v24

    if-ne v0, v2, :cond_17d

    .line 768
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2, v8}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v27

    goto/16 :goto_94

    .line 770
    :cond_17d
    const/4 v2, 0x1

    move/from16 v0, v24

    if-le v0, v2, :cond_94

    .line 771
    new-instance v27, Landroid/content/Intent;

    .end local v27           #intent:Landroid/content/Intent;
    const-class v2, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account"

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "circle_action_data"

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v2, "notif_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .restart local v27       #intent:Landroid/content/Intent;
    goto/16 :goto_94

    .line 780
    .end local v17           #actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    .end local v21           #circleData:[B
    .end local v24           #count:I
    :pswitch_1af
    const/16 v2, 0x12

    if-ne v5, v2, :cond_1bd

    .line 782
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/plus/phone/Intents;->getInstantUploadAlbumIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v27

    goto/16 :goto_94

    .line 785
    :cond_1bd
    const/16 v2, 0x8

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 787
    .local v18, albumId:Ljava/lang/String;
    const/4 v2, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 789
    .local v10, gaiaId:Ljava/lang/String;
    const/16 v2, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 791
    .local v11, photoId:J
    const-wide/16 v2, 0x0

    cmp-long v2, v11, v2

    if-eqz v2, :cond_94

    .line 793
    new-instance v9, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v13, 0x0

    const/4 v14, 0x0

    sget-object v15, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v9 .. v15}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 795
    .local v9, photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v19

    .line 797
    .local v19, builder:Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setForceLoadId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 804
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v27

    goto/16 :goto_94

    .line 827
    .end local v9           #photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v10           #gaiaId:Ljava/lang/String;
    .end local v11           #photoId:J
    .end local v18           #albumId:Ljava/lang/String;
    .end local v19           #builder:Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    :cond_20e
    const/16 v29, 0x0

    goto/16 :goto_a9

    .line 749
    :pswitch_data_212
    .packed-switch 0x1
        :pswitch_100
        :pswitch_14a
        :pswitch_1af
        :pswitch_94
        :pswitch_94
        :pswitch_94
        :pswitch_94
        :pswitch_100
    .end packed-switch
.end method

.method private static newViewOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/PendingIntent;
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "cursor"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 688
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v0

    .line 689
    .local v0, intent:Landroid/content/Intent;
    if-eqz v0, :cond_21

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_21

    .line 690
    const-string v2, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 691
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {p0, v2, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 698
    :goto_20
    return-object v2

    .line 695
    :cond_21
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/phone/Intents;->getNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v1

    .line 696
    .local v1, notifIntent:Landroid/content/Intent;
    const/high16 v2, 0x1400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 697
    const-string v2, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 698
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {p0, v2, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    goto :goto_20
.end method

.method public static shouldNotify(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    .prologue
    .line 915
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 916
    .local v3, res:Landroid/content/res/Resources;
    const v4, 0x7f080008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 917
    .local v1, key:Ljava/lang/String;
    const v4, 0x7f0c0002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 919
    .local v0, defValue:Z
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 921
    .local v2, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    return v4
.end method

.method public static shouldVibrate(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    .prologue
    .line 933
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 934
    .local v3, res:Landroid/content/res/Resources;
    const v4, 0x7f080009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 935
    .local v1, key:Ljava/lang/String;
    const v4, 0x7f0c0003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 937
    .local v0, defValue:Z
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 939
    .local v2, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    return v4
.end method

.method public static showCircleAddFailedNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "name"

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1084
    const/4 v4, 0x0

    invoke-static {p0, p1, p2, v4}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1085
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1086
    const/high16 v4, 0x1400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1087
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {p0, v4, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1089
    .local v1, contentIntent:Landroid/app/PendingIntent;
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 1090
    .local v0, builder:Landroid/support/v4/app/NotificationCompat$Builder;
    const v4, 0x1080027

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1091
    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1092
    const v4, 0x7f08040b

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1093
    const v4, 0x7f08040c

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p3, v5, v6

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1095
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 1097
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 1100
    .local v3, notificationManager:Landroid/app/NotificationManager;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":notifications:add:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1102
    return-void
.end method

.method public static showOutOfBoxRequiredNotification$faab20d()V
    .registers 0

    .prologue
    .line 123
    return-void
.end method

.method public static showUpgradeRequiredNotification(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    .prologue
    .line 89
    const-string v7, "notification"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 92
    .local v3, notificationManager:Landroid/app/NotificationManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 93
    .local v5, when:J
    const v7, 0x7f08003b

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 95
    .local v4, tickerText:Ljava/lang/CharSequence;
    new-instance v2, Landroid/app/Notification;

    const v7, 0x7f020140

    invoke-direct {v2, v7, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 98
    .local v2, notification:Landroid/app/Notification;
    new-instance v1, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 99
    .local v1, intent:Landroid/content/Intent;
    const/high16 v7, 0x8

    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 100
    const-string v7, "market://details?id=com.google.android.apps.plus"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 103
    const/high16 v7, 0x1400

    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    long-to-int v7, v7

    const/4 v8, 0x0

    invoke-static {p0, v7, v1, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 108
    .local v0, contentIntent:Landroid/app/PendingIntent;
    const v7, 0x7f080029

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, p0, v7, v4, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 110
    iget v7, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x10

    iput v7, v2, Landroid/app/Notification;->flags:I

    .line 111
    iget v7, v2, Landroid/app/Notification;->defaults:I

    or-int/lit8 v7, v7, 0x4

    iput v7, v2, Landroid/app/Notification;->defaults:I

    .line 113
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":notifications:upgrade"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v3, v7, v8, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 115
    return-void
.end method

.method public static declared-synchronized update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 7
    .parameter "context"
    .parameter "account"

    .prologue
    .line 132
    const-class v4, Lcom/google/android/apps/plus/service/AndroidNotification;

    monitor-enter v4

    :try_start_3
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 134
    .local v1, notificationManager:Landroid/app/NotificationManager;
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, notificationTag:Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->createNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/Notification;

    move-result-object v0

    .line 137
    .local v0, notification:Landroid/app/Notification;
    if-eqz v0, :cond_42

    .line 138
    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v0, Landroid/app/Notification;->flags:I

    .line 139
    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/app/Notification;->flags:I

    .line 140
    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v0, Landroid/app/Notification;->flags:I

    .line 143
    const/4 v3, -0x1

    iput v3, v0, Landroid/app/Notification;->ledARGB:I

    .line 144
    const/16 v3, 0x1f4

    iput v3, v0, Landroid/app/Notification;->ledOnMS:I

    .line 145
    const/16 v3, 0x7d0

    iput v3, v0, Landroid/app/Notification;->ledOffMS:I

    .line 147
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldVibrate(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3e

    .line 148
    iget v3, v0, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Landroid/app/Notification;->defaults:I

    .line 151
    :cond_3e
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V
    :try_end_42
    .catchall {:try_start_3 .. :try_end_42} :catchall_44

    .line 153
    :cond_42
    monitor-exit v4

    return-void

    .line 132
    .end local v0           #notification:Landroid/app/Notification;
    .end local v1           #notificationManager:Landroid/app/NotificationManager;
    .end local v2           #notificationTag:Ljava/lang/String;
    :catchall_44
    move-exception v3

    monitor-exit v4

    throw v3
.end method
