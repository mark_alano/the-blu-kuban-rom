.class public final Lcom/google/android/apps/plus/service/CircleMembershipManager;
.super Ljava/lang/Object;
.source "CircleMembershipManager.java"


# static fields
.field private static sAccountName:Ljava/lang/String;

.field private static sCompletedRequests:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sHandler:Landroid/os/Handler;

.field private static sPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPeopleListVisible:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sCompletedRequests:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public static isCircleMembershipRequestPending(Ljava/lang/String;)Z
    .registers 2
    .parameter "personId"

    .prologue
    .line 139
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static onPeopleListVisibilityChange(Z)V
    .registers 2
    .parameter "visible"

    .prologue
    .line 39
    sget-boolean v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPeopleListVisible:Z

    if-eq v0, p0, :cond_d

    .line 41
    sput-boolean p0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPeopleListVisible:Z

    if-eqz p0, :cond_d

    .line 42
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sCompletedRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 45
    :cond_d
    return-void
.end method

.method public static onStartAddToCircleRequest$3608be29(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "personId"

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sAccountName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 50
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 51
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sAccountName:Ljava/lang/String;

    .line 53
    :cond_17
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, ""

    invoke-virtual {v0, p2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 55
    return-void
.end method

.method public static setCircleMembershipResult(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "name"
    .parameter "success"

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sAccountName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 73
    :cond_c
    :goto_c
    return-void

    .line 63
    :cond_d
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sCompletedRequests:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v1, ""

    invoke-virtual {v0, p2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    if-nez p4, :cond_c

    .line 67
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/service/AndroidNotification;->showCircleAddFailedNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_c
.end method

.method public static showToastIfNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 15
    .parameter "context"
    .parameter "account"

    .prologue
    .line 77
    sget-boolean v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPeopleListVisible:Z

    if-eqz v0, :cond_5

    .line 136
    :cond_4
    :goto_4
    return-void

    .line 82
    :cond_5
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-gtz v0, :cond_4

    .line 86
    const/4 v6, 0x0

    .line 87
    .local v6, added:Z
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .local v11, sb:Ljava/lang/StringBuilder;
    const-string v0, "in_my_circles!= 0 AND person_id IN ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sCompletedRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :goto_22
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 91
    .local v10, personId:Ljava/lang/String;
    invoke-static {v11, v10}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 92
    const/16 v0, 0x2c

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 93
    const/4 v6, 0x1

    goto :goto_22

    .line 96
    .end local v10           #personId:Ljava/lang/String;
    :cond_38
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sCompletedRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 98
    if-eqz v6, :cond_4

    .line 102
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 103
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    const/4 v7, 0x0

    .line 109
    .local v7, count:I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "person_id"

    aput-object v4, v2, v3

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 112
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_73

    .line 114
    :try_start_6c
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_6f
    .catchall {:try_start_6c .. :try_end_6f} :catchall_a5

    move-result v7

    .line 116
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 119
    :cond_73
    if-eqz v7, :cond_4

    .line 123
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0029

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v7, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 126
    .local v12, text:Ljava/lang/String;
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_99

    .line 127
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sHandler:Landroid/os/Handler;

    .line 129
    :cond_99
    sget-object v0, Lcom/google/android/apps/plus/service/CircleMembershipManager;->sHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/service/CircleMembershipManager$1;

    invoke-direct {v1, p0, v12}, Lcom/google/android/apps/plus/service/CircleMembershipManager$1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4

    .line 116
    .end local v12           #text:Ljava/lang/String;
    :catchall_a5
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method
