.class final Lcom/google/android/apps/plus/service/EsService$13;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/EsService;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4009
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$13;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 15

    .prologue
    .line 4016
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$intent:Landroid/content/Intent;

    const-string v2, "circle_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 4017
    .local v10, circleId:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$intent:Landroid/content/Intent;

    const-string v2, "person_ids"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    .line 4019
    .local v13, personIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x1

    new-array v6, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v10, v6, v1

    .line 4020
    .local v6, circle:[Ljava/lang/String;
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, i$:Ljava/util/Iterator;
    :goto_1a
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_49

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 4021
    .local v3, personId:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4025
    .local v0, op:Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->start()V
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_37} :catch_38

    goto :goto_1a

    .line 4028
    .end local v0           #op:Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;
    .end local v3           #personId:Ljava/lang/String;
    .end local v6           #circle:[Ljava/lang/String;
    .end local v10           #circleId:Ljava/lang/String;
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v13           #personIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_38
    move-exception v11

    .line 4029
    .local v11, ex:Ljava/lang/Exception;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$13;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$intent:Landroid/content/Intent;

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v4, v5, v7, v11}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v5, 0x0

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    .line 4031
    .end local v11           #ex:Ljava/lang/Exception;
    :goto_48
    return-void

    .line 4027
    .restart local v6       #circle:[Ljava/lang/String;
    .restart local v10       #circleId:Ljava/lang/String;
    .restart local v12       #i$:Ljava/util/Iterator;
    .restart local v13       #personIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_49
    :try_start_49
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$13;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$13;->val$intent:Landroid/content/Intent;

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_56
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_56} :catch_38

    goto :goto_48
.end method
