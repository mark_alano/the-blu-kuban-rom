.class final Lcom/google/android/apps/plus/service/EsService$2;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/PhotoCache$PhotoLoader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/plus/service/PhotoCache$PhotoLoader",
        "<",
        "Lcom/google/android/apps/plus/service/EsService$ImageKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static loadBitmapFromStorage(Landroid/content/Context;Lcom/google/android/apps/plus/service/EsService$ImageKey;III)Landroid/graphics/Bitmap;
    .registers 20
    .parameter "context"
    .parameter "key"
    .parameter "width"
    .parameter "height"
    .parameter "cropType"

    .prologue
    .line 576
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/plus/service/EsService$ImageKey;->getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v13

    .line 577
    .local v13, ref:Lcom/google/android/apps/plus/api/MediaRef;
    if-nez v13, :cond_8

    .line 578
    const/4 v10, 0x0

    .line 600
    :cond_7
    :goto_7
    return-object v10

    .line 582
    :cond_8
    :try_start_8
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/google/android/apps/plus/service/EsService$LocalImageKey;

    if-eqz v3, :cond_29

    .line 583
    new-instance v14, Lcom/google/android/apps/plus/content/LocalImageRequest;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {v14, v13, v0, v1}, Lcom/google/android/apps/plus/content/LocalImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .line 584
    .local v14, request:Lcom/google/android/apps/plus/content/LocalImageRequest;
    invoke-static {p0, v14}, Lcom/google/android/apps/plus/content/EsPhotosData;->loadLocalBitmap(Landroid/content/Context;Lcom/google/android/apps/plus/content/LocalImageRequest;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 585
    .local v10, decodedBitmap:Landroid/graphics/Bitmap;
    const/4 v3, 0x2

    move/from16 v0, p4

    if-ne v0, v3, :cond_7

    .line 586
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v10, v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->cropWideBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v10

    goto :goto_7

    .line 589
    .end local v10           #decodedBitmap:Landroid/graphics/Bitmap;
    .end local v14           #request:Lcom/google/android/apps/plus/content/LocalImageRequest;
    :cond_29
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;

    if-eqz v3, :cond_5e

    .line 591
    invoke-virtual {v13}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    invoke-virtual {v13}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    move-object v3, p0

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-static/range {v3 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosData;->loadPicasaImageBytes(Landroid/content/Context;JLjava/lang/String;III)[B

    move-result-object v11

    .line 593
    .local v11, imageBytes:[B
    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v11, v0, v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->createBitmap([BIII)Landroid/graphics/Bitmap;
    :try_end_4b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_4b} :catch_4d

    move-result-object v10

    goto :goto_7

    .line 595
    .end local v11           #imageBytes:[B
    :catch_4d
    move-exception v12

    .line 596
    .local v12, oome:Ljava/lang/OutOfMemoryError;
    const-string v3, "EsService"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5e

    .line 597
    const-string v3, "EsService"

    const-string v4, "Could not load image"

    invoke-static {v3, v4, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 600
    .end local v12           #oome:Ljava/lang/OutOfMemoryError;
    :cond_5e
    const/4 v10, 0x0

    goto :goto_7
.end method


# virtual methods
.method public final bridge synthetic loadBitmapFromStorage(Landroid/content/Context;Ljava/lang/Object;III)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 572
    check-cast p2, Lcom/google/android/apps/plus/service/EsService$ImageKey;

    .end local p2
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/apps/plus/service/EsService$2;->loadBitmapFromStorage(Landroid/content/Context;Lcom/google/android/apps/plus/service/EsService$ImageKey;III)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
