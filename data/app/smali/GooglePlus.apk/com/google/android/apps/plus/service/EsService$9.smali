.class final Lcom/google/android/apps/plus/service/EsService$9;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/EsService;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$gaiaId:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3526
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$9;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$gaiaId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 16

    .prologue
    .line 3529
    new-instance v12, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v12}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    .line 3532
    .local v12, result:Lcom/google/android/apps/plus/service/ServiceResult;
    const/4 v14, 0x1

    .line 3533
    .local v14, success:Z
    const/4 v9, 0x0

    .line 3534
    .local v9, code:I
    const/4 v11, 0x0

    .line 3536
    .local v11, reason:Ljava/lang/String;
    :try_start_8
    new-instance v0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$gaiaId:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 3539
    .local v0, op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 3540
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_47

    .line 3541
    const/4 v14, 0x0

    .line 3542
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    .line 3543
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    .line 3545
    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed user photo; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3549
    :cond_47
    new-instance v0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;

    .end local v0           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$gaiaId:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 3552
    .restart local v0       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 3553
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_85

    .line 3554
    const/4 v14, 0x0

    .line 3555
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    .line 3556
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    .line 3558
    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed photo albums; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3562
    :cond_85
    new-instance v0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    .end local v0           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const-string v4, "camerasync"

    iget-object v5, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$gaiaId:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 3566
    .restart local v0       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 3567
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_c6

    .line 3568
    const/4 v14, 0x0

    .line 3569
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    .line 3570
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    .line 3572
    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed camera photos; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3576
    :cond_c6
    new-instance v0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    .end local v0           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const-string v4, "posts"

    iget-object v5, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$gaiaId:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 3580
    .restart local v0       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 3581
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_107

    .line 3582
    const/4 v14, 0x0

    .line 3583
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    .line 3584
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    .line 3586
    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed post photos; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3590
    :cond_107
    if-nez v14, :cond_110

    .line 3591
    new-instance v13, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    invoke-direct {v13, v9, v11, v1}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_10f
    .catchall {:try_start_8 .. :try_end_10f} :catchall_12b
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_10f} :catch_119

    .end local v12           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    .local v13, result:Lcom/google/android/apps/plus/service/ServiceResult;
    move-object v12, v13

    .line 3596
    .end local v13           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    .restart local v12       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_110
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$9;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$intent:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-static {v1, v2, v12, v3}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    .line 3597
    .end local v0           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    :goto_118
    return-void

    .line 3593
    :catch_119
    move-exception v10

    .line 3594
    .local v10, ex:Ljava/lang/Exception;
    :try_start_11a
    new-instance v13, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v13, v1, v2, v10}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_121
    .catchall {:try_start_11a .. :try_end_121} :catchall_12b

    .line 3596
    .end local v12           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    .restart local v13       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$9;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$intent:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-static {v1, v2, v13, v3}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    move-object v12, v13

    .line 3597
    .end local v13           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    .restart local v12       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    goto :goto_118

    .line 3596
    .end local v10           #ex:Ljava/lang/Exception;
    :catchall_12b
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$9;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$9;->val$intent:Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-static {v2, v3, v12, v4}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    throw v1
.end method
