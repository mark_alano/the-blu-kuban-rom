.class final Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/EsService$ImageKey;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RemoteImageKey"
.end annotation


# instance fields
.field private final mCropType:I

.field private final mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/api/MediaRef;I)V
    .registers 3
    .parameter "mediaRef"
    .parameter "cropType"

    .prologue
    .line 446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 448
    iput p2, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mCropType:I

    .line 449
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 466
    if-ne p1, p0, :cond_5

    .line 473
    :cond_4
    :goto_4
    return v1

    .line 469
    :cond_5
    instance-of v3, p1, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;

    if-nez v3, :cond_b

    move v1, v2

    .line 470
    goto :goto_4

    :cond_b
    move-object v0, p1

    .line 472
    check-cast v0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;

    .line 473
    .local v0, other:Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;
    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    iget v3, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mCropType:I

    iget v4, v0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mCropType:I

    if-eq v3, v4, :cond_4

    :cond_1e
    move v1, v2

    goto :goto_4
.end method

.method public final getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 479
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 481
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mCropType:I

    add-int v0, v1, v2

    .line 482
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[ ref: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", crop: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mCropType:I

    if-nez v0, :cond_2c

    const-string v0, "NONE"

    :goto_1d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2c
    iget v0, p0, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;->mCropType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_34

    const-string v0, "WIDE"

    goto :goto_1d

    :cond_34
    const-string v0, "SQUARE"

    goto :goto_1d
.end method
