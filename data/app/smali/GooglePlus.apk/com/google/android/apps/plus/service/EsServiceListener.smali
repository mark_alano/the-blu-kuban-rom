.class public Lcom/google/android/apps/plus/service/EsServiceListener;
.super Ljava/lang/Object;
.source "EsServiceListener.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountActivated$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 43
    return-void
.end method

.method public onAccountAdded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "account"
    .parameter "result"

    .prologue
    .line 33
    return-void
.end method

.method public onAccountUpgraded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "account"
    .parameter "result"

    .prologue
    .line 63
    return-void
.end method

.method public onAddPeopleToCirclesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 602
    return-void
.end method

.method public onChangeNotificationsRequestComplete$6a63df5(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "account"
    .parameter "result"

    .prologue
    .line 702
    return-void
.end method

.method public onCircleSyncComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 332
    return-void
.end method

.method public onCreateCircleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 647
    return-void
.end method

.method public onCreateComment$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 177
    return-void
.end method

.method public onCreateEventComment$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 187
    return-void
.end method

.method public onCreateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 865
    return-void
.end method

.method public onCreatePhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 383
    return-void
.end method

.method public onCreatePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 2
    .parameter "result"

    .prologue
    .line 253
    return-void
.end method

.method public onCreateProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 625
    return-void
.end method

.method public onDeleteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 141
    return-void
.end method

.method public onDeleteCirclesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 669
    return-void
.end method

.method public onDeleteComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 213
    return-void
.end method

.method public onDeleteEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 883
    return-void
.end method

.method public onDeletePhotoCommentsComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 405
    return-void
.end method

.method public onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 474
    return-void
.end method

.method public onDeletePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 2
    .parameter "result"

    .prologue
    .line 265
    return-void
.end method

.method public onDeleteProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 636
    return-void
.end method

.method public onDeleteReviewComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 939
    return-void
.end method

.method public onDismissSuggestedPeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 691
    return-void
.end method

.method public onEditActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 117
    return-void
.end method

.method public onEditComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 200
    return-void
.end method

.method public onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 394
    return-void
.end method

.method public onEventHomeRequestComplete$b5e9bbb(I)V
    .registers 2
    .parameter "requestId"

    .prologue
    .line 830
    return-void
.end method

.method public onEventInviteComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 901
    return-void
.end method

.method public onEventManageGuestComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 907
    return-void
.end method

.method public onGetActivities$35a362dd(IZILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "newer"
    .parameter "streamSize"
    .parameter "result"

    .prologue
    .line 80
    return-void
.end method

.method public onGetActivity$63505a2b(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "activityId"
    .parameter "result"

    .prologue
    .line 92
    return-void
.end method

.method public onGetActivityAudience$6db92636(ILcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "audienceData"
    .parameter "result"

    .prologue
    .line 105
    return-void
.end method

.method public onGetAlbumComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 362
    return-void
.end method

.method public onGetAlbumListComplete$6a63df5(I)V
    .registers 2
    .parameter "requestId"

    .prologue
    .line 352
    return-void
.end method

.method public onGetEventInviteesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 915
    return-void
.end method

.method public onGetNotificationSettings$434dcfc8(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V
    .registers 4
    .parameter "requestId"
    .parameter "account"
    .parameter "notificationSettings"

    .prologue
    .line 714
    return-void
.end method

.method public onGetPhoto$4894d499(IJ)V
    .registers 4
    .parameter "requestId"
    .parameter "photoId"

    .prologue
    .line 508
    return-void
.end method

.method public onGetPhotoSettings$6e3d3b8d(IZ)V
    .registers 3
    .parameter "requestId"
    .parameter "downloadAllowed"

    .prologue
    .line 520
    return-void
.end method

.method public onGetPhotosOfUserComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 440
    return-void
.end method

.method public onGetProfileAndContactComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 614
    return-void
.end method

.method public onGetStreamPhotosComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 372
    return-void
.end method

.method public onImageThumbnailUploaded$1c9f65a1(ILcom/google/wireless/tacotruck/proto/Network$UploadPhotoResponse;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "response"
    .parameter "result"

    .prologue
    .line 737
    return-void
.end method

.method public onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 803
    return-void
.end method

.method public onLocalImageLoaded(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;II)Z
    .registers 6
    .parameter "ref"
    .parameter "bitmap"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 497
    .local p2, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    return-void
.end method

.method public onLocationQuery$260d7f24(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 290
    return-void
.end method

.method public onModerateComment$56b78e3(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "commentId"
    .parameter "isUndo"
    .parameter "result"

    .prologue
    .line 227
    return-void
.end method

.method public onModifyCirclePropertiesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 658
    return-void
.end method

.method public onMuteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 153
    return-void
.end method

.method public onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "photoId"
    .parameter "result"

    .prologue
    .line 452
    return-void
.end method

.method public onOobRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 544
    return-void
.end method

.method public onPhotoImageLoaded$b81653(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;I)V
    .registers 4
    .parameter "ref"
    .parameter "bitmap"
    .parameter "cropType"

    .prologue
    .line 301
    return-void
.end method

.method public onPhotoPlusOneComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "plusOned"
    .parameter "result"

    .prologue
    .line 430
    return-void
.end method

.method public onPhotosHomeComplete$6a63df5(I)V
    .registers 2
    .parameter "requestId"

    .prologue
    .line 462
    return-void
.end method

.method public onPlusOneApplyResult(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 821
    return-void
.end method

.method public onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "plusOne"
    .parameter "result"

    .prologue
    .line 241
    return-void
.end method

.method public onPostActivityResult(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 812
    return-void
.end method

.method public onReadEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 847
    return-void
.end method

.method public onRemovePeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 680
    return-void
.end method

.method public onReportAbuseRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 578
    return-void
.end method

.method public onReportActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 165
    return-void
.end method

.method public onReportPhotoCommentsComplete$141714ed(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "commentId"
    .parameter "isUndo"
    .parameter "result"

    .prologue
    .line 418
    return-void
.end method

.method public onReportPhotoComplete$4894d499(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 486
    return-void
.end method

.method public onReshareActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 129
    return-void
.end method

.method public onSavePhoto(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "saveToFile"
    .parameter "isFullRes"
    .parameter "description"
    .parameter "mimeType"
    .parameter "result"

    .prologue
    .line 534
    return-void
.end method

.method public onSearchActivitiesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 792
    return-void
.end method

.method public onSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 856
    return-void
.end method

.method public onSetBlockedRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 567
    return-void
.end method

.method public onSetCircleMemebershipComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 589
    return-void
.end method

.method public onSetMutedRequestComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "isMuted"
    .parameter "result"

    .prologue
    .line 556
    return-void
.end method

.method public onSetProfilePhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 725
    return-void
.end method

.method public onSharePhotosToEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 923
    return-void
.end method

.method public onSyncNotifications$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 322
    return-void
.end method

.method public onUpdateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 874
    return-void
.end method

.method public onWriteReviewComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 931
    return-void
.end method
