.class public Lcom/google/android/apps/plus/service/EsSyncAdapterService;
.super Landroid/app/Service;
.source "EsSyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;,
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;,
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;,
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;
    }
.end annotation


# static fields
.field private static sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private static sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

.field private static final sSyncAdapterLock:Ljava/lang/Object;

.field private static sSyncStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 69
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    .line 254
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 320
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .registers 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .registers 1
    .parameter "x0"

    .prologue
    .line 60
    sput-object p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    return-object p0
.end method

.method static synthetic access$100(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/os/Bundle;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/SyncResult;)V
    .registers 16
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    const/4 v7, 0x1

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 60
    invoke-virtual {p3, p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->requestAccountSync(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_248

    :cond_a
    :goto_a
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->pollAccountSyncRequest()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_248

    new-instance v8, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;

    invoke-direct {v8, p4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;-><init>(Landroid/content/SyncResult;)V

    #setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v7}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    const-string v1, "G+ sync"

    invoke-virtual {p3, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v1, "sync_from_tickle"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v0, :cond_88

    :try_start_25
    const-string v2, "sync_what"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v7, :cond_88

    const-string v0, "EsSyncAdapterService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3e

    const-string v0, "EsSyncAdapterService"

    const-string v2, "onPerformSync: ====> Notifications Gsync begin"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3e
    invoke-static {p0, p1, p3, v8, v1}, Lcom/google/android/apps/plus/content/EsNotificationData;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V
    :try_end_41
    .catchall {:try_start_25 .. :try_end_41} :catchall_22f
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_41} :catch_1e9
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_41} :catch_20c

    move v0, v6

    :goto_42
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    #setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    :goto_48
    invoke-virtual {p4}, Landroid/content/SyncResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_237

    const-string v1, "EsSyncAdapterService"

    invoke-static {v1, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5d

    const-string v1, "EsSyncAdapterService"

    const-string v2, "onPerformSync: ====> Sync end with no errors."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5d
    if-eqz v0, :cond_a

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "last_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v9, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_a

    :cond_88
    if-eqz v0, :cond_a9

    :try_start_8a
    const-string v2, "sync_what"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_a9

    const-string v0, "EsSyncAdapterService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a4

    const-string v0, "EsSyncAdapterService"

    const-string v1, "onPerformSync: ====> Events Gsync begin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a4
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/content/EsEventData;->syncCurrentEvents$1ef5a3b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    move v0, v6

    goto :goto_42

    :cond_a9
    const-string v0, "EsSyncAdapterService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b9

    const-string v0, "EsSyncAdapterService"

    const-string v2, "onPerformSync: ====> Full sync begin"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b9
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryNotificationPushEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_cc

    new-instance v0, Lcom/google/android/apps/plus/api/TacoTruckOperation;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, p1, v2, v3}, Lcom/google/android/apps/plus/api/TacoTruckOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/TacoTruckOperation;->enablePush()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/TacoTruckOperation;->start()V

    :cond_cc
    invoke-static {p0, p1, p3, v8}, Lcom/google/android/apps/plus/content/EsPostsData;->syncActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-static {p0, p1, p3, v8, v1}, Lcom/google/android/apps/plus/content/EsNotificationData;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    invoke-static {p0, p1, p3, v8}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    const/4 v0, 0x1

    invoke-static {p0, p1, p3, v8, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->uploadChangedSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v0, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_103

    const-string v1, "EsAccountsData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_103

    const-string v1, "EsAccountsData"

    const-string v2, "Cannot refresh settings"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_103
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_12a

    const-string v1, "EsAccountsData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_12a

    const-string v1, "EsAccountsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot refresh settings: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getErrorCode()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12a
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/content/EsPhotosData;->syncPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->analyzeDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_153

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastStatsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_150

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_153

    :cond_150
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/service/ContactsStatsSync;->sync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    :cond_153
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->removeAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/List;

    move-result-object v0

    const-string v1, "EsSyncAdapterService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_17e

    const-string v1, "EsSyncAdapterService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " analytics events"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17e
    if-eqz v0, :cond_1ce

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1ce

    const-string v1, "SendAnalyticsData"

    invoke-virtual {p3, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/plus/api/PostClientLogsOperation;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2, v8}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->setClientOzEvents(Ljava/util/List;)V

    new-instance v2, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    invoke-virtual {v1, p3, v2}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getErrorCode()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1ad

    if-eqz v1, :cond_1d8

    :cond_1ad
    const-string v3, "EsSyncAdapterService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PostClientLogsOperation failed ex "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " errorCode "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->bulkInsert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    :cond_1ce
    :goto_1ce
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->saveLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    move v0, v7

    goto/16 :goto_42

    :cond_1d8
    const-string v0, "EsSyncAdapterService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1ce

    const-string v0, "EsSyncAdapterService"

    const-string v1, "PostClientLogsOperation was successful"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1e8
    .catchall {:try_start_8a .. :try_end_1e8} :catchall_22f
    .catch Ljava/io/IOException; {:try_start_8a .. :try_end_1e8} :catch_1e9
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_1e8} :catch_20c

    goto :goto_1ce

    :catch_1e9
    move-exception v0

    :try_start_1ea
    const-string v1, "EsSyncAdapterService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1fa

    const-string v1, "EsSyncAdapterService"

    const-string v2, "Sync failure"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1fa
    iget-object v0, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_203
    .catchall {:try_start_1ea .. :try_end_203} :catchall_22f

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    #setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    move v0, v6

    goto/16 :goto_48

    :catch_20c
    move-exception v0

    :try_start_20d
    const-string v1, "EsSyncAdapterService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_21d

    const-string v1, "EsSyncAdapterService"

    const-string v2, "Sync failure"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_21d
    iget-object v0, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_226
    .catchall {:try_start_20d .. :try_end_226} :catchall_22f

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    #setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    move v0, v6

    goto/16 :goto_48

    :catchall_22f
    move-exception v0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    #setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    throw v0

    :cond_237
    const-string v0, "EsSyncAdapterService"

    invoke-static {v0, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "EsSyncAdapterService"

    const-string v1, "onPerformSync: ====> Sync end with error(s)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :cond_248
    return-void
.end method

.method public static activateAccount(Ljava/lang/String;)V
    .registers 4
    .parameter "accountName"

    .prologue
    const/4 v2, 0x1

    .line 774
    invoke-static {p0}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 775
    .local v0, account:Landroid/accounts/Account;
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 776
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 777
    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 778
    return-void
.end method

.method public static deactivateAccount(Ljava/lang/String;)V
    .registers 5
    .parameter "accountName"

    .prologue
    .line 786
    invoke-static {p0}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 787
    .local v0, account:Landroid/accounts/Account;
    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 788
    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v2}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 789
    sget-object v2, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 790
    .local v1, syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    if-eqz v1, :cond_1c

    .line 791
    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    .line 793
    :cond_1c
    return-void
.end method

.method public static getAccountSyncState(Ljava/lang/String;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .registers 4
    .parameter "accountName"

    .prologue
    .line 262
    sget-object v2, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    monitor-enter v2

    .line 263
    :try_start_3
    sget-object v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 264
    .local v0, syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    if-nez v0, :cond_17

    .line 265
    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .end local v0           #syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-direct {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    .line 266
    .restart local v0       #syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    sget-object v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_19

    return-object v0

    .line 269
    .end local v0           #syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    :catchall_19
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    .prologue
    .line 765
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .registers 4

    .prologue
    .line 753
    sget-object v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    .line 754
    :try_start_3
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    if-nez v0, :cond_12

    .line 755
    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    .line 757
    :cond_12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_14

    return-void

    :catchall_14
    move-exception v0

    monitor-exit v1

    throw v0
.end method
