.class final Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageHolder"
.end annotation


# instance fields
.field final bytes:[B

.field final complete:Z

.field decodeInBackground:Z

.field volatile fresh:Z

.field image:Ljava/lang/Object;


# direct methods
.method public constructor <init>([BZ)V
    .registers 4
    .parameter "bytes"
    .parameter "complete"

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    .line 184
    iput-boolean p2, p0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->complete:Z

    .line 185
    return-void
.end method
