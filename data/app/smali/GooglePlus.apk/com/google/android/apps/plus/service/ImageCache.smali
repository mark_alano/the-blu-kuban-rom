.class public final Lcom/google/android/apps/plus/service/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;,
        Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;,
        Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;,
        Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;,
        Lcom/google/android/apps/plus/service/ImageCache$DrawableConsumer;,
        Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;,
        Lcom/google/android/apps/plus/service/ImageCache$OnRemoteDrawableChangeListener;,
        Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;,
        Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;,
        Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
    }
.end annotation


# static fields
.field private static final EMPTY_ARRAY:[B

.field private static final FAILED_IMAGE:Ljava/lang/Object;

.field private static mAvatarListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mMediaImageListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mRemoteImageListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private static mRequestCompleteListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/service/ImageCache$OnImageRequestCompleteListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sMediumAvatarEstimatedSize:I

.field private static sSmallAvatarEstimatedSize:I

.field private static sTinyAvatarEstimatedSize:I


# instance fields
.field private mBackgroundThreadBitmapCount:I

.field private final mContext:Landroid/content/Context;

.field private final mImageCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/android/apps/plus/content/ImageRequest;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mImageHolderCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/android/apps/plus/content/ImageRequest;",
            "Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final mImageHolderCacheRedZoneBytes:I

.field private final mImageHolderQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

.field private mLoadingRequested:Z

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private mPaused:Z

.field private final mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;",
            "Lcom/google/android/apps/plus/content/ImageRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 162
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/plus/service/ImageCache;->EMPTY_ARRAY:[B

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/ImageCache;->FAILED_IMAGE:Ljava/lang/Object;

    .line 220
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mAvatarListeners:Ljava/util/HashSet;

    .line 223
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mMediaImageListeners:Ljava/util/HashSet;

    .line 226
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mRemoteImageListeners:Ljava/util/HashSet;

    .line 229
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mRequestCompleteListeners:Ljava/util/HashSet;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    .prologue
    const/16 v5, 0x30

    const v7, 0x3e99999a

    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderQueue:Ljava/util/ArrayList;

    .line 262
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    .line 268
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;

    .line 307
    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mContext:Landroid/content/Context;

    .line 309
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 311
    .local v2, resources:Landroid/content/res/Resources;
    sget v3, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    if-lt v3, v5, :cond_a2

    .line 313
    const v3, 0x7f0b0002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    div-int/lit8 v4, v4, 0x4

    mul-int/lit16 v4, v4, 0x400

    mul-int/lit16 v4, v4, 0x400

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 321
    .local v0, maxBytesDecoded:I
    :goto_42
    new-instance v3, Lcom/google/android/apps/plus/service/ImageCache$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/plus/service/ImageCache$1;-><init>(Lcom/google/android/apps/plus/service/ImageCache;I)V

    iput-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    .line 337
    sget v3, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    if-lt v3, v5, :cond_aa

    sget v3, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit16 v3, v3, 0x400

    mul-int/lit16 v1, v3, 0x400

    .line 339
    .local v1, maxBytesEncoded:I
    :goto_55
    const/high16 v3, 0x7f0b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 341
    new-instance v3, Lcom/google/android/apps/plus/service/ImageCache$2;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/plus/service/ImageCache$2;-><init>(Lcom/google/android/apps/plus/service/ImageCache;I)V

    iput-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    .line 349
    int-to-double v3, v1

    const-wide v5, 0x3feccccccccccccdL

    mul-double/2addr v3, v5

    double-to-int v3, v3

    iput v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCacheRedZoneBytes:I

    .line 351
    sget v3, Lcom/google/android/apps/plus/service/ImageCache;->sTinyAvatarEstimatedSize:I

    if-nez v3, :cond_a1

    .line 352
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v7

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/service/ImageCache;->sTinyAvatarEstimatedSize:I

    .line 355
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v7

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/service/ImageCache;->sSmallAvatarEstimatedSize:I

    .line 358
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v7

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/service/ImageCache;->sMediumAvatarEstimatedSize:I

    .line 362
    :cond_a1
    return-void

    .line 317
    .end local v0           #maxBytesDecoded:I
    .end local v1           #maxBytesEncoded:I
    :cond_a2
    const v3, 0x7f0b0001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .restart local v0       #maxBytesDecoded:I
    goto :goto_42

    .line 337
    :cond_aa
    const/4 v1, 0x0

    goto :goto_55
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/support/v4/util/LruCache;
    .registers 2
    .parameter "x0"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/service/ImageCache;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCacheRedZoneBytes:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/service/ImageCache;Lcom/google/android/apps/plus/content/ImageRequest;[BZZ)V
    .registers 9
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    const/4 v3, 0x1

    .line 46
    new-instance v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    invoke-direct {v0, p2, p3}, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;-><init>([BZ)V

    iput-boolean v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    if-eqz p3, :cond_22

    if-nez p4, :cond_22

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    iget v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mBackgroundThreadBitmapCount:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_22

    iget v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mBackgroundThreadBitmapCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mBackgroundThreadBitmapCount:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/service/ImageCache;->decodeImage(Lcom/google/android/apps/plus/content/ImageRequest;Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;)V

    :cond_22
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3a

    new-instance v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;-><init>([BZ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3a
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderQueue:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_3d
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderQueue:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_43
    .catchall {:try_start_3d .. :try_end_43} :catchall_44

    return-void

    :catchall_44
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/service/ImageCache;Ljava/util/HashSet;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 46
    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/ImageRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    if-eqz v1, :cond_d

    iget-boolean v1, v1, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->decodeInBackground:Z

    if-eqz v1, :cond_d

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_2b
    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/service/ImageCache;Ljava/util/HashSet;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 46
    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/ImageRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    if-eqz v1, :cond_27

    iget-boolean v1, v1, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    if-nez v1, :cond_d

    :cond_27
    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_2b
    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/service/ImageCache;)Ljava/util/concurrent/ConcurrentHashMap;
    .registers 2
    .parameter "x0"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method private clearTemporaryImageReferences()V
    .registers 5

    .prologue
    .line 953
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderQueue:Ljava/util/ArrayList;

    monitor-enter v3

    .line 954
    :try_start_3
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderQueue:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    .line 955
    .local v0, holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_19

    goto :goto_9

    .line 959
    .end local v0           #holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_19
    move-exception v2

    monitor-exit v3

    throw v2

    .line 958
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_1c
    :try_start_1c
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderQueue:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 959
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_1c .. :try_end_22} :catchall_19

    return-void
.end method

.method private decodeImage(Lcom/google/android/apps/plus/content/ImageRequest;Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;)V
    .registers 6
    .parameter "request"
    .parameter "holder"

    .prologue
    .line 756
    iget-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    if-eqz v1, :cond_5

    .line 789
    :cond_4
    :goto_4
    return-void

    .line 760
    :cond_5
    iget-object v0, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    .line 761
    .local v0, bytes:[B
    if-eqz v0, :cond_4

    array-length v1, v0

    if-eqz v1, :cond_4

    .line 765
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 766
    iget-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    if-eqz v1, :cond_30

    .line 767
    iget-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    instance-of v1, v1, Lcom/google/android/apps/plus/util/GifDrawable;

    if-eqz v1, :cond_4

    .line 770
    iget-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/plus/util/GifDrawable;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/GifDrawable;->newDrawable()Lcom/google/android/apps/plus/util/GifDrawable;

    move-result-object v1

    iput-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 771
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    iget-object v2, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 776
    :cond_30
    invoke-static {v0}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeMedia([B)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 777
    iget-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    if-nez v1, :cond_3e

    .line 778
    sget-object v1, Lcom/google/android/apps/plus/service/ImageCache;->FAILED_IMAGE:Ljava/lang/Object;

    iput-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 782
    :cond_3e
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    iget-object v2, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    .line 786
    sget-object v1, Lcom/google/android/apps/plus/service/ImageCache;->FAILED_IMAGE:Ljava/lang/Object;

    iput-object v1, p2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 787
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    sget-object v2, Lcom/google/android/apps/plus/service/ImageCache;->FAILED_IMAGE:Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4
.end method

.method private ensureLoaderThread()V
    .registers 3

    .prologue
    .line 911
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    if-nez v0, :cond_15

    .line 912
    new-instance v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;-><init>(Lcom/google/android/apps/plus/service/ImageCache;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    .line 913
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->start()V

    .line 915
    :cond_15
    return-void
.end method

.method private evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V
    .registers 4
    .parameter "request"

    .prologue
    .line 903
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    .line 905
    .local v0, holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    if-eqz v0, :cond_12

    .line 906
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    .line 908
    :cond_12
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;
    .registers 4
    .parameter "context"

    .prologue
    .line 300
    const-class v1, Lcom/google/android/apps/plus/service/ImageCache;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->sInstance:Lcom/google/android/apps/plus/service/ImageCache;

    if-nez v0, :cond_12

    .line 301
    new-instance v0, Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/ImageCache;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/service/ImageCache;->sInstance:Lcom/google/android/apps/plus/service/ImageCache;

    .line 303
    :cond_12
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->sInstance:Lcom/google/android/apps/plus/service/ImageCache;
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_16

    monitor-exit v1

    return-object v0

    .line 300
    :catchall_16
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private loadCachedImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;Z)Z
    .registers 11
    .parameter "consumer"
    .parameter "request"
    .parameter "clearIfNotCached"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 668
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    .line 669
    .local v0, holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    if-eqz v0, :cond_2f

    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    if-eqz v3, :cond_2f

    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->complete:Z

    if-eqz v3, :cond_2f

    iget-object v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    if-nez v3, :cond_2f

    iget-object v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    if-eqz v3, :cond_2f

    iget-object v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    array-length v3, v3

    const/16 v4, 0xfa0

    if-le v3, v4, :cond_2f

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2f

    .line 676
    iput-boolean v6, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->decodeInBackground:Z

    .line 730
    .end local p1
    :cond_2e
    :goto_2e
    return v2

    .line 680
    .restart local p1
    :cond_2f
    if-nez v0, :cond_37

    .line 681
    if-eqz p3, :cond_2e

    .line 683
    invoke-interface {p1, v5, v6}, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;->setBitmap(Landroid/graphics/Bitmap;Z)V

    goto :goto_2e

    .line 689
    :cond_37
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p2, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_4e

    .line 693
    new-instance v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    .end local v0           #holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;-><init>([BZ)V

    .line 694
    .restart local v0       #holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p2, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    :cond_4e
    iget-object v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    if-nez v3, :cond_63

    .line 698
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->complete:Z

    if-eqz v3, :cond_5f

    .line 699
    invoke-interface {p1, v5, v2}, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;->setBitmap(Landroid/graphics/Bitmap;Z)V

    .line 700
    invoke-static {p2}, Lcom/google/android/apps/plus/service/ImageCache;->notifyRequestComplete(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 705
    :goto_5c
    iget-boolean v2, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    goto :goto_2e

    .line 703
    :cond_5f
    invoke-interface {p1, v5, v6}, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;->setBitmap(Landroid/graphics/Bitmap;Z)V

    goto :goto_5c

    .line 709
    :cond_63
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/plus/service/ImageCache;->decodeImage(Lcom/google/android/apps/plus/content/ImageRequest;Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;)V

    .line 711
    iget-object v1, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 712
    .local v1, image:Ljava/lang/Object;
    instance-of v3, v1, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_79

    .line 713
    check-cast v1, Landroid/graphics/Bitmap;

    .end local v1           #image:Ljava/lang/Object;
    invoke-interface {p1, v1, v2}, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;->setBitmap(Landroid/graphics/Bitmap;Z)V

    .line 725
    .end local p1
    :cond_71
    :goto_71
    invoke-static {p2}, Lcom/google/android/apps/plus/service/ImageCache;->notifyRequestComplete(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 728
    iput-object v5, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 730
    iget-boolean v2, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    goto :goto_2e

    .line 714
    .restart local v1       #image:Ljava/lang/Object;
    .restart local p1
    :cond_79
    instance-of v3, v1, Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_89

    instance-of v3, p1, Lcom/google/android/apps/plus/service/ImageCache$DrawableConsumer;

    if-eqz v3, :cond_89

    .line 715
    check-cast p1, Lcom/google/android/apps/plus/service/ImageCache$DrawableConsumer;

    .end local p1
    check-cast v1, Landroid/graphics/drawable/Drawable;

    .end local v1           #image:Ljava/lang/Object;
    invoke-interface {p1, v1, v2}, Lcom/google/android/apps/plus/service/ImageCache$DrawableConsumer;->setDrawable(Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_71

    .line 716
    .restart local v1       #image:Ljava/lang/Object;
    .restart local p1
    :cond_89
    instance-of v3, v1, Lcom/google/android/apps/plus/util/GifDrawable;

    if-eqz v3, :cond_97

    .line 717
    check-cast v1, Lcom/google/android/apps/plus/util/GifDrawable;

    .end local v1           #image:Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/GifDrawable;->getFirstFrame()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-interface {p1, v3, v2}, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;->setBitmap(Landroid/graphics/Bitmap;Z)V

    goto :goto_71

    .line 718
    .restart local v1       #image:Ljava/lang/Object;
    :cond_97
    sget-object v3, Lcom/google/android/apps/plus/service/ImageCache;->FAILED_IMAGE:Ljava/lang/Object;

    if-ne v1, v3, :cond_9f

    .line 719
    invoke-interface {p1, v5, v2}, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;->setBitmap(Landroid/graphics/Bitmap;Z)V

    goto :goto_71

    .line 720
    :cond_9f
    if-eqz v1, :cond_71

    .line 721
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot handle drawables of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;Z)V
    .registers 7
    .parameter "consumer"
    .parameter "request"
    .parameter "clearIfNotCached"

    .prologue
    .line 537
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/ImageRequest;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 539
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;->setBitmap(Landroid/graphics/Bitmap;Z)V

    .line 540
    invoke-static {p2}, Lcom/google/android/apps/plus/service/ImageCache;->notifyRequestComplete(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 554
    :cond_e
    :goto_e
    return-void

    .line 542
    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/service/ImageCache;->loadCachedImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;Z)Z

    move-result v0

    .line 544
    .local v0, loaded:Z
    if-eqz v0, :cond_20

    .line 545
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_e

    .line 547
    :cond_20
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    iget-boolean v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPaused:Z

    if-nez v1, :cond_e

    .line 550
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->requestLoading()V

    goto :goto_e
.end method

.method private static notifyRequestComplete(Lcom/google/android/apps/plus/content/ImageRequest;)V
    .registers 3
    .parameter "request"

    .prologue
    .line 656
    sget-object v1, Lcom/google/android/apps/plus/service/ImageCache;->mRequestCompleteListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_6

    .line 659
    :cond_10
    return-void
.end method

.method public static registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 560
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mAvatarListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 561
    return-void
.end method

.method public static registerMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 587
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mMediaImageListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 588
    return-void
.end method

.method public static registerRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 619
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mRemoteImageListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 620
    return-void
.end method

.method private requestLoading()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 809
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoadingRequested:Z

    if-nez v0, :cond_c

    .line 810
    iput-boolean v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoadingRequested:Z

    .line 811
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 813
    :cond_c
    return-void
.end method

.method private trimCache(I)V
    .registers 5
    .parameter "size"

    .prologue
    .line 481
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1}, Landroid/support/v4/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 483
    .local v0, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/google/android/apps/plus/content/ImageRequest;Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;>;>;"
    :goto_e
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1}, Landroid/support/v4/util/LruCache;->size()I

    move-result v1

    if-le v1, p1, :cond_2c

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 484
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_e

    .line 486
    :cond_2c
    return-void
.end method

.method public static unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 567
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mAvatarListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 568
    return-void
.end method

.method public static unregisterMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 594
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mMediaImageListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 595
    return-void
.end method

.method public static unregisterRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 626
    sget-object v0, Lcom/google/android/apps/plus/service/ImageCache;->mRemoteImageListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 627
    return-void
.end method


# virtual methods
.method public final cancel(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;)V
    .registers 3
    .parameter "consumer"

    .prologue
    .line 1413
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1414
    return-void
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 383
    return-void
.end method

.method public final clearFailedRequests()V
    .registers 6

    .prologue
    .line 513
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v4}, Landroid/support/v4/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 514
    .local v2, iterator:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/android/apps/plus/content/ImageRequest;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/ImageRequest;

    .line 515
    .local v3, request:Lcom/google/android/apps/plus/content/ImageRequest;
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v4, v3}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    .line 516
    .local v0, holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    if-eqz v0, :cond_e

    iget-boolean v4, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    if-eqz v4, :cond_e

    iget-object v4, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    if-nez v4, :cond_e

    .line 517
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->fresh:Z

    goto :goto_e

    .line 520
    .end local v0           #holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    .end local v3           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    :cond_30
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .registers 16
    .parameter "msg"

    .prologue
    const/4 v10, 0x0

    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 820
    iget v9, p1, Landroid/os/Message;->what:I

    packed-switch v9, :pswitch_data_170

    move v9, v12

    .line 899
    :goto_a
    return v9

    .line 822
    :pswitch_b
    iput-boolean v12, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoadingRequested:Z

    .line 823
    iget-boolean v9, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPaused:Z

    if-nez v9, :cond_19

    .line 824
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->ensureLoaderThread()V

    .line 825
    iget-object v9, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->requestLoading()V

    :cond_19
    move v9, v11

    .line 827
    goto :goto_a

    .line 831
    :pswitch_1b
    iget-boolean v9, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPaused:Z

    if-nez v9, :cond_59

    .line 832
    iget-object v9, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_29
    :goto_29
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_49

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;

    iget-object v10, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, v9}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/ImageRequest;

    if-eqz v10, :cond_29

    invoke-direct {p0, v9, v10, v12}, Lcom/google/android/apps/plus/service/ImageCache;->loadCachedImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;Z)Z

    move-result v9

    if-eqz v9, :cond_29

    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    goto :goto_29

    :cond_49
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->clearTemporaryImageReferences()V

    iput v12, p0, Lcom/google/android/apps/plus/service/ImageCache;->mBackgroundThreadBitmapCount:I

    iget-object v9, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_59

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->requestLoading()V

    :cond_59
    move v9, v11

    .line 834
    goto :goto_a

    .line 838
    :pswitch_5b
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 840
    .local v1, gaiaId:Ljava/lang/String;
    new-instance v9, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-direct {v9, v1, v12}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/service/ImageCache;->evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 841
    new-instance v9, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-direct {v9, v1, v12, v11}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;IZ)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/service/ImageCache;->evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 842
    new-instance v9, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-direct {v9, v1, v11}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/service/ImageCache;->evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 843
    new-instance v9, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-direct {v9, v1, v11, v11}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;IZ)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/service/ImageCache;->evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 844
    new-instance v9, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-direct {v9, v1, v13}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/service/ImageCache;->evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 845
    new-instance v9, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-direct {v9, v1, v13, v11}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;IZ)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/service/ImageCache;->evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 847
    sget-object v9, Lcom/google/android/apps/plus/service/ImageCache;->mAvatarListeners:Ljava/util/HashSet;

    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_95
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;

    .line 848
    .local v5, listener:Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
    invoke-interface {v5, v1}, Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;->onAvatarChanged(Ljava/lang/String;)V

    goto :goto_95

    .end local v5           #listener:Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
    :cond_a5
    move v9, v11

    .line 850
    goto/16 :goto_a

    .line 854
    .end local v1           #gaiaId:Ljava/lang/String;
    .end local v3           #i$:Ljava/util/Iterator;
    :pswitch_a8
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;

    .line 855
    .local v6, notification:Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;
    iget-object v9, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v9}, Landroid/support/v4/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_ba
    :goto_ba
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_e1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/content/ImageRequest;

    .line 856
    .local v8, request:Lcom/google/android/apps/plus/content/ImageRequest;
    iget-object v9, v6, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;->request:Lcom/google/android/apps/plus/content/MediaImageRequest;

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_ba

    instance-of v9, v8, Lcom/google/android/apps/plus/content/MediaImageRequest;

    if-eqz v9, :cond_ba

    move-object v9, v8

    check-cast v9, Lcom/google/android/apps/plus/content/MediaImageRequest;

    iget-object v10, v6, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;->request:Lcom/google/android/apps/plus/content/MediaImageRequest;

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Lcom/google/android/apps/plus/content/MediaImageRequest;Lcom/google/android/apps/plus/content/MediaImageRequest;)Z

    move-result v9

    if-eqz v9, :cond_ba

    .line 860
    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/service/ImageCache;->evictImage(Lcom/google/android/apps/plus/content/ImageRequest;)V

    goto :goto_ba

    .line 864
    .end local v8           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    :cond_e1
    sget-object v9, Lcom/google/android/apps/plus/service/ImageCache;->mMediaImageListeners:Ljava/util/HashSet;

    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_fd

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;

    .line 865
    .local v5, listener:Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;
    iget-object v9, v6, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;->request:Lcom/google/android/apps/plus/content/MediaImageRequest;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9}, Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;->onMediaImageChanged(Ljava/lang/String;)V

    goto :goto_e7

    .end local v5           #listener:Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;
    :cond_fd
    move v9, v11

    .line 867
    goto/16 :goto_a

    .line 871
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v6           #notification:Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;
    :pswitch_100
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;

    .line 873
    .local v6, notification:Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;
    iget-object v7, v6, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;->request:Lcom/google/android/apps/plus/content/ImageRequest;

    .line 874
    .local v7, notificationRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    iget-object v9, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v9, v7}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    .line 875
    .local v2, holder:Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;
    if-eqz v2, :cond_131

    iget-object v4, v2, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->image:Ljava/lang/Object;

    .line 877
    .local v4, image:Ljava/lang/Object;
    :goto_112
    sget-object v9, Lcom/google/android/apps/plus/service/ImageCache;->mRemoteImageListeners:Ljava/util/HashSet;

    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_118
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_16d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;

    .line 878
    .local v5, listener:Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;
    instance-of v9, v4, Landroid/graphics/Bitmap;

    if-nez v9, :cond_12a

    if-nez v4, :cond_133

    :cond_12a
    move-object v9, v4

    .line 879
    check-cast v9, Landroid/graphics/Bitmap;

    invoke-interface {v5, v7, v9}, Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;->onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/Bitmap;)V

    goto :goto_118

    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #image:Ljava/lang/Object;
    .end local v5           #listener:Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;
    :cond_131
    move-object v4, v10

    .line 875
    goto :goto_112

    .line 880
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #image:Ljava/lang/Object;
    .restart local v5       #listener:Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;
    :cond_133
    instance-of v9, v4, Lcom/google/android/apps/plus/util/GifDrawable;

    if-eqz v9, :cond_14c

    move-object v0, v4

    .line 881
    check-cast v0, Lcom/google/android/apps/plus/util/GifDrawable;

    .line 882
    .local v0, drawable:Lcom/google/android/apps/plus/util/GifDrawable;
    instance-of v9, v5, Lcom/google/android/apps/plus/service/ImageCache$OnRemoteDrawableChangeListener;

    if-eqz v9, :cond_144

    .line 883
    check-cast v5, Lcom/google/android/apps/plus/service/ImageCache$OnRemoteDrawableChangeListener;

    .end local v5           #listener:Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;
    invoke-interface {v5, v7, v0}, Lcom/google/android/apps/plus/service/ImageCache$OnRemoteDrawableChangeListener;->onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/drawable/Drawable;)V

    goto :goto_118

    .line 886
    .restart local v5       #listener:Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;
    :cond_144
    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->getFirstFrame()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-interface {v5, v7, v9}, Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;->onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/Bitmap;)V

    goto :goto_118

    .line 889
    .end local v0           #drawable:Lcom/google/android/apps/plus/util/GifDrawable;
    :cond_14c
    sget-object v9, Lcom/google/android/apps/plus/service/ImageCache;->FAILED_IMAGE:Ljava/lang/Object;

    if-ne v4, v9, :cond_154

    .line 890
    invoke-interface {v5, v7, v10}, Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;->onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/Bitmap;)V

    goto :goto_118

    .line 892
    :cond_154
    new-instance v9, Ljava/lang/UnsupportedOperationException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Unsupported remote image type "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v9

    .end local v5           #listener:Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;
    :cond_16d
    move v9, v11

    .line 896
    goto/16 :goto_a

    .line 820
    :pswitch_data_170
    .packed-switch 0x1
        :pswitch_b
        :pswitch_1b
        :pswitch_5b
        :pswitch_a8
        :pswitch_100
    .end packed-switch
.end method

.method public final loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V
    .registers 4
    .parameter "consumer"
    .parameter "request"

    .prologue
    .line 495
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/service/ImageCache;->loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;Z)V

    .line 496
    return-void
.end method

.method public final notifyAvatarChange(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 575
    if-nez p1, :cond_3

    .line 581
    :goto_2
    return-void

    .line 579
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->ensureLoaderThread()V

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->notifyAvatarChange(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final notifyMediaImageChange(Lcom/google/android/apps/plus/content/CachedImageRequest;[B)V
    .registers 6
    .parameter "request"
    .parameter "imageBytes"

    .prologue
    .line 602
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->ensureLoaderThread()V

    .line 604
    instance-of v1, p1, Lcom/google/android/apps/plus/content/MediaImageRequest;

    if-eqz v1, :cond_19

    .line 605
    new-instance v0, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;-><init>(B)V

    .line 606
    .local v0, notification:Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;
    check-cast p1, Lcom/google/android/apps/plus/content/MediaImageRequest;

    .end local p1
    iput-object p1, v0, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;->request:Lcom/google/android/apps/plus/content/MediaImageRequest;

    .line 607
    iput-object p2, v0, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;->imageBytes:[B

    .line 608
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->notifyMediaImageChange(Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;)V

    .line 613
    .end local v0           #notification:Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;
    :cond_18
    :goto_18
    return-void

    .line 609
    .restart local p1
    :cond_19
    instance-of v1, p1, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    if-eqz v1, :cond_18

    .line 610
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->ensureLoaderThread()V

    .line 611
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    check-cast p1, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    .end local p1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AvatarImageRequest;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->notifyAvatarChange(Ljava/lang/String;)V

    goto :goto_18
.end method

.method public final notifyRemoteImageChange(Lcom/google/android/apps/plus/content/ImageRequest;[B)V
    .registers 5
    .parameter "request"
    .parameter "imageBytes"

    .prologue
    .line 634
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->ensureLoaderThread()V

    .line 635
    new-instance v0, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;-><init>(B)V

    .line 636
    .local v0, notification:Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;
    iput-object p1, v0, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;->request:Lcom/google/android/apps/plus/content/ImageRequest;

    .line 637
    iput-object p2, v0, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;->imageBytes:[B

    .line 638
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->notifyRemoteImageChange(Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;)V

    .line 639
    return-void
.end method

.method public final pause()V
    .registers 2

    .prologue
    .line 792
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPaused:Z

    .line 793
    return-void
.end method

.method public final preloadAvatarsInBackground(Ljava/util/List;)V
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, requests:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    const/4 v5, 0x0

    .line 389
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->ensureLoaderThread()V

    .line 391
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    move v0, v5

    :goto_c
    if-ltz v4, :cond_29

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AvatarRequest;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    if-eqz v3, :cond_27

    iget-object v6, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v6, v2, v3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_23
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_c

    :cond_27
    const/4 v0, 0x1

    goto :goto_23

    .line 392
    .local v0, preloadingNeeded:Z
    :cond_29
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2}, Landroid/support/v4/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v5

    :cond_38
    :goto_38
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/ImageRequest;

    instance-of v3, v2, Lcom/google/android/apps/plus/content/AvatarRequest;

    if-eqz v3, :cond_38

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AvatarRequest;->getSize()I

    move-result v3

    if-nez v3, :cond_38

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    iget-object v4, v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    if-eqz v4, :cond_66

    iget-object v4, v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    array-length v4, v4

    add-int/2addr v1, v4

    :cond_66
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v4, v2, v3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_38

    .line 394
    .local v1, totalTinyAvatarSize:I
    :cond_6c
    if-nez v0, :cond_6f

    .line 401
    .end local v1           #totalTinyAvatarSize:I
    :goto_6e
    return-void

    .line 398
    .restart local v1       #totalTinyAvatarSize:I
    :cond_6f
    iget v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCacheRedZoneBytes:I

    move v4, v5

    move v6, v2

    .end local v1           #totalTinyAvatarSize:I
    :goto_73
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_bd

    iget v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCacheRedZoneBytes:I

    if-lt v1, v2, :cond_8a

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/service/ImageCache;->trimCache(I)V

    invoke-interface {p1, v5, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 400
    :goto_84
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache;->mLoaderThread:Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->startPreloading(Ljava/util/List;)V

    goto :goto_6e

    .line 398
    :cond_8a
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AvatarRequest;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    if-eqz v3, :cond_a8

    iget-object v7, v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    if-eqz v7, :cond_a8

    iget-object v2, v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    array-length v2, v2

    add-int/2addr v1, v2

    move v3, v6

    :goto_a3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v6, v3

    goto :goto_73

    :cond_a8
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AvatarRequest;->getSize()I

    move-result v2

    packed-switch v2, :pswitch_data_c2

    move v2, v5

    :goto_b0
    sub-int v3, v6, v2

    add-int/2addr v1, v2

    goto :goto_a3

    :pswitch_b4
    sget v2, Lcom/google/android/apps/plus/service/ImageCache;->sTinyAvatarEstimatedSize:I

    goto :goto_b0

    :pswitch_b7
    sget v2, Lcom/google/android/apps/plus/service/ImageCache;->sSmallAvatarEstimatedSize:I

    goto :goto_b0

    :pswitch_ba
    sget v2, Lcom/google/android/apps/plus/service/ImageCache;->sMediumAvatarEstimatedSize:I

    goto :goto_b0

    :cond_bd
    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/service/ImageCache;->trimCache(I)V

    goto :goto_84

    nop

    :pswitch_data_c2
    .packed-switch 0x0
        :pswitch_b4
        :pswitch_b7
        :pswitch_ba
    .end packed-switch
.end method

.method public final refreshImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V
    .registers 4
    .parameter "consumer"
    .parameter "request"

    .prologue
    .line 505
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/service/ImageCache;->loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;Z)V

    .line 506
    return-void
.end method

.method public final resume()V
    .registers 2

    .prologue
    .line 796
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPaused:Z

    .line 797
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 798
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache;->requestLoading()V

    .line 800
    :cond_e
    return-void
.end method
