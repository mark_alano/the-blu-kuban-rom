.class public Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;
.super Landroid/app/IntentService;
.source "PackagesMediaMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/PackagesMediaMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AsyncService"
.end annotation


# instance fields
.field private mServiceLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 47
    const-string v0, "GPlusPackageMediaMonitor"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;->setIntentRedelivery(Z)V

    .line 49
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .registers 8
    .parameter "intent"

    .prologue
    .line 67
    :try_start_0
    invoke-static {p0}, Lcom/google/android/picasasync/PicasaFacade;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;

    move-result-object v2

    .line 68
    .local v2, facade:Lcom/google/android/picasasync/PicasaFacade;
    invoke-static {p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v3

    .line 69
    .local v3, storeFacade:Lcom/google/android/picasastore/PicasaStoreFacade;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, action:Ljava/lang/String;
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 73
    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaFacade;->onPackageAdded$552c4e01()V

    .line 74
    invoke-virtual {v3}, Lcom/google/android/picasastore/PicasaStoreFacade;->onPackageAdded$552c4e01()V
    :try_end_21
    .catchall {:try_start_0 .. :try_end_21} :catchall_36

    .line 101
    :cond_21
    :goto_21
    iget-object v4, p0, Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;->mServiceLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 102
    return-void

    .line 75
    :cond_27
    :try_start_27
    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 76
    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaFacade;->onPackageRemoved$552c4e01()V

    .line 77
    invoke-virtual {v3}, Lcom/google/android/picasastore/PicasaStoreFacade;->onPackageRemoved$552c4e01()V
    :try_end_35
    .catchall {:try_start_27 .. :try_end_35} :catchall_36

    goto :goto_21

    .line 101
    .end local v1           #action:Ljava/lang/String;
    .end local v2           #facade:Lcom/google/android/picasasync/PicasaFacade;
    .end local v3           #storeFacade:Lcom/google/android/picasastore/PicasaStoreFacade;
    :catchall_36
    move-exception v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;->mServiceLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v4

    .line 78
    .restart local v1       #action:Ljava/lang/String;
    .restart local v2       #facade:Lcom/google/android/picasasync/PicasaFacade;
    .restart local v3       #storeFacade:Lcom/google/android/picasastore/PicasaStoreFacade;
    :cond_3d
    :try_start_3d
    const-string v4, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_58

    .line 84
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 85
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z
    :try_end_4c
    .catchall {:try_start_3d .. :try_end_4c} :catchall_36

    move-result v4

    if-eqz v4, :cond_21

    .line 91
    const-wide/16 v4, 0x1388

    :try_start_51
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_54
    .catchall {:try_start_51 .. :try_end_54} :catchall_36
    .catch Ljava/lang/InterruptedException; {:try_start_51 .. :try_end_54} :catch_64

    .line 95
    :goto_54
    :try_start_54
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    goto :goto_21

    .line 97
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_58
    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 98
    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaFacade;->onMediaMounted()V
    :try_end_63
    .catchall {:try_start_54 .. :try_end_63} :catchall_36

    goto :goto_21

    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    :catch_64
    move-exception v4

    goto :goto_54
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 6
    .parameter "intent"
    .parameter "startId"

    .prologue
    .line 53
    iget-object v1, p0, Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;->mServiceLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_15

    .line 57
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 58
    .local v0, pm:Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v2, "AsyncService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;->mServiceLock:Landroid/os/PowerManager$WakeLock;

    .line 60
    .end local v0           #pm:Landroid/os/PowerManager;
    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/plus/service/PackagesMediaMonitor$AsyncService;->mServiceLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 61
    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    .line 62
    return-void
.end method
