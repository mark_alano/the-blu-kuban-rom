.class public Lcom/google/android/apps/plus/settings/AboutSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "AboutSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final PRIVACY:Landroid/net/Uri;

.field private static final TERMS:Landroid/net/Uri;

.field private static sLicenseKey:Ljava/lang/String;

.field private static sNetworkStatsKey:Ljava/lang/String;

.field private static sNetworkTransactionsKey:Ljava/lang/String;

.field private static sPrivacyKey:Ljava/lang/String;

.field private static sRemoveAccountKey:Ljava/lang/String;

.field private static sTermsKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 31
    const-string v0, "http://m.google.com/app/plus/serviceurl?type=tos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->TERMS:Landroid/net/Uri;

    .line 32
    const-string v0, "http://m.google.com/app/plus/serviceurl?type=privacy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->PRIVACY:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/net/Uri;
    .registers 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->PRIVACY:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100()Landroid/net/Uri;
    .registers 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->TERMS:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 165
    packed-switch p2, :pswitch_data_1e

    .line 173
    :goto_3
    return-void

    .line 167
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    .line 169
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "sign_out"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 170
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 171
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->finish()V

    goto :goto_3

    .line 165
    :pswitch_data_1e
    .packed-switch -0x1
        :pswitch_4
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 14
    .parameter "savedInstanceState"

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    sget-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sLicenseKey:Ljava/lang/String;

    if-nez v9, :cond_3d

    .line 49
    const v9, 0x7f080022

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sLicenseKey:Ljava/lang/String;

    .line 50
    const v9, 0x7f080023

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sPrivacyKey:Ljava/lang/String;

    .line 51
    const v9, 0x7f080024

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sTermsKey:Ljava/lang/String;

    .line 52
    const v9, 0x7f080025

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sRemoveAccountKey:Ljava/lang/String;

    .line 53
    const v9, 0x7f080026

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkTransactionsKey:Ljava/lang/String;

    .line 54
    const v9, 0x7f080027

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkStatsKey:Ljava/lang/String;

    .line 57
    :cond_3d
    sget-boolean v9, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v9, :cond_47

    .line 58
    const v9, 0x7f05000b

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->addPreferencesFromResource(I)V

    .line 60
    :cond_47
    const/high16 v9, 0x7f05

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->addPreferencesFromResource(I)V

    .line 63
    :try_start_4c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 65
    .local v5, packageInfo:Landroid/content/pm/PackageInfo;
    const-string v9, "build_version"

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    iget-object v10, v5, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_64
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4c .. :try_end_64} :catch_c1

    .line 70
    .end local v5           #packageInfo:Landroid/content/pm/PackageInfo;
    :goto_64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 72
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    sget-boolean v9, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v9, :cond_88

    .line 73
    sget-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkTransactionsKey:Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 74
    .local v3, netRequestsPreference:Landroid/preference/Preference;
    new-instance v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$1;

    invoke-direct {v9, p0, v0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v3, v9}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 86
    sget-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkStatsKey:Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 87
    .local v4, netStatsPreference:Landroid/preference/Preference;
    new-instance v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$2;

    invoke-direct {v9, p0, v0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$2;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v4, v9}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 100
    .end local v3           #netRequestsPreference:Landroid/preference/Preference;
    .end local v4           #netStatsPreference:Landroid/preference/Preference;
    :cond_88
    sget-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sLicenseKey:Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 101
    .local v2, licensePreference:Landroid/preference/Preference;
    new-instance v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$3;

    invoke-direct {v9, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$3;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v2, v9}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 112
    sget-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sPrivacyKey:Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 113
    .local v6, privacyPreference:Landroid/preference/Preference;
    new-instance v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$4;

    invoke-direct {v9, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$4;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v6, v9}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 121
    sget-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sTermsKey:Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    .line 122
    .local v8, termsPreference:Landroid/preference/Preference;
    new-instance v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;

    invoke-direct {v9, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 137
    sget-object v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sRemoveAccountKey:Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .line 138
    .local v7, removePreference:Landroid/preference/Preference;
    new-instance v9, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;

    invoke-direct {v9, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v7, v9}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 145
    return-void

    .line 66
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v2           #licensePreference:Landroid/preference/Preference;
    .end local v6           #privacyPreference:Landroid/preference/Preference;
    .end local v7           #removePreference:Landroid/preference/Preference;
    .end local v8           #termsPreference:Landroid/preference/Preference;
    :catch_c1
    move-exception v1

    .line 67
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "build_version"

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    const-string v10, "?"

    invoke-virtual {v9, v10}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_64
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter "dialogId"
    .parameter "args"

    .prologue
    .line 149
    packed-switch p1, :pswitch_data_28

    .line 159
    const/4 v1, 0x0

    :goto_4
    return-object v1

    .line 151
    :pswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 152
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0802ae

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 153
    const v1, 0x7f0802b0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 154
    const v1, 0x7f0801c4

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 155
    const v1, 0x7f0801c5

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_4

    .line 149
    nop

    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method
