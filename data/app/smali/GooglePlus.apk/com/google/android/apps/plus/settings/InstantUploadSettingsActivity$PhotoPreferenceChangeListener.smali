.class final Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;
.super Ljava/lang/Object;
.source "InstantUploadSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PhotoPreferenceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 348
    iput-object p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 13
    .parameter "preference"
    .parameter "value"

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 351
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 353
    .local v2, key:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$500()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_44

    .line 354
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "account"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    move-object v1, p2

    .line 355
    check-cast v1, Ljava/lang/Boolean;

    .line 357
    .local v1, boolValue:Ljava/lang/Boolean;
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->updateEnabledStates(Z)V
    invoke-static {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$600(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)V

    .line 359
    iget-object v6, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_41

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_35
    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v6, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$700(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 362
    new-instance v5, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$1;

    invoke-direct {v5, p0, v0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$1;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 459
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #boolValue:Ljava/lang/Boolean;
    :cond_40
    :goto_40
    return v8

    .line 359
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    .restart local v1       #boolValue:Ljava/lang/Boolean;
    :cond_41
    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_35

    .line 372
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #boolValue:Ljava/lang/Boolean;
    :cond_44
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$800()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6a

    move-object v1, p2

    .line 373
    check-cast v1, Ljava/lang/Boolean;

    .line 375
    .restart local v1       #boolValue:Ljava/lang/Boolean;
    iget-object v6, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_67

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_5b
    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v6, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$700(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 378
    new-instance v5, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$2;

    invoke-direct {v5, p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$2;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_40

    .line 375
    :cond_67
    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_5b

    .line 386
    .end local v1           #boolValue:Ljava/lang/Boolean;
    :cond_6a
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_80

    move-object v1, p2

    .line 387
    check-cast v1, Ljava/lang/Boolean;

    .line 389
    .restart local v1       #boolValue:Ljava/lang/Boolean;
    new-instance v5, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$3;

    invoke-direct {v5, p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$3;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_40

    .line 399
    .end local v1           #boolValue:Ljava/lang/Boolean;
    :cond_80
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1000()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_db

    move-object v3, p2

    .line 400
    check-cast v3, Ljava/lang/String;

    .line 403
    .local v3, stringValue:Ljava/lang/String;
    const-string v5, "WIFI_ONLY"

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_bf

    .line 404
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$700(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 406
    const v5, 0x7f0800b2

    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(I)V

    .line 407
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 416
    .local v4, wifiOnly:Ljava/lang/Boolean;
    :goto_a6
    if-eqz v4, :cond_40

    .line 417
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "account"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 418
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v5, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$4;

    invoke-direct {v5, p0, v0, v4}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$4;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_40

    .line 408
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v4           #wifiOnly:Ljava/lang/Boolean;
    :cond_bf
    const-string v5, "WIFI_OR_MOBILE"

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d9

    .line 409
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$700(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 410
    const v5, 0x7f0800b3

    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(I)V

    .line 411
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .restart local v4       #wifiOnly:Ljava/lang/Boolean;
    goto :goto_a6

    .line 413
    .end local v4           #wifiOnly:Ljava/lang/Boolean;
    :cond_d9
    const/4 v4, 0x0

    .restart local v4       #wifiOnly:Ljava/lang/Boolean;
    goto :goto_a6

    .line 429
    .end local v3           #stringValue:Ljava/lang/String;
    .end local v4           #wifiOnly:Ljava/lang/Boolean;
    :cond_db
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1100()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_40

    move-object v3, p2

    .line 430
    check-cast v3, Ljava/lang/String;

    .line 433
    .restart local v3       #stringValue:Ljava/lang/String;
    const-string v5, "WIFI_ONLY"

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_11b

    .line 434
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$700(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 435
    const v5, 0x7f0800b7

    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(I)V

    .line 436
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 445
    .restart local v4       #wifiOnly:Ljava/lang/Boolean;
    :goto_101
    if-eqz v4, :cond_40

    .line 446
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "account"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 447
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v5, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$5;

    invoke-direct {v5, p0, v0, v4}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$5;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Boolean;)V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_40

    .line 437
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v4           #wifiOnly:Ljava/lang/Boolean;
    :cond_11b
    const-string v5, "WIFI_OR_MOBILE"

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_135

    .line 438
    iget-object v5, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$700(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 439
    const v5, 0x7f0800b8

    invoke-virtual {p1, v5}, Landroid/preference/Preference;->setSummary(I)V

    .line 440
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .restart local v4       #wifiOnly:Ljava/lang/Boolean;
    goto :goto_101

    .line 442
    .end local v4           #wifiOnly:Ljava/lang/Boolean;
    :cond_135
    const/4 v4, 0x0

    .restart local v4       #wifiOnly:Ljava/lang/Boolean;
    goto :goto_101
.end method
