.class final Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;
.super Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;
.source "InstantUploadSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SystemSettingLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mSettingsUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter "context"

    .prologue
    .line 474
    iput-object p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    .line 475
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 470
    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    .line 472
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->settingsUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mSettingsUri:Landroid/net/Uri;

    .line 476
    return-void
.end method

.method private esLoadInBackground()Ljava/util/Map;
    .registers 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 490
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 491
    .local v14, settings:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 493
    .local v3, resolver:Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 494
    .local v9, autoUploadEnabled:I
    const/16 v21, 0x1

    .line 495
    .local v21, wifiOnlyPhoto:I
    const/16 v22, 0x1

    .line 496
    .local v22, wifiOnlyVideo:I
    const/4 v13, 0x0

    .line 497
    .local v13, roamingUpload:I
    const/4 v11, 0x1

    .line 498
    .local v11, onBatteryUpload:I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mSettingsUri:Landroid/net/Uri;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1200()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 500
    .local v10, cursor:Landroid/database/Cursor;
    if-eqz v10, :cond_47

    .line 502
    :try_start_25
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_44

    .line 503
    const/4 v5, 0x0

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 504
    const/4 v5, 0x1

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 505
    const/4 v5, 0x2

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 506
    const/4 v5, 0x3

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 507
    const/4 v5, 0x4

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_43
    .catchall {:try_start_25 .. :try_end_43} :catchall_e9

    move-result v22

    .line 510
    :cond_44
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 513
    :cond_47
    const-string v5, "auto_upload_enabled"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    const-string v5, "sync_on_wifi_only"

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    const-string v5, "sync_on_roaming"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    const-string v5, "sync_on_battery"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    const-string v5, "video_upload_wifi_only"

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$400(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Landroid/net/Uri;

    move-result-object v4

    .line 521
    .local v4, uploadAll:Landroid/net/Uri;
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 525
    .local v20, uploadStatus:Landroid/database/Cursor;
    :try_start_84
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-eqz v5, :cond_137

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_137

    .line 526
    const/4 v5, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 527
    .local v12, progress:I
    const/4 v5, 0x2

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 528
    .local v19, total:I
    const/4 v5, 0x3

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 530
    .local v15, state:I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    move/from16 v0, v19

    if-eq v0, v12, :cond_ee

    const/4 v5, 0x1

    :goto_ae
    #setter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z
    invoke-static {v6, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$002(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)Z

    .line 532
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    #getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z
    invoke-static {v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z

    move-result v5

    if-nez v5, :cond_f0

    .line 533
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v6, 0x7f0800b9

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 534
    .local v18, title:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v6, 0x7f0800bb

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;
    :try_end_d0
    .catchall {:try_start_84 .. :try_end_d0} :catchall_14e

    move-result-object v17

    .line 552
    .end local v12           #progress:I
    .end local v15           #state:I
    .end local v19           #total:I
    .local v17, summary:Ljava/lang/String;
    :goto_d1
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 555
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget-object v5, v5, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v17

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader$1;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 575
    return-object v14

    .line 510
    .end local v4           #uploadAll:Landroid/net/Uri;
    .end local v17           #summary:Ljava/lang/String;
    .end local v18           #title:Ljava/lang/String;
    .end local v20           #uploadStatus:Landroid/database/Cursor;
    :catchall_e9
    move-exception v5

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v5

    .line 530
    .restart local v4       #uploadAll:Landroid/net/Uri;
    .restart local v12       #progress:I
    .restart local v15       #state:I
    .restart local v19       #total:I
    .restart local v20       #uploadStatus:Landroid/database/Cursor;
    :cond_ee
    const/4 v5, 0x0

    goto :goto_ae

    .line 536
    :cond_f0
    :try_start_f0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v6, 0x7f0800ba

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 537
    .restart local v18       #title:Ljava/lang/String;
    if-eqz v15, :cond_100

    const/4 v5, 0x1

    if-ne v15, v5, :cond_11d

    .line 539
    :cond_100
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v6, 0x7f0800bd

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v7, v8

    const/4 v8, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .restart local v17       #summary:Ljava/lang/String;
    goto :goto_d1

    .line 542
    .end local v17           #summary:Ljava/lang/String;
    :cond_11d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {v5, v15}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$200(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;I)Ljava/lang/String;

    move-result-object v16

    .line 543
    .local v16, stateString:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v6, 0x7f0800be

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v16, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 547
    .restart local v17       #summary:Ljava/lang/String;
    goto :goto_d1

    .line 548
    .end local v12           #progress:I
    .end local v15           #state:I
    .end local v16           #stateString:Ljava/lang/String;
    .end local v17           #summary:Ljava/lang/String;
    .end local v18           #title:Ljava/lang/String;
    .end local v19           #total:I
    :cond_137
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v6, 0x7f0800b9

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 549
    .restart local v18       #title:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    const v6, 0x7f0800bb

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;
    :try_end_14c
    .catchall {:try_start_f0 .. :try_end_14c} :catchall_14e

    move-result-object v17

    .restart local v17       #summary:Ljava/lang/String;
    goto :goto_d1

    .line 552
    .end local v17           #summary:Ljava/lang/String;
    .end local v18           #title:Ljava/lang/String;
    :catchall_14e
    move-exception v5

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    throw v5
.end method


# virtual methods
.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .registers 7
    .parameter "x0"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 466
    check-cast p1, Ljava/util/Map;

    .end local p1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    if-eqz p1, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    #getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mMasterSyncEnabled:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1300(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    #getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mPhotoSyncEnabled:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1400(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_19
    :goto_19
    return-void

    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "auto_upload_enabled"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v0, :cond_dd

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getMasterSwitch()Landroid/widget/Switch;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_da

    move v0, v2

    :goto_3d
    invoke-virtual {v4, v0}, Landroid/widget/Switch;->setChecked(Z)V

    :goto_40
    iget-object v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_eb

    move v0, v2

    :goto_49
    #calls: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->updateEnabledStates(Z)V
    invoke-static {v4, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$600(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$800()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "sync_on_roaming"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_ee

    move v1, v2

    :goto_67
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "sync_on_battery"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v2, :cond_85

    move v3, v2

    :cond_85
    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1600()Z

    move-result v1

    if-eqz v1, :cond_f1

    move v1, v2

    :goto_9b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_fe

    const-string v1, "WIFI_ONLY"

    const v3, 0x7f0800b2

    :goto_aa
    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1100()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1600()Z

    move-result v1

    if-eqz v1, :cond_104

    move v1, v2

    :goto_c3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_111

    const-string v1, "WIFI_ONLY"

    const v2, 0x7f0800b7

    :goto_d2
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto/16 :goto_19

    :cond_da
    move v0, v3

    goto/16 :goto_3d

    :cond_dd
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v2, :cond_e9

    move v4, v2

    :goto_e4
    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_40

    :cond_e9
    move v4, v3

    goto :goto_e4

    :cond_eb
    move v0, v3

    goto/16 :goto_49

    :cond_ee
    move v1, v3

    goto/16 :goto_67

    :cond_f1
    const-string v1, "sync_on_wifi_only"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_9b

    :cond_fe
    const-string v1, "WIFI_OR_MOBILE"

    const v3, 0x7f0800b3

    goto :goto_aa

    :cond_104
    const-string v1, "video_upload_wifi_only"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_c3

    :cond_111
    const-string v1, "WIFI_OR_MOBILE"

    const v2, 0x7f0800b8

    goto :goto_d2
.end method

.method public final bridge synthetic esLoadInBackground()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 466
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->esLoadInBackground()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected final onAbandon()V
    .registers 3

    .prologue
    .line 647
    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_14

    .line 648
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 649
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    .line 651
    :cond_14
    return-void
.end method

.method protected final onReset()V
    .registers 1

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->onAbandon()V

    .line 656
    return-void
.end method

.method protected final onStartLoading()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 480
    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    if-nez v1, :cond_16

    .line 481
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 482
    .local v0, resolver:Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mSettingsUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 483
    iput-boolean v3, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    .line 485
    .end local v0           #resolver:Landroid/content/ContentResolver;
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->forceLoad()V

    .line 486
    return-void
.end method
