.class public Lcom/google/android/apps/plus/settings/SettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "SettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;,
        Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsSyncPreferenceChangeListener;,
        Lcom/google/android/apps/plus/settings/SettingsActivity$InstantUploadSettingsLoader;
    }
.end annotation


# static fields
.field private static sContactsStatsSyncKey:Ljava/lang/String;

.field private static sContactsSyncKey:Ljava/lang/String;

.field private static sHangoutKey:Ljava/lang/String;

.field private static sHangoutOnOffKey:Ljava/lang/String;

.field private static sInstantUploadKey:Ljava/lang/String;

.field private static sMessengerKey:Ljava/lang/String;

.field private static sMessengerOnOffKey:Ljava/lang/String;

.field private static sNotificationsKey:Ljava/lang/String;

.field private static sNotificationsOnOffKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    .line 307
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/settings/SettingsActivity;Lcom/google/android/apps/plus/settings/LabelPreference;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setOnOffLabel(Lcom/google/android/apps/plus/settings/LabelPreference;Z)V

    return-void
.end method

.method private setOnOffLabel(Lcom/google/android/apps/plus/settings/LabelPreference;Z)V
    .registers 5
    .parameter "pref"
    .parameter "on"

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 232
    .local v0, res:Landroid/content/res/Resources;
    if-eqz p2, :cond_1b

    .line 233
    const v1, 0x7f080350

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setLabel(Ljava/lang/CharSequence;)V

    .line 234
    const v1, 0x7f0a00ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setLabelColor(I)V

    .line 239
    :goto_1a
    return-void

    .line 236
    :cond_1b
    const v1, 0x7f080351

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setLabel(Ljava/lang/CharSequence;)V

    .line 237
    const v1, 0x7f0a00ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setLabelColor(I)V

    goto :goto_1a
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v4, 0x7f08005f

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "account"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 59
    .local v1, intentAccount:Landroid/os/Parcelable;
    if-nez v1, :cond_1d

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->finish()V

    .line 125
    :goto_1c
    return-void

    .line 64
    :cond_1d
    sget-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sNotificationsKey:Ljava/lang/String;

    if-nez v4, :cond_72

    .line 65
    const v4, 0x7f080005

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sNotificationsKey:Ljava/lang/String;

    .line 66
    const v4, 0x7f080006

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sMessengerKey:Ljava/lang/String;

    .line 67
    const v4, 0x7f080007

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sHangoutKey:Ljava/lang/String;

    .line 68
    const v4, 0x7f08001f

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    .line 69
    const v4, 0x7f080020

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsSyncKey:Ljava/lang/String;

    .line 70
    const v4, 0x7f080021

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsStatsSyncKey:Ljava/lang/String;

    .line 71
    const v4, 0x7f080008

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sNotificationsOnOffKey:Ljava/lang/String;

    .line 72
    const v4, 0x7f08000d

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sMessengerOnOffKey:Ljava/lang/String;

    .line 73
    const v4, 0x7f080012

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sHangoutOnOffKey:Ljava/lang/String;

    .line 77
    :cond_72
    instance-of v4, v1, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v4, :cond_8d

    move-object v0, v1

    .line 78
    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 97
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    :goto_79
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v4

    if-eqz v4, :cond_ba

    .line 98
    const v4, 0x7f05000a

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->addPreferencesFromResource(I)V

    .line 124
    :goto_85
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->putAccountExtra(Landroid/preference/PreferenceGroup;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_1c

    .line 79
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_8d
    instance-of v4, v1, Landroid/accounts/Account;

    if-eqz v4, :cond_b5

    .line 83
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 84
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_ab

    .line 85
    const v4, 0x7f080055

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->finish()V

    goto/16 :goto_1c

    .line 90
    :cond_ab
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "account"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_79

    .line 93
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_b5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->finish()V

    goto/16 :goto_1c

    .line 100
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_ba
    const v4, 0x7f050009

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->addPreferencesFromResource(I)V

    .line 101
    const v4, 0x7f050001

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->addPreferencesFromResource(I)V

    .line 103
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_e3

    .line 104
    sget-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsSyncKey:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    .line 106
    .local v3, syncPreference:Landroid/preference/CheckBoxPreference;
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 107
    new-instance v4, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsSyncPreferenceChangeListener;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsSyncPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/SettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 111
    .end local v3           #syncPreference:Landroid/preference/CheckBoxPreference;
    :cond_e3
    sget-object v4, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsStatsSyncKey:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 113
    .local v2, statsSyncPreference:Landroid/preference/CheckBoxPreference;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AndroidUtils;->hasTelephony(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_110

    const v4, 0x7f0802ba

    :goto_f8
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 117
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 119
    new-instance v4, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/SettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_85

    .line 113
    :cond_110
    const v4, 0x7f0802bb

    goto :goto_f8
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10001c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 6
    .parameter "item"

    .prologue
    .line 144
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_28

    .line 154
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    :goto_b
    return v2

    .line 146
    :pswitch_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080415

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, helpUrlParam:Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 149
    .local v0, helpUrl:Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/SettingsActivity;->startExternalActivity(Landroid/content/Intent;)V

    .line 150
    const/4 v2, 0x1

    goto :goto_b

    .line 144
    nop

    :pswitch_data_28
    .packed-switch 0x7f0902d3
        :pswitch_c
    .end packed-switch
.end method

.method public onResume()V
    .registers 6

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 129
    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onResume()V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/settings/LabelPreference;

    if-eqz v2, :cond_96

    if-eqz v1, :cond_96

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/settings/LabelPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/settings/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v0, Lcom/google/android/apps/plus/settings/SettingsActivity$InstantUploadSettingsLoader;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/plus/settings/SettingsActivity$InstantUploadSettingsLoader;-><init>(Lcom/google/android/apps/plus/settings/SettingsActivity;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/settings/SettingsActivity$InstantUploadSettingsLoader;->startLoading()V

    .line 133
    :cond_3f
    :goto_3f
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sNotificationsKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/settings/LabelPreference;

    if-eqz v0, :cond_61

    sget-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sNotificationsOnOffKey:Ljava/lang/String;

    const v4, 0x7f0c0002

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setOnOffLabel(Lcom/google/android/apps/plus/settings/LabelPreference;Z)V

    :cond_61
    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sMessengerKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/settings/LabelPreference;

    if-eqz v0, :cond_7b

    sget-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sMessengerOnOffKey:Ljava/lang/String;

    const v4, 0x7f0c0004

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setOnOffLabel(Lcom/google/android/apps/plus/settings/LabelPreference;Z)V

    :cond_7b
    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sHangoutKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/settings/LabelPreference;

    if-eqz v0, :cond_95

    sget-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sHangoutOnOffKey:Ljava/lang/String;

    const v4, 0x7f0c0006

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setOnOffLabel(Lcom/google/android/apps/plus/settings/LabelPreference;Z)V

    .line 134
    :cond_95
    return-void

    .line 131
    :cond_96
    if-eqz v2, :cond_ba

    const v1, 0x7f08002d

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0800ab

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_ae
    new-instance v1, Lcom/google/android/apps/plus/settings/SettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/settings/SettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setOnOffLabel(Lcom/google/android/apps/plus/settings/LabelPreference;Z)V

    goto :goto_3f

    :cond_ba
    const v1, 0x7f0800ac

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setSummary(I)V

    goto :goto_ae
.end method
