.class public final Lcom/google/android/apps/plus/util/EsLog;
.super Ljava/lang/Object;
.source "EsLog.java"


# static fields
.field public static final ENABLE_DOGFOOD_FEATURES:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    return-void
.end method

.method public static isLoggable(Ljava/lang/String;I)Z
    .registers 3
    .parameter "tag"
    .parameter "level"

    .prologue
    .line 27
    sget-boolean v0, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-nez v0, :cond_7

    const/4 v0, 0x6

    if-ne p1, v0, :cond_f

    :cond_7
    invoke-static {p0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static writeToLog(ILjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "priority"
    .parameter "tag"
    .parameter "logEntry"

    .prologue
    const/16 v3, 0xa

    .line 34
    invoke-static {p1, p0}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_9

    .line 46
    :goto_8
    return-void

    .line 38
    :cond_9
    const/4 v1, 0x0

    .line 39
    .local v1, lastIndex:I
    const/4 v2, 0x0

    invoke-virtual {p2, v3, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 40
    .local v0, index:I
    :goto_f
    const/4 v2, -0x1

    if-eq v0, v2, :cond_20

    .line 41
    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 42
    add-int/lit8 v1, v0, 0x1

    .line 43
    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    goto :goto_f

    .line 45
    :cond_20
    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method
