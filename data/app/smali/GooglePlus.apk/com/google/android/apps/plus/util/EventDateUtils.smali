.class public final Lcom/google/android/apps/plus/util/EventDateUtils;
.super Ljava/lang/Object;
.source "EventDateUtils.java"


# static fields
.field private static sEndDateFormat:Ljava/lang/String;

.field private static sRelativeBeginDateFormat:Ljava/lang/String;

.field private static sRelativeEndDateFormat:Ljava/lang/String;

.field private static sStartDateFormat:Ljava/lang/String;

.field private static sToday:Ljava/lang/String;

.field private static sTodayMsec:J

.field private static sTomorrow:Ljava/lang/String;

.field private static sTomorrowMsec:J

.field private static sYesterday:Ljava/lang/String;

.field private static sYesterdayMsec:J


# direct methods
.method public static getDateRange(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;
    .registers 12
    .parameter "context"
    .parameter "start"
    .parameter "end"
    .parameter "abbreviated"

    .prologue
    const-wide/32 v6, 0x5265c00

    .line 102
    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeStrings(Landroid/content/Context;)V

    .line 104
    iget-object v4, p1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 105
    .local v1, startMsec:J
    const v4, 0x10018

    invoke-static {p0, v1, v2, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    sget-wide v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    cmp-long v4, v1, v4

    if-lez v4, :cond_38

    sget-wide v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    add-long/2addr v4, v6

    cmp-long v4, v1, v4

    if-gez v4, :cond_38

    sget-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sToday:Ljava/lang/String;

    .line 106
    .local v0, startDate:Ljava/lang/String;
    :cond_22
    :goto_22
    const/16 v4, 0x4b41

    invoke-static {p0, v1, v2, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, startTime:Ljava/lang/String;
    const-string v4, "%s %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 105
    .end local v0           #startDate:Ljava/lang/String;
    .end local v3           #startTime:Ljava/lang/String;
    :cond_38
    sget-wide v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    cmp-long v4, v1, v4

    if-lez v4, :cond_48

    sget-wide v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    add-long/2addr v4, v6

    cmp-long v4, v1, v4

    if-gez v4, :cond_48

    sget-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrow:Ljava/lang/String;

    goto :goto_22

    :cond_48
    sget-wide v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    cmp-long v4, v1, v4

    if-lez v4, :cond_22

    sget-wide v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    add-long/2addr v4, v6

    cmp-long v4, v1, v4

    if-gez v4, :cond_22

    sget-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterday:Ljava/lang/String;

    goto :goto_22
.end method

.method public static getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;
    .registers 13
    .parameter "context"
    .parameter "inTime"
    .parameter "formatEndDate"

    .prologue
    .line 134
    iget-object v6, p1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 135
    .local v1, dateMsec:J
    const/4 v4, 0x0

    .line 136
    .local v4, relativeDateText:Ljava/lang/String;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 138
    .local v0, date:Ljava/util/Date;
    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeStrings(Landroid/content/Context;)V

    .line 140
    sget-wide v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    cmp-long v6, v1, v6

    if-lez v6, :cond_3e

    sget-wide v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    const-wide/32 v8, 0x5265c00

    add-long/2addr v6, v8

    cmp-long v6, v1, v6

    if-gez v6, :cond_3e

    .line 141
    sget-object v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sToday:Ljava/lang/String;

    .line 148
    :cond_21
    :goto_21
    if-eqz v4, :cond_67

    .line 149
    const/4 v6, 0x3

    invoke-static {v6}, Ljava/text/SimpleDateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v5

    .line 151
    .local v5, timeFormatter:Ljava/text/DateFormat;
    if-eqz p2, :cond_64

    sget-object v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeEndDateFormat:Ljava/lang/String;

    :goto_2c
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v5, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 157
    .end local v5           #timeFormatter:Ljava/text/DateFormat;
    :goto_3d
    return-object v6

    .line 142
    :cond_3e
    sget-wide v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    cmp-long v6, v1, v6

    if-lez v6, :cond_51

    sget-wide v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    const-wide/32 v8, 0x5265c00

    add-long/2addr v6, v8

    cmp-long v6, v1, v6

    if-gez v6, :cond_51

    .line 143
    sget-object v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrow:Ljava/lang/String;

    goto :goto_21

    .line 144
    :cond_51
    sget-wide v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    cmp-long v6, v1, v6

    if-lez v6, :cond_21

    sget-wide v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    const-wide/32 v8, 0x5265c00

    add-long/2addr v6, v8

    cmp-long v6, v1, v6

    if-gez v6, :cond_21

    .line 145
    sget-object v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterday:Ljava/lang/String;

    goto :goto_21

    .line 151
    .restart local v5       #timeFormatter:Ljava/text/DateFormat;
    :cond_64
    sget-object v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeBeginDateFormat:Ljava/lang/String;

    goto :goto_2c

    .line 154
    .end local v5           #timeFormatter:Ljava/text/DateFormat;
    :cond_67
    const/4 v6, 0x2

    const/4 v7, 0x3

    invoke-static {v6, v7}, Ljava/text/SimpleDateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v3

    .line 157
    .local v3, dateTimeFormatter:Ljava/text/DateFormat;
    if-eqz p2, :cond_80

    sget-object v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sEndDateFormat:Ljava/lang/String;

    :goto_71
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_3d

    :cond_80
    sget-object v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sStartDateFormat:Ljava/lang/String;

    goto :goto_71
.end method

.method private static initializeStrings(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    .prologue
    const-wide/32 v6, 0x5265c00

    const/4 v5, 0x0

    .line 202
    sget-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sToday:Ljava/lang/String;

    if-nez v1, :cond_47

    .line 203
    const v1, 0x7f08039f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sToday:Ljava/lang/String;

    .line 204
    const v1, 0x7f0803a0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrow:Ljava/lang/String;

    .line 205
    const v1, 0x7f0803a1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterday:Ljava/lang/String;

    .line 206
    const v1, 0x7f08036d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeBeginDateFormat:Ljava/lang/String;

    .line 207
    const v1, 0x7f08036f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeEndDateFormat:Ljava/lang/String;

    .line 208
    const v1, 0x7f080370

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sEndDateFormat:Ljava/lang/String;

    .line 209
    const v1, 0x7f08036e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sStartDateFormat:Ljava/lang/String;

    .line 213
    :cond_47
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 214
    .local v0, today:Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    sget-wide v3, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_77

    .line 215
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 216
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 217
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 218
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 220
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 221
    sput-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    add-long/2addr v1, v6

    sput-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    .line 222
    sget-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    sub-long/2addr v1, v6

    sput-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    .line 224
    :cond_77
    return-void
.end method
