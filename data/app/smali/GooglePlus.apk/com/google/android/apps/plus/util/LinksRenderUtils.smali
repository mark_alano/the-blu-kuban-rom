.class public final Lcom/google/android/apps/plus/util/LinksRenderUtils;
.super Ljava/lang/Object;
.source "LinksRenderUtils.java"


# static fields
.field protected static sHorizontalSpacing:I

.field protected static sIconHorizontalSpacing:I

.field protected static sImageBorderPaint:Landroid/graphics/Paint;

.field protected static sImageHorizontalSpacing:I

.field protected static sImageMaxWidthPercentage:F

.field protected static sLinkTitleTextPaint:Landroid/text/TextPaint;

.field protected static sLinkUrlTextPaint:Landroid/text/TextPaint;

.field private static sLinksCardViewInitialized:Z

.field protected static sLinksTopAreaBackgroundPaint:Landroid/graphics/Paint;

.field protected static sMaxImageDimension:I

.field protected static final sResizePaint:Landroid/graphics/Paint;

.field protected static final sTransparentResizePaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x2

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sTransparentResizePaint:Landroid/graphics/Paint;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sResizePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public static createBackgroundRects(IIIIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 15
    .parameter "width"
    .parameter "height"
    .parameter "imageDimension"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "srcRect"
    .parameter "destRect"

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f80

    .line 105
    int-to-float v2, p0

    mul-float/2addr v2, v4

    int-to-float v3, p1

    div-float v0, v2, v3

    .line 106
    .local v0, aspectRatio:F
    cmpl-float v2, v0, v4

    if-lez v2, :cond_1c

    .line 107
    int-to-float v2, p2

    int-to-float v3, p2

    div-float/2addr v3, v0

    sub-float/2addr v2, v3

    float-to-int v2, v2

    div-int/lit8 v1, v2, 0x2

    .line 108
    .local v1, offset:I
    sub-int v2, p2, v1

    invoke-virtual {p7, v5, v1, p2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 114
    :goto_18
    invoke-virtual {p8, p3, p4, p5, p6}, Landroid/graphics/Rect;->set(IIII)V

    .line 115
    return-void

    .line 110
    .end local v1           #offset:I
    :cond_1c
    int-to-float v2, p2

    int-to-float v3, p2

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    float-to-int v2, v2

    div-int/lit8 v1, v2, 0x2

    .line 111
    .restart local v1       #offset:I
    sub-int v2, p2, v1

    invoke-virtual {p7, v1, v5, v2, p2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_18
.end method

.method public static createImageRects(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 13
    .parameter "height"
    .parameter "imageDimension"
    .parameter "left"
    .parameter "top"
    .parameter "imageRect"
    .parameter "imageBorderRect"

    .prologue
    .line 119
    sget v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    add-int v1, p2, v3

    .line 120
    .local v1, xOffset:I
    sub-int v3, p0, p1

    div-int/lit8 v3, v3, 0x2

    add-int v2, p3, v3

    .line 121
    .local v2, yOffset:I
    add-int v3, v1, p1

    add-int v4, v2, p1

    invoke-virtual {p4, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 123
    sget-object v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    float-to-int v0, v3

    .line 124
    .local v0, strokeWidth:I
    add-int v3, v1, v0

    add-int v4, v2, v0

    add-int v5, v1, p1

    sub-int/2addr v5, v0

    add-int v6, v2, p1

    sub-int/2addr v6, v0

    invoke-virtual {p5, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 126
    return-void
.end method

.method public static createTitle(Ljava/lang/String;II)Landroid/text/StaticLayout;
    .registers 7
    .parameter "title"
    .parameter "imageDimension"
    .parameter "width"

    .prologue
    .line 129
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 130
    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    div-int v2, p1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 132
    .local v0, maxLines:I
    if-lez v0, :cond_2d

    .line 133
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p2, v2

    sget v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    sub-int/2addr v2, v3

    invoke-static {v1, p0, v2, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    .line 138
    .end local v0           #maxLines:I
    :goto_2c
    return-object v1

    :cond_2d
    const/4 v1, 0x0

    goto :goto_2c
.end method

.method public static createUrl(Ljava/lang/String;III)Landroid/text/StaticLayout;
    .registers 9
    .parameter "url"
    .parameter "imageDimension"
    .parameter "width"
    .parameter "titleHeight"

    .prologue
    .line 143
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_31

    .line 144
    const/4 v1, 0x1

    sub-int v2, p1, p3

    sget-object v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 146
    .local v0, maxLines:I
    if-lez v0, :cond_31

    .line 147
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p2, v2

    sget v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    sub-int/2addr v2, v3

    sget v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sIconHorizontalSpacing:I

    sub-int/2addr v2, v3

    invoke-static {v1, p0, v2, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    .line 154
    .end local v0           #maxLines:I
    :goto_30
    return-object v1

    :cond_31
    const/4 v1, 0x0

    goto :goto_30
.end method

.method public static drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 8
    .parameter "canvas"
    .parameter "bitmap"
    .parameter "backgroundSrcRect"
    .parameter "backgroundDestRect"
    .parameter "imageRect"
    .parameter "imageBorderRect"

    .prologue
    .line 159
    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sTransparentResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 160
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v0, p4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 161
    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p5, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 162
    return-void
.end method

.method public static drawTitleAndUrl(Landroid/graphics/Canvas;IILandroid/text/StaticLayout;Landroid/text/StaticLayout;Landroid/graphics/Bitmap;)V
    .registers 9
    .parameter "canvas"
    .parameter "xOffset"
    .parameter "yOffset"
    .parameter "titleLayout"
    .parameter "urlLayout"
    .parameter "linkBitmap"

    .prologue
    .line 166
    sget v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    sget v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    add-int/2addr v0, v1

    add-int/2addr p1, v0

    .line 168
    if-eqz p3, :cond_1c

    .line 169
    int-to-float v0, p1

    int-to-float v1, p2

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 170
    invoke-virtual {p3, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 171
    neg-int v0, p1

    int-to-float v0, v0

    neg-int v1, p2

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 172
    invoke-virtual {p3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 175
    :cond_1c
    if-eqz p4, :cond_4a

    .line 176
    int-to-float v0, p1

    invoke-virtual {p4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p0, p5, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 181
    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sIconHorizontalSpacing:I

    add-int/2addr v0, v1

    add-int/2addr p1, v0

    .line 182
    int-to-float v0, p1

    int-to-float v1, p2

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 183
    invoke-virtual {p4, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 184
    neg-int v0, p1

    int-to-float v0, v0

    neg-int v1, p2

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 185
    invoke-virtual {p4}, Landroid/text/StaticLayout;->getHeight()I

    .line 187
    :cond_4a
    return-void
.end method

.method public static getImageMaxWidthPercentage()F
    .registers 1

    .prologue
    .line 92
    sget v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageMaxWidthPercentage:F

    return v0
.end method

.method public static getLinksTopAreaBackgroundPaint()Landroid/graphics/Paint;
    .registers 1

    .prologue
    .line 100
    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinksTopAreaBackgroundPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public static getMaxImageDimension()I
    .registers 1

    .prologue
    .line 96
    sget v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sMaxImageDimension:I

    return v0
.end method

.method public static init(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    .prologue
    const v8, 0x7f0d0152

    const v7, 0x7f0d014e

    const/4 v6, 0x1

    .line 41
    sget-boolean v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinksCardViewInitialized:Z

    if-nez v1, :cond_11b

    .line 42
    sput-boolean v6, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinksCardViewInitialized:Z

    .line 44
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    .local v0, res:Landroid/content/res/Resources;
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sTransparentResizePaint:Landroid/graphics/Paint;

    const v2, 0x7f0b0009

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 48
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 49
    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinksTopAreaBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a0001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 52
    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00ce

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0d0149

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 57
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 58
    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 59
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00cf

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 60
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 61
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 62
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d014f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f0d0150

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const v4, 0x7f0d0151

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    const v5, 0x7f0a00d0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 67
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 70
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 71
    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 72
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00d1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 73
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 74
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 75
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0153

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f0d0154

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const v4, 0x7f0d0155

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    const v5, 0x7f0a00d2

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 80
    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 83
    const v1, 0x7f0d014b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageMaxWidthPercentage:F

    .line 84
    const v1, 0x7f0d0148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sMaxImageDimension:I

    .line 85
    const v1, 0x7f0d014a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    .line 86
    const v1, 0x7f0d014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    .line 87
    const v1, 0x7f0d014d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sIconHorizontalSpacing:I

    .line 89
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_11b
    return-void
.end method
