.class public final Lcom/google/android/apps/plus/util/LocationUtils;
.super Ljava/lang/Object;
.source "LocationUtils.java"


# direct methods
.method public static convertLocationToPlace(Lcom/google/api/services/plusi/model/Location;)Lcom/google/api/services/plusi/model/Place;
    .registers 8
    .parameter "location"

    .prologue
    const-wide v5, 0x416312d000000000L

    .line 32
    if-nez p0, :cond_9

    .line 33
    const/4 v1, 0x0

    .line 51
    :cond_8
    :goto_8
    return-object v1

    .line 36
    :cond_9
    new-instance v1, Lcom/google/api/services/plusi/model/Place;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/Place;-><init>()V

    .line 37
    .local v1, place:Lcom/google/api/services/plusi/model/Place;
    iget-object v2, p0, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    if-eqz v2, :cond_3d

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    if-eqz v2, :cond_3d

    .line 38
    new-instance v2, Lcom/google/api/services/plusi/model/GeoCoordinates;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/GeoCoordinates;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    .line 39
    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/GeoCoordinates;->latitude:Ljava/lang/Double;

    .line 40
    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/GeoCoordinates;->longitude:Ljava/lang/Double;

    .line 43
    :cond_3d
    iget-object v2, p0, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_57

    iget-object v0, p0, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    .line 44
    .local v0, locationName:Ljava/lang/String;
    :goto_47
    if-eqz v0, :cond_8

    .line 45
    iput-object v0, v1, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    .line 47
    new-instance v2, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    .line 48
    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iput-object v0, v2, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    goto :goto_8

    .line 43
    .end local v0           #locationName:Ljava/lang/String;
    :cond_57
    iget-object v0, p0, Lcom/google/api/services/plusi/model/Location;->bestAddress:Ljava/lang/String;

    goto :goto_47
.end method
