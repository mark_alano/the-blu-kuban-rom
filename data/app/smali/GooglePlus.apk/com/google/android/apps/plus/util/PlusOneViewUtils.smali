.class public final Lcom/google/android/apps/plus/util/PlusOneViewUtils;
.super Ljava/lang/Object;
.source "PlusOneViewUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;
    }
.end annotation


# static fields
.field private static sInitialized:Z

.field private static sPlusOneButtonDescActive:Ljava/lang/CharSequence;

.field private static sPlusOneButtonDescInactive:Ljava/lang/CharSequence;

.field private static sPlusOneButtonSelectionPadding:I

.field private static sPlusOneByMeIcon:Landroid/graphics/Bitmap;

.field private static sPlusOneByMePressedIcon:Landroid/graphics/Bitmap;

.field private static sPlusOneCountDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field private static sPlusOneCountPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field private static sPlusOneIcon:Landroid/graphics/Bitmap;

.field private static sPlusOneIconYOffset:I

.field private static sPlusOnePaint:Landroid/text/TextPaint;

.field private static sPlusOnePressedIcon:Landroid/graphics/Bitmap;


# direct methods
.method public static setupButtons(Landroid/content/Context;Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;Lcom/google/api/services/plusi/model/DataPlusOne;IIIZ)Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;
    .registers 28
    .parameter "context"
    .parameter "clickableImageButtonListener"
    .parameter "clickableButtonListener"
    .parameter "plusOneData"
    .parameter "xStart"
    .parameter "contentWidth"
    .parameter "yStart"
    .parameter "offsetUpByHeight"

    .prologue
    .line 79
    sget-boolean v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sInitialized:Z

    if-nez v2, :cond_96

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v2, 0x7f020054

    invoke-static {v5, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneIcon:Landroid/graphics/Bitmap;

    const v2, 0x7f020055

    invoke-static {v5, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOnePressedIcon:Landroid/graphics/Bitmap;

    const v2, 0x7f020056

    invoke-static {v5, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneByMeIcon:Landroid/graphics/Bitmap;

    const v2, 0x7f020057

    invoke-static {v5, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneByMePressedIcon:Landroid/graphics/Bitmap;

    const v2, 0x7f020064

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneCountDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    const v2, 0x7f020065

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneCountPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOnePaint:Landroid/text/TextPaint;

    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOnePaint:Landroid/text/TextPaint;

    const v8, 0x7f0a000b

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOnePaint:Landroid/text/TextPaint;

    const v8, 0x7f0d000e

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    invoke-virtual {v2, v8}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOnePaint:Landroid/text/TextPaint;

    const v8, 0x7f0d000e

    invoke-static {v2, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    const v2, 0x7f0803de

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneButtonDescInactive:Ljava/lang/CharSequence;

    const v2, 0x7f0803df

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneButtonDescActive:Ljava/lang/CharSequence;

    const v2, 0x7f0d000c

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneIconYOffset:I

    const v2, 0x7f0d000d

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneButtonSelectionPadding:I

    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sInitialized:Z

    .line 80
    :cond_96
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 82
    .local v19, res:Landroid/content/res/Resources;
    if-eqz p3, :cond_138

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    if-eqz v2, :cond_134

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 84
    .local v17, plusOneCount:I
    :goto_aa
    if-eqz p3, :cond_13c

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-eqz v2, :cond_13c

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_13c

    const/16 v16, 0x1

    .line 87
    .local v16, plusOneByMe:Z
    :goto_be
    if-eqz v16, :cond_13f

    sget-object v3, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneByMeIcon:Landroid/graphics/Bitmap;

    .line 88
    .local v3, plusOneIcon:Landroid/graphics/Bitmap;
    :goto_c2
    if-eqz v16, :cond_142

    sget-object v6, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneButtonDescActive:Ljava/lang/CharSequence;

    .line 90
    .local v6, plusOneDescription:Ljava/lang/CharSequence;
    :goto_c6
    if-eqz v16, :cond_145

    sget-object v4, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneByMePressedIcon:Landroid/graphics/Bitmap;

    .line 93
    .local v4, plusOnePressedIcon:Landroid/graphics/Bitmap;
    :goto_ca
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableImageButton;

    move-object/from16 v2, p0

    move-object/from16 v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/ClickableImageButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Lcom/google/android/apps/plus/views/ClickableImageButton$ClickableImageButtonListener;Ljava/lang/CharSequence;)V

    .line 95
    .local v1, plusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;
    add-int v2, p4, p5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int/2addr v2, v5

    sget v5, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneIconYOffset:I

    add-int v5, v5, p6

    invoke-virtual {v1, v2, v5}, Lcom/google/android/apps/plus/views/ClickableImageButton;->setPosition(II)V

    .line 97
    sget v2, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneButtonSelectionPadding:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ClickableImageButton;->setSelectionPadding(I)V

    .line 99
    const v2, 0x7f08039d

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v8

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 100
    .local v9, plusOneText:Ljava/lang/String;
    new-instance v7, Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v10, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOnePaint:Landroid/text/TextPaint;

    sget-object v11, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneCountDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v12, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneCountPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v14, 0x0

    move-object/from16 v8, p0

    move-object/from16 v13, p2

    move/from16 v15, p6

    invoke-direct/range {v7 .. v15}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    .line 103
    .local v7, plusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableImageButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sget v8, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneButtonSelectionPadding:I

    sub-int/2addr v5, v8

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v5, v8

    move/from16 v0, p6

    invoke-virtual {v2, v5, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 107
    new-instance v18, Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;-><init>()V

    .line 115
    .local v18, plusOneViews:Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;
    move-object/from16 v0, v18

    iput-object v1, v0, Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;

    .line 116
    move-object/from16 v0, v18

    iput-object v7, v0, Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;->mPlusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 117
    return-object v18

    .line 82
    .end local v1           #plusOneButton:Lcom/google/android/apps/plus/views/ClickableImageButton;
    .end local v3           #plusOneIcon:Landroid/graphics/Bitmap;
    .end local v4           #plusOnePressedIcon:Landroid/graphics/Bitmap;
    .end local v6           #plusOneDescription:Ljava/lang/CharSequence;
    .end local v7           #plusOneCountButton:Lcom/google/android/apps/plus/views/ClickableButton;
    .end local v9           #plusOneText:Ljava/lang/String;
    .end local v16           #plusOneByMe:Z
    .end local v17           #plusOneCount:I
    .end local v18           #plusOneViews:Lcom/google/android/apps/plus/util/PlusOneViewUtils$PlusOneViews;
    :cond_134
    const/16 v17, 0x0

    goto/16 :goto_aa

    :cond_138
    const/16 v17, 0x0

    goto/16 :goto_aa

    .line 84
    .restart local v17       #plusOneCount:I
    :cond_13c
    const/16 v16, 0x0

    goto :goto_be

    .line 87
    .restart local v16       #plusOneByMe:Z
    :cond_13f
    sget-object v3, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneIcon:Landroid/graphics/Bitmap;

    goto :goto_c2

    .line 88
    .restart local v3       #plusOneIcon:Landroid/graphics/Bitmap;
    :cond_142
    sget-object v6, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOneButtonDescInactive:Ljava/lang/CharSequence;

    goto :goto_c6

    .line 90
    .restart local v6       #plusOneDescription:Ljava/lang/CharSequence;
    :cond_145
    sget-object v4, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->sPlusOnePressedIcon:Landroid/graphics/Bitmap;

    goto :goto_ca
.end method
