.class public Lcom/google/android/apps/plus/views/ArticleActivityPreviewView;
.super Lcom/google/android/apps/plus/views/ActivityPreviewView;
.source "ArticleActivityPreviewView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method


# virtual methods
.method public setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    .registers 15
    .parameter "activity"

    .prologue
    const/4 v5, 0x0

    const/16 v12, 0x8

    const/4 v11, 0x0

    .line 54
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;->setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ArticleActivityPreviewView;->getActivity()Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ArticleActivityPreviewView;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ArticleActivityPreviewView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300b2

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/views/ArticleActivityPreviewView;->addView(Landroid/view/View;)V

    const v1, 0x7f09020e

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/EsImageView;

    const v2, 0x7f09020f

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f090211

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090210

    invoke-virtual {v6, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->getContent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->getFavIconUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->getImages()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_c0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->getImages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_66
    if-eqz v7, :cond_b0

    const-string v5, ""

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b0

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_76
    if-eqz v8, :cond_b4

    const-string v2, ""

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b4

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_86
    if-eqz v9, :cond_b8

    const-string v2, ""

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b8

    invoke-virtual {v1, v11}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    :goto_96
    if-eqz v0, :cond_bc

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_bc

    invoke-virtual {v4, v11}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    :goto_a6
    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ArticleActivityPreviewView;->invalidate()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ArticleActivityPreviewView;->requestLayout()V

    .line 58
    return-void

    .line 55
    :cond_b0
    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_76

    :cond_b4
    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_86

    :cond_b8
    invoke-virtual {v1, v12}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    goto :goto_96

    :cond_bc
    invoke-virtual {v4, v12}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    goto :goto_a6

    :cond_c0
    move-object v0, v5

    goto :goto_66
.end method
