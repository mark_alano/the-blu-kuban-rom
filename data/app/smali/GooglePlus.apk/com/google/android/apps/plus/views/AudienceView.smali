.class public Lcom/google/android/apps/plus/views/AudienceView;
.super Landroid/widget/FrameLayout;
.source "AudienceView.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/AudienceView$SavedState;,
        Lcom/google/android/apps/plus/views/AudienceView$CirclesQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAudienceChangedCallback:Ljava/lang/Runnable;

.field protected final mCanRemoveChips:Z

.field protected mChipContainer:Landroid/view/ViewGroup;

.field protected final mChips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;"
        }
    .end annotation
.end field

.field protected mEdited:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 42
    const-class v0, Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/plus/views/AudienceView;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 154
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    .line 155
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"
    .parameter "canRemoveChips"

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->init()V

    .line 167
    iput-boolean p4, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    .line 168
    return-void
.end method

.method private getCloseIcon()I
    .registers 2

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    if-eqz v0, :cond_8

    .line 412
    const v0, 0x7f020090

    .line 414
    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 16
    .parameter "audience"

    .prologue
    .line 263
    new-instance v3, Ljava/util/ArrayList;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-direct {v3, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 264
    .local v3, currentChips:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AudienceData;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v2

    .line 265
    .local v2, currentAudience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v4

    .line 266
    .local v4, currentCircles:[Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v5

    .line 268
    .local v5, currentUsers:[Lcom/google/android/apps/plus/content/PersonData;
    iget-object v12, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    .line 271
    const/4 v6, 0x0

    .local v6, i:I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v10

    .local v10, n:I
    :goto_1d
    if-ge v6, v10, :cond_33

    .line 272
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/content/AudienceData;

    .line 273
    .local v8, item:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {p1, v8}, Lcom/google/android/apps/plus/util/PeopleUtils;->in(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v12

    if-eqz v12, :cond_30

    .line 274
    iget-object v12, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_30
    add-int/lit8 v6, v6, 0x1

    goto :goto_1d

    .line 279
    .end local v8           #item:Lcom/google/android/apps/plus/content/AudienceData;
    :cond_33
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v9, v0

    .local v9, len$:I
    const/4 v7, 0x0

    .local v7, i$:I
    :goto_39
    if-ge v7, v9, :cond_50

    aget-object v1, v0, v7

    .line 280
    .local v1, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-static {v4, v1}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z

    move-result v12

    if-nez v12, :cond_4d

    .line 283
    iget-object v12, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v13, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v13, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    :cond_4d
    add-int/lit8 v7, v7, 0x1

    goto :goto_39

    .line 288
    .end local v1           #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_50
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v9, v0

    const/4 v7, 0x0

    :goto_56
    if-ge v7, v9, :cond_6d

    aget-object v11, v0, v7

    .line 289
    .local v11, user:Lcom/google/android/apps/plus/content/PersonData;
    invoke-static {v5, v11}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v12

    if-nez v12, :cond_6a

    .line 292
    iget-object v12, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v13, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v13, v11}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    :cond_6a
    add-int/lit8 v7, v7, 0x1

    goto :goto_56

    .line 296
    .end local v11           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_6d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    .line 297
    return-void
.end method

.method private updateChip(IIILjava/lang/String;Ljava/lang/Object;Z)V
    .registers 12
    .parameter "index"
    .parameter "iconLeft"
    .parameter "iconRight"
    .parameter "text"
    .parameter "tag"
    .parameter "isRestricted"

    .prologue
    const v2, 0x7f020059

    const/4 v4, 0x0

    .line 436
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getChipCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_1f

    .line 437
    const v1, 0x7f030079

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/AudienceView;->inflate(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    if-eqz v3, :cond_1a

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1a
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 440
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 441
    .local v0, chip:Landroid/widget/TextView;
    invoke-virtual {v0, p2, v4, p3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 442
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    if-eqz p6, :cond_42

    const v1, 0x7f02005e

    :goto_32
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 447
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_3e

    .line 448
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 451
    :cond_3e
    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 452
    return-void

    .line 443
    :cond_42
    instance-of v1, p5, Lcom/google/android/apps/plus/content/CircleData;

    if-eqz v1, :cond_54

    move-object v1, p5

    check-cast v1, Lcom/google/android/apps/plus/content/CircleData;

    :goto_49
    if-eqz v1, :cond_5a

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_5c

    move v1, v2

    goto :goto_32

    :cond_54
    const/4 v1, 0x0

    goto :goto_49

    :pswitch_56
    const v1, 0x7f02005d

    goto :goto_32

    :cond_5a
    move v1, v2

    goto :goto_32

    :pswitch_data_5c
    .packed-switch 0x7
        :pswitch_56
        :pswitch_56
        :pswitch_56
    .end packed-switch
.end method


# virtual methods
.method public final addCircle(Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 7
    .parameter "newCircle"

    .prologue
    .line 305
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .line 307
    .local v0, circles:[Lcom/google/android/apps/plus/content/CircleData;
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 317
    :goto_11
    return-void

    .line 311
    :cond_12
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 312
    .local v1, context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    .line 313
    .local v2, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v3, v4, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 315
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v4, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v4, p1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_11
.end method

.method public final addPerson(Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 7
    .parameter "newPerson"

    .prologue
    .line 325
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v2

    .line 327
    .local v2, users:[Lcom/google/android/apps/plus/content/PersonData;
    invoke-static {v2, p1}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 337
    :goto_11
    return-void

    .line 331
    :cond_12
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 332
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    .line 333
    .local v1, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 335
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v4, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v4, p1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_11
.end method

.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_11
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v6

    array-length v7, v6

    move v2, v1

    :goto_23
    if-ge v2, v7, :cond_2d

    aget-object v8, v6, v2

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_23

    :cond_2d
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v2

    array-length v6, v2

    move v0, v1

    :goto_33
    if-ge v0, v6, :cond_11

    aget-object v7, v2, v0

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_33

    :cond_3d
    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method protected getChipCount()I
    .registers 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    return v0
.end method

.method protected final inflate(I)Landroid/view/View;
    .registers 4
    .parameter "layoutResourceId"

    .prologue
    .line 475
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected init()V
    .registers 2

    .prologue
    .line 178
    const v0, 0x7f03000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AudienceView;->inflate(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AudienceView;->addView(Landroid/view/View;)V

    .line 180
    const v0, 0x7f090059

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    .line 181
    return-void
.end method

.method public final initLoaders(Landroid/support/v4/app/LoaderManager;)V
    .registers 4
    .parameter "loaders"

    .prologue
    .line 596
    const v0, 0x7f090026

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 597
    return-void
.end method

.method public final isEdited()Z
    .registers 2

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    .prologue
    .line 604
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    if-nez v3, :cond_5

    .line 617
    :cond_4
    :goto_4
    return-void

    .line 607
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 608
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    .line 609
    .local v2, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 611
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 612
    .local v1, index:I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_4

    .line 613
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    .line 614
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 615
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_4
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 524
    packed-switch p1, :pswitch_data_18

    .line 530
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 526
    :pswitch_9
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x5

    sget-object v4, Lcom/google/android/apps/plus/views/AudienceView$CirclesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    return-object v0

    .line 524
    :pswitch_data_18
    .packed-switch 0x7f090026
        :pswitch_9
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 13
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 42
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_a8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_11
    if-eqz p2, :cond_a6

    sget-boolean v0, Lcom/google/android/apps/plus/views/AudienceView;->$assertionsDisabled:Z

    if-nez v0, :cond_29

    sget-object v0, Lcom/google/android/apps/plus/views/AudienceView$CirclesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_29

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_29
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    :goto_35
    if-ge v2, v3, :cond_8c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v5

    if-ne v5, v9, :cond_82

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    move v0, v1

    :goto_4a
    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_7b

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7f

    new-instance v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v5

    invoke-direct {v0, v6, v7, v8, v5}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    new-instance v5, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7b
    :goto_7b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_35

    :cond_7f
    add-int/lit8 v0, v0, 0x1

    goto :goto_4a

    :cond_82
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v5

    if-ne v5, v9, :cond_7b

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7b

    :cond_8c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :goto_91
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_a3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_91

    :cond_a3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    :cond_a6
    return-void

    nop

    :pswitch_data_a8
    .packed-switch 0x7f090026
        :pswitch_11
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 590
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    .prologue
    .line 211
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;

    .line 212
    .local v0, ss:Lcom/google/android/apps/plus/views/AudienceView$SavedState;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->audience:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 217
    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->edited:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    .line 220
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    .prologue
    .line 197
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 198
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/AudienceView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 200
    .local v0, state:Lcom/google/android/apps/plus/views/AudienceView$SavedState;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->audience:Ljava/util/ArrayList;

    .line 201
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->edited:Z

    .line 203
    return-object v0
.end method

.method protected final removeLastChip()V
    .registers 3

    .prologue
    .line 458
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 465
    :goto_8
    return-void

    .line 461
    :cond_9
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    .line 462
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 463
    .local v0, lastIndex:I
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 464
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_8
.end method

.method public final removePerson(Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 7
    .parameter "person"

    .prologue
    const/4 v4, 0x1

    .line 345
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    .line 346
    const/4 v1, 0x0

    .line 347
    .local v1, chipToRemove:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    .line 348
    .local v0, chip:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v3

    if-ne v3, v4, :cond_a

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v3

    if-nez v3, :cond_a

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUser(I)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->isSamePerson(Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 350
    move-object v1, v0

    .line 355
    .end local v0           #chip:Lcom/google/android/apps/plus/content/AudienceData;
    :cond_2e
    if-eqz v1, :cond_38

    .line 356
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    .line 359
    :cond_38
    return-void
.end method

.method public final replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 3
    .parameter "audience"

    .prologue
    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    .line 254
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AudienceView;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 255
    return-void
.end method

.method public setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 2
    .parameter "account"

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 190
    return-void
.end method

.method public setAudienceChangedCallback(Ljava/lang/Runnable;)V
    .registers 2
    .parameter "callback"

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    .line 229
    return-void
.end method

.method public setDefaultAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 2
    .parameter "audience"

    .prologue
    .line 242
    if-eqz p1, :cond_5

    .line 243
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AudienceView;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 245
    :cond_5
    return-void
.end method

.method protected update()V
    .registers 25

    .prologue
    .line 365
    const/4 v3, 0x0

    .line 367
    .local v3, i:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_9
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_dd

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/content/AudienceData;

    .line 368
    .local v20, item:Lcom/google/android/apps/plus/content/AudienceData;
    sget-boolean v2, Lcom/google/android/apps/plus/views/AudienceView;->$assertionsDisabled:Z

    if-nez v2, :cond_2d

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_2d

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_2d

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 370
    :cond_2d
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v16

    .local v16, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v21, v0

    .local v21, len$:I
    const/16 v19, 0x0

    .local v19, i$:I
    :goto_38
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_8b

    aget-object v7, v16, v19

    .line 371
    .local v7, circle:Lcom/google/android/apps/plus/content/CircleData;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v4

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v8

    .line 373
    .local v8, isRestricted:Z
    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_73

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v6

    .line 375
    .local v6, text:Ljava/lang/String;
    :goto_5a
    add-int/lit8 v10, v3, 0x1

    .end local v3           #i:I
    .local v10, i:I
    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_12e

    const v4, 0x7f02008c

    :goto_66
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getCloseIcon()I

    move-result v5

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/AudienceView;->updateChip(IIILjava/lang/String;Ljava/lang/Object;Z)V

    .line 370
    add-int/lit8 v19, v19, 0x1

    move v3, v10

    .end local v10           #i:I
    .restart local v3       #i:I
    goto :goto_38

    .line 373
    .end local v6           #text:Ljava/lang/String;
    :cond_73
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f080096

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_5a

    .line 375
    .end local v3           #i:I
    .restart local v6       #text:Ljava/lang/String;
    .restart local v10       #i:I
    :pswitch_7f
    const v4, 0x7f02008f

    goto :goto_66

    :pswitch_83
    const v4, 0x7f02008e

    goto :goto_66

    :pswitch_87
    const v4, 0x7f02008d

    goto :goto_66

    .line 379
    .end local v6           #text:Ljava/lang/String;
    .end local v7           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v8           #isRestricted:Z
    .end local v10           #i:I
    .restart local v3       #i:I
    :cond_8b
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v16

    .local v16, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v21, v0

    const/16 v19, 0x0

    move v10, v3

    .end local v3           #i:I
    .restart local v10       #i:I
    :goto_97
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_da

    aget-object v14, v16, v19

    .line 380
    .local v14, person:Lcom/google/android/apps/plus/content/PersonData;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_bf

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v6

    .line 383
    .restart local v6       #text:Ljava/lang/String;
    :goto_ad
    add-int/lit8 v3, v10, 0x1

    .end local v10           #i:I
    .restart local v3       #i:I
    const/4 v11, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getCloseIcon()I

    move-result v12

    const/4 v15, 0x0

    move-object/from16 v9, p0

    move-object v13, v6

    invoke-direct/range {v9 .. v15}, Lcom/google/android/apps/plus/views/AudienceView;->updateChip(IIILjava/lang/String;Ljava/lang/Object;Z)V

    .line 379
    add-int/lit8 v19, v19, 0x1

    move v10, v3

    .end local v3           #i:I
    .restart local v10       #i:I
    goto :goto_97

    .line 380
    .end local v6           #text:Ljava/lang/String;
    :cond_bf
    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_ce

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v6

    goto :goto_ad

    :cond_ce
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x104000e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_ad

    .end local v14           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_da
    move v3, v10

    .line 379
    .end local v10           #i:I
    .restart local v3       #i:I
    goto/16 :goto_9

    .line 387
    .end local v16           #arr$:[Lcom/google/android/apps/plus/content/PersonData;
    .end local v19           #i$:I
    .end local v20           #item:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v21           #len$:I
    :cond_dd
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getChipCount()I

    move-result v17

    .line 388
    .local v17, chipCount:I
    :goto_e1
    move/from16 v0, v17

    if-ge v3, v0, :cond_f5

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 390
    add-int/lit8 v3, v3, 0x1

    goto :goto_e1

    .line 394
    :cond_f5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v23, v2, -0x1

    .local v23, x:I
    :goto_ff
    if-ltz v23, :cond_11f

    .line 395
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    .line 396
    .local v22, view:Landroid/view/View;
    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-ne v2, v4, :cond_11c

    .line 397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 394
    :cond_11c
    add-int/lit8 v23, v23, -0x1

    goto :goto_ff

    .line 401
    .end local v22           #view:Landroid/view/View;
    :cond_11f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    if-eqz v2, :cond_12c

    .line 402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 404
    :cond_12c
    return-void

    .line 375
    nop

    :pswitch_data_12e
    .packed-switch 0x7
        :pswitch_83
        :pswitch_87
        :pswitch_7f
    .end packed-switch
.end method
