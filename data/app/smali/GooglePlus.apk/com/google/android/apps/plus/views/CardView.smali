.class public abstract Lcom/google/android/apps/plus/views/CardView;
.super Landroid/view/View;
.source "CardView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field protected static sBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field protected static sBottomBorderPadding:I

.field private static sCardViewInitialized:Z

.field protected static sDefaultTextPaint:Landroid/text/TextPaint;

.field protected static sDrawRect:Landroid/graphics/Rect;

.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field protected static sLeftBorderPadding:I

.field protected static sPressedStateBackground:Landroid/graphics/drawable/Drawable;

.field protected static final sResizePaint:Landroid/graphics/Paint;

.field protected static sRightBorderPadding:I

.field protected static sTopBorderPadding:I

.field protected static sXDoublePadding:I

.field protected static sXPadding:I

.field protected static sYDoublePadding:I

.field protected static sYPadding:I


# instance fields
.field private final mClickableItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field protected mDisplaySizeType:I

.field protected mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field protected mPaddingEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 62
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/CardView;->sResizePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v3, 0x7f0d0131

    const/4 v2, 0x1

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    const/4 v1, -0x2

    iput v1, p0, Lcom/google/android/apps/plus/views/CardView;->mDisplaySizeType:I

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    .line 71
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    .line 97
    sget-boolean v1, Lcom/google/android/apps/plus/views/CardView;->sCardViewInitialized:Z

    if-nez v1, :cond_b1

    .line 98
    sput-boolean v2, Lcom/google/android/apps/plus/views/CardView;->sCardViewInitialized:Z

    .line 100
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 102
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 104
    .local v0, res:Landroid/content/res/Resources;
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 105
    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 106
    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00c4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 107
    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 108
    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00c8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 109
    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 112
    const v1, 0x7f020029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    .line 113
    const v1, 0x7f02017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    .line 115
    const v1, 0x7f0d0129

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    .line 116
    const v1, 0x7f0d012a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sRightBorderPadding:I

    .line 117
    const v1, 0x7f0d012b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    .line 118
    const v1, 0x7f0d012c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sBottomBorderPadding:I

    .line 119
    const v1, 0x7f0d012d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 120
    sput v1, Lcom/google/android/apps/plus/views/CardView;->sXPadding:I

    mul-int/lit8 v1, v1, 0x2

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sXDoublePadding:I

    .line 121
    const v1, 0x7f0d012e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 122
    sput v1, Lcom/google/android/apps/plus/views/CardView;->sYPadding:I

    mul-int/lit8 v1, v1, 0x2

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sYDoublePadding:I

    .line 123
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sDrawRect:Landroid/graphics/Rect;

    .line 125
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_b1
    return-void
.end method

.method protected static shouldWrapContent(I)Z
    .registers 2
    .parameter "heightMeasureSpec"

    .prologue
    .line 74
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method public final addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .registers 3
    .parameter "item"

    .prologue
    .line 259
    if-nez p1, :cond_3

    .line 264
    :goto_2
    return-void

    .line 262
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public final addClickableItemAtStart(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .registers 4
    .parameter "item"

    .prologue
    .line 246
    if-nez p1, :cond_3

    .line 251
    :goto_2
    return-void

    .line 249
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "event"

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 276
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v3, v7

    .line 277
    .local v3, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v4, v7

    .line 279
    .local v4, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_78

    move v5, v6

    .line 322
    :cond_15
    :goto_15
    return v5

    .line 281
    :pswitch_16
    iget-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    .local v1, i:I
    :goto_1e
    if-ltz v1, :cond_15

    .line 282
    iget-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 283
    .local v2, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v2, v3, v4, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v7

    if-eqz v7, :cond_34

    .line 284
    iput-object v2, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    goto :goto_15

    .line 281
    :cond_34
    add-int/lit8 v1, v1, -0x1

    goto :goto_1e

    .line 293
    .end local v1           #i:I
    .end local v2           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :pswitch_37
    iput-object v8, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 294
    const/4 v0, 0x0

    .line 295
    .local v0, handled:Z
    iget-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    .restart local v1       #i:I
    :goto_42
    if-ltz v1, :cond_54

    .line 296
    iget-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 297
    .restart local v2       #item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v7

    or-int/2addr v0, v7

    .line 295
    add-int/lit8 v1, v1, -0x1

    goto :goto_42

    .line 299
    .end local v2           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    .line 300
    if-nez v0, :cond_62

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v5, :cond_62

    .line 301
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v5, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_62
    move v5, v6

    .line 303
    goto :goto_15

    .line 307
    .end local v0           #handled:Z
    .end local v1           #i:I
    :pswitch_64
    iget-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v7, :cond_74

    .line 308
    iget-object v6, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v7, 0x3

    invoke-interface {v6, v3, v4, v7}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 309
    iput-object v8, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 310
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    goto :goto_15

    :cond_74
    move v5, v6

    .line 313
    goto :goto_15

    :pswitch_76
    move v5, v6

    .line 318
    goto :goto_15

    .line 279
    :pswitch_data_78
    .packed-switch 0x0
        :pswitch_16
        :pswitch_37
        :pswitch_76
        :pswitch_64
    .end packed-switch
.end method

.method protected abstract draw(Landroid/graphics/Canvas;IIII)I
.end method

.method public init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V
    .registers 9
    .parameter "cursor"
    .parameter "displaySizeType"
    .parameter "size"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewedListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"

    .prologue
    .line 135
    iput p2, p0, Lcom/google/android/apps/plus/views/CardView;->mDisplaySizeType:I

    .line 136
    iput-object p4, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 137
    iput-object p5, p0, Lcom/google/android/apps/plus/views/CardView;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 138
    return-void
.end method

.method protected abstract layoutElements(IIII)I
.end method

.method protected onAttachedToWindow()V
    .registers 1

    .prologue
    .line 215
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->onStart()V

    .line 217
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 2
    .parameter "gaiaId"

    .prologue
    .line 230
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    .prologue
    .line 221
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->onStop()V

    .line 223
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 14
    .parameter "canvas"

    .prologue
    const/4 v1, 0x0

    .line 188
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->getWidth()I

    move-result v7

    .line 191
    .local v7, width:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->getHeight()I

    move-result v6

    .line 193
    .local v6, height:I
    const/4 v9, 0x0

    .line 194
    .local v9, xPadding:I
    const/4 v11, 0x0

    .line 195
    .local v11, yPadding:I
    const/4 v8, 0x0

    .line 196
    .local v8, xDoublePadding:I
    const/4 v10, 0x0

    .line 198
    .local v10, yDoublePadding:I
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    if-eqz v0, :cond_1c

    .line 199
    sget v9, Lcom/google/android/apps/plus/views/CardView;->sXPadding:I

    .line 200
    sget v11, Lcom/google/android/apps/plus/views/CardView;->sYPadding:I

    .line 201
    sget v8, Lcom/google/android/apps/plus/views/CardView;->sXDoublePadding:I

    .line 202
    sget v10, Lcom/google/android/apps/plus/views/CardView;->sYDoublePadding:I

    .line 205
    :cond_1c
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, v1, v1, v7, v6}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 206
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 208
    sget v0, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int v2, v0, v9

    sget v0, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int v3, v0, v11

    sget v0, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int/2addr v0, v8

    sget v1, Lcom/google/android/apps/plus/views/CardView;->sRightBorderPadding:I

    add-int/2addr v0, v1

    sub-int v4, v7, v0

    sget v0, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int/2addr v0, v10

    sget v1, Lcom/google/android/apps/plus/views/CardView;->sBottomBorderPadding:I

    add-int/2addr v0, v1

    sub-int v5, v6, v0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/CardView;->draw(Landroid/graphics/Canvas;IIII)I

    .line 211
    return-void
.end method

.method protected onMeasure(II)V
    .registers 15
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 163
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 164
    .local v2, widthDimension:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 165
    .local v1, heightDimensionArg:I
    if-gtz v1, :cond_38

    move v0, v2

    .line 167
    .local v0, heightDimension:I
    :goto_b
    const/4 v4, 0x0

    .line 168
    .local v4, xPadding:I
    const/4 v6, 0x0

    .line 169
    .local v6, yPadding:I
    const/4 v3, 0x0

    .line 170
    .local v3, xDoublePadding:I
    const/4 v5, 0x0

    .line 172
    .local v5, yDoublePadding:I
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    if-eqz v7, :cond_1b

    .line 173
    sget v4, Lcom/google/android/apps/plus/views/CardView;->sXPadding:I

    .line 174
    sget v6, Lcom/google/android/apps/plus/views/CardView;->sYPadding:I

    .line 175
    sget v3, Lcom/google/android/apps/plus/views/CardView;->sXDoublePadding:I

    .line 176
    sget v5, Lcom/google/android/apps/plus/views/CardView;->sYDoublePadding:I

    .line 179
    :cond_1b
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/views/CardView;->setMeasuredDimension(II)V

    .line 181
    sget v7, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int/2addr v7, v4

    sget v8, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int/2addr v8, v6

    sget v9, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int/2addr v9, v3

    sget v10, Lcom/google/android/apps/plus/views/CardView;->sRightBorderPadding:I

    add-int/2addr v9, v10

    sub-int v9, v2, v9

    sget v10, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int/2addr v10, v5

    sget v11, Lcom/google/android/apps/plus/views/CardView;->sBottomBorderPadding:I

    add-int/2addr v10, v11

    sub-int v10, v0, v10

    invoke-virtual {p0, v7, v8, v9, v10}, Lcom/google/android/apps/plus/views/CardView;->layoutElements(IIII)I

    .line 184
    return-void

    .end local v0           #heightDimension:I
    .end local v3           #xDoublePadding:I
    .end local v4           #xPadding:I
    .end local v5           #yDoublePadding:I
    .end local v6           #yPadding:I
    :cond_38
    move v0, v1

    .line 165
    goto :goto_b
.end method

.method public onMediaImageChanged(Ljava/lang/String;)V
    .registers 2
    .parameter "url"

    .prologue
    .line 234
    return-void
.end method

.method public onRecycle()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 155
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 156
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 157
    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->clearAnimation()V

    .line 159
    return-void
.end method

.method public onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter "request"
    .parameter "bitmap"

    .prologue
    .line 238
    return-void
.end method

.method public onStart()V
    .registers 2

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 142
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 143
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V

    .line 144
    return-void
.end method

.method public onStop()V
    .registers 2

    .prologue
    .line 147
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 148
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 149
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V

    .line 150
    return-void
.end method

.method public final removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .registers 3
    .parameter "item"

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 272
    return-void
.end method

.method public setPaddingEnabled(Z)V
    .registers 2
    .parameter "enablePadding"

    .prologue
    .line 128
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    .line 129
    return-void
.end method
