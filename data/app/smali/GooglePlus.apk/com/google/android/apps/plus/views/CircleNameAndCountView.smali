.class public Lcom/google/android/apps/plus/views/CircleNameAndCountView;
.super Landroid/view/ViewGroup;
.source "CircleNameAndCountView.java"


# instance fields
.field private mCountTextView:Landroid/view/View;

.field private mIconView:Landroid/view/View;

.field private mNameTextView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .registers 2

    .prologue
    .line 35
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 36
    const v0, 0x1020014

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    .line 37
    const v0, 0x1020015

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    .line 38
    const v0, 0x1020006

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    .line 39
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 15
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 114
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 115
    .local v1, nameWidth:I
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 116
    .local v0, nameHeight:I
    sub-int v4, p5, p3

    sub-int/2addr v4, v0

    div-int/lit8 v3, v4, 0x2

    .line 117
    .local v3, textTop:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingLeft()I

    move-result v2

    .line 118
    .local v2, textLeft:I
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    add-int v5, v2, v1

    add-int v6, v3, v0

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 119
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3d

    .line 120
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    add-int v5, v2, v1

    add-int v6, v2, v1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 124
    :cond_3d
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_59

    .line 125
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    sub-int v5, p4, v5

    const/4 v6, 0x0

    sub-int v7, p4, p2

    sub-int v8, p5, p3

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 127
    :cond_59
    return-void
.end method

.method protected onMeasure(II)V
    .registers 20
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 43
    const/4 v14, 0x0

    move/from16 v0, p1

    invoke-static {v14, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->resolveSize(II)I

    move-result v13

    .line 44
    .local v13, width:I
    const/4 v14, 0x0

    move/from16 v0, p2

    invoke-static {v14, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->resolveSize(II)I

    move-result v4

    .line 46
    .local v4, height:I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_b7

    const/4 v2, 0x1

    .line 47
    .local v2, countVisible:Z
    :goto_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_ba

    const/4 v5, 0x1

    .line 49
    .local v5, iconVisible:Z
    :goto_24
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    .line 51
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 52
    .local v8, nameWidth:I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 54
    .local v7, nameHeight:I
    const/4 v3, 0x0

    .line 55
    .local v3, countWidth:I
    const/4 v1, 0x0

    .line 56
    .local v1, countHeight:I
    if-eqz v2, :cond_5c

    .line 57
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    .line 58
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 59
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 62
    :cond_5c
    const/4 v6, 0x0

    .line 63
    .local v6, iconWidth:I
    if-eqz v5, :cond_69

    .line 64
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    iget v6, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 67
    :cond_69
    add-int v12, v8, v3

    .line 68
    .local v12, totalTextWidth:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingLeft()I

    move-result v9

    .line 69
    .local v9, paddingLeft:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingRight()I

    move-result v10

    .line 70
    .local v10, paddingRight:I
    add-int v14, v12, v9

    add-int v11, v14, v10

    .line 72
    .local v11, textWidthWithPadding:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    sparse-switch v14, :sswitch_data_e8

    .line 89
    :cond_7e
    :goto_7e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    const/high16 v15, 0x4000

    invoke-static {v8, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    const/high16 v16, 0x4000

    move/from16 v0, v16

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    .line 93
    if-eqz v2, :cond_aa

    .line 94
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    const/high16 v15, 0x4000

    invoke-static {v3, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    const/high16 v16, 0x4000

    move/from16 v0, v16

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    .line 99
    :cond_aa
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    sparse-switch v14, :sswitch_data_f6

    .line 103
    :goto_b1
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v4}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->setMeasuredDimension(II)V

    .line 110
    return-void

    .line 46
    .end local v1           #countHeight:I
    .end local v2           #countVisible:Z
    .end local v3           #countWidth:I
    .end local v5           #iconVisible:Z
    .end local v6           #iconWidth:I
    .end local v7           #nameHeight:I
    .end local v8           #nameWidth:I
    .end local v9           #paddingLeft:I
    .end local v10           #paddingRight:I
    .end local v11           #textWidthWithPadding:I
    .end local v12           #totalTextWidth:I
    :cond_b7
    const/4 v2, 0x0

    goto/16 :goto_19

    .line 47
    .restart local v2       #countVisible:Z
    :cond_ba
    const/4 v5, 0x0

    goto/16 :goto_24

    .line 74
    .restart local v1       #countHeight:I
    .restart local v3       #countWidth:I
    .restart local v5       #iconVisible:Z
    .restart local v6       #iconWidth:I
    .restart local v7       #nameHeight:I
    .restart local v8       #nameWidth:I
    .restart local v9       #paddingLeft:I
    .restart local v10       #paddingRight:I
    .restart local v11       #textWidthWithPadding:I
    .restart local v12       #totalTextWidth:I
    :sswitch_bd
    add-int v13, v11, v6

    .line 75
    goto :goto_7e

    .line 78
    :sswitch_c0
    if-eqz v13, :cond_c6

    add-int v14, v11, v6

    if-ge v14, v13, :cond_7e

    .line 79
    :cond_c6
    add-int v13, v11, v6

    goto :goto_7e

    .line 84
    :sswitch_c9
    sub-int v14, v13, v9

    sub-int/2addr v14, v10

    sub-int/2addr v14, v3

    sub-int/2addr v14, v6

    const/4 v15, 0x0

    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-static {v8, v14}, Ljava/lang/Math;->min(II)I

    move-result v8

    goto :goto_7e

    .line 102
    :sswitch_d8
    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingTop()I

    move-result v15

    add-int/2addr v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingBottom()I

    move-result v15

    add-int v4, v14, v15

    goto :goto_b1

    .line 72
    :sswitch_data_e8
    .sparse-switch
        -0x80000000 -> :sswitch_c0
        0x0 -> :sswitch_bd
        0x40000000 -> :sswitch_c9
    .end sparse-switch

    .line 99
    :sswitch_data_f6
    .sparse-switch
        -0x80000000 -> :sswitch_d8
        0x0 -> :sswitch_d8
    .end sparse-switch
.end method
