.class public final Lcom/google/android/apps/plus/views/ClickableButton;
.super Ljava/lang/Object;
.source "ClickableButton.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    }
.end annotation


# static fields
.field private static sBitmapTextXSpacing:I

.field private static sClickableButtonInitialized:Z

.field private static sTextXPadding:I


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mClicked:Z

.field private mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field private mContentDescription:Ljava/lang/CharSequence;

.field private mContext:Landroid/content/Context;

.field private mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field private mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

.field private mRect:Landroid/graphics/Rect;

.field private mTextLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V
    .registers 20
    .parameter "context"
    .parameter "bitmap"
    .parameter "defaultBackground"
    .parameter "clickedBackground"
    .parameter "listener"
    .parameter "x"
    .parameter "y"
    .parameter "contentDescription"

    .prologue
    .line 66
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V
    .registers 21
    .parameter "context"
    .parameter "bitmap"
    .parameter "text"
    .parameter "textPaint"
    .parameter "defaultBackground"
    .parameter "clickedBackground"
    .parameter "listener"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 105
    const/4 v2, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v8, p8

    move/from16 v9, p9

    move-object v10, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V
    .registers 28
    .parameter "context"
    .parameter "bitmap"
    .parameter "text"
    .parameter "textPaint"
    .parameter "defaultBackground"
    .parameter "clickedBackground"
    .parameter "listener"
    .parameter "x"
    .parameter "y"
    .parameter "contentDescription"

    .prologue
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 126
    sget-boolean v2, Lcom/google/android/apps/plus/views/ClickableButton;->sClickableButtonInitialized:Z

    if-nez v2, :cond_22

    .line 127
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/views/ClickableButton;->sClickableButtonInitialized:Z

    .line 129
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 130
    .local v15, res:Landroid/content/res/Resources;
    const v2, 0x7f0d0020

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/ClickableButton;->sTextXPadding:I

    .line 131
    const v2, 0x7f0d0021

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/ClickableButton;->sBitmapTextXSpacing:I

    .line 135
    .end local v15           #res:Landroid/content/res/Resources;
    :cond_22
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mContext:Landroid/content/Context;

    .line 136
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    .line 137
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    .line 138
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    .line 139
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    .line 140
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mContentDescription:Ljava/lang/CharSequence;

    .line 142
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/drawable/NinePatchDrawable;->getMinimumWidth()I

    move-result v12

    .line 143
    .local v12, bgWidth:I
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/drawable/NinePatchDrawable;->getMinimumHeight()I

    move-result v11

    .line 144
    .local v11, bgHeight:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_8c

    if-eqz p3, :cond_8c

    sget v10, Lcom/google/android/apps/plus/views/ClickableButton;->sBitmapTextXSpacing:I

    .line 147
    .local v10, additionalXSpacing:I
    :goto_58
    if-nez p3, :cond_8e

    .line 148
    const/16 v16, 0x0

    .local v16, textHeight:I
    move/from16 v5, v16

    .line 156
    .local v5, textWidth:I
    :goto_5e
    if-nez p2, :cond_b5

    const/4 v14, 0x0

    .line 157
    .local v14, bitmapWidth:I
    :goto_61
    if-nez p2, :cond_ba

    const/4 v13, 0x0

    .line 159
    .local v13, bitmapHeight:I
    :goto_64
    new-instance v2, Landroid/graphics/Rect;

    add-int v3, v5, v14

    add-int/2addr v3, v10

    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int v3, v3, p8

    sget v4, Lcom/google/android/apps/plus/views/ClickableButton;->sTextXPadding:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v11, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int v4, v4, p9

    move/from16 v0, p8

    move/from16 v1, p9

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    .line 161
    return-void

    .line 144
    .end local v5           #textWidth:I
    .end local v10           #additionalXSpacing:I
    .end local v13           #bitmapHeight:I
    .end local v14           #bitmapWidth:I
    .end local v16           #textHeight:I
    :cond_8c
    const/4 v10, 0x0

    goto :goto_58

    .line 150
    .restart local v10       #additionalXSpacing:I
    :cond_8e
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v5, v2

    .line 151
    .restart local v5       #textWidth:I
    new-instance v2, Landroid/text/StaticLayout;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f80

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v16

    .restart local v16       #textHeight:I
    goto :goto_5e

    .line 156
    :cond_b5
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    goto :goto_61

    .line 157
    .restart local v14       #bitmapWidth:I
    :cond_ba
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    goto :goto_64
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V
    .registers 20
    .parameter "context"
    .parameter "text"
    .parameter "textPaint"
    .parameter "defaultBackground"
    .parameter "clickedBackground"
    .parameter "listener"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 85
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    .line 87
    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p2
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final createAbsoluteCoordinatesCopy(II)Lcom/google/android/apps/plus/views/ClickableButton;
    .registers 14
    .parameter "viewX"
    .parameter "viewY"

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_21

    .line 261
    const/4 v3, 0x0

    .line 262
    .local v3, text:Ljava/lang/String;
    const/4 v4, 0x0

    .line 268
    .local v4, textPaint:Landroid/text/TextPaint;
    :goto_6
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, p1

    iget-object v9, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, p2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mContentDescription:Ljava/lang/CharSequence;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    return-object v0

    .line 264
    .end local v3           #text:Ljava/lang/String;
    .end local v4           #textPaint:Landroid/text/TextPaint;
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 265
    .restart local v3       #text:Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    .restart local v4       #textPaint:Landroid/text/TextPaint;
    goto :goto_6
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .registers 12
    .parameter "canvas"

    .prologue
    const/4 v5, 0x0

    .line 169
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    if-eqz v6, :cond_7f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    .line 170
    .local v0, background:Landroid/graphics/drawable/NinePatchDrawable;
    :goto_7
    if-eqz v0, :cond_11

    .line 171
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 172
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 175
    :cond_11
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v6, :cond_82

    move v1, v5

    .line 176
    .local v1, bitmapWidth:I
    :goto_16
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-nez v6, :cond_89

    move v2, v5

    .line 178
    .local v2, textWidth:I
    :goto_1b
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    sub-int/2addr v7, v1

    sub-int/2addr v7, v2

    div-int/lit8 v7, v7, 0x2

    add-int v3, v6, v7

    .line 180
    .local v3, x:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_54

    .line 181
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 182
    .local v4, y:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    int-to-float v7, v3

    int-to-float v8, v4

    const/4 v9, 0x0

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 183
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_52

    sget v5, Lcom/google/android/apps/plus/views/ClickableButton;->sBitmapTextXSpacing:I

    :cond_52
    add-int/2addr v5, v1

    add-int/2addr v3, v5

    .line 186
    .end local v4           #y:I
    :cond_54
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-eqz v5, :cond_7e

    .line 187
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    add-int v4, v5, v6

    .line 188
    .restart local v4       #y:I
    int-to-float v5, v3

    int-to-float v6, v4

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 189
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 190
    neg-int v5, v3

    int-to-float v5, v5

    neg-int v6, v4

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 192
    .end local v4           #y:I
    :cond_7e
    return-void

    .line 169
    .end local v0           #background:Landroid/graphics/drawable/NinePatchDrawable;
    .end local v1           #bitmapWidth:I
    .end local v2           #textWidth:I
    .end local v3           #x:I
    :cond_7f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    goto :goto_7

    .line 175
    .restart local v0       #background:Landroid/graphics/drawable/NinePatchDrawable;
    :cond_82
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto :goto_16

    .line 176
    .restart local v1       #bitmapWidth:I
    :cond_89
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    goto :goto_1b
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .registers 7
    .parameter "x"
    .parameter "y"
    .parameter "event"

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    if-nez v2, :cond_7

    .line 234
    :cond_6
    :goto_6
    return v0

    .line 211
    :cond_7
    const/4 v2, 0x3

    if-ne p3, v2, :cond_e

    .line 212
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    move v0, v1

    .line 213
    goto :goto_6

    .line 216
    :cond_e
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 217
    if-ne p3, v1, :cond_6

    .line 218
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    goto :goto_6

    .line 223
    :cond_1b
    packed-switch p3, :pswitch_data_30

    :goto_1e
    move v0, v1

    .line 234
    goto :goto_6

    .line 225
    :pswitch_20
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    goto :goto_1e

    .line 230
    :pswitch_23
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    if-eqz v2, :cond_2c

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;->onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V

    .line 233
    :cond_2c
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    goto :goto_1e

    .line 223
    nop

    :pswitch_data_30
    .packed-switch 0x0
        :pswitch_20
        :pswitch_23
    .end packed-switch
.end method

.method public final setListener(Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    .line 255
    return-void
.end method
