.class public final Lcom/google/android/apps/plus/views/ClickableRect;
.super Ljava/lang/Object;
.source "ClickableRect.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;
    }
.end annotation


# instance fields
.field private mClicked:Z

.field private mContentDescription:Ljava/lang/CharSequence;

.field private mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

.field private mRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V
    .registers 10
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "listener"
    .parameter "contentDescription"

    .prologue
    .line 30
    new-instance v0, Landroid/graphics/Rect;

    add-int v1, p1, p3

    add-int v2, p2, p4

    invoke-direct {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {p0, v0, p5, p6}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(Landroid/graphics/Rect;Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/graphics/Rect;Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "rect"
    .parameter "listener"
    .parameter "contentDescription"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    .line 37
    iput-object p3, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mContentDescription:Ljava/lang/CharSequence;

    .line 38
    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 12
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p2
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .registers 7
    .parameter "x"
    .parameter "y"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 42
    const/4 v2, 0x3

    if-ne p3, v2, :cond_8

    .line 43
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    .line 65
    :goto_7
    return v0

    .line 47
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_16

    .line 48
    if-ne p3, v0, :cond_14

    .line 49
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    :cond_14
    move v0, v1

    .line 51
    goto :goto_7

    .line 54
    :cond_16
    packed-switch p3, :pswitch_data_2e

    goto :goto_7

    .line 56
    :pswitch_1a
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    goto :goto_7

    .line 61
    :pswitch_1d
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    if-eqz v2, :cond_2a

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;->onClickableRectClick$598f98c1()V

    .line 64
    :cond_2a
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    goto :goto_7

    .line 54
    nop

    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_1d
    .end packed-switch
.end method
