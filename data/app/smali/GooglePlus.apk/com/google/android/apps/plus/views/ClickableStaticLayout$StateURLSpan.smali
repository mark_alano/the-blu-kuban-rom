.class public final Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
.super Landroid/text/style/URLSpan;
.source "ClickableStaticLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ClickableStaticLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StateURLSpan"
.end annotation


# instance fields
.field private mBgColor:I

.field private mClicked:Z

.field private mFirstTime:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mFirstTime:Z

    .line 57
    return-void
.end method


# virtual methods
.method public final setClicked(Z)V
    .registers 2
    .parameter "clicked"

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mClicked:Z

    .line 92
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .registers 5
    .parameter "ds"

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mFirstTime:Z

    if-eqz v0, :cond_b

    .line 65
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mFirstTime:Z

    .line 67
    iget v0, p1, Landroid/text/TextPaint;->bgColor:I

    iput v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mBgColor:I

    .line 70
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mClicked:Z

    if-eqz v0, :cond_28

    .line 71
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_23

    .line 75
    const v0, -0xcc4a1b

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    .line 83
    :goto_1a
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 84
    invoke-virtual {p1, v2}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 85
    return-void

    .line 77
    :cond_23
    const/16 v0, -0x8000

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    goto :goto_1a

    .line 80
    :cond_28
    iget v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mBgColor:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    goto :goto_1a
.end method
