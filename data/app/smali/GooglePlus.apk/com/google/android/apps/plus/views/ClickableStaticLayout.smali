.class public final Lcom/google/android/apps/plus/views/ClickableStaticLayout;
.super Lcom/google/android/apps/plus/views/PositionedStaticLayout;
.source "ClickableStaticLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;,
        Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;
    }
.end annotation


# instance fields
.field private final mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

.field private mContentDescription:Ljava/lang/CharSequence;

.field private final mSpannedText:Landroid/text/Spanned;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V
    .registers 18
    .parameter "source"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "clickListener"

    .prologue
    .line 98
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 100
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 101
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentDescription:Ljava/lang/CharSequence;

    .line 103
    instance-of v1, p1, Landroid/text/Spanned;

    if-eqz v1, :cond_1a

    .line 104
    check-cast p1, Landroid/text/Spanned;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    .line 108
    :goto_19
    return-void

    .line 106
    .restart local p1
    :cond_1a
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    goto :goto_19
.end method

.method public static buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .registers 9
    .parameter "html"

    .prologue
    const/4 v1, 0x0

    .line 239
    if-nez p0, :cond_9

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    :goto_8
    return-object v0

    :cond_9
    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v3, Landroid/text/style/URLSpan;

    invoke-virtual {v2, v1, v0, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    :goto_1e
    array-length v3, v0

    if-ge v1, v3, :cond_41

    aget-object v3, v0, v1

    new-instance v4, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1e

    :cond_41
    move-object v0, v2

    goto :goto_8
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p2
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .registers 13
    .parameter "x"
    .parameter "y"
    .parameter "event"

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 123
    const/4 v4, 0x3

    if-ne p3, v4, :cond_13

    .line 124
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    if-eqz v4, :cond_12

    .line 125
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    .line 126
    iput-object v7, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    .line 179
    :cond_12
    :goto_12
    return v2

    .line 131
    :cond_13
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    if-nez v4, :cond_19

    move v2, v3

    .line 132
    goto :goto_12

    .line 135
    :cond_19
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_30

    .line 136
    if-ne p3, v2, :cond_2e

    .line 137
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    if-eqz v2, :cond_2e

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    .line 139
    iput-object v7, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    :cond_2e
    move v2, v3

    .line 142
    goto :goto_12

    .line 145
    :cond_30
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int v4, p1, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int v5, p2, v5

    int-to-float v5, v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-static {v6, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLineForVertical(I)I

    move-result v5

    invoke-static {v8, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-static {v6, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-virtual {p0, v5, v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getOffsetForHorizontal(IF)I

    move-result v0

    .line 146
    .local v0, offset:I
    if-gez v0, :cond_69

    move v2, v3

    .line 147
    goto :goto_12

    .line 150
    :cond_69
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    const-class v5, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-interface {v4, v0, v0, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    .line 151
    .local v1, spans:[Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    array-length v4, v1

    if-nez v4, :cond_78

    move v2, v3

    .line 152
    goto :goto_12

    .line 155
    :cond_78
    packed-switch p3, :pswitch_data_a4

    goto :goto_12

    .line 157
    :pswitch_7c
    aget-object v3, v1, v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    .line 158
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    goto :goto_12

    .line 163
    :pswitch_86
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    aget-object v5, v1, v3

    if-ne v4, v5, :cond_97

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    if-eqz v4, :cond_97

    .line 164
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    aget-object v5, v1, v3

    invoke-interface {v4, v5}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;->onSpanClick(Landroid/text/style/URLSpan;)V

    .line 167
    :cond_97
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    if-eqz v4, :cond_12

    .line 168
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    .line 169
    iput-object v7, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    goto/16 :goto_12

    .line 155
    :pswitch_data_a4
    .packed-switch 0x0
        :pswitch_7c
        :pswitch_86
    .end packed-switch
.end method
