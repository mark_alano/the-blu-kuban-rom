.class final Lcom/google/android/apps/plus/views/ClickableUserImage;
.super Ljava/lang/Object;
.source "ClickableUserImage.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;
    }
.end annotation


# static fields
.field private static sImageSelectedPaint:Landroid/graphics/Paint;


# instance fields
.field private final mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private mAvatarInvalidated:Z

.field private mAvatarLoaded:Z

.field private mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mClickListener:Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;

.field private mClicked:Z

.field private mContentDescription:Ljava/lang/CharSequence;

.field private final mContentRect:Landroid/graphics/Rect;

.field private final mUserId:Ljava/lang/String;

.field private final mUserName:Ljava/lang/String;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;)V
    .registers 13
    .parameter "view"
    .parameter "gaiaId"
    .parameter "avatarUrl"
    .parameter "userName"
    .parameter "clickListener"

    .prologue
    .line 67
    const/4 v3, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V
    .registers 11
    .parameter "view"
    .parameter "gaiaId"
    .parameter "avatarUrl"
    .parameter "userName"
    .parameter "clickListener"
    .parameter "avatarSize"

    .prologue
    const/4 v3, 0x1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mView:Landroid/view/View;

    .line 83
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 84
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentRect:Landroid/graphics/Rect;

    .line 85
    iput-object p5, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClickListener:Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;

    .line 86
    iput-object p2, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mUserId:Ljava/lang/String;

    .line 87
    iput-object p4, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mUserName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentDescription:Ljava/lang/CharSequence;

    .line 89
    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 90
    new-instance v1, Lcom/google/android/apps/plus/content/AvatarRequest;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mUserId:Ljava/lang/String;

    invoke-direct {v1, v2, p3, p6, v3}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 91
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarInvalidated:Z

    .line 93
    sget-object v1, Lcom/google/android/apps/plus/views/ClickableUserImage;->sImageSelectedPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_5a

    .line 94
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 95
    sput-object v1, Lcom/google/android/apps/plus/views/ClickableUserImage;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 96
    sget-object v1, Lcom/google/android/apps/plus/views/ClickableUserImage;->sImageSelectedPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x4080

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 97
    sget-object v1, Lcom/google/android/apps/plus/views/ClickableUserImage;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    sget-object v1, Lcom/google/android/apps/plus/views/ClickableUserImage;->sImageSelectedPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    :cond_5a
    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p1
    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    .end local p2
    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final drawSelectionRect(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/plus/views/ClickableUserImage;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 162
    return-void
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarInvalidated:Z

    if-eqz v0, :cond_e

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarInvalidated:Z

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/ImageCache;->refreshImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 129
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .registers 9
    .parameter "x"
    .parameter "y"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 177
    const/4 v2, 0x3

    if-ne p3, v2, :cond_8

    .line 178
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClicked:Z

    .line 201
    :goto_7
    return v0

    .line 182
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_16

    .line 183
    if-ne p3, v0, :cond_14

    .line 184
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClicked:Z

    :cond_14
    move v0, v1

    .line 186
    goto :goto_7

    .line 189
    :cond_16
    packed-switch p3, :pswitch_data_32

    goto :goto_7

    .line 191
    :pswitch_1a
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClicked:Z

    goto :goto_7

    .line 196
    :pswitch_1d
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClicked:Z

    if-eqz v2, :cond_2e

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClickListener:Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;

    if-eqz v2, :cond_2e

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClickListener:Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mUserId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mUserName:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;->onUserImageClick(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_2e
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClicked:Z

    goto :goto_7

    .line 189
    nop

    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_1d
    .end packed-switch
.end method

.method public final isClicked()Z
    .registers 2

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mClicked:Z

    return v0
.end method

.method public final onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mUserId:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarInvalidated:Z

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarLoaded:Z

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 221
    :cond_13
    return-void
.end method

.method public final setBitmap(Landroid/graphics/Bitmap;Z)V
    .registers 4
    .parameter "bitmap"
    .parameter "loading"

    .prologue
    .line 108
    if-nez p2, :cond_d

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mAvatarLoaded:Z

    .line 109
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 111
    return-void

    .line 108
    :cond_d
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final setRect(IIII)V
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableUserImage;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 142
    return-void
.end method
