.class public Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;
.super Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;
.source "EventDetailOptionRowTime.java"


# static fields
.field private static sClockIconDrawabale:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mClockIcon:Landroid/widget/ImageView;

.field private sInitialized:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public final bind$3ba8bae(Lcom/google/api/services/plusi/model/PlusEvent;)V
    .registers 7
    .parameter "event"

    .prologue
    .line 53
    const/4 v2, 0x0

    .line 54
    .local v2, formattedStartTime:Ljava/lang/String;
    const/4 v1, 0x0

    .line 56
    .local v1, formattedEndTime:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    .local v0, context:Landroid/content/Context;
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_17

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-eqz v3, :cond_17

    .line 59
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;

    move-result-object v2

    .line 63
    :cond_17
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_28

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-eqz v3, :cond_28

    .line 64
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;

    move-result-object v1

    .line 68
    :cond_28
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->mClockIcon:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-super {p0, v2, v1, v3, v4}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->bind(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V

    .line 69
    return-void
.end method

.method protected final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sInitialized:Z

    if-nez v1, :cond_17

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 44
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x7f02015b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sClockIconDrawabale:Landroid/graphics/drawable/Drawable;

    .line 45
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sInitialized:Z

    .line 48
    .end local v0           #resources:Landroid/content/res/Resources;
    :cond_17
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->mClockIcon:Landroid/widget/ImageView;

    .line 49
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->mClockIcon:Landroid/widget/ImageView;

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sClockIconDrawabale:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 50
    return-void
.end method
