.class public Lcom/google/android/apps/plus/views/EventDetailsMainLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsMainLayout.java"


# static fields
.field private static sDescriptionFontColor:I

.field private static sDescriptionFontSize:F

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sGoingLabelColor:I

.field private static sGoingLabelSize:I

.field private static sGoingLabelText:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sPadding:I

.field private static sWentLabelText:Ljava/lang/String;


# instance fields
.field private mDescriptionView:Landroid/widget/TextView;

.field private mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

.field private mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

.field private mRsvpLabel:Landroid/widget/TextView;

.field private mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

.field private mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 16
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x1

    const/4 v9, -0x2

    const/4 v5, 0x0

    .line 63
    sget-boolean v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sInitialized:Z

    if-nez v0, :cond_6a

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 66
    .local v8, resources:Landroid/content/res/Resources;
    const v0, 0x7f0d00a0

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionFontSize:F

    .line 68
    const v0, 0x7f0a0063

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionFontColor:I

    .line 71
    const v0, 0x7f0d0092

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    .line 73
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 74
    sput-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDividerPaint:Landroid/graphics/Paint;

    const v1, 0x7f0a0100

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 76
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDividerPaint:Landroid/graphics/Paint;

    const v1, 0x7f0d0094

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 79
    const v0, 0x7f0a0066

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelColor:I

    .line 80
    const v0, 0x7f0d00b3

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelSize:I

    .line 82
    const v0, 0x7f08035c

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelText:Ljava/lang/String;

    .line 83
    const v0, 0x7f08035d

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sWentLabelText:Ljava/lang/String;

    .line 84
    sput-boolean v11, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sInitialized:Z

    .line 87
    .end local v8           #resources:Landroid/content/res/Resources;
    :cond_6a
    sget v3, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionFontSize:F

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionFontColor:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v6, v5

    move v7, v5

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    .line 91
    new-instance v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    .line 94
    new-instance v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    .line 97
    new-instance v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    const v1, 0x7f090042

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setId(I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    .line 101
    new-instance v0, Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    new-instance v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v1, v10, v9}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    const v1, 0x7f090043

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setId(I)V

    .line 107
    sget v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelSize:I

    int-to-float v3, v0

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelColor:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v6, v11

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v1, v10, v9}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    .line 113
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .registers 10
    .parameter "event"
    .parameter "state"
    .parameter "listener"

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 191
    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    if-eqz v2, :cond_7e

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->descriptionHtml:Ljava/lang/String;

    if-eqz v2, :cond_7e

    .line 192
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->descriptionHtml:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    :goto_1a
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 200
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 202
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 203
    .local v0, now:J
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v2

    if-eqz v2, :cond_91

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sWentLabelText:Ljava/lang/String;

    :goto_37
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->bind$3ba8bae(Lcom/google/api/services/plusi/model/PlusEvent;)V

    .line 206
    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-nez v2, :cond_47

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    if-eqz v2, :cond_94

    .line 207
    :cond_47
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v2, p1, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/EventActionListener;)V

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->setVisibility(I)V

    .line 213
    :goto_51
    iget-boolean v2, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    if-nez v2, :cond_59

    iget-boolean v2, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    if-eqz v2, :cond_9a

    .line 214
    :cond_59
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setListener(Lcom/google/android/apps/plus/views/EventActionListener;)V

    .line 215
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->bind(Lcom/google/android/apps/plus/fragments/EventActiveState;)V

    .line 216
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setVisibility(I)V

    .line 221
    :goto_68
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->canRsvp(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v2

    if-eqz v2, :cond_a0

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 224
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setVisibility(I)V

    .line 230
    :goto_7d
    return-void

    .line 193
    .end local v0           #now:J
    :cond_7e
    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    if-eqz v2, :cond_8a

    .line 194
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1a

    .line 196
    :cond_8a
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1a

    .line 203
    .restart local v0       #now:J
    :cond_91
    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelText:Ljava/lang/String;

    goto :goto_37

    .line 210
    :cond_94
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->setVisibility(I)V

    goto :goto_51

    .line 218
    :cond_9a
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setVisibility(I)V

    goto :goto_68

    .line 226
    :cond_a0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setVisibility(I)V

    goto :goto_7d
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->clear()V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->clear()V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->clear()V

    .line 236
    return-void
.end method

.method protected measureChildren(II)V
    .registers 21
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 117
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 118
    .local v6, width:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 120
    .local v3, height:I
    const/4 v1, 0x0

    .line 122
    .local v1, currentTop:I
    sget v7, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/lit8 v4, v7, 0x0

    .line 123
    .local v4, paddedLeft:I
    sget v7, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    mul-int/lit8 v7, v7, 0x2

    sub-int v5, v6, v7

    .line 125
    .local v5, paddedWidth:I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_49

    .line 126
    add-int/lit8 v2, v3, 0x0

    .line 128
    .local v2, descriptionMaxHeight:I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    sub-int v8, v5, v8

    const/high16 v9, -0x8000

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v2, v10}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    .line 130
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/2addr v8, v4

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    .line 132
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/2addr v7, v8

    add-int/lit8 v1, v7, 0x0

    .line 135
    .end local v2           #descriptionMaxHeight:I
    :cond_49
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_73

    .line 136
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    const/high16 v8, 0x4000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v5, v8, v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    .line 137
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-static {v7, v4, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    .line 138
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/2addr v7, v8

    add-int/2addr v1, v7

    .line 141
    :cond_73
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_9b

    .line 142
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    const/high16 v8, 0x4000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v6, v8, v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    .line 143
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    const/4 v8, 0x0

    invoke-static {v7, v8, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    .line 144
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v1, v7

    .line 147
    :cond_9b
    const/4 v7, 0x3

    new-array v11, v7, [Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    aput-object v8, v11, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    aput-object v8, v11, v7

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    aput-object v8, v11, v7

    const/4 v9, 0x0

    array-length v12, v11

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v7, 0x0

    move/from16 v17, v7

    move v7, v10

    move/from16 v10, v17

    :goto_bd
    if-ge v10, v12, :cond_cf

    if-nez v7, :cond_c9

    aget-object v7, v11, v10

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_cd

    :cond_c9
    const/4 v7, 0x1

    :goto_ca
    add-int/lit8 v10, v10, 0x1

    goto :goto_bd

    :cond_cd
    const/4 v7, 0x0

    goto :goto_ca

    :cond_cf
    const/4 v7, 0x0

    move v10, v9

    move v9, v8

    move v8, v7

    .end local v1           #currentTop:I
    :goto_d3
    if-ge v8, v12, :cond_103

    aget-object v7, v11, v8

    const/4 v13, 0x0

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->getVisibility()I

    move-result v14

    const/16 v15, 0x8

    if-eq v14, v15, :cond_ff

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setFirst(Z)V

    const/high16 v14, -0x8000

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v7, v6, v14, v15, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    invoke-static {v7, v13, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->getMeasuredHeight()I

    move-result v7

    :goto_f4
    add-int/2addr v10, v7

    add-int/2addr v1, v7

    if-eqz v9, :cond_101

    if-nez v7, :cond_101

    const/4 v7, 0x1

    :goto_fb
    add-int/lit8 v8, v8, 0x1

    move v9, v7

    goto :goto_d3

    :cond_ff
    const/4 v7, 0x0

    goto :goto_f4

    :cond_101
    const/4 v7, 0x0

    goto :goto_fb

    .line 149
    :cond_103
    return-void
.end method
