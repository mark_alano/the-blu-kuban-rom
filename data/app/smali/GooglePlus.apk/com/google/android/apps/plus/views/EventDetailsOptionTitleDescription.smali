.class public Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsOptionTitleDescription.java"


# static fields
.field private static sDescriptionColor:I

.field private static sDescriptionSize:F

.field private static sInitialized:Z

.field private static sTitleColor:I

.field private static sTitleSize:F


# instance fields
.field private mDescription:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 43
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 49
    .local v7, resources:Landroid/content/res/Resources;
    sget-boolean v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sInitialized:Z

    if-nez v0, :cond_2f

    .line 50
    const v0, 0x7f0a0067

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleColor:I

    .line 51
    const v0, 0x7f0d00a8

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleSize:F

    .line 53
    const v0, 0x7f0a0068

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionColor:I

    .line 55
    const v0, 0x7f0d00a9

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionSize:F

    .line 58
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sInitialized:Z

    .line 61
    :cond_2f
    sget v3, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleSize:F

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleColor:I

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->addView(Landroid/view/View;)V

    .line 64
    sget v3, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionSize:F

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionColor:I

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->addView(Landroid/view/View;)V

    .line 67
    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "title"
    .parameter "description"

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_31

    move v0, v1

    :goto_17
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_33

    :goto_2d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    return-void

    :cond_31
    move v0, v2

    .line 86
    goto :goto_17

    :cond_33
    move v1, v2

    .line 89
    goto :goto_2d
.end method

.method public final clear()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

.method protected measureChildren(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v5, -0x8000

    const/4 v4, 0x0

    .line 71
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 72
    .local v2, width:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 74
    .local v1, height:I
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-static {v3, v2, v5, v1, v4}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->measure(Landroid/view/View;IIII)V

    .line 76
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-static {v3, v4, v4}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->setCorner(Landroid/view/View;II)V

    .line 78
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2f

    move v3, v4

    :goto_22
    add-int/lit8 v0, v3, 0x0

    .line 80
    .local v0, descriptionTop:I
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    invoke-static {v3, v2, v5, v1, v4}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->measure(Landroid/view/View;IIII)V

    .line 81
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescription:Landroid/widget/TextView;

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->setCorner(Landroid/view/View;II)V

    .line 82
    return-void

    .line 78
    .end local v0           #descriptionTop:I
    :cond_2f
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    goto :goto_22
.end method
