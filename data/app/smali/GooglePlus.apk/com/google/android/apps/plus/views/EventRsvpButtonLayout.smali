.class public Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;
.super Landroid/widget/LinearLayout;
.source "EventRsvpButtonLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

.field private mMaybeDivider:Landroid/view/View;

.field private mMaybeView:Landroid/view/View;

.field private mNoView:Landroid/view/View;

.field private mYesView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/android/apps/plus/views/EventRsvpListener;Z)V
    .registers 5
    .parameter "listener"
    .parameter "past"

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    .line 46
    if-eqz p2, :cond_11

    const/16 v0, 0x8

    .line 47
    .local v0, maybeVisibility:I
    :goto_6
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeDivider:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 49
    return-void

    .line 46
    .end local v0           #maybeVisibility:I
    :cond_11
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    if-eqz v0, :cond_f

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mYesView:Landroid/view/View;

    if-ne p1, v0, :cond_10

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    .line 78
    :cond_f
    :goto_f
    return-void

    .line 72
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    if-ne p1, v0, :cond_1c

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_f

    .line 74
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mNoView:Landroid/view/View;

    if-ne p1, v0, :cond_f

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_f
.end method

.method protected onFinishInflate()V
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 58
    const v0, 0x7f09009e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const v0, 0x7f09009d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mYesView:Landroid/view/View;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mYesView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v0, 0x7f0900a0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mNoView:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mNoView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const v0, 0x7f09009f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeDivider:Landroid/view/View;

    .line 65
    return-void
.end method
