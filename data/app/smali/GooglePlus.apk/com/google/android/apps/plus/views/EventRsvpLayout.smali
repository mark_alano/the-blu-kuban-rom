.class public Lcom/google/android/apps/plus/views/EventRsvpLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventRsvpLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/EventRsvpListener;


# static fields
.field private static sBackgroundColor:I

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sInitialized:Z

.field private static sRsvpSectionHeight:I


# instance fields
.field private mEventOver:Z

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mMeasuredWidth:I

.field private mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

.field private mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v5, 0x7f0d0094

    const v4, 0x7f0a0100

    .line 55
    sget-boolean v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sInitialized:Z

    if-nez v2, :cond_51

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 57
    .local v1, resources:Landroid/content/res/Resources;
    const v2, 0x7f0a0065

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sBackgroundColor:I

    .line 59
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 60
    sput-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    sget-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 65
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 66
    sput-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    sget-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 69
    const v2, 0x7f0d009a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sRsvpSectionHeight:I

    .line 72
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sInitialized:Z

    .line 75
    .end local v1           #resources:Landroid/content/res/Resources;
    :cond_51
    sget v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sBackgroundColor:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setBackgroundColor(I)V

    .line 77
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 79
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v2, 0x7f03002f

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    .line 81
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->addView(Landroid/view/View;)V

    .line 83
    new-instance v2, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    .line 84
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setVisibility(I)V

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->addView(Landroid/view/View;)V

    .line 87
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setWillNotDraw(Z)V

    .line 88
    return-void
.end method

.method private setRsvpView(Ljava/lang/String;Z)V
    .registers 13
    .parameter "rsvpAttending"
    .parameter "animate"

    .prologue
    const/4 v7, 0x4

    const/high16 v9, 0x3f80

    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 158
    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_16

    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5e

    :cond_16
    move v3, v5

    .line 160
    .local v3, undecided:Z
    :goto_17
    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_RESPONDED:Ljava/lang/String;

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_60

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mEventOver:Z

    if-eqz v6, :cond_25

    if-nez v3, :cond_60

    .line 162
    :cond_25
    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getVisibility()I

    move-result v1

    .line 163
    .local v1, originalVisibility:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->setVisibility(I)V

    .line 164
    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setVisibility(I)V

    .line 166
    if-eqz p2, :cond_5d

    if-nez v1, :cond_5d

    .line 167
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 168
    .local v2, outAnimation:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v6, 0x1f4

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 169
    invoke-virtual {v2, v5}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 170
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 171
    .local v0, inAnimation:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 172
    invoke-virtual {v0, v5}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 174
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 175
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 181
    .end local v0           #inAnimation:Landroid/view/animation/AlphaAnimation;
    .end local v1           #originalVisibility:I
    .end local v2           #outAnimation:Landroid/view/animation/AlphaAnimation;
    :cond_5d
    :goto_5d
    return-void

    .end local v3           #undecided:Z
    :cond_5e
    move v3, v4

    .line 158
    goto :goto_17

    .line 178
    .restart local v3       #undecided:Z
    :cond_60
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->setVisibility(I)V

    .line 179
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setVisibility(I)V

    goto :goto_5d
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .registers 8
    .parameter "event"
    .parameter "state"
    .parameter "listener"

    .prologue
    .line 130
    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 132
    .local v0, now:J
    invoke-static {p1, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mEventOver:Z

    .line 133
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v2, p1, p2, p0, p3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventRsvpListener;Lcom/google/android/apps/plus/views/EventActionListener;)V

    .line 134
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mEventOver:Z

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->bind(Lcom/google/android/apps/plus/views/EventRsvpListener;Z)V

    .line 135
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setRsvpView(Ljava/lang/String;Z)V

    .line 136
    return-void
.end method

.method protected measureChildren(II)V
    .registers 8
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v4, 0x4000

    const/4 v3, 0x0

    .line 92
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    .line 93
    sget v0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sRsvpSectionHeight:I

    .line 97
    .local v0, maxHeight:I
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v0, v4}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    invoke-static {v1, v3, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setCorner(Landroid/view/View;II)V

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v0, v4}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    invoke-static {v1, v3, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setCorner(Landroid/view/View;II)V

    .line 113
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter "canvas"

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 119
    iget v0, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    int-to-float v3, v0

    sget-object v5, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 120
    return-void
.end method

.method public final onRsvpChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "rsvpAttending"

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_d

    .line 146
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setRsvpView(Ljava/lang/String;Z)V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/views/EventActionListener;->onRsvpChanged(Ljava/lang/String;)V

    .line 149
    :cond_d
    return-void
.end method
