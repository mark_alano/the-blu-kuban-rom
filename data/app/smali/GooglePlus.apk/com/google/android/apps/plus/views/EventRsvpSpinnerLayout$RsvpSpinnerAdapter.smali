.class final Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;
.super Landroid/widget/BaseAdapter;
.source "EventRsvpSpinnerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RsvpSpinnerAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPast:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;Landroid/content/Context;Z)V
    .registers 4
    .parameter
    .parameter "context"
    .parameter "past"

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->this$0:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 169
    iput-boolean p3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    .line 170
    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    .line 171
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 158
    const/4 v0, -0x1

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_13

    :cond_11
    const/4 v0, 0x0

    :cond_12
    :goto_12
    return v0

    :cond_13
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_27

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    if-eqz v1, :cond_29

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_29

    :cond_27
    const/4 v0, 0x1

    goto :goto_12

    :cond_29
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    const/4 v0, 0x2

    goto :goto_12
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x3

    goto :goto_5
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    .prologue
    .line 264
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 269
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const v6, 0x7f09004f

    const/4 v4, 0x0

    .line 200
    const/4 v1, 0x0

    .line 201
    .local v1, spinnerItem:Landroid/view/View;
    const/4 v0, 0x0

    .line 203
    .local v0, rsvpType:Ljava/lang/String;
    packed-switch p1, :pswitch_data_84

    .line 224
    :goto_9
    if-eqz v1, :cond_37

    if-eqz v0, :cond_37

    .line 225
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 226
    .local v2, textView:Landroid/widget/TextView;
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_37

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->this$0:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6c

    const v4, 0x7f08035f

    :cond_30
    :goto_30
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    .end local v2           #textView:Landroid/widget/TextView;
    :cond_37
    return-object v1

    .line 205
    :pswitch_38
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f03002d

    invoke-virtual {v3, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 207
    sget-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    .line 208
    goto :goto_9

    .line 210
    :pswitch_48
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    if-nez v3, :cond_5c

    .line 211
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f030032

    invoke-virtual {v3, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 213
    sget-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    .line 214
    goto :goto_9

    .line 218
    :cond_5c
    :pswitch_5c
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f030033

    invoke-virtual {v3, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 220
    sget-object v0, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    goto :goto_9

    .line 226
    .restart local v2       #textView:Landroid/widget/TextView;
    :cond_6c
    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_78

    const v4, 0x7f080360

    goto :goto_30

    :cond_78
    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_30

    const v4, 0x7f08035a

    goto :goto_30

    .line 203
    :pswitch_data_84
    .packed-switch 0x0
        :pswitch_38
        :pswitch_48
        :pswitch_5c
    .end packed-switch
.end method
