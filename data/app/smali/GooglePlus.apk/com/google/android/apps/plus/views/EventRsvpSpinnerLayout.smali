.class public Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventRsvpSpinnerLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;
    }
.end annotation


# static fields
.field private static sAddPhotosDrawable:Landroid/graphics/drawable/Drawable;

.field private static sAddPhotosText:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sInviteMoreDrawable:Landroid/graphics/drawable/Drawable;

.field private static sInviteMoreText:Ljava/lang/String;

.field private static sPadding:I


# instance fields
.field private mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

.field private mCurrentSelectionIndex:I

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mEventOver:Z

.field private mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

.field private mRsvpSpinner:Landroid/widget/Spinner;

.field private mShowActionButton:Z

.field private mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    .line 51
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 65
    sget-boolean v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInitialized:Z

    if-nez v1, :cond_38

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x7f0d009d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    .line 68
    const v1, 0x7f020167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreDrawable:Landroid/graphics/drawable/Drawable;

    .line 69
    const v1, 0x7f08010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreText:Ljava/lang/String;

    .line 70
    const v1, 0x7f020166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosDrawable:Landroid/graphics/drawable/Drawable;

    .line 71
    const v1, 0x7f080110

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosText:Ljava/lang/String;

    .line 72
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInitialized:Z

    .line 75
    .end local v0           #resources:Landroid/content/res/Resources;
    :cond_38
    new-instance v1, Landroid/widget/Spinner;

    invoke-direct {v1, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    .line 76
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->addView(Landroid/view/View;)V

    .line 80
    new-instance v1, Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->addView(Landroid/view/View;)V

    .line 84
    sget v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sget v2, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sget v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sget v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setPadding(IIII)V

    .line 85
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventRsvpListener;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .registers 11
    .parameter "event"
    .parameter "state"
    .parameter "listener"
    .parameter "actionListener"

    .prologue
    .line 96
    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    .line 97
    iput-object p4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 98
    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 101
    .local v0, now:J
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    .line 103
    new-instance v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;-><init>(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;Landroid/content/Context;Z)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    .line 104
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 107
    iget-object v3, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->temporalRsvpValue:Ljava/lang/String;

    if-eqz v3, :cond_70

    iget-object v2, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->temporalRsvpValue:Ljava/lang/String;

    .line 109
    .local v2, rsvpState:Ljava/lang/String;
    :goto_2c
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    .line 111
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    iget v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 112
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->notifyDataSetChanged()V

    .line 113
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 114
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    iget-boolean v4, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isRsvpEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 116
    iget v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_75

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v3, :cond_64

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsEventData;->canViewerAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v3

    if-nez v3, :cond_6c

    :cond_64
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-nez v3, :cond_75

    iget-boolean v3, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->canUploadPhotos:Z

    if-eqz v3, :cond_75

    :cond_6c
    const/4 v3, 0x1

    :goto_6d
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    .line 120
    return-void

    .line 107
    .end local v2           #rsvpState:Ljava/lang/String;
    :cond_70
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2c

    .line 116
    .restart local v2       #rsvpState:Ljava/lang/String;
    :cond_75
    const/4 v3, 0x0

    goto :goto_6d
.end method

.method protected measureChildren(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v4, 0x4000

    const/4 v5, 0x0

    .line 133
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 134
    .local v0, measuredWidth:I
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    if-nez v2, :cond_46

    move v1, v0

    .line 139
    .local v1, splitWidth:I
    :goto_c
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-static {v2, v1, v4, v5, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->measure(Landroid/view/View;IIII)V

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-static {v2, v5, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setCorner(Landroid/view/View;II)V

    .line 142
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    if-eqz v2, :cond_5b

    .line 143
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v1, v4, v3, v4}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->measure(Landroid/view/View;IIII)V

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    add-int/lit8 v3, v1, 0x0

    sget v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    add-int/2addr v3, v4

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setCorner(Landroid/view/View;II)V

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setVisibility(I)V

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_51

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v2, :cond_51

    .line 149
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->bind(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 156
    :goto_45
    return-void

    .line 134
    .end local v1           #splitWidth:I
    :cond_46
    sget v2, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_c

    .line 151
    .restart local v1       #splitWidth:I
    :cond_51
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->bind(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_45

    .line 154
    :cond_5b
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setVisibility(I)V

    goto :goto_45
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    if-ne p1, v0, :cond_11

    .line 308
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v0, :cond_12

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onAddPhotosClicked()V

    .line 314
    :cond_11
    :goto_11
    return-void

    .line 311
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onInviteMoreClicked()V

    goto :goto_11
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 275
    iget v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    if-eq v1, p3, :cond_25

    .line 276
    iget v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_26

    move v0, v2

    .line 278
    .local v0, initialSetting:Z
    :goto_c
    if-nez v0, :cond_11

    .line 279
    packed-switch p3, :pswitch_data_4a

    .line 293
    :cond_11
    :goto_11
    iput p3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    .line 294
    iget v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I

    move-result v4

    if-ne v1, v4, :cond_47

    move v1, v2

    :goto_20
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    .line 297
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->requestLayout()V

    .line 299
    .end local v0           #initialSetting:Z
    :cond_25
    return-void

    :cond_26
    move v0, v3

    .line 276
    goto :goto_c

    .line 281
    .restart local v0       #initialSetting:Z
    :pswitch_28
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    sget-object v4, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_ATTENDING:Ljava/lang/String;

    invoke-interface {v1, v4}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_11

    .line 284
    :pswitch_30
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v1, :cond_3c

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    :goto_38
    invoke-interface {v4, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_11

    :cond_3c
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_MAYBE:Ljava/lang/String;

    goto :goto_38

    .line 288
    :pswitch_3f
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    sget-object v4, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_NOT_ATTENDING:Ljava/lang/String;

    invoke-interface {v1, v4}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_11

    :cond_47
    move v1, v3

    .line 294
    goto :goto_20

    .line 279
    nop

    :pswitch_data_4a
    .packed-switch 0x0
        :pswitch_28
        :pswitch_30
        :pswitch_3f
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 303
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method
