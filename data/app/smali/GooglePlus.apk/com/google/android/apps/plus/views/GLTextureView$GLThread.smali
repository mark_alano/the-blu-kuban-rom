.class final Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
.super Ljava/lang/Thread;
.source "GLTextureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/GLTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GLThread"
.end annotation


# instance fields
.field private mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

.field private mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mExited:Z

.field private mHasSurface:Z

.field private mHaveEglContext:Z

.field private mHaveEglSurface:Z

.field private mHeight:I

.field private mPaused:Z

.field private mRenderComplete:Z

.field private mRenderMode:I

.field private mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

.field private mRequestPaused:Z

.field private mRequestRender:Z

.field private mShouldExit:Z

.field private mShouldReleaseEglContext:Z

.field private mWaitingForSurface:Z

.field private mWidth:I

.field final synthetic this$0:Lcom/google/android/apps/plus/views/GLTextureView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/GLTextureView;Lcom/google/android/apps/plus/views/GLTextureView$Renderer;)V
    .registers 6
    .parameter
    .parameter "renderer"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1213
    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    .line 1214
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1704
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    .line 1215
    iput v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    .line 1216
    iput v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    .line 1217
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    .line 1218
    iput v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    .line 1219
    iput-object p2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    .line 1220
    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    return v0
.end method

.method private guardedRun()V
    .registers 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1261
    new-instance v17, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    .line 1262
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    .line 1263
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    .line 1265
    const/4 v10, 0x0

    .line 1266
    .local v10, gl:Ljavax/microedition/khronos/opengles/GL10;
    const/4 v6, 0x0

    .line 1267
    .local v6, createEglContext:Z
    const/4 v7, 0x0

    .line 1268
    .local v7, createEglSurface:Z
    const/4 v12, 0x0

    .line 1269
    .local v12, lostEglContext:Z
    const/4 v13, 0x0

    .line 1270
    .local v13, sizeChanged:Z
    const/16 v16, 0x0

    .line 1271
    .local v16, wantRenderNotification:Z
    const/4 v8, 0x0

    .line 1272
    .local v8, doRenderNotification:Z
    const/4 v5, 0x0

    .line 1273
    .local v5, askedToReleaseEglContext:Z
    const/4 v15, 0x0

    .line 1274
    .local v15, w:I
    const/4 v11, 0x0

    .line 1275
    .local v11, h:I
    const/4 v9, 0x0

    .line 1278
    .local v9, event:Ljava/lang/Runnable;
    :cond_2d
    :goto_2d
    :try_start_2d
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18
    :try_end_32
    .catchall {:try_start_2d .. :try_end_32} :catchall_1ba

    .line 1280
    :goto_32
    :try_start_32
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldExit:Z

    move/from16 v17, v0

    if-eqz v17, :cond_4b

    .line 1281
    monitor-exit v18
    :try_end_3b
    .catchall {:try_start_32 .. :try_end_3b} :catchall_1b7

    .line 1510
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18

    .line 1511
    :try_start_40
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1512
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1513
    monitor-exit v18
    :try_end_47
    .catchall {:try_start_40 .. :try_end_47} :catchall_48

    .line 1515
    :goto_47
    return-void

    .line 1513
    :catchall_48
    move-exception v17

    monitor-exit v18

    throw v17

    .line 1284
    :cond_4b
    :try_start_4b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_74

    .line 1285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Runnable;

    move-object v9, v0

    .line 1451
    :goto_6c
    monitor-exit v18
    :try_end_6d
    .catchall {:try_start_4b .. :try_end_6d} :catchall_1b7

    .line 1453
    if-eqz v9, :cond_210

    .line 1454
    :try_start_6f
    invoke-interface {v9}, Ljava/lang/Runnable;->run()V
    :try_end_72
    .catchall {:try_start_6f .. :try_end_72} :catchall_1ba

    .line 1455
    const/4 v9, 0x0

    .line 1456
    goto :goto_2d

    .line 1290
    :cond_74
    :try_start_74
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v19

    if-eq v0, v1, :cond_99

    .line 1291
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    .line 1292
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    .line 1300
    :cond_99
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    move/from16 v17, v0

    if-eqz v17, :cond_b0

    .line 1306
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1307
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1308
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    .line 1309
    const/4 v5, 0x1

    .line 1313
    :cond_b0
    if-eqz v12, :cond_b9

    .line 1314
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1315
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1316
    const/4 v12, 0x0

    .line 1320
    :cond_b9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f8

    .line 1325
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v17, v0

    #getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mPreserveEGLContextOnPause:Z
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView;->access$900(Lcom/google/android/apps/plus/views/GLTextureView;)Z

    move-result v17

    if-eqz v17, :cond_e2

    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->shouldReleaseEGLContextWhenPausing()Z

    move-result v17

    if-eqz v17, :cond_e5

    .line 1328
    :cond_e2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1335
    :cond_e5
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->shouldTerminateEGLWhenPausing()Z

    move-result v17

    if-eqz v17, :cond_f8

    .line 1336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->finish()V

    .line 1345
    :cond_f8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    move/from16 v17, v0

    if-nez v17, :cond_122

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    move/from16 v17, v0

    if-nez v17, :cond_122

    .line 1350
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_113

    .line 1351
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1353
    :cond_113
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    .line 1354
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    .line 1358
    :cond_122
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_141

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_141

    .line 1363
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    .line 1364
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    .line 1367
    :cond_141
    if-eqz v8, :cond_155

    .line 1372
    const/16 v16, 0x0

    .line 1373
    const/4 v8, 0x0

    .line 1374
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    .line 1375
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    .line 1379
    :cond_155
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->readyToDraw()Z

    move-result v17

    if-eqz v17, :cond_207

    .line 1382
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    move/from16 v17, v0

    if-nez v17, :cond_166

    .line 1383
    if-eqz v5, :cond_1c8

    .line 1384
    const/4 v5, 0x0

    .line 1399
    :cond_166
    :goto_166
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    move/from16 v17, v0

    if-eqz v17, :cond_180

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-nez v17, :cond_180

    .line 1400
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    .line 1401
    const/4 v7, 0x1

    .line 1402
    const/4 v13, 0x1

    .line 1405
    :cond_180
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    if-eqz v17, :cond_207

    .line 1406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v17, v0

    #getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1000(Lcom/google/android/apps/plus/views/GLTextureView;)Z

    move-result v17

    if-eqz v17, :cond_1fe

    .line 1407
    const/4 v13, 0x1

    .line 1408
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    .line 1409
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    .line 1410
    const/16 v16, 0x1

    .line 1426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v17, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    #setter for: Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1002(Lcom/google/android/apps/plus/views/GLTextureView;Z)Z

    .line 1430
    :goto_1ae
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V
    :try_end_1b5
    .catchall {:try_start_74 .. :try_end_1b5} :catchall_1b7

    goto/16 :goto_6c

    .line 1451
    :catchall_1b7
    move-exception v17

    :try_start_1b8
    monitor-exit v18

    throw v17
    :try_end_1ba
    .catchall {:try_start_1b8 .. :try_end_1ba} :catchall_1ba

    .line 1514
    :catchall_1ba
    move-exception v17

    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18

    .line 1511
    :try_start_1c0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1512
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1513
    monitor-exit v18
    :try_end_1c7
    .catchall {:try_start_1c0 .. :try_end_1c7} :catchall_2cf

    throw v17

    .line 1385
    :cond_1c8
    :try_start_1c8
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->tryAcquireEglContextLocked(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)Z
    :try_end_1d3
    .catchall {:try_start_1c8 .. :try_end_1d3} :catchall_1b7

    move-result v17

    if-eqz v17, :cond_166

    .line 1387
    :try_start_1d6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->start()V
    :try_end_1df
    .catchall {:try_start_1d6 .. :try_end_1df} :catchall_1b7
    .catch Ljava/lang/RuntimeException; {:try_start_1d6 .. :try_end_1df} :catch_1f1

    .line 1392
    const/16 v17, 0x1

    :try_start_1e1
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    .line 1393
    const/4 v6, 0x1

    .line 1395
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_166

    .line 1388
    :catch_1f1
    move-exception v14

    .line 1389
    .local v14, t:Ljava/lang/RuntimeException;
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->releaseEglContextLocked(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    .line 1390
    throw v14

    .line 1428
    .end local v14           #t:Ljava/lang/RuntimeException;
    :cond_1fe
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    goto :goto_1ae

    .line 1449
    :cond_207
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->wait()V
    :try_end_20e
    .catchall {:try_start_1e1 .. :try_end_20e} :catchall_1b7

    goto/16 :goto_32

    .line 1459
    :cond_210
    if-eqz v7, :cond_237

    .line 1464
    :try_start_212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/plus/views/GLTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->createSurface(Landroid/graphics/SurfaceTexture;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    move-object v10, v0

    .line 1465
    if-eqz v10, :cond_2be

    .line 1467
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1470
    const/4 v7, 0x0

    .line 1473
    :cond_237
    if-eqz v6, :cond_24f

    .line 1477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$Renderer;->onSurfaceCreated$4a9c201c()V

    .line 1478
    const/4 v6, 0x0

    .line 1481
    :cond_24f
    if-eqz v13, :cond_2a3

    .line 1485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    sget-object v20, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v21, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v22, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface/range {v18 .. v22}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v17, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    move-object/from16 v4, v17

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 1486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v15, v11}, Lcom/google/android/apps/plus/views/GLTextureView$Renderer;->onSurfaceChanged$4ccda93f(II)V

    .line 1487
    const/4 v13, 0x0

    .line 1493
    :cond_2a3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$Renderer;->onDrawFrame$62c01aa1()V

    .line 1494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->swap()Z
    :try_end_2b5
    .catchall {:try_start_212 .. :try_end_2b5} :catchall_1ba

    move-result v17

    if-nez v17, :cond_2b9

    .line 1498
    const/4 v12, 0x1

    .line 1501
    :cond_2b9
    if-eqz v16, :cond_2d

    .line 1502
    const/4 v8, 0x1

    goto/16 :goto_2d

    .line 1510
    :cond_2be
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v18

    monitor-enter v18

    .line 1511
    :try_start_2c3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglSurfaceLocked()V

    .line 1512
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->stopEglContextLocked()V

    .line 1513
    monitor-exit v18
    :try_end_2ca
    .catchall {:try_start_2c3 .. :try_end_2ca} :catchall_2cc

    goto/16 :goto_47

    :catchall_2cc
    move-exception v17

    monitor-exit v18

    throw v17

    :catchall_2cf
    move-exception v17

    monitor-exit v18

    throw v17
.end method

.method private readyToDraw()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1522
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    if-nez v1, :cond_1a

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    if-eqz v1, :cond_1a

    iget v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    if-lez v1, :cond_1a

    iget v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    if-lez v1, :cond_1a

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    if-nez v1, :cond_19

    iget v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    if-ne v1, v0, :cond_1a

    :cond_19
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private stopEglContextLocked()V
    .registers 2

    .prologue
    .line 1254
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_13

    .line 1255
    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->finish()V

    .line 1256
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    .line 1257
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->releaseEglContextLocked(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    .line 1259
    :cond_13
    return-void
.end method

.method private stopEglSurfaceLocked()V
    .registers 7

    .prologue
    .line 1243
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_32

    .line 1244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    .line 1245
    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mEglHelper:Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v1, :cond_32

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v1, v2, :cond_32

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    iget-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    #getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/GLTextureView;->access$500(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;->destroySurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1247
    :cond_32
    return-void
.end method


# virtual methods
.method public final getRenderMode()I
    .registers 3

    .prologue
    .line 1539
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1540
    :try_start_5
    iget v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    monitor-exit v1
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_9

    return v0

    .line 1541
    :catchall_9
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onPause()V
    .registers 3

    .prologue
    .line 1586
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1590
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    .line 1591
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1592
    :goto_f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    if-nez v0, :cond_2b

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z
    :try_end_15
    .catchall {:try_start_6 .. :try_end_15} :catchall_28

    if-nez v0, :cond_2b

    .line 1597
    :try_start_17
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1e
    .catchall {:try_start_17 .. :try_end_1e} :catchall_28
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_1e} :catch_1f

    goto :goto_f

    .line 1599
    :catch_1f
    move-exception v0

    :try_start_20
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_27
    .catchall {:try_start_20 .. :try_end_27} :catchall_28

    goto :goto_f

    .line 1602
    :catchall_28
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2b
    :try_start_2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_28

    return-void
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 1606
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1610
    const/4 v0, 0x0

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestPaused:Z

    .line 1611
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    .line 1612
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    .line 1613
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1614
    :goto_15
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    if-nez v0, :cond_35

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    if-eqz v0, :cond_35

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z
    :try_end_1f
    .catchall {:try_start_6 .. :try_end_1f} :catchall_32

    if-nez v0, :cond_35

    .line 1619
    :try_start_21
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_28
    .catchall {:try_start_21 .. :try_end_28} :catchall_32
    .catch Ljava/lang/InterruptedException; {:try_start_21 .. :try_end_28} :catch_29

    goto :goto_15

    .line 1621
    :catch_29
    move-exception v0

    :try_start_2a
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_31
    .catchall {:try_start_2a .. :try_end_31} :catchall_32

    goto :goto_15

    .line 1624
    :catchall_32
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_35
    :try_start_35
    monitor-exit v1
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_32

    return-void
.end method

.method public final onWindowResize(II)V
    .registers 8
    .parameter "w"
    .parameter "h"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1628
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v3

    monitor-enter v3

    .line 1629
    :try_start_7
    iput p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWidth:I

    .line 1630
    iput p2, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHeight:I

    .line 1631
    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    const/4 v4, 0x1

    #setter for: Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1002(Lcom/google/android/apps/plus/views/GLTextureView;Z)Z

    .line 1632
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    .line 1633
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    .line 1634
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1638
    :goto_1e
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z

    if-nez v0, :cond_5f

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mPaused:Z

    if-nez v0, :cond_5f

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderComplete:Z

    if-nez v0, :cond_5f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    #getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1100(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    move-result-object v0

    if-eqz v0, :cond_5f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    #getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/GLTextureView;->access$1100(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    move-result-object v0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglContext:Z

    if-eqz v4, :cond_5d

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHaveEglSurface:Z

    if-eqz v4, :cond_5d

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->readyToDraw()Z
    :try_end_43
    .catchall {:try_start_7 .. :try_end_43} :catchall_5a

    move-result v0

    if-eqz v0, :cond_5d

    move v0, v1

    :goto_47
    if-eqz v0, :cond_5f

    .line 1645
    :try_start_49
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_50
    .catchall {:try_start_49 .. :try_end_50} :catchall_5a
    .catch Ljava/lang/InterruptedException; {:try_start_49 .. :try_end_50} :catch_51

    goto :goto_1e

    .line 1647
    :catch_51
    move-exception v0

    :try_start_52
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_59
    .catchall {:try_start_52 .. :try_end_59} :catchall_5a

    goto :goto_1e

    .line 1650
    :catchall_5a
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5d
    move v0, v2

    .line 1638
    goto :goto_47

    .line 1650
    :cond_5f
    :try_start_5f
    monitor-exit v3
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5a

    return-void
.end method

.method public final requestExitAndWait()V
    .registers 3

    .prologue
    .line 1656
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1657
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldExit:Z

    .line 1658
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1659
    :goto_f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z
    :try_end_11
    .catchall {:try_start_6 .. :try_end_11} :catchall_24

    if-nez v0, :cond_27

    .line 1661
    :try_start_13
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_24
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_1a} :catch_1b

    goto :goto_f

    .line 1663
    :catch_1b
    move-exception v0

    :try_start_1c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_23
    .catchall {:try_start_1c .. :try_end_23} :catchall_24

    goto :goto_f

    .line 1666
    :catchall_24
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_27
    :try_start_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_24

    return-void
.end method

.method public final requestReleaseEglContextLocked()V
    .registers 2

    .prologue
    .line 1670
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mShouldReleaseEglContext:Z

    .line 1671
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1672
    return-void
.end method

.method public final requestRender()V
    .registers 3

    .prologue
    .line 1545
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1546
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRequestRender:Z

    .line 1547
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1548
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_11

    return-void

    :catchall_11
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final run()V
    .registers 4

    .prologue
    .line 1224
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GLThread "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->setName(Ljava/lang/String;)V

    .line 1230
    :try_start_16
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->guardedRun()V
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_2a
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_19} :catch_21

    .line 1234
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->threadExiting(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    .line 1235
    :goto_20
    return-void

    .line 1234
    :catch_21
    move-exception v0

    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->threadExiting(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    goto :goto_20

    :catchall_2a
    move-exception v0

    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;->threadExiting(Lcom/google/android/apps/plus/views/GLTextureView$GLThread;)V

    throw v0
.end method

.method public final setRenderMode(I)V
    .registers 4
    .parameter "renderMode"

    .prologue
    .line 1528
    if-ltz p1, :cond_5

    const/4 v0, 0x1

    if-le p1, v0, :cond_d

    .line 1530
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1532
    :cond_d
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1533
    :try_start_12
    iput p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mRenderMode:I

    .line 1534
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1535
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_12 .. :try_end_1c} :catchall_1d

    return-void

    :catchall_1d
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final surfaceCreated()V
    .registers 3

    .prologue
    .line 1552
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1556
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    .line 1557
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1558
    :goto_f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    if-eqz v0, :cond_2b

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z
    :try_end_15
    .catchall {:try_start_6 .. :try_end_15} :catchall_28

    if-nez v0, :cond_2b

    .line 1560
    :try_start_17
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1e
    .catchall {:try_start_17 .. :try_end_1e} :catchall_28
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_1e} :catch_1f

    goto :goto_f

    .line 1562
    :catch_1f
    move-exception v0

    :try_start_20
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_27
    .catchall {:try_start_20 .. :try_end_27} :catchall_28

    goto :goto_f

    .line 1565
    :catchall_28
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2b
    :try_start_2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_28

    return-void
.end method

.method public final surfaceDestroyed()V
    .registers 3

    .prologue
    .line 1569
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1573
    const/4 v0, 0x0

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mHasSurface:Z

    .line 1574
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1575
    :goto_f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mWaitingForSurface:Z

    if-nez v0, :cond_2b

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->mExited:Z
    :try_end_15
    .catchall {:try_start_6 .. :try_end_15} :catchall_28

    if-nez v0, :cond_2b

    .line 1577
    :try_start_17
    invoke-static {}, Lcom/google/android/apps/plus/views/GLTextureView;->access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1e
    .catchall {:try_start_17 .. :try_end_1e} :catchall_28
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_1e} :catch_1f

    goto :goto_f

    .line 1579
    :catch_1f
    move-exception v0

    :try_start_20
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_27
    .catchall {:try_start_20 .. :try_end_27} :catchall_28

    goto :goto_f

    .line 1582
    :catchall_28
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2b
    :try_start_2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_28

    return-void
.end method
