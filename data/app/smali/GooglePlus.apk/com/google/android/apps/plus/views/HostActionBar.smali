.class public Lcom/google/android/apps/plus/views/HostActionBar;
.super Landroid/widget/RelativeLayout;
.source "HostActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/HostActionBar$SavedState;,
        Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;,
        Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;,
        Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;,
        Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;,
        Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
    }
.end annotation


# static fields
.field private static sHandler:Landroid/os/Handler;


# instance fields
.field private mActionButton1:Landroid/widget/ImageView;

.field private mActionButton1Visible:Z

.field private mActionButton2:Landroid/widget/ImageView;

.field private mActionButton2Visible:Z

.field private mActionId1:I

.field private mActionId2:I

.field private mActive:Z

.field private mAppIcon:Landroid/widget/ImageView;

.field private mContextActionMode:Z

.field private mCurrentButtonActionText:Ljava/lang/String;

.field private mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

.field private mDoneButton:Landroid/view/View;

.field private mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

.field private mInvalidateActionBarRunnable:Ljava/lang/Runnable;

.field private mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

.field private mNotificationCount:I

.field private mNotificationCountOverflow:Landroid/view/View;

.field private mNotificationCountText:Landroid/widget/TextView;

.field private mOverflowMenuButton:Landroid/view/View;

.field private mOverflowPopupMenu:Ljava/lang/Object;

.field private mOverflowPopupMenuVisible:Z

.field private mPrimarySpinner:Landroid/widget/Spinner;

.field private mPrimarySpinnerContainer:Landroid/view/View;

.field private mPrimarySpinnerVisible:Z

.field private mProgressIndicator:Landroid/view/View;

.field private mProgressIndicatorVisible:Z

.field private mRefreshButton:Landroid/view/View;

.field private mRefreshButtonVisible:Z

.field private mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

.field private mSearchViewContainer:Landroid/view/View;

.field private mSearchViewVisible:Z

.field private mShareMenuButton:Landroid/view/View;

.field private mShareMenuVisible:Z

.field private mSharePopupMenu:Ljava/lang/Object;

.field private mSharePopupMenuVisible:Z

.field private mTitle:Landroid/widget/TextView;

.field private mTitleVisible:Z

.field private mUpButton:Landroid/view/View;

.field private mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 150
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 139
    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar$1;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 139
    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar$1;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    .line 155
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 158
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 139
    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar$1;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    .line 159
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/HostActionBar;)Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/HostActionBar;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/views/HostActionBar;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/views/HostActionBar;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showOverflowMenu()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showSharePopupMenu()V

    return-void
.end method

.method private configurePopupMenuListeners(Ljava/lang/Object;)V
    .registers 6
    .parameter "popupMenu"

    .prologue
    const/4 v3, 0x0

    .line 757
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1b

    .line 758
    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;B)V

    .local v0, listener:Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;
    move-object v1, p1

    .line 759
    check-cast v1, Landroid/widget/PopupMenu;

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 763
    :goto_15
    check-cast p1, Landroid/widget/PopupMenu;

    .end local p1
    invoke-virtual {p1, v0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 764
    return-void

    .line 761
    .end local v0           #listener:Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;
    .restart local p1
    :cond_1b
    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;B)V

    .restart local v0       #listener:Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;
    goto :goto_15
.end method

.method private getOverflowPopupMenu()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 682
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    if-eqz v1, :cond_7

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    .line 692
    :goto_6
    return-object v0

    .line 686
    :cond_7
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 687
    .local v0, popupMenu:Landroid/widget/PopupMenu;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->configurePopupMenuListeners(Ljava/lang/Object;)V

    .line 689
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 691
    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    goto :goto_6
.end method

.method private isOverflowMenuSupported()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 225
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_19

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-nez v2, :cond_17

    .line 228
    :cond_16
    :goto_16
    return v0

    :cond_17
    move v0, v1

    .line 226
    goto :goto_16

    .line 228
    :cond_19
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_16

    move v0, v1

    goto :goto_16
.end method

.method private prepareOverflowMenu()Z
    .registers 7

    .prologue
    .line 637
    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    if-nez v5, :cond_6

    .line 638
    const/4 v0, 0x0

    .line 678
    :cond_5
    :goto_5
    return v0

    .line 641
    :cond_6
    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    check-cast v5, Landroid/widget/PopupMenu;

    invoke-virtual {v5}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    .line 644
    .local v3, menu:Landroid/view/Menu;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5, v3}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 646
    const/4 v0, 0x0

    .line 647
    .local v0, anyVisible:Z
    invoke-interface {v3}, Landroid/view/Menu;->size()I

    move-result v4

    .line 669
    .local v4, size:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1d
    if-ge v1, v4, :cond_5

    .line 670
    invoke-interface {v3, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 671
    .local v2, item:Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_2b

    .line 672
    const/4 v0, 0x1

    .line 673
    goto :goto_5

    .line 669
    :cond_2b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1d
.end method

.method private prepareSharePopupMenu()Z
    .registers 12

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 709
    iget-object v8, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    if-nez v8, :cond_8

    move v0, v9

    .line 737
    :cond_7
    return v0

    .line 713
    :cond_8
    iget-object v8, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    check-cast v8, Landroid/widget/PopupMenu;

    invoke-virtual {v8}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v6

    .line 716
    .local v6, menu:Landroid/view/Menu;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8, v6}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 719
    const/4 v0, 0x0

    .line 720
    .local v0, anyVisible:Z
    invoke-interface {v6}, Landroid/view/Menu;->size()I

    move-result v7

    .line 721
    .local v7, size:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1f
    if-ge v1, v7, :cond_7

    .line 722
    invoke-interface {v6, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 723
    .local v4, item:Landroid/view/MenuItem;
    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 724
    .local v2, id:I
    const/4 v3, 0x0

    .line 725
    .local v3, isShareItem:Z
    const/4 v5, 0x0

    .local v5, j:I
    :goto_2b
    array-length v8, v10

    if-ge v5, v8, :cond_33

    .line 726
    aget v8, v10, v5

    if-ne v2, v8, :cond_3c

    .line 727
    const/4 v3, 0x1

    .line 732
    :cond_33
    if-nez v3, :cond_39

    .line 733
    const/4 v0, 0x1

    .line 734
    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 721
    :cond_39
    add-int/lit8 v1, v1, 0x1

    goto :goto_1f

    .line 725
    :cond_3c
    add-int/lit8 v5, v5, 0x1

    goto :goto_2b
.end method

.method private showOverflowMenu()V
    .registers 3

    .prologue
    .line 620
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    .line 621
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getOverflowPopupMenu()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/PopupMenu;

    .line 622
    .local v0, popupMenu:Landroid/widget/PopupMenu;
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareOverflowMenu()Z

    .line 623
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    .line 624
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 625
    return-void
.end method

.method private showSharePopupMenu()V
    .registers 5

    .prologue
    .line 699
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    .line 700
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    if-eqz v2, :cond_18

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    :goto_9
    check-cast v1, Landroid/widget/PopupMenu;

    .line 701
    .local v1, popupMenu:Landroid/widget/PopupMenu;
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareSharePopupMenu()Z

    move-result v0

    .line 702
    .local v0, anyVisible:Z
    if-eqz v0, :cond_17

    .line 703
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    .line 704
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 706
    :cond_17
    return-void

    .line 700
    .end local v0           #anyVisible:Z
    .end local v1           #popupMenu:Landroid/widget/PopupMenu;
    :cond_18
    new-instance v1, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-direct {v1, v2, v3}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->configurePopupMenuListeners(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    goto :goto_9
.end method

.method private showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z
    .registers 13
    .parameter "view"
    .parameter "tooltip"

    .prologue
    const/4 v8, 0x0

    .line 589
    const/4 v7, 0x2

    new-array v3, v7, [I

    .line 590
    .local v3, screenPos:[I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 591
    .local v1, displayFrame:Landroid/graphics/Rect;
    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 592
    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 594
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 595
    .local v0, context:Landroid/content/Context;
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 596
    .local v6, width:I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 597
    .local v2, height:I
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v4, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 599
    .local v4, screenWidth:I
    invoke-static {v0, p2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 601
    .local v5, toast:Landroid/widget/Toast;
    const/16 v7, 0x35

    aget v8, v3, v8

    sub-int v8, v4, v8

    div-int/lit8 v9, v6, 0x2

    sub-int/2addr v8, v9

    invoke-virtual {v5, v7, v8, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 603
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 604
    const/4 v7, 0x1

    return v7
.end method


# virtual methods
.method public final commit()V
    .registers 5

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 289
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v0, :cond_95

    move v0, v1

    :goto_a
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 290
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v0, :cond_98

    move v0, v2

    :goto_14
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 292
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    if-eqz v0, :cond_9b

    move v0, v2

    :goto_1e
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 293
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerContainer:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    if-eqz v0, :cond_9d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_9d

    move v0, v2

    :goto_34
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 297
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    if-nez v0, :cond_42

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 302
    :cond_42
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewContainer:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    if-eqz v0, :cond_9f

    move v0, v2

    :goto_49
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setVisible(Z)V

    .line 305
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisible:Z

    if-eqz v0, :cond_a1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    if-nez v0, :cond_a1

    move v0, v2

    :goto_5e
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 307
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    if-eqz v0, :cond_a3

    move v0, v2

    :goto_68
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 309
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-eqz v0, :cond_a5

    move v0, v2

    :goto_72
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    if-eqz v0, :cond_a7

    move v0, v2

    :goto_7c
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 315
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isOverflowMenuSupported()Z

    move-result v0

    if-eqz v0, :cond_91

    .line 316
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    if-eqz v0, :cond_a9

    .line 317
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareSharePopupMenu()Z

    .line 324
    :cond_91
    :goto_91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    .line 325
    return-void

    :cond_95
    move v0, v2

    .line 289
    goto/16 :goto_a

    :cond_98
    move v0, v1

    .line 290
    goto/16 :goto_14

    :cond_9b
    move v0, v1

    .line 292
    goto :goto_1e

    :cond_9d
    move v0, v1

    .line 293
    goto :goto_34

    :cond_9f
    move v0, v1

    .line 302
    goto :goto_49

    :cond_a1
    move v0, v1

    .line 305
    goto :goto_5e

    :cond_a3
    move v0, v1

    .line 307
    goto :goto_68

    :cond_a5
    move v0, v1

    .line 309
    goto :goto_72

    :cond_a7
    move v0, v1

    .line 310
    goto :goto_7c

    .line 318
    :cond_a9
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    if-eqz v0, :cond_b1

    .line 319
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareOverflowMenu()Z

    goto :goto_91

    .line 321
    :cond_b1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getOverflowPopupMenu()Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareOverflowMenu()Z

    move-result v3

    if-eqz v3, :cond_c0

    :goto_bc
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_91

    :cond_c0
    move v2, v1

    goto :goto_bc
.end method

.method public final dismissPopupMenus()V
    .registers 2

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    if-eqz v0, :cond_b

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    check-cast v0, Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 611
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    if-eqz v0, :cond_16

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    check-cast v0, Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 614
    :cond_16
    return-void
.end method

.method public final finishContextActionMode()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 338
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v0, :cond_17

    .line 339
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    .line 340
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_17

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 345
    :cond_17
    return-void
.end method

.method public final getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;
    .registers 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    return-object v0
.end method

.method public final hideProgressIndicator()V
    .registers 5

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 425
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    .line 426
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v2, :cond_17

    .line 427
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 428
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisible:Z

    if-eqz v3, :cond_18

    :goto_14
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 430
    :cond_17
    return-void

    :cond_18
    move v0, v1

    .line 428
    goto :goto_14
.end method

.method public final invalidateActionBar()V
    .registers 3

    .prologue
    .line 529
    sget-object v0, Lcom/google/android/apps/plus/views/HostActionBar;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_f

    .line 530
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/views/HostActionBar;->sHandler:Landroid/os/Handler;

    .line 533
    :cond_f
    sget-object v0, Lcom/google/android/apps/plus/views/HostActionBar;->sHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 534
    sget-object v0, Lcom/google/android/apps/plus/views/HostActionBar;->sHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 535
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    if-ne p1, v0, :cond_e

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;

    if-eqz v0, :cond_d

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;->onUpButtonClick()V

    .line 567
    :cond_d
    :goto_d
    return-void

    .line 546
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    if-ne p1, v0, :cond_1c

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

    if-eqz v0, :cond_d

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;->onDoneButtonClick()V

    goto :goto_d

    .line 550
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    if-ne p1, v0, :cond_24

    .line 551
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showOverflowMenu()V

    goto :goto_d

    .line 552
    :cond_24
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    if-ne p1, v0, :cond_2c

    .line 553
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showSharePopupMenu()V

    goto :goto_d

    .line 554
    :cond_2c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    if-ne p1, v0, :cond_3a

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_d

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onRefreshButtonClicked()V

    goto :goto_d

    .line 558
    :cond_3a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_4a

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_d

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId1:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onActionButtonClicked(I)V

    goto :goto_d

    .line 562
    :cond_4a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_d

    .line 563
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_d

    .line 564
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId2:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onActionButtonClicked(I)V

    goto :goto_d
.end method

.method protected onFinishInflate()V
    .registers 5

    .prologue
    const/16 v3, 0x8

    .line 166
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 167
    const v0, 0x7f0900e9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    const v0, 0x7f0900fc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    const v0, 0x7f09009c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mAppIcon:Landroid/widget/ImageView;

    .line 175
    const v0, 0x7f090063

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    .line 176
    const v0, 0x7f090104

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerContainer:Landroid/view/View;

    .line 177
    const v0, 0x7f09004e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 179
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0300bf

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 183
    const v0, 0x7f090103

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewContainer:Landroid/view/View;

    .line 184
    const v0, 0x7f09004c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->createInstance(Landroid/view/View;)Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->requestFocus(Z)V

    .line 188
    const v0, 0x7f090102

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    .line 189
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isOverflowMenuSupported()Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 194
    :cond_9a
    const v0, 0x7f0900fe

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 198
    const v0, 0x7f090101

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 202
    const v0, 0x7f090100

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 206
    const v0, 0x7f0900ff

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    .line 208
    const v0, 0x7f0900fa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    const-string v1, "99"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    const v0, 0x7f0900fb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    .line 213
    const v0, 0x7f0900fd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    .line 215
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isOverflowMenuSupported()Z

    move-result v0

    if-eqz v0, :cond_119

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    :goto_114
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mCurrentButtonActionText:Ljava/lang/String;

    .line 222
    return-void

    .line 218
    :cond_119
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_114
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter "voiw"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, spinner:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_9

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    invoke-interface {v0, p3}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onPrimarySpinnerSelectionChange(I)V

    .line 516
    :cond_9
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 5
    .parameter "v"

    .prologue
    const/4 v0, 0x1

    .line 571
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    if-ne p1, v1, :cond_14

    .line 572
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0803e4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    .line 585
    :goto_13
    return v0

    .line 574
    :cond_14
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    if-ne p1, v1, :cond_27

    .line 575
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0800e2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    goto :goto_13

    .line 577
    :cond_27
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_35

    .line 578
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    goto :goto_13

    .line 580
    :cond_35
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_43

    .line 581
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    goto :goto_13

    .line 585
    :cond_43
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 523
    .local p1, view:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .parameter "state"

    .prologue
    .line 844
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;

    .line 845
    .local v0, ss:Lcom/google/android/apps/plus/views/HostActionBar$SavedState;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 846
    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->overflowPopupMenuVisible:Z

    if-eqz v1, :cond_16

    .line 847
    new-instance v1, Lcom/google/android/apps/plus/views/HostActionBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/HostActionBar$2;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->post(Ljava/lang/Runnable;)Z

    .line 855
    :cond_16
    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->sharePopupMenuVisible:Z

    if-eqz v1, :cond_22

    .line 856
    new-instance v1, Lcom/google/android/apps/plus/views/HostActionBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/HostActionBar$3;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->post(Ljava/lang/Runnable;)Z

    .line 864
    :cond_22
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 5

    .prologue
    .line 833
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 834
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 835
    .local v0, ss:Lcom/google/android/apps/plus/views/HostActionBar$SavedState;
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_17

    .line 836
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->overflowPopupMenuVisible:Z

    .line 837
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->sharePopupMenuVisible:Z

    .line 839
    :cond_17
    return-object v0
.end method

.method public final reset()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 272
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    .line 273
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    .line 274
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    .line 275
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    .line 276
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    .line 277
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisible:Z

    .line 278
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    .line 279
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    .line 280
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    .line 281
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuVisible:Z

    .line 282
    return-void
.end method

.method public setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    .line 233
    return-void
.end method

.method public setNotificationCount(I)V
    .registers 6
    .parameter "notificationCount"

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 484
    iput p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    .line 485
    iget v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mAppIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_18
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setUpButtonContentDescription(Ljava/lang/String;)V

    .line 486
    return-void

    .line 485
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mAppIcon:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    const/16 v1, 0x63

    if-gt v0, v1, :cond_3f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_18

    :cond_3f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_18
.end method

.method public setOnDoneButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

    .line 241
    return-void
.end method

.method public setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 236
    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;

    .line 237
    return-void
.end method

.method public setPrimarySpinnerSelection(I)V
    .registers 4
    .parameter "selectedPosition"

    .prologue
    .line 393
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    .line 395
    .local v0, count:I
    if-ge p1, v0, :cond_f

    if-ltz p1, :cond_f

    .line 396
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 398
    :cond_f
    return-void
.end method

.method public setUpButtonContentDescription(Ljava/lang/String;)V
    .registers 9
    .parameter "buttonAction"

    .prologue
    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3a

    .line 253
    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mCurrentButtonActionText:Ljava/lang/String;

    .line 254
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 259
    :goto_10
    iget v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    if-lez v1, :cond_30

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0026

    iget v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 264
    :cond_30
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 265
    return-void

    .line 256
    :cond_3a
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mCurrentButtonActionText:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    goto :goto_10
.end method

.method public final showActionButton(III)V
    .registers 8
    .parameter "actionId"
    .parameter "iconResId"
    .parameter "labelResId"

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 442
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-nez v2, :cond_2e

    .line 443
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    .line 444
    iput p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId1:I

    .line 445
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v2, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 446
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 448
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v2, :cond_2b

    .line 449
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-eqz v3, :cond_2c

    :goto_28
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 463
    :cond_2b
    :goto_2b
    return-void

    :cond_2c
    move v0, v1

    .line 449
    goto :goto_28

    .line 451
    :cond_2e
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    if-nez v2, :cond_58

    .line 452
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    .line 453
    iput p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId2:I

    .line 454
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v2, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 455
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 457
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v2, :cond_2b

    .line 458
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    if-eqz v3, :cond_56

    :goto_52
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2b

    :cond_56
    move v0, v1

    goto :goto_52

    .line 461
    :cond_58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only two action buttons are supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V
    .registers 6
    .parameter "spinnerAdapter"
    .parameter "selectedPosition"

    .prologue
    .line 378
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 380
    invoke-interface {p1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    .line 381
    .local v0, count:I
    if-lez v0, :cond_13

    .line 382
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 384
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v1, :cond_1f

    .line 385
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerContainer:Landroid/view/View;

    if-lez v0, :cond_20

    const/4 v1, 0x0

    :goto_1c
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 387
    :cond_1f
    return-void

    .line 385
    :cond_20
    const/16 v1, 0x8

    goto :goto_1c
.end method

.method public final showProgressIndicator()V
    .registers 3

    .prologue
    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    .line 415
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_14

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 419
    :cond_14
    return-void
.end method

.method public final showRefreshButton()V
    .registers 3

    .prologue
    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisible:Z

    .line 405
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    if-nez v0, :cond_11

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 408
    :cond_11
    return-void
.end method

.method public final showSearchView()V
    .registers 3

    .prologue
    .line 364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    .line 365
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_d

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 368
    :cond_d
    return-void
.end method

.method public final showTitle(I)V
    .registers 5
    .parameter "titleResId"

    .prologue
    const/4 v1, 0x0

    .line 348
    if-eqz p1, :cond_19

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 350
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_18

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    if-eqz v2, :cond_1b

    :goto_15
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 353
    :cond_18
    return-void

    :cond_19
    move v0, v1

    .line 348
    goto :goto_4

    .line 351
    :cond_1b
    const/16 v1, 0x8

    goto :goto_15
.end method

.method public final showTitle(Ljava/lang/String;)V
    .registers 5
    .parameter "title"

    .prologue
    const/4 v1, 0x0

    .line 356
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1d

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_1c

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    if-eqz v2, :cond_1f

    :goto_19
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 361
    :cond_1c
    return-void

    :cond_1d
    move v0, v1

    .line 356
    goto :goto_8

    .line 359
    :cond_1f
    const/16 v1, 0x8

    goto :goto_19
.end method

.method public final startContextActionMode()V
    .registers 3

    .prologue
    .line 328
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-nez v0, :cond_18

    .line 329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    .line 330
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_18

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 335
    :cond_18
    return-void
.end method
