.class public final Lcom/google/android/apps/plus/views/MediaImage;
.super Ljava/lang/Object;
.source "MediaImage.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;


# static fields
.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mInvalidated:Z

.field private final mPostHeight:I

.field private final mPostWidth:I

.field private final mRequest:Lcom/google/android/apps/plus/content/ImageRequest;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;)V
    .registers 4
    .parameter "view"
    .parameter "request"

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;II)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;II)V
    .registers 6
    .parameter "view"
    .parameter "request"
    .parameter "postWidth"
    .parameter "postHeight"

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/plus/views/MediaImage;->mView:Landroid/view/View;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/plus/views/MediaImage;->mRequest:Lcom/google/android/apps/plus/content/ImageRequest;

    .line 41
    sget-object v0, Lcom/google/android/apps/plus/views/MediaImage;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    if-nez v0, :cond_15

    .line 42
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/MediaImage;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 44
    :cond_15
    iput p3, p0, Lcom/google/android/apps/plus/views/MediaImage;->mPostWidth:I

    .line 45
    iput p4, p0, Lcom/google/android/apps/plus/views/MediaImage;->mPostHeight:I

    .line 46
    return-void
.end method


# virtual methods
.method public final cancelRequest()V
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mRequest:Lcom/google/android/apps/plus/content/ImageRequest;

    if-eqz v0, :cond_9

    .line 110
    sget-object v0, Lcom/google/android/apps/plus/views/MediaImage;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/ImageCache;->cancel(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;)V

    .line 112
    :cond_9
    return-void
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final invalidate()V
    .registers 2

    .prologue
    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mInvalidated:Z

    .line 81
    return-void
.end method

.method public final load()V
    .registers 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mRequest:Lcom/google/android/apps/plus/content/ImageRequest;

    if-eqz v0, :cond_b

    .line 101
    sget-object v0, Lcom/google/android/apps/plus/views/MediaImage;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/MediaImage;->mRequest:Lcom/google/android/apps/plus/content/ImageRequest;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/ImageCache;->loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 103
    :cond_b
    return-void
.end method

.method public final refreshIfInvalidated()V
    .registers 3

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mInvalidated:Z

    if-eqz v0, :cond_12

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mInvalidated:Z

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mRequest:Lcom/google/android/apps/plus/content/ImageRequest;

    if-eqz v0, :cond_12

    .line 91
    sget-object v0, Lcom/google/android/apps/plus/views/MediaImage;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/MediaImage;->mRequest:Lcom/google/android/apps/plus/content/ImageRequest;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/ImageCache;->refreshImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 94
    :cond_12
    return-void
.end method

.method public final setBitmap(Landroid/graphics/Bitmap;Z)V
    .registers 5
    .parameter "bitmap"
    .parameter "loading"

    .prologue
    .line 50
    if-eqz p1, :cond_18

    iget v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mPostWidth:I

    if-eqz v0, :cond_18

    iget v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mPostHeight:I

    if-eqz v0, :cond_18

    iget v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mPostWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/views/MediaImage;->mPostHeight:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeAndCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1a

    :cond_18
    iput-object p1, p0, Lcom/google/android/apps/plus/views/MediaImage;->mBitmap:Landroid/graphics/Bitmap;

    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MediaImage;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 51
    return-void
.end method
