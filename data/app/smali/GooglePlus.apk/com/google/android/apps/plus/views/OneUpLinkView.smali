.class public Lcom/google/android/apps/plus/views/OneUpLinkView;
.super Lcom/google/android/apps/plus/views/OneUpBackgroundView;
.source "OneUpLinkView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;


# static fields
.field protected static sLinkBitmap:Landroid/graphics/Bitmap;

.field protected static sMaxWidth:I

.field protected static sMinExposureLand:I

.field protected static sMinExposurePort:I

.field private static sOneUpLinkViewInitialized:Z


# instance fields
.field protected mAvailableContentHeight:I

.field protected mBackgroundDestRect:Landroid/graphics/Rect;

.field protected mBackgroundSrcRect:Landroid/graphics/Rect;

.field protected mImageBorderRect:Landroid/graphics/Rect;

.field protected mImageDimension:I

.field protected mImageRect:Landroid/graphics/Rect;

.field protected mLinkTitle:Ljava/lang/String;

.field protected mLinkTitleLayout:Landroid/text/StaticLayout;

.field protected mLinkUrl:Ljava/lang/String;

.field protected mLinkUrlLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/OneUpLinkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/OneUpLinkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    sget-boolean v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sOneUpLinkViewInitialized:Z

    if-nez v1, :cond_36

    .line 58
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sOneUpLinkViewInitialized:Z

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    .local v0, res:Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020100

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sLinkBitmap:Landroid/graphics/Bitmap;

    .line 64
    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMaxWidth:I

    .line 65
    const v1, 0x7f0d0165

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposureLand:I

    .line 67
    const v1, 0x7f0d0164

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposurePort:I

    .line 71
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_36
    invoke-static {p1}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->init(Landroid/content/Context;)V

    .line 73
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    .line 74
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundDestRect:Landroid/graphics/Rect;

    .line 75
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    .line 76
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageBorderRect:Landroid/graphics/Rect;

    .line 77
    return-void
.end method


# virtual methods
.method public final init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "mediaRef"
    .parameter "type"
    .parameter "listener"
    .parameter "title"
    .parameter "linkUrl"

    .prologue
    .line 81
    iput-object p4, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitle:Ljava/lang/String;

    .line 82
    iput-object p5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrl:Ljava/lang/String;

    .line 84
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;)V

    .line 85
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 90
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onAttachedToWindow()V

    .line 91
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 95
    sget-object v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 96
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onDetachedFromWindow()V

    .line 97
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    .prologue
    const/4 v6, 0x0

    .line 145
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onDraw(Landroid/graphics/Canvas;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 151
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_1d

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundDestRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageBorderRect:Landroid/graphics/Rect;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 156
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 159
    .local v3, xOffset:I
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_70

    .line 160
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_57

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    :goto_39
    float-to-int v8, v0

    .line 163
    .local v8, textOffset:I
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_62

    move v9, v6

    .line 164
    .local v9, titleHeight:I
    :goto_3f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_69

    move v10, v6

    .line 165
    .local v10, urlHeight:I
    :goto_44
    iget v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    sub-int/2addr v0, v9

    sub-int/2addr v0, v10

    div-int/lit8 v0, v0, 0x2

    add-int v4, v0, v8

    .line 170
    .end local v8           #textOffset:I
    .end local v9           #titleHeight:I
    .end local v10           #urlHeight:I
    .local v4, yOffset:I
    :goto_4c
    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    sget-object v7, Lcom/google/android/apps/plus/views/OneUpLinkView;->sLinkBitmap:Landroid/graphics/Bitmap;

    move-object v2, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->drawTitleAndUrl(Landroid/graphics/Canvas;IILandroid/text/StaticLayout;Landroid/text/StaticLayout;Landroid/graphics/Bitmap;)V

    .line 172
    return-void

    .line 160
    .end local v4           #yOffset:I
    :cond_57
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    goto :goto_39

    .line 163
    .restart local v8       #textOffset:I
    :cond_62
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    goto :goto_3f

    .line 164
    .restart local v9       #titleHeight:I
    :cond_69
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v10

    goto :goto_44

    .line 167
    .end local v8           #textOffset:I
    .end local v9           #titleHeight:I
    :cond_70
    const/4 v4, 0x0

    .restart local v4       #yOffset:I
    goto :goto_4c
.end method

.method public onLayout(ZIIII)V
    .registers 21
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 101
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->onLayout(ZIIII)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getMeasuredWidth()I

    move-result v14

    .line 104
    .local v14, availableWidth:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getMeasuredHeight()I

    move-result v13

    .line 105
    .local v13, availableHeight:I
    sget v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMaxWidth:I

    if-gt v14, v1, :cond_94

    .line 106
    iput v13, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    .line 115
    :goto_11
    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getMaxImageDimension()I

    move-result v3

    .line 116
    .local v3, maxImageDimension:I
    iget v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    if-nez v1, :cond_2c

    .line 118
    int-to-float v1, v14

    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getImageMaxWidthPercentage()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    .line 123
    :cond_2c
    new-instance v0, Lcom/google/android/apps/plus/content/MediaImageRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v5, 0x1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .line 125
    .local v0, request:Lcom/google/android/apps/plus/content/ImageRequest;
    new-instance v1, Lcom/google/android/apps/plus/views/MediaImage;

    invoke-direct {v1, p0, v0, v3, v3}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;II)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 128
    iget v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundDestRect:Landroid/graphics/Rect;

    move v4, v14

    move v5, v13

    move v9, v14

    move v10, v13

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createBackgroundRects(IIIIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 132
    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    iget v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageBorderRect:Landroid/graphics/Rect;

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createImageRects(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitle:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v4, v14, v4

    invoke-static {v1, v2, v4}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createTitle(Ljava/lang/String;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrl:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, v14, v1

    sget-object v5, Lcom/google/android/apps/plus/views/OneUpLinkView;->sLinkBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int v5, v1, v5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v1, :cond_b1

    const/4 v1, 0x0

    :goto_8d
    invoke-static {v2, v4, v5, v1}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createUrl(Ljava/lang/String;III)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    .line 141
    return-void

    .line 108
    .end local v0           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v3           #maxImageDimension:I
    :cond_94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_a9

    .line 110
    sget v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposureLand:I

    sub-int v1, v13, v1

    iput v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    goto/16 :goto_11

    .line 112
    :cond_a9
    sget v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposurePort:I

    sub-int v1, v13, v1

    iput v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    goto/16 :goto_11

    .line 138
    .restart local v0       #request:Lcom/google/android/apps/plus/content/ImageRequest;
    .restart local v3       #maxImageDimension:I
    :cond_b1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    goto :goto_8d
.end method

.method public final onMediaImageChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->onImageChanged()V

    .line 179
    :cond_13
    return-void
.end method
