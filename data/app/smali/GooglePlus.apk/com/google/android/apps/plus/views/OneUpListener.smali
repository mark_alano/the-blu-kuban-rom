.class public interface abstract Lcom/google/android/apps/plus/views/OneUpListener;
.super Ljava/lang/Object;
.source "OneUpListener.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;
.implements Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;


# virtual methods
.method public abstract onLocationClick$75c560e7(Lcom/google/android/apps/plus/content/DbLocation;)V
.end method

.method public abstract onPlaceClick(Ljava/lang/String;)V
.end method

.method public abstract onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
.end method

.method public abstract onSkyjamBuyClick(Ljava/lang/String;)V
.end method

.method public abstract onSkyjamListenClick(Ljava/lang/String;)V
.end method

.method public abstract onSourceAppContentClick(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/text/style/URLSpan;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/text/style/URLSpan;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onSourceAppNameClick$1b7460f0(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onSourceAppNameLinkEnabled()V
.end method
