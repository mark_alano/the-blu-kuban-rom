.class public Lcom/google/android/apps/plus/views/OneUpTouchHandler;
.super Landroid/view/ViewGroup;
.source "OneUpTouchHandler.java"


# instance fields
.field private mActionBar:Landroid/view/View;

.field private mBackground:Landroid/view/View;

.field private mLocation:[I

.field private mScrollView:Landroid/view/View;

.field private mTagView:Landroid/view/View;

.field private mTargetView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    .line 43
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 13
    .parameter "event"

    .prologue
    const/4 v10, 0x3

    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 47
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 48
    .local v1, eventX:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 51
    .local v2, eventY:F
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-nez v4, :cond_5b

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mScrollView:Landroid/view/View;

    if-eqz v4, :cond_5b

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mScrollView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_5b

    .line 53
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mScrollView:Landroid/view/View;

    const v5, 0x102000a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 54
    .local v3, listView:Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 55
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_5b

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_5b

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_5b

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gez v4, :cond_5b

    .line 57
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mScrollView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    .line 62
    .end local v3           #listView:Landroid/view/View;
    :cond_5b
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-nez v4, :cond_a8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTagView:Landroid/view/View;

    if-eqz v4, :cond_a8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTagView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_a8

    .line 64
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTagView:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    invoke-virtual {v4, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 65
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_a8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTagView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_a8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_a8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTagView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gez v4, :cond_a8

    .line 67
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTagView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    .line 72
    :cond_a8
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-nez v4, :cond_f5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mActionBar:Landroid/view/View;

    if-eqz v4, :cond_f5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mActionBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_f5

    .line 74
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mActionBar:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    invoke-virtual {v4, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 75
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_f5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mActionBar:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_f5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_f5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mActionBar:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gez v4, :cond_f5

    .line 77
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mActionBar:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    .line 82
    :cond_f5
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-nez v4, :cond_101

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mBackground:Landroid/view/View;

    if-eqz v4, :cond_101

    .line 83
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mBackground:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    .line 86
    :cond_101
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-eqz v4, :cond_121

    .line 87
    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v10, :cond_122

    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-virtual {v4, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 89
    :goto_116
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 90
    .local v0, action:I
    if-eq v0, v10, :cond_11e

    if-ne v0, v9, :cond_121

    .line 91
    :cond_11e
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    .line 94
    .end local v0           #action:I
    :cond_121
    return v9

    .line 87
    :cond_122
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->getScrollX()I

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->getScrollY()I

    move-result v7

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    invoke-virtual {v4, v5}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v5}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_116
.end method

.method public final getTargetView()Landroid/view/View;
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTargetView:Landroid/view/View;

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .registers 6
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 110
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public setActionBar(Landroid/view/View;)V
    .registers 2
    .parameter "actionBar"

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mActionBar:Landroid/view/View;

    .line 130
    return-void
.end method

.method public setBackground(Landroid/view/View;)V
    .registers 2
    .parameter "background"

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mBackground:Landroid/view/View;

    .line 115
    return-void
.end method

.method public setScrollView(Landroid/view/View;)V
    .registers 2
    .parameter "scrollView"

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mScrollView:Landroid/view/View;

    .line 120
    return-void
.end method

.method public setTagLayout(Landroid/view/View;)V
    .registers 2
    .parameter "tagLayout"

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->mTagView:Landroid/view/View;

    .line 125
    return-void
.end method
