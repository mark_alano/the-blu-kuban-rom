.class public Lcom/google/android/apps/plus/views/PeopleListItemView;
.super Lcom/google/android/apps/plus/views/CheckableListItemView;
.source "PeopleListItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;
    }
.end annotation


# static fields
.field private static sAddButtonIcon:Landroid/graphics/drawable/Drawable;

.field private static sDefaultUserImage:Landroid/graphics/Bitmap;

.field private static sMediumAvatarSize:I

.field private static sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

.field private static sSmallAvatarSize:I

.field private static sTinyAvatarSize:I

.field private static sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

.field private static sVerticalDivider:Landroid/graphics/drawable/Drawable;

.field private static sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

.field private static sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mActionButton:Landroid/widget/TextView;

.field private final mActionButtonResourceId:I

.field private mActionButtonVisible:Z

.field private final mActionButtonWidth:I

.field private mAddButton:Landroid/widget/ImageView;

.field private mAddButtonVisible:Z

.field private mAvatarBitmap:Landroid/graphics/Bitmap;

.field private final mAvatarBounds:Landroid/graphics/Rect;

.field private final mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private mAvatarInvalidated:Z

.field private final mAvatarOriginalBounds:Landroid/graphics/Rect;

.field private final mAvatarPaint:Landroid/graphics/Paint;

.field private mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

.field private mAvatarRequestSize:I

.field private final mAvatarSize:I

.field private mAvatarUrl:Ljava/lang/String;

.field private mAvatarVisible:Z

.field private final mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

.field private final mCircleIconSize:I

.field private mCircleIconVisible:Z

.field private mCircleLineHeight:I

.field private mCircleListVisible:Z

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private final mCirclesTextColor:I

.field private final mCirclesTextSize:F

.field private final mCirclesTextView:Landroid/widget/TextView;

.field private mContactLookupKey:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private final mEmailIconPaddingLeft:I

.field private final mEmailIconPaddingTop:I

.field private final mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

.field private mFirstRow:Z

.field private mGaiaId:Ljava/lang/String;

.field private final mGapBetweenIconAndCircles:I

.field private final mGapBetweenImageAndText:I

.field private final mGapBetweenNameAndCircles:I

.field private final mGapBetweenTextAndButton:I

.field private mHighlightedText:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

.field private final mNameTextBuilder:Landroid/text/SpannableStringBuilder;

.field private final mNameTextView:Landroid/widget/TextView;

.field private final mPaddingBottom:I

.field private final mPaddingLeft:I

.field private final mPaddingRight:I

.field private final mPaddingTop:I

.field private mPersonId:Ljava/lang/String;

.field private mPlusPage:Z

.field private final mPreferredHeight:I

.field private mRemoveButton:Landroid/widget/ImageView;

.field private mRemoveButtonVisible:Z

.field protected mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

.field protected mSectionHeaderHeight:I

.field protected mSectionHeaderVisible:Z

.field private mTypeTextView:Landroid/widget/TextView;

.field private mTypeTextViewVisible:Z

.field private mUnblockButton:Landroid/widget/ImageView;

.field private mUnblockButtonVisible:Z

.field private mVerticalDividerLeft:I

.field private final mVerticalDividerPadding:I

.field private final mVerticalDividerWidth:I

.field private mWellFormedEmail:Ljava/lang/String;

.field private mWellFormedEmailMode:Z

.field private mWellFormedSms:Ljava/lang/String;

.field private mWellFormedSmsMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 12
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v6, -0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 160
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/CheckableListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    .line 75
    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    .line 83
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    .line 84
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    .line 94
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    .line 95
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    .line 162
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 164
    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->ContactListItemView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 165
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    .line 167
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingTop:I

    .line 169
    invoke-virtual {v0, v7, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingBottom:I

    .line 171
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingLeft:I

    .line 173
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingRight:I

    .line 175
    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 177
    .local v1, nameTextSize:F
    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextSize:F

    .line 179
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenImageAndText:I

    .line 181
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    .line 183
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    .line 185
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenNameAndCircles:I

    .line 187
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    .line 189
    const/16 v2, 0xd

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenTextAndButton:I

    .line 191
    const/16 v2, 0xe

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    .line 193
    const/16 v2, 0xf

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    .line 195
    invoke-virtual {v0, v8, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    .line 197
    const/16 v2, 0x11

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    .line 199
    const/16 v2, 0xa

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextColor:I

    .line 201
    const/16 v2, 0x12

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingTop:I

    .line 203
    const/16 v2, 0x13

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingLeft:I

    .line 205
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 207
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 210
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    const v3, 0x1030044

    invoke-virtual {v2, p1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 211
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 213
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 215
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    .line 217
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    .line 218
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 219
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    const v3, 0x1030044

    invoke-virtual {v2, p1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextSize:F

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 224
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    .line 228
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    .line 230
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_14d

    .line 231
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    .line 234
    :cond_14d
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_162

    .line 235
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020069

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    .line 239
    :cond_162
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_177

    .line 240
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    .line 244
    :cond_177
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_18c

    .line 245
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    .line 249
    :cond_18c
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    .line 251
    sget v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    if-nez v2, :cond_1b0

    .line 252
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    .line 253
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sSmallAvatarSize:I

    .line 254
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sTinyAvatarSize:I

    .line 257
    :cond_1b0
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    if-le v2, v3, :cond_1be

    .line 258
    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    .line 259
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 276
    :goto_1bd
    return-void

    .line 260
    :cond_1be
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    if-ne v2, v3, :cond_1c7

    .line 261
    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    goto :goto_1bd

    .line 262
    :cond_1c7
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sSmallAvatarSize:I

    if-le v2, v3, :cond_1d5

    .line 263
    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    .line 264
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_1bd

    .line 265
    :cond_1d5
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sSmallAvatarSize:I

    if-ne v2, v3, :cond_1de

    .line 266
    iput v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    goto :goto_1bd

    .line 267
    :cond_1de
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sTinyAvatarSize:I

    if-le v2, v3, :cond_1ec

    .line 268
    iput v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    .line 269
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_1bd

    .line 270
    :cond_1ec
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sTinyAvatarSize:I

    if-ne v2, v3, :cond_1f5

    .line 271
    iput v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    goto :goto_1bd

    .line 273
    :cond_1f5
    iput v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    .line 274
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_1bd
.end method

.method public static createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;
    .registers 6
    .parameter "context"

    .prologue
    .line 142
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2d

    .line 144
    :try_start_6
    const-string v1, "com.google.android.apps.plus.views.PeopleListItemViewV11"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListItemView;
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_24} :catch_25

    .line 152
    :goto_24
    return-object v1

    .line 147
    :catch_25
    move-exception v0

    .line 148
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "PeopleListItemView"

    const-string v2, "Cannot instantiate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 152
    .end local v0           #e:Ljava/lang/Exception;
    :cond_2d
    new-instance v1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;-><init>(Landroid/content/Context;)V

    goto :goto_24
.end method

.method private updateDisplayName()V
    .registers 7

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/PeopleListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v5, Lcom/google/android/apps/plus/views/PeopleListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 400
    :goto_13
    return-void

    .line 397
    :cond_14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmailMode:Z

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_13
.end method


# virtual methods
.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    .prologue
    const/4 v9, 0x0

    .line 967
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmailMode:Z

    if-eqz v6, :cond_6b

    .line 968
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 969
    .local v5, width:I
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 970
    .local v1, height:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v5

    div-int/lit8 v7, v7, 0x2

    add-int v2, v6, v7

    .line 971
    .local v2, left:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v1

    div-int/lit8 v7, v7, 0x2

    add-int v3, v6, v7

    .line 972
    .local v3, top:I
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    add-int v7, v2, v5

    add-int v8, v3, v1

    invoke-virtual {v6, v2, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 973
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1007
    .end local v1           #height:I
    .end local v2           #left:I
    .end local v3           #top:I
    .end local v5           #width:I
    :cond_35
    :goto_35
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-nez v6, :cond_45

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-nez v6, :cond_45

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-nez v6, :cond_45

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-eqz v6, :cond_67

    .line 1009
    :cond_45
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v6, :cond_10e

    iget v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    add-int v4, v6, v7

    .line 1011
    .local v4, verticalDividerTop:I
    :goto_4f
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    iget v9, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getHeight()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    sub-int/2addr v9, v10

    invoke-virtual {v6, v7, v4, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1014
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1016
    .end local v4           #verticalDividerTop:I
    :cond_67
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/CheckableListItemView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1017
    return-void

    .line 974
    :cond_6b
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSmsMode:Z

    if-eqz v6, :cond_a0

    .line 975
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 976
    .restart local v5       #width:I
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 977
    .restart local v1       #height:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v5

    div-int/lit8 v7, v7, 0x2

    add-int v2, v6, v7

    .line 978
    .restart local v2       #left:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v1

    div-int/lit8 v7, v7, 0x2

    add-int v3, v6, v7

    .line 979
    .restart local v3       #top:I
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    add-int v7, v2, v5

    add-int v8, v3, v1

    invoke-virtual {v6, v2, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 980
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_35

    .line 981
    .end local v1           #height:I
    .end local v2           #left:I
    .end local v3           #top:I
    .end local v5           #width:I
    :cond_a0
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    if-eqz v6, :cond_35

    .line 982
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    if-eqz v6, :cond_ad

    .line 983
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 986
    :cond_ad
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v6, :cond_de

    .line 987
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarInvalidated:Z

    if-eqz v6, :cond_c2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    if-eqz v6, :cond_c2

    .line 988
    iput-boolean v9, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarInvalidated:Z

    .line 989
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-virtual {v6, p0, v7}, Lcom/google/android/apps/plus/service/ImageCache;->refreshImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 992
    :cond_c2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_10b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 993
    .local v0, bitmap:Landroid/graphics/Bitmap;
    :goto_c8
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v6, v9, v9, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 994
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 997
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :cond_de
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    if-eqz v6, :cond_35

    .line 998
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 999
    .restart local v5       #width:I
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1000
    .restart local v1       #height:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLeft()I

    move-result v6

    sub-int/2addr v6, v5

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingLeft:I

    sub-int v2, v6, v7

    .line 1001
    .restart local v2       #left:I
    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingTop:I

    .line 1002
    .restart local v3       #top:I
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    add-int v7, v2, v5

    add-int v8, v3, v1

    invoke-virtual {v6, v2, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1003
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_35

    .line 992
    .end local v1           #height:I
    .end local v2           #left:I
    .end local v3           #top:I
    .end local v5           #width:I
    :cond_10b
    sget-object v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    goto :goto_c8

    .line 1009
    :cond_10e
    iget v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    goto/16 :goto_4f
.end method

.method protected final drawBackground(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .registers 7
    .parameter "canvas"
    .parameter "background"

    .prologue
    const/4 v1, 0x0

    .line 1033
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v2, :cond_16

    iget v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    .line 1034
    .local v0, top:I
    :goto_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getHeight()I

    move-result v3

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1035
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1036
    return-void

    .end local v0           #top:I
    :cond_16
    move v0, v1

    .line 1033
    goto :goto_7
.end method

.method public getContactName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getGaiaId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public getWellFormedEmail()Ljava/lang/String;
    .registers 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getWellFormedSms()Ljava/lang/String;
    .registers 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSms:Ljava/lang/String;

    return-object v0
.end method

.method public isPlusPage()Z
    .registers 2

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPlusPage:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 704
    invoke-super {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->onAttachedToWindow()V

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 706
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 370
    if-eqz p1, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 371
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarInvalidated:Z

    .line 372
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->invalidate()V

    .line 374
    :cond_10
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    if-eqz v0, :cond_e

    .line 1044
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_f

    .line 1045
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    .line 1054
    :cond_e
    :goto_e
    return-void

    .line 1046
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1a

    .line 1047
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    goto :goto_e

    .line 1048
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_25

    .line 1049
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x2

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    goto :goto_e

    .line 1050
    :cond_25
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-ne p1, v0, :cond_e

    .line 1051
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x3

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    goto :goto_e
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 713
    invoke-super {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->onDetachedFromWindow()V

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 715
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 38
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 832
    sub-int v14, p5, p3

    .line 833
    .local v14, height:I
    const/16 v23, 0x0

    .line 834
    .local v23, topBound:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_29

    .line 835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    sub-int v30, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    move/from16 v31, v0

    invoke-virtual/range {v27 .. v31}, Lcom/google/android/apps/plus/views/SectionHeaderView;->layout(IIII)V

    .line 836
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    move/from16 v27, v0

    add-int/lit8 v23, v27, 0x0

    .line 839
    :cond_29
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingLeft:I

    move/from16 v16, v0

    .line 841
    .local v16, leftBound:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_ad

    .line 842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move/from16 v0, v16

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    sub-int v28, v14, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v29, v0

    sub-int v28, v28, v29

    div-int/lit8 v28, v28, 0x2

    add-int v28, v28, v23

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 847
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenImageAndText:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v16, v16, v27

    .line 850
    :cond_ad
    sub-int v27, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingRight:I

    move/from16 v28, v0

    sub-int v20, v27, v28

    .line 852
    .local v20, rightBound:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_ec

    .line 853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    .line 854
    .local v6, actionButtonWidth:I
    sub-int v5, v20, v6

    .line 855
    .local v5, actionButtonLeft:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v27, v0

    sub-int v27, v5, v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    .line 856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v5, v6

    move-object/from16 v0, v27

    move/from16 v1, v23

    move/from16 v2, v28

    invoke-virtual {v0, v5, v1, v2, v14}, Landroid/widget/TextView;->layout(IIII)V

    .line 858
    sub-int v20, v20, v6

    .line 861
    .end local v5           #actionButtonLeft:I
    .end local v6           #actionButtonWidth:I
    :cond_ec
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_127

    .line 862
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v26

    .line 863
    .local v26, unblockButtonWidth:I
    sub-int v27, v20, v26

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    .line 864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v23

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v14}, Landroid/widget/ImageView;->layout(IIII)V

    .line 866
    sub-int v20, v20, v26

    .line 869
    .end local v26           #unblockButtonWidth:I
    :cond_127
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_162

    .line 870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v19

    .line 871
    .local v19, removeButtonWidth:I
    sub-int v27, v20, v19

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    .line 872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v23

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v14}, Landroid/widget/ImageView;->layout(IIII)V

    .line 874
    sub-int v20, v20, v19

    .line 877
    .end local v19           #removeButtonWidth:I
    :cond_162
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_1a5

    .line 878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    .line 880
    .local v8, addButtonWidth:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_2b8

    .line 881
    sub-int v27, v20, v8

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    .line 882
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v28, v0

    add-int v7, v27, v28

    .line 886
    .local v7, addButtonLeft:I
    :goto_192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    add-int v28, v7, v8

    move-object/from16 v0, v27

    move/from16 v1, v23

    move/from16 v2, v28

    invoke-virtual {v0, v7, v1, v2, v14}, Landroid/widget/ImageView;->layout(IIII)V

    .line 887
    sub-int v20, v20, v8

    .line 890
    .end local v7           #addButtonLeft:I
    .end local v8           #addButtonWidth:I
    :cond_1a5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_1e0

    .line 891
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v11

    .line 892
    .local v11, checkboxWidth:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v9

    .line 893
    .local v9, checkboxHeight:I
    sub-int v27, v14, v23

    sub-int v27, v27, v9

    div-int/lit8 v27, v27, 0x2

    add-int v10, v23, v27

    .line 894
    .local v10, checkboxTop:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v27, v0

    sub-int v28, v20, v11

    add-int v29, v10, v9

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v20

    move/from16 v3, v29

    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/widget/CheckBox;->layout(IIII)V

    .line 896
    sub-int v20, v20, v11

    .line 899
    .end local v9           #checkboxHeight:I
    .end local v10           #checkboxTop:I
    .end local v11           #checkboxWidth:I
    :cond_1e0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_208

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_208

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_208

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_208

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_210

    .line 901
    :cond_208
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenTextAndButton:I

    move/from16 v27, v0

    sub-int v20, v20, v27

    .line 904
    :cond_210
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_2c2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v25

    .line 905
    .local v25, typeTextWidth:I
    :goto_222
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    move/from16 v27, v0

    if-nez v27, :cond_2c6

    .line 906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    .line 907
    .local v17, nameTextHeight:I
    sub-int v27, v14, v23

    sub-int v27, v27, v17

    div-int/lit8 v27, v27, 0x2

    add-int v22, v23, v27

    .line 908
    .local v22, textTop:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_265

    .line 909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    sub-int v28, v20, v25

    add-int v29, v22, v17

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v22

    move/from16 v3, v20

    move/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 911
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    move/from16 v27, v0

    add-int v27, v27, v25

    sub-int v20, v20, v27

    .line 914
    :cond_265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v22, v17

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v22

    move/from16 v3, v20

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 956
    .end local v22           #textTop:I
    :goto_27a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_2b7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmailMode:Z

    move/from16 v27, v0

    if-nez v27, :cond_2b7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSmsMode:Z

    move/from16 v27, v0

    if-nez v27, :cond_2b7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v27, v0

    if-nez v27, :cond_2b7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    move-object/from16 v27, v0

    if-eqz v27, :cond_2b7

    .line 958
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/service/ImageCache;->loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 960
    :cond_2b7
    return-void

    .line 884
    .end local v17           #nameTextHeight:I
    .end local v25           #typeTextWidth:I
    .restart local v8       #addButtonWidth:I
    :cond_2b8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v27, v0

    sub-int v7, v27, v8

    .restart local v7       #addButtonLeft:I
    goto/16 :goto_192

    .line 904
    .end local v7           #addButtonLeft:I
    .end local v8           #addButtonWidth:I
    :cond_2c2
    const/16 v25, 0x0

    goto/16 :goto_222

    .line 915
    .restart local v25       #typeTextWidth:I
    :cond_2c6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_3d6

    .line 916
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    .line 917
    .restart local v17       #nameTextHeight:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v12

    .line 919
    .local v12, circleTextHeight:I
    move/from16 v24, v12

    .line 920
    .local v24, totalHeight:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_2fa

    .line 921
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v27, v0

    move/from16 v0, v27

    move/from16 v1, v24

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v24

    .line 923
    :cond_2fa
    add-int v24, v24, v17

    .line 925
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    move/from16 v27, v0

    sub-int v27, v27, v24

    div-int/lit8 v27, v27, 0x2

    add-int v23, v23, v27

    .line 927
    move/from16 v18, v16

    .line 928
    .local v18, nameTextLeft:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    move-object/from16 v27, v0

    if-eqz v27, :cond_322

    .line 929
    sget-object v27, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingLeft:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v18, v18, v27

    .line 931
    :cond_322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v23, v17

    move-object/from16 v0, v27

    move/from16 v1, v18

    move/from16 v2, v23

    move/from16 v3, v20

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 933
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenNameAndCircles:I

    move/from16 v27, v0

    add-int v27, v27, v17

    add-int v23, v23, v27

    .line 934
    move/from16 v21, v16

    .line 935
    .local v21, textLeft:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_38e

    .line 936
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    div-int/lit8 v27, v27, 0x2

    add-int v15, v23, v27

    .line 937
    .local v15, iconTop:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v28, v0

    add-int v28, v28, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v29, v0

    add-int v29, v29, v15

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 939
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v21, v21, v27

    .line 942
    .end local v15           #iconTop:I
    :cond_38e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    move/from16 v27, v0

    sub-int v27, v27, v12

    div-int/lit8 v27, v27, 0x2

    add-int v13, v23, v27

    .line 943
    .local v13, circleTextTop:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_3c1

    .line 944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    sub-int v28, v20, v25

    add-int v29, v13, v12

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v20

    move/from16 v3, v29

    invoke-virtual {v0, v1, v13, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 946
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    move/from16 v27, v0

    add-int v27, v27, v25

    sub-int v20, v20, v27

    .line 948
    :cond_3c1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v13, v12

    move-object/from16 v0, v27

    move/from16 v1, v21

    move/from16 v2, v20

    move/from16 v3, v28

    invoke-virtual {v0, v1, v13, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_27a

    .line 951
    .end local v12           #circleTextHeight:I
    .end local v13           #circleTextTop:I
    .end local v17           #nameTextHeight:I
    .end local v18           #nameTextLeft:I
    .end local v21           #textLeft:I
    .end local v24           #totalHeight:I
    :cond_3d6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    .line 952
    .restart local v17       #nameTextHeight:I
    sub-int v27, v14, v23

    sub-int v27, v27, v17

    div-int/lit8 v27, v27, 0x2

    add-int v22, v23, v27

    .line 953
    .restart local v22       #textTop:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v22, v17

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v22

    move/from16 v3, v20

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_27a
.end method

.method protected onMeasure(II)V
    .registers 15
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v11, 0x4000

    const/4 v10, 0x0

    .line 722
    invoke-static {v10, p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->resolveSize(II)I

    move-result v6

    .line 723
    .local v6, width:I
    const/4 v3, 0x0

    .line 725
    .local v3, height:I
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingLeft:I

    sub-int v7, v6, v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingRight:I

    sub-int v5, v7, v8

    .line 727
    .local v5, textWidth:I
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v7, :cond_1a

    .line 728
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenImageAndText:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    .line 731
    :cond_1a
    const/4 v1, 0x0

    .line 732
    .local v1, actionButtonWidth:I
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-eqz v7, :cond_38

    .line 733
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v7, v10, p2}, Landroid/widget/TextView;->measure(II)V

    .line 734
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    .line 735
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 736
    .local v0, actionButtonHeight:I
    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 737
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v1

    sub-int/2addr v5, v7

    .line 740
    .end local v0           #actionButtonHeight:I
    :cond_38
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-eqz v7, :cond_57

    .line 741
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/ImageView;->measure(II)V

    .line 744
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 745
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    .line 748
    :cond_57
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-eqz v7, :cond_76

    .line 749
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/ImageView;->measure(II)V

    .line 752
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 753
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    .line 756
    :cond_76
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-eqz v7, :cond_95

    .line 757
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/ImageView;->measure(II)V

    .line 760
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 761
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    .line 764
    :cond_95
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    if-eqz v7, :cond_ae

    .line 765
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v10, p2}, Landroid/widget/CheckBox;->measure(II)V

    .line 766
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    .line 767
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v5, v7

    .line 770
    :cond_ae
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-nez v7, :cond_c2

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-nez v7, :cond_c2

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-nez v7, :cond_c2

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-nez v7, :cond_c2

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    if-eqz v7, :cond_c5

    .line 772
    :cond_c2
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenTextAndButton:I

    sub-int/2addr v5, v7

    .line 775
    :cond_c5
    move v4, v5

    .line 776
    .local v4, nameTextWidth:I
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    if-eqz v7, :cond_d4

    .line 777
    sget-object v7, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    add-int/2addr v7, v8

    sub-int/2addr v4, v7

    .line 779
    :cond_d4
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-static {v4, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/TextView;->measure(II)V

    .line 783
    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    .line 785
    move v2, v5

    .line 787
    .local v2, circleTextWidth:I
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    if-eqz v7, :cond_ea

    .line 788
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    add-int/2addr v7, v8

    sub-int/2addr v2, v7

    .line 791
    :cond_ea
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    if-eqz v7, :cond_fd

    .line 792
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v10, v10}, Landroid/widget/TextView;->measure(II)V

    .line 793
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    add-int/2addr v7, v8

    sub-int/2addr v2, v7

    .line 796
    :cond_fd
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    if-eqz v7, :cond_13d

    .line 797
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v10, v10}, Landroid/widget/TextView;->measure(II)V

    .line 799
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 800
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    .line 802
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->measure(II)V

    .line 806
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenNameAndCircles:I

    add-int/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    add-int/2addr v7, v8

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 810
    :cond_13d
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingTop:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingBottom:I

    add-int/2addr v7, v8

    add-int/2addr v3, v7

    .line 811
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 813
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-eqz v7, :cond_15a

    .line 814
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-static {v1, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->measure(II)V

    .line 819
    :cond_15a
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v7, :cond_16e

    .line 820
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {v7, p1, v10}, Lcom/google/android/apps/plus/views/SectionHeaderView;->measure(II)V

    .line 821
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/SectionHeaderView;->getMeasuredHeight()I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    .line 822
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    add-int/2addr v3, v7

    .line 824
    :cond_16e
    invoke-virtual {p0, v6, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setMeasuredDimension(II)V

    .line 825
    return-void
.end method

.method public onRecycle()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1082
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPersonId:Ljava/lang/String;

    .line 1083
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    .line 1084
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    .line 1085
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    .line 1086
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 1087
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    .line 1088
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPlusPage:Z

    .line 1089
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    .line 1090
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmailMode:Z

    .line 1091
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    .line 1092
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSmsMode:Z

    .line 1093
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSms:Ljava/lang/String;

    .line 1094
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    .line 1095
    return-void
.end method

.method public setActionButtonLabel(I)V
    .registers 7
    .parameter "labelResId"

    .prologue
    const/4 v4, 0x0

    .line 615
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-nez v1, :cond_31

    .line 616
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    .line 617
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 618
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 619
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 620
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 621
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    .line 624
    :cond_31
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 625
    .local v0, label:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 626
    return-void
.end method

.method public setActionButtonVisible(Z)V
    .registers 4
    .parameter "visible"

    .prologue
    .line 632
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-ne v0, p1, :cond_5

    .line 645
    :cond_4
    :goto_4
    return-void

    .line 636
    :cond_5
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    .line 637
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-eqz v0, :cond_1c

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-nez v0, :cond_15

    .line 639
    const v0, 0x7f0801c9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setActionButtonLabel(I)V

    .line 641
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 642
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method

.method public setAddButtonVisible(Z)V
    .registers 6
    .parameter "visible"

    .prologue
    const/4 v3, 0x0

    .line 535
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-ne v0, p1, :cond_6

    .line 555
    :cond_5
    :goto_5
    return-void

    .line 539
    :cond_6
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    .line 540
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-eqz v0, :cond_6e

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    if-nez v0, :cond_68

    .line 542
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sAddButtonIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_47

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02009d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sAddButtonIcon:Landroid/graphics/drawable/Drawable;

    :cond_47
    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sAddButtonIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080209

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    .line 551
    :cond_68
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 552
    :cond_6e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5
.end method

.method public setAvatarVisible(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 362
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    .line 363
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;Z)V
    .registers 3
    .parameter "bitmap"
    .parameter "loading"

    .prologue
    .line 1024
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 1025
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->invalidate()V

    .line 1026
    return-void
.end method

.method public setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .registers 2
    .parameter "circleNames"

    .prologue
    .line 445
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 446
    return-void
.end method

.method public setContactIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "gaiaId"
    .parameter "lookupKey"
    .parameter "avatarUrl"

    .prologue
    const/4 v4, 0x0

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_26

    :cond_11
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v0, :cond_26

    .line 346
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    .line 347
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    .line 348
    iput-object p3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarUrl:Ljava/lang/String;

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    if-eqz v0, :cond_27

    .line 350
    iput-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 355
    :goto_21
    iput-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->requestLayout()V

    .line 358
    :cond_26
    return-void

    .line 352
    :cond_27
    new-instance v0, Lcom/google/android/apps/plus/content/AvatarRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarUrl:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    goto :goto_21
.end method

.method public setContactName(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    .prologue
    .line 385
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    .line 386
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateDisplayName()V

    .line 387
    return-void
.end method

.method public setFirstRow(Z)V
    .registers 4
    .parameter "firstRow"

    .prologue
    .line 688
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    .line 689
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    if-eqz v0, :cond_d

    const/4 v0, 0x0

    :goto_9
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 690
    return-void

    .line 689
    :cond_d
    const/16 v0, 0x8

    goto :goto_9
.end method

.method public setGaiaId(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method public setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "gaiaId"
    .parameter "avatarUrl"

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1d

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v0, :cond_1d

    .line 331
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    .line 332
    new-instance v0, Lcom/google/android/apps/plus/content/AvatarRequest;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 333
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 334
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->requestLayout()V

    .line 336
    :cond_1d
    return-void
.end method

.method public setHighlightedText(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    .prologue
    .line 377
    if-nez p1, :cond_6

    .line 378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    .line 382
    :goto_5
    return-void

    .line 380
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    goto :goto_5
.end method

.method public setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 696
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    .line 697
    return-void
.end method

.method public setPackedCircleIds(Ljava/lang/String;)V
    .registers 5
    .parameter "packedCircleIds"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 449
    if-eqz p1, :cond_1b

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    .line 450
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1d

    :goto_d
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    return-void

    :cond_1b
    move v0, v2

    .line 449
    goto :goto_5

    :cond_1d
    move v1, v2

    .line 450
    goto :goto_d
.end method

.method public setPackedCircleIdsAndEmailAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "packedCircleIds"
    .parameter "emailAddress"
    .parameter "matchingEmailAddress"

    .prologue
    const/4 v4, 0x0

    .line 456
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIdsEmailAddressAndPhoneNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    return-void
.end method

.method public setPackedCircleIdsEmailAddressAndPhoneNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "packedCircleIds"
    .parameter "emailAddress"
    .parameter "matchingEmailAddress"
    .parameter "phoneNumber"
    .parameter "phoneType"

    .prologue
    .line 463
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIdsEmailAddressPhoneNumberAndSnippet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    return-void
.end method

.method public setPackedCircleIdsEmailAddressPhoneNumberAndSnippet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 19
    .parameter "packedCircleIds"
    .parameter "emailAddress"
    .parameter "matchingEmailAddress"
    .parameter "phoneNumber"
    .parameter "phoneType"
    .parameter "snippet"

    .prologue
    .line 470
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    .line 471
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_84

    .line 472
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    .line 473
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    .line 474
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-static/range {p4 .. p4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForPhoneType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 476
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6b

    .line 477
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    if-nez v1, :cond_5f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    const v4, 0x1030044

    invoke-virtual {v3, v1, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextSize:F

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextColor:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    const/16 v3, 0x10

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    :cond_5f
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    .line 525
    :cond_6b
    :goto_6b
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    if-eqz v1, :cond_133

    const/4 v1, 0x0

    :goto_72
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 526
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_83

    .line 527
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    if-eqz v1, :cond_137

    const/4 v1, 0x0

    :goto_80
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 529
    :cond_83
    return-void

    .line 480
    :cond_84
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e2

    .line 481
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    .line 482
    const/4 v9, 0x0

    .line 483
    .local v9, circleCount:I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d0

    .line 484
    const/4 v9, 0x1

    .line 485
    const/4 v10, 0x0

    .line 487
    .local v10, offset:I
    :goto_96
    const/16 v1, 0x7c

    invoke-virtual {p1, v1, v10}, Ljava/lang/String;->indexOf(II)I

    move-result v11

    .line 488
    .local v11, separatorIndex:I
    const/4 v1, -0x1

    if-eq v11, v1, :cond_a4

    .line 489
    add-int/lit8 v9, v9, 0x1

    .line 492
    add-int/lit8 v10, v11, 0x1

    .line 493
    goto :goto_96

    .line 494
    :cond_a4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0e0015

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-virtual {v1, v3, v9, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 497
    .local v2, text:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/views/PeopleListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 500
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    goto :goto_6b

    .line 502
    .end local v2           #text:Ljava/lang/String;
    .end local v10           #offset:I
    .end local v11           #separatorIndex:I
    :cond_d0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    sget-object v7, Lcom/google/android/apps/plus/views/PeopleListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v8, Lcom/google/android/apps/plus/views/PeopleListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    move-object v4, p3

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 504
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    goto :goto_6b

    .line 506
    .end local v9           #circleCount:I
    :cond_e2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_fb

    .line 507
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    .line 508
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    .line 509
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6b

    .line 511
    :cond_fb
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10e

    .line 512
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    .line 513
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    .line 514
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6b

    .line 515
    :cond_10e
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_125

    .line 516
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    .line 517
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    .line 518
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-static/range {p6 .. p6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6b

    .line 520
    :cond_125
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    .line 521
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    .line 522
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6b

    .line 525
    :cond_133
    const/16 v1, 0x8

    goto/16 :goto_72

    .line 527
    :cond_137
    const/16 v1, 0x8

    goto/16 :goto_80
.end method

.method public setPersonId(Ljava/lang/String;)V
    .registers 2
    .parameter "personId"

    .prologue
    .line 317
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPersonId:Ljava/lang/String;

    .line 318
    return-void
.end method

.method public setPlusPage(Z)V
    .registers 2
    .parameter "plusPage"

    .prologue
    .line 407
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPlusPage:Z

    .line 408
    return-void
.end method

.method public setRemoveButtonVisible(Z)V
    .registers 6
    .parameter "visible"

    .prologue
    const/4 v3, 0x0

    .line 561
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-ne v0, p1, :cond_6

    .line 582
    :cond_5
    :goto_5
    return-void

    .line 565
    :cond_6
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    .line 566
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-eqz v0, :cond_6e

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    if-nez v0, :cond_68

    .line 568
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    .line 569
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_47

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02009e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

    :cond_47
    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 574
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08020b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    .line 578
    :cond_68
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 579
    :cond_6e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5
.end method

.method public setSectionHeader(C)V
    .registers 4
    .parameter "firstLetter"

    .prologue
    .line 679
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderVisible(Z)V

    .line 680
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setText(Ljava/lang/CharSequence;)V

    .line 681
    return-void
.end method

.method protected setSectionHeaderBackgroundColor()V
    .registers 4

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setBackgroundColor(I)V

    .line 673
    return-void
.end method

.method public setSectionHeaderVisible(Z)V
    .registers 5
    .parameter "flag"

    .prologue
    const/4 v2, 0x0

    .line 651
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    .line 652
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v1, :cond_2d

    .line 653
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    if-nez v1, :cond_27

    .line 654
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 655
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x7f0300af

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SectionHeaderView;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 657
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderBackgroundColor()V

    .line 658
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    .line 665
    .end local v0           #inflater:Landroid/view/LayoutInflater;
    :cond_26
    :goto_26
    return-void

    .line 660
    :cond_27
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setVisibility(I)V

    goto :goto_26

    .line 662
    :cond_2d
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    if-eqz v1, :cond_26

    .line 663
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setVisibility(I)V

    goto :goto_26
.end method

.method public setUnblockButtonVisible(Z)V
    .registers 6
    .parameter "visible"

    .prologue
    const/4 v3, 0x0

    .line 588
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-ne v0, p1, :cond_6

    .line 609
    :cond_5
    :goto_5
    return-void

    .line 592
    :cond_6
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    .line 593
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-eqz v0, :cond_6e

    .line 594
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    if-nez v0, :cond_68

    .line 595
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_47

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02017d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

    :cond_47
    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 601
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080254

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    .line 605
    :cond_68
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 606
    :cond_6e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5
.end method

.method public setWellFormedEmail(Ljava/lang/String;)V
    .registers 2
    .parameter "wellFormedEmail"

    .prologue
    .line 419
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    .line 420
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateDisplayName()V

    .line 421
    return-void
.end method

.method public setWellFormedSms(Ljava/lang/String;)V
    .registers 3
    .parameter "wellFormedSms"

    .prologue
    .line 432
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSmsMode:Z

    .line 433
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSms:Ljava/lang/String;

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 435
    return-void
.end method

.method public updateContentDescription()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1061
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1063
    .local v1, res:Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1064
    .local v0, circleNames:Ljava/lang/CharSequence;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    if-eqz v2, :cond_2c

    if-eqz v0, :cond_2c

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_2c

    .line 1065
    const v2, 0x7f0801de

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1075
    :cond_2b
    :goto_2b
    return-void

    .line 1068
    :cond_2c
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    if-eqz v2, :cond_41

    .line 1069
    const v2, 0x7f0801dd

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2b

    .line 1071
    :cond_41
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    if-eqz v2, :cond_2b

    .line 1072
    const v2, 0x7f0801df

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2b
.end method
