.class public Lcom/google/android/apps/plus/views/PersonCardView;
.super Landroid/view/ViewGroup;
.source "PersonCardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;
    }
.end annotation


# static fields
.field private static sAvatarPaint:Landroid/graphics/Paint;

.field private static final sBoldSpan:Landroid/text/style/StyleSpan;

.field private static sCircleIconBitmap:Landroid/graphics/Bitmap;

.field private static sCircleIconPaint:Landroid/graphics/Paint;

.field private static sColorSpan:Landroid/text/style/ForegroundColorSpan;

.field private static sDefaultAvatar:Landroid/graphics/Bitmap;

.field private static sEmailIcon:Landroid/graphics/drawable/Drawable;

.field private static sInitialized:Z


# instance fields
.field private mAction:I

.field private mActionButton:Landroid/widget/Button;

.field private mActionButtonHeight:I

.field private mActionButtonMinWidth:I

.field private mActionButtonTextColor:I

.field private mActionButtonVisible:Z

.field private mAutoWidth:Z

.field private mAvatarBitmap:Landroid/graphics/Bitmap;

.field private final mAvatarBounds:Landroid/graphics/Rect;

.field private final mAvatarBoxHeight:I

.field private final mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private mAvatarInvalidated:Z

.field private final mAvatarOriginalBounds:Landroid/graphics/Rect;

.field private mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

.field private final mAvatarSize:I

.field private mAvatarVisible:Z

.field private final mBackground:Landroid/graphics/drawable/Drawable;

.field private mCircleChangePending:Z

.field private mCircleIconPaddingTop:I

.field private mCircleIconVisible:Z

.field private mCircleIconX:I

.field private mCircleIconY:I

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

.field private final mCirclesButtonPadding:I

.field private mContactLookupKey:Ljava/lang/String;

.field private final mDescriptionTextView:Landroid/widget/TextView;

.field private mDescriptionVisible:Z

.field private mDismissButton:Landroid/widget/ImageView;

.field private mDismissButtonBackground:Landroid/graphics/drawable/Drawable;

.field private final mDismissButtonSize:I

.field private mDismissButtonVisible:Z

.field private mDisplayName:Ljava/lang/String;

.field private mEmailIconPaddingRight:I

.field private mEmailIconPaddingTop:I

.field private final mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

.field private mForSharing:Z

.field private mForceAvatarDefault:Z

.field private mGaiaId:Ljava/lang/String;

.field private final mGapBetweenAvatarAndText:I

.field private final mGapBetweenIconAndCircles:I

.field private final mGapBetweenNameAndCircles:I

.field private mHighlightedText:Ljava/lang/String;

.field private final mImageButtonMargin:I

.field private mListener:Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;

.field private final mMinHeight:I

.field private final mMinWidth:I

.field private final mNameTextBuilder:Landroid/text/SpannableStringBuilder;

.field private final mNameTextView:Landroid/widget/TextView;

.field private final mNextCardPeekWidth:I

.field private mOneClickMode:I

.field private mOptimalWidth:I

.field private final mPaddingBottom:I

.field private final mPaddingLeft:I

.field private final mPaddingRight:I

.field private final mPaddingTop:I

.field private mPersonId:Ljava/lang/String;

.field private mPlusPage:Z

.field private mPosition:I

.field private final mPreferredWidth:I

.field private final mSelector:Landroid/graphics/drawable/Drawable;

.field private mShowTooltip:Z

.field private mTooltipText:Ljava/lang/String;

.field private mWellFormedEmail:Ljava/lang/String;

.field private mWellFormedEmailMode:Z

.field private mWideLeftMargin:I

.field private mWideMargin:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 61
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/PersonCardView;->sBoldSpan:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PersonCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v6, -0x2

    .line 154
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    iput v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    .line 91
    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarVisible:Z

    .line 104
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    .line 105
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    .line 110
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    .line 111
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 157
    .local v3, resources:Landroid/content/res/Resources;
    sget-boolean v4, Lcom/google/android/apps/plus/views/PersonCardView;->sInitialized:Z

    if-nez v4, :cond_79

    .line 158
    sput-boolean v5, Lcom/google/android/apps/plus/views/PersonCardView;->sInitialized:Z

    .line 159
    invoke-static {p1, v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sDefaultAvatar:Landroid/graphics/Bitmap;

    .line 160
    const v4, 0x7f0200a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sCircleIconBitmap:Landroid/graphics/Bitmap;

    .line 162
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 163
    sput-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 165
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    sput-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sCircleIconPaint:Landroid/graphics/Paint;

    .line 167
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    const v5, 0x7f0a0027

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    .line 169
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0201a1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sEmailIcon:Landroid/graphics/drawable/Drawable;

    .line 173
    :cond_79
    sget-object v4, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 174
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonBackground:Landroid/graphics/drawable/Drawable;

    .line 175
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 177
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 179
    const v4, 0x7f020029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 181
    const v4, 0x7f0201c8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 182
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 184
    const v4, 0x7f0d01a4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPreferredWidth:I

    .line 185
    const v4, 0x7f0d01a5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mMinWidth:I

    .line 186
    const v4, 0x7f0d01a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mMinHeight:I

    .line 187
    const v4, 0x7f0d01a9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingTop:I

    .line 188
    const v4, 0x7f0d01a8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingLeft:I

    .line 189
    const v4, 0x7f0d01aa

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingRight:I

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingBottom:I

    .line 191
    const v4, 0x7f0d01bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideLeftMargin:I

    .line 192
    const v4, 0x7f0d01ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarSize:I

    .line 193
    const v4, 0x7f0d01ac

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBoxHeight:I

    .line 195
    const v4, 0x7f0d01ad

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenAvatarAndText:I

    .line 197
    const v4, 0x7f0d01b4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonMinWidth:I

    .line 199
    const v4, 0x7f0d01b3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonHeight:I

    .line 201
    const v4, 0x7f0a0106

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonTextColor:I

    .line 202
    const v4, 0x7f0d01b0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconPaddingTop:I

    .line 204
    const v4, 0x7f0d01b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenIconAndCircles:I

    .line 206
    const v4, 0x7f0d01b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenNameAndCircles:I

    .line 208
    const v4, 0x7f0d01b6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mImageButtonMargin:I

    .line 210
    const v4, 0x7f0d01b5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonSize:I

    .line 212
    const v4, 0x7f0d01b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButtonPadding:I

    .line 214
    const v4, 0x7f0d01bb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNextCardPeekWidth:I

    .line 216
    const v4, 0x7f0d01bd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mEmailIconPaddingTop:I

    .line 218
    const v4, 0x7f0d01be

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mEmailIconPaddingRight:I

    .line 221
    const v4, 0x7f0d01ae

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 222
    .local v2, nameTextSize:F
    const v4, 0x7f0d01af

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 224
    .local v1, circleTextSize:F
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    .line 225
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 226
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 227
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    const v5, 0x1030044

    invoke-virtual {v4, p1, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 228
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v7, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 229
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 232
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->addView(Landroid/view/View;)V

    .line 234
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    .line 235
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 236
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    const/16 v5, 0x30

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 237
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 238
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 239
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    const v5, 0x7f0a0107

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 241
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->addView(Landroid/view/View;)V

    .line 245
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/PersonCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/PersonCardView;IIII)V
    .registers 11
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f09004f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mTooltipText:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    add-int v2, p2, p4

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    invoke-virtual {v0, v4, v4}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    const/16 v3, 0x31

    div-int/lit8 v4, p3, 0x2

    add-int/2addr v4, p1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v4, v1

    invoke-virtual {v0, p0, v3, v1, v2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    return-void
.end method

.method private addCirclesButton()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 495
    new-instance v0, Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const v1, 0x7f02019c

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setBackgroundResource(I)V

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    iget v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButtonPadding:I

    iget v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButtonPadding:I

    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/google/android/apps/plus/views/CirclesButton;->setPadding(IIII)V

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->addView(Landroid/view/View;)V

    .line 500
    return-void
.end method


# virtual methods
.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    .prologue
    const/4 v10, 0x0

    .line 789
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 791
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    if-eqz v6, :cond_17

    .line 792
    sget-object v6, Lcom/google/android/apps/plus/views/PersonCardView;->sCircleIconBitmap:Landroid/graphics/Bitmap;

    iget v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconX:I

    int-to-float v7, v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconY:I

    int-to-float v8, v8

    sget-object v9, Lcom/google/android/apps/plus/views/PersonCardView;->sCircleIconPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 795
    :cond_17
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarVisible:Z

    if-eqz v6, :cond_4b

    .line 796
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarInvalidated:Z

    if-eqz v6, :cond_2c

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    if-eqz v6, :cond_2c

    .line 797
    iput-boolean v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarInvalidated:Z

    .line 798
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-virtual {v6, p0, v7}, Lcom/google/android/apps/plus/service/ImageCache;->refreshImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 801
    :cond_2c
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_b1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 802
    .local v0, bitmap:Landroid/graphics/Bitmap;
    :goto_32
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 803
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    .line 804
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    sget-object v8, Lcom/google/android/apps/plus/views/PersonCardView;->sAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 807
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    :cond_4b
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mContactLookupKey:Ljava/lang/String;

    if-eqz v6, :cond_7a

    .line 808
    sget-object v6, Lcom/google/android/apps/plus/views/PersonCardView;->sEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 809
    .local v5, width:I
    sget-object v6, Lcom/google/android/apps/plus/views/PersonCardView;->sEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 810
    .local v1, height:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v5

    iget v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mEmailIconPaddingRight:I

    add-int v2, v6, v7

    .line 811
    .local v2, left:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mEmailIconPaddingTop:I

    add-int v4, v6, v7

    .line 812
    .local v4, top:I
    sget-object v6, Lcom/google/android/apps/plus/views/PersonCardView;->sEmailIcon:Landroid/graphics/drawable/Drawable;

    add-int v7, v2, v5

    add-int v8, v4, v1

    invoke-virtual {v6, v2, v4, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 813
    sget-object v6, Lcom/google/android/apps/plus/views/PersonCardView;->sEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 817
    .end local v1           #height:I
    .end local v2           #left:I
    .end local v4           #top:I
    .end local v5           #width:I
    :cond_7a
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 819
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->isPressed()Z

    move-result v6

    if-nez v6, :cond_89

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->isFocused()Z

    move-result v6

    if-eqz v6, :cond_8e

    .line 820
    :cond_89
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 823
    :cond_8e
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mShowTooltip:Z

    if-eqz v6, :cond_b0

    .line 824
    iput-boolean v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mShowTooltip:Z

    .line 825
    const/4 v6, 0x2

    new-array v3, v6, [I

    .line 826
    .local v3, screenPos:[I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/plus/views/CirclesButton;->getLocationOnScreen([I)V

    .line 828
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CirclesButton;->getWidth()I

    move-result v5

    .line 829
    .restart local v5       #width:I
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CirclesButton;->getHeight()I

    move-result v1

    .line 831
    .restart local v1       #height:I
    new-instance v6, Lcom/google/android/apps/plus/views/PersonCardView$1;

    invoke-direct {v6, p0, v3, v5, v1}, Lcom/google/android/apps/plus/views/PersonCardView$1;-><init>(Lcom/google/android/apps/plus/views/PersonCardView;[III)V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/views/PersonCardView;->post(Ljava/lang/Runnable;)Z

    .line 839
    .end local v1           #height:I
    .end local v3           #screenPos:[I
    .end local v5           #width:I
    :cond_b0
    return-void

    .line 801
    :cond_b1
    sget-object v0, Lcom/google/android/apps/plus/views/PersonCardView;->sDefaultAvatar:Landroid/graphics/Bitmap;

    goto/16 :goto_32
.end method

.method protected drawableStateChanged()V
    .registers 3

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 858
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->invalidate()V

    .line 860
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 861
    return-void
.end method

.method public final getContactName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPersonId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPosition()I
    .registers 2

    .prologue
    .line 260
    iget v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPosition:I

    return v0
.end method

.method public final getWellFormedEmail()Ljava/lang/String;
    .registers 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWellFormedEmail:Ljava/lang/String;

    return-object v0
.end method

.method public final isForSharing()Z
    .registers 2

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mForSharing:Z

    return v0
.end method

.method public final isOneClickAdd()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 331
    iget v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 579
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 581
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 346
    if-eqz p1, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarInvalidated:Z

    .line 348
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->invalidate()V

    .line 350
    :cond_10
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    .line 878
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleChangePending:Z

    if-eqz v0, :cond_5

    .line 893
    :cond_4
    :goto_4
    return-void

    .line 882
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mListener:Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;

    if-eqz v0, :cond_4

    .line 883
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_15

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mListener:Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAction:I

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PersonCardView;I)V

    goto :goto_4

    .line 885
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    if-ne p1, v0, :cond_1f

    .line 886
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mListener:Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;->onChangeCircles(Lcom/google/android/apps/plus/views/PersonCardView;)V

    goto :goto_4

    .line 887
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_29

    .line 888
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mListener:Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;->onDismissButtonClick(Lcom/google/android/apps/plus/views/PersonCardView;)V

    goto :goto_4

    .line 890
    :cond_29
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mListener:Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;->onItemClick(Lcom/google/android/apps/plus/views/PersonCardView;)V

    goto :goto_4
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 588
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 590
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 31
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 695
    const/4 v10, 0x0

    .line 696
    .local v10, leftMargin:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideMargin:Z

    move/from16 v20, v0

    if-eqz v20, :cond_d

    .line 697
    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideLeftMargin:I

    .line 699
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sub-int v22, p4, p2

    sub-int v23, p5, p3

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 700
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mSelector:Landroid/graphics/drawable/Drawable;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sub-int v22, p4, p2

    sub-int v23, p5, p3

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 702
    sub-int v19, p4, p2

    .line 703
    .local v19, width:I
    sub-int v8, p5, p3

    .line 704
    .local v8, height:I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingTop:I

    .line 706
    .local v13, topBound:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingLeft:I

    move/from16 v20, v0

    add-int v9, v20, v10

    .line 707
    .local v9, leftBound:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingRight:I

    move/from16 v20, v0

    sub-int v12, v19, v20

    .line 709
    .local v12, rightBound:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_98

    .line 710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v7

    .line 711
    .local v7, dismissWidth:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v6

    .line 712
    .local v6, dismissHeight:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mImageButtonMargin:I

    move/from16 v21, v0

    sub-int v21, v19, v21

    sub-int v21, v21, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mImageButtonMargin:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mImageButtonMargin:I

    move/from16 v23, v0

    sub-int v23, v19, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mImageButtonMargin:I

    move/from16 v24, v0

    add-int v24, v24, v6

    invoke-virtual/range {v20 .. v24}, Landroid/widget/ImageView;->layout(IIII)V

    .line 717
    .end local v6           #dismissHeight:I
    .end local v7           #dismissWidth:I
    :cond_98
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_116

    .line 718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput v9, v0, Landroid/graphics/Rect;->left:I

    .line 719
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarSize:I

    move/from16 v22, v0

    add-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 720
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBoxHeight:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarSize:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 721
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarSize:I

    move/from16 v22, v0

    add-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 723
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarSize:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenAvatarAndText:I

    move/from16 v21, v0

    add-int v20, v20, v21

    add-int v9, v9, v20

    .line 726
    :cond_116
    const/4 v14, 0x0

    .line 727
    .local v14, widget:Landroid/view/View;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    move/from16 v20, v0

    packed-switch v20, :pswitch_data_25a

    .line 742
    :cond_120
    :goto_120
    :pswitch_120
    if-eqz v14, :cond_145

    .line 743
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v18

    .line 744
    .local v18, widgetWidth:I
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    .line 745
    .local v15, widgetHeight:I
    move/from16 v16, v9

    .line 746
    .local v16, widgetLeft:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingBottom:I

    move/from16 v20, v0

    sub-int v20, v8, v20

    sub-int v17, v20, v15

    .line 747
    .local v17, widgetTop:I
    add-int v20, v16, v18

    add-int v21, v17, v15

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 751
    .end local v15           #widgetHeight:I
    .end local v16           #widgetLeft:I
    .end local v17           #widgetTop:I
    .end local v18           #widgetWidth:I
    :cond_145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    .line 752
    .local v11, nameTextHeight:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    add-int v21, v13, v11

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v9, v13, v12, v1}, Landroid/widget/TextView;->layout(IIII)V

    .line 754
    move v4, v9

    .line 755
    .local v4, circleTextLeft:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingTop:I

    move/from16 v20, v0

    add-int v20, v20, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenNameAndCircles:I

    move/from16 v21, v0

    add-int v5, v20, v21

    .line 757
    .local v5, circleTextTop:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_187

    .line 758
    sget-object v20, Lcom/google/android/apps/plus/views/PersonCardView;->sCircleIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenIconAndCircles:I

    move/from16 v21, v0

    add-int v20, v20, v21

    add-int v4, v4, v20

    .line 761
    :cond_187
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1b6

    .line 762
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v21

    add-int v21, v21, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v22

    add-int v22, v22, v5

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v4, v5, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 767
    :cond_1b6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1ec

    .line 768
    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconX:I

    .line 769
    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconY:I

    .line 770
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getLineCount()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_236

    .line 771
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconY:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconPaddingTop:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconY:I

    .line 778
    :cond_1ec
    :goto_1ec
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_221

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    if-nez v20, :cond_221

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    move-object/from16 v20, v0

    if-eqz v20, :cond_221

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mWellFormedEmailMode:Z

    move/from16 v20, v0

    if-nez v20, :cond_221

    .line 780
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/service/ImageCache;->loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 782
    :cond_221
    return-void

    .line 729
    .end local v4           #circleTextLeft:I
    .end local v5           #circleTextTop:I
    .end local v11           #nameTextHeight:I
    :pswitch_222
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonVisible:Z

    move/from16 v20, v0

    if-eqz v20, :cond_120

    .line 730
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    goto/16 :goto_120

    .line 737
    :pswitch_230
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    goto/16 :goto_120

    .line 773
    .restart local v4       #circleTextLeft:I
    .restart local v5       #circleTextTop:I
    .restart local v11       #nameTextHeight:I
    :cond_236
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconY:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v21

    sget-object v22, Lcom/google/android/apps/plus/views/PersonCardView;->sCircleIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    sub-int v21, v21, v22

    div-int/lit8 v21, v21, 0x2

    add-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconY:I

    goto :goto_1ec

    .line 727
    nop

    :pswitch_data_25a
    .packed-switch 0x0
        :pswitch_222
        :pswitch_230
        :pswitch_120
        :pswitch_230
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 598
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAutoWidth:Z

    if-eqz v9, :cond_117

    .line 599
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOptimalWidth:I

    if-nez v9, :cond_35

    .line 600
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNextCardPeekWidth:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPreferredWidth:I

    add-int/2addr v10, v9

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPreferredWidth:I

    div-int/2addr v10, v11

    const/4 v11, 0x1

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    div-int/2addr v9, v10

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mMinWidth:I

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPreferredWidth:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    iput v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOptimalWidth:I

    .line 602
    :cond_35
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOptimalWidth:I

    invoke-static {v9, p1}, Lcom/google/android/apps/plus/views/PersonCardView;->resolveSize(II)I

    move-result v8

    .line 606
    .local v8, width:I
    :goto_3b
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mMinHeight:I

    invoke-static {v9, p2}, Lcom/google/android/apps/plus/views/PersonCardView;->resolveSize(II)I

    move-result v1

    .line 608
    .local v1, height:I
    const/4 v2, 0x0

    .line 609
    .local v2, leftMargin:I
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideMargin:Z

    if-eqz v9, :cond_4b

    .line 610
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideLeftMargin:I

    add-int/2addr v8, v9

    .line 611
    iget v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideLeftMargin:I

    .line 614
    :cond_4b
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingLeft:I

    sub-int v9, v8, v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingRight:I

    sub-int/2addr v9, v10

    sub-int v6, v9, v2

    .line 615
    .local v6, textWidth:I
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingTop:I

    sub-int v9, v1, v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPaddingBottom:I

    sub-int v5, v9, v10

    .line 617
    .local v5, textHeight:I
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarVisible:Z

    if-eqz v9, :cond_66

    .line 618
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarSize:I

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenAvatarAndText:I

    add-int/2addr v9, v10

    sub-int/2addr v6, v9

    .line 621
    :cond_66
    const/4 v7, 0x0

    .line 622
    .local v7, widget:Landroid/view/View;
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    packed-switch v9, :pswitch_data_12c

    .line 635
    :cond_6c
    :goto_6c
    :pswitch_6c
    if-eqz v7, :cond_87

    .line 636
    const/high16 v9, 0x4000

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonHeight:I

    const/high16 v11, 0x4000

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v7, v9, v10}, Landroid/view/View;->measure(II)V

    .line 639
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenNameAndCircles:I

    add-int/2addr v9, v10

    sub-int/2addr v5, v9

    .line 642
    :cond_87
    move v4, v6

    .line 643
    .local v4, nameTextWidth:I
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonVisible:Z

    if-eqz v9, :cond_8f

    .line 644
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mImageButtonMargin:I

    sub-int/2addr v4, v9

    .line 646
    :cond_8f
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    const/high16 v10, 0x4000

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    const/high16 v11, -0x8000

    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->measure(II)V

    .line 650
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenNameAndCircles:I

    add-int/2addr v9, v10

    sub-int/2addr v5, v9

    .line 652
    move v0, v6

    .line 653
    .local v0, circleTextWidth:I
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    if-eqz v9, :cond_b9

    .line 654
    sget-object v9, Lcom/google/android/apps/plus/views/PersonCardView;->sCircleIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGapBetweenIconAndCircles:I

    add-int/2addr v9, v10

    sub-int/2addr v0, v9

    .line 657
    :cond_b9
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    if-eqz v9, :cond_fa

    .line 659
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getLineHeight()I

    move-result v3

    .line 660
    .local v3, lineHeight:I
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    div-int v10, v5, v3

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 661
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    const/high16 v10, 0x4000

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    iget v11, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mMinHeight:I

    const/high16 v12, -0x8000

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->measure(II)V

    .line 664
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    const/high16 v11, 0x4000

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    const/high16 v12, 0x4000

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->measure(II)V

    .line 671
    .end local v3           #lineHeight:I
    :cond_fa
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonVisible:Z

    if-eqz v9, :cond_113

    .line 672
    iget-object v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    iget v10, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonSize:I

    const/high16 v11, 0x4000

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    iget v11, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonSize:I

    const/high16 v12, 0x4000

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Landroid/widget/ImageView;->measure(II)V

    .line 677
    :cond_113
    invoke-virtual {p0, v8, v1}, Lcom/google/android/apps/plus/views/PersonCardView;->setMeasuredDimension(II)V

    .line 678
    return-void

    .line 604
    .end local v0           #circleTextWidth:I
    .end local v1           #height:I
    .end local v2           #leftMargin:I
    .end local v4           #nameTextWidth:I
    .end local v5           #textHeight:I
    .end local v6           #textWidth:I
    .end local v7           #widget:Landroid/view/View;
    .end local v8           #width:I
    :cond_117
    iget v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPreferredWidth:I

    invoke-static {v9, p1}, Lcom/google/android/apps/plus/views/PersonCardView;->resolveSize(II)I

    move-result v8

    .restart local v8       #width:I
    goto/16 :goto_3b

    .line 624
    .restart local v1       #height:I
    .restart local v2       #leftMargin:I
    .restart local v5       #textHeight:I
    .restart local v6       #textWidth:I
    .restart local v7       #widget:Landroid/view/View;
    :pswitch_11f
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonVisible:Z

    if-eqz v9, :cond_6c

    .line 625
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    goto/16 :goto_6c

    .line 631
    :pswitch_127
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    goto/16 :goto_6c

    .line 622
    nop

    :pswitch_data_12c
    .packed-switch 0x0
        :pswitch_11f
        :pswitch_127
        :pswitch_6c
        :pswitch_127
    .end packed-switch
.end method

.method public setActionButtonVisible(ZII)V
    .registers 7
    .parameter "visible"
    .parameter "labelResId"
    .parameter "action"

    .prologue
    const/4 v2, 0x0

    .line 506
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonVisible:Z

    if-ne v0, p1, :cond_11

    .line 507
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonVisible:Z

    if-eqz v0, :cond_10

    .line 508
    iput p3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAction:I

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(I)V

    .line 531
    :cond_10
    :goto_10
    return-void

    .line 514
    :cond_11
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonVisible:Z

    .line 515
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonVisible:Z

    if-eqz v0, :cond_58

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    if-nez v0, :cond_4b

    .line 517
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 519
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    const v1, 0x7f02019c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    iget v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    iget v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButtonMinWidth:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 523
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->addView(Landroid/view/View;)V

    .line 525
    :cond_4b
    iput p3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAction:I

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(I)V

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_10

    .line 528
    :cond_58
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    if-eqz v0, :cond_10

    .line 529
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mActionButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_10
.end method

.method public setAutoWidthForHorizontalScrolling()V
    .registers 2

    .prologue
    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAutoWidth:Z

    .line 253
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;Z)V
    .registers 3
    .parameter "bitmap"
    .parameter "loading"

    .prologue
    .line 868
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 869
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->invalidate()V

    .line 870
    return-void
.end method

.method public setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .registers 2
    .parameter "circleNames"

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 383
    return-void
.end method

.method public setContactIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "gaiaId"
    .parameter "lookupKey"
    .parameter "avatarUrl"

    .prologue
    const/4 v4, 0x0

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mContactLookupKey:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_28

    :cond_11
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarVisible:Z

    if-eqz v0, :cond_28

    .line 298
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGaiaId:Ljava/lang/String;

    .line 299
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mContactLookupKey:Ljava/lang/String;

    .line 300
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mForceAvatarDefault:Z

    if-nez v0, :cond_23

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mContactLookupKey:Ljava/lang/String;

    if-eqz v0, :cond_29

    .line 302
    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 308
    :cond_23
    :goto_23
    iput-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 309
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->requestLayout()V

    .line 311
    :cond_28
    return-void

    .line 304
    :cond_29
    new-instance v0, Lcom/google/android/apps/plus/content/AvatarRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGaiaId:Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-direct {v0, v1, p3, v2, v3}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    goto :goto_23
.end method

.method public setContactName(Ljava/lang/String;)V
    .registers 8
    .parameter "name"

    .prologue
    .line 361
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDisplayName:Ljava/lang/String;

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mHighlightedText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v5, Lcom/google/android/apps/plus/views/PersonCardView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 364
    return-void
.end method

.method public setDescription(Ljava/lang/String;ZZ)V
    .registers 6
    .parameter "description"
    .parameter "html"
    .parameter "clickable"

    .prologue
    const/4 v1, 0x0

    .line 413
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    .line 415
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    .line 416
    if-eqz p2, :cond_18

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    :goto_17
    return-void

    .line 419
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_17

    .line 422
    :cond_1e
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    .line 423
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_17
.end method

.method public setDismissActionButtonVisible(Z)V
    .registers 6
    .parameter "visible"

    .prologue
    const/4 v3, 0x0

    .line 534
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonVisible:Z

    if-ne v0, p1, :cond_6

    .line 555
    :cond_5
    :goto_5
    return-void

    .line 538
    :cond_6
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonVisible:Z

    .line 539
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonVisible:Z

    if-eqz v0, :cond_56

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    if-nez v0, :cond_50

    .line 541
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButtonBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    const v1, 0x7f0200b3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802a0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->addView(Landroid/view/View;)V

    .line 551
    :cond_50
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 552
    :cond_56
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDismissButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5
.end method

.method public setForceAvatarDefault(Z)V
    .registers 3
    .parameter "forceDefault"

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mForceAvatarDefault:Z

    .line 265
    if-eqz p1, :cond_7

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 268
    :cond_7
    return-void
.end method

.method public setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "gaiaId"
    .parameter "avatarUrl"

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarVisible:Z

    if-eqz v0, :cond_21

    .line 280
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mGaiaId:Ljava/lang/String;

    .line 281
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mForceAvatarDefault:Z

    if-nez v0, :cond_1b

    .line 282
    new-instance v0, Lcom/google/android/apps/plus/content/AvatarRequest;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 285
    :cond_1b
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mAvatarBitmap:Landroid/graphics/Bitmap;

    .line 286
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->requestLayout()V

    .line 288
    :cond_21
    return-void
.end method

.method public setHighlightedText(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    .prologue
    .line 353
    if-nez p1, :cond_6

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mHighlightedText:Ljava/lang/String;

    .line 358
    :goto_5
    return-void

    .line 356
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mHighlightedText:Ljava/lang/String;

    goto :goto_5
.end method

.method public setOnPersonCardClickListener(Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 571
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mListener:Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;

    .line 572
    return-void
.end method

.method public setOneClickCircles(Ljava/lang/String;Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;Z)V
    .registers 9
    .parameter "circleIds"
    .parameter "circleSpinnerAdapter"
    .parameter "forSharing"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 430
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 431
    iput v3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    .line 436
    :goto_a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    if-nez v0, :cond_11

    .line 437
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->addCirclesButton()V

    .line 440
    :cond_11
    iget v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    if-ne v0, v3, :cond_55

    .line 441
    if-eqz p3, :cond_44

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080209

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    .line 446
    :goto_27
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowIcon(Z)V

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/CirclesButton;->setHighlighted(Z)V

    .line 454
    :goto_31
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 455
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 458
    iput-boolean p3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mForSharing:Z

    .line 459
    return-void

    .line 433
    :cond_40
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    goto :goto_a

    .line 444
    :cond_44
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08020a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    goto :goto_27

    .line 449
    :cond_55
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNameListForPackedIds(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setCircles(Ljava/util/ArrayList;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowIcon(Z)V

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CirclesButton;->setHighlighted(Z)V

    goto :goto_31
.end method

.method public setPackedCircleIdsEmailAndDescription(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .registers 12
    .parameter "circleIds"
    .parameter "emailAddress"
    .parameter "description"
    .parameter "html"
    .parameter "clickable"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 388
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 389
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    .line 390
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    :goto_17
    return-void

    .line 392
    :cond_18
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_31

    .line 393
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    .line 394
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mHighlightedText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/PersonCardView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v5, Lcom/google/android/apps/plus/views/PersonCardView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    move-object v1, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_17

    .line 397
    :cond_31
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4d

    .line 398
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    .line 399
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    .line 400
    if-eqz p4, :cond_47

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-static {p3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_17

    .line 403
    :cond_47
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_17

    .line 406
    :cond_4d
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    .line 407
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleIconVisible:Z

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_17
.end method

.method public setPersonId(Ljava/lang/String;)V
    .registers 2
    .parameter "personId"

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPersonId:Ljava/lang/String;

    .line 272
    return-void
.end method

.method public setPlusPage(Z)V
    .registers 2
    .parameter "plusPage"

    .prologue
    .line 371
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPlusPage:Z

    .line 372
    return-void
.end method

.method public setPosition(I)V
    .registers 2
    .parameter "position"

    .prologue
    .line 256
    iput p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mPosition:I

    .line 257
    return-void
.end method

.method public setShowCircleChangePending(Z)V
    .registers 5
    .parameter "flag"

    .prologue
    const/4 v2, 0x0

    .line 474
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleChangePending:Z

    if-eq v0, p1, :cond_34

    .line 475
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleChangePending:Z

    .line 477
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCircleChangePending:Z

    if-eqz v0, :cond_2b

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    if-nez v0, :cond_15

    .line 479
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mOneClickMode:I

    .line 480
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->addCirclesButton()V

    .line 483
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowIcon(Z)V

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setHighlighted(Z)V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 488
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    if-eqz v0, :cond_34

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowProgressIndicator(Z)V

    .line 492
    :cond_34
    return-void
.end method

.method public setShowTooltip(ZI)V
    .registers 4
    .parameter "flag"
    .parameter "resId"

    .prologue
    .line 466
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mShowTooltip:Z

    .line 467
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mShowTooltip:Z

    if-eqz v0, :cond_13

    .line 468
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mTooltipText:Ljava/lang/String;

    .line 469
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->invalidate()V

    .line 471
    :cond_13
    return-void
.end method

.method public setWellFormedEmail(Ljava/lang/String;)V
    .registers 3
    .parameter "wellFormedEmail"

    .prologue
    .line 318
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWellFormedEmailMode:Z

    .line 319
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWellFormedEmail:Ljava/lang/String;

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    return-void
.end method

.method public setWideMargin(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 561
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideMargin:Z

    if-eq v0, p1, :cond_9

    .line 562
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mWideMargin:Z

    .line 563
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->requestLayout()V

    .line 565
    :cond_9
    return-void
.end method

.method public final updateContentDescription()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 899
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PersonCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 901
    .local v1, res:Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 902
    .local v0, description:Ljava/lang/CharSequence;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDescriptionVisible:Z

    if-eqz v2, :cond_2a

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2a

    .line 903
    const v2, 0x7f0801de

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 910
    :goto_29
    return-void

    .line 907
    :cond_2a
    const v2, 0x7f0801dd

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_29
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    .prologue
    .line 846
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PersonCardView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_6

    .line 847
    const/4 v0, 0x1

    .line 849
    :goto_5
    return v0

    :cond_6
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_5
.end method
