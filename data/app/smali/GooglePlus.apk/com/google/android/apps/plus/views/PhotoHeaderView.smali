.class public Lcom/google/android/apps/plus/views/PhotoHeaderView;
.super Landroid/view/View;
.source "PhotoHeaderView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;
    }
.end annotation


# static fields
.field private static sBackgroundColor:I

.field private static sCommentBitmap:Landroid/graphics/Bitmap;

.field private static sCommentCountLeftMargin:I

.field private static sCommentCountPaint:Landroid/text/TextPaint;

.field private static sCommentCountTextWidth:I

.field private static sCropDimPaint:Landroid/graphics/Paint;

.field private static sCropPaint:Landroid/graphics/Paint;

.field private static sCropSize:I

.field private static sHasMultitouchDistinct:Z

.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sInitialized:Z

.field private static sPhotoOverlayBottomPadding:I

.field private static sPhotoOverlayRightPadding:I

.field private static sPlusOneBitmap:Landroid/graphics/Bitmap;

.field private static sPlusOneBottomMargin:I

.field private static sPlusOneCountLeftMargin:I

.field private static sPlusOneCountPaint:Landroid/text/TextPaint;

.field private static sPlusOneCountTextWidth:I

.field private static sTagPaint:Landroid/graphics/Paint;

.field private static sTagTextBackgroundPaint:Landroid/graphics/Paint;

.field private static sTagTextPadding:I

.field private static sTagTextPaint:Landroid/text/TextPaint;

.field private static sVideoImage:Landroid/graphics/Bitmap;

.field private static sVideoNotReadyImage:Landroid/graphics/Bitmap;


# instance fields
.field private mAllowCrop:Z

.field private mCommentText:Ljava/lang/String;

.field private mCropRect:Landroid/graphics/Rect;

.field private mCropSize:I

.field private mDoubleTapDebounce:Z

.field private mDoubleTapToZoomEnabled:Z

.field private mDrawMatrix:Landroid/graphics/Matrix;

.field private mDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mExternalClickListener:Landroid/view/View$OnClickListener;

.field private mFixedHeight:I

.field private mFlingDebounce:Z

.field private mFullScreen:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHaveLayout:Z

.field private mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

.field private mIsDoubleTouch:Z

.field private mLastTwoFingerUp:J

.field private mLoading:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMaxScale:F

.field private mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

.field mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mMinScale:F

.field private mOriginalMatrix:Landroid/graphics/Matrix;

.field private mPlusOneText:Ljava/lang/String;

.field private mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

.field private mRotation:F

.field private mScaleFactor:F

.field private mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

.field private mShouldTriggerViewLoaded:Z

.field private mShowTagShape:Z

.field private mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

.field private mTagName:Ljava/lang/CharSequence;

.field private mTagNameBackground:Landroid/graphics/RectF;

.field private mTagShape:Landroid/graphics/RectF;

.field private mTempDst:Landroid/graphics/RectF;

.field private mTempSrc:Landroid/graphics/RectF;

.field private mTransformsEnabled:Z

.field private mTranslateRect:Landroid/graphics/RectF;

.field private mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

.field private mValues:[F

.field private mVideoBlob:[B

.field private mVideoReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    const/4 v1, 0x1

    .line 260
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 153
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    .line 161
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    .line 163
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    .line 166
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    .line 183
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    .line 205
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    .line 241
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    .line 243
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    .line 245
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    .line 247
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    .line 249
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    .line 253
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    if-nez v0, :cond_50

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 261
    :cond_50
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->initialize()V

    .line 262
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v1, 0x1

    .line 265
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 153
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    .line 161
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    .line 163
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    .line 166
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    .line 183
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    .line 205
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    .line 241
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    .line 243
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    .line 245
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    .line 247
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    .line 249
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    .line 253
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    if-nez v0, :cond_50

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 266
    :cond_50
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->initialize()V

    .line 267
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v1, 0x1

    .line 270
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 153
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    .line 161
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    .line 163
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    .line 166
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    .line 183
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    .line 205
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    .line 241
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    .line 243
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    .line 245
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    .line 247
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    .line 249
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    .line 253
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    if-nez v0, :cond_50

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 271
    :cond_50
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->initialize()V

    .line 272
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/PhotoHeaderView;FFF)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->scale(FFF)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/PhotoHeaderView;FF)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->translate(FF)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->snap()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/PhotoHeaderView;FZ)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    return-void
.end method

.method private configureBounds(Z)V
    .registers 11
    .parameter "changed"

    .prologue
    const/4 v3, 0x0

    const/high16 v8, 0x4100

    const/4 v7, 0x0

    .line 1102
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    if-nez v2, :cond_d

    .line 1118
    :cond_c
    :goto_c
    return-void

    .line 1105
    :cond_d
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    .line 1106
    .local v1, dwidth:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    .line 1109
    .local v0, dheight:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, v3, v3, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 1112
    if-nez p1, :cond_2e

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    cmpl-float v2, v2, v7

    if-nez v2, :cond_82

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_82

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    if-eqz v2, :cond_82

    .line 1113
    :cond_2e
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v5

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v2, :cond_87

    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSize:I

    :goto_40
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v3, :cond_8c

    sget v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSize:I

    :goto_46
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    int-to-float v4, v4

    int-to-float v5, v5

    invoke-virtual {v6, v7, v7, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v4, :cond_91

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    :goto_58
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1114
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getScale()F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    const/high16 v3, 0x4000

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    mul-float/2addr v3, v8

    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMaxScale:F

    .line 1117
    :cond_82
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    goto :goto_c

    .line 1113
    :cond_87
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    goto :goto_40

    :cond_8c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v3

    goto :goto_46

    :cond_91
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-virtual {v4, v7, v7, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_58
.end method

.method private getScale()F
    .registers 3

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1155
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private initialize()V
    .registers 10

    .prologue
    const v8, 0x7f0d0076

    const v7, 0x7f0d006e

    const v5, 0x7f0d006d

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 1332
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1334
    .local v0, context:Landroid/content/Context;
    sget-boolean v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sInitialized:Z

    if-nez v3, :cond_1c3

    .line 1335
    sput-boolean v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sInitialized:Z

    .line 1337
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1340
    .local v1, resources:Landroid/content/res/Resources;
    const v3, 0x7f02012a

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    .line 1342
    const v3, 0x7f02012b

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    .line 1343
    const v3, 0x7f0201e9

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoImage:Landroid/graphics/Bitmap;

    .line 1344
    const v3, 0x7f0200c9

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoNotReadyImage:Landroid/graphics/Bitmap;

    .line 1348
    const v3, 0x7f0a0039

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sBackgroundColor:I

    .line 1351
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    .line 1352
    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1353
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    const v4, 0x7f0a0046

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 1354
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1356
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 1359
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    .line 1360
    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1361
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    const v4, 0x7f0a0045

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 1362
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1364
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 1367
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 1368
    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1369
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    const v4, 0x7f0a003a

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1370
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1371
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    const v4, 0x7f0d007b

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1372
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    const v4, 0x7f0d007a

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    const v5, 0x7f0a003b

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v6, v6, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1375
    const v3, 0x7f0d005c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSize:I

    .line 1377
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 1378
    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1379
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    const v4, 0x7f0a004b

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1380
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1382
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 1383
    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1384
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    const v4, 0x7f0a004c

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1385
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1386
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    const v4, 0x7f0d005d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1388
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    .line 1389
    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 1390
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    const v4, 0x7f0a003c

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 1391
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1392
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1393
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    const/high16 v4, -0x100

    invoke-virtual {v3, v6, v6, v6, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 1394
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 1396
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 1397
    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    const v4, 0x7f0a003d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1399
    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1402
    const v3, 0x7f0d005e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayRightPadding:I

    .line 1404
    const v3, 0x7f0d005f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayBottomPadding:I

    .line 1406
    const v3, 0x7f0d0064

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountLeftMargin:I

    .line 1408
    const v3, 0x7f0d0065

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountTextWidth:I

    .line 1410
    const v3, 0x7f0d0068

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountLeftMargin:I

    .line 1412
    const v3, 0x7f0d0069

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountTextWidth:I

    .line 1414
    const v3, 0x7f0d0067

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBottomMargin:I

    .line 1416
    const v3, 0x7f0d0079

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    .line 1419
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    sput-boolean v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sHasMultitouchDistinct:Z

    .line 1423
    .end local v1           #resources:Landroid/content/res/Resources;
    :cond_1c3
    new-instance v3, Landroid/view/GestureDetector;

    const/4 v4, 0x0

    sget-boolean v5, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sHasMultitouchDistinct:Z

    if-nez v5, :cond_1f3

    :goto_1ca
    invoke-direct {v3, v0, p0, v4, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1424
    new-instance v2, Landroid/view/ScaleGestureDetector;

    invoke-direct {v2, v0, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    .line 1425
    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    .line 1426
    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    .line 1427
    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    .line 1428
    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    .line 1429
    return-void

    .line 1423
    :cond_1f3
    const/4 v2, 0x0

    goto :goto_1ca
.end method

.method private scale(FFF)V
    .registers 10
    .parameter "newScale"
    .parameter "centerX"
    .parameter "centerY"

    .prologue
    .line 1173
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    neg-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1176
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    invoke-static {p1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMaxScale:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result p1

    .line 1178
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getScale()F

    move-result v0

    .line 1179
    .local v0, currentScale:F
    div-float v1, p1, v0

    .line 1182
    .local v1, factor:F
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1185
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->snap()V

    .line 1188
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1190
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    .line 1192
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    if-eqz v2, :cond_55

    .line 1193
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    div-float v3, p1, v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;->onImageScaled(F)V

    .line 1195
    :cond_55
    return-void
.end method

.method private snap()V
    .registers 16

    .prologue
    const/high16 v14, 0x41a0

    const/high16 v13, 0x4000

    const/4 v10, 0x0

    .line 1261
    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1262
    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1265
    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v11, :cond_7d

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v3, v11

    .line 1266
    .local v3, maxLeft:F
    :goto_1c
    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v11, :cond_7f

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    int-to-float v4, v11

    .line 1267
    .local v4, maxRight:F
    :goto_25
    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->left:F

    .line 1268
    .local v1, l:F
    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v11, Landroid/graphics/RectF;->right:F

    .line 1271
    .local v6, r:F
    sub-float v11, v6, v1

    sub-float v12, v4, v3

    cmpg-float v11, v11, v12

    if-gez v11, :cond_85

    .line 1273
    sub-float v11, v4, v3

    add-float v12, v6, v1

    sub-float/2addr v11, v12

    div-float/2addr v11, v13

    add-float v8, v3, v11

    .line 1285
    .local v8, translateX:F
    :goto_3d
    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v11, :cond_95

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    .line 1286
    .local v5, maxTop:F
    :goto_46
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_97

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    .line 1287
    .local v2, maxBottom:F
    :goto_4f
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    .line 1288
    .local v7, t:F
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    .line 1291
    .local v0, b:F
    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_9d

    .line 1293
    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    div-float/2addr v10, v13

    add-float v9, v5, v10

    .line 1304
    .local v9, translateY:F
    :goto_67
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-gtz v10, :cond_77

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-lez v10, :cond_ad

    .line 1305
    :cond_77
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-virtual {v10, v8, v9}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;->start(FF)Z

    .line 1310
    :goto_7c
    return-void

    .end local v0           #b:F
    .end local v1           #l:F
    .end local v2           #maxBottom:F
    .end local v3           #maxLeft:F
    .end local v4           #maxRight:F
    .end local v5           #maxTop:F
    .end local v6           #r:F
    .end local v7           #t:F
    .end local v8           #translateX:F
    .end local v9           #translateY:F
    :cond_7d
    move v3, v10

    .line 1265
    goto :goto_1c

    .line 1266
    .restart local v3       #maxLeft:F
    :cond_7f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v11

    int-to-float v4, v11

    goto :goto_25

    .line 1274
    .restart local v1       #l:F
    .restart local v4       #maxRight:F
    .restart local v6       #r:F
    :cond_85
    cmpl-float v11, v1, v3

    if-lez v11, :cond_8c

    .line 1276
    sub-float v8, v3, v1

    .restart local v8       #translateX:F
    goto :goto_3d

    .line 1277
    .end local v8           #translateX:F
    :cond_8c
    cmpg-float v11, v6, v4

    if-gez v11, :cond_93

    .line 1279
    sub-float v8, v4, v6

    .restart local v8       #translateX:F
    goto :goto_3d

    .line 1281
    .end local v8           #translateX:F
    :cond_93
    const/4 v8, 0x0

    .restart local v8       #translateX:F
    goto :goto_3d

    :cond_95
    move v5, v10

    .line 1285
    goto :goto_46

    .line 1286
    .restart local v5       #maxTop:F
    :cond_97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_4f

    .line 1294
    .restart local v0       #b:F
    .restart local v2       #maxBottom:F
    .restart local v7       #t:F
    :cond_9d
    cmpl-float v10, v7, v5

    if-lez v10, :cond_a4

    .line 1296
    sub-float v9, v5, v7

    .restart local v9       #translateY:F
    goto :goto_67

    .line 1297
    .end local v9           #translateY:F
    :cond_a4
    cmpg-float v10, v0, v2

    if-gez v10, :cond_ab

    .line 1299
    sub-float v9, v2, v0

    .restart local v9       #translateY:F
    goto :goto_67

    .line 1301
    .end local v9           #translateY:F
    :cond_ab
    const/4 v9, 0x0

    .restart local v9       #translateY:F
    goto :goto_67

    .line 1307
    :cond_ad
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1308
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_7c
.end method

.method private translate(FF)Z
    .registers 15
    .parameter "tx"
    .parameter "ty"

    .prologue
    .line 1208
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1209
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1211
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_84

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v3, v10

    .line 1212
    .local v3, maxLeft:F
    :goto_17
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_86

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    int-to-float v4, v10

    .line 1213
    .local v4, maxRight:F
    :goto_20
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v10, Landroid/graphics/RectF;->left:F

    .line 1214
    .local v1, l:F
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v10, Landroid/graphics/RectF;->right:F

    .line 1217
    .local v6, r:F
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_8c

    .line 1219
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float v10, v3, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    sub-float v11, v4, v11

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 1230
    .local v8, translateX:F
    :goto_40
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_ac

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    .line 1231
    .local v5, maxTop:F
    :goto_49
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_ae

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    .line 1232
    .local v2, maxBottom:F
    :goto_52
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    .line 1233
    .local v7, t:F
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    .line 1237
    .local v0, b:F
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_b4

    .line 1239
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v5, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    sub-float v11, v2, v11

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 1251
    .local v9, translateY:F
    :goto_72
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1252
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    .line 1254
    cmpl-float v10, v8, p1

    if-nez v10, :cond_d4

    cmpl-float v10, v9, p2

    if-nez v10, :cond_d4

    const/4 v10, 0x1

    :goto_83
    return v10

    .line 1211
    .end local v0           #b:F
    .end local v1           #l:F
    .end local v2           #maxBottom:F
    .end local v3           #maxLeft:F
    .end local v4           #maxRight:F
    .end local v5           #maxTop:F
    .end local v6           #r:F
    .end local v7           #t:F
    .end local v8           #translateX:F
    .end local v9           #translateY:F
    :cond_84
    const/4 v3, 0x0

    goto :goto_17

    .line 1212
    .restart local v3       #maxLeft:F
    :cond_86
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v10

    int-to-float v4, v10

    goto :goto_20

    .line 1223
    .restart local v1       #l:F
    .restart local v4       #maxRight:F
    .restart local v6       #r:F
    :cond_8c
    sub-float v10, v6, v1

    sub-float v11, v4, v3

    cmpg-float v10, v10, v11

    if-gez v10, :cond_9f

    .line 1224
    sub-float v10, v4, v3

    add-float v11, v6, v1

    sub-float/2addr v10, v11

    const/high16 v11, 0x4000

    div-float/2addr v10, v11

    add-float v8, v3, v10

    .restart local v8       #translateX:F
    goto :goto_40

    .line 1226
    .end local v8           #translateX:F
    :cond_9f
    sub-float v10, v4, v6

    sub-float v11, v3, v1

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .restart local v8       #translateX:F
    goto :goto_40

    .line 1230
    :cond_ac
    const/4 v5, 0x0

    goto :goto_49

    .line 1231
    .restart local v5       #maxTop:F
    :cond_ae
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_52

    .line 1243
    .restart local v0       #b:F
    .restart local v2       #maxBottom:F
    .restart local v7       #t:F
    :cond_b4
    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_c7

    .line 1244
    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    const/high16 v11, 0x4000

    div-float/2addr v10, v11

    add-float v9, v5, v10

    .restart local v9       #translateY:F
    goto :goto_72

    .line 1246
    .end local v9           #translateY:F
    :cond_c7
    sub-float v10, v2, v0

    sub-float v11, v5, v7

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .restart local v9       #translateY:F
    goto :goto_72

    .line 1254
    :cond_d4
    const/4 v10, 0x0

    goto :goto_83
.end method


# virtual methods
.method public final bindPhoto(Landroid/graphics/Bitmap;)V
    .registers 6
    .parameter "photoBitmap"

    .prologue
    .line 518
    const/4 v0, 0x0

    .line 519
    .local v0, changed:Z
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_2f

    .line 520
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 521
    .local v1, drawableBitmap:Landroid/graphics/Bitmap;
    if-ne p1, v1, :cond_e

    .line 541
    .end local v1           #drawableBitmap:Landroid/graphics/Bitmap;
    :goto_d
    return-void

    .line 526
    .restart local v1       #drawableBitmap:Landroid/graphics/Bitmap;
    :cond_e
    if-eqz p1, :cond_47

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v2, v3, :cond_28

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v2, v3, :cond_47

    :cond_28
    const/4 v0, 0x1

    .line 531
    :goto_29
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    .line 532
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 535
    .end local v1           #drawableBitmap:Landroid/graphics/Bitmap;
    :cond_2f
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v2, :cond_40

    if-eqz p1, :cond_40

    .line 536
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 539
    :cond_40
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->configureBounds(Z)V

    .line 540
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_d

    .line 526
    .restart local v1       #drawableBitmap:Landroid/graphics/Bitmap;
    :cond_47
    const/4 v0, 0x0

    goto :goto_29
.end method

.method public final bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "rect"
    .parameter "name"

    .prologue
    .line 647
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    .line 648
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    .line 649
    return-void
.end method

.method public final clear()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 497
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 498
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    .line 499
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->stop()V

    .line 501
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    .line 502
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->stop()V

    .line 503
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;->stop()V

    .line 505
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->stop()V

    .line 507
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    .line 508
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 509
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    .line 510
    return-void
.end method

.method public final enableAllowCrop(Z)V
    .registers 4
    .parameter "allowCrop"

    .prologue
    .line 714
    if-eqz p1, :cond_e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    if-eqz v0, :cond_e

    .line 715
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot set crop after view has been laid out"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 717
    :cond_e
    if-nez p1, :cond_1c

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v0, :cond_1c

    .line 718
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot unset crop mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 720
    :cond_1c
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    .line 721
    return-void
.end method

.method public final enableImageTransforms(Z)V
    .registers 3
    .parameter "enable"

    .prologue
    .line 1037
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    .line 1038
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-nez v0, :cond_9

    .line 1039
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->resetTransformations()V

    .line 1041
    :cond_9
    return-void
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_b

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 559
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final getCroppedPhoto()Landroid/graphics/Bitmap;
    .registers 8

    .prologue
    const/16 v6, 0x100

    .line 727
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-nez v5, :cond_8

    .line 728
    const/4 v1, 0x0

    .line 753
    :cond_7
    :goto_7
    return-object v1

    .line 731
    :cond_8
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v6, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 733
    .local v1, croppedBitmap:Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 736
    .local v2, croppedCanvas:Landroid/graphics/Canvas;
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sub-int v0, v5, v6

    .line 737
    .local v0, cropWidth:I
    const/high16 v5, 0x4380

    int-to-float v6, v0

    div-float v4, v5, v6

    .line 738
    .local v4, scaleWidth:F
    new-instance v3, Landroid/graphics/Matrix;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-direct {v3, v5}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 742
    .local v3, matrix:Landroid/graphics/Matrix;
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    neg-int v5, v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    neg-int v6, v6

    int-to-float v6, v6

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 743
    invoke-virtual {v3, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 746
    sget v5, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sBackgroundColor:I

    invoke-virtual {v2, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 749
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_7

    .line 750
    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 751
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5, v2}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_7
.end method

.method public final getPhoto()Landroid/graphics/Bitmap;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final getVideoData()[B
    .registers 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    return-object v0
.end method

.method public final hideTagShape()V
    .registers 2

    .prologue
    .line 664
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShowTagShape:Z

    .line 666
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    .line 667
    return-void
.end method

.method public final init(Lcom/google/android/apps/plus/api/MediaRef;)V
    .registers 2
    .parameter "mediaRef"

    .prologue
    .line 987
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 988
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->requestLayout()V

    .line 989
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    .line 990
    return-void
.end method

.method public final interceptMoveLeft$2548a39(F)Z
    .registers 9
    .parameter "origY"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 428
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-nez v5, :cond_7

    .line 454
    :cond_6
    :goto_6
    return v3

    .line 431
    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    #getter for: Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->mRunning:Z
    invoke-static {v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->access$000(Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;)Z

    move-result v5

    if-eqz v5, :cond_11

    move v3, v4

    .line 433
    goto :goto_6

    .line 435
    :cond_11
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->getValues([F)V

    .line 436
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 437
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v5

    int-to-float v2, v5

    .line 440
    .local v2, viewWidth:F
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v6, 0x2

    aget v1, v5, v6

    .line 441
    .local v1, transX:F
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    sub-float v0, v5, v6

    .line 443
    .local v0, drawWidth:F
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v5, :cond_6

    cmpg-float v5, v0, v2

    if-lez v5, :cond_6

    .line 446
    const/4 v5, 0x0

    cmpl-float v5, v1, v5

    if-eqz v5, :cond_6

    .line 449
    add-float v3, v0, v1

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_4f

    move v3, v4

    .line 451
    goto :goto_6

    :cond_4f
    move v3, v4

    .line 454
    goto :goto_6
.end method

.method public final interceptMoveRight$2548a39(F)Z
    .registers 9
    .parameter "origY"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 461
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-nez v5, :cond_7

    .line 487
    :cond_6
    :goto_6
    return v3

    .line 464
    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    #getter for: Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->mRunning:Z
    invoke-static {v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->access$000(Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;)Z

    move-result v5

    if-eqz v5, :cond_11

    move v3, v4

    .line 466
    goto :goto_6

    .line 468
    :cond_11
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->getValues([F)V

    .line 469
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 470
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 472
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v5

    int-to-float v2, v5

    .line 473
    .local v2, viewWidth:F
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v6, 0x2

    aget v1, v5, v6

    .line 474
    .local v1, transX:F
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    sub-float v0, v5, v6

    .line 476
    .local v0, drawWidth:F
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v5, :cond_6

    cmpg-float v5, v0, v2

    if-lez v5, :cond_6

    .line 479
    const/4 v5, 0x0

    cmpl-float v5, v1, v5

    if-nez v5, :cond_49

    move v3, v4

    .line 481
    goto :goto_6

    .line 482
    :cond_49
    add-float v5, v0, v1

    cmpl-float v5, v2, v5

    if-gez v5, :cond_6

    move v3, v4

    .line 487
    goto :goto_6
.end method

.method public final isPhotoBound()Z
    .registers 2

    .prologue
    .line 673
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final isPhotoLoading()Z
    .registers 2

    .prologue
    .line 680
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLoading:Z

    return v0
.end method

.method public final isVideo()Z
    .registers 2

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final isVideoReady()Z
    .registers 2

    .prologue
    .line 640
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoReady:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 786
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 787
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V

    .line 788
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 789
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 793
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 794
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterRemoteImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnRemoteImageChangeListener;)V

    .line 795
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 796
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "e"

    .prologue
    .line 311
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    if-eqz v2, :cond_30

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v2, :cond_30

    .line 312
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapDebounce:Z

    if-nez v2, :cond_2d

    .line 313
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getScale()F

    move-result v0

    .line 314
    .local v0, currentScale:F
    const/high16 v2, 0x3fc0

    mul-float v1, v0, v2

    .line 317
    .local v1, targetScale:F
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 318
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMaxScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 320
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->start(FFFF)Z

    .line 322
    .end local v0           #currentScale:F
    .end local v1           #targetScale:F
    :cond_2d
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapDebounce:Z

    .line 324
    :cond_30
    const/4 v2, 0x1

    return v2
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "e"

    .prologue
    .line 329
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "e"

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_e

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->stop()V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;->stop()V

    .line 371
    :cond_e
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 31
    .parameter "canvas"

    .prologue
    .line 800
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 802
    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sBackgroundColor:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 805
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v2, :cond_4e

    .line 806
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 808
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v20

    .line 809
    .local v20, newBitmap:Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v2, :cond_2f8

    const/16 v21, 0x0

    .line 810
    .local v21, oldBitmap:Landroid/graphics/Bitmap;
    :goto_27
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_4e

    .line 811
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindPhoto(Landroid/graphics/Bitmap;)V

    .line 812
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    if-eqz v2, :cond_4e

    if-eqz v20, :cond_4e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    if-eqz v2, :cond_4e

    .line 813
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;->onImageLoaded$46d55081()V

    .line 814
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    .line 819
    .end local v20           #newBitmap:Landroid/graphics/Bitmap;
    .end local v21           #oldBitmap:Landroid/graphics/Bitmap;
    :cond_4e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_217

    .line 820
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v25

    .line 821
    .local v25, saveCount:I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 823
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_6a

    .line 824
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 826
    :cond_6a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 828
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 830
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    if-eqz v2, :cond_ac

    .line 831
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoReady:Z

    if-eqz v2, :cond_302

    sget-object v26, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoImage:Landroid/graphics/Bitmap;

    .line 832
    .local v26, videoImage:Landroid/graphics/Bitmap;
    :goto_88
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v2, v7

    div-int/lit8 v17, v2, 0x2

    .line 833
    .local v17, drawLeft:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int/2addr v2, v7

    div-int/lit8 v18, v2, 0x2

    .line 834
    .local v18, drawTop:I
    move/from16 v0, v17

    int-to-float v2, v0

    move/from16 v0, v18

    int-to-float v7, v0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v2, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 838
    .end local v17           #drawLeft:I
    .end local v18           #drawTop:I
    .end local v26           #videoImage:Landroid/graphics/Bitmap;
    :cond_ac
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 839
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_cc

    .line 840
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 842
    :cond_cc
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShowTagShape:Z

    if-eqz v2, :cond_1c4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    if-eqz v2, :cond_1c4

    .line 843
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v19

    .line 844
    .local v19, drawWidth:F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v16

    .line 846
    .local v16, drawHeight:F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float v3, v2, v7

    .line 847
    .local v3, tagLeft:F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    mul-float v2, v2, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float v4, v2, v7

    .line 848
    .local v4, tagTop:F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float v5, v2, v7

    .line 849
    .local v5, tagRight:F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    mul-float v2, v2, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float v6, v2, v7

    .line 851
    .local v6, tagBottom:F
    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 853
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1c4

    const/high16 v2, 0x4000

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    int-to-float v7, v7

    mul-float/2addr v2, v7

    sub-float v7, v5, v3

    const/high16 v8, 0x4000

    div-float/2addr v7, v8

    add-float/2addr v7, v3

    sget-object v8, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    invoke-interface {v11}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v8

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->descent()F

    move-result v9

    sget-object v10, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v10}, Landroid/text/TextPaint;->ascent()F

    move-result v10

    sub-float/2addr v9, v10

    add-float/2addr v8, v2

    add-float/2addr v9, v2

    const/high16 v2, 0x4000

    div-float v2, v8, v2

    sub-float v2, v7, v2

    const/4 v7, 0x0

    cmpg-float v7, v2, v7

    if-gez v7, :cond_16f

    const/4 v2, 0x0

    :cond_16f
    add-float v7, v2, v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v10

    int-to-float v10, v10

    cmpl-float v10, v7, v10

    if-lez v10, :cond_309

    sub-float v2, v5, v8

    .end local v5           #tagRight:F
    :goto_17c
    add-float v7, v6, v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v8

    int-to-float v8, v8

    cmpl-float v8, v7, v8

    if-lez v8, :cond_306

    sub-float v6, v4, v9

    .end local v4           #tagTop:F
    .end local v6           #tagBottom:F
    :goto_189
    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    int-to-float v7, v7

    add-float v11, v2, v7

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    int-to-float v7, v7

    add-float/2addr v7, v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    invoke-virtual {v8, v2, v6, v5, v4}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    const/high16 v8, 0x4040

    const/high16 v9, 0x4040

    sget-object v10, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v9, v10}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v10

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float v12, v7, v2

    sget-object v13, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v13}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 856
    .end local v3           #tagLeft:F
    .end local v16           #drawHeight:F
    .end local v19           #drawWidth:F
    :cond_1c4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v2, :cond_217

    .line 857
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v24

    .line 858
    .local v24, previousSaveCount:I
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    int-to-float v10, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    int-to-float v11, v2

    sget-object v12, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 859
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 860
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 862
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_1fc

    .line 863
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 866
    :cond_1fc
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 867
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 868
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 873
    .end local v24           #previousSaveCount:I
    .end local v25           #saveCount:I
    :cond_217
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayBottomPadding:I

    sub-int v28, v2, v7

    .line 875
    .local v28, yPos:I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-eqz v2, :cond_28b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    if-eqz v2, :cond_28b

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-nez v2, :cond_28b

    .line 877
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->descent()F

    move-result v7

    sub-float/2addr v2, v7

    float-to-int v15, v2

    .line 879
    .local v15, commentTextHeight:I
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2, v15}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 881
    .local v14, commentHeight:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayRightPadding:I

    sub-int/2addr v2, v7

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountTextWidth:I

    sub-int v27, v2, v7

    .line 883
    .local v27, xPos:I
    sub-int v28, v28, v14

    .line 884
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    move/from16 v0, v27

    int-to-float v7, v0

    move/from16 v0, v28

    int-to-float v8, v0

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->ascent()F

    move-result v9

    sub-float/2addr v8, v9

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 887
    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountLeftMargin:I

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/2addr v2, v7

    sub-int v27, v27, v2

    .line 888
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v27

    int-to-float v7, v0

    move/from16 v0, v28

    int-to-float v8, v0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 890
    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBottomMargin:I

    sub-int v28, v28, v2

    .line 893
    .end local v14           #commentHeight:I
    .end local v15           #commentTextHeight:I
    .end local v27           #xPos:I
    :cond_28b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-eqz v2, :cond_2f7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    if-eqz v2, :cond_2f7

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-nez v2, :cond_2f7

    .line 895
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->descent()F

    move-result v7

    sub-float/2addr v2, v7

    float-to-int v0, v2

    move/from16 v23, v0

    .line 897
    .local v23, plusOneTextHeight:I
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    move/from16 v0, v23

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 899
    .local v22, plusOneHeight:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayRightPadding:I

    sub-int/2addr v2, v7

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountTextWidth:I

    sub-int v27, v2, v7

    .line 901
    .restart local v27       #xPos:I
    sub-int v28, v28, v22

    .line 902
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    move/from16 v0, v27

    int-to-float v7, v0

    move/from16 v0, v28

    int-to-float v8, v0

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->ascent()F

    move-result v9

    sub-float/2addr v8, v9

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 905
    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountLeftMargin:I

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/2addr v2, v7

    sub-int v27, v27, v2

    .line 906
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v27

    int-to-float v7, v0

    move/from16 v0, v28

    int-to-float v8, v0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 908
    .end local v22           #plusOneHeight:I
    .end local v23           #plusOneTextHeight:I
    .end local v27           #xPos:I
    :cond_2f7
    return-void

    .line 809
    .end local v28           #yPos:I
    .restart local v20       #newBitmap:Landroid/graphics/Bitmap;
    :cond_2f8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v21

    goto/16 :goto_27

    .line 831
    .end local v20           #newBitmap:Landroid/graphics/Bitmap;
    .restart local v25       #saveCount:I
    :cond_302
    sget-object v26, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoNotReadyImage:Landroid/graphics/Bitmap;

    goto/16 :goto_88

    .restart local v3       #tagLeft:F
    .restart local v4       #tagTop:F
    .restart local v6       #tagBottom:F
    .restart local v16       #drawHeight:F
    .restart local v19       #drawWidth:F
    :cond_306
    move v4, v7

    goto/16 :goto_189

    .restart local v5       #tagRight:F
    :cond_309
    move v5, v7

    goto/16 :goto_17c
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter "e1"
    .parameter "e2"
    .parameter "velocityX"
    .parameter "velocityY"

    .prologue
    .line 376
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_10

    .line 377
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFlingDebounce:Z

    if-nez v0, :cond_d

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->start(FF)Z

    .line 380
    :cond_d
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFlingDebounce:Z

    .line 382
    :cond_10
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .registers 19
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 912
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 913
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    .line 914
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v12

    .line 915
    .local v12, layoutWidth:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v11

    .line 917
    .local v11, layoutHeight:I
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_87

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_26

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_87

    :cond_26
    const/4 v10, 0x1

    .line 919
    .local v10, hasValidMediaRef:Z
    :goto_27
    if-eqz v10, :cond_5a

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-nez v1, :cond_5a

    .line 921
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_89

    .line 922
    new-instance v0, Lcom/google/android/apps/plus/content/MediaImageRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredHeight()I

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .line 928
    .local v0, req:Lcom/google/android/apps/plus/content/ImageRequest;
    :goto_4e
    new-instance v1, Lcom/google/android/apps/plus/views/MediaImage;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 929
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 932
    .end local v0           #req:Lcom/google/android/apps/plus/content/ImageRequest;
    :cond_5a
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v1, :cond_83

    .line 933
    sget v1, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSize:I

    invoke-static {v12, v11}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSize:I

    .line 934
    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSize:I

    sub-int v1, v12, v1

    div-int/lit8 v7, v1, 0x2

    .line 935
    .local v7, cropLeft:I
    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSize:I

    sub-int v1, v11, v1

    div-int/lit8 v9, v1, 0x2

    .line 936
    .local v9, cropTop:I
    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSize:I

    add-int v8, v7, v1

    .line 937
    .local v8, cropRight:I
    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSize:I

    add-int v6, v9, v1

    .line 941
    .local v6, cropBottom:I
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v7, v9, v8, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 943
    .end local v6           #cropBottom:I
    .end local v7           #cropLeft:I
    .end local v8           #cropRight:I
    .end local v9           #cropTop:I
    :cond_83
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->configureBounds(Z)V

    .line 944
    return-void

    .line 917
    .end local v10           #hasValidMediaRef:Z
    :cond_87
    const/4 v10, 0x0

    goto :goto_27

    .line 925
    .restart local v10       #hasValidMediaRef:Z
    :cond_89
    new-instance v0, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/RemoteImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .restart local v0       #req:Lcom/google/android/apps/plus/content/ImageRequest;
    goto :goto_4e
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "e"

    .prologue
    .line 348
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 948
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1a

    .line 949
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    const/high16 v1, -0x8000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    .line 951
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setMeasuredDimension(II)V

    .line 955
    :goto_19
    return-void

    .line 953
    :cond_1a
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_19
.end method

.method public final onMediaImageChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 959
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v0, :cond_9

    .line 967
    :cond_8
    :goto_8
    return-void

    .line 962
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 963
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    .line 964
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 965
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_8
.end method

.method public final onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/Bitmap;)V
    .registers 8
    .parameter "request"
    .parameter "bitmap"

    .prologue
    .line 971
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_c

    instance-of v1, p1, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    if-nez v1, :cond_d

    .line 981
    :cond_c
    :goto_c
    return-void

    :cond_d
    move-object v0, p1

    .line 974
    check-cast v0, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    .line 975
    .local v0, remoteRequest:Lcom/google/android/apps/plus/content/RemoteImageRequest;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getPhotoId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_c

    .line 977
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    .line 978
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 979
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_c
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .registers 8
    .parameter "detector"

    .prologue
    const/4 v5, 0x0

    .line 387
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    const/high16 v4, 0x3f80

    sub-float v2, v3, v4

    .line 389
    .local v2, scaleDelta:F
    cmpg-float v3, v2, v5

    if-gez v3, :cond_13

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_1d

    :cond_13
    cmpl-float v3, v2, v5

    if-lez v3, :cond_1f

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1f

    .line 390
    :cond_1d
    iput v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    .line 392
    :cond_1f
    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    add-float/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    .line 393
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v3, :cond_4d

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x3d23d70a

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4d

    .line 394
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    .line 395
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getScale()F

    move-result v0

    .line 396
    .local v0, currentScale:F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    mul-float v1, v0, v3

    .line 397
    .local v1, newScale:F
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    invoke-direct {p0, v1, v3, v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->scale(FFF)V

    .line 399
    .end local v0           #currentScale:F
    .end local v1           #newScale:F
    :cond_4d
    const/4 v3, 0x1

    return v3
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .registers 4
    .parameter "detector"

    .prologue
    const/4 v1, 0x1

    .line 404
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_f

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->stop()V

    .line 406
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    .line 407
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    .line 409
    :cond_f
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .registers 4
    .parameter "detector"

    .prologue
    const/4 v1, 0x1

    .line 414
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    if-eqz v0, :cond_e

    .line 415
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapDebounce:Z

    .line 416
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->resetTransformations()V

    .line 418
    :cond_e
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFlingDebounce:Z

    .line 419
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 11
    .parameter "e1"
    .parameter "e2"
    .parameter "distanceX"
    .parameter "distanceY"

    .prologue
    .line 356
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLastTwoFingerUp:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 358
    .local v0, timeSinceTwoFingerUp:J
    const-wide/16 v2, 0x190

    cmp-long v2, v0, v2

    if-lez v2, :cond_1a

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v2, :cond_1a

    .line 360
    neg-float v2, p3

    neg-float v3, p4

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->translate(FF)Z

    .line 362
    :cond_1a
    const/4 v2, 0x1

    return v2
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "e"

    .prologue
    .line 352
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "e"

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    if-nez v0, :cond_d

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 337
    :cond_d
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    .line 338
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "e"

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_9

    .line 1004
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->load()V

    .line 1006
    :cond_9
    return-void
.end method

.method public final onStop()V
    .registers 4

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_b

    .line 1013
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/MediaImage;->setBitmap(Landroid/graphics/Bitmap;Z)V

    .line 1015
    :cond_b
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    .prologue
    const/4 v4, 0x1

    .line 276
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    if-nez v2, :cond_a

    .line 306
    :cond_9
    :goto_9
    return v4

    .line 281
    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 282
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 284
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 286
    .local v1, actionMasked:I
    packed-switch v1, :pswitch_data_48

    .line 295
    :cond_1b
    :goto_1b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 297
    .local v0, action:I
    packed-switch v0, :pswitch_data_4e

    :pswitch_22
    goto :goto_9

    .line 300
    :pswitch_23
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    #getter for: Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->mRunning:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->access$000(Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 301
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->snap()V

    goto :goto_9

    .line 288
    .end local v0           #action:I
    :pswitch_2f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3d

    .line 289
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLastTwoFingerUp:J

    goto :goto_1b

    .line 290
    :cond_3d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v2, v4, :cond_1b

    .line 291
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLastTwoFingerUp:J

    goto :goto_1b

    .line 286
    :pswitch_data_48
    .packed-switch 0x6
        :pswitch_2f
    .end packed-switch

    .line 297
    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_23
        :pswitch_22
        :pswitch_23
    .end packed-switch
.end method

.method public final resetTransformations()V
    .registers 3

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 767
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    .line 768
    return-void
.end method

.method public setCommentCount(I)V
    .registers 5
    .parameter "commentCount"

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    .line 568
    .local v0, oldCommentText:Ljava/lang/String;
    if-gez p1, :cond_5

    .line 583
    :cond_4
    :goto_4
    return-void

    .line 572
    :cond_5
    if-nez p1, :cond_16

    .line 573
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    .line 580
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 581
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_4

    .line 574
    :cond_16
    const/16 v1, 0x63

    if-le p1, v1, :cond_28

    .line 575
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    goto :goto_a

    .line 577
    :cond_28
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    goto :goto_a
.end method

.method public setFixedHeight(I)V
    .registers 6
    .parameter "fixedHeight"

    .prologue
    const/4 v1, 0x1

    .line 1023
    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    if-eq p1, v2, :cond_1a

    move v0, v1

    .line 1024
    .local v0, adjustBounds:Z
    :goto_6
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    .line 1025
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setMeasuredDimension(II)V

    .line 1026
    if-eqz v0, :cond_19

    .line 1027
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->configureBounds(Z)V

    .line 1028
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->requestLayout()V

    .line 1030
    :cond_19
    return-void

    .line 1023
    .end local v0           #adjustBounds:Z
    :cond_1a
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public setFullScreen(ZZ)V
    .registers 4
    .parameter "fullScreen"
    .parameter "animate"

    .prologue
    .line 696
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-eq p1, v0, :cond_1f

    .line 697
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    .line 698
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-nez v0, :cond_19

    .line 699
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->stop()V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->stop()V

    .line 701
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->stop()V

    .line 703
    :cond_19
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->requestLayout()V

    .line 704
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    .line 706
    :cond_1f
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 423
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    .line 424
    return-void
.end method

.method public setOnImageListener(Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 996
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    .line 997
    return-void
.end method

.method public setPhotoLoading(Z)V
    .registers 2
    .parameter "loading"

    .prologue
    .line 687
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLoading:Z

    .line 688
    return-void
.end method

.method public setPlusOneCount(I)V
    .registers 5
    .parameter "plusOneCount"

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    .line 590
    .local v0, oldPlusOneText:Ljava/lang/String;
    if-gez p1, :cond_5

    .line 607
    :cond_4
    :goto_4
    return-void

    .line 594
    :cond_5
    if-nez p1, :cond_16

    .line 595
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    .line 604
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 605
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_4

    .line 597
    :cond_16
    const/16 v1, 0x63

    if-le p1, v1, :cond_28

    .line 598
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    goto :goto_a

    .line 600
    :cond_28
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    goto :goto_a
.end method

.method public setVideoBlob([B)V
    .registers 5
    .parameter "blob"

    .prologue
    .line 613
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    .line 614
    if-eqz p1, :cond_25

    .line 615
    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    .line 616
    .local v0, videoData:Lcom/google/api/services/plusi/model/DataVideo;
    const-string v1, "FINAL"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string v1, "READY"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_26

    :cond_22
    const/4 v1, 0x1

    :goto_23
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoReady:Z

    .line 620
    .end local v0           #videoData:Lcom/google/api/services/plusi/model/DataVideo;
    :cond_25
    return-void

    .line 616
    .restart local v0       #videoData:Lcom/google/api/services/plusi/model/DataVideo;
    :cond_26
    const/4 v1, 0x0

    goto :goto_23
.end method

.method public final showTagShape()V
    .registers 2

    .prologue
    .line 655
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShowTagShape:Z

    .line 657
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    .line 658
    return-void
.end method
