.class public Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;
.super Lcom/google/android/apps/plus/views/OneUpBaseView;
.source "PhotoOneUpInfoView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;
    }
.end annotation


# static fields
.field private static sActionBarBackgroundPaint:Landroid/graphics/Paint;

.field private static sAvatarMarginLeft:I

.field private static sAvatarMarginRight:I

.field private static sAvatarMarginTop:I

.field private static sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sCaptionMarginTop:I

.field private static sContentPaint:Landroid/text/TextPaint;

.field private static sDatePaint:Landroid/text/TextPaint;

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sFontSpacing:F

.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sMarginBottom:I

.field private static sMarginLeft:I

.field private static sMarginRight:I

.field private static sNameMarginTop:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sPlusOneButtonMarginLeft:I

.field private static sPlusOneButtonMarginRight:I

.field private static sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mAlbumId:Ljava/lang/String;

.field private mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

.field private mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mBackgroundOffset:I

.field private mCaption:Landroid/text/Spannable;

.field private mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContentDescriptionDirty:Z

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/String;

.field private mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mOwnerId:Ljava/lang/String;

.field private mOwnerName:Ljava/lang/String;

.field protected mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field private mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    .prologue
    const v7, 0x7f0d018b

    const v6, 0x7f0d018a

    const v5, 0x7f0d0189

    const v4, 0x7f0a00de

    const/4 v3, 0x1

    .line 135
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;)V

    .line 100
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    .line 130
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 147
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_161

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 151
    .local v0, res:Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    .line 153
    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    .line 155
    const v1, 0x7f0d0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    .line 157
    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    .line 159
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    .line 161
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    .line 163
    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    .line 165
    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    .line 167
    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    .line 169
    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    .line 171
    const v1, 0x7f0d019b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    .line 174
    const v1, 0x7f0d0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 179
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 181
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 182
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 183
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 184
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00df

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 185
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 186
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 188
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 189
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 190
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 191
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 192
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 193
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 195
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 196
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 197
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 198
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 199
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 200
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 203
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 204
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 205
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 207
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 208
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 210
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 212
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    .line 214
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 217
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_161
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setupAccessibility(Landroid/content/Context;)V

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v7, 0x7f0d018b

    const v6, 0x7f0d018a

    const v5, 0x7f0d0189

    const v4, 0x7f0a00de

    const/4 v3, 0x1

    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    .line 130
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 147
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_161

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 151
    .local v0, res:Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    .line 153
    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    .line 155
    const v1, 0x7f0d0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    .line 157
    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    .line 159
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    .line 161
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    .line 163
    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    .line 165
    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    .line 167
    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    .line 169
    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    .line 171
    const v1, 0x7f0d019b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    .line 174
    const v1, 0x7f0d0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 179
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 181
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 182
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 183
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 184
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00df

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 185
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 186
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 188
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 189
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 190
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 191
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 192
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 193
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 195
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 196
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 197
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 198
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 199
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 200
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 203
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 204
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 205
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 207
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 208
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 210
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 212
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    .line 214
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 217
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_161
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setupAccessibility(Landroid/content/Context;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v7, 0x7f0d018b

    const v6, 0x7f0d018a

    const v5, 0x7f0d0189

    const v4, 0x7f0a00de

    const/4 v3, 0x1

    .line 143
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 100
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    .line 130
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 147
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_161

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 151
    .local v0, res:Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    .line 153
    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    .line 155
    const v1, 0x7f0d0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    .line 157
    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    .line 159
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    .line 161
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    .line 163
    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    .line 165
    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    .line 167
    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    .line 169
    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    .line 171
    const v1, 0x7f0d019b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    .line 174
    const v1, 0x7f0d0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 179
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 181
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 182
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 183
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 184
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00df

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 185
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 186
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 188
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 189
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 190
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 191
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 192
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 193
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 195
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 196
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 197
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 198
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 199
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 200
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 203
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 204
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 205
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 207
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 208
    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 210
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 212
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    .line 214
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 217
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_161
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setupAccessibility(Landroid/content/Context;)V

    .line 144
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;)Ljava/util/Set;
    .registers 2
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    return-object v0
.end method

.method private setupAccessibility(Landroid/content/Context;)V
    .registers 4
    .parameter "ctx"

    .prologue
    .line 625
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1c

    invoke-static {p1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-nez v0, :cond_1c

    .line 627
    new-instance v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;-><init>(Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->install(Landroid/view/View;)V

    .line 631
    :cond_1c
    return-void
.end method

.method private updateAccessibility()V
    .registers 2

    .prologue
    .line 634
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_e

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->invalidateParent()V

    .line 638
    :cond_e
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 306
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 307
    .local v2, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 309
    .local v3, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_64

    :pswitch_14
    move v4, v5

    .line 341
    :cond_15
    :goto_15
    return v4

    .line 311
    :pswitch_16
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1c
    :goto_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 312
    .local v1, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 313
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    goto :goto_1c

    .line 321
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :pswitch_34
    iput-object v7, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 322
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_3c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 323
    .restart local v1       #item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_3c

    .line 325
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_4c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    move v4, v5

    .line 326
    goto :goto_15

    .line 330
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_51
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_61

    .line 331
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 332
    iput-object v7, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    goto :goto_15

    :cond_61
    move v4, v5

    .line 336
    goto :goto_15

    .line 309
    nop

    :pswitch_data_64
    .packed-switch 0x0
        :pswitch_16
        :pswitch_34
        :pswitch_14
        :pswitch_51
    .end packed-switch
.end method

.method public invalidate()V
    .registers 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 238
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->invalidate()V

    .line 239
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    if-eqz v4, :cond_40

    .line 241
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-ge v4, v5, :cond_3e

    .line 242
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    .local v1, sb:Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 245
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 246
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 248
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v4, :cond_41

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v4

    if-eqz v4, :cond_41

    move v0, v2

    .line 250
    .local v0, plusOnedByMe:Z
    :goto_30
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 252
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setFocusable(Z)V

    .line 255
    .end local v0           #plusOnedByMe:Z
    .end local v1           #sb:Ljava/lang/StringBuilder;
    :cond_3e
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 257
    :cond_40
    return-void

    .restart local v1       #sb:Ljava/lang/StringBuilder;
    :cond_41
    move v0, v3

    .line 248
    goto :goto_30
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 222
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onAttachedToWindow()V

    .line 224
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->updateAccessibility()V

    .line 226
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_9

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->onAvatarChanged(Ljava/lang/String;)V

    .line 351
    :cond_9
    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .registers 8
    .parameter "button"

    .prologue
    .line 355
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    if-eqz v3, :cond_50

    .line 356
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v3, :cond_50

    .line 357
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAlbumId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/plus/views/OneUpListener;->onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_50

    .line 359
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v3, :cond_51

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v3

    if-eqz v3, :cond_51

    const/4 v1, 0x1

    .line 361
    .local v1, plusOnedByMe:Z
    :goto_28
    if-eqz v1, :cond_53

    const v2, 0x7f0803e1

    .line 363
    .local v2, speakRes:I
    :goto_2d
    const/16 v3, 0x4000

    invoke-static {v3}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 365
    .local v0, notificationEvent:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 369
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 370
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-interface {v3, p0, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 374
    .end local v0           #notificationEvent:Landroid/view/accessibility/AccessibilityEvent;
    .end local v1           #plusOnedByMe:Z
    .end local v2           #speakRes:I
    :cond_50
    return-void

    .line 359
    :cond_51
    const/4 v1, 0x0

    goto :goto_28

    .line 361
    .restart local v1       #plusOnedByMe:Z
    :cond_53
    const v2, 0x7f0803e0

    goto :goto_2d
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 230
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDetachedFromWindow()V

    .line 232
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->uninstall()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    .line 234
    :cond_14
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 9
    .parameter "canvas"

    .prologue
    const/4 v6, 0x0

    .line 287
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDraw(Landroid/graphics/Canvas;)V

    .line 289
    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mBackgroundOffset:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_b3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_26
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_4b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_71

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_71
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_af

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 294
    :cond_af
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->updateAccessibility()V

    .line 295
    return-void

    .line 291
    :cond_b3
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_26
.end method

.method protected onMeasure(II)V
    .registers 18
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 261
    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onMeasure(II)V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    add-int v11, v0, v1

    .line 264
    .local v11, xStart:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingTop()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    sub-int v12, v0, v1

    .line 266
    .local v12, yStart:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getMeasuredWidth()I

    move-result v10

    .line 267
    .local v10, width:I
    sub-int v0, v10, v11

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    sub-int v9, v0, v1

    .line 269
    .local v9, contentWidth:I
    iput v12, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mBackgroundOffset:I

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    add-int/2addr v0, v11

    sget v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    add-int/2addr v2, v12

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    sget v4, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    add-int/2addr v4, v0

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    add-int/2addr v5, v2

    invoke-virtual {v3, v0, v2, v4, v5}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v0, :cond_168

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v0

    if-eqz v0, :cond_168

    const/4 v0, 0x1

    move v5, v0

    :goto_47
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v0, :cond_16c

    const/4 v0, 0x1

    :goto_4c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08039e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    add-int v0, v11, v9

    sget v3, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    sub-int v13, v0, v3

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    add-int v14, v12, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v5, :cond_174

    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    :goto_7d
    if-eqz v5, :cond_178

    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_81
    if-eqz v5, :cond_17c

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_85
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v0, v13, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1, v0, v14}, Landroid/graphics/Rect;->offsetTo(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    add-int/2addr v0, v11

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    add-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    add-int v14, v0, v1

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    add-int v8, v12, v0

    sub-int v0, v9, v14

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sub-int v3, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    int-to-float v2, v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, v2, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0, v14, v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v0

    add-int v13, v8, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    if-eqz v0, :cond_1b2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v0, " "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, v14, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v13

    :goto_13b
    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    sub-int/2addr v0, v12

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_180

    .line 274
    :goto_14a
    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    add-int/2addr v0, v12

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v10, v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setMeasuredDimension(II)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v0, :cond_15e

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    .line 280
    :cond_15e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_167

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    .line 283
    :cond_167
    return-void

    .line 271
    :cond_168
    const/4 v0, 0x0

    move v5, v0

    goto/16 :goto_47

    :cond_16c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v0

    goto/16 :goto_4c

    :cond_174
    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    goto/16 :goto_7d

    :cond_178
    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_81

    :cond_17c
    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_85

    .line 272
    :cond_180
    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    add-int v13, v12, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move v3, v9

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, v11, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v12

    goto :goto_14a

    :cond_1b2
    move v0, v13

    goto :goto_13b
.end method

.method public onRecycle()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 299
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    .line 300
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 301
    return-void
.end method

.method public setAlbum(Ljava/lang/String;)V
    .registers 2
    .parameter "albumId"

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAlbumId:Ljava/lang/String;

    .line 408
    return-void
.end method

.method public setCaption(Ljava/lang/String;)V
    .registers 3
    .parameter "caption"

    .prologue
    .line 414
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    .line 415
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 416
    invoke-static {p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    .line 418
    :cond_f
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 419
    return-void
.end method

.method public setDate(J)V
    .registers 5
    .parameter "date"

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 426
    .local v0, context:Landroid/content/Context;
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/util/Dates;->getAbbreviatedRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    .line 427
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 428
    return-void
.end method

.method public setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V
    .registers 2
    .parameter "oneUpListener"

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 381
    return-void
.end method

.method public setOwner(Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "ownerId"
    .parameter "ownerName"

    .prologue
    .line 387
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerId:Ljava/lang/String;

    .line 388
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    if-nez v0, :cond_20

    .line 390
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    .line 391
    const-string v0, "PhotoOneUp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "===> Author name was null for gaia id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_2b

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 397
    :cond_2b
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableUserImage;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerId:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    const/4 v6, 0x2

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 400
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 401
    return-void
.end method

.method public setPlusOne([B)V
    .registers 7
    .parameter "plusOneBytes"

    .prologue
    const/4 v4, 0x0

    .line 434
    iput-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 435
    if-eqz p1, :cond_2a

    .line 436
    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPlusOne;

    .line 437
    .local v0, plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    if-eqz v1, :cond_2a

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-eqz v1, :cond_2a

    .line 438
    new-instance v1, Lcom/google/android/apps/plus/content/DbPlusOneData;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {v1, v4, v2, v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>(Ljava/lang/String;IZ)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 442
    .end local v0           #plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    :cond_2a
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    .line 443
    return-void
.end method
