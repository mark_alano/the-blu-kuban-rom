.class public final Lcom/google/android/apps/plus/views/PlusOneDataUtils;
.super Ljava/lang/Object;
.source "PlusOneDataUtils.java"


# static fields
.field private static final sPlusOneWithYouStringIds:[[I

.field private static final sPlusOneWithoutYouStringIds:[[I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 22
    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_38

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_40

    aput-object v1, v0, v4

    new-array v1, v2, [I

    fill-array-data v1, :array_48

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/views/PlusOneDataUtils;->sPlusOneWithoutYouStringIds:[[I

    .line 29
    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_50

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_58

    aput-object v1, v0, v4

    new-array v1, v2, [I

    fill-array-data v1, :array_60

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/views/PlusOneDataUtils;->sPlusOneWithYouStringIds:[[I

    return-void

    .line 22
    nop

    :array_38
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0x9t 0x0t 0xet 0x7ft
    .end array-data

    :array_40
    .array-data 0x4
        0x86t 0x1t 0x8t 0x7ft
        0xbt 0x0t 0xet 0x7ft
    .end array-data

    :array_48
    .array-data 0x4
        0x88t 0x1t 0x8t 0x7ft
        0xdt 0x0t 0xet 0x7ft
    .end array-data

    .line 29
    :array_50
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0xat 0x0t 0xet 0x7ft
    .end array-data

    :array_58
    .array-data 0x4
        0x87t 0x1t 0x8t 0x7ft
        0xct 0x0t 0xet 0x7ft
    .end array-data

    :array_60
    .array-data 0x4
        0x89t 0x1t 0x8t 0x7ft
        0xet 0x0t 0xet 0x7ft
    .end array-data
.end method

.method public static getLongPlusOnesString(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPlusOne;)Ljava/lang/String;
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "plusOneData"

    .prologue
    .line 75
    if-nez p2, :cond_5

    .line 76
    const-string v10, ""

    .line 150
    :goto_4
    return-object v10

    .line 79
    :cond_5
    iget-object v10, p2, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    if-eqz v10, :cond_14

    iget-object v10, p2, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 80
    .local v4, plusOneCount:I
    :goto_f
    if-nez v4, :cond_16

    .line 81
    const-string v10, ""

    goto :goto_4

    .line 79
    .end local v4           #plusOneCount:I
    :cond_14
    const/4 v4, 0x0

    goto :goto_f

    .line 84
    .restart local v4       #plusOneCount:I
    :cond_16
    iget-object v10, p2, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-eqz v10, :cond_30

    iget-object v10, p2, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_30

    const/4 v5, 0x1

    .line 86
    .local v5, plusOnedByYou:Z
    :goto_23
    if-eqz v5, :cond_34

    .line 87
    const/4 v10, 0x1

    if-ne v4, v10, :cond_32

    .line 88
    const v10, 0x7f080185

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto :goto_4

    .line 84
    .end local v5           #plusOnedByYou:Z
    :cond_30
    const/4 v5, 0x0

    goto :goto_23

    .line 90
    .restart local v5       #plusOnedByYou:Z
    :cond_32
    add-int/lit8 v4, v4, -0x1

    .line 94
    :cond_34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v1, knownNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v10, p2, Lcom/google/api/services/plusi/model/DataPlusOne;->person:Ljava/util/List;

    if-eqz v10, :cond_6a

    .line 96
    iget-object v10, p2, Lcom/google/api/services/plusi/model/DataPlusOne;->person:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_43
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/DataPerson;

    .line 98
    .local v3, person:Lcom/google/api/services/plusi/model/DataPerson;
    iget-object v10, v3, Lcom/google/api/services/plusi/model/DataPerson;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {p1, v10}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_63

    .line 99
    iget-object v10, v3, Lcom/google/api/services/plusi/model/DataPerson;->nickName:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x2

    if-ge v10, v11, :cond_6a

    .line 102
    :cond_63
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_43

    .line 112
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v3           #person:Lcom/google/api/services/plusi/model/DataPerson;
    :cond_6a
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 113
    .local v2, knownNamesSize:I
    sub-int v9, v4, v2

    .line 114
    .local v9, unknownNamesSize:I
    if-lez v9, :cond_83

    const/4 v8, 0x1

    .line 116
    .local v8, unknownNamesIndex:I
    :goto_73
    if-eqz v5, :cond_85

    .line 117
    sget-object v10, Lcom/google/android/apps/plus/views/PlusOneDataUtils;->sPlusOneWithYouStringIds:[[I

    aget-object v10, v10, v2

    aget v7, v10, v8

    .line 122
    .local v7, stringId:I
    :goto_7b
    if-nez v8, :cond_bc

    .line 123
    packed-switch v2, :pswitch_data_10c

    .line 150
    :goto_80
    const-string v10, ""

    goto :goto_4

    .line 114
    .end local v7           #stringId:I
    .end local v8           #unknownNamesIndex:I
    :cond_83
    const/4 v8, 0x0

    goto :goto_73

    .line 119
    .restart local v8       #unknownNamesIndex:I
    :cond_85
    sget-object v10, Lcom/google/android/apps/plus/views/PlusOneDataUtils;->sPlusOneWithoutYouStringIds:[[I

    aget-object v10, v10, v2

    aget v7, v10, v8

    .restart local v7       #stringId:I
    goto :goto_7b

    .line 125
    :pswitch_8c
    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_4

    .line 128
    :pswitch_92
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v7, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_4

    .line 131
    :pswitch_a3
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v7, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_4

    .line 134
    :cond_bc
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 135
    .local v6, resources:Landroid/content/res/Resources;
    packed-switch v2, :pswitch_data_116

    goto :goto_80

    .line 137
    :pswitch_c4
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v6, v7, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_4

    .line 141
    :pswitch_d4
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v6, v7, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_4

    .line 145
    :pswitch_ec
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v6, v7, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_4

    .line 123
    :pswitch_data_10c
    .packed-switch 0x0
        :pswitch_8c
        :pswitch_92
        :pswitch_a3
    .end packed-switch

    .line 135
    :pswitch_data_116
    .packed-switch 0x0
        :pswitch_c4
        :pswitch_d4
        :pswitch_ec
    .end packed-switch
.end method
