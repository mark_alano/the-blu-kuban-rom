.class final Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;
.super Ljava/lang/Object;
.source "ProfileAboutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InfoRow"
.end annotation


# instance fields
.field container:Landroid/view/View;

.field public icon:Landroid/widget/ImageView;

.field public text:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .registers 5
    .parameter "parent"
    .parameter "resourceId"

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    const v1, 0x7f09009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    .line 106
    return-void
.end method
