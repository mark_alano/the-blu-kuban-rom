.class public abstract Lcom/google/android/apps/plus/views/StreamCardView;
.super Lcom/google/android/apps/plus/views/CardView;
.source "StreamCardView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;,
        Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;,
        Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;
    }
.end annotation


# static fields
.field protected static sAuthorBitmap:Landroid/graphics/Bitmap;

.field protected static sAuthorNameYOffset:I

.field protected static sAutoTextPaint:Landroid/text/TextPaint;

.field protected static sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

.field protected static sAvatarSize:I

.field protected static sCommentsBitmap:Landroid/graphics/Bitmap;

.field protected static sContentXPadding:I

.field protected static sContentYPadding:I

.field private static final sDampingInterpolator:Landroid/view/animation/Interpolator;

.field protected static sMediaCardBigHeightPercentage:F

.field protected static sMediaCardHeightPercentage:F

.field protected static sMediaShadowDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field protected static sNameTextPaint:Landroid/text/TextPaint;

.field protected static sRelativeTimeTextPaint:Landroid/text/TextPaint;

.field protected static sRelativeTimeYOffset:I

.field protected static sReshareBitmap:Landroid/graphics/Bitmap;

.field private static sStreamCardViewInitialized:Z

.field protected static sTagAlbumBitmaps:[Landroid/graphics/Bitmap;

.field protected static sTagBackgroundYPadding:I

.field protected static sTagDrawable:Landroid/graphics/drawable/Drawable;

.field protected static sTagHangoutBitmaps:[Landroid/graphics/Bitmap;

.field protected static sTagIconXPadding:I

.field protected static sTagIconYPaddingCheckin:I

.field protected static sTagIconYPaddingLocation:I

.field protected static sTagIconYPaddingWithPhoto:I

.field protected static sTagLinkBitmaps:[Landroid/graphics/Bitmap;

.field protected static sTagLocationBitmaps:[Landroid/graphics/Bitmap;

.field protected static sTagMusicBitmaps:[Landroid/graphics/Bitmap;

.field protected static sTagTextPaint:Landroid/text/TextPaint;

.field protected static sTagTextXPadding:I

.field protected static sTagVideoBitmaps:[Landroid/graphics/Bitmap;

.field protected static sTagYOffset:I

.field protected static sTiledStageDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field protected static sWhatsHotBitmap:Landroid/graphics/Bitmap;


# instance fields
.field protected mActivityId:Ljava/lang/String;

.field protected mAttribution:Ljava/lang/CharSequence;

.field protected mAttributionLayout:Landroid/text/StaticLayout;

.field protected mAuthorGaiaId:Ljava/lang/String;

.field protected mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

.field protected mAuthorName:Ljava/lang/String;

.field protected mAuthorNameLayout:Landroid/text/StaticLayout;

.field protected mAutoText:I

.field protected mAutoTextLayout:Landroid/text/StaticLayout;

.field protected mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field protected mContent:Ljava/lang/CharSequence;

.field protected mContentLayout:Landroid/text/StaticLayout;

.field protected mFillerContent:Ljava/lang/CharSequence;

.field protected mFillerContentLayout:Landroid/text/StaticLayout;

.field protected mInvisiblePlusOneButton:Z

.field protected mIsLimited:Z

.field protected mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field protected mOverridePlusOnedButtonDisplay:Z

.field protected mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field protected mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field protected mPopularPost:Z

.field protected mRelativeTime:Ljava/lang/String;

.field protected mRelativeTimeLayout:Landroid/text/StaticLayout;

.field protected mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mShakeAnimation:Ljava/lang/Runnable;

.field protected mStreamMediaClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;

.field protected mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

.field protected mTag:Ljava/lang/CharSequence;

.field protected mTagDrawableInstance:Landroid/graphics/drawable/Drawable;

.field protected mTagIcon:Landroid/graphics/Bitmap;

.field protected mTagLayout:Landroid/text/StaticLayout;

.field protected mTotalComments:I

.field private mViewedListener:Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/apps/plus/views/StreamCardView$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/StreamCardView$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/StreamCardView;->sDampingInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 174
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 175
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v7, 0x7f0d0136

    const v5, 0x7f0d012f

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 181
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 183
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 185
    .local v0, res:Landroid/content/res/Resources;
    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamCardView;->sStreamCardViewInitialized:Z

    if-nez v1, :cond_248

    .line 186
    sput-boolean v6, Lcom/google/android/apps/plus/views/StreamCardView;->sStreamCardViewInitialized:Z

    .line 188
    invoke-static {p1}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    .line 190
    invoke-static {p1, v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAuthorBitmap:Landroid/graphics/Bitmap;

    .line 192
    new-array v1, v4, [Landroid/graphics/Bitmap;

    const v2, 0x7f0200fe

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f02009a

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagAlbumBitmaps:[Landroid/graphics/Bitmap;

    .line 195
    new-array v1, v4, [Landroid/graphics/Bitmap;

    const v2, 0x7f020100

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f0200c8

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagLinkBitmaps:[Landroid/graphics/Bitmap;

    .line 198
    new-array v1, v4, [Landroid/graphics/Bitmap;

    const v2, 0x7f020101

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f020169

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagLocationBitmaps:[Landroid/graphics/Bitmap;

    .line 201
    new-array v1, v4, [Landroid/graphics/Bitmap;

    const v2, 0x7f020102

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f020106

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagMusicBitmaps:[Landroid/graphics/Bitmap;

    .line 204
    new-array v1, v4, [Landroid/graphics/Bitmap;

    const v2, 0x7f020103

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f020145

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagVideoBitmaps:[Landroid/graphics/Bitmap;

    .line 207
    new-array v1, v4, [Landroid/graphics/Bitmap;

    const v2, 0x7f0200ff

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x0

    aput-object v2, v1, v6

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagHangoutBitmaps:[Landroid/graphics/Bitmap;

    .line 211
    const v1, 0x7f0200a8

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sCommentsBitmap:Landroid/graphics/Bitmap;

    .line 212
    const v1, 0x7f0200f1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sReshareBitmap:Landroid/graphics/Bitmap;

    .line 213
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 214
    const v1, 0x7f020147

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sWhatsHotBitmap:Landroid/graphics/Bitmap;

    .line 216
    const v1, 0x7f020058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagDrawable:Landroid/graphics/drawable/Drawable;

    .line 217
    const v1, 0x7f0201d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sMediaShadowDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 219
    const v1, 0x7f020028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 220
    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTiledStageDrawable:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    .line 221
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTiledStageDrawable:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeY(Landroid/graphics/Shader$TileMode;)V

    .line 223
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 224
    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 225
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00c3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 226
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 227
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 228
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 231
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 232
    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 233
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00c9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 234
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 235
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 237
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 240
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 241
    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 242
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ca

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 243
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 244
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d013d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 245
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0140

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const v3, 0x7f0d0141

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const v4, 0x7f0d0142

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    const v5, 0x7f0a00cb

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 250
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d013d

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 252
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 253
    sput-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 254
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00cd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 255
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0137

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 256
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0137

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 259
    const v1, 0x7f0d0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    .line 260
    const v1, 0x7f0d0130

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAuthorNameYOffset:I

    .line 261
    const v1, 0x7f0d0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeYOffset:I

    .line 262
    const v1, 0x7f0d013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sContentXPadding:I

    .line 263
    const v1, 0x7f0d013b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    .line 264
    const v1, 0x7f0d013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagYOffset:I

    .line 265
    const v1, 0x7f0d013e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextXPadding:I

    .line 266
    const v1, 0x7f0d013f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagBackgroundYPadding:I

    .line 267
    const v1, 0x7f0d0143

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconXPadding:I

    .line 268
    const v1, 0x7f0d0144

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconYPaddingCheckin:I

    .line 270
    const v1, 0x7f0d0145

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconYPaddingLocation:I

    .line 272
    const v1, 0x7f0d0146

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconYPaddingWithPhoto:I

    .line 275
    const v1, 0x7f0d01a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sMediaCardHeightPercentage:F

    .line 276
    const v1, 0x7f0d01a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamCardView;->sMediaCardBigHeightPercentage:F

    .line 280
    :cond_248
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTagDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagDrawableInstance:Landroid/graphics/drawable/Drawable;

    .line 281
    return-void
.end method

.method static synthetic access$000()Landroid/view/animation/Interpolator;
    .registers 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardView;->sDampingInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/views/StreamCardView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mShakeAnimation:Ljava/lang/Runnable;

    return-object v0
.end method

.method protected static drawMediaTopAreaStageWithTiledBackground(Landroid/graphics/Canvas;II)V
    .registers 8
    .parameter "canvas"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 590
    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTiledStageDrawable:Landroid/graphics/drawable/BitmapDrawable;

    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sXDoublePadding:I

    add-int/2addr v3, p1

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sRightBorderPadding:I

    add-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sYPadding:I

    add-int/2addr v4, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 592
    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTiledStageDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 593
    return-void
.end method

.method private ensureOverridePlusOnedButton(I)V
    .registers 12
    .parameter "overrideCount"

    .prologue
    .line 895
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-nez v0, :cond_35

    .line 896
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v9

    .line 897
    .local v9, rect:Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08039e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 899
    .local v2, incrementedText:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    iget v7, v9, Landroid/graphics/Rect;->left:I

    iget v8, v9, Landroid/graphics/Rect;->top:I

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 904
    .end local v2           #incrementedText:Ljava/lang/String;
    .end local v9           #rect:Landroid/graphics/Rect;
    :cond_35
    return-void
.end method


# virtual methods
.method protected final createAuthorNameAndRelativeTimeLayoutOnSameLine$4868d301(II)I
    .registers 16
    .parameter "y"
    .parameter "width"

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    int-to-float v4, p2

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v2, v4, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 556
    .local v1, nameToDraw:Ljava/lang/CharSequence;
    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v3, v0

    .line 557
    .local v3, nameWidth:I
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f80

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int v12, p1, v0

    .line 561
    .local v12, returnHeight:I
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTime:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    int-to-float v4, p2

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v2, v4, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 563
    .local v5, relativeTimeToDraw:Ljava/lang/CharSequence;
    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v7, v0

    .line 565
    .local v7, relativeTimeWidth:I
    sub-int v0, p2, v3

    if-ge v7, v0, :cond_56

    .line 566
    new-instance v4, Landroid/text/StaticLayout;

    sget-object v6, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeTextPaint:Landroid/text/TextPaint;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f80

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v4 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    .line 569
    :cond_56
    return v12
.end method

.method protected final createMediaBottomArea(IIII)I
    .registers 11
    .parameter "originalX"
    .parameter "originalY"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 606
    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sYDoublePadding:I

    add-int/2addr v3, p4

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getMediaHeightPercentage()F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    add-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sAuthorNameYOffset:I

    add-int v2, v3, v4

    .line 609
    .local v2, y:I
    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v2, v3

    invoke-virtual {p0, p1, v3}, Lcom/google/android/apps/plus/views/StreamCardView;->setAuthorImagePosition(II)V

    .line 610
    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sContentXPadding:I

    .line 611
    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sContentXPadding:I

    add-int/2addr v3, v4

    sub-int/2addr p3, v3

    .line 613
    const/4 v1, 0x0

    .line 615
    .local v1, showingSomething:Z
    invoke-virtual {p0, v2, p3}, Lcom/google/android/apps/plus/views/StreamCardView;->createAuthorNameAndRelativeTimeLayoutOnSameLine$4868d301(II)I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int v2, v3, v4

    .line 616
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5f

    .line 617
    add-int v3, p4, p2

    sub-int/2addr v3, v2

    sget-object v4, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->descent()F

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v5}, Landroid/text/TextPaint;->ascent()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    div-int v0, v3, v4

    .line 619
    .local v0, maxLines:I
    if-lez v0, :cond_5f

    .line 620
    sget-object v3, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    invoke-static {v3, v4, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContentLayout:Landroid/text/StaticLayout;

    .line 622
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 623
    const/4 v1, 0x1

    .line 627
    .end local v0           #maxLines:I
    :cond_5f
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttribution:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_91

    .line 628
    add-int v3, p4, p2

    sub-int/2addr v3, v2

    sget-object v4, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->descent()F

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v5}, Landroid/text/TextPaint;->ascent()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    div-int v0, v3, v4

    .line 630
    .restart local v0       #maxLines:I
    if-lez v0, :cond_91

    .line 631
    sget-object v3, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttribution:Ljava/lang/CharSequence;

    invoke-static {v3, v4, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    .line 633
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 634
    const/4 v1, 0x1

    .line 638
    .end local v0           #maxLines:I
    :cond_91
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContent:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c3

    .line 639
    add-int v3, p4, p2

    sub-int/2addr v3, v2

    sget-object v4, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->descent()F

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v5}, Landroid/text/TextPaint;->ascent()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    div-int v0, v3, v4

    .line 641
    .restart local v0       #maxLines:I
    if-lez v0, :cond_c3

    .line 642
    sget-object v3, Lcom/google/android/apps/plus/views/StreamCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContent:Ljava/lang/CharSequence;

    invoke-static {v3, v4, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    .line 644
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 645
    const/4 v1, 0x1

    .line 649
    .end local v0           #maxLines:I
    :cond_c3
    if-nez v1, :cond_fa

    iget v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    if-eqz v3, :cond_fa

    .line 650
    add-int v3, p4, p2

    sub-int/2addr v3, v2

    sget-object v4, Lcom/google/android/apps/plus/views/StreamCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->descent()F

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v5}, Landroid/text/TextPaint;->ascent()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    div-int v0, v3, v4

    .line 652
    .restart local v0       #maxLines:I
    if-lez v0, :cond_fa

    .line 653
    sget-object v3, Lcom/google/android/apps/plus/views/StreamCardView;->sAutoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    .line 655
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 659
    .end local v0           #maxLines:I
    :cond_fa
    return v2
.end method

.method protected final createNameLayout$4868d301(II)I
    .registers 11
    .parameter "y"
    .parameter "width"

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    int-to-float v3, p2

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v2, v3, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 519
    .local v1, nameToDraw:Ljava/lang/CharSequence;
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sNameTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f80

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v3, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method protected final createPlusOneBar(III)I
    .registers 22
    .parameter "x"
    .parameter "y"
    .parameter "width"

    .prologue
    .line 759
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 760
    .local v3, context:Landroid/content/Context;
    add-int p1, p1, p3

    .line 762
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v2, :cond_172

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v2

    if-eqz v2, :cond_172

    const/16 v16, 0x1

    .line 763
    .local v16, plusOnedByMe:Z
    :goto_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v2, :cond_176

    const/16 v17, 0x1

    .line 764
    .local v17, plusOnes:I
    :goto_20
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f08039e

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v9, 0x1

    move/from16 v0, v17

    invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 766
    .local v4, plusOnesString:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 767
    new-instance v2, Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v16, :cond_180

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    :goto_4b
    if-eqz v16, :cond_184

    sget-object v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_4f
    if-eqz v16, :cond_188

    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_53
    move-object/from16 v8, p0

    move/from16 v9, p1

    move/from16 v10, p2

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 772
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int p1, p1, v2

    .line 773
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int p2, p2, v2

    .line 774
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 775
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 777
    if-eqz v16, :cond_e9

    .line 778
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 779
    new-instance v5, Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v7, Lcom/google/android/apps/plus/views/StreamCardView;->sReshareBitmap:Landroid/graphics/Bitmap;

    sget-object v8, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v9, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0803ee

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object v6, v3

    move-object/from16 v10, p0

    move/from16 v11, p1

    move/from16 v12, p2

    invoke-direct/range {v5 .. v13}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 782
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sget v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusBarXPadding:I

    add-int/2addr v2, v5

    sub-int p1, p1, v2

    .line 783
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 784
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 787
    :cond_e9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    if-lez v2, :cond_163

    .line 788
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 789
    .local v8, comments:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 790
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0e0023

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    invoke-virtual {v2, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 792
    .local v15, commentCountDesc:Ljava/lang/CharSequence;
    new-instance v5, Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v7, Lcom/google/android/apps/plus/views/StreamCardView;->sCommentsBitmap:Landroid/graphics/Bitmap;

    sget-object v9, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget-object v10, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v11, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/google/android/apps/plus/views/EventStreamCardView;

    if-eqz v2, :cond_18c

    const/4 v12, 0x0

    :goto_12e
    move-object v6, v3

    move/from16 v13, p1

    move/from16 v14, p2

    invoke-direct/range {v5 .. v15}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 796
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sget v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusBarXPadding:I

    add-int/2addr v2, v5

    sub-int p1, p1, v2

    .line 797
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 798
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 801
    .end local v8           #comments:Ljava/lang/String;
    .end local v15           #commentCountDesc:Ljava/lang/CharSequence;
    :cond_163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int v2, v2, p2

    return v2

    .line 762
    .end local v4           #plusOnesString:Ljava/lang/String;
    .end local v16           #plusOnedByMe:Z
    .end local v17           #plusOnes:I
    :cond_172
    const/16 v16, 0x0

    goto/16 :goto_18

    .line 763
    .restart local v16       #plusOnedByMe:Z
    :cond_176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v17

    goto/16 :goto_20

    .line 767
    .restart local v4       #plusOnesString:Ljava/lang/String;
    .restart local v17       #plusOnes:I
    :cond_180
    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    goto/16 :goto_4b

    :cond_184
    sget-object v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_4f

    :cond_188
    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_53

    .restart local v8       #comments:Ljava/lang/String;
    .restart local v15       #commentCountDesc:Ljava/lang/CharSequence;
    :cond_18c
    move-object/from16 v12, p0

    .line 792
    goto :goto_12e
.end method

.method protected final createTagBar(III)I
    .registers 12
    .parameter "x"
    .parameter "y"
    .parameter "width"

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTag:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4f

    .line 712
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextXPadding:I

    .line 713
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextXPadding:I

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr p3, v0

    .line 714
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTagYOffset:I

    add-int/2addr p2, v0

    .line 715
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_23

    .line 716
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconXPadding:I

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconXPadding:I

    add-int/2addr v0, v2

    sub-int/2addr p3, v0

    .line 720
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTag:Ljava/lang/CharSequence;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v2, p3, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->smartEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 722
    .local v1, tagTextToDraw:Ljava/lang/CharSequence;
    sget-object v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int p3, v0

    .line 723
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f80

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p2, v0

    .line 727
    .end local v1           #tagTextToDraw:Ljava/lang/CharSequence;
    :cond_4f
    return p2
.end method

.method protected final drawAuthorImage$494937f0(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    .prologue
    const/4 v4, 0x0

    .line 497
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-nez v2, :cond_6

    .line 514
    :cond_5
    :goto_5
    return-void

    .line 502
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_34

    .line 503
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 508
    .local v1, userImageToDraw:Landroid/graphics/Bitmap;
    :goto_14
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 509
    .local v0, rect:Landroid/graphics/Rect;
    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 510
    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamCardView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 511
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->isClicked()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 512
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->drawSelectionRect(Landroid/graphics/Canvas;)V

    goto :goto_5

    .line 505
    .end local v0           #rect:Landroid/graphics/Rect;
    .end local v1           #userImageToDraw:Landroid/graphics/Bitmap;
    :cond_34
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAuthorBitmap:Landroid/graphics/Bitmap;

    .restart local v1       #userImageToDraw:Landroid/graphics/Bitmap;
    goto :goto_14
.end method

.method protected final drawAuthorName(Landroid/graphics/Canvas;II)I
    .registers 6
    .parameter "canvas"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1c

    .line 526
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 528
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 529
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p3, v0

    .line 532
    :cond_1c
    return p3
.end method

.method protected final drawMediaBottomArea$1be95c43(Landroid/graphics/Canvas;III)I
    .registers 9
    .parameter "canvas"
    .parameter "originalX"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 664
    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sYPadding:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p4

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getMediaHeightPercentage()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    add-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sAuthorNameYOffset:I

    add-int v0, v1, v2

    .line 666
    .local v0, originalY:I
    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/StreamCardView;->drawAuthorImage$494937f0(Landroid/graphics/Canvas;)V

    .line 668
    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sContentXPadding:I

    add-int/2addr v1, v2

    add-int/2addr p2, v1

    .line 669
    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sContentXPadding:I

    add-int/2addr v1, v2

    sub-int/2addr p3, v1

    .line 671
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamCardView;->drawAuthorName(Landroid/graphics/Canvas;II)I

    move-result v0

    .line 672
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_43

    .line 673
    add-int v1, p2, p3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sRelativeTimeYOffset:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->drawRelativeTimeLayout(Landroid/graphics/Canvas;II)I

    .line 677
    :cond_43
    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v0, v1

    .line 679
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContentLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_65

    .line 680
    int-to-float v1, p2

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 681
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 682
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 683
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 686
    :cond_65
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_84

    .line 687
    int-to-float v1, p2

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 688
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 689
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 690
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 693
    :cond_84
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_a3

    .line 694
    int-to-float v1, p2

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 695
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 696
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 697
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 700
    :cond_a3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_c2

    .line 701
    int-to-float v1, p2

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 702
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 703
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 704
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sContentYPadding:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 707
    :cond_c2
    return v0
.end method

.method protected final drawMediaTopAreaShadow(Landroid/graphics/Canvas;II)V
    .registers 11
    .parameter "canvas"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 596
    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sXPadding:I

    mul-int/lit8 v2, v2, 0x2

    add-int v1, p2, v2

    .line 597
    .local v1, mediaWidth:I
    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sYPadding:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p3

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getMediaHeightPercentage()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 598
    .local v0, mediaHeight:I
    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sMediaShadowDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    sget v4, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    sget v5, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    add-int/2addr v5, v1

    sget v6, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    add-int/2addr v6, v0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 601
    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sMediaShadowDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 602
    return-void
.end method

.method protected final drawMediaTopAreaStage(Landroid/graphics/Canvas;IIZLandroid/graphics/Paint;)V
    .registers 15
    .parameter "canvas"
    .parameter "width"
    .parameter "height"
    .parameter "hasBitmap"
    .parameter "mediaBackgroundPaint"

    .prologue
    .line 574
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sXPadding:I

    mul-int/lit8 v0, v0, 0x2

    add-int v8, p2, v0

    .line 575
    .local v8, mediaWidth:I
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sYPadding:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p3

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getMediaHeightPercentage()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .line 576
    .local v7, mediaHeight:I
    if-eqz p4, :cond_28

    .line 577
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    int-to-float v1, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    int-to-float v2, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    add-int/2addr v0, v8

    int-to-float v3, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    add-int/2addr v0, v7

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 587
    :goto_27
    return-void

    .line 580
    :cond_28
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getBackgroundPattern(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v6

    .line 583
    .local v6, bitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    add-int/2addr v2, v8

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sTopBorderPadding:I

    add-int/2addr v3, v7

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 585
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_27
.end method

.method protected final drawPlusOneBar(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    .prologue
    .line 805
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mInvisiblePlusOneButton:Z

    if-nez v0, :cond_d

    .line 806
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButtonDisplay:Z

    if-nez v0, :cond_20

    .line 807
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    .line 813
    :cond_d
    :goto_d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_16

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    .line 816
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1f

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    .line 819
    :cond_1f
    return-void

    .line 808
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_d

    .line 809
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    goto :goto_d
.end method

.method protected final drawRelativeTimeLayout(Landroid/graphics/Canvas;II)I
    .registers 6
    .parameter "canvas"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1c

    .line 545
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 547
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p3, v0

    .line 550
    :cond_1c
    return p3
.end method

.method protected final drawTagBarIconAndBackground(Landroid/graphics/Canvas;II)V
    .registers 11
    .parameter "canvas"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 731
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    if-eqz v2, :cond_79

    .line 732
    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTagYOffset:I

    add-int/2addr p3, v2

    .line 733
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextXPadding:I

    mul-int/lit8 v3, v3, 0x2

    add-int v1, v2, v3

    .line 734
    .local v1, width:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_21

    .line 735
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconXPadding:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 738
    :cond_21
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagDrawableInstance:Landroid/graphics/drawable/Drawable;

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sTagBackgroundYPadding:I

    sub-int v3, p3, v3

    add-int v4, p2, v1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v5, p3

    sget v6, Lcom/google/android/apps/plus/views/StreamCardView;->sTagBackgroundYPadding:I

    add-int/2addr v5, v6

    invoke-virtual {v2, p2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 740
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagDrawableInstance:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 742
    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTagTextXPadding:I

    add-int/2addr p2, v2

    .line 744
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_68

    .line 745
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p3

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconYPaddingWithPhoto:I

    add-int v0, v2, v3

    .line 747
    .local v0, iconY:I
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    int-to-float v3, p2

    int-to-float v4, v0

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 748
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/StreamCardView;->sTagIconXPadding:I

    add-int/2addr v2, v3

    add-int/2addr p2, v2

    .line 751
    .end local v0           #iconY:I
    :cond_68
    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 752
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 753
    neg-int v2, p2

    int-to-float v2, v2

    neg-int v3, p3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 756
    .end local v1           #width:I
    :cond_79
    return-void
.end method

.method protected final drawWhatsHot(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    .prologue
    .line 822
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPopularPost:Z

    if-eqz v1, :cond_1d

    .line 823
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sBottomBorderPadding:I

    sub-int/2addr v1, v2

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sWhatsHotBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v0, v1, v2

    .line 824
    .local v0, y:I
    sget-object v1, Lcom/google/android/apps/plus/views/StreamCardView;->sWhatsHotBitmap:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sLeftBorderPadding:I

    int-to-float v2, v2

    int-to-float v3, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 826
    .end local v0           #y:I
    :cond_1d
    return-void
.end method

.method protected formatLocationName(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "locationName"

    .prologue
    .line 983
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getActivityId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 840
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDesiredHeight()I
    .registers 2

    .prologue
    .line 967
    const/4 v0, 0x0

    return v0
.end method

.method public getDesiredWidth()I
    .registers 2

    .prologue
    .line 963
    const/4 v0, 0x0

    return v0
.end method

.method public getLinkTitle()Ljava/lang/String;
    .registers 2

    .prologue
    .line 975
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLinkUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 979
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaCount()I
    .registers 2

    .prologue
    .line 971
    const/4 v0, 0x0

    return v0
.end method

.method protected final getMediaHeightPercentage()F
    .registers 3

    .prologue
    .line 857
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mDisplaySizeType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mDisplaySizeType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_d

    .line 859
    :cond_a
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sMediaCardBigHeightPercentage:F

    .line 861
    :goto_c
    return v0

    :cond_d
    sget v0, Lcom/google/android/apps/plus/views/StreamCardView;->sMediaCardHeightPercentage:F

    goto :goto_c
.end method

.method public getMediaLinkUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 959
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2

    .prologue
    .line 955
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getPlusOneButtonAnimationCopies()Landroid/util/Pair;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableButton;",
            "Lcom/google/android/apps/plus/views/ClickableButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 879
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getX()F

    move-result v5

    float-to-int v3, v5

    .line 880
    .local v3, x:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getY()F

    move-result v5

    float-to-int v4, v5

    .line 881
    .local v4, y:I
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5, v3, v4}, Lcom/google/android/apps/plus/views/ClickableButton;->createAbsoluteCoordinatesCopy(II)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v1

    .line 884
    .local v1, originalButton:Lcom/google/android/apps/plus/views/ClickableButton;
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v5, :cond_24

    const/4 v0, 0x1

    .line 886
    .local v0, incrementedCount:I
    :goto_15
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/StreamCardView;->ensureOverridePlusOnedButton(I)V

    .line 888
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5, v3, v4}, Lcom/google/android/apps/plus/views/ClickableButton;->createAbsoluteCoordinatesCopy(II)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v2

    .line 891
    .local v2, overridePlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;
    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v5

    .line 884
    .end local v0           #incrementedCount:I
    .end local v2           #overridePlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;
    :cond_24
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v5

    add-int/lit8 v0, v5, 0x1

    goto :goto_15
.end method

.method public init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V
    .registers 29
    .parameter "cursor"
    .parameter "displaySizeType"
    .parameter "size"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewedListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"

    .prologue
    .line 288
    invoke-super/range {p0 .. p8}, Lcom/google/android/apps/plus/views/CardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getContext()Landroid/content/Context;

    move-result-object v11

    .line 292
    .local v11, context:Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 294
    .local v18, res:Landroid/content/res/Resources;
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/StreamCardView;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    .line 295
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/StreamCardView;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;

    .line 297
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    .line 298
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorGaiaId:Ljava/lang/String;

    .line 299
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    .line 300
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    if-nez v2, :cond_44

    .line 301
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    .line 304
    :cond_44
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 305
    new-instance v2, Lcom/google/android/apps/plus/views/ClickableUserImage;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorGaiaId:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    const/4 v8, 0x2

    move-object/from16 v3, p0

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 309
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v11, v2, v3}, Lcom/google/android/apps/plus/util/Dates;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTime:Ljava/lang/String;

    .line 313
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 315
    .local v9, contentFlags:J
    const-wide/16 v2, 0x2

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_22f

    .line 316
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    .line 321
    :goto_a2
    const-wide/16 v2, 0x1

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c1

    .line 322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_236

    .line 323
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    .line 329
    :cond_c1
    :goto_c1
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 330
    .local v14, originalAuthorId:Ljava/lang/String;
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 331
    .local v15, originalAuthorName:Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f0

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f0

    .line 332
    const v2, 0x7f0800e9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v15, v3, v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttribution:Ljava/lang/CharSequence;

    .line 336
    :cond_f0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTag:Ljava/lang/CharSequence;

    if-nez v2, :cond_123

    const-wide/16 v2, 0x8

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_123

    .line 337
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    .line 338
    .local v13, locationData:[B
    if-eqz v13, :cond_123

    .line 339
    invoke-static {v13}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v12

    .line 340
    .local v12, location:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-virtual {v12}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->formatLocationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTag:Ljava/lang/CharSequence;

    .line 341
    sget-object v2, Lcom/google/android/apps/plus/views/StreamCardView;->sTagLocationBitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    .line 345
    .end local v12           #location:Lcom/google/android/apps/plus/content/DbLocation;
    .end local v13           #locationData:[B
    :cond_123
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    .line 347
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v16

    .line 349
    .local v16, plusOneBytes:[B
    if-eqz v16, :cond_244

    .line 350
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 355
    :goto_13f
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_150

    .line 356
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/StreamCardView;->mViewedListener:Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;

    .line 358
    :cond_150
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_24b

    const/4 v2, 0x1

    :goto_15b
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mIsLimited:Z

    .line 359
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_24e

    const/4 v2, 0x1

    :goto_16b
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPopularPost:Z

    .line 361
    const-wide/16 v2, 0x1000

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_251

    .line 362
    const v2, 0x7f0803bf

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    .line 385
    :goto_17f
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 387
    .local v19, sb:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 388
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    if-eqz v2, :cond_1a2

    .line 389
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 391
    :cond_1a2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTime:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContent:Ljava/lang/CharSequence;

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttribution:Ljava/lang/CharSequence;

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTag:Ljava/lang/CharSequence;

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 397
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    if-lez v2, :cond_1fb

    .line 398
    const v2, 0x7f0e000f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    :cond_1fb
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v2, :cond_2d8

    const/16 v17, 0x0

    .line 403
    .local v17, plusOnes:I
    :goto_203
    if-lez v17, :cond_21f

    .line 404
    const v2, 0x7f0e0025

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    :cond_21f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 409
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->setFocusable(Z)V

    .line 410
    return-void

    .line 318
    .end local v14           #originalAuthorId:Ljava/lang/String;
    .end local v15           #originalAuthorName:Ljava/lang/String;
    .end local v16           #plusOneBytes:[B
    .end local v17           #plusOnes:I
    .end local v19           #sb:Ljava/lang/StringBuilder;
    :cond_22f
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    goto/16 :goto_a2

    .line 325
    :cond_236
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContent:Ljava/lang/CharSequence;

    goto/16 :goto_c1

    .line 352
    .restart local v14       #originalAuthorId:Ljava/lang/String;
    .restart local v15       #originalAuthorName:Ljava/lang/String;
    .restart local v16       #plusOneBytes:[B
    :cond_244
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    goto/16 :goto_13f

    .line 358
    :cond_24b
    const/4 v2, 0x0

    goto/16 :goto_15b

    .line 359
    :cond_24e
    const/4 v2, 0x0

    goto/16 :goto_16b

    .line 363
    :cond_251
    const-wide/16 v2, 0x4000

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_263

    .line 364
    const v2, 0x7f0803c0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 365
    :cond_263
    const-wide/16 v2, 0x40

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_275

    .line 366
    const v2, 0x7f0803bc

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 367
    :cond_275
    const-wide/16 v2, 0x80

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_287

    .line 368
    const v2, 0x7f0803be

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 369
    :cond_287
    const-wide/32 v2, 0x8004

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_29a

    .line 371
    const v2, 0x7f0803c2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 372
    :cond_29a
    const-wide/16 v2, 0x20

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2ac

    .line 373
    const v2, 0x7f0803bd

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 374
    :cond_2ac
    const-wide/16 v2, 0x8

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2be

    .line 375
    const v2, 0x7f0803c1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 376
    :cond_2be
    const-wide/32 v2, 0x10000

    and-long/2addr v2, v9

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2d1

    .line 377
    const v2, 0x7f0803c3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 379
    :cond_2d1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    goto/16 :goto_17f

    .line 402
    .restart local v19       #sb:Ljava/lang/StringBuilder;
    :cond_2d8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v17

    goto/16 :goto_203
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_9

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->onAvatarChanged(Ljava/lang/String;)V

    .line 833
    :cond_9
    return-void
.end method

.method public onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .registers 5
    .parameter "button"

    .prologue
    .line 867
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    if-eqz v0, :cond_11

    .line 868
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v0, :cond_12

    .line 869
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;->onPlusOneClicked(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Lcom/google/android/apps/plus/views/StreamCardView;)V

    .line 876
    :cond_11
    :goto_11
    return-void

    .line 870
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v0, :cond_20

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mIsLimited:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;->onReshareClicked(Ljava/lang/String;Z)V

    goto :goto_11

    .line 872
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v0, :cond_11

    .line 873
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;->onCommentsClicked$1b4287ec(Lcom/google/android/apps/plus/views/StreamCardView;)V

    goto :goto_11
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mShakeAnimation:Ljava/lang/Runnable;

    if-nez v0, :cond_c

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mShakeAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamCardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mShakeAnimation:Ljava/lang/Runnable;

    .line 418
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->clearAnimation()V

    .line 420
    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onDetachedFromWindow()V

    .line 421
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter "canvas"

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 481
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/CardView;->onDraw(Landroid/graphics/Canvas;)V

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mViewedListener:Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;

    if-eqz v0, :cond_15

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mViewedListener:Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;->onStreamCardViewed(Ljava/lang/String;)V

    .line 485
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mViewedListener:Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;

    .line 488
    :cond_15
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/4 v1, 0x0

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 472
    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/plus/views/StreamCardView;->setMeasuredDimension(II)V

    .line 476
    :goto_8
    return-void

    .line 474
    :cond_9
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/views/CardView;->onMeasure(II)V

    goto :goto_8
.end method

.method public onRecycle()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 425
    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onRecycle()V

    .line 427
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mActivityId:Ljava/lang/String;

    .line 428
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorGaiaId:Ljava/lang/String;

    .line 429
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorName:Ljava/lang/String;

    .line 430
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 431
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorNameLayout:Landroid/text/StaticLayout;

    .line 433
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTime:Ljava/lang/String;

    .line 434
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    .line 436
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTag:Ljava/lang/CharSequence;

    .line 437
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagIcon:Landroid/graphics/Bitmap;

    .line 438
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTagLayout:Landroid/text/StaticLayout;

    .line 440
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContent:Ljava/lang/CharSequence;

    .line 441
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mContentLayout:Landroid/text/StaticLayout;

    .line 443
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttribution:Ljava/lang/CharSequence;

    .line 444
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    .line 446
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContent:Ljava/lang/CharSequence;

    .line 447
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    .line 449
    iput v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoText:I

    .line 450
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAutoTextLayout:Landroid/text/StaticLayout;

    .line 452
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 453
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 454
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 455
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 456
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mInvisiblePlusOneButton:Z

    .line 457
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButtonDisplay:Z

    .line 459
    iput v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mTotalComments:I

    .line 460
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 462
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;

    .line 463
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mIsLimited:Z

    .line 464
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPopularPost:Z

    .line 466
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mViewedListener:Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;

    .line 467
    return-void
.end method

.method public final overridePlusOnedButtonDisplay(ZI)V
    .registers 5
    .parameter "override"
    .parameter "overrideCount"

    .prologue
    const/4 v1, 0x0

    .line 844
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButtonDisplay:Z

    .line 845
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButtonDisplay:Z

    if-eqz v0, :cond_1b

    .line 846
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/StreamCardView;->ensureOverridePlusOnedButton(I)V

    .line 847
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ClickableButton;->setListener(Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)V

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ClickableButton;->setListener(Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)V

    .line 849
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mInvisiblePlusOneButton:Z

    .line 853
    :goto_17
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->invalidate()V

    .line 854
    return-void

    .line 851
    :cond_1b
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    goto :goto_17
.end method

.method protected final setAuthorImagePosition(II)V
    .registers 6
    .parameter "left"
    .parameter "top"

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_f

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    sget v1, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    add-int/2addr v1, p1

    sget v2, Lcom/google/android/apps/plus/views/StreamCardView;->sAvatarSize:I

    add-int/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    .line 494
    :cond_f
    return-void
.end method

.method public final startDelayedShakeAnimation()V
    .registers 9

    .prologue
    const-wide/16 v6, 0x267

    const v5, 0x3f733333

    .line 907
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mInvisiblePlusOneButton:Z

    .line 908
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->invalidate()V

    .line 912
    iget v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mDisplaySizeType:I

    packed-switch v2, :pswitch_data_5c

    .line 927
    const/high16 v0, -0x4040

    .line 928
    .local v0, rotX:F
    const/high16 v1, 0x4000

    .line 933
    .local v1, rotY:F
    :goto_14
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_4a

    .line 934
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v3, 0x12c

    invoke-virtual {v2, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->rotationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamCardView;->sDampingInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 952
    :goto_3d
    return-void

    .line 914
    .end local v0           #rotX:F
    .end local v1           #rotY:F
    :pswitch_3e
    instance-of v2, p0, Lcom/google/android/apps/plus/views/TextCardView;

    if-eqz v2, :cond_47

    .line 915
    const/high16 v0, -0x3fe0

    .line 919
    .restart local v0       #rotX:F
    :goto_44
    const/high16 v1, 0x4020

    .line 920
    .restart local v1       #rotY:F
    goto :goto_14

    .line 917
    .end local v0           #rotX:F
    .end local v1           #rotY:F
    :cond_47
    const/high16 v0, -0x4000

    .restart local v0       #rotX:F
    goto :goto_44

    .line 938
    .restart local v1       #rotY:F
    :cond_4a
    new-instance v2, Lcom/google/android/apps/plus/views/StreamCardView$2;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/plus/views/StreamCardView$2;-><init>(Lcom/google/android/apps/plus/views/StreamCardView;FF)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mShakeAnimation:Ljava/lang/Runnable;

    .line 949
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mShakeAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/StreamCardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 950
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCardView;->mShakeAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v6, v7}, Lcom/google/android/apps/plus/views/StreamCardView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3d

    .line 912
    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_3e
    .end packed-switch
.end method
