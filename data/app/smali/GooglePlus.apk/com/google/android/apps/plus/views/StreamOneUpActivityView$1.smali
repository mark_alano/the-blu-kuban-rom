.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;
.super Ljava/lang/Object;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V
    .registers 2
    .parameter

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .registers 6
    .parameter "span"

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 281
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/OneUpListener;->onSourceAppNameClick$1b7460f0(Ljava/util/List;)V

    .line 288
    :cond_2c
    :goto_2c
    return-void

    .line 285
    :cond_2d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v0

    new-instance v1, Landroid/text/style/URLSpan;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "acl:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/OneUpListener;->onSpanClick(Landroid/text/style/URLSpan;)V

    goto :goto_2c
.end method
