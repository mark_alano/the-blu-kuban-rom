.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;
.super Ljava/lang/Object;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V
    .registers 2
    .parameter

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .registers 8
    .parameter "span"

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppData:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$400(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    #getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$500(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;

    move-result-object v5

    move-object v4, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/plus/views/OneUpListener;->onSourceAppContentClick(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/text/style/URLSpan;Ljava/lang/String;)V

    .line 299
    :cond_2a
    return-void
.end method
