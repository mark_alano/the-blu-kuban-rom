.class public Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.super Lcom/google/android/apps/plus/views/OneUpBaseView;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;
    }
.end annotation


# static fields
.field private static sActionBarBackgroundPaint:Landroid/graphics/Paint;

.field private static sAvatarMarginLeft:I

.field private static sAvatarMarginRight:I

.field private static sAvatarMarginTop:I

.field private static sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sCheckInIconBitmap:Landroid/graphics/Bitmap;

.field private static sContentPaint:Landroid/text/TextPaint;

.field private static sDatePaint:Landroid/text/TextPaint;

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sFontSpacing:F

.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sIsMuteColor:I

.field private static sLinkedBackgroundPaint:Landroid/graphics/Paint;

.field private static sLinkedBodyPaint:Landroid/text/TextPaint;

.field private static sLinkedBorderPaint:Landroid/graphics/Paint;

.field private static sLinkedBorderRadius:F

.field private static sLinkedBorderSize:I

.field private static sLinkedHeaderPaint:Landroid/text/TextPaint;

.field private static sLinkedIconBitmap:Landroid/graphics/Bitmap;

.field private static sLinkedIconMarginRight:I

.field private static sLinkedInnerMargin:I

.field private static sLocationIconBitmap:Landroid/graphics/Bitmap;

.field private static sLocationIconMarginRight:I

.field private static sLocationIconMarginTop:I

.field private static sLocationPaint:Landroid/text/TextPaint;

.field private static sMarginBottom:I

.field private static sMarginLeft:I

.field private static sMarginRight:I

.field private static sNameMarginTop:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sPlaceReviewAspectsMarginBottom:I

.field private static sPlaceReviewAspectsMarginTop:I

.field private static sPlaceReviewDividerMargin:I

.field private static sPlaceReviewDividerPaint:Landroid/graphics/Paint;

.field private static sPlusOneButtonMarginLeft:I

.field private static sPlusOneButtonMarginRight:I

.field private static sReshareBackgroundPaint:Landroid/graphics/Paint;

.field private static sReshareBodyPaint:Landroid/text/TextPaint;

.field private static sReshareBorderPaint:Landroid/graphics/Paint;

.field private static sReshareBorderRadius:F

.field private static sReshareBorderSize:I

.field private static sReshareHeaderPaint:Landroid/text/TextPaint;

.field private static sReshareInnerMargin:I

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sSkyjamIconBitmap:Landroid/graphics/Bitmap;

.field private static sTitleMarginBottom:I


# instance fields
.field private final mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mAclText:Ljava/lang/String;

.field private mActivityId:Ljava/lang/String;

.field private mAnnotation:Landroid/text/Spannable;

.field private mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mAuthorId:Ljava/lang/String;

.field private mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

.field private mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mAuthorName:Ljava/lang/String;

.field private mBackgroundOffset:I

.field private mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCreationSource:Ljava/lang/String;

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/String;

.field private mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mIsCheckin:Z

.field private mLinkedBody:Landroid/text/Spannable;

.field private mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mLinkedContentBorder:Landroid/graphics/RectF;

.field private mLinkedHeader:Landroid/text/Spannable;

.field private mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mLinkedIconRect:Landroid/graphics/Rect;

.field private mLocation:Landroid/text/Spannable;

.field private final mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

.field private mLocationIcon:Landroid/graphics/Bitmap;

.field private mLocationIconRect:Landroid/graphics/Rect;

.field private mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mMuteState:Ljava/lang/String;

.field private mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

.field private mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mPlaceReviewDividerRect:Landroid/graphics/Rect;

.field protected mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field private mReshareBody:Landroid/text/Spannable;

.field private mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mReshareContentBorder:Landroid/graphics/RectF;

.field private mReshareHeader:Landroid/text/Spannable;

.field private mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private final mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mSkyjamContentBorder:Landroid/graphics/RectF;

.field private mSkyjamHeader:Landroid/text/Spannable;

.field private mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mSkyjamIconRect:Landroid/graphics/Rect;

.field private mSkyjamSubheader1:Landroid/text/Spannable;

.field private mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mSkyjamSubheader2:Landroid/text/Spannable;

.field private mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mSourceAppData:Ljava/lang/String;

.field private final mSourceAppPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mTitle:Landroid/text/Spannable;

.field private mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    .prologue
    const v7, 0x7f0d018b

    const v6, 0x7f0d018a

    const v5, 0x7f0d0189

    const v4, 0x7f0a00de

    const/4 v3, 0x1

    .line 340
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;)V

    .line 172
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    .line 276
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 291
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 302
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 322
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 352
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_3a9

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 354
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 356
    .local v0, res:Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    .line 358
    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    .line 360
    const v1, 0x7f0d0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    .line 362
    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginBottom:I

    .line 364
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginLeft:I

    .line 366
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginRight:I

    .line 368
    const v1, 0x7f0d016e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTitleMarginBottom:I

    .line 370
    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginTop:I

    .line 372
    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginLeft:I

    .line 374
    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginRight:I

    .line 376
    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNameMarginTop:I

    .line 378
    const v1, 0x7f0d0171

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    .line 380
    const v1, 0x7f0d016f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderSize:I

    .line 382
    const v1, 0x7f0d0170

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    .line 384
    const v1, 0x7f0d0172

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    .line 386
    const v1, 0x7f0d0175

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    .line 388
    const v1, 0x7f0d0173

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderSize:I

    .line 390
    const v1, 0x7f0d0174

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderRadius:F

    .line 392
    const v1, 0x7f0d0176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginTop:I

    .line 394
    const v1, 0x7f0d0177

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginRight:I

    .line 397
    const v1, 0x7f0d0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginRight:I

    .line 399
    const v1, 0x7f0d0197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerMargin:I

    .line 401
    const v1, 0x7f0d019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginBottom:I

    .line 403
    const v1, 0x7f0d0199

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginTop:I

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 408
    const v1, 0x7f0200c8

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    .line 409
    const v1, 0x7f020106

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    .line 410
    const v1, 0x7f0200a2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sCheckInIconBitmap:Landroid/graphics/Bitmap;

    .line 411
    const v1, 0x7f020169

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconBitmap:Landroid/graphics/Bitmap;

    .line 413
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 415
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 416
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 417
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 418
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00df

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 419
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 420
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 422
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 423
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 424
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 425
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 426
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 427
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 429
    const v1, 0x7f0a00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sIsMuteColor:I

    .line 431
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 432
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 433
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 434
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 435
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 436
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 439
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 440
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 441
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 442
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 443
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 444
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 446
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018c

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 449
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 450
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 451
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 452
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 453
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 455
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018d

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 458
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 459
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 460
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 461
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 462
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 464
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018e

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 467
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 468
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 469
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ea

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 470
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 471
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 473
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018f

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 476
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 477
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 478
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00eb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 479
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 480
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0190

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 481
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0190

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 484
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 485
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 486
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 488
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 489
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 490
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 492
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 493
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 494
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 495
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 497
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 498
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 500
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 502
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 503
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 504
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 505
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 507
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 508
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 510
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 512
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    .line 514
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 515
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00fb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 517
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0d0198

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 520
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 523
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_3a9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setupAccessibility(Landroid/content/Context;)V

    .line 341
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v7, 0x7f0d018b

    const v6, 0x7f0d018a

    const v5, 0x7f0d0189

    const v4, 0x7f0a00de

    const/4 v3, 0x1

    .line 344
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 172
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    .line 276
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 291
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 302
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 322
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 352
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_3a9

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 354
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 356
    .local v0, res:Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    .line 358
    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    .line 360
    const v1, 0x7f0d0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    .line 362
    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginBottom:I

    .line 364
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginLeft:I

    .line 366
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginRight:I

    .line 368
    const v1, 0x7f0d016e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTitleMarginBottom:I

    .line 370
    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginTop:I

    .line 372
    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginLeft:I

    .line 374
    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginRight:I

    .line 376
    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNameMarginTop:I

    .line 378
    const v1, 0x7f0d0171

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    .line 380
    const v1, 0x7f0d016f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderSize:I

    .line 382
    const v1, 0x7f0d0170

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    .line 384
    const v1, 0x7f0d0172

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    .line 386
    const v1, 0x7f0d0175

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    .line 388
    const v1, 0x7f0d0173

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderSize:I

    .line 390
    const v1, 0x7f0d0174

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderRadius:F

    .line 392
    const v1, 0x7f0d0176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginTop:I

    .line 394
    const v1, 0x7f0d0177

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginRight:I

    .line 397
    const v1, 0x7f0d0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginRight:I

    .line 399
    const v1, 0x7f0d0197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerMargin:I

    .line 401
    const v1, 0x7f0d019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginBottom:I

    .line 403
    const v1, 0x7f0d0199

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginTop:I

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 408
    const v1, 0x7f0200c8

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    .line 409
    const v1, 0x7f020106

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    .line 410
    const v1, 0x7f0200a2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sCheckInIconBitmap:Landroid/graphics/Bitmap;

    .line 411
    const v1, 0x7f020169

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconBitmap:Landroid/graphics/Bitmap;

    .line 413
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 415
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 416
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 417
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 418
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00df

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 419
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 420
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 422
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 423
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 424
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 425
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 426
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 427
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 429
    const v1, 0x7f0a00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sIsMuteColor:I

    .line 431
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 432
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 433
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 434
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 435
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 436
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 439
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 440
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 441
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 442
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 443
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 444
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 446
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018c

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 449
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 450
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 451
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 452
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 453
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 455
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018d

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 458
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 459
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 460
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 461
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 462
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 464
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018e

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 467
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 468
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 469
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ea

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 470
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 471
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 473
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018f

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 476
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 477
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 478
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00eb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 479
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 480
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0190

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 481
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0190

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 484
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 485
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 486
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 488
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 489
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 490
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 492
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 493
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 494
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 495
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 497
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 498
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 500
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 502
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 503
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 504
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 505
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 507
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 508
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 510
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 512
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    .line 514
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 515
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00fb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 517
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0d0198

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 520
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 523
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_3a9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setupAccessibility(Landroid/content/Context;)V

    .line 345
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v7, 0x7f0d018b

    const v6, 0x7f0d018a

    const v5, 0x7f0d0189

    const v4, 0x7f0a00de

    const/4 v3, 0x1

    .line 348
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 172
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    .line 276
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 291
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 302
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$3;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 322
    new-instance v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$4;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    .line 352
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_3a9

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 354
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 356
    .local v0, res:Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/google/android/apps/plus/util/PlusBarUtils;->init(Landroid/content/Context;)V

    .line 358
    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    .line 360
    const v1, 0x7f0d0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    .line 362
    const v1, 0x7f0d0169

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginBottom:I

    .line 364
    const v1, 0x7f0d0167

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginLeft:I

    .line 366
    const v1, 0x7f0d0168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginRight:I

    .line 368
    const v1, 0x7f0d016e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTitleMarginBottom:I

    .line 370
    const v1, 0x7f0d016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginTop:I

    .line 372
    const v1, 0x7f0d016b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginLeft:I

    .line 374
    const v1, 0x7f0d016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginRight:I

    .line 376
    const v1, 0x7f0d016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNameMarginTop:I

    .line 378
    const v1, 0x7f0d0171

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    .line 380
    const v1, 0x7f0d016f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderSize:I

    .line 382
    const v1, 0x7f0d0170

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    .line 384
    const v1, 0x7f0d0172

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    .line 386
    const v1, 0x7f0d0175

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    .line 388
    const v1, 0x7f0d0173

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderSize:I

    .line 390
    const v1, 0x7f0d0174

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderRadius:F

    .line 392
    const v1, 0x7f0d0176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginTop:I

    .line 394
    const v1, 0x7f0d0177

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginRight:I

    .line 397
    const v1, 0x7f0d0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginRight:I

    .line 399
    const v1, 0x7f0d0197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerMargin:I

    .line 401
    const v1, 0x7f0d019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginBottom:I

    .line 403
    const v1, 0x7f0d0199

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginTop:I

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 408
    const v1, 0x7f0200c8

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    .line 409
    const v1, 0x7f020106

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    .line 410
    const v1, 0x7f0200a2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sCheckInIconBitmap:Landroid/graphics/Bitmap;

    .line 411
    const v1, 0x7f020169

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconBitmap:Landroid/graphics/Bitmap;

    .line 413
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 415
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 416
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 417
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 418
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00df

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 419
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 420
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 422
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 423
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 424
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 425
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 426
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 427
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 429
    const v1, 0x7f0a00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sIsMuteColor:I

    .line 431
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 432
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 433
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 434
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 435
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 436
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 439
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 440
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 441
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 442
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 443
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 444
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 446
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018c

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 449
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 450
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 451
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 452
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 453
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 455
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018d

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 458
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 459
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 460
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00e9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 461
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 462
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 464
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018e

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 467
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 468
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 469
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ea

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 470
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 471
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 473
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d018f

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 476
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 477
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 478
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00eb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 479
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 480
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0190

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 481
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    const v2, 0x7f0d0190

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 484
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 485
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 486
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 488
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 489
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 490
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 492
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 493
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 494
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 495
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 497
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 498
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 500
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 502
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 503
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00e7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 504
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 505
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 507
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 508
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 510
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 512
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    .line 514
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 515
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00fb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 517
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0d0198

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 520
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 523
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_3a9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setupAccessibility(Landroid/content/Context;)V

    .line 349
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/api/services/plusi/model/PlaceReview;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/Set;
    .registers 2
    .parameter "x0"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    return-object v0
.end method

.method private clearLayoutState()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1524
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 1525
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1526
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1527
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1528
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1529
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1530
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1531
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1532
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 1533
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 1536
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 1537
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIcon:Landroid/graphics/Bitmap;

    .line 1540
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1541
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 1542
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedContentBorder:Landroid/graphics/RectF;

    .line 1543
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamContentBorder:Landroid/graphics/RectF;

    .line 1544
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    .line 1545
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    .line 1546
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    .line 1547
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    .line 1548
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    .line 1549
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 1550
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    .line 1551
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    .line 1552
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    .line 1553
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    .line 1554
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    .line 1555
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    .line 1556
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    .line 1557
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    .line 1558
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBody:Landroid/text/Spannable;

    .line 1559
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    .line 1560
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    .line 1561
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewDividerRect:Landroid/graphics/Rect;

    .line 1562
    return-void
.end method

.method private measureAndLayoutLinkedContent(III)I
    .registers 22
    .parameter "xStart"
    .parameter "yStart"
    .parameter "contentWidth"

    .prologue
    .line 842
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 909
    .end local p2
    :cond_14
    :goto_14
    return p2

    .line 845
    .restart local p2
    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-nez v1, :cond_14

    .line 851
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v11, p1, v1

    .line 852
    .local v11, drawX:I
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v12, p2, v1

    .line 853
    .local v12, drawY:I
    const/4 v15, 0x0

    .line 854
    .local v15, left:I
    const/16 v17, 0x0

    .line 855
    .local v17, top:I
    const/16 v16, 0x0

    .line 856
    .local v16, right:I
    const/4 v10, 0x0

    .line 858
    .local v10, bottom:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_152

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 862
    .local v9, oneUpListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;
    :goto_37
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    if-eqz v1, :cond_cd

    .line 863
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    .line 864
    .local v14, iconWidth:I
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    .line 865
    .local v13, iconHeight:I
    new-instance v1, Landroid/graphics/Rect;

    add-int v2, v11, v14

    add-int v3, v12, v13

    invoke-direct {v1, v11, v12, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    .line 867
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    add-int/2addr v1, v14

    add-int/2addr v11, v1

    .line 868
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, p3, v1

    sub-int/2addr v1, v14

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    sub-int v4, v1, v2

    .line 871
    .local v4, availableWidth:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 872
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 875
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1, v11, v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    .line 876
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 878
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v15, v1, v2

    .line 879
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v17, v1, v2

    .line 880
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v16, v1, v2

    .line 881
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v10, v1, v2

    .line 883
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    add-int/2addr v1, v14

    sub-int/2addr v11, v1

    .line 884
    add-int v1, v12, v13

    invoke-static {v1, v10}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 888
    .end local v4           #availableWidth:I
    .end local v13           #iconHeight:I
    .end local v14           #iconWidth:I
    :cond_cd
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    if-eqz v1, :cond_13d

    .line 889
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v4, p3, v1

    .line 891
    .restart local v4       #availableWidth:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 892
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 895
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1, v11, v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    .line 896
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 898
    if-nez v15, :cond_125

    .line 899
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v15, v1, v2

    .line 900
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v17, v1, v2

    .line 902
    :cond_125
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v16, v1, v2

    .line 903
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v10, v1, v2

    .line 907
    .end local v4           #availableWidth:I
    :cond_13d
    new-instance v1, Landroid/graphics/RectF;

    int-to-float v2, v15

    move/from16 v0, v17

    int-to-float v3, v0

    move/from16 v0, v16

    int-to-float v5, v0

    int-to-float v6, v10

    invoke-direct {v1, v2, v3, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedContentBorder:Landroid/graphics/RectF;

    move/from16 p2, v10

    .line 909
    goto/16 :goto_14

    .line 858
    .end local v9           #oneUpListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;
    :cond_152
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    goto/16 :goto_37
.end method

.method private measureAndLayoutLocation(III)I
    .registers 16
    .parameter "xStart"
    .parameter "yStart"
    .parameter "contentWidth"

    .prologue
    const/4 v7, 0x0

    .line 1101
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1125
    .end local p2
    :goto_9
    return p2

    .line 1108
    .restart local p2
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 1112
    .local v11, iconWidth:I
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 1113
    .local v10, iconHeight:I
    new-instance v1, Landroid/graphics/Rect;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mIsCheckin:Z

    if-eqz v0, :cond_6b

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginTop:I

    :goto_1e
    add-int v2, p2, v0

    add-int v4, p1, v11

    add-int v5, p2, v10

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mIsCheckin:Z

    if-eqz v0, :cond_6d

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginTop:I

    :goto_2a
    add-int/2addr v0, v5

    invoke-direct {v1, p1, v2, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    .line 1116
    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginRight:I

    add-int/2addr v0, v11

    add-int v9, p1, v0

    .line 1117
    .local v9, drawX:I
    sub-int v0, p3, v11

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconMarginRight:I

    sub-int v3, v0, v1

    .line 1119
    .local v3, availableWidth:I
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1120
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v6, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 1122
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, v9, p2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    .line 1123
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1125
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v0

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    goto :goto_9

    .end local v3           #availableWidth:I
    .end local v9           #drawX:I
    :cond_6b
    move v0, v7

    .line 1113
    goto :goto_1e

    :cond_6d
    move v0, v7

    goto :goto_2a
.end method

.method private measureAndLayoutPlaceReviewContent(III)I
    .registers 21
    .parameter "xStart"
    .parameter "yStart"
    .parameter "contentWidth"

    .prologue
    .line 1161
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-nez v1, :cond_7

    .line 1219
    .end local p2
    :goto_6
    return p2

    .line 1165
    .restart local p2
    :cond_7
    move/from16 v14, p2

    .line 1168
    .local v14, drawY:I
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1169
    .local v2, aspectsTextBuilder:Landroid/text/SpannableStringBuilder;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewRating:Ljava/util/List;

    if-eqz v1, :cond_82

    .line 1170
    const/4 v15, 0x0

    .local v15, i:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewRating:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v16

    .local v16, size:I
    :goto_21
    move/from16 v0, v16

    if-ge v15, v0, :cond_82

    .line 1171
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewRating:Ljava/util/List;

    invoke-interface {v1, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/Rating;

    .line 1172
    .local v11, aspect:Lcom/google/api/services/plusi/model/Rating;
    iget-object v12, v11, Lcom/google/api/services/plusi/model/Rating;->name:Ljava/lang/String;

    .line 1173
    .local v12, aspectName:Ljava/lang/String;
    iget-object v13, v11, Lcom/google/api/services/plusi/model/Rating;->ratingValue:Ljava/lang/String;

    .line 1174
    .local v13, aspectValue:Ljava/lang/String;
    iget-object v1, v11, Lcom/google/api/services/plusi/model/Rating;->clientDisplayData:Lcom/google/api/services/plusi/model/RatingClientDisplayData;

    if-eqz v1, :cond_47

    iget-object v1, v11, Lcom/google/api/services/plusi/model/Rating;->clientDisplayData:Lcom/google/api/services/plusi/model/RatingClientDisplayData;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/RatingClientDisplayData;->renderedRatingText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_47

    .line 1176
    iget-object v1, v11, Lcom/google/api/services/plusi/model/Rating;->clientDisplayData:Lcom/google/api/services/plusi/model/RatingClientDisplayData;

    iget-object v13, v1, Lcom/google/api/services/plusi/model/RatingClientDisplayData;->renderedRatingText:Ljava/lang/String;

    .line 1178
    :cond_47
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7f

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7f

    .line 1180
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0f006e

    invoke-direct {v1, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v12, v1}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 1185
    const-string v1, "\u00a0"

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1186
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0f006f

    invoke-direct {v1, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v13, v1}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 1192
    add-int/lit8 v1, v16, -0x1

    if-eq v15, v1, :cond_7f

    .line 1193
    const-string v1, "  "

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1170
    :cond_7f
    add-int/lit8 v15, v15, 0x1

    goto :goto_21

    .line 1198
    .end local v11           #aspect:Lcom/google/api/services/plusi/model/Rating;
    .end local v12           #aspectName:Ljava/lang/String;
    .end local v13           #aspectValue:Ljava/lang/String;
    .end local v15           #i:I
    .end local v16           #size:I
    :cond_82
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_b4

    .line 1199
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginTop:I

    add-int v14, p2, v1

    .line 1201
    new-instance v1, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v4, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 1204
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move/from16 v0, p1

    invoke-virtual {v1, v0, v14}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    .line 1206
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v1

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewAspectsMarginBottom:I

    add-int/2addr v1, v3

    add-int/2addr v14, v1

    .line 1210
    :cond_b4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_eb

    .line 1211
    new-instance v3, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v9, 0x0

    const/4 v10, 0x0

    move/from16 v6, p3

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 1214
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move/from16 v0, p1

    invoke-virtual {v1, v0, v14}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    .line 1216
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v14, v1

    :cond_eb
    move/from16 p2, v14

    .line 1219
    goto/16 :goto_6
.end method

.method private measureAndLayoutPlaceReviewDivider(III)I
    .registers 7
    .parameter "xStart"
    .parameter "yStart"
    .parameter "contentWidth"

    .prologue
    .line 1137
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-nez v1, :cond_5

    .line 1149
    .end local p2
    :goto_4
    return p2

    .line 1141
    .restart local p2
    :cond_5
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerMargin:I

    add-int v0, p2, v1

    .line 1146
    .local v0, drawY:I
    new-instance v1, Landroid/graphics/Rect;

    add-int v2, p1, p3

    invoke-direct {v1, p1, v0, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewDividerRect:Landroid/graphics/Rect;

    .line 1147
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerMargin:I

    add-int/2addr v0, v1

    move p2, v0

    .line 1149
    goto :goto_4
.end method

.method private measureAndLayoutSkyjamContent(III)I
    .registers 22
    .parameter "xStart"
    .parameter "yStart"
    .parameter "contentWidth"

    .prologue
    .line 921
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 987
    .end local p2
    :goto_a
    return p2

    .line 926
    .restart local p2
    :cond_b
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v11, p1, v1

    .line 927
    .local v11, drawX:I
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v12, p2, v1

    .line 934
    .local v12, drawY:I
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    .line 935
    .local v14, iconWidth:I
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    .line 936
    .local v13, iconHeight:I
    new-instance v1, Landroid/graphics/Rect;

    add-int v2, v11, v14

    add-int v3, v12, v13

    invoke-direct {v1, v11, v12, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    .line 938
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    add-int/2addr v1, v14

    add-int/2addr v11, v1

    .line 939
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, p3, v1

    sub-int/2addr v1, v14

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    sub-int v4, v1, v2

    .line 942
    .local v4, availableWidth:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 943
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedHeaderPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 946
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1, v11, v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    .line 947
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 949
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v15, v1, v2

    .line 950
    .local v15, left:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v17, v1, v2

    .line 951
    .local v17, top:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v16, v1, v2

    .line 952
    .local v16, right:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v10, v1, v2

    .line 954
    .local v10, bottom:I
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconMarginRight:I

    add-int/2addr v1, v14

    sub-int/2addr v11, v1

    .line 955
    add-int v1, v12, v13

    invoke-static {v1, v10}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 958
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v4, p3, v1

    .line 960
    new-instance v1, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 962
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1, v11, v12}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    .line 964
    if-nez v15, :cond_e3

    .line 965
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v15, v1, v2

    .line 966
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    sub-int v17, v1, v2

    .line 968
    :cond_e3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getRight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int/2addr v1, v2

    move/from16 v0, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 969
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getBottom()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v10, v1, v2

    .line 972
    move v12, v10

    .line 974
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 975
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBodyPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 978
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1, v11, v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    .line 979
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 981
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int/2addr v1, v2

    move/from16 v0, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 982
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedInnerMargin:I

    add-int v10, v1, v2

    .line 985
    new-instance v1, Landroid/graphics/RectF;

    int-to-float v2, v15

    move/from16 v0, v17

    int-to-float v3, v0

    move/from16 v0, v16

    int-to-float v5, v0

    int-to-float v6, v10

    invoke-direct {v1, v2, v3, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamContentBorder:Landroid/graphics/RectF;

    move/from16 p2, v10

    .line 987
    goto/16 :goto_a
.end method

.method private measureAndLayoutTitle(III)I
    .registers 13
    .parameter "xStart"
    .parameter "yStart"
    .parameter "contentWidth"

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 829
    .end local p2
    :goto_8
    return p2

    .line 819
    .restart local p2
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 824
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 829
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result p2

    goto :goto_8
.end method

.method private setupAccessibility(Landroid/content/Context;)V
    .registers 4
    .parameter "ctx"

    .prologue
    .line 1492
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1c

    invoke-static {p1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1493
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-nez v0, :cond_1c

    .line 1494
    new-instance v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    .line 1495
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->install(Landroid/view/View;)V

    .line 1498
    :cond_1c
    return-void
.end method

.method private updateAccessibility()V
    .registers 2

    .prologue
    .line 1501
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_e

    .line 1502
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    .line 1503
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->invalidateParent()V

    .line 1505
    :cond_e
    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;)V
    .registers 53
    .parameter "cursor"

    .prologue
    .line 1565
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v16

    .line 1566
    .local v16, context:Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    .line 1569
    .local v40, res:Landroid/content/res/Resources;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->clearLayoutState()V

    .line 1571
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1572
    .local v10, activityId:Ljava/lang/String;
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1573
    .local v5, authorId:Ljava/lang/String;
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1574
    .local v14, contentFlags:J
    const-wide/16 v3, 0x10

    and-long/2addr v3, v14

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-eqz v3, :cond_1e1

    const/4 v3, 0x1

    :goto_2b
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mIsCheckin:Z

    .line 1576
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;

    .line 1577
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    .line 1578
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    .line 1579
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorId:Ljava/lang/String;

    .line 1580
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    .line 1581
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    if-nez v3, :cond_79

    .line 1582
    const-string v3, ""

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    .line 1583
    const-string v3, "StreamOneUp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "===> Author name was null for gaia id: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    :cond_79
    new-instance v3, Lcom/google/android/apps/plus/views/ClickableUserImage;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    const/4 v9, 0x2

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 1588
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1590
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    move-object/from16 v0, v16

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getAbbreviatedRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDate:Ljava/lang/String;

    .line 1593
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_1e4

    .line 1594
    const v3, 0x7f0803ca

    move-object/from16 v0, v40

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    .line 1599
    :goto_c6
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 1600
    .local v35, originalAuthorId:Ljava/lang/String;
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 1602
    .local v36, originalAuthorName:Ljava/lang/String;
    invoke-static/range {v35 .. v35}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_149

    invoke-static/range {v36 .. v36}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_149

    .line 1603
    const v3, 0x7f0803cb

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v36, v4, v6

    move-object/from16 v0, v40

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v41

    .line 1606
    .local v41, reshareHeader:Ljava/lang/String;
    invoke-virtual/range {v40 .. v40}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v0, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v29, v0

    .line 1607
    .local v29, locale:Ljava/util/Locale;
    move-object/from16 v0, v41

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v50

    .line 1608
    .local v50, upperReshareHeader:Ljava/lang/String;
    move-object/from16 v0, v36

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v49

    .line 1610
    .local v49, upperAuthorName:Ljava/lang/String;
    new-instance v44, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#~loop:svt=person&view=stream&pid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    .line 1612
    .local v44, span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    move-object/from16 v0, v50

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v46

    .line 1613
    .local v46, spanStart:I
    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->length()I

    move-result v3

    add-int v45, v46, v3

    .line 1615
    .local v45, spanEnd:I
    new-instance v3, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v50

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    .line 1616
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    const/16 v4, 0x21

    move-object/from16 v0, v44

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-interface {v3, v0, v1, v2, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1620
    .end local v29           #locale:Ljava/util/Locale;
    .end local v41           #reshareHeader:Ljava/lang/String;
    .end local v44           #span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    .end local v45           #spanEnd:I
    .end local v46           #spanStart:I
    .end local v49           #upperAuthorName:Ljava/lang/String;
    .end local v50           #upperReshareHeader:Ljava/lang/String;
    :cond_149
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1621
    .local v11, annotationHtml:Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_15f

    .line 1622
    invoke-static {v11}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    .line 1626
    :cond_15f
    const-wide/16 v3, 0x2000

    and-long/2addr v3, v14

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-eqz v3, :cond_202

    .line 1627
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v21

    .line 1628
    .local v21, hangoutDataBytes:[B
    if-eqz v21, :cond_1ff

    .line 1629
    invoke-static {}, Lcom/google/api/services/plusi/model/HangoutDataJson;->getInstance()Lcom/google/api/services/plusi/model/HangoutDataJson;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/HangoutDataJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/api/services/plusi/model/HangoutData;

    .line 1631
    .local v20, hangoutData:Lcom/google/api/services/plusi/model/HangoutData;
    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/service/Hangout;->isInProgress(Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v3

    if-eqz v3, :cond_1eb

    .line 1632
    const v3, 0x7f0803c4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    aput-object v7, v4, v6

    move-object/from16 v0, v40

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v47

    .line 1643
    .end local v20           #hangoutData:Lcom/google/api/services/plusi/model/HangoutData;
    .end local v21           #hangoutDataBytes:[B
    .local v47, titleHtml:Ljava/lang/String;
    :goto_197
    invoke-static/range {v47 .. v47}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1a5

    .line 1644
    invoke-static/range {v47 .. v47}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    .line 1647
    :cond_1a5
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v33

    .line 1648
    .local v33, mediaBytes:[B
    if-eqz v33, :cond_346

    .line 1649
    invoke-static/range {v33 .. v33}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v17

    .line 1650
    .local v17, dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    const/16 v22, 0x0

    .local v22, i:I
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v43, v0

    .local v43, size:I
    :goto_1b9
    move/from16 v0, v22

    move/from16 v1, v43

    if-ge v0, v1, :cond_346

    .line 1651
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v48

    .line 1652
    .local v48, type:I
    sparse-switch v48, :sswitch_data_5b4

    .line 1716
    const-string v3, "StreamOneUp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Unhandled media type: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1650
    :cond_1de
    :goto_1de
    add-int/lit8 v22, v22, 0x1

    goto :goto_1b9

    .line 1574
    .end local v11           #annotationHtml:Ljava/lang/String;
    .end local v17           #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v22           #i:I
    .end local v33           #mediaBytes:[B
    .end local v35           #originalAuthorId:Ljava/lang/String;
    .end local v36           #originalAuthorName:Ljava/lang/String;
    .end local v43           #size:I
    .end local v47           #titleHtml:Ljava/lang/String;
    .end local v48           #type:I
    :cond_1e1
    const/4 v3, 0x0

    goto/16 :goto_2b

    .line 1596
    :cond_1e4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    goto/16 :goto_c6

    .line 1634
    .restart local v11       #annotationHtml:Ljava/lang/String;
    .restart local v20       #hangoutData:Lcom/google/api/services/plusi/model/HangoutData;
    .restart local v21       #hangoutDataBytes:[B
    .restart local v35       #originalAuthorId:Ljava/lang/String;
    .restart local v36       #originalAuthorName:Ljava/lang/String;
    :cond_1eb
    const v3, 0x7f0803c5

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    aput-object v7, v4, v6

    move-object/from16 v0, v40

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v47

    .line 1636
    .restart local v47       #titleHtml:Ljava/lang/String;
    goto :goto_197

    .line 1637
    .end local v20           #hangoutData:Lcom/google/api/services/plusi/model/HangoutData;
    .end local v47           #titleHtml:Ljava/lang/String;
    :cond_1ff
    const/16 v47, 0x0

    .restart local v47       #titleHtml:Ljava/lang/String;
    goto :goto_197

    .line 1640
    .end local v21           #hangoutDataBytes:[B
    .end local v47           #titleHtml:Ljava/lang/String;
    :cond_202
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v47

    .restart local v47       #titleHtml:Ljava/lang/String;
    goto :goto_197

    .line 1654
    .restart local v17       #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .restart local v22       #i:I
    .restart local v33       #mediaBytes:[B
    .restart local v43       #size:I
    .restart local v48       #type:I
    :sswitch_20b
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getTitle()Ljava/lang/String;

    move-result-object v26

    .line 1655
    .local v26, linkedHeader:Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_24e

    .line 1656
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v24

    .line 1657
    .local v24, linkUrl:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_246

    .line 1658
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<a href=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</a>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 1661
    :cond_246
    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    .line 1664
    .end local v24           #linkUrl:Ljava/lang/String;
    :cond_24e
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getDescription()Ljava/lang/String;

    move-result-object v25

    .line 1665
    .local v25, linkedBody:Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1de

    .line 1666
    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    goto/16 :goto_1de

    .line 1675
    .end local v25           #linkedBody:Ljava/lang/String;
    .end local v26           #linkedHeader:Ljava/lang/String;
    :sswitch_264
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbumArtist()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1de

    .line 1676
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getSkyjamMarketUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 1680
    .local v32, marketUrl:Ljava/lang/String;
    new-instance v12, Landroid/text/SpannableStringBuilder;

    invoke-direct {v12}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1681
    .local v12, builder:Landroid/text/SpannableStringBuilder;
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getSongTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_32d

    .line 1682
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbum()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1684
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getSkyjamPreviewUrl()Ljava/lang/String;

    move-result-object v39

    .line 1685
    .local v39, previewUrl:Ljava/lang/String;
    const-string v3, "https://"

    move-object/from16 v0, v39

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v34

    .line 1686
    .local v34, musicUrlIndex:I
    if-gez v34, :cond_2ac

    .line 1687
    const-string v3, "http://"

    move-object/from16 v0, v39

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v34

    .line 1689
    :cond_2ac
    if-ltz v34, :cond_1de

    .line 1690
    move-object/from16 v0, v39

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v39

    .line 1694
    new-instance v27, Landroid/text/SpannableStringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080170

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1697
    .local v27, listenLinkBuilder:Landroid/text/SpannableStringBuilder;
    new-instance v28, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "skyjam:listen:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    .line 1699
    .local v28, listenSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    const/4 v3, 0x0

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v6, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v3, v4, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1701
    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    .line 1707
    .end local v27           #listenLinkBuilder:Landroid/text/SpannableStringBuilder;
    .end local v28           #listenSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    .end local v34           #musicUrlIndex:I
    .end local v39           #previewUrl:Ljava/lang/String;
    :goto_2f4
    new-instance v44, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "skyjam:buy:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    .line 1708
    .restart local v44       #span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    const/4 v3, 0x0

    invoke-virtual {v12}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v6, 0x21

    move-object/from16 v0, v44

    invoke-virtual {v12, v0, v3, v4, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1710
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    .line 1711
    new-instance v3, Landroid/text/SpannableString;

    aget-object v4, v17, v22

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbumArtist()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    goto/16 :goto_1de

    .line 1703
    .end local v44           #span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    :cond_32d
    aget-object v3, v17, v22

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbMedia;->getSongTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1704
    new-instance v3, Landroid/text/SpannableString;

    aget-object v4, v17, v22

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbum()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    goto :goto_2f4

    .line 1723
    .end local v12           #builder:Landroid/text/SpannableStringBuilder;
    .end local v17           #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v22           #i:I
    .end local v32           #marketUrl:Ljava/lang/String;
    .end local v43           #size:I
    .end local v48           #type:I
    :cond_346
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v37

    .line 1724
    .local v37, plusOneBytes:[B
    if-eqz v37, :cond_357

    .line 1725
    invoke-static/range {v37 .. v37}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 1729
    :cond_357
    const-wide/32 v3, 0x10000

    and-long/2addr v3, v14

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-eqz v3, :cond_381

    .line 1730
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v19

    .line 1731
    .local v19, embedData:[B
    if-eqz v19, :cond_381

    .line 1732
    invoke-static {}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->getInstance()Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/EmbedClientItem;

    .line 1733
    .local v18, embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    if-eqz v18, :cond_381

    .line 1734
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->placeReview:Lcom/google/api/services/plusi/model/PlaceReview;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    .line 1739
    .end local v18           #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    .end local v19           #embedData:[B
    :cond_381
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v30

    .line 1740
    .local v30, locationData:[B
    if-eqz v30, :cond_43a

    .line 1741
    invoke-static/range {v30 .. v30}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    .line 1743
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIcon:Landroid/graphics/Bitmap;

    if-nez v3, :cond_3a5

    .line 1744
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mIsCheckin:Z

    if-eqz v3, :cond_436

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sCheckInIconBitmap:Landroid/graphics/Bitmap;

    :goto_3a1
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIcon:Landroid/graphics/Bitmap;

    .line 1747
    :cond_3a5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v31

    .line 1748
    .local v31, locationName:Ljava/lang/String;
    new-instance v44, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    move-object/from16 v0, v44

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    .line 1749
    .restart local v44       #span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    new-instance v3, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v31

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    .line 1750
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    const/4 v4, 0x0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x21

    move-object/from16 v0, v44

    invoke-interface {v3, v0, v4, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1759
    .end local v31           #locationName:Ljava/lang/String;
    .end local v44           #span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    :cond_3d1
    :goto_3d1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1761
    const-wide/32 v3, 0x8000

    and-long/2addr v3, v14

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-eqz v3, :cond_4f4

    .line 1762
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v19

    .line 1763
    .restart local v19       #embedData:[B
    if-eqz v19, :cond_4f4

    .line 1764
    invoke-static {}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->getInstance()Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/EmbedClientItem;

    .line 1765
    .restart local v18       #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    if-eqz v18, :cond_4f4

    .line 1766
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DeepLinkData;->deepLinkId:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppData:Ljava/lang/String;

    .line 1767
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DeepLinkData;->client:Ljava/util/List;

    if-eqz v3, :cond_480

    .line 1768
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DeepLinkData;->client:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, i$:Ljava/util/Iterator;
    :cond_416
    :goto_416
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_480

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/plusi/model/PackagingServiceClient;

    .line 1769
    .local v13, client:Lcom/google/api/services/plusi/model/PackagingServiceClient;
    iget-object v3, v13, Lcom/google/api/services/plusi/model/PackagingServiceClient;->type:Ljava/lang/String;

    const-string v4, "ANDROID"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_416

    .line 1770
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    iget-object v4, v13, Lcom/google/api/services/plusi/model/PackagingServiceClient;->androidPackageName:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_416

    .line 1744
    .end local v13           #client:Lcom/google/api/services/plusi/model/PackagingServiceClient;
    .end local v18           #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    .end local v19           #embedData:[B
    .end local v23           #i$:Ljava/util/Iterator;
    :cond_436
    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_3a1

    .line 1751
    :cond_43a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-eqz v3, :cond_3d1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3d1

    .line 1752
    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLocationIconBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIcon:Landroid/graphics/Bitmap;

    .line 1753
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    .line 1754
    .restart local v31       #locationName:Ljava/lang/String;
    new-instance v44, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    move-object/from16 v0, v44

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    .line 1755
    .restart local v44       #span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    new-instance v3, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v31

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    .line 1756
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    const/4 v4, 0x0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x21

    move-object/from16 v0, v44

    invoke-interface {v3, v0, v4, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3d1

    .line 1774
    .end local v31           #locationName:Ljava/lang/String;
    .end local v44           #span:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
    .restart local v18       #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    .restart local v19       #embedData:[B
    :cond_480
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4f4

    .line 1777
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4e7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4e7

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    if-eqz v3, :cond_4e7

    .line 1779
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Thing;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    .line 1780
    .restart local v26       #linkedHeader:Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4d1

    .line 1781
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<a href=\"\">"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</a>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 1782
    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    .line 1784
    :cond_4d1
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Thing;->description:Ljava/lang/String;

    move-object/from16 v25, v0

    .line 1785
    .restart local v25       #linkedBody:Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4e7

    .line 1786
    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    .line 1790
    .end local v25           #linkedBody:Ljava/lang/String;
    .end local v26           #linkedHeader:Ljava/lang/String;
    :cond_4e7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    if-eqz v3, :cond_4f4

    .line 1791
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/OneUpListener;->onSourceAppNameLinkEnabled()V

    .line 1799
    .end local v18           #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    .end local v19           #embedData:[B
    :cond_4f4
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v3, v4, :cond_5aa

    .line 1800
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    .line 1802
    .local v42, sb:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1803
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDate:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1804
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1805
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1806
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1807
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1808
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1809
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1810
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1811
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1812
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1813
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1814
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1815
    const/4 v3, 0x0

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1816
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    move-object/from16 v0, v42

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 1818
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v3, :cond_5b1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v3

    if-eqz v3, :cond_5b1

    const/16 v38, 0x1

    .line 1819
    .local v38, plusOnedByMe:Z
    :goto_595
    const/4 v3, 0x0

    move-object/from16 v0, v42

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1820
    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1821
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setFocusable(Z)V

    .line 1824
    .end local v38           #plusOnedByMe:Z
    .end local v42           #sb:Ljava/lang/StringBuilder;
    :cond_5aa
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    .line 1825
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->requestLayout()V

    .line 1826
    return-void

    .line 1818
    .restart local v42       #sb:Ljava/lang/StringBuilder;
    :cond_5b1
    const/16 v38, 0x0

    goto :goto_595

    .line 1652
    :sswitch_data_5b4
    .sparse-switch
        0x1 -> :sswitch_20b
        0xb -> :sswitch_264
        0xd -> :sswitch_264
    .end sparse-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 601
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 602
    .local v2, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 604
    .local v3, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_64

    :pswitch_14
    move v4, v5

    .line 636
    :cond_15
    :goto_15
    return v4

    .line 606
    :pswitch_16
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1c
    :goto_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 607
    .local v1, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 608
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 609
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    goto :goto_1c

    .line 616
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :pswitch_34
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 617
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_3c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 618
    .restart local v1       #item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_3c

    .line 620
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_4c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    move v4, v5

    .line 621
    goto :goto_15

    .line 625
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_51
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_61

    .line 626
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 627
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 628
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->invalidate()V

    goto :goto_15

    :cond_61
    move v4, v5

    .line 631
    goto :goto_15

    .line 604
    nop

    :pswitch_data_64
    .packed-switch 0x0
        :pswitch_16
        :pswitch_34
        :pswitch_14
        :pswitch_51
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 528
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onAttachedToWindow()V

    .line 530
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 531
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->updateAccessibility()V

    .line 532
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 643
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_9

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->onAvatarChanged(Ljava/lang/String;)V

    .line 646
    :cond_9
    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .registers 8
    .parameter "button"

    .prologue
    .line 650
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    if-eqz v3, :cond_50

    .line 651
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v3, :cond_50

    .line 652
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/plus/views/OneUpListener;->onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    .line 653
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_50

    .line 654
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v3, :cond_51

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v3

    if-eqz v3, :cond_51

    const/4 v1, 0x1

    .line 656
    .local v1, plusOnedByMe:Z
    :goto_28
    if-eqz v1, :cond_53

    const v2, 0x7f0803e1

    .line 658
    .local v2, speakRes:I
    :goto_2d
    const/16 v3, 0x4000

    invoke-static {v3}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 660
    .local v0, notificationEvent:Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 664
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 665
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-interface {v3, p0, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 669
    .end local v0           #notificationEvent:Landroid/view/accessibility/AccessibilityEvent;
    .end local v1           #plusOnedByMe:Z
    .end local v2           #speakRes:I
    :cond_50
    return-void

    .line 654
    :cond_51
    const/4 v1, 0x0

    goto :goto_28

    .line 656
    .restart local v1       #plusOnedByMe:Z
    :cond_53
    const v2, 0x7f0803e0

    goto :goto_2d
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 536
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDetachedFromWindow()V

    .line 538
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 539
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->uninstall()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    .line 540
    :cond_14
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 9
    .parameter "canvas"

    .prologue
    const/4 v6, 0x0

    .line 581
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDraw(Landroid/graphics/Canvas;)V

    .line 583
    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mBackgroundOffset:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_8e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2a7

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_2a
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_4f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 586
    :cond_8e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_af

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 587
    :cond_af
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-nez v0, :cond_b7

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_10f

    :cond_b7
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderRadius:F

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderRadius:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderRadius:F

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderRadius:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_ee

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_ee
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_10f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 588
    :cond_10f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_130

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitleLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 589
    :cond_130
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewDividerRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_14e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewDividerRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewDividerRect:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v0

    int-to-float v2, v2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewDividerRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewDividerRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlaceReviewDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 590
    :cond_14e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_176

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIcon:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v6, v1, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 591
    :cond_176
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-nez v0, :cond_17e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_1dd

    :cond_17e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedContentBorder:Landroid/graphics/RectF;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedContentBorder:Landroid/graphics/RectF;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_1bc

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedIconBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v6, v1, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1bc
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_1dd

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 592
    :cond_1dd
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    if-eqz v0, :cond_261

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamContentBorder:Landroid/graphics/RectF;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamContentBorder:Landroid/graphics/RectF;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderRadius:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sLinkedBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_21f

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sSkyjamIconBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v6, v1, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_21f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_240

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader1Layout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_240
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_261

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamSubheader2Layout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 593
    :cond_261
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_282

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewAspectsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_282
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_2a3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReviewBodyLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 595
    :cond_2a3
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->updateAccessibility()V

    .line 596
    return-void

    .line 585
    :cond_2a7
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_2a
.end method

.method protected onMeasure(II)V
    .registers 23
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 544
    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onMeasure(II)V

    .line 546
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingLeft()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginLeft:I

    add-int v14, v3, v4

    .line 547
    .local v14, xStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingTop()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginTop:I

    sub-int v15, v3, v4

    .line 549
    .local v15, yStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getMeasuredWidth()I

    move-result v13

    .line 550
    .local v13, width:I
    sub-int v3, v13, v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginRight:I

    sub-int v12, v3, v4

    .line 552
    .local v12, contentWidth:I
    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mBackgroundOffset:I

    .line 554
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginLeft:I

    add-int/2addr v3, v14

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginTop:I

    add-int/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int/2addr v7, v3

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int/2addr v8, v5

    invoke-virtual {v6, v3, v5, v7, v8}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v3, :cond_24e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v3

    if-eqz v3, :cond_24e

    const/4 v3, 0x1

    move v8, v3

    :goto_4f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v3, :cond_252

    const/4 v3, 0x1

    :goto_56
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08039e

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    add-int v3, v14, v12

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginRight:I

    sub-int v16, v3, v6

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNameMarginTop:I

    add-int v17, v15, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v3, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v8, :cond_25c

    sget-object v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    :goto_8b
    if-eqz v8, :cond_260

    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_8f
    if-eqz v8, :cond_264

    sget-object v8, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_93
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int v3, v16, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v3, v0}, Landroid/graphics/Rect;->offsetTo(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginLeft:I

    add-int/2addr v3, v14

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    add-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarMarginRight:I

    add-int v16, v3, v4

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNameMarginTop:I

    add-int v11, v15, v3

    sub-int v3, v12, v16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sPlusOneButtonMarginLeft:I

    sub-int v6, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorName:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    int-to-float v5, v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v3, v4, v5, v7}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v3, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move/from16 v0, v16

    invoke-virtual {v3, v0, v11}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDate:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v3, "   "

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSourceAppPackages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_268

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    new-instance v7, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    const/16 v8, 0x21

    invoke-virtual {v4, v7, v3, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_17f

    const-string v3, "   "

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    new-instance v7, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclText:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    const/16 v8, 0x21

    invoke-virtual {v4, v7, v3, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_17f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1a9

    const-string v3, "   "

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMuteState:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sIsMuteColor:I

    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v8, 0x21

    invoke-virtual {v4, v7, v3, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1a9
    const-string v3, " "

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v3

    add-int v17, v11, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sDatePaint:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAclClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v3, v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sAvatarSize:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDateSourceAclLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sTitleMarginBottom:I

    add-int v15, v3, v4

    .line 555
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_271

    .line 557
    :goto_20a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    if-eqz v3, :cond_2b2

    const/4 v3, 0x1

    :goto_211
    if-eqz v3, :cond_466

    .line 558
    const/4 v3, 0x0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2b5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2b5

    .line 568
    :goto_224
    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sMarginBottom:I

    add-int/2addr v3, v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setMeasuredDimension(II)V

    .line 570
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v3, :cond_240

    .line 571
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    move-object/from16 v0, p0

    invoke-interface {v3, v0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    .line 574
    :cond_240
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    if-eqz v3, :cond_24d

    .line 575
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTouchExplorer:Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    .line 577
    :cond_24d
    return-void

    .line 554
    :cond_24e
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_4f

    :cond_252
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v3

    goto/16 :goto_56

    :cond_25c
    sget-object v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    goto/16 :goto_8b

    :cond_260
    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_8f

    :cond_264
    sget-object v8, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_93

    :cond_268
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mCreationSource:Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_153

    .line 555
    :cond_271
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotation:Landroid/text/Spannable;

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move v6, v12

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3, v14, v15}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mAnnotationLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v15

    goto/16 :goto_20a

    .line 557
    :cond_2b2
    const/4 v3, 0x0

    goto/16 :goto_211

    .line 558
    :cond_2b5
    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v6, v12, v3

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int v19, v14, v3

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int v16, v15, v3

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_44e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeader:Landroid/text/Spannable;

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareHeaderPaint:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v3, v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    sub-int v7, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    sub-int v5, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int/2addr v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v3

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int/2addr v3, v8

    move/from16 v16, v5

    move/from16 v17, v7

    move/from16 v18, v3

    :goto_33f
    const/4 v5, 0x0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3af

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareBodyPaint:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sFontSpacing:F

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-nez v17, :cond_397

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    sub-int v17, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    sub-int v16, v3, v4

    :cond_397
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int/2addr v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareBodyLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v3

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int/2addr v3, v5

    move/from16 v18, v3

    :cond_3af
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mTitle:Landroid/text/Spannable;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3c5

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutTitle(III)I

    move-result v3

    move/from16 v18, v3

    :cond_3c5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-eqz v5, :cond_3d7

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutPlaceReviewDivider(III)I

    move-result v3

    move/from16 v18, v3

    :cond_3d7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLocation:Landroid/text/Spannable;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3ed

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLocation(III)I

    move-result v3

    move/from16 v18, v3

    :cond_3ed
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_401

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedBody:Landroid/text/Spannable;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_40f

    :cond_401
    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLinkedContent(III)I

    move-result v18

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int v3, v3, v18

    :cond_40f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mSkyjamHeader:Landroid/text/Spannable;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_427

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutSkyjamContent(III)I

    move-result v18

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int v3, v3, v18

    :cond_427
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mPlaceReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-eqz v5, :cond_43a

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutPlaceReviewContent(III)I

    move-result v3

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->sReshareInnerMargin:I

    add-int/2addr v3, v5

    :cond_43a
    new-instance v5, Landroid/graphics/RectF;

    move/from16 v0, v17

    int-to-float v6, v0

    move/from16 v0, v16

    int-to-float v7, v0

    int-to-float v4, v4

    int-to-float v8, v3

    invoke-direct {v5, v6, v7, v4, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareContentBorder:Landroid/graphics/RectF;

    move v15, v3

    goto/16 :goto_224

    :cond_44e
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v8, v9}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mReshareHeaderLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v17, v7

    move/from16 v18, v16

    move/from16 v16, v5

    goto/16 :goto_33f

    .line 560
    :cond_466
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v12}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutTitle(III)I

    move-result v15

    .line 561
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v12}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutPlaceReviewDivider(III)I

    move-result v15

    .line 562
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v12}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLocation(III)I

    move-result v15

    .line 563
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v12}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutLinkedContent(III)I

    move-result v15

    .line 564
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v12}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutSkyjamContent(III)I

    move-result v15

    .line 565
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15, v12}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->measureAndLayoutPlaceReviewContent(III)I

    move-result v15

    goto/16 :goto_224
.end method

.method public onRecycle()V
    .registers 2

    .prologue
    .line 1516
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->clearLayoutState()V

    .line 1517
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 1518
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mIsCheckin:Z

    .line 1519
    return-void
.end method

.method public setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V
    .registers 2
    .parameter "oneUpListener"

    .prologue
    .line 675
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 676
    return-void
.end method
