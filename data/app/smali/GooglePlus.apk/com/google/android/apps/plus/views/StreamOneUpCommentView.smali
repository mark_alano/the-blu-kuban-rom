.class public Lcom/google/android/apps/plus/views/StreamOneUpCommentView;
.super Lcom/google/android/apps/plus/views/OneUpBaseView;
.source "StreamOneUpCommentView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;
    }
.end annotation


# static fields
.field private static sAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sAvatarMarginRight:I

.field private static sAvatarMarginTop:I

.field private static sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBackgroundFadePaint:Landroid/graphics/Paint;

.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sContentPaint:Landroid/text/TextPaint;

.field private static sDatePaint:Landroid/text/TextPaint;

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sDividerThickness:I

.field private static sFlaggedCommentFadeArea:Landroid/graphics/Rect;

.field private static sFontSpacing:F

.field private static sMarginBottom:I

.field private static sMarginLeft:I

.field private static sMarginRight:I

.field private static sMarginTop:I

.field private static sNameMarginRight:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sPlusOneColor:I

.field private static sPlusOneInverseColor:I

.field private static sPlusOnePaint:Landroid/text/TextPaint;

.field protected static sPressedStateBackground:Landroid/graphics/drawable/Drawable;

.field private static sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mAuthorId:Ljava/lang/String;

.field private mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

.field private mAuthorName:Ljava/lang/String;

.field private mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCommentContent:Ljava/lang/String;

.field private mCommentId:Ljava/lang/String;

.field private mContentDescriptionDirty:Z

.field private mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/String;

.field private mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mIsFlagged:Z

.field private mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mPlusOneByMe:Z

.field private mPlusOneCount:I

.field private mPlusOneId:Ljava/lang/String;

.field private mPressed:Z

.field private mSetPressedRunnable:Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v7, 0x7f0d0195

    const v6, 0x7f0d0194

    const v5, 0x7f0d0193

    const v4, 0x7f0d0192

    const/4 v3, 0x1

    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    .line 138
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    .line 151
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_1a6

    .line 152
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 156
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f02017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    .line 158
    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    .line 160
    const v1, 0x7f0d0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    .line 162
    const v1, 0x7f0d017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginTop:I

    .line 164
    const v1, 0x7f0d017d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginLeft:I

    .line 166
    const v1, 0x7f0d017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginRight:I

    .line 168
    const v1, 0x7f0d017f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginBottom:I

    .line 170
    const v1, 0x7f0d0180

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginTop:I

    .line 172
    const v1, 0x7f0d0181

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    .line 174
    const v1, 0x7f0d0182

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNameMarginRight:I

    .line 176
    const v1, 0x7f0d0183

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    .line 179
    const v1, 0x7f0a00f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneColor:I

    .line 180
    const v1, 0x7f0a00f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneInverseColor:I

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 184
    const v1, 0x7f020027

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    .line 186
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 187
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 188
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 189
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ee

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 190
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 191
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 194
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 195
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 196
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 197
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 198
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 201
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 202
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 203
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00f0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 204
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00de

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    .line 205
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 207
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 212
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 213
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 214
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 216
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 219
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 220
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00db

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 221
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 223
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 224
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundFadePaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00dc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 225
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundFadePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 227
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    .line 229
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 230
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a00f3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 231
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 232
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 234
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sResizePaint:Landroid/graphics/Paint;

    .line 236
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_1a6
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/views/StreamOneUpCommentView;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    return v0
.end method

.method private removeSetPressedRunnable()V
    .registers 2

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mSetPressedRunnable:Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;

    if-eqz v0, :cond_9

    .line 701
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mSetPressedRunnable:Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 703
    :cond_9
    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;Z)V
    .registers 5
    .parameter "cursor"
    .parameter "isFlagged"

    .prologue
    .line 623
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setAuthor(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setComment(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 628
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setDate(J)V

    .line 629
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setPlusOne([B)V

    .line 631
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    .line 632
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->requestLayout()V

    .line 633
    return-void
.end method

.method public final cancelPressedState()V
    .registers 2

    .prologue
    .line 692
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    if-eqz v0, :cond_a

    .line 693
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    .line 694
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    .line 696
    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->removeSetPressedRunnable()V

    .line 697
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "event"

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 444
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v3, v7

    .line 445
    .local v3, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v4, v7

    .line 447
    .local v4, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_a2

    move v5, v6

    .line 501
    :goto_15
    return v5

    .line 449
    :pswitch_16
    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_34

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 450
    .local v1, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v3, v4, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v7

    if-eqz v7, :cond_1c

    .line 451
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 452
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    goto :goto_15

    .line 457
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_34
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mSetPressedRunnable:Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;

    if-nez v5, :cond_3f

    .line 458
    new-instance v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;

    invoke-direct {v5, p0, v6}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpCommentView;B)V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mSetPressedRunnable:Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;

    .line 460
    :cond_3f
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mSetPressedRunnable:Lcom/google/android/apps/plus/views/StreamOneUpCommentView$SetPressedRunnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {p0, v5, v7, v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v5, v6

    .line 461
    goto :goto_15

    .line 465
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_4b
    iput-object v8, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 466
    iput-boolean v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    .line 467
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->removeSetPressedRunnable()V

    .line 468
    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_58
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_68

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 469
    .restart local v1       #item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v3, v4, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_58

    .line 472
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_68
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    move v5, v6

    .line 473
    goto :goto_15

    .line 477
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_6d
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    .line 478
    .local v2, wasChecked:Z
    iput-boolean v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    .line 479
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->removeSetPressedRunnable()V

    .line 480
    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v7, :cond_84

    .line 481
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v7, 0x3

    invoke-interface {v6, v3, v4, v7}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 482
    iput-object v8, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 483
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    goto :goto_15

    .line 486
    :cond_84
    if-eqz v2, :cond_89

    .line 487
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    :cond_89
    move v5, v6

    .line 489
    goto :goto_15

    .line 494
    .end local v2           #wasChecked:Z
    :pswitch_8b
    if-ltz v3, :cond_9b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getWidth()I

    move-result v5

    if-ge v3, v5, :cond_9b

    if-ltz v4, :cond_9b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getHeight()I

    move-result v5

    if-lt v4, v5, :cond_9e

    .line 495
    :cond_9b
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->removeSetPressedRunnable()V

    :cond_9e
    move v5, v6

    .line 497
    goto/16 :goto_15

    .line 447
    nop

    :pswitch_data_a2
    .packed-switch 0x0
        :pswitch_16
        :pswitch_4b
        :pswitch_8b
        :pswitch_6d
    .end packed-switch
.end method

.method public final getAuthorId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCommentContent()Ljava/lang/String;
    .registers 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    return-object v0
.end method

.method public final getCommentId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlusOneByMe()Z
    .registers 2

    .prologue
    .line 576
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    return v0
.end method

.method public final getPlusOneCount()I
    .registers 2

    .prologue
    .line 583
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    return v0
.end method

.method public final getPlusOneId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    return-object v0
.end method

.method public invalidate()V
    .registers 4

    .prologue
    const/16 v2, 0xa

    .line 240
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->invalidate()V

    .line 241
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    if-eqz v1, :cond_50

    .line 242
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 243
    .local v0, sb:Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 246
    :cond_1d
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    if-eqz v1, :cond_2a

    .line 247
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 249
    :cond_2a
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    if-eqz v1, :cond_37

    .line 250
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 252
    :cond_37
    iget v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    if-lez v1, :cond_46

    .line 253
    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 255
    :cond_46
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 257
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    .line 259
    .end local v0           #sb:Ljava/lang/StringBuffer;
    :cond_50
    return-void
.end method

.method public final isFlagged()Z
    .registers 2

    .prologue
    .line 590
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mIsFlagged:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 520
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onAttachedToWindow()V

    .line 521
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 522
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_9

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->onAvatarChanged(Ljava/lang/String;)V

    .line 535
    :cond_9
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 526
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDetachedFromWindow()V

    .line 527
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 528
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 12
    .parameter "canvas"

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 286
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDraw(Landroid/graphics/Canvas;)V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getWidth()I

    move-result v7

    .line 289
    .local v7, width:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getHeight()I

    move-result v6

    .line 291
    .local v6, height:I
    int-to-float v3, v7

    int-to-float v4, v6

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_d1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_fc

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_29
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_4e
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mIsFlagged:Z

    if-eqz v0, :cond_d1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getRight()I

    move-result v4

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFlaggedCommentFadeArea:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sBackgroundFadePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 295
    :cond_d1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    if-eqz v0, :cond_e3

    .line 296
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    sub-int v1, v6, v1

    invoke-virtual {v0, v8, v8, v7, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 297
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 300
    :cond_e3
    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginLeft:I

    int-to-float v1, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    sub-int v0, v6, v0

    int-to-float v2, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginRight:I

    sub-int v0, v7, v0

    int-to-float v3, v0

    sget v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    sub-int v0, v6, v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 302
    return-void

    .line 293
    :cond_fc
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_29
.end method

.method protected onMeasure(II)V
    .registers 20
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 263
    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onMeasure(II)V

    .line 265
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingLeft()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginLeft:I

    add-int v12, v1, v2

    .line 266
    .local v12, xStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingTop()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginTop:I

    add-int v13, v1, v2

    .line 268
    .local v13, yStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getMeasuredWidth()I

    move-result v11

    .line 269
    .local v11, width:I
    sub-int v1, v11, v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginRight:I

    sub-int v10, v1, v2

    .line 271
    .local v10, contentWidth:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 272
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 274
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginTop:I

    add-int v8, v13, v1

    new-instance v1, Lcom/google/android/apps/plus/views/ClickableUserImage;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    const/4 v7, 0x2

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    add-int/2addr v2, v12

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    add-int/2addr v3, v8

    invoke-virtual {v1, v12, v8, v2, v3}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    add-int/2addr v1, v2

    add-int v9, v12, v1

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginTop:I

    sub-int v14, v8, v1

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v4, v1

    new-instance v1, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sub-int v1, v10, v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    int-to-float v1, v1

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v3, v1, v5}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sub-int v1, v10, v1

    sget v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    sub-int/2addr v1, v3

    sub-int/2addr v1, v4

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    const/4 v4, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v3, v2, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-instance v1, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1, v9, v14}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getWidth()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNameMarginRight:I

    add-int/2addr v1, v2

    add-int/2addr v1, v9

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v2, v3

    add-int/2addr v2, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    sub-int v1, v10, v1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    sub-int v14, v1, v2

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarSize:I

    add-int/2addr v1, v12

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarMarginRight:I

    add-int v15, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v1

    add-int v16, v13, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    if-lez v1, :cond_1fc

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08039e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    const/4 v6, 0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " &nbsp; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOnePaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    if-eqz v2, :cond_1f8

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneInverseColor:I

    :goto_16e
    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v7, v1, v2, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    const/16 v1, 0x21

    invoke-interface {v7, v3, v2, v4, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    move-object v2, v7

    :goto_189
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sFontSpacing:F

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move v4, v14

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v1

    add-int v1, v1, v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 276
    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sMarginBottom:I

    add-int/2addr v1, v13

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sDividerThickness:I

    add-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setMeasuredDimension(II)V

    .line 279
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v1, :cond_1f7

    .line 280
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    move-object/from16 v0, p0

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    .line 282
    :cond_1f7
    return-void

    .line 274
    :cond_1f8
    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sPlusOneColor:I

    goto/16 :goto_16e

    :cond_1fc
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto :goto_189
.end method

.method public onRecycle()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 539
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    iput v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    .line 540
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 541
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPressed:Z

    .line 542
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 508
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStart()V

    .line 509
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 510
    return-void
.end method

.method public final onStop()V
    .registers 2

    .prologue
    .line 514
    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStop()V

    .line 515
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->sAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 516
    return-void
.end method

.method public setAuthor(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "authorId"
    .parameter "authorName"

    .prologue
    const/4 v3, 0x0

    .line 639
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    .line 640
    iput-object p2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    if-nez v0, :cond_23

    .line 642
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorName:Ljava/lang/String;

    .line 643
    const-string v0, "StreamOneUp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "===> Author name was null for gaia id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    :cond_23
    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mNameLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 647
    iput-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 648
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    .line 649
    return-void
.end method

.method public setComment(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 5
    .parameter "commentId"
    .parameter "commentContent"
    .parameter "isFlagged"

    .prologue
    .line 655
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentId:Ljava/lang/String;

    .line 656
    iput-object p2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mCommentContent:Ljava/lang/String;

    .line 657
    iput-boolean p3, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mIsFlagged:Z

    .line 659
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 660
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    .line 661
    return-void
.end method

.method public setDate(J)V
    .registers 4
    .parameter "date"

    .prologue
    .line 667
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/util/Dates;->getAbbreviatedRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDate:Ljava/lang/String;

    .line 670
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    .line 671
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    .line 672
    return-void
.end method

.method public setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V
    .registers 2
    .parameter "oneUpListener"

    .prologue
    .line 597
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 598
    return-void
.end method

.method public setPlusOne([B)V
    .registers 5
    .parameter "plusOneBytes"

    .prologue
    const/4 v2, 0x0

    .line 678
    if-eqz p1, :cond_1d

    .line 679
    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v0

    .line 680
    .local v0, plusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    .line 681
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    .line 682
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    .line 688
    .end local v0           #plusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    :goto_19
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mContentDescriptionDirty:Z

    .line 689
    return-void

    .line 684
    :cond_1d
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneId:Ljava/lang/String;

    .line 685
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneByMe:Z

    .line 686
    iput v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->mPlusOneCount:I

    goto :goto_19
.end method
