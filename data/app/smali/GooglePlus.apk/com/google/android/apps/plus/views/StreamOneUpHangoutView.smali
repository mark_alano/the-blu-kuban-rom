.class public Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;
.super Landroid/view/View;
.source "StreamOneUpHangoutView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;


# static fields
.field protected static sActiveTextPaint:Landroid/text/TextPaint;

.field protected static sAvatarSize:I

.field protected static sAvatarSpacing:I

.field protected static sButtonMarginBottom:I

.field protected static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field protected static sDefaultTextPaint:Landroid/text/TextPaint;

.field protected static sHangoutActiveBitmap:Landroid/graphics/Bitmap;

.field private static sHangoutCardViewInitialized:Z

.field protected static sHangoutJoinDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field protected static sHangoutJoinPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field protected static sHangoutOverBitmap:Landroid/graphics/Bitmap;

.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sJoinButtonPaint:Landroid/text/TextPaint;

.field protected static sMaxHangoutAvatarsToDisplay:I

.field protected static sMaxWidth:I

.field protected static sNameMargin:I

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sUnsupportedTextPaint:Landroid/text/TextPaint;


# instance fields
.field private mAuthorId:Ljava/lang/String;

.field private mAuthorName:Ljava/lang/String;

.field private mAvatarsToDisplay:I

.field private final mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private final mHangoutAvatars:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableUserImage;",
            ">;"
        }
    .end annotation
.end field

.field private mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

.field private mHangoutIcon:Landroid/graphics/Bitmap;

.field private mHangoutIconRect:Landroid/graphics/Rect;

.field private mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mParticipantNames:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    .prologue
    const v7, 0x7f0d015c

    const v6, 0x7f0d015b

    const v5, 0x7f0a00f4

    const v4, 0x7f0d0196

    const/4 v3, 0x1

    .line 84
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    .line 67
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 99
    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutCardViewInitialized:Z

    if-nez v1, :cond_13b

    .line 100
    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutCardViewInitialized:Z

    .line 102
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f02002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 108
    const v1, 0x7f02002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 111
    invoke-static {p1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 112
    const v1, 0x7f02010a

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutActiveBitmap:Landroid/graphics/Bitmap;

    .line 113
    const v1, 0x7f0200c3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutOverBitmap:Landroid/graphics/Bitmap;

    .line 115
    const v1, 0x7f0d0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    .line 116
    const v1, 0x7f0d0184

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    .line 118
    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxWidth:I

    .line 119
    const v1, 0x7f0d0185

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sButtonMarginBottom:I

    .line 121
    const v1, 0x7f0d0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sNameMargin:I

    .line 123
    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxHangoutAvatarsToDisplay:I

    .line 125
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sResizePaint:Landroid/graphics/Paint;

    .line 127
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 128
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 129
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 130
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 132
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 135
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 136
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 137
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00f6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 139
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 141
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 144
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 145
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 146
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 147
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 149
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 150
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 153
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 154
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 155
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00d7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 156
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 158
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 159
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 85
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_13b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 11
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v7, 0x7f0d015c

    const v6, 0x7f0d015b

    const v5, 0x7f0a00f4

    const v4, 0x7f0d0196

    const/4 v3, 0x1

    .line 88
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    .line 67
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 99
    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutCardViewInitialized:Z

    if-nez v1, :cond_13b

    .line 100
    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutCardViewInitialized:Z

    .line 102
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f02002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 108
    const v1, 0x7f02002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 111
    invoke-static {p1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 112
    const v1, 0x7f02010a

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutActiveBitmap:Landroid/graphics/Bitmap;

    .line 113
    const v1, 0x7f0200c3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutOverBitmap:Landroid/graphics/Bitmap;

    .line 115
    const v1, 0x7f0d0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    .line 116
    const v1, 0x7f0d0184

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    .line 118
    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxWidth:I

    .line 119
    const v1, 0x7f0d0185

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sButtonMarginBottom:I

    .line 121
    const v1, 0x7f0d0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sNameMargin:I

    .line 123
    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxHangoutAvatarsToDisplay:I

    .line 125
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sResizePaint:Landroid/graphics/Paint;

    .line 127
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 128
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 129
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 130
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 132
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 135
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 136
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 137
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00f6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 139
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 141
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 144
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 145
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 146
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 147
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 149
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 150
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 153
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 154
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 155
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00d7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 156
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 158
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 159
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 89
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_13b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v7, 0x7f0d015c

    const v6, 0x7f0d015b

    const v5, 0x7f0a00f4

    const v4, 0x7f0d0196

    const/4 v3, 0x1

    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    .line 67
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 99
    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutCardViewInitialized:Z

    if-nez v1, :cond_13b

    .line 100
    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutCardViewInitialized:Z

    .line 102
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f02002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 108
    const v1, 0x7f02002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 111
    invoke-static {p1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 112
    const v1, 0x7f02010a

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutActiveBitmap:Landroid/graphics/Bitmap;

    .line 113
    const v1, 0x7f0200c3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutOverBitmap:Landroid/graphics/Bitmap;

    .line 115
    const v1, 0x7f0d0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    .line 116
    const v1, 0x7f0d0184

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    .line 118
    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxWidth:I

    .line 119
    const v1, 0x7f0d0185

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sButtonMarginBottom:I

    .line 121
    const v1, 0x7f0d0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sNameMargin:I

    .line 123
    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxHangoutAvatarsToDisplay:I

    .line 125
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sResizePaint:Landroid/graphics/Paint;

    .line 127
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 128
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 129
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 130
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 132
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 135
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 136
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 137
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00f6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 139
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 141
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 144
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 145
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 146
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 147
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 149
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 150
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 153
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 154
    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 155
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a00d7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 156
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 158
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 159
    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 94
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_13b
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/HangoutData;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/OneUpListener;)V
    .registers 20
    .parameter "hangoutData"
    .parameter "authorName"
    .parameter "authorId"
    .parameter "oneUpListener"

    .prologue
    .line 166
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutIcon:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutIconRect:Landroid/graphics/Rect;

    .line 168
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    .line 169
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mAuthorName:Ljava/lang/String;

    .line 170
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mAuthorId:Ljava/lang/String;

    .line 172
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .local v14, sb:Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    iget-object v13, v2, Lcom/google/api/services/plusi/model/HangoutData;->occupant:Ljava/util/List;

    .line 174
    .local v13, occupants:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/HangoutOccupant;>;"
    if-eqz v13, :cond_7b

    .line 175
    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxHangoutAvatarsToDisplay:I

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 176
    .local v11, maxAvatars:I
    const/4 v8, 0x0

    .line 177
    .local v8, i:I
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, i$:Ljava/util/Iterator;
    :goto_45
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/HangoutOccupant;

    .line 178
    .local v12, occupant:Lcom/google/api/services/plusi/model/HangoutOccupant;
    add-int/lit8 v9, v8, 0x1

    .end local v8           #i:I
    .local v9, i:I
    if-ge v8, v11, :cond_7b

    .line 179
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableUserImage;

    iget-object v3, v12, Lcom/google/api/services/plusi/model/HangoutOccupant;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v4, v12, Lcom/google/api/services/plusi/model/HangoutOccupant;->avatarurl:Ljava/lang/String;

    iget-object v5, v12, Lcom/google/api/services/plusi/model/HangoutOccupant;->name:Ljava/lang/String;

    const/4 v7, 0x2

    move-object v2, p0

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V

    .line 185
    .local v1, userImage:Lcom/google/android/apps/plus/views/ClickableUserImage;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    const/16 v2, 0xa

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v12, Lcom/google/api/services/plusi/model/HangoutOccupant;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v8, v9

    .line 188
    .end local v9           #i:I
    .restart local v8       #i:I
    goto :goto_45

    .line 190
    .end local v1           #userImage:Lcom/google/android/apps/plus/views/ClickableUserImage;
    .end local v8           #i:I
    .end local v10           #i$:Ljava/util/Iterator;
    .end local v11           #maxAvatars:I
    .end local v12           #occupant:Lcom/google/api/services/plusi/model/HangoutOccupant;
    :cond_7b
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mParticipantNames:Ljava/lang/String;

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->invalidate()V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->requestLayout()V

    .line 194
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 210
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    .line 211
    .local v2, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 213
    .local v3, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_68

    move v4, v5

    .line 252
    :goto_15
    return v4

    .line 215
    :pswitch_16
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_34

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 216
    .local v1, item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 217
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->invalidate()V

    goto :goto_15

    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_34
    move v4, v5

    .line 223
    goto :goto_15

    .line 227
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_36
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 228
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_3e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    .line 229
    .restart local v1       #item:Lcom/google/android/apps/plus/views/ClickableItem;
    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_3e

    .line 232
    .end local v1           #item:Lcom/google/android/apps/plus/views/ClickableItem;
    :cond_4e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->invalidate()V

    move v4, v5

    .line 233
    goto :goto_15

    .line 237
    .end local v0           #i$:Ljava/util/Iterator;
    :pswitch_53
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_63

    .line 238
    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 239
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->invalidate()V

    goto :goto_15

    :cond_63
    move v4, v5

    .line 243
    goto :goto_15

    :pswitch_65
    move v4, v5

    .line 248
    goto :goto_15

    .line 213
    nop

    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_16
        :pswitch_36
        :pswitch_65
        :pswitch_53
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 198
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 199
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 200
    return-void
.end method

.method public onAvatarChanged(Ljava/lang/String;)V
    .registers 5
    .parameter "gaiaId"

    .prologue
    .line 259
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 260
    .local v0, avatar:Lcom/google/android/apps/plus/views/ClickableUserImage;
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->onAvatarChanged(Ljava/lang/String;)V

    goto :goto_6

    .line 262
    .end local v0           #avatar:Lcom/google/android/apps/plus/views/ClickableUserImage;
    :cond_16
    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .registers 8
    .parameter "button"

    .prologue
    .line 266
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v2, :cond_47

    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 268
    .local v0, context:Landroid/content/Context;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/Hangout;->isViewOnlyHangoutOnAir(Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v2

    if-eqz v2, :cond_48

    .line 270
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://www.youtube.com/watch?v="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/HangoutData;->broadcastDetails:Lcom/google/api/services/plusi/model/HangoutDataBroadcastDetails;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/HangoutDataBroadcastDetails;->youtubeLiveId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 273
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 275
    const-string v2, "com.google.android.youtube"

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/hangout/Utils;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_44

    .line 276
    const-string v2, "com.google.android.youtube"

    const-string v3, "com.google.android.youtube.WatchActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    :cond_44
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 286
    .end local v0           #context:Landroid/content/Context;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_47
    :goto_47
    return-void

    .line 282
    .restart local v0       #context:Landroid/content/Context;
    :cond_48
    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mAuthorId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mAuthorName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    invoke-static {v2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/service/Hangout;->enterGreenRoomFromStream(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/HangoutData;)V

    goto :goto_47
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 204
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 205
    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 206
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    .prologue
    const/4 v4, 0x0

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutIcon:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-nez v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_2d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_36
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mAvatarsToDisplay:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_45
    if-ltz v2, :cond_64

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_57

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    :cond_57
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_45

    :cond_64
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_85

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 320
    :cond_85
    return-void
.end method

.method protected onMeasure(II)V
    .registers 24
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 290
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v16

    .line 292
    .local v16, widthDimension:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_2be

    .line 301
    sget v15, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxWidth:I

    .line 304
    .local v15, width:I
    :goto_d
    const/high16 v2, 0x4000

    invoke-static {v15, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-super {v0, v2, v1}, Landroid/view/View;->onMeasure(II)V

    .line 306
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getPaddingLeft()I

    move-result v17

    .line 307
    .local v17, xStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getPaddingTop()I

    move-result v18

    .line 309
    .local v18, yStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getMeasuredWidth()I

    move-result v14

    .line 310
    .local v14, measuredWidth:I
    sub-int v2, v14, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getPaddingRight()I

    move-result v3

    sub-int v13, v2, v3

    .line 312
    .local v13, contentWidth:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/HangoutData;->occupant:Ljava/util/List;

    if-nez v2, :cond_213

    const/4 v2, 0x0

    move/from16 v19, v2

    :goto_3d
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/Hangout;->isInProgress(Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v3

    if-eqz v3, :cond_23a

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    move-object/from16 v0, v20

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutActiveBitmap:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v2, v3, :cond_221

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sUnsupportedTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->getErrorMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    move-object v10, v2

    move-object v2, v5

    :goto_66
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int v7, v13, v5

    div-int/lit8 v7, v7, 0x2

    add-int v7, v7, v17

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutIcon:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Rect;

    add-int/2addr v5, v7

    add-int v8, v18, v6

    move/from16 v0, v18

    invoke-direct {v2, v7, v0, v5, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutIconRect:Landroid/graphics/Rect;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sNameMargin:I

    add-int/2addr v2, v6

    add-int v11, v18, v2

    invoke-virtual {v4, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v5, v2

    new-instance v2, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f80

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getWidth()I

    move-result v2

    sub-int v2, v13, v2

    div-int/lit8 v2, v2, 0x2

    add-int v2, v2, v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v4, v2, v11}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sNameMargin:I

    add-int/2addr v2, v4

    add-int/2addr v2, v11

    sget-object v4, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-ne v10, v4, :cond_2ba

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    invoke-static {v4}, Lcom/google/android/apps/plus/service/Hangout;->isViewOnlyHangoutOnAir(Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v4

    if-eqz v4, :cond_259

    const v4, 0x7f0802e1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_d7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v7, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sJoinButtonPaint:Landroid/text/TextPaint;

    sget-object v8, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v9, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutJoinPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, v20

    move-object/from16 v10, p0

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int v5, v13, v5

    div-int/lit8 v5, v5, 0x2

    add-int v5, v5, v17

    invoke-virtual {v4, v5, v2}, Landroid/graphics/Rect;->offset(II)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sButtonMarginBottom:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    move v12, v2

    :goto_128
    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v2, 0x7f0e0021

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    move/from16 v0, v19

    invoke-virtual {v6, v2, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int v2, v13, v2

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    add-int/2addr v5, v7

    div-int/2addr v2, v5

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    sub-int v8, v19, v4

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    add-int/2addr v5, v7

    mul-int/2addr v5, v4

    add-int/2addr v2, v5

    if-lez v8, :cond_2b6

    const v5, 0x7f0e0021

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    invoke-virtual {v6, v5, v8, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v7, v7

    add-int v9, v2, v7

    if-le v9, v13, :cond_2b2

    add-int/lit8 v5, v8, -0x1

    add-int/lit8 v4, v4, -0x1

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    add-int/2addr v7, v8

    mul-int/2addr v7, v4

    add-int/2addr v2, v7

    const v7, 0x7f0e0021

    add-int/lit8 v8, v5, -0x1

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v9, v10

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v6, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v6

    float-to-int v7, v6

    move/from16 v19, v4

    :goto_1ae
    new-instance v4, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v6, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f80

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getWidth()I

    move-result v4

    sub-int/2addr v13, v4

    .end local v13           #contentWidth:I
    :goto_1c8
    sub-int v2, v13, v2

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    add-int v4, v4, v17

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v4, v2

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mAvatarsToDisplay:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v2, 0x0

    move v5, v2

    :goto_1e1
    if-ge v5, v6, :cond_26d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ClickableUserImage;

    move/from16 v0, v19

    if-ge v5, v0, :cond_264

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    add-int/2addr v7, v4

    sget v8, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    add-int/2addr v8, v12

    invoke-virtual {v2, v4, v12, v7, v8}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    sget v7, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSpacing:I

    add-int/2addr v2, v7

    add-int/2addr v2, v4

    :goto_200
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v2

    goto :goto_1e1

    .line 294
    .end local v14           #measuredWidth:I
    .end local v15           #width:I
    .end local v17           #xStart:I
    .end local v18           #yStart:I
    :sswitch_205
    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sMaxWidth:I

    move/from16 v0, v16

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 295
    .restart local v15       #width:I
    goto/16 :goto_d

    .line 297
    .end local v15           #width:I
    :sswitch_20f
    move/from16 v15, v16

    .line 298
    .restart local v15       #width:I
    goto/16 :goto_d

    .line 312
    .restart local v13       #contentWidth:I
    .restart local v14       #measuredWidth:I
    .restart local v17       #xStart:I
    .restart local v18       #yStart:I
    :cond_213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mHangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/HangoutData;->occupant:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_3d

    :cond_221
    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sActiveTextPaint:Landroid/text/TextPaint;

    const v3, 0x7f0803d8

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mAuthorName:Ljava/lang/String;

    aput-object v8, v6, v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v10, v2

    move-object v2, v5

    goto/16 :goto_66

    :cond_23a
    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sHangoutOverBitmap:Landroid/graphics/Bitmap;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0e0024

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    move/from16 v0, v19

    invoke-virtual {v3, v6, v0, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v10, v2

    move-object v2, v5

    goto/16 :goto_66

    :cond_259
    const v4, 0x7f0803d9

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_d7

    .end local v13           #contentWidth:I
    :cond_264
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v2, v7, v8, v9, v10}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    move v2, v4

    goto :goto_200

    :cond_26d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v2, :cond_288

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mExtraParticpantsLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v12

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    :cond_288
    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->sAvatarSize:I

    add-int v18, v12, v2

    new-instance v2, Ljava/lang/StringBuilder;

    .end local v18           #yStart:I
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mParticipantNames:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 314
    .restart local v18       #yStart:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getPaddingBottom()I

    move-result v2

    add-int v2, v2, v18

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->setMeasuredDimension(II)V

    .line 315
    return-void

    .restart local v13       #contentWidth:I
    :cond_2b2
    move/from16 v19, v4

    goto/16 :goto_1ae

    :cond_2b6
    move/from16 v19, v4

    goto/16 :goto_1c8

    :cond_2ba
    move v12, v2

    goto/16 :goto_128

    .line 292
    nop

    :sswitch_data_2be
    .sparse-switch
        -0x80000000 -> :sswitch_205
        0x40000000 -> :sswitch_20f
    .end sparse-switch
.end method

.method public final processClick(FF)V
    .registers 7
    .parameter "x"
    .parameter "y"

    .prologue
    .line 511
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_29

    .line 512
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 513
    .local v1, rect:Landroid/graphics/Rect;
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 514
    .local v0, loc:[I
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->getLocationOnScreen([I)V

    .line 515
    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    float-to-int v2, v2

    const/4 v3, 0x1

    aget v3, v0, v3

    int-to-float v3, v3

    sub-float v3, p2, v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 516
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V

    .line 519
    .end local v0           #loc:[I
    .end local v1           #rect:Landroid/graphics/Rect;
    :cond_29
    return-void
.end method
