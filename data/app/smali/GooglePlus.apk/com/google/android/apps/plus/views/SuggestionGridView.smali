.class public Lcom/google/android/apps/plus/views/SuggestionGridView;
.super Landroid/widget/LinearLayout;
.source "SuggestionGridView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

.field private mObserver:Landroid/database/DataSetObserver;

.field private mRows:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/views/ColumnGridView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 100
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mRows:Ljava/util/HashMap;

    .line 35
    new-instance v0, Lcom/google/android/apps/plus/views/SuggestionGridView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SuggestionGridView$1;-><init>(Lcom/google/android/apps/plus/views/SuggestionGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mObserver:Landroid/database/DataSetObserver;

    .line 101
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->setOrientation(I)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mRows:Ljava/util/HashMap;

    .line 35
    new-instance v0, Lcom/google/android/apps/plus/views/SuggestionGridView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SuggestionGridView$1;-><init>(Lcom/google/android/apps/plus/views/SuggestionGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mObserver:Landroid/database/DataSetObserver;

    .line 106
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->setOrientation(I)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mRows:Ljava/util/HashMap;

    .line 35
    new-instance v0, Lcom/google/android/apps/plus/views/SuggestionGridView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SuggestionGridView$1;-><init>(Lcom/google/android/apps/plus/views/SuggestionGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mObserver:Landroid/database/DataSetObserver;

    .line 111
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->setOrientation(I)V

    .line 112
    return-void
.end method


# virtual methods
.method public final getScrollPositions()Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;
    .registers 9

    .prologue
    .line 189
    new-instance v5, Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    invoke-direct {v5}, Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;-><init>()V

    .line 190
    .local v5, positions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;
    iget-object v7, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mRows:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 191
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 192
    .local v0, category:Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 193
    .local v2, grid:Lcom/google/android/apps/plus/views/ColumnGridView;
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 195
    .local v6, v:Landroid/view/View;
    if-eqz v6, :cond_3a

    .line 196
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 201
    .local v4, offset:I
    :goto_32
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->getFirstVisiblePosition()I

    move-result v7

    invoke-virtual {v5, v0, v7, v4}, Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;->setScrollPosition(Ljava/lang/String;II)V

    goto :goto_f

    .line 198
    .end local v4           #offset:I
    :cond_3a
    const/4 v4, 0x0

    .restart local v4       #offset:I
    goto :goto_32

    .line 203
    .end local v0           #category:Ljava/lang/String;
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;>;"
    .end local v2           #grid:Lcom/google/android/apps/plus/views/ColumnGridView;
    .end local v4           #offset:I
    .end local v6           #v:Landroid/view/View;
    :cond_3c
    return-object v5
.end method

.method protected final onDataChanged()V
    .registers 14

    .prologue
    const v12, 0x7f090234

    const/4 v11, 0x1

    .line 127
    iget-object v9, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->getCategories()Ljava/util/ArrayList;

    move-result-object v0

    .line 128
    .local v0, categories:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 129
    .local v4, count:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getChildCount()I

    move-result v9

    if-le v4, v9, :cond_4b

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 131
    .local v7, inflater:Landroid/view/LayoutInflater;
    :goto_1c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getChildCount()I

    move-result v9

    if-le v4, v9, :cond_5b

    .line 132
    const v9, 0x7f0300cd

    const/4 v10, 0x0

    invoke-virtual {v7, v9, p0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 133
    .local v8, view:Landroid/view/View;
    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 134
    .local v5, gridView:Lcom/google/android/apps/plus/views/ColumnGridView;
    invoke-virtual {v5, v11}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0d01a6

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/ColumnGridView;->setMinColumnWidth(I)V

    .line 140
    invoke-virtual {v5, v11}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    .line 142
    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    .line 144
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/views/SuggestionGridView;->addView(Landroid/view/View;)V

    goto :goto_1c

    .line 147
    .end local v5           #gridView:Lcom/google/android/apps/plus/views/ColumnGridView;
    .end local v7           #inflater:Landroid/view/LayoutInflater;
    .end local v8           #view:Landroid/view/View;
    :cond_4b
    :goto_4b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getChildCount()I

    move-result v9

    if-le v9, v4, :cond_5b

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getChildCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/views/SuggestionGridView;->removeViewAt(I)V

    goto :goto_4b

    .line 152
    :cond_5b
    iget-object v9, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mRows:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->clear()V

    .line 153
    const/4 v6, 0x0

    .local v6, i:I
    :goto_61
    if-ge v6, v4, :cond_bf

    .line 154
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;

    .line 155
    .local v1, category:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 156
    .local v3, categoryView:Landroid/view/View;
    const v9, 0x7f090233

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 157
    .local v2, categoryLabel:Landroid/widget/TextView;
    const-string v9, "#"

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->getCategory()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_af

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0803fd

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :goto_94
    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 166
    .restart local v5       #gridView:Lcom/google/android/apps/plus/views/ColumnGridView;
    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v9

    if-eq v9, v1, :cond_bb

    .line 167
    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 172
    :goto_a3
    iget-object v9, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mRows:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->getCategory()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    add-int/lit8 v6, v6, 0x1

    goto :goto_61

    .line 161
    .end local v5           #gridView:Lcom/google/android/apps/plus/views/ColumnGridView;
    :cond_af
    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->getCategoryLabel()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_94

    .line 169
    .restart local v5       #gridView:Lcom/google/android/apps/plus/views/ColumnGridView;
    :cond_bb
    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->notifyDataSetChanged()V

    goto :goto_a3

    .line 174
    .end local v1           #category:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
    .end local v2           #categoryLabel:Landroid/widget/TextView;
    .end local v3           #categoryView:Landroid/view/View;
    .end local v5           #gridView:Lcom/google/android/apps/plus/views/ColumnGridView;
    :cond_bf
    return-void
.end method

.method public final onScroll(Lcom/google/android/apps/plus/views/ColumnGridView;IIIII)V
    .registers 7
    .parameter "view"
    .parameter "firstItem"
    .parameter "visibleOffset"
    .parameter "viewItemCount"
    .parameter "totalItemCount"
    .parameter "scrollDelta"

    .prologue
    .line 186
    return-void
.end method

.method public final onScrollStateChanged(Lcom/google/android/apps/plus/views/ColumnGridView;I)V
    .registers 4
    .parameter "view"
    .parameter "scrollState"

    .prologue
    const/4 v0, 0x1

    .line 178
    if-ne p2, v0, :cond_6

    .line 179
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestionGridView;->requestDisallowInterceptTouchEvent(Z)V

    .line 181
    :cond_6
    return-void
.end method

.method public setAdapter(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;)V
    .registers 4
    .parameter "adapter"

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    if-eqz v0, :cond_b

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 122
    :cond_b
    iput-object p1, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 124
    return-void
.end method

.method public setScrollPositions(Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;)V
    .registers 10
    .parameter "positions"

    .prologue
    .line 207
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestionGridView;->mRows:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_a
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_46

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 208
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 209
    .local v0, category:Ljava/lang/String;
    #getter for: Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;->positions:Ljava/util/HashMap;
    invoke-static {p1}, Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;->access$000(Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 210
    .local v5, position:Ljava/lang/Integer;
    #getter for: Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;->offsets:Ljava/util/HashMap;
    invoke-static {p1}, Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;->access$100(Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 211
    .local v4, offset:Ljava/lang/Integer;
    if-eqz v5, :cond_a

    if-eqz v4, :cond_a

    .line 212
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 213
    .local v2, grid:Lcom/google/android/apps/plus/views/ColumnGridView;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionFromTop(II)V

    goto :goto_a

    .line 216
    .end local v0           #category:Ljava/lang/String;
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;>;"
    .end local v2           #grid:Lcom/google/android/apps/plus/views/ColumnGridView;
    .end local v4           #offset:Ljava/lang/Integer;
    .end local v5           #position:Ljava/lang/Integer;
    :cond_46
    return-void
.end method
