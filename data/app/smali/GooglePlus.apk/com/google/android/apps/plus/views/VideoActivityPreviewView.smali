.class public Lcom/google/android/apps/plus/views/VideoActivityPreviewView;
.super Lcom/google/android/apps/plus/views/ActivityPreviewView;
.source "VideoActivityPreviewView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ActivityPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method


# virtual methods
.method public setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    .registers 10
    .parameter "activity"

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 54
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ActivityPreviewView;->setActivity(Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/VideoActivityPreviewView;->getActivity()Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/VideoActivityPreviewView;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/VideoActivityPreviewView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300b5

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/VideoActivityPreviewView;->addView(Landroid/view/View;)V

    const v1, 0x7f09021c

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f09021d

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->getImage()Ljava/lang/String;

    move-result-object v0

    if-eqz v4, :cond_66

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_66

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4c
    if-eqz v0, :cond_6a

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6a

    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    :goto_5c
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/VideoActivityPreviewView;->invalidate()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/VideoActivityPreviewView;->requestLayout()V

    .line 58
    return-void

    .line 55
    :cond_66
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4c

    :cond_6a
    invoke-virtual {v2, v7}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    goto :goto_5c
.end method
