.class public Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "EsWidgetConfigurationActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;,
        Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$WidgetCircleQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/FragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

.field private final mAdapterLock:Ljava/lang/Object;

.field private mAppWidgetId:I

.field private mDisplayingEmptyView:Z

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapterLock:Ljava/lang/Object;

    .line 60
    return-void
.end method

.method private updateDisplay()V
    .registers 8

    .prologue
    const v6, 0x7f090073

    const v5, 0x7f090066

    const v4, 0x1020004

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 252
    iget-boolean v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mDisplayingEmptyView:Z

    if-eqz v0, :cond_2c

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 254
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 255
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 256
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 263
    :goto_2b
    return-void

    .line 258
    :cond_2c
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 259
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 260
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 261
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2b
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter "savedInstance"

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 107
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 110
    .local v3, intent:Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 111
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_22

    .line 112
    const-string v5, "appWidgetId"

    invoke-virtual {v1, v5, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    .line 118
    :goto_1a
    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    if-nez v5, :cond_25

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    .line 153
    :goto_21
    return-void

    .line 115
    :cond_22
    iput v8, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    goto :goto_1a

    .line 123
    :cond_25
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    if-nez v5, :cond_4c

    .line 124
    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v5, v7, v7}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->saveCircleInfo(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureWidget(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    .line 127
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 128
    .local v4, resultValue:Landroid/content/Intent;
    const-string v5, "appWidgetId"

    iget v6, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 129
    const/4 v5, -0x1

    invoke-virtual {p0, v5, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    .line 133
    .end local v4           #resultValue:Landroid/content/Intent;
    :cond_4c
    const v5, 0x7f0300d8

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setContentView(I)V

    .line 135
    iput-boolean v9, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mDisplayingEmptyView:Z

    .line 137
    const v5, 0x102000a

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    .line 138
    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 140
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f0300d9

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 141
    .local v2, header:Landroid/view/View;
    const v5, 0x7f090246

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 142
    const v5, 0x7f090247

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 143
    .local v0, circleName:Landroid/widget/TextView;
    const v5, 0x7f08021f

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 144
    const v5, 0x7f090248

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v2, v7, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 147
    new-instance v5, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-direct {v5, p0, p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;-><init>(Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    .line 148
    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 150
    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->updateDisplay()V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v8, v7, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_21
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 7
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 160
    packed-switch p1, :pswitch_data_18

    .line 171
    :cond_4
    :goto_4
    return-object v0

    .line 162
    :pswitch_5
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 166
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x4

    sget-object v3, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$WidgetCircleQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_4

    .line 160
    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v4

    sub-int/2addr p3, v4

    .line 208
    if-gez p3, :cond_34

    .line 209
    const-string v0, "v.all.circles"

    .line 210
    .local v0, circleId:Ljava/lang/String;
    const v4, 0x7f0800e0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 228
    .local v1, circleName:Ljava/lang/String;
    :goto_12
    iget v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v4, v0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->saveCircleInfo(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v4, v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureWidget(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    .line 233
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 234
    .local v3, resultValue:Landroid/content/Intent;
    const-string v4, "appWidgetId"

    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235
    const/4 v4, -0x1

    invoke-virtual {p0, v4, v3}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    .line 237
    .end local v0           #circleId:Ljava/lang/String;
    .end local v1           #circleName:Ljava/lang/String;
    .end local v3           #resultValue:Landroid/content/Intent;
    :goto_33
    return-void

    .line 212
    :cond_34
    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapterLock:Ljava/lang/Object;

    monitor-enter v5

    .line 213
    :try_start_37
    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    if-eqz v4, :cond_69

    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    if-eqz v4, :cond_69

    .line 214
    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 215
    .local v2, cursor:Landroid/database/Cursor;
    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_55

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-gt v4, p3, :cond_5a

    .line 216
    :cond_55
    monitor-exit v5
    :try_end_56
    .catchall {:try_start_37 .. :try_end_56} :catchall_57

    goto :goto_33

    .line 225
    .end local v2           #cursor:Landroid/database/Cursor;
    :catchall_57
    move-exception v4

    monitor-exit v5

    throw v4

    .line 219
    .restart local v2       #cursor:Landroid/database/Cursor;
    :cond_5a
    :try_start_5a
    invoke-interface {v2, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 220
    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 221
    .restart local v0       #circleId:Ljava/lang/String;
    const/4 v4, 0x2

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_66
    .catchall {:try_start_5a .. :try_end_66} :catchall_57

    move-result-object v1

    .line 225
    .restart local v1       #circleName:Ljava/lang/String;
    monitor-exit v5

    goto :goto_12

    .line 223
    .end local v0           #circleId:Ljava/lang/String;
    .end local v1           #circleName:Ljava/lang/String;
    .end local v2           #cursor:Landroid/database/Cursor;
    :cond_69
    :try_start_69
    monitor-exit v5
    :try_end_6a
    .catchall {:try_start_69 .. :try_end_6a} :catchall_57

    goto :goto_33
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 33
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_1e

    :goto_9
    return-void

    :pswitch_a
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_d
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mDisplayingEmptyView:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->updateDisplay()V

    monitor-exit v1
    :try_end_19
    .catchall {:try_start_d .. :try_end_19} :catchall_1a

    goto :goto_9

    :catchall_1a
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_a
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method
