.class public Lcom/google/android/apps/plus/widget/EsWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "EsWidgetProvider.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "appWidgetId"
    .parameter "remoteViews"
    .parameter "currentActivityId"
    .parameter "showRefresh"

    .prologue
    .line 230
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    .line 231
    .local v2, homeIconIntent:Landroid/content/Intent;
    const-string v11, "com.google.android.apps.plus.widget.HOME_ACTION"

    invoke-virtual {v2, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    const/4 v11, 0x0

    const/high16 v12, 0x800

    invoke-static {p0, v11, v2, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 234
    .local v1, homeClickIntent:Landroid/app/PendingIntent;
    const v11, 0x7f09024b

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 236
    if-eqz p1, :cond_be

    const/4 v10, 0x1

    .line 237
    .local v10, showPostIcon:Z
    :goto_1b
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_c1

    const/4 v9, 0x1

    .line 239
    .local v9, showNextIcon:Z
    :goto_22
    const v11, 0x7f090251

    const/16 v12, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 240
    const v11, 0x7f090254

    const/16 v12, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 242
    const v12, 0x7f09024e

    if-eqz v10, :cond_c4

    const/4 v11, 0x0

    :goto_3c
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 243
    const v12, 0x7f090250

    if-eqz p5, :cond_c8

    const/4 v11, 0x0

    :goto_47
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 244
    const v12, 0x7f090253

    if-eqz v9, :cond_cc

    const/4 v11, 0x0

    :goto_52
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 246
    const v12, 0x7f09024f

    if-eqz v10, :cond_cf

    if-nez p5, :cond_60

    if-eqz v9, :cond_cf

    :cond_60
    const/4 v11, 0x0

    :goto_61
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 248
    const v12, 0x7f090252

    if-eqz p5, :cond_d2

    if-eqz v9, :cond_d2

    const/4 v11, 0x0

    :goto_6e
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 251
    if-eqz v10, :cond_8e

    .line 253
    const/4 v11, 0x0

    invoke-static {p0, p1, v11}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;)Landroid/content/Intent;

    move-result-object v5

    .line 255
    .local v5, postActivityIntent:Landroid/content/Intent;
    const-string v11, "com.google.android.apps.plus.widget.POST_ACTION"

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    const/4 v11, 0x0

    const/high16 v12, 0x800

    invoke-static {p0, v11, v5, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 258
    .local v6, postClickIntent:Landroid/app/PendingIntent;
    const v11, 0x7f09024e

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 261
    .end local v5           #postActivityIntent:Landroid/content/Intent;
    .end local v6           #postClickIntent:Landroid/app/PendingIntent;
    :cond_8e
    if-eqz p5, :cond_a5

    .line 263
    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-static {p0, p2, v11, v12}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v8

    .line 265
    .local v8, refreshIntent:Landroid/content/Intent;
    const/4 v11, 0x0

    const/high16 v12, 0x800

    invoke-static {p0, v11, v8, v12}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 267
    .local v7, refreshClickIntent:Landroid/app/PendingIntent;
    const v11, 0x7f090250

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 270
    .end local v7           #refreshClickIntent:Landroid/app/PendingIntent;
    .end local v8           #refreshIntent:Landroid/content/Intent;
    :cond_a5
    if-eqz v9, :cond_bd

    .line 272
    const/4 v11, 0x0

    move-object/from16 v0, p4

    invoke-static {p0, p2, v0, v11}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    .line 274
    .local v3, nextActivityIntent:Landroid/content/Intent;
    const/4 v11, 0x0

    const/high16 v12, 0x800

    invoke-static {p0, v11, v3, v12}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 276
    .local v4, nextClickIntent:Landroid/app/PendingIntent;
    const v11, 0x7f090253

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 278
    .end local v3           #nextActivityIntent:Landroid/content/Intent;
    .end local v4           #nextClickIntent:Landroid/app/PendingIntent;
    :cond_bd
    return-void

    .line 236
    .end local v9           #showNextIcon:Z
    .end local v10           #showPostIcon:Z
    :cond_be
    const/4 v10, 0x0

    goto/16 :goto_1b

    .line 237
    .restart local v10       #showPostIcon:Z
    :cond_c1
    const/4 v9, 0x0

    goto/16 :goto_22

    .line 242
    .restart local v9       #showNextIcon:Z
    :cond_c4
    const/16 v11, 0x8

    goto/16 :goto_3c

    .line 243
    :cond_c8
    const/16 v11, 0x8

    goto/16 :goto_47

    .line 244
    :cond_cc
    const/16 v11, 0x8

    goto :goto_52

    .line 246
    :cond_cf
    const/16 v11, 0x8

    goto :goto_61

    .line 248
    :cond_d2
    const/16 v11, 0x8

    goto :goto_6e
.end method

.method public static configureWidget(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "appWidgetId"

    .prologue
    .line 289
    const-string v0, "EsWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 290
    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] configureWidget"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_23
    if-nez p1, :cond_29

    .line 294
    invoke-static {p0, p2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showTapToConfigure(Landroid/content/Context;I)V

    .line 299
    :goto_28
    return-void

    .line 297
    :cond_29
    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_28
.end method

.method private static getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;
    .registers 7
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "currentActivityId"
    .parameter "refresh"

    .prologue
    const/4 v2, 0x1

    .line 199
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/widget/EsWidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 200
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 202
    const-string v1, "activity_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    :cond_18
    if-eqz p3, :cond_1f

    .line 205
    const-string v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 210
    :cond_1f
    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 212
    return-object v0
.end method

.method public static showLoadingView(Landroid/content/Context;I)V
    .registers 9
    .parameter "context"
    .parameter "appWidgetId"

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 131
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0300de

    invoke-direct {v3, v0, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .local v3, rv:Landroid/widget/RemoteViews;
    move-object v0, p0

    move v2, p1

    move-object v4, v1

    .line 134
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    .line 137
    const v0, 0x7f090254

    invoke-virtual {v3, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 139
    const v0, 0x7f090255

    invoke-virtual {v3, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 140
    const v0, 0x7f090261

    invoke-virtual {v3, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 141
    const v0, 0x7f090249

    invoke-virtual {v3, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 142
    const v0, 0x7f09024a

    const v1, 0x7f080096

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 144
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 145
    return-void
.end method

.method public static showNoPostsFound(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "appWidgetId"

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 155
    const-string v0, "EsWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 156
    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] showNoPostsFound"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_26
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0300de

    invoke-direct {v3, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 162
    .local v3, rv:Landroid/widget/RemoteViews;
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    .line 165
    const v0, 0x7f090255

    invoke-virtual {v3, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 166
    const v0, 0x7f090261

    invoke-virtual {v3, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 167
    const v0, 0x7f090249

    invoke-virtual {v3, v0, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 168
    const v0, 0x7f09024a

    const v1, 0x7f0800dd

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 170
    invoke-static {p0, p2}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 171
    .local v6, circleId:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 172
    const-string v6, "v.all.circles"

    .line 174
    :cond_65
    invoke-static {p0, p1, v6}, Lcom/google/android/apps/plus/phone/Intents;->getCirclePostsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 176
    .local v7, circlePostsIntent:Landroid/content/Intent;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.google.android.apps.plus.widget.CIRCLE_ACTION"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    const/high16 v0, 0x800

    invoke-static {p0, v9, v7, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 179
    .local v8, clickIntent:Landroid/app/PendingIntent;
    const v0, 0x7f090260

    invoke-virtual {v3, v0, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 181
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, p2, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 182
    return-void
.end method

.method public static showProgressIndicator(Landroid/content/Context;IZ)V
    .registers 13
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "isRefresh"

    .prologue
    const v9, 0x7f090253

    const/16 v8, 0x8

    const/4 v5, 0x0

    .line 64
    const-string v4, "EsWidget"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 65
    const-string v4, "EsWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] showProgressIndicator"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_29
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f0300de

    invoke-direct {v3, v4, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 71
    .local v3, rv:Landroid/widget/RemoteViews;
    const v4, 0x7f09024a

    const v6, 0x7f080096

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 72
    if-eqz p2, :cond_6f

    .line 73
    const v4, 0x7f090250

    invoke-virtual {v3, v4, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 74
    const v4, 0x7f090251

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 78
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/widget/EsWidgetService;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 79
    .local v1, dummy:Landroid/content/Intent;
    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 80
    .local v2, pendingIntent:Landroid/app/PendingIntent;
    invoke-virtual {v3, v9, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 86
    .end local v1           #dummy:Landroid/content/Intent;
    .end local v2           #pendingIntent:Landroid/app/PendingIntent;
    :goto_5e
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 87
    .local v0, appWidgetManager:Landroid/appwidget/AppWidgetManager;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v4, v6, :cond_79

    const/4 v4, 0x1

    :goto_69
    if-eqz v4, :cond_7b

    .line 88
    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 95
    :goto_6e
    return-void

    .line 82
    .end local v0           #appWidgetManager:Landroid/appwidget/AppWidgetManager;
    :cond_6f
    invoke-virtual {v3, v9, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 83
    const v4, 0x7f090254

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_5e

    .restart local v0       #appWidgetManager:Landroid/appwidget/AppWidgetManager;
    :cond_79
    move v4, v5

    .line 87
    goto :goto_69

    .line 90
    :cond_7b
    const v4, 0x7f090249

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 91
    const v4, 0x7f090255

    invoke-virtual {v3, v4, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 92
    const v4, 0x7f090261

    invoke-virtual {v3, v4, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 93
    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_6e
.end method

.method public static showTapToConfigure(Landroid/content/Context;I)V
    .registers 11
    .parameter "context"
    .parameter "appWidgetId"

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 104
    const-string v0, "EsWidget"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 105
    const-string v0, "EsWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] showTapToConfigure"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_27
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0300de

    invoke-direct {v3, v0, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .local v3, rv:Landroid/widget/RemoteViews;
    move-object v0, p0

    move v2, p1

    move-object v4, v1

    .line 111
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    .line 114
    const v0, 0x7f090255

    invoke-virtual {v3, v0, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 115
    const v0, 0x7f090261

    invoke-virtual {v3, v0, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 116
    const v0, 0x7f090249

    invoke-virtual {v3, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 117
    const v0, 0x7f09024a

    const v2, 0x7f08021c

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 119
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v7

    .line 120
    .local v7, streamActivityIntent:Landroid/content/Intent;
    const/high16 v0, 0x800

    invoke-static {p0, v5, v7, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 122
    .local v6, clickIntent:Landroid/app/PendingIntent;
    const v0, 0x7f090260

    invoke-virtual {v3, v0, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 124
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 125
    return-void
.end method

.method public static updateWidget(Landroid/content/Context;ILjava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "currentActivityId"

    .prologue
    .line 193
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 194
    .local v0, service:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 195
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .registers 10
    .parameter "context"
    .parameter "appWidgetIds"

    .prologue
    .line 43
    move-object v1, p2

    .local v1, arr$:[I
    array-length v3, p2

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_3
    if-ge v2, v3, :cond_69

    aget v0, v1, v2

    .line 44
    .local v0, appWidgetId:I
    const-string v4, "EsWidget"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 45
    const-string v4, "EsWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] onDeleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_2a
    const-string v4, "com.google.android.apps.plus.widget.EsWidgetUtils"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "circleId_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "circleName_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x9

    if-ge v5, v6, :cond_65

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 43
    :goto_62
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 47
    :cond_65
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_62

    .line 49
    .end local v0           #appWidgetId:I
    :cond_69
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .registers 11
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetIds"

    .prologue
    .line 33
    move-object v1, p3

    .local v1, arr$:[I
    array-length v3, p3

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_3
    if-ge v2, v3, :cond_31

    aget v0, v1, v2

    .line 34
    .local v0, appWidgetId:I
    const-string v4, "EsWidget"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 35
    const-string v4, "EsWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] onUpdate"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_2a
    const/4 v4, 0x0

    invoke-static {p1, v0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;)V

    .line 33
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 39
    .end local v0           #appWidgetId:I
    :cond_31
    return-void
.end method
