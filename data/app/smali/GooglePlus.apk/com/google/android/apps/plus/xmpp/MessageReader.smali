.class public final Lcom/google/android/apps/plus/xmpp/MessageReader;
.super Ljava/lang/Object;
.source "MessageReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAuthenticationRequired:Z

.field private mBindAvailable:Z

.field private mEventData:Ljava/lang/String;

.field private final mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

.field private final mParser:Lorg/xmlpull/v1/XmlPullParser;

.field private mStringBuilder:Ljava/lang/StringBuilder;

.field private mTlsRequired:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 21
    const-string v0, "MessageReader"

    sput-object v0, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .registers 7
    .parameter "stream"
    .parameter "inDebugMode"

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    :try_start_3
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    .line 48
    if-eqz p2, :cond_1f

    .line 49
    new-instance v1, Lcom/google/android/apps/plus/xmpp/LogInputStream;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/xmpp/LogInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    .line 50
    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    iget-object v2, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 58
    :goto_1e
    return-void

    .line 52
    :cond_1f
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_28
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_28} :catch_29

    goto :goto_1e

    .line 55
    :catch_29
    move-exception v0

    .line 56
    .local v0, exception:Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to create XML parser"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private updateEventData()V
    .registers 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mEventData:Ljava/lang/String;

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    .line 195
    return-void
.end method


# virtual methods
.method public final getEventData()Ljava/lang/String;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mEventData:Ljava/lang/String;

    return-object v0
.end method

.method public final read()Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    .registers 11

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x5

    const/4 v8, 0x1

    .line 70
    :cond_3
    :try_start_3
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    if-eq v6, v8, :cond_170

    .line 71
    const/4 v1, 0x0

    .line 72
    .local v1, event:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 73
    .local v4, parserEvent:I
    packed-switch v4, :pswitch_data_18a

    .line 87
    :cond_15
    :goto_15
    if-eqz v1, :cond_3

    .line 107
    .end local v1           #event:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    .end local v4           #parserEvent:I
    :goto_17
    return-object v1

    .line 75
    .restart local v1       #event:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    .restart local v4       #parserEvent:I
    :pswitch_18
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "push:data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2e

    const-string v7, "jid"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_35

    :cond_2e
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    :cond_35
    const-string v7, "stream"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_90

    const-string v7, "starttls"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_48

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mTlsRequired:Z

    :cond_48
    const-string v7, "stream:features"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_59

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mTlsRequired:Z

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mAuthenticationRequired:Z

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mBindAvailable:Z

    :cond_59
    const-string v7, "bind"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_64

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mBindAvailable:Z

    :cond_64
    const-string v7, "mechanisms"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6f

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mAuthenticationRequired:Z

    :cond_6f
    const-string v7, "proceed"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7a

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->PROCEED_WITH_TLS:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    .line 76
    :goto_79
    goto :goto_15

    .line 75
    :cond_7a
    const-string v7, "success"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_85

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->AUTHENTICATION_SUCCEEDED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_79

    :cond_85
    const-string v7, "failure"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_90

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->AUTHENTICATION_FAILED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_79

    :cond_90
    move-object v1, v5

    goto :goto_79

    .line 78
    :pswitch_92
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "push:data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a7

    invoke-direct {p0}, Lcom/google/android/apps/plus/xmpp/MessageReader;->updateEventData()V

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->DATA_RECEIVED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    .line 79
    :goto_a5
    goto/16 :goto_15

    .line 78
    :cond_a7
    const-string v7, "stream:features"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_107

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_bf

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "Processing stream features"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_bf
    iget-boolean v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mTlsRequired:Z

    if-eqz v6, :cond_d6

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_d3

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "TLS required"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d3
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->TLS_REQUIRED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_a5

    :cond_d6
    iget-boolean v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mAuthenticationRequired:Z

    if-eqz v6, :cond_ed

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_ea

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "Authentication required"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_ea
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->AUTHENTICATION_REQUIRED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_a5

    :cond_ed
    iget-boolean v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mBindAvailable:Z

    if-eqz v6, :cond_104

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_101

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "Stream is ready"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_101
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->STREAM_READY:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_a5

    :cond_104
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->UNEXPECTED_FEATURES:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_a5

    :cond_107
    const-string v7, "jid"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_115

    invoke-direct {p0}, Lcom/google/android/apps/plus/xmpp/MessageReader;->updateEventData()V

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->JID_AVAILABLE:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_a5

    :cond_115
    move-object v1, v5

    goto :goto_a5

    .line 81
    :pswitch_117
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    if-eqz v6, :cond_15

    .line 82
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_126
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_126} :catch_128
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_126} :catch_174

    goto/16 :goto_15

    .line 92
    .end local v1           #event:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    .end local v4           #parserEvent:I
    :catch_128
    move-exception v2

    .line 93
    .local v2, exception:Ljava/lang/Exception;
    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    invoke-static {v5, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_138

    .line 94
    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v6, "XML parser exception"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 96
    :cond_138
    iget-object v5, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    if-eqz v5, :cond_16c

    .line 97
    iget-object v5, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    invoke-static {}, Lcom/google/android/apps/plus/xmpp/LogInputStream;->getLog()Ljava/lang/String;

    move-result-object v3

    .line 98
    .local v3, log:Ljava/lang/String;
    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    invoke-static {v5, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_16c

    .line 99
    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "XML Data ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    .end local v3           #log:Ljava/lang/String;
    :cond_16c
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->END_OF_STREAM:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto/16 :goto_17

    .line 91
    .end local v2           #exception:Ljava/lang/Exception;
    :cond_170
    :try_start_170
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->END_OF_STREAM:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    :try_end_172
    .catch Ljava/lang/Exception; {:try_start_170 .. :try_end_172} :catch_128
    .catch Ljava/lang/Error; {:try_start_170 .. :try_end_172} :catch_174

    goto/16 :goto_17

    .line 103
    :catch_174
    move-exception v0

    .line 104
    .local v0, error:Ljava/lang/Error;
    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_185

    .line 105
    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v6, "Error "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 107
    :cond_185
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->END_OF_STREAM:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto/16 :goto_17

    .line 73
    nop

    :pswitch_data_18a
    .packed-switch 0x2
        :pswitch_18
        :pswitch_92
        :pswitch_117
    .end packed-switch
.end method
