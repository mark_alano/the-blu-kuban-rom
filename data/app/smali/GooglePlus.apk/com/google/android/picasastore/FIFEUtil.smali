.class public final Lcom/google/android/picasastore/FIFEUtil;
.super Ljava/lang/Object;
.source "FIFEUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasastore/FIFEUtil$1;,
        Lcom/google/android/picasastore/FIFEUtil$Splitter;,
        Lcom/google/android/picasastore/FIFEUtil$Joiner;
    }
.end annotation


# static fields
.field private static final FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

.field private static final JOIN_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Joiner;

.field private static final SPLIT_ON_EQUALS:Lcom/google/android/picasastore/FIFEUtil$Splitter;

.field private static final SPLIT_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Splitter;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 21
    const-string v0, "="

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->on(Ljava/lang/String;)Lcom/google/android/picasastore/FIFEUtil$Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->omitEmptyStrings()Lcom/google/android/picasastore/FIFEUtil$Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_EQUALS:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    .line 23
    const-string v0, "/"

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->on(Ljava/lang/String;)Lcom/google/android/picasastore/FIFEUtil$Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->omitEmptyStrings()Lcom/google/android/picasastore/FIFEUtil$Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    .line 25
    const-string v0, "/"

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil$Joiner;->on(Ljava/lang/String;)Lcom/google/android/picasastore/FIFEUtil$Joiner;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasastore/FIFEUtil;->JOIN_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Joiner;

    .line 27
    const-string v0, "^((http(s)?):)?\\/\\/((((lh[3-6]\\.((ggpht)|(googleusercontent)|(google)))|([1-4]\\.bp\\.blogspot)|(bp[0-3]\\.blogger))\\.com)|(www\\.google\\.com\\/visualsearch\\/lh))\\/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasastore/FIFEUtil;->FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static getImageUrlOptions(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "url"

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 134
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v1, :cond_a1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "image"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a1

    add-int/lit8 v0, v3, -0x1

    :goto_2f
    if-lt v0, v6, :cond_7e

    const/4 v3, 0x6

    if-gt v0, v3, :cond_7e

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    invoke-virtual {v0, v3}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_59

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "image"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_59
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_77

    const/4 v3, 0x5

    if-ne v0, v3, :cond_77

    move v3, v1

    :goto_69
    if-ne v0, v6, :cond_79

    move v0, v1

    :goto_6c
    if-nez v3, :cond_7b

    if-nez v0, :cond_7b

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_76
    return-object v0

    :cond_77
    move v3, v2

    goto :goto_69

    :cond_79
    move v0, v2

    goto :goto_6c

    :cond_7b
    const-string v0, ""

    goto :goto_76

    :cond_7e
    if-ne v0, v1, :cond_9e

    sget-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_EQUALS:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v1, :cond_9b

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_76

    :cond_9b
    const-string v0, ""

    goto :goto_76

    :cond_9e
    const-string v0, ""

    goto :goto_76

    :cond_a1
    move v0, v3

    goto :goto_2f
.end method

.method public static isFifeHostedUrl(Ljava/lang/String;)Z
    .registers 3
    .parameter "url"

    .prologue
    .line 173
    if-nez p0, :cond_4

    .line 174
    const/4 v1, 0x0

    .line 178
    :goto_3
    return v1

    .line 177
    :cond_4
    sget-object v1, Lcom/google/android/picasastore/FIFEUtil;->FIFE_HOSTED_IMAGE_URL_RE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 178
    .local v0, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    goto :goto_3
.end method

.method private static newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Ljava/util/ArrayList",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 314
    .local p0, elements:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+TE;>;"
    instance-of v3, p0, Ljava/util/Collection;

    if-eqz v3, :cond_d

    move-object v0, p0

    .line 315
    check-cast v0, Ljava/util/Collection;

    .line 316
    .local v0, collection:Ljava/util/Collection;,"Ljava/util/Collection<+TE;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 323
    .end local v0           #collection:Ljava/util/Collection;,"Ljava/util/Collection<+TE;>;"
    :cond_c
    return-object v2

    .line 318
    :cond_d
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 319
    .local v1, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<+TE;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TE;>;"
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 321
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_16
.end method

.method public static setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 11
    .parameter "options"
    .parameter "url"

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v1, :cond_fa

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "image"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_fa

    add-int/lit8 v0, v4, -0x1

    :goto_2f
    if-lt v0, v8, :cond_bb

    const/4 v4, 0x6

    if-gt v0, v4, :cond_bb

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    invoke-virtual {v0, v4}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_f7

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v6, "image"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f7

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    :goto_5a
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const-string v7, "/"

    invoke-virtual {v4, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_b3

    const/4 v4, 0x5

    if-ne v6, v4, :cond_b3

    move v4, v1

    :goto_6a
    if-ne v6, v8, :cond_b5

    :goto_6c
    if-eqz v4, :cond_75

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_75
    if-eqz v1, :cond_b7

    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7a
    if-eqz v0, :cond_81

    const-string v0, "image"

    invoke-interface {v5, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_81
    if-eqz v7, :cond_88

    const-string v0, ""

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_88
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/picasastore/FIFEUtil;->JOIN_ON_SLASH:Lcom/google/android/picasastore/FIFEUtil$Joiner;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3, v5}, Lcom/google/android/picasastore/FIFEUtil$Joiner;->appendTo(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :goto_b2
    return-object v0

    :cond_b3
    move v4, v2

    goto :goto_6a

    :cond_b5
    move v1, v2

    goto :goto_6c

    :cond_b7
    invoke-interface {v5, v8, p0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_7a

    :cond_bb
    if-ne v0, v1, :cond_f5

    sget-object v0, Lcom/google/android/picasastore/FIFEUtil;->SPLIT_ON_EQUALS:Lcom/google/android/picasastore/FIFEUtil$Splitter;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/FIFEUtil;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_b2

    :cond_f5
    move-object v0, v3

    goto :goto_b2

    :cond_f7
    move v0, v2

    goto/16 :goto_5a

    :cond_fa
    move v0, v4

    goto/16 :goto_2f
.end method
