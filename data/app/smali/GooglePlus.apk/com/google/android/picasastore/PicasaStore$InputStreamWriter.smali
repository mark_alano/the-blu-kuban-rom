.class final Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;
.super Ljava/lang/Object;
.source "PicasaStore.java"

# interfaces
.implements Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/PicasaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InputStreamWriter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private mId:J

.field private mInputStream:Ljava/io/InputStream;

.field final synthetic this$0:Lcom/google/android/picasastore/PicasaStore;


# direct methods
.method public constructor <init>(Lcom/google/android/picasastore/PicasaStore;JLjava/io/InputStream;)V
    .registers 5
    .parameter
    .parameter "id"
    .parameter "inputStream"

    .prologue
    .line 573
    iput-object p1, p0, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;->this$0:Lcom/google/android/picasastore/PicasaStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574
    iput-wide p2, p0, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;->mId:J

    .line 575
    iput-object p4, p0, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;->mInputStream:Ljava/io/InputStream;

    .line 576
    return-void
.end method


# virtual methods
.method public final writeDataToPipe(Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V
    .registers 13
    .parameter "output"
    .parameter "object"

    .prologue
    .line 581
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "PicasaStore.download "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v8}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v5

    .line 583
    .local v5, statsId:I
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;->mInputStream:Ljava/io/InputStream;

    .line 584
    .local v2, is:Ljava/io/InputStream;
    new-instance v3, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v3, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 586
    .local v3, os:Ljava/io/OutputStream;
    const/16 v7, 0x800

    :try_start_26
    new-array v0, v7, [B

    .line 587
    .local v0, buffer:[B
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 588
    .local v4, rc:I
    :goto_2c
    if-lez v4, :cond_37

    .line 589
    const/4 v7, 0x0

    invoke-virtual {v3, v0, v7, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 590
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_35
    .catchall {:try_start_26 .. :try_end_35} :catchall_72
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_35} :catch_41
    .catch Ljava/lang/Throwable; {:try_start_26 .. :try_end_35} :catch_60

    move-result v4

    goto :goto_2c

    .line 601
    :cond_37
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 603
    invoke-static {v5}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    .line 604
    .end local v0           #buffer:[B
    .end local v4           #rc:I
    :goto_40
    return-void

    .line 592
    :catch_41
    move-exception v1

    .line 597
    .local v1, e:Ljava/io/IOException;
    :try_start_42
    const-string v7, "PicasaStore"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "pipe closed early by caller? "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_56
    .catchall {:try_start_42 .. :try_end_56} :catchall_72

    .line 601
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 603
    invoke-static {v5}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    goto :goto_40

    .line 598
    .end local v1           #e:Ljava/io/IOException;
    :catch_60
    move-exception v6

    .line 599
    .local v6, t:Ljava/lang/Throwable;
    :try_start_61
    const-string v7, "PicasaStore"

    const-string v8, "fail to write to pipe"

    invoke-static {v7, v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_68
    .catchall {:try_start_61 .. :try_end_68} :catchall_72

    .line 601
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 603
    invoke-static {v5}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    goto :goto_40

    .line 601
    .end local v6           #t:Ljava/lang/Throwable;
    :catchall_72
    move-exception v7

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 603
    invoke-static {v5}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    throw v7
.end method
