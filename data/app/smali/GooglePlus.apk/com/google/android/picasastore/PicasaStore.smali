.class final Lcom/google/android/picasastore/PicasaStore;
.super Ljava/lang/Object;
.source "PicasaStore.java"

# interfaces
.implements Lcom/google/android/picasastore/UrlDownloader$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;,
        Lcom/google/android/picasastore/PicasaStore$ImagePack;,
        Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;,
        Lcom/google/android/picasastore/PicasaStore$DownloadWriter;,
        Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;,
        Lcom/google/android/picasastore/PicasaStore$DownloadListener;
    }
.end annotation


# instance fields
.field private mBlobCache:Lcom/android/gallery3d/common/BlobCache;

.field private mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
            "<[B>;"
        }
    .end annotation
.end field

.field private mCacheDir:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private final mCreatePipe:Ljava/lang/reflect/Method;

.field private mFileCache:Lcom/android/gallery3d/common/FileCache;

.field private mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
            "<",
            "Lcom/google/android/picasastore/PicasaStore$ImagePack;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeepAlive:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/os/ParcelFileDescriptor;",
            "Ljava/net/Socket;",
            ">;"
        }
    .end annotation
.end field

.field private final mMountListener:Landroid/content/BroadcastReceiver;

.field private final mServerSocket:Ljava/net/ServerSocket;

.field private final mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

.field private final mUrlDownloader:Lcom/google/android/picasastore/UrlDownloader;

.field private mUsingInternalStorage:Z

.field private mVersionInfo:Lcom/google/android/picasastore/VersionInfo;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    .prologue
    const/4 v3, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-boolean v3, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    .line 93
    new-instance v3, Ljava/util/WeakHashMap;

    invoke-direct {v3}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mKeepAlive:Ljava/util/WeakHashMap;

    .line 101
    new-instance v3, Lcom/android/gallery3d/util/ThreadPool;

    invoke-direct {v3}, Lcom/android/gallery3d/util/ThreadPool;-><init>()V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    .line 608
    new-instance v3, Lcom/google/android/picasastore/PicasaStore$2;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/PicasaStore$2;-><init>(Lcom/google/android/picasastore/PicasaStore;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    .line 624
    new-instance v3, Lcom/google/android/picasastore/PicasaStore$3;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/PicasaStore$3;-><init>(Lcom/google/android/picasastore/PicasaStore;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    .line 105
    new-instance v3, Lcom/google/android/picasastore/UrlDownloader;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/UrlDownloader;-><init>(Lcom/google/android/picasastore/UrlDownloader$Controller;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mUrlDownloader:Lcom/google/android/picasastore/UrlDownloader;

    .line 108
    :try_start_2f
    new-instance v3, Ljava/net/ServerSocket;

    invoke-direct {v3}, Ljava/net/ServerSocket;-><init>()V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    .line 109
    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {v3, v4, v5}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;I)V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_3d} :catch_6e

    .line 114
    const/4 v0, 0x0

    .line 116
    .local v0, createPipeMethod:Ljava/lang/reflect/Method;
    :try_start_3e
    const-class v3, Landroid/os/ParcelFileDescriptor;

    const-string v4, "createPipe"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_48
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3e .. :try_end_48} :catch_77

    move-result-object v0

    .line 121
    :goto_49
    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mCreatePipe:Ljava/lang/reflect/Method;

    .line 133
    new-instance v3, Lcom/google/android/picasastore/PicasaStore$1;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/PicasaStore$1;-><init>(Lcom/google/android/picasastore/PicasaStore;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mMountListener:Landroid/content/BroadcastReceiver;

    .line 140
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 141
    .local v2, filter:Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 142
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 143
    const-string v3, "file"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 145
    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaStore;->mMountListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 146
    return-void

    .line 110
    .end local v0           #createPipeMethod:Ljava/lang/reflect/Method;
    .end local v2           #filter:Landroid/content/IntentFilter;
    :catch_6e
    move-exception v1

    .line 111
    .local v1, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "cannot create server socket"

    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .end local v1           #e:Ljava/io/IOException;
    .restart local v0       #createPipeMethod:Ljava/lang/reflect/Method;
    :catch_77
    move-exception v3

    goto :goto_49
.end method

.method static synthetic access$000(Lcom/google/android/picasastore/PicasaStore;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->onMediaMountOrUnmount()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/picasastore/PicasaStore;JI)[B
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 49
    invoke-static {p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->makeKey(JI)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/picasastore/PicasaStore;Ljava/lang/String;)[B
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 49
    invoke-static {p1}, Lcom/google/android/picasastore/PicasaStore;->makeAuxKey(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/picasastore/PicasaStore;[B[B[B)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache([B[B[B)V

    return-void
.end method

.method private checkCacheVersion(Ljava/lang/String;I)Z
    .registers 7
    .parameter "key"
    .parameter "currentVersion"

    .prologue
    const/4 v0, 0x1

    .line 171
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    if-nez v1, :cond_23

    .line 172
    new-instance v1, Lcom/google/android/picasastore/VersionInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getCacheDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/cache_versions.info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/picasastore/VersionInfo;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    .line 174
    :cond_23
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    invoke-virtual {v1, p1}, Lcom/google/android/picasastore/VersionInfo;->getVersion(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v0, :cond_36

    .line 175
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/picasastore/VersionInfo;->setVersion(Ljava/lang/String;I)V

    .line 176
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    invoke-virtual {v1}, Lcom/google/android/picasastore/VersionInfo;->sync()V

    .line 179
    :goto_35
    return v0

    :cond_36
    const/4 v0, 0x0

    goto :goto_35
.end method

.method private createPipe()[Landroid/os/ParcelFileDescriptor;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 826
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mCreatePipe:Ljava/lang/reflect/Method;

    if-nez v1, :cond_9

    .line 827
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->createSocketPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 830
    :goto_8
    return-object v1

    :cond_9
    :try_start_9
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mCreatePipe:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/os/ParcelFileDescriptor;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_15} :catch_16

    goto :goto_8

    .line 831
    :catch_16
    move-exception v0

    .line 832
    .local v0, e:Ljava/lang/Throwable;
    const-string v1, "PicasaStore"

    const-string v2, "fail to create pipe"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 833
    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_25

    check-cast v0, Ljava/io/IOException;

    .end local v0           #e:Ljava/lang/Throwable;
    throw v0

    .line 834
    .restart local v0       #e:Ljava/lang/Throwable;
    :cond_25
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private createSocketPipe()[Landroid/os/ParcelFileDescriptor;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 808
    new-array v1, v8, [Ljava/net/Socket;

    .line 809
    .local v1, socket:[Ljava/net/Socket;
    monitor-enter p0

    .line 810
    const/4 v2, 0x0

    :try_start_7
    new-instance v3, Ljava/net/Socket;

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v5}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v5

    invoke-direct {v3, v4, v5}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    aput-object v3, v1, v2

    .line 812
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    aput-object v3, v1, v2

    .line 813
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_7 .. :try_end_24} :catchall_49

    .line 814
    new-array v0, v8, [Landroid/os/ParcelFileDescriptor;

    aget-object v2, v1, v6

    invoke-static {v2}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    aput-object v2, v0, v6

    aget-object v2, v1, v7

    invoke-static {v2}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    aput-object v2, v0, v7

    .line 820
    .local v0, pipe:[Landroid/os/ParcelFileDescriptor;
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mKeepAlive:Ljava/util/WeakHashMap;

    aget-object v3, v0, v6

    aget-object v4, v1, v6

    invoke-virtual {v2, v3, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mKeepAlive:Ljava/util/WeakHashMap;

    aget-object v3, v0, v7

    aget-object v4, v1, v7

    invoke-virtual {v2, v3, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    return-object v0

    .line 813
    .end local v0           #pipe:[Landroid/os/ParcelFileDescriptor;
    :catchall_49
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized findInCacheOrDownload(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 10
    .parameter "id"
    .parameter "contentUrl"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 379
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getDownloadCache()Lcom/android/gallery3d/common/FileCache;

    move-result-object v0

    .line 381
    .local v0, cache:Lcom/android/gallery3d/common/FileCache;
    if-eqz v0, :cond_1f

    .line 383
    invoke-virtual {v0, p3}, Lcom/android/gallery3d/common/FileCache;->lookup(Ljava/lang/String;)Lcom/android/gallery3d/common/FileCache$CacheEntry;

    move-result-object v1

    .line 384
    .local v1, entry:Lcom/android/gallery3d/common/FileCache$CacheEntry;
    if-eqz v1, :cond_1f

    .line 385
    iget-object v3, v1, Lcom/android/gallery3d/common/FileCache$CacheEntry;->cacheFile:Ljava/io/File;

    const/high16 v4, 0x1000

    invoke-static {v3, v4}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_3e
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_14} :catch_17

    move-result-object v3

    .line 395
    .end local v0           #cache:Lcom/android/gallery3d/common/FileCache;
    .end local v1           #entry:Lcom/android/gallery3d/common/FileCache$CacheEntry;
    :goto_15
    monitor-exit p0

    return-object v3

    .line 389
    :catch_17
    move-exception v2

    .line 390
    .local v2, t:Ljava/lang/Throwable;
    :try_start_18
    const-string v3, "PicasaStore"

    const-string v4, "open image from download cache"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1f
    .catchall {:try_start_18 .. :try_end_1f} :catchall_3e

    .line 395
    .end local v2           #t:Ljava/lang/Throwable;
    :cond_1f
    const/4 v3, 0x0

    :try_start_20
    new-instance v4, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;

    iget-object v5, p0, Lcom/google/android/picasastore/PicasaStore;->mUrlDownloader:Lcom/google/android/picasastore/UrlDownloader;

    invoke-virtual {v5, p3}, Lcom/google/android/picasastore/UrlDownloader;->openInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, p0, p1, p2, v5}, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/io/InputStream;)V

    invoke-direct {p0, v3, v4}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_2e
    .catchall {:try_start_20 .. :try_end_2e} :catchall_3e
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_2e} :catch_30

    move-result-object v3

    goto :goto_15

    .line 397
    :catch_30
    move-exception v2

    .line 398
    .restart local v2       #t:Ljava/lang/Throwable;
    :try_start_31
    const-string v3, "PicasaStore"

    const-string v4, "download fail"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 399
    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_3e
    .catchall {:try_start_31 .. :try_end_3e} :catchall_3e

    .line 379
    .end local v2           #t:Ljava/lang/Throwable;
    :catchall_3e
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized getBlobCache()Lcom/android/gallery3d/common/BlobCache;
    .registers 9

    .prologue
    .line 183
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    if-nez v0, :cond_3d

    .line 184
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getCacheDirectory()Ljava/io/File;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_58

    move-result-object v6

    .line 186
    .local v6, cacheDir:Ljava/io/File;
    :try_start_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/picasa-cache"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, path:Ljava/lang/String;
    const-string v0, "picasa-image-cache-version"

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/picasastore/PicasaStore;->checkCacheVersion(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 188
    invoke-static {v1}, Lcom/android/gallery3d/common/BlobCache;->deleteFiles(Ljava/lang/String;)V

    .line 190
    :cond_2c
    iget-boolean v0, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    if-eqz v0, :cond_41

    .line 191
    new-instance v0, Lcom/android/gallery3d/common/BlobCache;

    const/16 v2, 0x4e2

    const/high16 v3, 0x320

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/BlobCache;-><init>(Ljava/lang/String;IIZI)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;
    :try_end_3d
    .catchall {:try_start_9 .. :try_end_3d} :catchall_58
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_3d} :catch_4f

    .line 202
    .end local v1           #path:Ljava/lang/String;
    .end local v6           #cacheDir:Ljava/io/File;
    :cond_3d
    :goto_3d
    :try_start_3d
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;
    :try_end_3f
    .catchall {:try_start_3d .. :try_end_3f} :catchall_58

    monitor-exit p0

    return-object v0

    .line 195
    .restart local v1       #path:Ljava/lang/String;
    .restart local v6       #cacheDir:Ljava/io/File;
    :cond_41
    :try_start_41
    new-instance v0, Lcom/android/gallery3d/common/BlobCache;

    const/16 v2, 0x1388

    const/high16 v3, 0xc80

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/BlobCache;-><init>(Ljava/lang/String;IIZI)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;
    :try_end_4e
    .catchall {:try_start_41 .. :try_end_4e} :catchall_58
    .catch Ljava/lang/Throwable; {:try_start_41 .. :try_end_4e} :catch_4f

    goto :goto_3d

    .line 198
    .end local v1           #path:Ljava/lang/String;
    :catch_4f
    move-exception v7

    .line 199
    .local v7, t:Ljava/lang/Throwable;
    :try_start_50
    const-string v0, "PicasaStore"

    const-string v2, "fail to create blob cache"

    invoke-static {v0, v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_57
    .catchall {:try_start_50 .. :try_end_57} :catchall_58

    goto :goto_3d

    .line 183
    .end local v6           #cacheDir:Ljava/io/File;
    .end local v7           #t:Ljava/lang/Throwable;
    :catchall_58
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getCacheDirectory()Ljava/io/File;
    .registers 6

    .prologue
    .line 149
    monitor-enter p0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5a

    .line 167
    :goto_7
    monitor-exit p0

    return-object v2

    .line 151
    :cond_9
    :try_start_9
    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    .line 152
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    .line 155
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    if-nez v2, :cond_3f

    .line 156
    const-string v2, "PicasaStore"

    const-string v3, "switch to internal storage for picasastore cache"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    .line 158
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z
    :try_end_28
    .catchall {:try_start_9 .. :try_end_28} :catchall_5a

    .line 160
    :try_start_28
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    const-string v3, ".nomedia"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 161
    .local v1, nomedia:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3a

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_3a
    .catchall {:try_start_28 .. :try_end_3a} :catchall_5a
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_3a} :catch_42

    .line 165
    .end local v1           #nomedia:Ljava/io/File;
    :cond_3a
    :goto_3a
    :try_start_3a
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    :cond_3f
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    goto :goto_7

    .line 162
    :catch_42
    move-exception v0

    .line 163
    .local v0, e:Ljava/io/IOException;
    const-string v2, "PicasaStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fail to create \'.nomedia\' in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_59
    .catchall {:try_start_3a .. :try_end_59} :catchall_5a

    goto :goto_3a

    .line 149
    .end local v0           #e:Ljava/io/IOException;
    :catchall_5a
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized getDownloadCache()Lcom/android/gallery3d/common/FileCache;
    .registers 8

    .prologue
    .line 206
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_4e

    if-nez v0, :cond_32

    .line 208
    :try_start_5
    new-instance v2, Ljava/io/File;

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    const-string v1, "download-cache"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 210
    .local v2, rootDir:Ljava/io/File;
    const-string v0, "picasa-download-cache-version"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/picasastore/PicasaStore;->checkCacheVersion(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 211
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    const-string v1, "picasa-downloads"

    invoke-static {v0, v2, v1}, Lcom/android/gallery3d/common/FileCache;->deleteFiles(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V

    .line 213
    :cond_20
    iget-boolean v0, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    if-nez v0, :cond_36

    .line 214
    new-instance v0, Lcom/android/gallery3d/common/FileCache;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    const-string v3, "picasa-downloads"

    const-wide/32 v4, 0x6400000

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/FileCache;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_32
    .catchall {:try_start_5 .. :try_end_32} :catchall_4e
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_32} :catch_45

    .line 224
    .end local v2           #rootDir:Ljava/io/File;
    :cond_32
    :goto_32
    :try_start_32
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_34
    .catchall {:try_start_32 .. :try_end_34} :catchall_4e

    monitor-exit p0

    return-object v0

    .line 217
    .restart local v2       #rootDir:Ljava/io/File;
    :cond_36
    :try_start_36
    new-instance v0, Lcom/android/gallery3d/common/FileCache;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    const-string v3, "picasa-downloads"

    const-wide/32 v4, 0x1400000

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/FileCache;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_44
    .catchall {:try_start_36 .. :try_end_44} :catchall_4e
    .catch Ljava/lang/Throwable; {:try_start_36 .. :try_end_44} :catch_45

    goto :goto_32

    .line 220
    .end local v2           #rootDir:Ljava/io/File;
    :catch_45
    move-exception v6

    .line 221
    .local v6, t:Ljava/lang/Throwable;
    :try_start_46
    const-string v0, "PicasaStore"

    const-string v1, "fail to create file cache"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4d
    .catchall {:try_start_46 .. :try_end_4d} :catchall_4e

    goto :goto_32

    .line 206
    .end local v6           #t:Ljava/lang/Throwable;
    :catchall_4e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static getItemIdFromUri(Landroid/net/Uri;)J
    .registers 4
    .parameter "uri"

    .prologue
    .line 235
    :try_start_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_e
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_e} :catch_10

    move-result-wide v0

    .line 238
    :goto_f
    return-wide v0

    .line 237
    :catch_10
    move-exception v0

    const-string v0, "PicasaStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot get id from: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    const-wide/16 v0, -0x1

    goto :goto_f
.end method

.method private static isKeyTooLong(I)Z
    .registers 2
    .parameter "totalKeyLength"

    .prologue
    .line 652
    const/16 v0, 0x7fff

    if-le p0, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;
    .registers 8
    .parameter "id"
    .parameter "type"
    .parameter "url"

    .prologue
    .line 679
    invoke-static {p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->makeKey(JI)[B

    move-result-object v1

    .line 680
    .local v1, key:[B
    invoke-static {p4}, Lcom/google/android/picasastore/PicasaStore;->makeAuxKey(Ljava/lang/String;)[B

    move-result-object v0

    .line 681
    .local v0, auxKey:[B
    invoke-direct {p0, v1, v0}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache([B[B)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v2

    return-object v2
.end method

.method private lookupBlobCache([B[B)Lcom/google/android/picasastore/PicasaStore$ImagePack;
    .registers 16
    .parameter "key"
    .parameter "auxKey"

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 660
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getBlobCache()Lcom/android/gallery3d/common/BlobCache;

    move-result-object v0

    .line 661
    .local v0, cache:Lcom/android/gallery3d/common/BlobCache;
    if-nez v0, :cond_9

    .line 674
    .end local v0           #cache:Lcom/android/gallery3d/common/BlobCache;
    :cond_8
    :goto_8
    return-object v6

    .line 662
    .restart local v0       #cache:Lcom/android/gallery3d/common/BlobCache;
    :cond_9
    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    .line 664
    .local v2, hash:J
    monitor-enter v0
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_e} :catch_37

    .line 665
    :try_start_e
    invoke-virtual {v0, v2, v3}, Lcom/android/gallery3d/common/BlobCache;->lookup(J)[B

    move-result-object v1

    .line 666
    .local v1, content:[B
    monitor-exit v0
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_34

    .line 667
    if-eqz v1, :cond_8

    if-nez p2, :cond_4d

    :try_start_17
    array-length v8, v1

    array-length v9, p1

    if-ge v8, v9, :cond_40

    :cond_1b
    :goto_1b
    if-eqz v7, :cond_8

    .line 669
    array-length v7, p1

    aget-byte v7, v1, v7

    and-int/lit16 v7, v7, 0xff

    array-length v8, p1

    add-int/lit8 v8, v8, 0x1

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    add-int v4, v7, v8

    .line 671
    .local v4, offset:I
    new-instance v7, Lcom/google/android/picasastore/PicasaStore$ImagePack;

    invoke-direct {v7, v4, v1}, Lcom/google/android/picasastore/PicasaStore$ImagePack;-><init>(I[B)V

    move-object v6, v7

    goto :goto_8

    .line 666
    .end local v1           #content:[B
    .end local v4           #offset:I
    :catchall_34
    move-exception v7

    monitor-exit v0

    throw v7
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_37} :catch_37

    .line 672
    .end local v0           #cache:Lcom/android/gallery3d/common/BlobCache;
    .end local v2           #hash:J
    :catch_37
    move-exception v5

    .line 673
    .local v5, t:Ljava/lang/Throwable;
    const-string v7, "PicasaStore"

    const-string v8, "cache lookup fail"

    invoke-static {v7, v8, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    .line 667
    .end local v5           #t:Ljava/lang/Throwable;
    .restart local v0       #cache:Lcom/android/gallery3d/common/BlobCache;
    .restart local v1       #content:[B
    .restart local v2       #hash:J
    :cond_40
    :try_start_40
    array-length v9, p1

    move v8, v7

    :goto_42
    if-ge v8, v9, :cond_88

    aget-byte v10, p1, v8

    aget-byte v11, v1, v8

    if-ne v10, v11, :cond_1b

    add-int/lit8 v8, v8, 0x1

    goto :goto_42

    :cond_4d
    array-length v8, p1

    array-length v9, p2

    add-int/2addr v8, v9

    add-int/lit8 v9, v8, 0x2

    array-length v8, v1

    if-lt v8, v9, :cond_1b

    invoke-static {v9}, Lcom/google/android/picasastore/PicasaStore;->isKeyTooLong(I)Z

    move-result v8

    if-nez v8, :cond_1b

    array-length v10, p1

    move v8, v7

    :goto_5d
    if-ge v8, v10, :cond_68

    aget-byte v11, p1, v8

    aget-byte v12, v1, v8

    if-ne v11, v12, :cond_1b

    add-int/lit8 v8, v8, 0x1

    goto :goto_5d

    :cond_68
    array-length v8, p1

    add-int/lit8 v10, v8, 0x1

    aget-byte v8, v1, v8

    int-to-byte v11, v9

    if-ne v8, v11, :cond_1b

    add-int/lit8 v11, v10, 0x1

    aget-byte v8, v1, v10

    ushr-int/lit8 v9, v9, 0x8

    int-to-byte v9, v9

    if-ne v8, v9, :cond_1b

    array-length v9, p2

    move v8, v7

    :goto_7b
    if-ge v8, v9, :cond_88

    aget-byte v10, p2, v8

    add-int v12, v8, v11

    aget-byte v12, v1, v12
    :try_end_83
    .catch Ljava/lang/Throwable; {:try_start_40 .. :try_end_83} :catch_37

    if-ne v10, v12, :cond_1b

    add-int/lit8 v8, v8, 0x1

    goto :goto_7b

    :cond_88
    const/4 v7, 0x1

    goto :goto_1b
.end method

.method private static makeAuxKey(Ljava/lang/String;)[B
    .registers 3
    .parameter "url"

    .prologue
    .line 766
    if-nez p0, :cond_4

    .line 767
    const/4 v1, 0x0

    .line 777
    :goto_3
    return-object v1

    .line 770
    :cond_4
    const-string v1, "https://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 771
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 777
    .local v0, auxUrl:Ljava/lang/String;
    :goto_12
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    goto :goto_3

    .line 772
    .end local v0           #auxUrl:Ljava/lang/String;
    :cond_17
    const-string v1, "http://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 773
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #auxUrl:Ljava/lang/String;
    goto :goto_12

    .line 775
    .end local v0           #auxUrl:Ljava/lang/String;
    :cond_25
    move-object v0, p0

    .restart local v0       #auxUrl:Ljava/lang/String;
    goto :goto_12
.end method

.method private static makeKey(JI)[B
    .registers 8
    .parameter "id"
    .parameter "type"

    .prologue
    const/16 v4, 0x8

    .line 756
    const/16 v2, 0x9

    new-array v0, v2, [B

    .line 757
    .local v0, array:[B
    const/4 v1, 0x0

    .local v1, i:I
    :goto_7
    if-ge v1, v4, :cond_14

    .line 758
    mul-int/lit8 v2, v1, 0x8

    ushr-long v2, p0, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 757
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 760
    :cond_14
    int-to-byte v2, p2

    aput-byte v2, v0, v4

    .line 761
    return-object v0
.end method

.method private declared-synchronized onMediaMountOrUnmount()V
    .registers 2

    .prologue
    .line 839
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    .line 840
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    if-eqz v0, :cond_10

    .line 841
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 842
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    .line 844
    :cond_10
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;

    if-eqz v0, :cond_1c

    .line 845
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 846
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;

    .line 848
    :cond_1c
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;
    :try_end_1f
    .catchall {:try_start_2 .. :try_end_1f} :catchall_21

    .line 849
    monitor-exit p0

    return-void

    .line 839
    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private openFullImage(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 8
    .parameter "id"
    .parameter "contentUrl"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 363
    :try_start_0
    const-string v2, ".full"

    invoke-static {p1, p2, v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 364
    .local v0, file:Ljava/io/File;
    if-eqz v0, :cond_17

    .line 365
    const/high16 v2, 0x1000

    invoke-static {v0, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_d} :catch_f

    move-result-object v2

    .line 373
    .end local v0           #file:Ljava/io/File;
    :goto_e
    return-object v2

    .line 367
    :catch_f
    move-exception v1

    .line 368
    .local v1, t:Ljava/lang/Throwable;
    const-string v2, "PicasaStore"

    const-string v3, "openFullImage from cache"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 372
    .end local v1           #t:Ljava/lang/Throwable;
    :cond_17
    if-nez p3, :cond_1f

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2

    .line 373
    :cond_1f
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->findInCacheOrDownload(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    goto :goto_e
.end method

.method private openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
            "<TT;>;)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 784
    .local p1, args:Ljava/lang/Object;,"TT;"
    .local p2, func:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;,"Lcom/google/android/picasastore/PicasaStore$PipeDataWriter<TT;>;"
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 785
    .local v0, pipe:[Landroid/os/ParcelFileDescriptor;
    new-instance v1, Lcom/google/android/picasastore/PicasaStore$4;

    invoke-direct {v1, p0, p2, v0, p1}, Lcom/google/android/picasastore/PicasaStore$4;-><init>(Lcom/google/android/picasastore/PicasaStore;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;[Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V

    .line 796
    .local v1, task:Lcom/android/gallery3d/util/ThreadPool$Job;,"Lcom/android/gallery3d/util/ThreadPool$Job<Ljava/lang/Void;>;"
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    invoke-virtual {v2, v1}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;)Lcom/android/gallery3d/util/Future;

    .line 797
    const/4 v2, 0x0

    aget-object v2, v0, v2
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_11} :catch_12

    return-object v2

    .line 799
    .end local v0           #pipe:[Landroid/os/ParcelFileDescriptor;
    .end local v1           #task:Lcom/android/gallery3d/util/ThreadPool$Job;,"Lcom/android/gallery3d/util/ThreadPool$Job<Ljava/lang/Void;>;"
    :catch_12
    move-exception v2

    new-instance v2, Ljava/io/FileNotFoundException;

    const-string v3, "failure making pipe"

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private openScreenNail(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 14
    .parameter "id"
    .parameter "downloadUrl"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 407
    :try_start_0
    const-string v1, ".screen"

    invoke-static {p1, p2, v1}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v7

    .line 408
    .local v7, file:Ljava/io/File;
    if-eqz v7, :cond_17

    .line 409
    const/high16 v1, 0x1000

    invoke-static {v7, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_d} :catch_f

    move-result-object v1

    .line 421
    .end local v7           #file:Ljava/io/File;
    :goto_e
    return-object v1

    .line 411
    :catch_f
    move-exception v8

    .line 412
    .local v8, t:Ljava/lang/Throwable;
    const-string v1, "PicasaStore"

    const-string v2, "openScreenNail from cache"

    invoke-static {v1, v2, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 416
    .end local v8           #t:Ljava/lang/Throwable;
    :cond_17
    if-nez p3, :cond_1f

    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    throw v1

    .line 419
    :cond_1f
    :try_start_1f
    new-instance v0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;

    const/4 v4, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;-><init>(Lcom/google/android/picasastore/PicasaStore;JILjava/lang/String;)V

    .line 421
    .local v0, register:Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;
    const/4 v9, 0x0

    new-instance v1, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;

    move-object v2, p0

    move-wide v3, p1

    move-object v5, p3

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/lang/String;Lcom/google/android/picasastore/PicasaStore$DownloadListener;)V

    invoke-direct {p0, v9, v1}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_1f .. :try_end_35} :catch_37

    move-result-object v1

    goto :goto_e

    .line 423
    .end local v0           #register:Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;
    :catch_37
    move-exception v8

    .line 424
    .restart local v8       #t:Ljava/lang/Throwable;
    const-string v1, "PicasaStore"

    const-string v2, "download fail"

    invoke-static {v1, v2, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 425
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    throw v1
.end method

.method private openThumbNail(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 24
    .parameter "id"
    .parameter "downloadUrl"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 433
    :try_start_0
    const-string v2, ".screen"

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v16

    .line 434
    .local v16, file:Ljava/io/File;
    if-eqz v16, :cond_9e

    .line 435
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    .line 436
    .local v17, filepath:Ljava/lang/String;
    new-instance v18, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 437
    .local v18, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    move-object/from16 v0, v18

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 438
    invoke-static/range {v17 .. v18}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 439
    const/high16 v2, 0x4348

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f80

    div-float v2, v3, v2

    invoke-static {v2}, Landroid/util/FloatMath;->floor(F)F

    move-result v2

    float-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_69

    const/4 v2, 0x1

    :goto_38
    move-object/from16 v0, v18

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 442
    const/4 v2, 0x0

    move-object/from16 v0, v18

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 443
    invoke-static/range {v17 .. v18}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 445
    .local v15, bitmap:Landroid/graphics/Bitmap;
    if-eqz v15, :cond_77

    .line 446
    const/16 v2, 0xc8

    const/4 v3, 0x1

    invoke-static {v15, v2, v3}, Lcom/android/gallery3d/common/BitmapUtils;->resizeAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 447
    const/16 v2, 0x5f

    invoke-static {v15, v2}, Lcom/android/gallery3d/common/BitmapUtils;->compressToBytes(Landroid/graphics/Bitmap;I)[B

    move-result-object v7

    .line 449
    .local v7, content:[B
    const/4 v5, 0x1

    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move-object/from16 v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache(JILjava/lang/String;[B)V

    .line 450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasastore/PicasaStore;->mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 464
    .end local v7           #content:[B
    .end local v15           #bitmap:Landroid/graphics/Bitmap;
    .end local v16           #file:Ljava/io/File;
    .end local v17           #filepath:Ljava/lang/String;
    .end local v18           #options:Landroid/graphics/BitmapFactory$Options;
    :goto_68
    return-object v2

    .line 439
    .restart local v16       #file:Ljava/io/File;
    .restart local v17       #filepath:Ljava/lang/String;
    .restart local v18       #options:Landroid/graphics/BitmapFactory$Options;
    :cond_69
    const/16 v3, 0x8

    if-gt v2, v3, :cond_72

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->prevPowerOf2(I)I

    move-result v2

    goto :goto_38

    :cond_72
    div-int/lit8 v2, v2, 0x8

    mul-int/lit8 v2, v2, 0x8

    goto :goto_38

    .line 452
    .restart local v15       #bitmap:Landroid/graphics/Bitmap;
    :cond_77
    const-string v2, "PicasaStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid prefetch file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", length: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->delete()Z
    :try_end_9e
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_9e} :catch_a6

    .line 460
    .end local v15           #bitmap:Landroid/graphics/Bitmap;
    .end local v16           #file:Ljava/io/File;
    .end local v17           #filepath:Ljava/lang/String;
    .end local v18           #options:Landroid/graphics/BitmapFactory$Options;
    :cond_9e
    :goto_9e
    if-nez p3, :cond_b1

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2

    .line 455
    :catch_a6
    move-exception v19

    .line 456
    .local v19, t:Ljava/lang/Throwable;
    const-string v2, "PicasaStore"

    const-string v3, "openThumbNail from screennail"

    move-object/from16 v0, v19

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9e

    .line 462
    .end local v19           #t:Ljava/lang/Throwable;
    :cond_b1
    :try_start_b1
    new-instance v8, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;

    const/4 v12, 0x1

    move-object/from16 v9, p0

    move-wide/from16 v10, p1

    move-object/from16 v13, p3

    invoke-direct/range {v8 .. v13}, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;-><init>(Lcom/google/android/picasastore/PicasaStore;JILjava/lang/String;)V

    .line 464
    .local v8, register:Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;
    const/4 v2, 0x0

    new-instance v9, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;

    move-object/from16 v10, p0

    move-wide/from16 v11, p1

    move-object/from16 v13, p3

    move-object v14, v8

    invoke-direct/range {v9 .. v14}, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/lang/String;Lcom/google/android/picasastore/PicasaStore$DownloadListener;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_cf
    .catch Ljava/lang/Throwable; {:try_start_b1 .. :try_end_cf} :catch_d1

    move-result-object v2

    goto :goto_68

    .line 466
    .end local v8           #register:Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;
    :catch_d1
    move-exception v19

    .line 467
    .restart local v19       #t:Ljava/lang/Throwable;
    const-string v2, "PicasaStore"

    const-string v3, "download fail"

    move-object/from16 v0, v19

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 468
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2
.end method

.method private putBlobCache(JILjava/lang/String;[B)V
    .registers 8
    .parameter "id"
    .parameter "type"
    .parameter "url"
    .parameter "content"

    .prologue
    .line 713
    invoke-static {p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->makeKey(JI)[B

    move-result-object v1

    .line 714
    .local v1, key:[B
    invoke-static {p4}, Lcom/google/android/picasastore/PicasaStore;->makeAuxKey(Ljava/lang/String;)[B

    move-result-object v0

    .line 715
    .local v0, auxKey:[B
    invoke-direct {p0, v1, v0, p5}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache([B[B[B)V

    .line 716
    return-void
.end method

.method private putBlobCache([B[B[B)V
    .registers 14
    .parameter "key"
    .parameter "auxKey"
    .parameter "content"

    .prologue
    const/4 v0, 0x0

    .line 685
    if-nez p2, :cond_e

    .line 686
    .local v0, auxKeyLength:I
    :goto_3
    array-length v7, p1

    add-int/2addr v7, v0

    add-int/lit8 v6, v7, 0x2

    .line 687
    .local v6, totalKeyLength:I
    invoke-static {v6}, Lcom/google/android/picasastore/PicasaStore;->isKeyTooLong(I)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 710
    :cond_d
    :goto_d
    return-void

    .line 685
    .end local v0           #auxKeyLength:I
    .end local v6           #totalKeyLength:I
    :cond_e
    array-length v0, p2

    goto :goto_3

    .line 689
    .restart local v0       #auxKeyLength:I
    .restart local v6       #totalKeyLength:I
    :cond_10
    :try_start_10
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getBlobCache()Lcom/android/gallery3d/common/BlobCache;

    move-result-object v1

    .line 690
    .local v1, cache:Lcom/android/gallery3d/common/BlobCache;
    if-eqz v1, :cond_d

    .line 691
    array-length v7, p3

    add-int/2addr v7, v6

    new-array v4, v7, [B

    .line 695
    .local v4, output:[B
    const/4 v7, 0x0

    const/4 v8, 0x0

    array-length v9, p1

    invoke-static {p1, v7, v4, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 696
    array-length v7, p1

    int-to-byte v8, v6

    aput-byte v8, v4, v7

    .line 697
    array-length v7, p1

    add-int/lit8 v7, v7, 0x1

    ushr-int/lit8 v8, v6, 0x8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 698
    if-lez v0, :cond_35

    .line 699
    const/4 v7, 0x0

    array-length v8, p1

    add-int/lit8 v8, v8, 0x2

    invoke-static {p2, v7, v4, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 701
    :cond_35
    const/4 v7, 0x0

    array-length v8, p3

    invoke-static {p3, v7, v4, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 703
    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    .line 704
    .local v2, hash:J
    monitor-enter v1
    :try_end_3f
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_3f} :catch_47

    .line 705
    :try_start_3f
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/gallery3d/common/BlobCache;->insert(J[B)V

    .line 706
    monitor-exit v1
    :try_end_43
    .catchall {:try_start_3f .. :try_end_43} :catchall_44

    goto :goto_d

    :catchall_44
    move-exception v7

    :try_start_45
    monitor-exit v1

    throw v7
    :try_end_47
    .catch Ljava/lang/Throwable; {:try_start_45 .. :try_end_47} :catch_47

    .line 709
    .end local v1           #cache:Lcom/android/gallery3d/common/BlobCache;
    .end local v2           #hash:J
    .end local v4           #output:[B
    :catch_47
    move-exception v5

    .line 708
    .local v5, t:Ljava/lang/Throwable;
    const-string v7, "PicasaStore"

    const-string v8, "cache insert fail"

    invoke-static {v7, v8, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_d
.end method


# virtual methods
.method public final createTempFile()Ljava/io/File;
    .registers 4

    .prologue
    .line 854
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getDownloadCache()Lcom/android/gallery3d/common/FileCache;

    move-result-object v0

    .line 855
    .local v0, cache:Lcom/android/gallery3d/common/FileCache;
    if-nez v0, :cond_8

    const/4 v2, 0x0

    :goto_7
    return-object v2

    :cond_8
    invoke-virtual {v0}, Lcom/android/gallery3d/common/FileCache;->createFile()Ljava/io/File;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_b} :catch_d

    move-result-object v2

    goto :goto_7

    .line 856
    .end local v0           #cache:Lcom/android/gallery3d/common/FileCache;
    :catch_d
    move-exception v1

    .line 857
    .local v1, e:Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public final declared-synchronized onDownloadComplete(Ljava/lang/String;Ljava/io/File;)V
    .registers 5
    .parameter "downloadUrl"
    .parameter "file"

    .prologue
    .line 863
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getDownloadCache()Lcom/android/gallery3d/common/FileCache;

    move-result-object v0

    .line 864
    .local v0, cache:Lcom/android/gallery3d/common/FileCache;
    if-eqz v0, :cond_c

    .line 865
    invoke-virtual {v0, p1, p2}, Lcom/android/gallery3d/common/FileCache;->store(Ljava/lang/String;Ljava/io/File;)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_10

    .line 869
    :goto_a
    monitor-exit p0

    return-void

    .line 867
    :cond_c
    :try_start_c
    invoke-virtual {p2}, Ljava/io/File;->delete()Z
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_10

    goto :goto_a

    .line 863
    .end local v0           #cache:Lcom/android/gallery3d/common/FileCache;
    :catchall_10
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final openAlbumCover(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 26
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 251
    const-string v2, "w"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 252
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "invalid mode: "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 255
    :cond_21
    invoke-static/range {p1 .. p1}, Lcom/google/android/picasastore/PicasaStore;->getItemIdFromUri(Landroid/net/Uri;)J

    move-result-wide v3

    .line 256
    .local v3, id:J
    const-string v2, "content_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 259
    .local v6, thumbnailUrl:Ljava/lang/String;
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2, v6}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v20

    .line 260
    .local v20, pack:Lcom/google/android/picasastore/PicasaStore$ImagePack;
    if-eqz v20, :cond_43

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 296
    :goto_42
    return-object v2

    .line 263
    :cond_43
    if-nez v6, :cond_53

    .line 264
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 268
    :cond_53
    const-string v2, ".thumb"

    invoke-static {v3, v4, v6, v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_9d

    .line 269
    .local v17, file:Ljava/io/File;
    :goto_5f
    if-eqz v17, :cond_eb

    .line 270
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/32 v11, 0x80000

    cmp-long v2, v9, v11

    if-gez v2, :cond_e1

    .line 271
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v9

    long-to-int v2, v9

    new-array v7, v2, [B

    .line 272
    .local v7, content:[B
    new-instance v18, Ljava/io/FileInputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 274
    .local v18, fis:Ljava/io/FileInputStream;
    const/16 v19, 0x0

    .line 275
    .local v19, offset:I
    const/4 v2, 0x0

    :try_start_7f
    array-length v5, v7

    add-int/lit8 v5, v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v2, v5}, Ljava/io/FileInputStream;->read([BII)I

    move-result v21

    .line 276
    .local v21, rc:I
    :goto_88
    if-ltz v21, :cond_a0

    array-length v2, v7

    move/from16 v0, v19

    if-ge v0, v2, :cond_a0

    .line 277
    add-int v19, v19, v21

    .line 278
    array-length v2, v7

    sub-int v2, v2, v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1, v2}, Ljava/io/FileInputStream;->read([BII)I

    move-result v21

    goto :goto_88

    .line 268
    .end local v7           #content:[B
    .end local v17           #file:Ljava/io/File;
    .end local v18           #fis:Ljava/io/FileInputStream;
    .end local v19           #offset:I
    .end local v21           #rc:I
    :cond_9d
    const/16 v17, 0x0

    goto :goto_5f

    .line 280
    .restart local v7       #content:[B
    .restart local v17       #file:Ljava/io/File;
    .restart local v18       #fis:Ljava/io/FileInputStream;
    .restart local v19       #offset:I
    .restart local v21       #rc:I
    :cond_a0
    const/4 v5, 0x2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache(JILjava/lang/String;[B)V

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasastore/PicasaStore;->mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_af
    .catchall {:try_start_7f .. :try_end_af} :catchall_dc
    .catch Ljava/io/IOException; {:try_start_7f .. :try_end_af} :catch_b4

    move-result-object v2

    .line 285
    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_42

    .line 282
    .end local v21           #rc:I
    :catch_b4
    move-exception v16

    .line 283
    .local v16, e:Ljava/io/IOException;
    :try_start_b5
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ":"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_dc
    .catchall {:try_start_b5 .. :try_end_dc} :catchall_dc

    .line 285
    .end local v16           #e:Ljava/io/IOException;
    :catchall_dc
    move-exception v2

    invoke-static/range {v18 .. v18}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v2

    .line 288
    .end local v7           #content:[B
    .end local v18           #fis:Ljava/io/FileInputStream;
    .end local v19           #offset:I
    :cond_e1
    const/high16 v2, 0x1000

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    goto/16 :goto_42

    .line 293
    :cond_eb
    const/16 v2, 0xc8

    const/4 v5, 0x1

    :try_start_ee
    invoke-static {v6, v2, v5}, Lcom/google/android/picasastore/PicasaStoreFacade;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v15

    .line 295
    .local v15, downloadUrl:Ljava/lang/String;
    new-instance v8, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;

    const/4 v12, 0x2

    move-object/from16 v9, p0

    move-wide v10, v3

    move-object v13, v6

    invoke-direct/range {v8 .. v13}, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;-><init>(Lcom/google/android/picasastore/PicasaStore;JILjava/lang/String;)V

    .line 296
    .local v8, register:Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;
    const/4 v2, 0x0

    new-instance v9, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;

    move-object/from16 v10, p0

    move-wide v11, v3

    move-object v13, v15

    move-object v14, v8

    invoke-direct/range {v9 .. v14}, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/lang/String;Lcom/google/android/picasastore/PicasaStore$DownloadListener;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    :try_end_10c
    .catch Ljava/lang/Throwable; {:try_start_ee .. :try_end_10c} :catch_10f

    move-result-object v2

    goto/16 :goto_42

    .line 298
    .end local v8           #register:Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;
    .end local v15           #downloadUrl:Ljava/lang/String;
    :catch_10f
    move-exception v22

    .line 299
    .local v22, t:Ljava/lang/Throwable;
    const-string v2, "PicasaStore"

    const-string v5, "download fail"

    move-object/from16 v0, v22

    invoke-static {v2, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 300
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2
.end method

.method public final openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 15
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x280

    const/16 v10, 0xc8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 306
    const-string v6, "w"

    invoke-virtual {p2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_23

    .line 307
    new-instance v6, Ljava/io/FileNotFoundException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "invalid mode: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 310
    :cond_23
    const-string v6, "type"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 311
    .local v5, type:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/picasastore/PicasaStore;->getItemIdFromUri(Landroid/net/Uri;)J

    move-result-wide v2

    .line 314
    .local v2, id:J
    const-string v6, "content_url"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, contentUrl:Ljava/lang/String;
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-eqz v6, :cond_7a

    .line 319
    const-string v6, "screennail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_57

    .line 320
    invoke-static {v0, v11, v8}, Lcom/google/android/picasastore/PicasaStoreFacade;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    .line 322
    .local v1, downloadUrl:Ljava/lang/String;
    invoke-direct {p0, v2, v3, v8, v1}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v4

    .line 323
    .local v4, pack:Lcom/google/android/picasastore/PicasaStore$ImagePack;
    if-eqz v4, :cond_52

    .line 324
    iget-object v6, p0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    invoke-direct {p0, v4, v6}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    .line 341
    .end local v0           #contentUrl:Ljava/lang/String;
    .end local v1           #downloadUrl:Ljava/lang/String;
    .end local v4           #pack:Lcom/google/android/picasastore/PicasaStore$ImagePack;
    :goto_51
    return-object v6

    .line 326
    .restart local v0       #contentUrl:Ljava/lang/String;
    .restart local v1       #downloadUrl:Ljava/lang/String;
    .restart local v4       #pack:Lcom/google/android/picasastore/PicasaStore$ImagePack;
    :cond_52
    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/picasastore/PicasaStore;->openScreenNail(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_51

    .line 327
    .end local v1           #downloadUrl:Ljava/lang/String;
    .end local v4           #pack:Lcom/google/android/picasastore/PicasaStore$ImagePack;
    :cond_57
    const-string v6, "thumbnail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_75

    .line 328
    invoke-static {v0, v10, v9}, Lcom/google/android/picasastore/PicasaStoreFacade;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    .line 330
    .restart local v1       #downloadUrl:Ljava/lang/String;
    invoke-direct {p0, v2, v3, v9, v1}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v4

    .line 331
    .restart local v4       #pack:Lcom/google/android/picasastore/PicasaStore$ImagePack;
    if-eqz v4, :cond_70

    .line 332
    iget-object v6, p0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    invoke-direct {p0, v4, v6}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_51

    .line 334
    :cond_70
    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/picasastore/PicasaStore;->openThumbNail(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_51

    .line 336
    .end local v1           #downloadUrl:Ljava/lang/String;
    .end local v4           #pack:Lcom/google/android/picasastore/PicasaStore$ImagePack;
    :cond_75
    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/picasastore/PicasaStore;->openFullImage(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_51

    .line 338
    :cond_7a
    if-eqz v0, :cond_9c

    .line 341
    const-string v6, "thumbnail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8f

    invoke-static {v0, v10, v9}, Lcom/google/android/picasastore/PicasaStoreFacade;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .end local v0           #contentUrl:Ljava/lang/String;
    :cond_88
    :goto_88
    const-wide/16 v6, -0x1

    invoke-direct {p0, v6, v7, v0}, Lcom/google/android/picasastore/PicasaStore;->findInCacheOrDownload(JLjava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_51

    .restart local v0       #contentUrl:Ljava/lang/String;
    :cond_8f
    const-string v6, "screennail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_88

    invoke-static {v0, v11, v8}, Lcom/google/android/picasastore/PicasaStoreFacade;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_88

    .line 343
    :cond_9c
    new-instance v6, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v6
.end method
