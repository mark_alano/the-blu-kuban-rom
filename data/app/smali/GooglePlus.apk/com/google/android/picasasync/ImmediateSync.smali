.class final Lcom/google/android/picasasync/ImmediateSync;
.super Ljava/lang/Object;
.source "ImmediateSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/ImmediateSync$Task;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/picasasync/ImmediateSync;


# instance fields
.field private final mCompleteTaskMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/picasasync/ImmediateSync$Task;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mPendingTaskMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/picasasync/ImmediateSync$Task;",
            ">;"
        }
    .end annotation
.end field

.field private final mThreadPool:Lcom/android/gallery3d/util/ThreadPool;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/picasasync/ImmediateSync;->mCompleteTaskMap:Ljava/util/HashMap;

    .line 118
    new-instance v0, Lcom/android/gallery3d/util/ThreadPool;

    invoke-direct {v0}, Lcom/android/gallery3d/util/ThreadPool;-><init>()V

    iput-object v0, p0, Lcom/google/android/picasasync/ImmediateSync;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    .line 121
    iput-object p1, p0, Lcom/google/android/picasasync/ImmediateSync;->mContext:Landroid/content/Context;

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/picasasync/ImmediateSync;Lcom/google/android/picasasync/ImmediateSync$Task;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/picasasync/ImmediateSync;->completeTask(Lcom/google/android/picasasync/ImmediateSync$Task;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/picasasync/ImmediateSync;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/picasasync/ImmediateSync;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private declared-synchronized completeTask(Lcom/google/android/picasasync/ImmediateSync$Task;)V
    .registers 7
    .parameter "task"

    .prologue
    .line 322
    monitor-enter p0

    :try_start_1
    iget-object v0, p1, Lcom/google/android/picasasync/ImmediateSync$Task;->taskId:Ljava/lang/String;

    .line 323
    .local v0, taskId:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p1, :cond_21

    .line 324
    const-string v2, "ImmediateSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "new task added, ignored old:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_6c

    .line 337
    :cond_1f
    :goto_1f
    monitor-exit p0

    return-void

    .line 327
    :cond_21
    :try_start_21
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync;->mCompleteTaskMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/picasasync/PicasaFacade;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaFacade;->getSyncRequestUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 332
    .local v1, uri:Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 334
    iget v2, p1, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    if-eqz v2, :cond_1f

    .line 335
    const-string v2, "ImmediateSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sync "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " incomplete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6b
    .catchall {:try_start_21 .. :try_end_6b} :catchall_6c

    goto :goto_1f

    .line 322
    .end local v0           #taskId:Ljava/lang/String;
    .end local v1           #uri:Landroid/net/Uri;
    :catchall_6c
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/picasasync/ImmediateSync;
    .registers 3
    .parameter "context"

    .prologue
    .line 104
    const-class v1, Lcom/google/android/picasasync/ImmediateSync;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/picasasync/ImmediateSync;->sInstance:Lcom/google/android/picasasync/ImmediateSync;

    if-nez v0, :cond_e

    .line 105
    new-instance v0, Lcom/google/android/picasasync/ImmediateSync;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/ImmediateSync;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasasync/ImmediateSync;->sInstance:Lcom/google/android/picasasync/ImmediateSync;

    .line 107
    :cond_e
    sget-object v0, Lcom/google/android/picasasync/ImmediateSync;->sInstance:Lcom/google/android/picasasync/ImmediateSync;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 104
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private varargs requestSyncAlbumList(Ljava/lang/String;[Ljava/lang/String;)V
    .registers 5
    .parameter "taskId"
    .parameter "accountArgs"

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/picasasync/ImmediateSync$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/picasasync/ImmediateSync$1;-><init>(Lcom/google/android/picasasync/ImmediateSync;Ljava/lang/String;[Ljava/lang/String;)V

    .line 240
    .local v0, task:Lcom/google/android/picasasync/ImmediateSync$Task;
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;)Lcom/android/gallery3d/util/Future;

    .line 242
    return-void
.end method


# virtual methods
.method public final declared-synchronized cancelTask(Ljava/lang/String;)Z
    .registers 7
    .parameter "taskId"

    .prologue
    const/4 v1, 0x1

    .line 341
    monitor-enter p0

    :try_start_2
    const-string v2, "ImmediateSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cancel sync "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/ImmediateSync$Task;

    .line 343
    .local v0, task:Lcom/google/android/picasasync/ImmediateSync$Task;
    if-eqz v0, :cond_3a

    iget v2, v0, Lcom/google/android/picasasync/ImmediateSync$Task;->refCount:I

    if-lez v2, :cond_3a

    .line 344
    iget v2, v0, Lcom/google/android/picasasync/ImmediateSync$Task;->refCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/google/android/picasasync/ImmediateSync$Task;->refCount:I

    if-nez v2, :cond_38

    .line 345
    const/4 v2, 0x1

    iput v2, v0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I

    iget-object v2, v0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    if-eqz v2, :cond_38

    iget-object v2, v0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->stopSync()V
    :try_end_38
    .catchall {:try_start_2 .. :try_end_38} :catchall_3c

    .line 349
    :cond_38
    :goto_38
    monitor-exit p0

    return v1

    :cond_3a
    const/4 v1, 0x0

    goto :goto_38

    .line 341
    .end local v0           #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    :catchall_3c
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized getResult(Ljava/lang/String;)I
    .registers 4
    .parameter "taskId"

    .prologue
    .line 357
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync;->mCompleteTaskMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/ImmediateSync$Task;

    .line 358
    .local v0, task:Lcom/google/android/picasasync/ImmediateSync$Task;
    if-nez v0, :cond_13

    .line 359
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0           #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    check-cast v0, Lcom/google/android/picasasync/ImmediateSync$Task;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_1b

    .line 361
    .restart local v0       #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    :cond_13
    if-nez v0, :cond_18

    const/4 v1, 0x3

    :goto_16
    monitor-exit p0

    return v1

    :cond_18
    :try_start_18
    iget v1, v0, Lcom/google/android/picasasync/ImmediateSync$Task;->syncResultCode:I
    :try_end_1a
    .catchall {:try_start_18 .. :try_end_1a} :catchall_1b

    goto :goto_16

    .line 357
    .end local v0           #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    :catchall_1b
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized requestSyncAlbum(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "albumId"

    .prologue
    .line 248
    monitor-enter p0

    :try_start_1
    iget-object v5, p0, Lcom/google/android/picasasync/ImmediateSync;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v2

    .line 250
    .local v2, dbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;
    invoke-virtual {v2, p1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getAlbumEntry$2582d372(Ljava/lang/String;)Lcom/google/android/picasasync/AlbumEntry;

    move-result-object v1

    .line 251
    .local v1, album:Lcom/google/android/picasasync/AlbumEntry;
    if-nez v1, :cond_18

    .line 252
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "album does not exist"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_15

    .line 248
    .end local v1           #album:Lcom/google/android/picasasync/AlbumEntry;
    .end local v2           #dbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;
    :catchall_15
    move-exception v5

    monitor-exit p0

    throw v5

    .line 255
    .restart local v1       #album:Lcom/google/android/picasasync/AlbumEntry;
    .restart local v2       #dbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;
    :cond_18
    :try_start_18
    iget-wide v5, v1, Lcom/google/android/picasasync/AlbumEntry;->userId:J

    invoke-virtual {v2, v5, v6}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getUserAccount(J)Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, account:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 259
    .local v4, taskId:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/picasasync/ImmediateSync$Task;

    .line 260
    .local v3, task:Lcom/google/android/picasasync/ImmediateSync$Task;
    if-eqz v3, :cond_5f

    invoke-virtual {v3}, Lcom/google/android/picasasync/ImmediateSync$Task;->addRequester()Z

    move-result v5

    if-eqz v5, :cond_5f

    .line 261
    const-string v5, "ImmediateSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "task already exists:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5d
    .catchall {:try_start_18 .. :try_end_5d} :catchall_15

    .line 309
    :goto_5d
    monitor-exit p0

    return-object v4

    .line 264
    :cond_5f
    :try_start_5f
    iget-object v5, p0, Lcom/google/android/picasasync/ImmediateSync;->mCompleteTaskMap:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    new-instance v3, Lcom/google/android/picasasync/ImmediateSync$2;

    .end local v3           #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    invoke-direct {v3, p0, v4, v0, v1}, Lcom/google/android/picasasync/ImmediateSync$2;-><init>(Lcom/google/android/picasasync/ImmediateSync;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/picasasync/AlbumEntry;)V

    .line 307
    .restart local v3       #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    iget-object v5, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v5, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v5, p0, Lcom/google/android/picasasync/ImmediateSync;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    invoke-virtual {v5, v3}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;)Lcom/android/gallery3d/util/Future;
    :try_end_73
    .catchall {:try_start_5f .. :try_end_73} :catchall_15

    goto :goto_5d
.end method

.method public final declared-synchronized requestSyncAlbumListForAccount(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "account"

    .prologue
    .line 146
    monitor-enter p0

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, taskId:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/ImmediateSync$Task;

    .line 150
    .local v0, task:Lcom/google/android/picasasync/ImmediateSync$Task;
    if-eqz v0, :cond_2f

    invoke-virtual {v0}, Lcom/google/android/picasasync/ImmediateSync$Task;->addRequester()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 151
    const-string v2, "ImmediateSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "task already exists:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_1 .. :try_end_2d} :catchall_3e

    .line 157
    :goto_2d
    monitor-exit p0

    return-object v1

    .line 154
    :cond_2f
    :try_start_2f
    iget-object v2, p0, Lcom/google/android/picasasync/ImmediateSync;->mCompleteTaskMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/picasasync/ImmediateSync;->requestSyncAlbumList(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_3d
    .catchall {:try_start_2f .. :try_end_3d} :catchall_3e

    goto :goto_2d

    .line 146
    .end local v0           #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    .end local v1           #taskId:Ljava/lang/String;
    :catchall_3e
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized requestSyncAlbumListForAllAccounts()Ljava/lang/String;
    .registers 4

    .prologue
    .line 128
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync;->mPendingTaskMap:Ljava/util/HashMap;

    const-string v2, "all"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/ImmediateSync$Task;

    .line 132
    .local v0, task:Lcom/google/android/picasasync/ImmediateSync$Task;
    if-eqz v0, :cond_1e

    invoke-virtual {v0}, Lcom/google/android/picasasync/ImmediateSync$Task;->addRequester()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 133
    const-string v1, "ImmediateSync"

    const-string v2, "task already exists:all"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const-string v1, "all"
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_2e

    .line 139
    :goto_1c
    monitor-exit p0

    return-object v1

    .line 136
    :cond_1e
    :try_start_1e
    iget-object v1, p0, Lcom/google/android/picasasync/ImmediateSync;->mCompleteTaskMap:Ljava/util/HashMap;

    const-string v2, "all"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v1, "all"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/picasasync/ImmediateSync;->requestSyncAlbumList(Ljava/lang/String;[Ljava/lang/String;)V

    .line 139
    const-string v1, "all"
    :try_end_2d
    .catchall {:try_start_1e .. :try_end_2d} :catchall_2e

    goto :goto_1c

    .line 128
    .end local v0           #task:Lcom/google/android/picasasync/ImmediateSync$Task;
    :catchall_2e
    move-exception v1

    monitor-exit p0

    throw v1
.end method
