.class final Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;
.super Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;
.source "PhotoPrefetch.java"

# interfaces
.implements Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PhotoPrefetch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrefetchFullImage"
.end annotation


# instance fields
.field private mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

.field final synthetic this$0:Lcom/google/android/picasasync/PhotoPrefetch;


# direct methods
.method public constructor <init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "accountName"

    .prologue
    .line 272
    iput-object p1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    .line 273
    sget-object v0, Lcom/google/android/picasasync/SyncState;->PREFETCH_FULL_IMAGE:Lcom/google/android/picasasync/SyncState;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;-><init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    .line 274
    return-void
.end method


# virtual methods
.method public final isBackgroundSync()Z
    .registers 2

    .prologue
    .line 320
    const/4 v0, 0x0

    return v0
.end method

.method public final onDownloadFinish$256a6c5()V
    .registers 5

    .prologue
    .line 312
    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    .line 313
    .local v1, stats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;
    iget v2, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    .line 314
    iget v2, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I

    iget v3, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    sub-int v0, v2, v3

    .line 315
    .local v0, completeCount:I
    iget-object v2, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    iget v3, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I

    invoke-virtual {v2, v0, v3}, Lcom/google/android/picasasync/PhotoPrefetch;->updateOngoingNotification(II)V

    .line 316
    return-void
.end method

.method public final performSync(Landroid/content/SyncResult;)V
    .registers 5
    .parameter "result"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    const-string v1, "PrefetchFullImage"

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    .line 281
    .local v0, statsId:I
    :try_start_6
    invoke-virtual {p0, p1}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->performSyncCommon(Landroid/content/SyncResult;)V
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_f

    .line 284
    const-string v1, "picasa.prefetch.full_image"

    invoke-static {v0, v1}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    .line 285
    return-void

    .line 284
    :catchall_f
    move-exception v1

    const-string v2, "picasa.prefetch.full_image"

    invoke-static {v0, v2}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    throw v1
.end method

.method protected final performSyncInternal$45259df4(Lcom/google/android/picasasync/UserEntry;Lcom/google/android/picasasync/PrefetchHelper;)Z
    .registers 7
    .parameter "user"
    .parameter "helper"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 291
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {v0, p0}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->setCacheDownloadListener(Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-static {v0, v2}, Lcom/google/android/picasasync/PhotoPrefetch;->access$100(Lcom/google/android/picasasync/PhotoPrefetch;Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;)V

    .line 293
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/google/android/picasasync/PrefetchHelper;->getCacheStatistics(I)Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    .line 294
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget v0, v0, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    if-nez v0, :cond_1c

    move v0, v1

    .line 306
    :goto_1b
    return v0

    .line 296
    :cond_1c
    :try_start_1c
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {p2, v0, p1}, Lcom/google/android/picasasync/PrefetchHelper;->syncFullImagesForUser(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Lcom/google/android/picasasync/UserEntry;)V
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_4b

    .line 299
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    #getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 303
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget v0, v0, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    if-nez v0, :cond_41

    .line 304
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget v2, v2, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I

    invoke-virtual {v0, v2}, Lcom/google/android/picasasync/PhotoPrefetch;->showPrefetchCompleteNotification(I)V

    .line 306
    :cond_41
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z

    move-result v0

    if-nez v0, :cond_5f

    move v0, v1

    goto :goto_1b

    .line 299
    :catchall_4b
    move-exception v0

    move-object v2, v0

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    #getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v0

    const-string v3, "notification"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    throw v2

    .line 306
    :cond_5f
    const/4 v0, 0x0

    goto :goto_1b
.end method
