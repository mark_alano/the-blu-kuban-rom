.class final Lcom/google/android/picasasync/PrefetchHelper;
.super Ljava/lang/Object;
.source "PrefetchHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;,
        Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;,
        Lcom/google/android/picasasync/PrefetchHelper$CacheStats;
    }
.end annotation


# static fields
.field private static final ALBUM_TABLE_NAME:Ljava/lang/String;

.field private static final PHOTO_TABLE_NAME:Ljava/lang/String;

.field private static final PROJECTION_ID:[Ljava/lang/String;

.field private static final PROJECTION_ID_CACHE_FLAG_STATUS_THUMBNAIL:[Ljava/lang/String;

.field private static final PROJECTION_ID_ROTATION_CONTENT_URL_CONTENT_TYPE_SCREENNAIL_URL:[Ljava/lang/String;

.field private static final PROJECTION_ID_SCREENNAIL_URL:[Ljava/lang/String;

.field private static final PROJECTION_ID_THUMBNAIL_URL:[Ljava/lang/String;

.field private static final QUERY_CACHE_STATUS_COUNT:Ljava/lang/String;

.field private static final WHERE_ALBUM_ID_AND_CACHE_STATUS:Ljava/lang/String;

.field private static final WHERE_CACHE_STATUS_AND_USER_ID:Ljava/lang/String;

.field private static final WHERE_USER_ID_AND_CACHE_FLAG:Ljava/lang/String;

.field private static sInstance:Lcom/google/android/picasasync/PrefetchHelper;


# instance fields
.field private mCacheConfigVersion:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mCacheDir:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 80
    sget-object v0, Lcom/google/android/picasasync/AlbumEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    .line 81
    sget-object v0, Lcom/google/android/picasasync/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    .line 94
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID:[Ljava/lang/String;

    .line 95
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "rotation"

    aput-object v1, v0, v4

    const-string v1, "content_url"

    aput-object v1, v0, v5

    const-string v1, "content_type"

    aput-object v1, v0, v6

    const-string v1, "screennail_url"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_ROTATION_CONTENT_URL_CONTENT_TYPE_SCREENNAIL_URL:[Ljava/lang/String;

    .line 101
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "cache_flag"

    aput-object v1, v0, v4

    const-string v1, "cache_status"

    aput-object v1, v0, v5

    const-string v1, "thumbnail_url"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_CACHE_FLAG_STATUS_THUMBNAIL:[Ljava/lang/String;

    .line 106
    const-string v0, "%s=? AND %s=?"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "user_id"

    aput-object v2, v1, v3

    const-string v2, "cache_flag"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->WHERE_USER_ID_AND_CACHE_FLAG:Ljava/lang/String;

    .line 108
    const-string v0, "%s=? AND %s=?"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "album_id"

    aput-object v2, v1, v3

    const-string v2, "cache_status"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->WHERE_ALBUM_ID_AND_CACHE_STATUS:Ljava/lang/String;

    .line 339
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "screennail_url"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_SCREENNAIL_URL:[Ljava/lang/String;

    .line 342
    const-string v0, "%s = ? AND %s = ?"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "cache_status"

    aput-object v2, v1, v3

    const-string v2, "user_id"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->WHERE_CACHE_STATUS_AND_USER_ID:Ljava/lang/String;

    .line 383
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thumbnail_url"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_THUMBNAIL_URL:[Ljava/lang/String;

    .line 674
    const-string v0, "SELECT count(*), %s.%s AS status FROM %s, %s WHERE %s.%s = %s.%s AND %s.%s = ? GROUP BY status"

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    aput-object v2, v1, v3

    const-string v2, "cache_status"

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "album_id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "cache_flag"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->QUERY_CACHE_STATUS_COUNT:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper;->mCacheConfigVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper;->mContext:Landroid/content/Context;

    .line 127
    invoke-static {p1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    .line 128
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/picasasync/PrefetchHelper;)Ljava/util/concurrent/atomic/AtomicInteger;
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper;->mCacheConfigVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private static collectKeepSet(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/HashMap;Ljava/lang/Integer;)V
    .registers 14
    .parameter "db"
    .parameter "albumId"
    .parameter
    .parameter "type"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "J",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, keepSet:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 243
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 244
    .local v4, whereArgs:[Ljava/lang/String;
    sget-object v1, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID:[Ljava/lang/String;

    const-string v3, "album_id=?"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 247
    .local v8, cursor:Landroid/database/Cursor;
    :goto_18
    :try_start_18
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 248
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v0, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2a
    .catchall {:try_start_18 .. :try_end_2a} :catchall_2b

    goto :goto_18

    .line 251
    :catchall_2b
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_30
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 252
    return-void
.end method

.method private deleteUnusedAlbumCovers$3ec14b17(Ljava/util/HashSet;)V
    .registers 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    .local p1, coverSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->getCacheDirectory()Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, cacheDir:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    const-string v8, "picasa_covers"

    invoke-direct {v2, v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    .local v2, folder:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v7

    .line 305
    .local v7, names:[Ljava/lang/String;
    if-nez v7, :cond_12

    .line 314
    :cond_11
    return-void

    .line 306
    :cond_12
    move-object v0, v7

    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v7

    .local v5, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_15
    if-ge v3, v5, :cond_11

    aget-object v6, v0, v3

    .line 307
    .local v6, name:Ljava/lang/String;
    const/16 v8, 0x2e

    invoke-virtual {v6, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    if-gez v8, :cond_4a

    move-object v4, v6

    .line 308
    .local v4, key:Ljava/lang/String;
    :goto_22
    invoke-virtual {p1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_47

    .line 309
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v2, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v8

    if-nez v8, :cond_47

    .line 310
    const-string v8, "PrefetchHelper"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "cannot delete album cover: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :cond_47
    add-int/lit8 v3, v3, 0x1

    goto :goto_15

    .line 307
    .end local v4           #key:Ljava/lang/String;
    :cond_4a
    const/4 v9, 0x0

    invoke-virtual {v6, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_22
.end method

.method private deleteUnusedCacheFiles(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Ljava/util/HashMap;)V
    .registers 16
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    .local p2, keepSet:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->getCacheDirectory()Ljava/lang/String;

    move-result-object v3

    .line 319
    .local v3, cacheDir:Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .local v1, arr$:[Ljava/lang/String;
    array-length v9, v1

    .local v9, len$:I
    const/4 v7, 0x0

    .local v7, i$:I
    move v8, v7

    .end local v1           #arr$:[Ljava/lang/String;
    .end local v7           #i$:I
    .end local v9           #len$:I
    .local v8, i$:I
    :goto_10
    if-ge v8, v9, :cond_61

    aget-object v6, v1, v8

    .line 320
    .local v6, folderName:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z

    move-result v12

    if-nez v12, :cond_61

    .line 321
    const-string v12, "picasa-"

    invoke-virtual {v6, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2d

    .line 323
    :try_start_22
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    .local v5, folder:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, allFiles:[Ljava/lang/String;
    if-nez v0, :cond_31

    .line 319
    .end local v0           #allFiles:[Ljava/lang/String;
    .end local v5           #folder:Ljava/io/File;
    .end local v8           #i$:I
    :cond_2d
    :goto_2d
    add-int/lit8 v7, v8, 0x1

    .restart local v7       #i$:I
    move v8, v7

    .end local v7           #i$:I
    .restart local v8       #i$:I
    goto :goto_10

    .line 326
    .restart local v0       #allFiles:[Ljava/lang/String;
    .restart local v5       #folder:Ljava/io/File;
    :cond_31
    move-object v2, v0

    .local v2, arr$:[Ljava/lang/String;
    array-length v10, v0

    .local v10, len$:I
    const/4 v7, 0x0

    .end local v8           #i$:I
    .restart local v7       #i$:I
    :goto_34
    if-ge v7, v10, :cond_4f

    aget-object v4, v2, v7

    .line 327
    .local v4, filename:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z

    move-result v12

    if-nez v12, :cond_4f

    .line 328
    invoke-static {v5, v4, p2}, Lcom/google/android/picasasync/PrefetchHelper;->keepCacheFile(Ljava/io/File;Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v12

    if-nez v12, :cond_4c

    .line 329
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 326
    :cond_4c
    add-int/lit8 v7, v7, 0x1

    goto :goto_34

    .line 332
    .end local v4           #filename:Ljava/lang/String;
    :cond_4f
    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v12

    array-length v12, v12

    if-nez v12, :cond_2d

    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_59
    .catch Ljava/lang/Throwable; {:try_start_22 .. :try_end_59} :catch_5a

    goto :goto_2d

    .line 333
    .end local v0           #allFiles:[Ljava/lang/String;
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v5           #folder:Ljava/io/File;
    .end local v7           #i$:I
    .end local v10           #len$:I
    :catch_5a
    move-exception v11

    .line 334
    .local v11, t:Ljava/lang/Throwable;
    const-string v12, "PrefetchHelper"

    invoke-static {v12, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2d

    .line 337
    .end local v6           #folderName:Ljava/lang/String;
    .end local v11           #t:Ljava/lang/Throwable;
    .restart local v8       #i$:I
    :cond_61
    return-void
.end method

.method private static downloadPhoto(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Ljava/lang/String;Ljava/io/File;)Z
    .registers 15
    .parameter "context"
    .parameter "url"
    .parameter "file"

    .prologue
    const/4 v8, 0x0

    .line 597
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 598
    .local v6, startTime:J
    const/4 v3, 0x0

    .line 599
    .local v3, os:Ljava/io/OutputStream;
    const/4 v2, 0x0

    .line 601
    .local v2, is:Ljava/io/InputStream;
    :try_start_7
    invoke-static {p1}, Lcom/google/android/picasastore/HttpUtils;->openInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 602
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_7c
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_10} :catch_51

    .line 603
    .end local v3           #os:Ljava/io/OutputStream;
    .local v4, os:Ljava/io/OutputStream;
    const/16 v9, 0x1000

    :try_start_12
    new-array v0, v9, [B

    .line 604
    .local v0, buffer:[B
    const/4 v9, 0x0

    array-length v10, v0

    invoke-virtual {v2, v0, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    .line 605
    .local v5, rc:I
    :goto_1a
    if-lez v5, :cond_40

    .line 606
    invoke-virtual {p0}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z

    move-result v9

    if-eqz v9, :cond_35

    .line 607
    invoke-static {v2}, Lcom/google/android/picasastore/HttpUtils;->abortConnectionSilently(Ljava/io/InputStream;)V
    :try_end_25
    .catchall {:try_start_12 .. :try_end_25} :catchall_8c
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_25} :catch_8f

    .line 608
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 621
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    sub-long/2addr v9, v6

    invoke-static {v9, v10}, Lcom/google/android/picasastore/MetricsUtils;->incrementNetworkOpDurationAndCount(J)V

    move-object v3, v4

    .end local v0           #buffer:[B
    .end local v4           #os:Ljava/io/OutputStream;
    .end local v5           #rc:I
    .restart local v3       #os:Ljava/io/OutputStream;
    :goto_34
    return v8

    .line 610
    .end local v3           #os:Ljava/io/OutputStream;
    .restart local v0       #buffer:[B
    .restart local v4       #os:Ljava/io/OutputStream;
    .restart local v5       #rc:I
    :cond_35
    const/4 v9, 0x0

    :try_start_36
    invoke-virtual {v4, v0, v9, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 611
    const/4 v9, 0x0

    array-length v10, v0

    invoke-virtual {v2, v0, v9, v10}, Ljava/io/InputStream;->read([BII)I
    :try_end_3e
    .catchall {:try_start_36 .. :try_end_3e} :catchall_8c
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_3e} :catch_8f

    move-result v5

    goto :goto_1a

    .line 613
    :cond_40
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 621
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sub-long/2addr v8, v6

    invoke-static {v8, v9}, Lcom/google/android/picasastore/MetricsUtils;->incrementNetworkOpDurationAndCount(J)V

    const/4 v8, 0x1

    move-object v3, v4

    .end local v4           #os:Ljava/io/OutputStream;
    .restart local v3       #os:Ljava/io/OutputStream;
    goto :goto_34

    .line 614
    .end local v0           #buffer:[B
    .end local v5           #rc:I
    :catch_51
    move-exception v1

    .line 615
    .local v1, e:Ljava/io/IOException;
    :goto_52
    :try_start_52
    invoke-static {v2}, Lcom/google/android/picasastore/HttpUtils;->abortConnectionSilently(Ljava/io/InputStream;)V

    .line 616
    const-string v9, "PrefetchHelper"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fail to download: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6d
    .catchall {:try_start_52 .. :try_end_6d} :catchall_7c

    .line 617
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 621
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    sub-long/2addr v9, v6

    invoke-static {v9, v10}, Lcom/google/android/picasastore/MetricsUtils;->incrementNetworkOpDurationAndCount(J)V

    goto :goto_34

    .line 619
    .end local v1           #e:Ljava/io/IOException;
    :catchall_7c
    move-exception v8

    :goto_7d
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 620
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 621
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    sub-long/2addr v9, v6

    invoke-static {v9, v10}, Lcom/google/android/picasastore/MetricsUtils;->incrementNetworkOpDurationAndCount(J)V

    throw v8

    .line 619
    .end local v3           #os:Ljava/io/OutputStream;
    .restart local v4       #os:Ljava/io/OutputStream;
    :catchall_8c
    move-exception v8

    move-object v3, v4

    .end local v4           #os:Ljava/io/OutputStream;
    .restart local v3       #os:Ljava/io/OutputStream;
    goto :goto_7d

    .line 614
    .end local v3           #os:Ljava/io/OutputStream;
    .restart local v4       #os:Ljava/io/OutputStream;
    :catch_8f
    move-exception v1

    move-object v3, v4

    .end local v4           #os:Ljava/io/OutputStream;
    .restart local v3       #os:Ljava/io/OutputStream;
    goto :goto_52
.end method

.method private generateScreennail(JI)V
    .registers 23
    .parameter "photoId"
    .parameter "rotation"

    .prologue
    .line 514
    invoke-direct/range {p0 .. p0}, Lcom/google/android/picasasync/PrefetchHelper;->getAvailableStorage()J

    move-result-wide v9

    .line 515
    .local v9, availableStorage:J
    const-wide/32 v3, 0x40000000

    cmp-long v3, v9, v3

    if-gez v3, :cond_26

    .line 516
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "space not enough: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", stop sync"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 519
    :cond_26
    const/4 v14, 0x0

    .line 521
    .local v14, fos:Ljava/io/FileOutputStream;
    :try_start_27
    const-string v3, ".full"

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v3}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v11

    .line 522
    .local v11, cacheFile:Ljava/io/File;
    const-string v3, ".screen"

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v3}, Lcom/google/android/picasastore/PicasaStoreFacade;->createCacheFile(JLjava/lang/String;)Ljava/io/File;
    :try_end_36
    .catchall {:try_start_27 .. :try_end_36} :catchall_e6
    .catch Ljava/lang/Throwable; {:try_start_27 .. :try_end_36} :catch_ca

    move-result-object v17

    .line 524
    .local v17, screennailCacheFile:Ljava/io/File;
    if-eqz v11, :cond_3b

    if-nez v17, :cond_40

    .line 545
    :cond_3b
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 546
    .end local v11           #cacheFile:Ljava/io/File;
    .end local v17           #screennailCacheFile:Ljava/io/File;
    :goto_3f
    return-void

    .line 525
    .restart local v11       #cacheFile:Ljava/io/File;
    .restart local v17       #screennailCacheFile:Ljava/io/File;
    :cond_40
    :try_start_40
    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    .line 527
    .local v12, cacheFilePath:Ljava/lang/String;
    new-instance v16, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 528
    .local v16, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    move-object/from16 v0, v16

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 529
    move-object/from16 v0, v16

    invoke-static {v12, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 530
    move-object/from16 v0, v16

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v16

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/lit16 v3, v3, 0x280

    div-int/lit16 v4, v4, 0x280

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_7d

    const/4 v3, 0x1

    :goto_67
    move-object/from16 v0, v16

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 532
    const/4 v3, 0x0

    move-object/from16 v0, v16

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 533
    move-object/from16 v0, v16

    invoke-static {v12, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_75
    .catchall {:try_start_40 .. :try_end_75} :catchall_e6
    .catch Ljava/lang/Throwable; {:try_start_40 .. :try_end_75} :catch_ca

    move-result-object v2

    .line 534
    .local v2, bitmap:Landroid/graphics/Bitmap;
    if-nez v2, :cond_8b

    .line 545
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_3f

    .line 530
    .end local v2           #bitmap:Landroid/graphics/Bitmap;
    :cond_7d
    const/16 v4, 0x8

    if-gt v3, v4, :cond_86

    :try_start_81
    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->prevPowerOf2(I)I

    move-result v3

    goto :goto_67

    :cond_86
    div-int/lit8 v3, v3, 0x8

    mul-int/lit8 v3, v3, 0x8

    goto :goto_67

    .line 536
    .restart local v2       #bitmap:Landroid/graphics/Bitmap;
    :cond_8b
    const/16 v3, 0x280

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/android/gallery3d/common/BitmapUtils;->resizeDownBySideLength(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 538
    if-nez p3, :cond_ab

    .line 539
    :goto_94
    const/16 v3, 0x5f

    invoke-static {v2, v3}, Lcom/android/gallery3d/common/BitmapUtils;->compressToBytes(Landroid/graphics/Bitmap;I)[B

    move-result-object v13

    .line 540
    .local v13, content:[B
    new-instance v15, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_a1
    .catchall {:try_start_81 .. :try_end_a1} :catchall_e6
    .catch Ljava/lang/Throwable; {:try_start_81 .. :try_end_a1} :catch_ca

    .line 541
    .end local v14           #fos:Ljava/io/FileOutputStream;
    .local v15, fos:Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    :try_start_a2
    array-length v4, v13

    invoke-virtual {v15, v13, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_a6
    .catchall {:try_start_a2 .. :try_end_a6} :catchall_eb
    .catch Ljava/lang/Throwable; {:try_start_a2 .. :try_end_a6} :catch_ee

    .line 545
    invoke-static {v15}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v14, v15

    .line 546
    .end local v15           #fos:Ljava/io/FileOutputStream;
    .restart local v14       #fos:Ljava/io/FileOutputStream;
    goto :goto_3f

    .line 538
    .end local v13           #content:[B
    :cond_ab
    :try_start_ab
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    move/from16 v0, p3

    int-to-float v3, v0

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_c8
    .catchall {:try_start_ab .. :try_end_c8} :catchall_e6
    .catch Ljava/lang/Throwable; {:try_start_ab .. :try_end_c8} :catch_ca

    move-object v2, v3

    goto :goto_94

    .line 542
    .end local v2           #bitmap:Landroid/graphics/Bitmap;
    .end local v11           #cacheFile:Ljava/io/File;
    .end local v12           #cacheFilePath:Ljava/lang/String;
    .end local v16           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v17           #screennailCacheFile:Ljava/io/File;
    :catch_ca
    move-exception v18

    .line 543
    .local v18, t:Ljava/lang/Throwable;
    :goto_cb
    :try_start_cb
    const-string v3, "PrefetchHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cannot generate screennail: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e1
    .catchall {:try_start_cb .. :try_end_e1} :catchall_e6

    .line 545
    invoke-static {v14}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_3f

    .end local v18           #t:Ljava/lang/Throwable;
    :catchall_e6
    move-exception v3

    :goto_e7
    invoke-static {v14}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v3

    .end local v14           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #bitmap:Landroid/graphics/Bitmap;
    .restart local v11       #cacheFile:Ljava/io/File;
    .restart local v12       #cacheFilePath:Ljava/lang/String;
    .restart local v13       #content:[B
    .restart local v15       #fos:Ljava/io/FileOutputStream;
    .restart local v16       #options:Landroid/graphics/BitmapFactory$Options;
    .restart local v17       #screennailCacheFile:Ljava/io/File;
    :catchall_eb
    move-exception v3

    move-object v14, v15

    .end local v15           #fos:Ljava/io/FileOutputStream;
    .restart local v14       #fos:Ljava/io/FileOutputStream;
    goto :goto_e7

    .line 542
    .end local v14           #fos:Ljava/io/FileOutputStream;
    .restart local v15       #fos:Ljava/io/FileOutputStream;
    :catch_ee
    move-exception v18

    move-object v14, v15

    .end local v15           #fos:Ljava/io/FileOutputStream;
    .restart local v14       #fos:Ljava/io/FileOutputStream;
    goto :goto_cb
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/picasasync/PrefetchHelper;
    .registers 3
    .parameter "context"

    .prologue
    .line 119
    const-class v1, Lcom/google/android/picasasync/PrefetchHelper;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/picasasync/PrefetchHelper;->sInstance:Lcom/google/android/picasasync/PrefetchHelper;

    if-nez v0, :cond_e

    .line 120
    new-instance v0, Lcom/google/android/picasasync/PrefetchHelper;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/PrefetchHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasasync/PrefetchHelper;->sInstance:Lcom/google/android/picasasync/PrefetchHelper;

    .line 122
    :cond_e
    sget-object v0, Lcom/google/android/picasasync/PrefetchHelper;->sInstance:Lcom/google/android/picasasync/PrefetchHelper;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 119
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .registers 7
    .parameter "albumId"
    .parameter "thumbnailUrl"
    .parameter "ext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 591
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/picasastore/PicasaStoreFacade;->getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 592
    .local v0, file:Ljava/io/File;
    if-nez v0, :cond_e

    new-instance v1, Ljava/io/IOException;

    const-string v2, "external storage not present"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 593
    :cond_e
    return-object v0
.end method

.method private getAvailableStorage()J
    .registers 7

    .prologue
    .line 629
    :try_start_0
    new-instance v0, Landroid/os/StatFs;

    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->getCacheDirectory()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 630
    .local v0, stat:Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_11} :catch_15

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    .line 633
    .end local v0           #stat:Landroid/os/StatFs;
    :goto_14
    return-wide v2

    .line 631
    :catch_15
    move-exception v1

    .line 632
    .local v1, t:Ljava/lang/Throwable;
    const-string v2, "PrefetchHelper"

    const-string v3, "Fail to getAvailableStorage"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 633
    const-wide/16 v2, 0x0

    goto :goto_14
.end method

.method private getCacheDirectory()Ljava/lang/String;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v1, p0, Lcom/google/android/picasasync/PrefetchHelper;->mCacheDir:Ljava/lang/String;

    if-nez v1, :cond_18

    .line 132
    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    .line 133
    .local v0, cacheDir:Ljava/io/File;
    if-nez v0, :cond_12

    new-instance v1, Ljava/io/IOException;

    const-string v2, "external storage is not present"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_12
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/PrefetchHelper;->mCacheDir:Ljava/lang/String;

    .line 136
    .end local v0           #cacheDir:Ljava/io/File;
    :cond_18
    iget-object v1, p0, Lcom/google/android/picasasync/PrefetchHelper;->mCacheDir:Ljava/lang/String;

    return-object v1
.end method

.method private static keepCacheFile(Ljava/io/File;Ljava/lang/String;Ljava/util/HashMap;)Z
    .registers 15
    .parameter "folder"
    .parameter "filename"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, keepSet:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 263
    const/16 v8, 0x2e

    invoke-virtual {p1, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 264
    .local v0, dotPos:I
    const/4 v8, -0x1

    if-ne v0, v8, :cond_c

    .line 292
    :cond_b
    :goto_b
    return v6

    .line 265
    :cond_c
    invoke-virtual {p1, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 266
    .local v4, name:Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 268
    .local v1, ext:Ljava/lang/String;
    :try_start_14
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 269
    .local v2, id:J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 270
    .local v5, type:Ljava/lang/Integer;
    if-eqz v5, :cond_b

    .line 273
    const/4 v8, 0x2

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v8, v9, :cond_5e

    .line 274
    const-string v8, ".full"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5c

    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".screen"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_5c

    .line 276
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5c
    move v6, v7

    .line 279
    goto :goto_b

    .line 280
    :cond_5e
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v7, v8, :cond_75

    .line 281
    const-string v8, ".screen"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 282
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v6, v7

    .line 283
    goto :goto_b

    .line 286
    :cond_75
    const-string v7, "PrefetchHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "remove unknown cache file: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_89
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_89} :catch_8a

    goto :goto_b

    .line 290
    .end local v2           #id:J
    .end local v5           #type:Ljava/lang/Integer;
    :catch_8a
    move-exception v7

    const-string v7, "PrefetchHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "cannot parse id: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b
.end method

.method private notifyAlbumsChange()V
    .registers 5

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/picasasync/PrefetchHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/picasasync/PicasaFacade;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaFacade;->getAlbumsUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 258
    return-void
.end method

.method private static setCacheStatus(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/HashMap;)V
    .registers 11
    .parameter "db"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, keepSet:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    const/4 v6, 0x2

    const/4 v7, 0x1

    .line 208
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 210
    :try_start_5
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 211
    .local v3, values:Landroid/content/ContentValues;
    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    .line 212
    .local v4, whereArgs:[Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_51

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 213
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v6, :cond_4f

    move v2, v6

    .line 216
    .local v2, status:I
    :goto_2e
    const-string v5, "cache_status"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 217
    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    .line 218
    sget-object v5, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const-string v8, "_id=?"

    invoke-virtual {p0, v5, v3, v8, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_5 .. :try_end_49} :catchall_4a

    goto :goto_15

    .line 222
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #status:I
    .end local v3           #values:Landroid/content/ContentValues;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :catchall_4a
    move-exception v5

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .restart local v0       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #values:Landroid/content/ContentValues;
    .restart local v4       #whereArgs:[Ljava/lang/String;
    :cond_4f
    move v2, v7

    .line 213
    goto :goto_2e

    .line 220
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_51
    :try_start_51
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_54
    .catchall {:try_start_51 .. :try_end_54} :catchall_4a

    .line 222
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 223
    return-void
.end method

.method private syncOnePhoto(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;JLjava/lang/String;Ljava/lang/String;)Z
    .registers 18
    .parameter "context"
    .parameter "photoId"
    .parameter "url"
    .parameter "extension"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->getAvailableStorage()J

    move-result-wide v1

    .line 553
    .local v1, availableStorage:J
    const-wide/32 v6, 0x40000000

    cmp-long v6, v1, v6

    if-gez v6, :cond_26

    .line 554
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "space not enough: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", stop sync"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 558
    :cond_26
    const-string v6, ".download"

    invoke-static {p2, p3, v6}, Lcom/google/android/picasastore/PicasaStoreFacade;->createCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 559
    .local v4, tempFile:Ljava/io/File;
    if-nez v4, :cond_36

    new-instance v6, Ljava/io/IOException;

    const-string v7, "external storage absent?"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 560
    :cond_36
    const-string v6, "PrefetchHelper"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_67

    const-string v6, ".full"

    move-object/from16 v0, p5

    if-ne v0, v6, :cond_67

    .line 561
    const-string v6, "PrefetchHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "download full image for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {p4 .. p4}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    :cond_67
    move-object/from16 v0, p4

    invoke-static {p1, v0, v4}, Lcom/google/android/picasasync/PrefetchHelper;->downloadPhoto(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Ljava/lang/String;Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_78

    .line 564
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 565
    const/4 v6, 0x0

    invoke-virtual {p1, p2, p3, v6}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->onDownloadFinish(JZ)V

    .line 566
    const/4 v6, 0x0

    .line 586
    :goto_77
    return v6

    .line 569
    :cond_78
    move-object/from16 v0, p5

    invoke-static {p2, p3, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->createCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 570
    .local v3, cacheFile:Ljava/io/File;
    invoke-virtual {v4, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_a1

    .line 571
    const-string v6, "PrefetchHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cannot rename file: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 573
    const/4 v6, 0x0

    invoke-virtual {p1, p2, p3, v6}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->onDownloadFinish(JZ)V

    .line 574
    const/4 v6, 0x0

    goto :goto_77

    .line 576
    :cond_a1
    const/4 v6, 0x1

    invoke-virtual {p1, p2, p3, v6}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->onDownloadFinish(JZ)V

    .line 582
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 583
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "cache_status"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 584
    iget-object v6, p0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v6}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    sget-object v7, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v5, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 586
    const/4 v6, 0x1

    goto :goto_77
.end method

.method private updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V
    .registers 9
    .parameter "db"
    .parameter "albumId"
    .parameter "status"

    .prologue
    .line 228
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 229
    .local v0, values:Landroid/content/ContentValues;
    const-string v2, "cache_status"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 230
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 231
    .local v1, whereArgs:[Ljava/lang/String;
    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    const-string v3, "_id=?"

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 232
    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->notifyAlbumsChange()V

    .line 233
    return-void
.end method


# virtual methods
.method public final cleanCache(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;)V
    .registers 23
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    const-string v4, "PrefetchHelper.cleanCache"

    invoke-static {v4}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v18

    .line 144
    .local v18, statsId:I
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    .line 147
    .local v17, keepSet:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 149
    .local v13, albumSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 152
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 153
    .local v20, values:Landroid/content/ContentValues;
    const-string v4, "cache_status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 154
    sget-object v4, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const-string v5, "cache_status <> 0"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 158
    sget-object v4, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    sget-object v5, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_CACHE_FLAG_STATUS_THUMBNAIL:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 161
    .local v16, cursor:Landroid/database/Cursor;
    :cond_40
    :goto_40
    :try_start_40
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_b5

    .line 162
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z
    :try_end_49
    .catchall {:try_start_40 .. :try_end_49} :catchall_8f

    move-result v4

    if-eqz v4, :cond_50

    .line 190
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 203
    :goto_4f
    return-void

    .line 164
    :cond_50
    const/4 v4, 0x0

    :try_start_51
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 165
    .local v11, albumId:J
    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 166
    .local v14, cacheFlag:I
    const/4 v4, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 167
    .local v15, cacheStatus:I
    const/4 v4, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 168
    .local v19, thumbnailUrl:Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-static {v11, v12, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getAlbumCoverKey(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 170
    const/4 v4, 0x2

    if-ne v14, v4, :cond_94

    .line 171
    const/4 v4, 0x3

    if-eq v15, v4, :cond_84

    const/4 v4, 0x1

    if-eq v15, v4, :cond_84

    .line 173
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11, v12, v4}, Lcom/google/android/picasasync/PrefetchHelper;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V

    .line 175
    :cond_84
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-static {v3, v11, v12, v0, v4}, Lcom/google/android/picasasync/PrefetchHelper;->collectKeepSet(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/HashMap;Ljava/lang/Integer;)V
    :try_end_8e
    .catchall {:try_start_51 .. :try_end_8e} :catchall_8f

    goto :goto_40

    .line 190
    .end local v11           #albumId:J
    .end local v14           #cacheFlag:I
    .end local v15           #cacheStatus:I
    .end local v19           #thumbnailUrl:Ljava/lang/String;
    :catchall_8f
    move-exception v4

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    throw v4

    .line 176
    .restart local v11       #albumId:J
    .restart local v14       #cacheFlag:I
    .restart local v15       #cacheStatus:I
    .restart local v19       #thumbnailUrl:Ljava/lang/String;
    :cond_94
    const/4 v4, 0x1

    if-ne v14, v4, :cond_aa

    .line 178
    if-eqz v15, :cond_9f

    .line 179
    const/4 v4, 0x0

    :try_start_9a
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11, v12, v4}, Lcom/google/android/picasasync/PrefetchHelper;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V

    .line 181
    :cond_9f
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-static {v3, v11, v12, v0, v4}, Lcom/google/android/picasasync/PrefetchHelper;->collectKeepSet(Landroid/database/sqlite/SQLiteDatabase;JLjava/util/HashMap;Ljava/lang/Integer;)V

    goto :goto_40

    .line 182
    :cond_aa
    if-nez v14, :cond_40

    .line 184
    if-eqz v15, :cond_40

    .line 185
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11, v12, v4}, Lcom/google/android/picasasync/PrefetchHelper;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V
    :try_end_b4
    .catchall {:try_start_9a .. :try_end_b4} :catchall_8f

    goto :goto_40

    .line 190
    .end local v11           #albumId:J
    .end local v14           #cacheFlag:I
    .end local v15           #cacheStatus:I
    .end local v19           #thumbnailUrl:Ljava/lang/String;
    :cond_b5
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 193
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/picasasync/PrefetchHelper;->deleteUnusedAlbumCovers$3ec14b17(Ljava/util/HashSet;)V

    .line 197
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/picasasync/PrefetchHelper;->deleteUnusedCacheFiles(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Ljava/util/HashMap;)V

    .line 200
    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/google/android/picasasync/PrefetchHelper;->setCacheStatus(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/HashMap;)V

    .line 202
    invoke-static/range {v18 .. v18}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    goto :goto_4f
.end method

.method public final createPrefetchContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;
    .registers 4
    .parameter "syncResult"
    .parameter "thread"

    .prologue
    .line 705
    new-instance v0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;-><init>(Lcom/google/android/picasasync/PrefetchHelper;Landroid/content/SyncResult;Ljava/lang/Thread;)V

    return-object v0
.end method

.method public final getCacheStatistics(I)Lcom/google/android/picasasync/PrefetchHelper$CacheStats;
    .registers 11
    .parameter "flag"

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 684
    iget-object v6, p0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v6}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 685
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    new-array v5, v8, [Ljava/lang/String;

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 686
    .local v5, whereArgs:[Ljava/lang/String;
    sget-object v6, Lcom/google/android/picasasync/PrefetchHelper;->QUERY_CACHE_STATUS_COUNT:Ljava/lang/String;

    invoke-virtual {v2, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 687
    .local v1, cursor:Landroid/database/Cursor;
    new-instance v3, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    invoke-direct {v3}, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;-><init>()V

    .line 688
    .local v3, stats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;
    if-nez v1, :cond_1f

    .line 700
    :goto_1e
    return-object v3

    .line 690
    :cond_1f
    :goto_1f
    :try_start_1f
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_41

    .line 691
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 692
    .local v0, count:I
    const/4 v6, 0x1

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 693
    .local v4, status:I
    if-eqz v4, :cond_36

    .line 694
    iget v6, v3, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    add-int/2addr v6, v0

    iput v6, v3, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    .line 696
    :cond_36
    iget v6, v3, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I

    add-int/2addr v6, v0

    iput v6, v3, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I
    :try_end_3b
    .catchall {:try_start_1f .. :try_end_3b} :catchall_3c

    goto :goto_1f

    .line 700
    .end local v0           #count:I
    .end local v4           #status:I
    :catchall_3c
    move-exception v6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v6

    :cond_41
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1e
.end method

.method public final setAlbumCachingFlag(JI)V
    .registers 10
    .parameter "albumId"
    .parameter "cachingFlag"

    .prologue
    .line 648
    packed-switch p3, :pswitch_data_3e

    .line 667
    :cond_3
    :goto_3
    return-void

    .line 657
    :pswitch_4
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 658
    .local v1, values:Landroid/content/ContentValues;
    const-string v3, "cache_flag"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 659
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 660
    .local v2, whereArgs:[Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v3}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v4, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    const-string v5, "_id=?"

    invoke-virtual {v3, v4, v1, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 662
    .local v0, count:I
    if-lez v0, :cond_3

    .line 663
    iget-object v3, p0, Lcom/google/android/picasasync/PrefetchHelper;->mCacheConfigVersion:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 664
    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->notifyAlbumsChange()V

    .line 665
    iget-object v3, p0, Lcom/google/android/picasasync/PrefetchHelper;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/picasasync/PicasaSyncManager;->requestPrefetchSync()V

    goto :goto_3

    .line 648
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final syncAlbumCoversForUser(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Lcom/google/android/picasasync/UserEntry;)V
    .registers 16
    .parameter "context"
    .parameter "user"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 389
    new-instance v11, Ljava/io/File;

    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->getCacheDirectory()Ljava/lang/String;

    move-result-object v1

    const-string v2, "picasa_covers"

    invoke-direct {v11, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    .local v11, folder:Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_22

    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_22

    .line 391
    const-string v1, "PrefetchHelper"

    const-string v2, "cannot create album-cover folder"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :goto_21
    return-void

    .line 395
    :cond_22
    iget-object v1, p0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 396
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-array v4, v3, [Ljava/lang/String;

    iget-wide v1, p2, Lcom/google/android/picasasync/UserEntry;->id:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    .line 397
    .local v4, whereArgs:[Ljava/lang/String;
    sget-object v1, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_THUMBNAIL_URL:[Ljava/lang/String;

    const-string v3, "user_id=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 401
    .local v10, cursor:Landroid/database/Cursor;
    :cond_3e
    :goto_3e
    :try_start_3e
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_cf

    .line 402
    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z
    :try_end_47
    .catchall {:try_start_3e .. :try_end_47} :catchall_8c

    move-result v1

    if-eqz v1, :cond_4e

    .line 410
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_21

    .line 403
    :cond_4e
    const/4 v1, 0x0

    :try_start_4f
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 404
    .local v8, albumId:J
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 405
    .local v12, thumbnailUrl:Ljava/lang/String;
    const-string v1, ".thumb"

    invoke-static {v8, v9, v12, v1}, Lcom/google/android/picasasync/PrefetchHelper;->getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_91

    :goto_64
    if-nez v1, :cond_3e

    .line 406
    invoke-direct {p0}, Lcom/google/android/picasasync/PrefetchHelper;->getAvailableStorage()J

    move-result-wide v1

    const-wide/32 v6, 0x40000000

    cmp-long v3, v1, v6

    if-gez v3, :cond_93

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "space not enough: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", stop sync"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_8c
    .catchall {:try_start_4f .. :try_end_8c} :catchall_8c

    .line 410
    .end local v8           #albumId:J
    .end local v12           #thumbnailUrl:Ljava/lang/String;
    :catchall_8c
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1

    .restart local v8       #albumId:J
    .restart local v12       #thumbnailUrl:Ljava/lang/String;
    :cond_91
    move-object v1, v5

    .line 405
    goto :goto_64

    .line 406
    :cond_93
    :try_start_93
    const-string v1, ".download"

    invoke-static {v8, v9, v12, v1}, Lcom/google/android/picasasync/PrefetchHelper;->getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const/16 v2, 0xc8

    const/4 v3, 0x1

    invoke-static {v12, v2, v3}, Lcom/google/android/picasasync/PicasaApi;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v1}, Lcom/google/android/picasasync/PrefetchHelper;->downloadPhoto(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Ljava/lang/String;Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_aa

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_3e

    :cond_aa
    const-string v2, ".thumb"

    invoke-static {v8, v9, v12, v2}, Lcom/google/android/picasasync/PrefetchHelper;->getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_3e

    const-string v2, "PrefetchHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "cannot rename file: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_cd
    .catchall {:try_start_93 .. :try_end_cd} :catchall_8c

    goto/16 :goto_3e

    .line 410
    .end local v8           #albumId:J
    .end local v12           #thumbnailUrl:Ljava/lang/String;
    :cond_cf
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_21
.end method

.method public final syncFullImagesForUser(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Lcom/google/android/picasasync/UserEntry;)V
    .registers 22
    .parameter "context"
    .parameter "user"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 441
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 442
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    iget-wide v3, v0, Lcom/google/android/picasasync/UserEntry;->id:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    .line 444
    .local v5, whereArgs:[Ljava/lang/String;
    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->ALBUM_TABLE_NAME:Ljava/lang/String;

    sget-object v3, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID:[Ljava/lang/String;

    sget-object v4, Lcom/google/android/picasasync/PrefetchHelper;->WHERE_USER_ID_AND_CACHE_FLAG:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 448
    .local v14, cursor:Landroid/database/Cursor;
    :cond_2b
    :goto_2b
    :try_start_2b
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_143

    .line 449
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z
    :try_end_34
    .catchall {:try_start_2b .. :try_end_34} :catchall_73

    move-result v2

    if-eqz v2, :cond_3b

    .line 453
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 454
    :goto_3a
    return-void

    .line 450
    :cond_3b
    const/4 v2, 0x0

    :try_start_3c
    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const/4 v4, 0x2

    new-array v10, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v10, v4

    const/4 v4, 0x1

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v10, v4

    sget-object v7, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    sget-object v8, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_ROTATION_CONTENT_URL_CONTENT_TYPE_SCREENNAIL_URL:[Ljava/lang/String;

    sget-object v9, Lcom/google/android/picasasync/PrefetchHelper;->WHERE_ALBUM_ID_AND_CACHE_STATUS:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_66
    .catchall {:try_start_3c .. :try_end_66} :catchall_73

    move-result-object v4

    if-eqz v4, :cond_2b

    :try_start_69
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I
    :try_end_6c
    .catchall {:try_start_69 .. :try_end_6c} :catchall_f4

    move-result v7

    if-nez v7, :cond_78

    :try_start_6f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_72
    .catchall {:try_start_6f .. :try_end_72} :catchall_73

    goto :goto_2b

    .line 453
    :catchall_73
    move-exception v2

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v2

    .line 450
    :cond_78
    const/4 v7, 0x1

    :try_start_79
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2, v3, v7}, Lcom/google/android/picasasync/PrefetchHelper;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V

    :cond_7e
    :goto_7e
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_138

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->checkCacheConfigVersion()Z

    move-result v7

    if-nez v7, :cond_94

    const-string v7, "PrefetchHelper"

    const-string v8, "cache config has changed, stop sync"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->stopSync()V

    :cond_94
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z
    :try_end_97
    .catchall {:try_start_79 .. :try_end_97} :catchall_f4

    move-result v7

    if-eqz v7, :cond_9e

    :try_start_9a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_9d
    .catchall {:try_start_9a .. :try_end_9d} :catchall_73

    goto :goto_2b

    :cond_9e
    const/4 v7, 0x0

    :try_start_9f
    invoke-interface {v4, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const/4 v7, 0x1

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/4 v7, 0x2

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v7, 0x3

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/4 v7, 0x4

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, ".full"

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v7

    const-wide/16 v17, 0x0

    cmp-long v7, v7, v17

    if-nez v7, :cond_f9

    const-string v12, ".full"

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    invoke-direct/range {v7 .. v12}, Lcom/google/android/picasasync/PrefetchHelper;->syncOnePhoto(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;JLjava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_f9

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->getDownloadFailCount()I

    move-result v7

    const/4 v8, 0x3

    if-le v7, v8, :cond_f9

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "too many fail downloads"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_f4
    .catchall {:try_start_9f .. :try_end_f4} :catchall_f4

    :catchall_f4
    move-exception v2

    :try_start_f5
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_f9
    .catchall {:try_start_f5 .. :try_end_f9} :catchall_73

    :cond_f9
    :try_start_f9
    const-string v7, "image/"

    invoke-virtual {v15, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_108

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10, v13}, Lcom/google/android/picasasync/PrefetchHelper;->generateScreennail(JI)V

    goto/16 :goto_7e

    :cond_108
    const-string v7, "video/"

    invoke-virtual {v15, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7e

    const-string v12, ".screen"

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v11, v16

    invoke-direct/range {v7 .. v12}, Lcom/google/android/picasasync/PrefetchHelper;->syncOnePhoto(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;JLjava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7e

    const-string v7, "PrefetchHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "failed to fetch video screennail: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7e

    :cond_138
    const/4 v7, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2, v3, v7}, Lcom/google/android/picasasync/PrefetchHelper;->updateAlbumCacheStatus(Landroid/database/sqlite/SQLiteDatabase;JI)V
    :try_end_13e
    .catchall {:try_start_f9 .. :try_end_13e} :catchall_f4

    :try_start_13e
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_141
    .catchall {:try_start_13e .. :try_end_141} :catchall_73

    goto/16 :goto_2b

    .line 453
    :cond_143
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3a
.end method

.method public final syncScreenNailsForUser(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Lcom/google/android/picasasync/UserEntry;)V
    .registers 16
    .parameter "context"
    .parameter "user"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 349
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    iget-wide v1, p2, Lcom/google/android/picasasync/UserEntry;->id:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v3

    .line 352
    .local v4, whereArgs:[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/picasasync/PrefetchHelper;->mDbHelper:Lcom/google/android/picasasync/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 353
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v1, Lcom/google/android/picasasync/PrefetchHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/picasasync/PrefetchHelper;->PROJECTION_ID_SCREENNAIL_URL:[Ljava/lang/String;

    sget-object v3, Lcom/google/android/picasasync/PrefetchHelper;->WHERE_CACHE_STATUS_AND_USER_ID:Ljava/lang/String;

    const-string v7, "display_index"

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 358
    .local v11, cursor:Landroid/database/Cursor;
    :cond_27
    :try_start_27
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_76

    .line 359
    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->checkCacheConfigVersion()Z

    move-result v1

    if-nez v1, :cond_3d

    .line 360
    const-string v1, "PrefetchHelper"

    const-string v2, "cache config has changed, stop sync"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->stopSync()V

    .line 364
    :cond_3d
    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z
    :try_end_40
    .catchall {:try_start_27 .. :try_end_40} :catchall_71

    move-result v1

    if-eqz v1, :cond_47

    .line 379
    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 380
    :goto_46
    return-void

    .line 366
    :cond_47
    const/4 v1, 0x0

    :try_start_48
    invoke-interface {v11, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 367
    .local v7, photoId:J
    const/4 v1, 0x1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 369
    .local v12, screennailUrl:Ljava/lang/String;
    const/16 v1, 0x280

    const/4 v2, 0x0

    invoke-static {v12, v1, v2}, Lcom/google/android/picasasync/PicasaApi;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v9

    .line 372
    .local v9, downloadUrl:Ljava/lang/String;
    const-string v10, ".screen"

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/google/android/picasasync/PrefetchHelper;->syncOnePhoto(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;JLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_27

    .line 373
    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->getDownloadFailCount()I

    move-result v1

    const/4 v2, 0x3

    if-le v1, v2, :cond_27

    .line 374
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "too many fail downloads"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_71
    .catchall {:try_start_48 .. :try_end_71} :catchall_71

    .line 379
    .end local v7           #photoId:J
    .end local v9           #downloadUrl:Ljava/lang/String;
    .end local v12           #screennailUrl:Ljava/lang/String;
    :catchall_71
    move-exception v1

    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1

    :cond_76
    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_46
.end method
