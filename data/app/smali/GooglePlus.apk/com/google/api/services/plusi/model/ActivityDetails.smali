.class public final Lcom/google/api/services/plusi/model/ActivityDetails;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ActivityDetails.java"


# instance fields
.field public embedType:Ljava/lang/Integer;

.field public explanationType:Ljava/lang/String;

.field public isOwnerless:Ljava/lang/Boolean;

.field public isPublic:Ljava/lang/Boolean;

.field public isRead:Ljava/lang/Boolean;

.field public itemCategory:Ljava/lang/String;

.field public mediaType:Ljava/lang/String;

.field public mediaUrl:Ljava/lang/String;

.field public metadataNamespace:Ljava/lang/Integer;

.field public numComments:Ljava/lang/Integer;

.field public numPlusones:Ljava/lang/Integer;

.field public numShares:Ljava/lang/Integer;

.field public originalPosition:Ljava/lang/Integer;

.field public popularUpdatePosition:Ljava/lang/Integer;

.field public sourceStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
