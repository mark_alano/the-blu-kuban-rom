.class public final Lcom/google/api/services/plusi/model/AddressProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "AddressProto.java"


# instance fields
.field public addressLines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/AddressLinesProto;",
            ">;"
        }
    .end annotation
.end field

.field public component:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/AddressComponentProto;",
            ">;"
        }
    .end annotation
.end field

.field public crossStreet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/AddressComponentProto;",
            ">;"
        }
    .end annotation
.end field

.field public isMailing:Ljava/lang/Boolean;

.field public isPhysical:Ljava/lang/Boolean;

.field public koreanAddressMigration:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/AddressLinesProto;",
            ">;"
        }
    .end annotation
.end field

.field public unambiguouslyDesignatesFeature:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 60
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
