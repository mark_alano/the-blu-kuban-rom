.class public final Lcom/google/api/services/plusi/model/CategoryProtoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "CategoryProtoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/CategoryProto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/CategoryProtoJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/CategoryProtoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CategoryProtoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/CategoryProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/CategoryProtoJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/CategoryProto;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/GVerticalProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "gvertical"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "showClosedZippyEllipsis"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/CategoryProtoItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "zippyOpenedItem"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/CategoryProtoItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "zippyClosedItem"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "categoryLabel"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/CategoryProtoJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/CategoryProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/CategoryProtoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/CategoryProto;

    .end local p1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CategoryProto;->gvertical:Lcom/google/api/services/plusi/model/GVerticalProto;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CategoryProto;->showClosedZippyEllipsis:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CategoryProto;->zippyOpenedItem:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CategoryProto;->zippyClosedItem:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CategoryProto;->categoryLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/CategoryProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CategoryProto;-><init>()V

    return-object v0
.end method
