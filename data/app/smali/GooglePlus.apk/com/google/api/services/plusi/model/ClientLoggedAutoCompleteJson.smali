.class public final Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedAutoCompleteJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "acceptedQuery"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "personalizationType"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "suggestedText"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "query"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "obfuscatedGaiaId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "sourceNamespace"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedAutoCompleteJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;

    .end local p1
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;->acceptedQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;->personalizationType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;->suggestedText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;->query:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;->sourceNamespace:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;-><init>()V

    return-object v0
.end method
