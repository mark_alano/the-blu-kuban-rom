.class public final Lcom/google/api/services/plusi/model/ClientOutputData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientOutputData.java"


# instance fields
.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public circleMember:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public streamFilterCircle:Lcom/google/api/services/plusi/model/ClientLoggedCircle;

.field public suggestionInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public userInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientUserInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 49
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
