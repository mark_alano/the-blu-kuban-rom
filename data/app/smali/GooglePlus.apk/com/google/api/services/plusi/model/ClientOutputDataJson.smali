.class public final Lcom/google/api/services/plusi/model/ClientOutputDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientOutputDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ClientOutputData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ClientOutputDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ClientOutputDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientOutputDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ClientOutputDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientOutputDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ClientOutputData;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "streamFilterCircle"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/ClientLoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "circleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/ClientUserInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "userInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ClientOutputDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ClientOutputDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientOutputDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ClientOutputData;

    .end local p1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOutputData;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOutputData;->streamFilterCircle:Lcom/google/api/services/plusi/model/ClientLoggedCircle;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOutputData;->suggestionInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOutputData;->circleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientOutputData;->userInfo:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ClientOutputData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientOutputData;-><init>()V

    return-object v0
.end method
