.class public final Lcom/google/api/services/plusi/model/ContactInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ContactInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ContactInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ContactInfoJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ContactInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ContactInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ContactInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ContactInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ContactInfo;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "fax"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "mobile"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/GeoPointJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "geoPoint"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "deprecatedAddress"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "phone"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/ImJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "im"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/AddressJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "address"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "pager"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/EmailJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "email"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/MetadataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "metadata"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ContactInfoJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ContactInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ContactInfoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ContactInfo;

    .end local p1
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->fax:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->mobile:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->geoPoint:Lcom/google/api/services/plusi/model/GeoPoint;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->deprecatedAddress:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->phone:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->im:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->address:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->pager:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->email:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactInfo;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ContactInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ContactInfo;-><init>()V

    return-object v0
.end method
