.class public final Lcom/google/api/services/plusi/model/DataCoalescedItem;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataCoalescedItem.java"


# instance fields
.field public a2aData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/A2aData;",
            ">;"
        }
    .end annotation
.end field

.field public action:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAction;",
            ">;"
        }
    .end annotation
.end field

.field public category:Ljava/lang/String;

.field public coalescingCode:Ljava/lang/String;

.field public debug:Ljava/lang/String;

.field public entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

.field public entityReference:Ljava/lang/String;

.field public entityReferenceType:Ljava/lang/String;

.field public filterType:Ljava/lang/String;

.field public hashCode:Ljava/lang/Integer;

.field public id:Ljava/lang/String;

.field public involvedActorOid:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public isEntityDeleted:Ljava/lang/Boolean;

.field public isMuted:Ljava/lang/Boolean;

.field public isRead:Ljava/lang/Boolean;

.field public opaqueClientFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataKvPair;",
            ">;"
        }
    .end annotation
.end field

.field public pushEnabled:Ljava/lang/Boolean;

.field public timestamp:Ljava/lang/Double;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 86
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
