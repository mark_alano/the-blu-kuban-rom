.class public final Lcom/google/api/services/plusi/model/DataComment;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataComment.java"


# instance fields
.field public asbeActivityId:Ljava/lang/String;

.field public commentType:Ljava/lang/String;

.field public contentType:Ljava/lang/String;

.field public creationTimestamp:Ljava/lang/Long;

.field public id:Ljava/lang/String;

.field public lastUpdateTimestamp:Ljava/lang/Long;

.field public originalText:Ljava/lang/String;

.field public originalTextSegments:Lcom/google/api/services/plusi/model/EditSegments;

.field public plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

.field public text:Ljava/lang/String;

.field public textSegments:Lcom/google/api/services/plusi/model/ViewSegments;

.field public timestamp:Ljava/lang/String;

.field public user:Lcom/google/api/services/plusi/model/DataUser;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
