.class public final Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataNotificationSettingsDeliveryOptionJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "category"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "bucketId"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "enabledForEmail"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "enabled"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "offnetworkBucketId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "enabledForPhone"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    .end local p1
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;-><init>()V

    return-object v0
.end method
