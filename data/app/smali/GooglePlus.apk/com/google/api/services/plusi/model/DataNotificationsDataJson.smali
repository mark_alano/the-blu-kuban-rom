.class public final Lcom/google/api/services/plusi/model/DataNotificationsDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataNotificationsDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataNotificationsData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationsDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationsDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationsDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataNotificationsDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationsDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/DataNotificationsData;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "filteredId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "latestNotificationTime"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "hasMoreNotifications"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/DataCoalescedItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "coalescedItem"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "unreadCount"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/DataActorJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "actor"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/DataCoalescedItemJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "featuredItem"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "hasMoreUnreadNotifications"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "lastReadTime"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "continuationToken"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/DataUnreadCountDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "unreadCountData"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataNotificationsDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/DataNotificationsDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationsDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/DataNotificationsData;

    .end local p1
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->filteredId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->latestNotificationTime:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->hasMoreNotifications:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->coalescedItem:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->unreadCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->actor:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->featuredItem:Lcom/google/api/services/plusi/model/DataCoalescedItem;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->hasMoreUnreadNotifications:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->lastReadTime:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->continuationToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationsData;->unreadCountData:Lcom/google/api/services/plusi/model/DataUnreadCountData;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationsData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationsData;-><init>()V

    return-object v0
.end method
