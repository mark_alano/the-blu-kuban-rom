.class public final Lcom/google/api/services/plusi/model/DataSugggestionExplanation;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataSugggestionExplanation.java"


# instance fields
.field public activitiesByFriends:Ljava/lang/Integer;

.field public commonFriend:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCircleMemberId;",
            ">;"
        }
    .end annotation
.end field

.field public isFirstHop:Ljava/lang/Boolean;

.field public isSecondHop:Ljava/lang/Boolean;

.field public numberOfCommonFriends:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 77
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
