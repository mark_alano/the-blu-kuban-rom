.class public final Lcom/google/api/services/plusi/model/EditActivityRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EditActivityRequest.java"


# instance fields
.field public annotation:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public contentFormat:Ljava/lang/String;

.field public deleteLocation:Ljava/lang/Boolean;

.field public editSegments:Lcom/google/api/services/plusi/model/EditSegments;

.field public embed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedClientItem;",
            ">;"
        }
    .end annotation
.end field

.field public enableTracing:Ljava/lang/Boolean;

.field public externalId:Ljava/lang/String;

.field public isReshare:Ljava/lang/Boolean;

.field public mediaJson:Ljava/lang/String;

.field public preserveExistingAttachment:Ljava/lang/Boolean;

.field public renderContextLocation:Ljava/lang/String;

.field public updateText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 134
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
