.class public final Lcom/google/api/services/plusi/model/EntityEntityDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityEntityDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityEntityData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityEntityDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EntityEntityDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityEntityDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityEntityDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityEntityDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EntityEntityData;

    const/16 v1, 0x1c

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/EntityCirclesDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "circles"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/EntityEventsDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "plusEvents"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/EntityHangoutDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "hangout"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/EntityTargetSharedDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "targetShared"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/EntityBirthdayDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "birthdayData"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/EntityGadgetDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "gadget"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/EntitySegmentedShareDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "segmentedShareData"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/EntitySuggestionsDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "suggestions"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/EntityRecommendedPeopleDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "recommendedPeopleData"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-class v3, Lcom/google/api/services/plusi/model/EntitySummaryDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "summarySnippet"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-class v3, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "photos"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-class v3, Lcom/google/api/services/plusi/model/EntityLegacyClientDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "legacy"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "squares"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-class v3, Lcom/google/api/services/plusi/model/EntityUpdateDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "update"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityEntityDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EntityEntityDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityEntityDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EntityEntityData;

    .end local p1
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->circles:Lcom/google/api/services/plusi/model/EntityCirclesData;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->plusEvents:Lcom/google/api/services/plusi/model/EntityEventsData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->hangout:Lcom/google/api/services/plusi/model/EntityHangoutData;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->targetShared:Lcom/google/api/services/plusi/model/EntityTargetSharedData;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->birthdayData:Lcom/google/api/services/plusi/model/EntityBirthdayData;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->gadget:Lcom/google/api/services/plusi/model/EntityGadgetData;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->segmentedShareData:Lcom/google/api/services/plusi/model/EntitySegmentedShareData;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->suggestions:Lcom/google/api/services/plusi/model/EntitySuggestionsData;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->recommendedPeopleData:Lcom/google/api/services/plusi/model/EntityRecommendedPeopleData;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->photos:Lcom/google/api/services/plusi/model/EntityPhotosData;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->legacy:Lcom/google/api/services/plusi/model/EntityLegacyClientData;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->squares:Lcom/google/api/services/plusi/model/EntitySquaresData;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EntityEntityData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityEntityData;-><init>()V

    return-object v0
.end method
