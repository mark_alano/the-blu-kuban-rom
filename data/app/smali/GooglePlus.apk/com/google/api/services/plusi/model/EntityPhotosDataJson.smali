.class public final Lcom/google/api/services/plusi/model/EntityPhotosDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityPhotosDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityPhotosData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityPhotosDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityPhotosDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EntityPhotosData;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/EntityAlbumDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "album"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "photo"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "numPhotos"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "numTagsRemoved"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/EntityPhotoCropJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "photoCrop"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "numVideos"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "numTagged"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "numPhotosDeleted"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 30
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityPhotosDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityPhotosDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EntityPhotosData;

    .end local p1
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->album:Lcom/google/api/services/plusi/model/EntityAlbumData;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->numPhotos:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->numTagsRemoved:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->photoCrop:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->numVideos:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->numTagged:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityPhotosData;->numPhotosDeleted:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EntityPhotosData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityPhotosData;-><init>()V

    return-object v0
.end method
