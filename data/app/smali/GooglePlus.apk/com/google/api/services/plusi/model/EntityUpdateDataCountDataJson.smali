.class public final Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityUpdateDataCountDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "newResharerOid"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "resharerOid"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "globalPlusoneCount"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "newCommentPlusonerOid"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "commentPlusonerOid"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "newCommenterOid"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "newMentionerOid"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "postPlusonerOid"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "newPostPlusonerOid"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "mentionerOid"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "commenterOid"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;

    .end local p1
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->newResharerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->resharerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->globalPlusoneCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->newCommentPlusonerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->commentPlusonerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->newCommenterOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->newMentionerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->postPlusonerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->newPostPlusonerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->mentionerOid:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;->commenterOid:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;-><init>()V

    return-object v0
.end method
