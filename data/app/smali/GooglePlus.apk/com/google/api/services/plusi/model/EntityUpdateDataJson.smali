.class public final Lcom/google/api/services/plusi/model/EntityUpdateDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityUpdateDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityUpdateData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/EntityUpdateDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityUpdateDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityUpdateDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/EntityUpdateData;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "safeTitleHtml"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "safeAnnotationHtml"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "unreadCommentCount"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "unreadReshareCount"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "ownerOid"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/CommentJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "contextComment"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/EntityUpdateDataCountDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "countData"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/UpdateJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "activity"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "commentId"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippetJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "summary"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityUpdateDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/EntityUpdateDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityUpdateDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/EntityUpdateData;

    .end local p1
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->safeTitleHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->safeAnnotationHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->unreadCommentCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->unreadReshareCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->ownerOid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->contextComment:Lcom/google/api/services/plusi/model/Comment;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->countData:Lcom/google/api/services/plusi/model/EntityUpdateDataCountData;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->commentId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityUpdateData;->summary:Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/EntityUpdateData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityUpdateData;-><init>()V

    return-object v0
.end method
