.class public final Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "FavaDiagnosticsRequestStatJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;->INSTANCE:Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "failedAttemptsTime"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "requestPath"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "numFailedAttempts"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "serverTime"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "totalContributingTime"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "networkTime"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;->INSTANCE:Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStatJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;

    .end local p1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;->failedAttemptsTime:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;->requestPath:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;->numFailedAttempts:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;->serverTime:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;->totalContributingTime:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;->networkTime:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnosticsRequestStat;-><init>()V

    return-object v0
.end method
