.class public final Lcom/google/api/services/plusi/model/FieldRequestOptions;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "FieldRequestOptions.java"


# instance fields
.field public includeBuzzData:Ljava/lang/Boolean;

.field public includeDebugData:Ljava/lang/Boolean;

.field public includeDeprecatedFields:Ljava/lang/Boolean;

.field public includeEmbedsData:Ljava/lang/Boolean;

.field public includeLegacyMediaData:Ljava/lang/Boolean;

.field public includePhotosData:Ljava/lang/Boolean;

.field public includeSharingData:Ljava/lang/Boolean;

.field public includeSparksData:Ljava/lang/Boolean;

.field public includeUrls:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
