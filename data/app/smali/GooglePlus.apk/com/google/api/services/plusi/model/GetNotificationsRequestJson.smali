.class public final Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "GetNotificationsRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/GetNotificationsRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/GetNotificationsRequest;

    const/16 v1, 0x15

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dontCoalesce"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "clientVersion"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "featuredNotificationId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/NotificationsResponseOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "notificationsResponseOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "loadViewerData"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "geographicLocation"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "filter"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "notificationId"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "fetchUnreadCount"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "summarySnippets"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "maxResults"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "renderContextLocation"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "setPushEnabled"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "oldestNotificationTimeUsec"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "continuationToken"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 39
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;

    .end local p1
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->dontCoalesce:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->clientVersion:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->featuredNotificationId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->notificationsResponseOptions:Lcom/google/api/services/plusi/model/NotificationsResponseOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->loadViewerData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->geographicLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->filter:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->notificationId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->fetchUnreadCount:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->summarySnippets:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->maxResults:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->renderContextLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->setPushEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->oldestNotificationTimeUsec:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->continuationToken:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/GetNotificationsRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetNotificationsRequest;-><init>()V

    return-object v0
.end method
