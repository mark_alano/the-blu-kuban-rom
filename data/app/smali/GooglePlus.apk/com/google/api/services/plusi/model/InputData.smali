.class public final Lcom/google/api/services/plusi/model/InputData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "InputData.java"


# instance fields
.field public interestsAdded:Ljava/lang/Integer;

.field public interestsRemoved:Ljava/lang/Integer;

.field public invitesSent:Ljava/lang/Integer;

.field public profileCreationInfo:Lcom/google/api/services/plusi/model/ProfileCreationInfo;

.field public profileEdit:Lcom/google/api/services/plusi/model/ProfileEdit;

.field public socialCircleRawQuery:Ljava/lang/String;

.field public upgradeInfo:Lcom/google/api/services/plusi/model/UpgradeInfo;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
