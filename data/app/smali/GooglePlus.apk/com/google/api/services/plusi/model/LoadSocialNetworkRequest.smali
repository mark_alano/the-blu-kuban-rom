.class public final Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoadSocialNetworkRequest.java"


# instance fields
.field public circlesOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public personListOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

.field public systemGroupsOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
