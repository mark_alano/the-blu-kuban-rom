.class public final Lcom/google/api/services/plusi/model/Locations;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Locations.java"


# instance fields
.field public currentGeoLocation:Lcom/google/api/services/plusi/model/GeoLocation;

.field public currentLocation:Ljava/lang/String;

.field public locationMapUrl:Ljava/lang/String;

.field public metadata:Lcom/google/api/services/plusi/model/Metadata;

.field public otherGeoLocation:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/GeoLocation;",
            ">;"
        }
    .end annotation
.end field

.field public otherLocation:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 47
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
