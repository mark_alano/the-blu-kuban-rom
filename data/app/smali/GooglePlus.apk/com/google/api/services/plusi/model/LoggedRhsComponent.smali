.class public final Lcom/google/api/services/plusi/model/LoggedRhsComponent;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoggedRhsComponent.java"


# instance fields
.field public barType:Ljava/lang/Integer;

.field public componentType:Lcom/google/api/services/plusi/model/LoggedRhsComponentType;

.field public index:Ljava/lang/Integer;

.field public item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedRhsComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field public neighborInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LoggedRhsComponentType;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionSummaryInfo:Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 70
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
