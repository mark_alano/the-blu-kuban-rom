.class public final Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedRhsComponentJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoggedRhsComponent;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/LoggedRhsComponent;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "index"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "suggestionSummaryInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/LoggedRhsComponentTypeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "neighborInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/LoggedRhsComponentTypeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "componentType"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/LoggedRhsComponentItemJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "item"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "barType"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/LoggedRhsComponent;

    .end local p1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponent;->index:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponent;->suggestionSummaryInfo:Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponent;->neighborInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponent;->componentType:Lcom/google/api/services/plusi/model/LoggedRhsComponentType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponent;->item:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedRhsComponent;->barType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/LoggedRhsComponent;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedRhsComponent;-><init>()V

    return-object v0
.end method
