.class public final Lcom/google/api/services/plusi/model/OutOfBoxInputField;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "OutOfBoxInputField.java"


# instance fields
.field public hasError:Ljava/lang/Boolean;

.field public helpText:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public mandatory:Ljava/lang/Boolean;

.field public type:Ljava/lang/String;

.field public value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

.field public valueOption:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 65
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
