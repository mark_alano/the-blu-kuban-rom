.class public final Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "OutOfBoxInputFieldJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/OutOfBoxInputField;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;->INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "hasError"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "mandatory"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxFieldOptionJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "valueOption"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxFieldValueJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "value"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "label"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "helpText"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "id"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 30
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;->INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    .end local p1
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->valueOption:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->helpText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxInputField;-><init>()V

    return-object v0
.end method
