.class public final Lcom/google/api/services/plusi/model/OzDataSettingsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "OzDataSettingsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/OzDataSettings;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/OzDataSettingsJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/OzDataSettingsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OzDataSettingsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/OzDataSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/OzDataSettingsJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/OzDataSettings;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataPlusActionsSettingsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "plusActionsSettings"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "notificationSettings"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/DataWhoCanInterruptSettingsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "whoCanInterruptSettings"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/DataMobileSettingsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "mobileSettings"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/DataSocialAdsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "socialAds"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/DataHuddleSettingsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "huddleSettings"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/DataGadgetsSettingsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "gadgetsSettings"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/api/services/plusi/model/DataWhoCanNotifySettingsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "whoCanNotifySettings"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/DataWhoCanCommentSettingsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "whoCanCommentSettings"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/OzDataSettingsJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/OzDataSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/OzDataSettingsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/OzDataSettings;

    .end local p1
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->plusActionsSettings:Lcom/google/api/services/plusi/model/DataPlusActionsSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->notificationSettings:Lcom/google/api/services/plusi/model/DataNotificationSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->whoCanInterruptSettings:Lcom/google/api/services/plusi/model/DataWhoCanInterruptSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->mobileSettings:Lcom/google/api/services/plusi/model/DataMobileSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->socialAds:Lcom/google/api/services/plusi/model/DataSocialAds;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->huddleSettings:Lcom/google/api/services/plusi/model/DataHuddleSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->gadgetsSettings:Lcom/google/api/services/plusi/model/DataGadgetsSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->whoCanNotifySettings:Lcom/google/api/services/plusi/model/DataWhoCanNotifySettings;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OzDataSettings;->whoCanCommentSettings:Lcom/google/api/services/plusi/model/DataWhoCanCommentSettings;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/OzDataSettings;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OzDataSettings;-><init>()V

    return-object v0
.end method
