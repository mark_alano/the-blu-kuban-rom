.class public final Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosNameTagApprovalRequest.java"


# instance fields
.field public approve:Ljava/lang/Boolean;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public obfuscatedOwnerId:Ljava/lang/String;

.field public photoId:Ljava/lang/Long;

.field public shapeId:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
