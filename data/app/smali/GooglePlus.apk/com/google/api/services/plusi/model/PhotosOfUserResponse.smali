.class public final Lcom/google/api/services/plusi/model/PhotosOfUserResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosOfUserResponse.java"


# instance fields
.field public approvedPhoto:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public approvedQueryResumeToken:Ljava/lang/String;

.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public errorCode:Ljava/lang/String;

.field public localplusPhoto:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public localplusPhotoOfViewer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public localplusQueryResumeToken:Ljava/lang/String;

.field public localplusViewerPhotosQueryResumeToken:Ljava/lang/String;

.field public suggestedPhoto:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public suggestedQueryResumeToken:Ljava/lang/String;

.field public unapprovedPhoto:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public unapprovedQueryResumeToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 94
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
