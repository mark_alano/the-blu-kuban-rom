.class public final Lcom/google/api/services/plusi/model/ProfileJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ProfileJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/Profile;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ProfileJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ProfileJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ProfileJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ProfileJson;->INSTANCE:Lcom/google/api/services/plusi/model/ProfileJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/Profile;

    const/16 v1, 0x65

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/StringFieldJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "braggingRights"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/LocalUserActivityJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "localUserActivity"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ClassificationJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "classification"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "photoUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/LinksJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "contributorToLinks"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/LinksJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "meLinks"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/ContactMeFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "contactMeShare"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/ProfileStateJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "profileState"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/api/services/plusi/model/NameDisplayOptionsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "nameDisplayOptions"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-class v3, Lcom/google/api/services/plusi/model/ContactInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "workContact"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-class v3, Lcom/google/api/services/plusi/model/LinksJson;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "otherLinks"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-class v3, Lcom/google/api/services/plusi/model/BooleanFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "enableWallWrite"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-class v3, Lcom/google/api/services/plusi/model/ProfileCompletionStatsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "profileCompletionStats"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-class v3, Lcom/google/api/services/plusi/model/ContactMeFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "contactMeChat"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "profileWasAgeRestricted"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "obfuscatedGaiaId"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-class v3, Lcom/google/api/services/plusi/model/LocalEntityInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string v3, "localInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-class v3, Lcom/google/api/services/plusi/model/RelationshipInterestsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string v3, "relationshipInterests"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-class v3, Lcom/google/api/services/plusi/model/StringFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string v3, "occupation"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-class v3, Lcom/google/api/services/plusi/model/ContactInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string v3, "homeContact"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-class v3, Lcom/google/api/services/plusi/model/IntFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string v3, "incomingConnections"

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-class v3, Lcom/google/api/services/plusi/model/LinksJson;

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-string v3, "links"

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string v3, "googleMeEnabled"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-string v3, "profilePageCrawlable"

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string v3, "outOfBoxDismissed"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-class v3, Lcom/google/api/services/plusi/model/EntityInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string v3, "entityInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-class v3, Lcom/google/api/services/plusi/model/ContactMeFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-string v3, "contactMeEmail"

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-class v3, Lcom/google/api/services/plusi/model/StringFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-string v3, "aboutMeHtml"

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-class v3, Lcom/google/api/services/plusi/model/StringFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-string v3, "tagLine"

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-string v3, "profileType"

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const-class v3, Lcom/google/api/services/plusi/model/EmploymentsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x36

    const-string v3, "employments"

    aput-object v3, v1, v2

    const/16 v2, 0x37

    const-class v3, Lcom/google/api/services/plusi/model/NickNameJson;

    aput-object v3, v1, v2

    const/16 v2, 0x38

    const-string v3, "nickName"

    aput-object v3, v1, v2

    const/16 v2, 0x39

    const-string v3, "locationMapUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x3a

    const-string v3, "validAgeRestrictions"

    aput-object v3, v1, v2

    const/16 v2, 0x3b

    const-string v3, "photoIsSilhouette"

    aput-object v3, v1, v2

    const/16 v2, 0x3c

    const-class v3, Lcom/google/api/services/plusi/model/PlusPageInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x3d

    const-string v3, "plusPageInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x3e

    const-string v3, "canonicalProfileUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x3f

    const-class v3, Lcom/google/api/services/plusi/model/VerifiedDomainsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x40

    const-string v3, "verifiedDomains"

    aput-object v3, v1, v2

    const/16 v2, 0x41

    const-class v3, Lcom/google/api/services/plusi/model/RelationshipStatusJson;

    aput-object v3, v1, v2

    const/16 v2, 0x42

    const-string v3, "relationshipStatus"

    aput-object v3, v1, v2

    const/16 v2, 0x43

    const-string v3, "legacyPublicUsername"

    aput-object v3, v1, v2

    const/16 v2, 0x44

    const-class v3, Lcom/google/api/services/plusi/model/EducationsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x45

    const-string v3, "educations"

    aput-object v3, v1, v2

    const/16 v2, 0x46

    const-class v3, Lcom/google/api/services/plusi/model/OtherNamesJson;

    aput-object v3, v1, v2

    const/16 v2, 0x47

    const-string v3, "otherNames"

    aput-object v3, v1, v2

    const/16 v2, 0x48

    const-class v3, Lcom/google/api/services/plusi/model/BirthdayFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x49

    const-string v3, "birthday"

    aput-object v3, v1, v2

    const/16 v2, 0x4a

    const-class v3, Lcom/google/api/services/plusi/model/TabVisibilityJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4b

    const-string v3, "tabVisibility"

    aput-object v3, v1, v2

    const/16 v2, 0x4c

    const-class v3, Lcom/google/api/services/plusi/model/ScrapbookInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4d

    const-string v3, "scrapbookInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x4e

    const-class v3, Lcom/google/api/services/plusi/model/ContactMeFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4f

    const-string v3, "contactMeHangout"

    aput-object v3, v1, v2

    const/16 v2, 0x50

    const-class v3, Lcom/google/api/services/plusi/model/BooleanFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x51

    const-string v3, "enableWallRead"

    aput-object v3, v1, v2

    const/16 v2, 0x52

    const-string v3, "publicUsername"

    aput-object v3, v1, v2

    const/16 v2, 0x53

    const-string v3, "inAbuseiamQueue"

    aput-object v3, v1, v2

    const/16 v2, 0x54

    const-class v3, Lcom/google/api/services/plusi/model/NameJson;

    aput-object v3, v1, v2

    const/16 v2, 0x55

    const-string v3, "name"

    aput-object v3, v1, v2

    const/16 v2, 0x56

    const-class v3, Lcom/google/api/services/plusi/model/SegmentationInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x57

    const-string v3, "segmentationInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x58

    const-class v3, Lcom/google/api/services/plusi/model/LocationsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x59

    const-string v3, "locations"

    aput-object v3, v1, v2

    const/16 v2, 0x5a

    const-class v3, Lcom/google/api/services/plusi/model/GenderJson;

    aput-object v3, v1, v2

    const/16 v2, 0x5b

    const-string v3, "gender"

    aput-object v3, v1, v2

    const/16 v2, 0x5c

    const-class v3, Lcom/google/api/services/plusi/model/StringFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x5d

    const-string v3, "googleAnalyticsWebPropertyId"

    aput-object v3, v1, v2

    const/16 v2, 0x5e

    const-string v3, "profileBirthdayMissing"

    aput-object v3, v1, v2

    const/16 v2, 0x5f

    const-string v3, "showFollowerCounts"

    aput-object v3, v1, v2

    const/16 v2, 0x60

    const-class v3, Lcom/google/api/services/plusi/model/ContactMeFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x61

    const-string v3, "contactMePhone"

    aput-object v3, v1, v2

    const/16 v2, 0x62

    const-class v3, Lcom/google/api/services/plusi/model/ProfilesLinkJson;

    aput-object v3, v1, v2

    const/16 v2, 0x63

    const-string v3, "primaryLink"

    aput-object v3, v1, v2

    const/16 v2, 0x64

    const-string v3, "optedIntoLocal"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 81
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ProfileJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ProfileJson;->INSTANCE:Lcom/google/api/services/plusi/model/ProfileJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/Profile;

    .end local p1
    const/16 v0, 0x3b

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->braggingRights:Lcom/google/api/services/plusi/model/StringField;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->localUserActivity:Lcom/google/api/services/plusi/model/LocalUserActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->classification:Lcom/google/api/services/plusi/model/Classification;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->photoUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->contributorToLinks:Lcom/google/api/services/plusi/model/Links;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->meLinks:Lcom/google/api/services/plusi/model/Links;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->contactMeShare:Lcom/google/api/services/plusi/model/ContactMeField;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->profileState:Lcom/google/api/services/plusi/model/ProfileState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->nameDisplayOptions:Lcom/google/api/services/plusi/model/NameDisplayOptions;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->workContact:Lcom/google/api/services/plusi/model/ContactInfo;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->otherLinks:Lcom/google/api/services/plusi/model/Links;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->enableWallWrite:Lcom/google/api/services/plusi/model/BooleanField;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->profileCompletionStats:Lcom/google/api/services/plusi/model/ProfileCompletionStats;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->contactMeChat:Lcom/google/api/services/plusi/model/ContactMeField;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->profileWasAgeRestricted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->relationshipInterests:Lcom/google/api/services/plusi/model/RelationshipInterests;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->occupation:Lcom/google/api/services/plusi/model/StringField;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->homeContact:Lcom/google/api/services/plusi/model/ContactInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->incomingConnections:Lcom/google/api/services/plusi/model/IntField;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->links:Lcom/google/api/services/plusi/model/Links;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->googleMeEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->profilePageCrawlable:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->outOfBoxDismissed:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->entityInfo:Lcom/google/api/services/plusi/model/EntityInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->contactMeEmail:Lcom/google/api/services/plusi/model/ContactMeField;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->aboutMeHtml:Lcom/google/api/services/plusi/model/StringField;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->tagLine:Lcom/google/api/services/plusi/model/StringField;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->profileType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->employments:Lcom/google/api/services/plusi/model/Employments;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->nickName:Lcom/google/api/services/plusi/model/NickName;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->locationMapUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->validAgeRestrictions:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->photoIsSilhouette:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->plusPageInfo:Lcom/google/api/services/plusi/model/PlusPageInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->canonicalProfileUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->verifiedDomains:Lcom/google/api/services/plusi/model/VerifiedDomains;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->relationshipStatus:Lcom/google/api/services/plusi/model/RelationshipStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->legacyPublicUsername:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->educations:Lcom/google/api/services/plusi/model/Educations;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->otherNames:Lcom/google/api/services/plusi/model/OtherNames;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->birthday:Lcom/google/api/services/plusi/model/BirthdayField;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->tabVisibility:Lcom/google/api/services/plusi/model/TabVisibility;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->contactMeHangout:Lcom/google/api/services/plusi/model/ContactMeField;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->enableWallRead:Lcom/google/api/services/plusi/model/BooleanField;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->publicUsername:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->inAbuseiamQueue:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->name:Lcom/google/api/services/plusi/model/Name;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->segmentationInfo:Lcom/google/api/services/plusi/model/SegmentationInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->locations:Lcom/google/api/services/plusi/model/Locations;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->gender:Lcom/google/api/services/plusi/model/Gender;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->googleAnalyticsWebPropertyId:Lcom/google/api/services/plusi/model/StringField;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->profileBirthdayMissing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->showFollowerCounts:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->contactMePhone:Lcom/google/api/services/plusi/model/ContactMeField;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->primaryLink:Lcom/google/api/services/plusi/model/ProfilesLink;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Profile;->optedIntoLocal:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/Profile;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Profile;-><init>()V

    return-object v0
.end method
