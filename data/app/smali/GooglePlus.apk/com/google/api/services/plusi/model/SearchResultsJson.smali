.class public final Lcom/google/api/services/plusi/model/SearchResultsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "SearchResultsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/SearchResults;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/SearchResultsJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/SearchResultsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchResultsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/SearchResultsJson;->INSTANCE:Lcom/google/api/services/plusi/model/SearchResultsJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/SearchResults;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "status"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/SquareResultsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "squareResults"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "decorationMode"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ActivityResultsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "activityResults"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/PeopleResultsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "peopleResults"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "debugInfoHtml"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/SpellingSuggestionsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "spellingSuggestions"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/SearchResultsJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/SearchResultsJson;->INSTANCE:Lcom/google/api/services/plusi/model/SearchResultsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/SearchResults;

    .end local p1
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchResults;->status:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchResults;->squareResults:Lcom/google/api/services/plusi/model/SquareResults;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchResults;->decorationMode:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchResults;->peopleResults:Lcom/google/api/services/plusi/model/PeopleResults;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchResults;->debugInfoHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchResults;->spellingSuggestions:Lcom/google/api/services/plusi/model/SpellingSuggestions;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/SearchResults;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchResults;-><init>()V

    return-object v0
.end method
