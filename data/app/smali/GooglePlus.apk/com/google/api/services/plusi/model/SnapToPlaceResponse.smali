.class public final Lcom/google/api/services/plusi/model/SnapToPlaceResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SnapToPlaceResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public cityLocation:Lcom/google/api/services/plusi/model/LocationResult;

.field public localPlace:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/LocationResult;",
            ">;"
        }
    .end annotation
.end field

.field public preciseLocation:Lcom/google/api/services/plusi/model/LocationResult;

.field public userIsAtFirstPlace:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 47
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
