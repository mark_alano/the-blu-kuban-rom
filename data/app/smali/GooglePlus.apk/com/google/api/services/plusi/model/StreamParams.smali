.class public final Lcom/google/api/services/plusi/model/StreamParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "StreamParams.java"


# instance fields
.field public activitiesOldestTimestampUsec:Ljava/lang/Long;

.field public channelOrigin:Ljava/lang/String;

.field public collapserType:Ljava/lang/String;

.field public contentFormat:Ljava/lang/String;

.field public disableAbuseFiltering:Ljava/lang/Boolean;

.field public expandSharebox:Ljava/lang/Boolean;

.field public fieldInclusion:Ljava/lang/String;

.field public fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

.field public filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/StreamParamsFilter;",
            ">;"
        }
    .end annotation
.end field

.field public focusGroupId:Ljava/lang/String;

.field public gaiaGroupOid:Ljava/lang/String;

.field public initialOperation:Ljava/lang/String;

.field public interest:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Interest;",
            ">;"
        }
    .end annotation
.end field

.field public interestParams:Lcom/google/api/services/plusi/model/InterestParams;

.field public maxComments:Ljava/lang/Integer;

.field public maxNumImages:Ljava/lang/Integer;

.field public maxNumUpdates:Ljava/lang/Integer;

.field public openSocialDomain:Ljava/lang/String;

.field public perspectiveUserId:Ljava/lang/String;

.field public productionParams:Lcom/google/api/services/plusi/model/ProductionStreamParams;

.field public productionStreamOid:Ljava/lang/String;

.field public searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

.field public sort:Ljava/lang/String;

.field public streamItemFilter:Lcom/google/api/services/plusi/model/StreamItemFilter;

.field public updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

.field public updateMixinFilter:Lcom/google/api/services/plusi/model/UpdateMixinFilter;

.field public viewType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 75
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
