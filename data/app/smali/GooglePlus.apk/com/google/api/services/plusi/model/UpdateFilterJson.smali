.class public final Lcom/google/api/services/plusi/model/UpdateFilterJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "UpdateFilterJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/UpdateFilter;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/UpdateFilterJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/UpdateFilterJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/UpdateFilterJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/UpdateFilterJson;->INSTANCE:Lcom/google/api/services/plusi/model/UpdateFilterJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/UpdateFilter;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "includeNamespace"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/UpdateFilterJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/UpdateFilterJson;->INSTANCE:Lcom/google/api/services/plusi/model/UpdateFilterJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/UpdateFilter;

    .end local p1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/UpdateFilter;->includeNamespace:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/UpdateFilter;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/UpdateFilter;-><init>()V

    return-object v0
.end method
