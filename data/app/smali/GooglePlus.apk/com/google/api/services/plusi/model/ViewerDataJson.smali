.class public final Lcom/google/api/services/plusi/model/ViewerDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ViewerDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ViewerData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ViewerDataJson;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/api/services/plusi/model/ViewerDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ViewerDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ViewerDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ViewerDataJson;

    return-void
.end method

.method private constructor <init>()V
    .registers 5

    .prologue
    .line 21
    const-class v0, Lcom/google/api/services/plusi/model/ViewerData;

    const/16 v1, 0x27

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "siloPolicy"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "actualUserObfuscatedId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "birthdayHasYear"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/AllFocusGroupsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "allFocusGroups"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "fromAddress"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "obfuscatedGaiaId"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "profileHasAcls"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "isDefaultSortLatest"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "primaryUsername"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "sessionId"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "outgoingAnyCircleCount"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "selectedViewFilter"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "isGmailUser"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "isPlusOneUser"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "isFirstTimeResharePromoDismissed"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-class v3, Lcom/google/api/services/plusi/model/ProfileJson;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "profile"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "isGoogleMeUser"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "ageInYears"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "hasEsMobile"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "geographicLocation"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "isOrkutUser"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "emailAddress"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-class v3, Lcom/google/api/services/plusi/model/LinksJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "externalLinks"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "hasVoiceAccountDeprecated"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-class v3, Lcom/google/api/services/plusi/model/SegmentationInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "segmentationInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-class v3, Lcom/google/api/services/plusi/model/VisibleCirclesInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "visibleCirclesInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string v3, "isBuzzUser"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string v3, "contactMatchingOptIn"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-class v3, Lcom/google/api/services/plusi/model/NameFormatDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string v3, "nameFormatData"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string v3, "isProfilePublic"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string v3, "navShowChatPromo"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string v3, "navSelectedPath"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 55
    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ViewerDataJson;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/api/services/plusi/model/ViewerDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ViewerDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 5
    .parameter "x0"

    .prologue
    .line 11
    check-cast p1, Lcom/google/api/services/plusi/model/ViewerData;

    .end local p1
    const/16 v0, 0x21

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->siloPolicy:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->actualUserObfuscatedId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->birthdayHasYear:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->allFocusGroups:Lcom/google/api/services/plusi/model/AllFocusGroups;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->locale:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->fromAddress:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->profileHasAcls:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isDefaultSortLatest:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->primaryUsername:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->sessionId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->outgoingAnyCircleCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->selectedViewFilter:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isGmailUser:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isPlusOneUser:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isFirstTimeResharePromoDismissed:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->profile:Lcom/google/api/services/plusi/model/Profile;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isGoogleMeUser:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->ageInYears:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->hasEsMobile:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->geographicLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isOrkutUser:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->emailAddress:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->externalLinks:Lcom/google/api/services/plusi/model/Links;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->hasVoiceAccountDeprecated:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->segmentationInfo:Lcom/google/api/services/plusi/model/SegmentationInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->visibleCirclesInfo:Lcom/google/api/services/plusi/model/VisibleCirclesInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isBuzzUser:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->contactMatchingOptIn:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->nameFormatData:Lcom/google/api/services/plusi/model/NameFormatData;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->isProfilePublic:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->navShowChatPromo:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerData;->navSelectedPath:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/api/services/plusi/model/ViewerData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ViewerData;-><init>()V

    return-object v0
.end method
