.class public final Lcom/google/api/services/plusi/model/VisibleCirclesInfo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "VisibleCirclesInfo.java"


# instance fields
.field public allCirclesVisible:Ljava/lang/Boolean;

.field public displayIncomingEdges:Ljava/lang/Boolean;

.field public networkVisibility:Ljava/lang/String;

.field public personalCircle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PersonalCircle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 58
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
