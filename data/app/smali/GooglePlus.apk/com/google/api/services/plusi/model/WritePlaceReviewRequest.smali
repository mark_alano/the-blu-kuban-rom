.class public final Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "WritePlaceReviewRequest.java"


# instance fields
.field public abuseSignals:Lcom/google/api/services/plusi/model/AbuseSignals;

.field public cid:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public includeSuggestedPlaces:Ljava/lang/Boolean;

.field public oldZagatAspectRatings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;",
            ">;"
        }
    .end annotation
.end field

.field public price:Lcom/google/api/services/plusi/model/PriceProto;

.field public priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

.field public reviewText:Ljava/lang/String;

.field public source:Ljava/lang/String;

.field public userCountryCode:Ljava/lang/String;

.field public userLanguage:Ljava/lang/String;

.field public zagatAspectRatings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    .prologue
    .line 61
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
