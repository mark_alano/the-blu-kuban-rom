.class public final Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Webclient.java"

# interfaces
.implements Lcom/google/wireless/contacts/proto/Webclient$ExternalEventOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;",
        "Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;",
        ">;",
        "Lcom/google/wireless/contacts/proto/Webclient$ExternalEventOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

.field private startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 307
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    .line 350
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    .line 190
    return-void
.end method

.method static synthetic access$100()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 1

    .prologue
    .line 184
    new-instance v0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    invoke-direct {v0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;-><init>()V

    return-object v0
.end method

.method private buildPartial()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;
    .registers 6

    .prologue
    .line 235
    new-instance v1, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;-><init>(Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;B)V

    .line 236
    .local v1, result:Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 237
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 238
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    .line 239
    const/4 v2, 0x1

    .line 241
    :cond_f
    iget-object v3, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    #setter for: Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;
    invoke-static {v1, v3}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->access$302(Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;)Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    .line 242
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1b

    .line 243
    or-int/lit8 v2, v2, 0x2

    .line 245
    :cond_1b
    iget-object v3, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    #setter for: Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;
    invoke-static {v1, v3}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->access$402(Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;)Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    .line 246
    #setter for: Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->access$502(Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;I)I

    .line 247
    return-object v1
.end method

.method private clone()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 3

    .prologue
    .line 209
    new-instance v0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    invoke-direct {v0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->buildPartial()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->mergeFrom(Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 270
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 271
    .local v1, tag:I
    sparse-switch v1, :sswitch_data_46

    .line 276
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 278
    :sswitch_d
    return-object p0

    .line 283
    :sswitch_e
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;->newBuilder()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;

    move-result-object v0

    .line 284
    .local v0, subBuilder:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->hasNewContactsFetchedEvent()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 285
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->getNewContactsFetchedEvent()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;->mergeFrom(Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;)Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;

    .line 287
    :cond_1f
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 288
    invoke-virtual {v0}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;->buildPartial()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->setNewContactsFetchedEvent(Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    goto :goto_0

    .line 292
    .end local v0           #subBuilder:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;
    :sswitch_2a
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;->newBuilder()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;

    move-result-object v0

    .line 293
    .local v0, subBuilder:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->hasStartFetchEvent()Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 294
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->getStartFetchEvent()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;->mergeFrom(Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;)Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;

    .line 296
    :cond_3b
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 297
    invoke-virtual {v0}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;->buildPartial()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->setStartFetchEvent(Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    goto :goto_0

    .line 271
    :sswitch_data_46
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_2a
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 3

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->buildPartial()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v0

    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->buildPartial()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->clear()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->clear()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 2

    .prologue
    .line 200
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 201
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    .line 202
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 203
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    .line 204
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 205
    return-object p0
.end method

.method public final clearNewContactsFetchedEvent()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 2

    .prologue
    .line 343
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    .line 345
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 346
    return-object p0
.end method

.method public final clearStartFetchEvent()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 2

    .prologue
    .line 386
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    .line 388
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 389
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->clone()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->clone()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->clone()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->getDefaultInstanceForType()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->getDefaultInstanceForType()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;
    .registers 2

    .prologue
    .line 213
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    move-result-object v0

    return-object v0
.end method

.method public final getNewContactsFetchedEvent()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;
    .registers 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    return-object v0
.end method

.method public final getStartFetchEvent()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;
    .registers 2

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    return-object v0
.end method

.method public final hasNewContactsFetchedEvent()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 309
    iget v1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasStartFetchEvent()Z
    .registers 3

    .prologue
    .line 352
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 262
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 184
    check-cast p1, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->mergeFrom(Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 5
    .parameter "other"

    .prologue
    .line 251
    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 258
    :cond_6
    :goto_6
    return-object p0

    .line 252
    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->hasNewContactsFetchedEvent()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 253
    invoke-virtual {p1}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->getNewContactsFetchedEvent()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_66

    iget-object v1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v2

    if-eq v1, v2, :cond_66

    iget-object v1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    invoke-static {v1}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;->newBuilder(Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;)Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;->mergeFrom(Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;)Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;->buildPartial()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    :goto_30
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 255
    :cond_36
    invoke-virtual {p1}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->hasStartFetchEvent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 256
    invoke-virtual {p1}, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent;->getStartFetchEvent()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_69

    iget-object v1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    invoke-static {}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;->getDefaultInstance()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v2

    if-eq v1, v2, :cond_69

    iget-object v1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    invoke-static {v1}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;->newBuilder(Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;)Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;->mergeFrom(Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;)Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;->buildPartial()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    :goto_5f
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    goto :goto_6

    .line 253
    :cond_66
    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    goto :goto_30

    .line 256
    :cond_69
    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    goto :goto_5f
.end method

.method public final setNewContactsFetchedEvent(Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 325
    invoke-virtual {p1}, Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent$Builder;->build()Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    .line 327
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 328
    return-object p0
.end method

.method public final setNewContactsFetchedEvent(Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 315
    if-nez p1, :cond_8

    .line 316
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 318
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->newContactsFetchedEvent_:Lcom/google/wireless/contacts/proto/Webclient$NewContactsFetchedEvent;

    .line 320
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 321
    return-object p0
.end method

.method public final setStartFetchEvent(Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 368
    invoke-virtual {p1}, Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent$Builder;->build()Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    .line 370
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 371
    return-object p0
.end method

.method public final setStartFetchEvent(Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;)Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 358
    if-nez p1, :cond_8

    .line 359
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 361
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->startFetchEvent_:Lcom/google/wireless/contacts/proto/Webclient$StartFetchEvent;

    .line 363
    iget v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/contacts/proto/Webclient$ExternalEvent$Builder;->bitField0_:I

    .line 364
    return-object p0
.end method
