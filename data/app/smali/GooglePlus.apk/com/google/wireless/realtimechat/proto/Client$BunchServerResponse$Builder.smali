.class public final Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponseOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

.field private conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

.field private conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

.field private conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

.field private conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

.field private conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

.field private conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

.field private deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

.field private error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

.field private eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

.field private eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

.field private globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

.field private hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

.field private hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

.field private hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

.field private hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

.field private hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

.field private inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

.field private leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

.field private pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

.field private presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

.field private receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

.field private replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

.field private requestClientId_:Ljava/lang/Object;

.field private setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

.field private suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

.field private tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

.field private typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

.field private userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

.field private userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 55494
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 56112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    .line 56148
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    .line 56191
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    .line 56234
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    .line 56277
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    .line 56320
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    .line 56363
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    .line 56406
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    .line 56449
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    .line 56492
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    .line 56535
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    .line 56578
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Error;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    .line 56621
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    .line 56664
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    .line 56707
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    .line 56750
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    .line 56793
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    .line 56836
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    .line 56879
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    .line 56922
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    .line 56965
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    .line 57008
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    .line 57051
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    .line 57094
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    .line 57137
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    .line 57180
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    .line 57223
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    .line 57266
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    .line 57309
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    .line 57352
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    .line 55495
    return-void
.end method

.method static synthetic access$74900()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 1

    .prologue
    .line 55489
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 55570
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55827
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 55828
    .local v1, tag:I
    sparse-switch v1, :sswitch_data_362

    .line 55833
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 55835
    :sswitch_d
    return-object p0

    .line 55840
    :sswitch_e
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55841
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    goto :goto_0

    .line 55845
    :sswitch_1b
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;

    move-result-object v0

    .line 55846
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasChatMessageResponse()Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 55847
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getChatMessageResponse()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;

    .line 55849
    :cond_2c
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55850
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setChatMessageResponse(Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto :goto_0

    .line 55854
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;
    :sswitch_37
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;

    move-result-object v0

    .line 55855
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasPresenceResponse()Z

    move-result v2

    if-eqz v2, :cond_48

    .line 55856
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getPresenceResponse()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;

    .line 55858
    :cond_48
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55859
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setPresenceResponse(Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto :goto_0

    .line 55863
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;
    :sswitch_53
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;

    move-result-object v0

    .line 55864
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasTypingResponse()Z

    move-result v2

    if-eqz v2, :cond_64

    .line 55865
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getTypingResponse()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;)Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;

    .line 55867
    :cond_64
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55868
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setTypingResponse(Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto :goto_0

    .line 55872
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;
    :sswitch_6f
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;

    move-result-object v0

    .line 55873
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasReceiptResponse()Z

    move-result v2

    if-eqz v2, :cond_80

    .line 55874
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getReceiptResponse()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;

    .line 55876
    :cond_80
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55877
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setReceiptResponse(Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55881
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;
    :sswitch_8c
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    move-result-object v0

    .line 55882
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasConversationResponse()Z

    move-result v2

    if-eqz v2, :cond_9d

    .line 55883
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    .line 55885
    :cond_9d
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55886
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setConversationResponse(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55890
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;
    :sswitch_a9
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    .line 55891
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasEventSteamResponse()Z

    move-result v2

    if-eqz v2, :cond_ba

    .line 55892
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getEventSteamResponse()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    .line 55894
    :cond_ba
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55895
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setEventSteamResponse(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55899
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    :sswitch_c6
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;

    move-result-object v0

    .line 55900
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasDeviceRegistrationResponse()Z

    move-result v2

    if-eqz v2, :cond_d7

    .line 55901
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getDeviceRegistrationResponse()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;

    .line 55903
    :cond_d7
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55904
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setDeviceRegistrationResponse(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55908
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;
    :sswitch_e3
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;

    move-result-object v0

    .line 55909
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasPingResponse()Z

    move-result v2

    if-eqz v2, :cond_f4

    .line 55910
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getPingResponse()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$PingResponse;)Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;

    .line 55912
    :cond_f4
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55913
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setPingResponse(Lcom/google/wireless/realtimechat/proto/Client$PingResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55917
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;
    :sswitch_100
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;

    move-result-object v0

    .line 55918
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasConversationListResponse()Z

    move-result v2

    if-eqz v2, :cond_111

    .line 55919
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getConversationListResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;

    .line 55921
    :cond_111
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55922
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setConversationListResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55926
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;
    :sswitch_11d
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;

    move-result-object v0

    .line 55927
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasUserCreationResponse()Z

    move-result v2

    if-eqz v2, :cond_12e

    .line 55928
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getUserCreationResponse()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;

    .line 55930
    :cond_12e
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55931
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setUserCreationResponse(Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55935
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;
    :sswitch_13a
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Error;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;

    move-result-object v0

    .line 55936
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasError()Z

    move-result v2

    if-eqz v2, :cond_14b

    .line 55937
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getError()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Error;)Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;

    .line 55939
    :cond_14b
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55940
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setError(Lcom/google/wireless/realtimechat/proto/Client$Error;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55944
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;
    :sswitch_157
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;

    move-result-object v0

    .line 55945
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasLeaveConversationResponse()Z

    move-result v2

    if-eqz v2, :cond_168

    .line 55946
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getLeaveConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;

    .line 55948
    :cond_168
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55949
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setLeaveConversationResponse(Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55953
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;
    :sswitch_174
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;

    move-result-object v0

    .line 55954
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasConversationRenameResponse()Z

    move-result v2

    if-eqz v2, :cond_185

    .line 55955
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getConversationRenameResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;

    .line 55957
    :cond_185
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55958
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setConversationRenameResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55962
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;
    :sswitch_191
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;

    move-result-object v0

    .line 55963
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasTileEventResponse()Z

    move-result v2

    if-eqz v2, :cond_1a2

    .line 55964
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getTileEventResponse()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;)Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;

    .line 55966
    :cond_1a2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55967
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setTileEventResponse(Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55971
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;
    :sswitch_1ae
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;

    move-result-object v0

    .line 55972
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasConversationPreferenceResponse()Z

    move-result v2

    if-eqz v2, :cond_1bf

    .line 55973
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getConversationPreferenceResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;

    .line 55975
    :cond_1bf
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55976
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setConversationPreferenceResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55980
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;
    :sswitch_1cb
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    .line 55981
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasInviteResponse()Z

    move-result v2

    if-eqz v2, :cond_1dc

    .line 55982
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    .line 55984
    :cond_1dc
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55985
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55989
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    :sswitch_1e8
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;

    move-result-object v0

    .line 55990
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasReplyToInviteResponse()Z

    move-result v2

    if-eqz v2, :cond_1f9

    .line 55991
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getReplyToInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;

    .line 55993
    :cond_1f9
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55994
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setReplyToInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55998
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;
    :sswitch_205
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;

    move-result-object v0

    .line 55999
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasSetAclsResponse()Z

    move-result v2

    if-eqz v2, :cond_216

    .line 56000
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getSetAclsResponse()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;

    .line 56002
    :cond_216
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56003
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setSetAclsResponse(Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56007
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;
    :sswitch_222
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    .line 56008
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasUserInfoResponse()Z

    move-result v2

    if-eqz v2, :cond_233

    .line 56009
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getUserInfoResponse()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    .line 56011
    :cond_233
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56012
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setUserInfoResponse(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56016
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;
    :sswitch_23f
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    move-result-object v0

    .line 56017
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasConversationSearchResponse()Z

    move-result v2

    if-eqz v2, :cond_250

    .line 56018
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getConversationSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    .line 56020
    :cond_250
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56021
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setConversationSearchResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56025
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;
    :sswitch_25c
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;

    move-result-object v0

    .line 56026
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasEventSearchResponse()Z

    move-result v2

    if-eqz v2, :cond_26d

    .line 56027
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getEventSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;

    .line 56029
    :cond_26d
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56030
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setEventSearchResponse(Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56034
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;
    :sswitch_279
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    .line 56035
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasSuggestionsResponse()Z

    move-result v2

    if-eqz v2, :cond_28a

    .line 56036
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    .line 56038
    :cond_28a
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56039
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setSuggestionsResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56043
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    :sswitch_296
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;

    move-result-object v0

    .line 56044
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasGlobalConversationPreferencesResponse()Z

    move-result v2

    if-eqz v2, :cond_2a7

    .line 56045
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getGlobalConversationPreferencesResponse()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;)Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;

    .line 56047
    :cond_2a7
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56048
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setGlobalConversationPreferencesResponse(Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56052
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;
    :sswitch_2b3
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;

    move-result-object v0

    .line 56053
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasConversationJoinResponse()Z

    move-result v2

    if-eqz v2, :cond_2c4

    .line 56054
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getConversationJoinResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;

    .line 56056
    :cond_2c4
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56057
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setConversationJoinResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56061
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;
    :sswitch_2d0
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    move-result-object v0

    .line 56062
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasHangoutInviteResponse()Z

    move-result v2

    if-eqz v2, :cond_2e1

    .line 56063
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getHangoutInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    .line 56065
    :cond_2e1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56066
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setHangoutInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56070
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;
    :sswitch_2ed
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;

    move-result-object v0

    .line 56071
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasHangoutInviteKeepAliveResponse()Z

    move-result v2

    if-eqz v2, :cond_2fe

    .line 56072
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getHangoutInviteKeepAliveResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;

    .line 56074
    :cond_2fe
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56075
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setHangoutInviteKeepAliveResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56079
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;
    :sswitch_30a
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;

    move-result-object v0

    .line 56080
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasHangoutInviteReplyResponse()Z

    move-result v2

    if-eqz v2, :cond_31b

    .line 56081
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getHangoutInviteReplyResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;

    .line 56083
    :cond_31b
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56084
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setHangoutInviteReplyResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56088
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;
    :sswitch_327
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;

    move-result-object v0

    .line 56089
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasHangoutInviteFinishResponse()Z

    move-result v2

    if-eqz v2, :cond_338

    .line 56090
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getHangoutInviteFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;

    .line 56092
    :cond_338
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56093
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setHangoutInviteFinishResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 56097
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;
    :sswitch_344
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;

    move-result-object v0

    .line 56098
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hasHangoutRingFinishResponse()Z

    move-result v2

    if-eqz v2, :cond_355

    .line 56099
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getHangoutRingFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;

    .line 56101
    :cond_355
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56102
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setHangoutRingFinishResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    goto/16 :goto_0

    .line 55828
    nop

    :sswitch_data_362
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_1b
        0x1a -> :sswitch_37
        0x22 -> :sswitch_53
        0x2a -> :sswitch_6f
        0x32 -> :sswitch_8c
        0x3a -> :sswitch_a9
        0x42 -> :sswitch_c6
        0x4a -> :sswitch_e3
        0x52 -> :sswitch_100
        0x5a -> :sswitch_11d
        0x62 -> :sswitch_13a
        0x6a -> :sswitch_157
        0x72 -> :sswitch_174
        0x7a -> :sswitch_191
        0x82 -> :sswitch_1ae
        0x8a -> :sswitch_1cb
        0x92 -> :sswitch_1e8
        0x9a -> :sswitch_205
        0xa2 -> :sswitch_222
        0xaa -> :sswitch_23f
        0xb2 -> :sswitch_25c
        0xba -> :sswitch_279
        0xc2 -> :sswitch_296
        0xca -> :sswitch_2b3
        0xd2 -> :sswitch_2d0
        0xda -> :sswitch_2ed
        0xe2 -> :sswitch_30a
        0xea -> :sswitch_327
        0xf2 -> :sswitch_344
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 55489
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .registers 3

    .prologue
    .line 55578
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    .line 55579
    .local v0, result:Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    .line 55580
    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    .line 55582
    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 55489
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .registers 11

    .prologue
    const/high16 v9, 0x8

    const/high16 v8, 0x4

    const/high16 v7, 0x2

    const/high16 v6, 0x1

    const v5, 0x8000

    .line 55596
    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;B)V

    .line 55597
    .local v1, result:Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55598
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 55599
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1a

    .line 55600
    const/4 v2, 0x1

    .line 55602
    :cond_1a
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->requestClientId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75102(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55603
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_26

    .line 55604
    or-int/lit8 v2, v2, 0x2

    .line 55606
    :cond_26
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75202(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    .line 55607
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_32

    .line 55608
    or-int/lit8 v2, v2, 0x4

    .line 55610
    :cond_32
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75302(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    .line 55611
    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3f

    .line 55612
    or-int/lit8 v2, v2, 0x8

    .line 55614
    :cond_3f
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75402(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;)Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    .line 55615
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4c

    .line 55616
    or-int/lit8 v2, v2, 0x10

    .line 55618
    :cond_4c
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75502(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    .line 55619
    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_59

    .line 55620
    or-int/lit8 v2, v2, 0x20

    .line 55622
    :cond_59
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75602(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    .line 55623
    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_66

    .line 55624
    or-int/lit8 v2, v2, 0x40

    .line 55626
    :cond_66
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75702(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    .line 55627
    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_73

    .line 55628
    or-int/lit16 v2, v2, 0x80

    .line 55630
    :cond_73
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75802(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    .line 55631
    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_80

    .line 55632
    or-int/lit16 v2, v2, 0x100

    .line 55634
    :cond_80
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$75902(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$PingResponse;)Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    .line 55635
    and-int/lit16 v3, v0, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8d

    .line 55636
    or-int/lit16 v2, v2, 0x200

    .line 55638
    :cond_8d
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76002(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    .line 55639
    and-int/lit16 v3, v0, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_9a

    .line 55640
    or-int/lit16 v2, v2, 0x400

    .line 55642
    :cond_9a
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76102(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    .line 55643
    and-int/lit16 v3, v0, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_a7

    .line 55644
    or-int/lit16 v2, v2, 0x800

    .line 55646
    :cond_a7
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76202(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$Error;)Lcom/google/wireless/realtimechat/proto/Client$Error;

    .line 55647
    and-int/lit16 v3, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_b4

    .line 55648
    or-int/lit16 v2, v2, 0x1000

    .line 55650
    :cond_b4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76302(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    .line 55651
    and-int/lit16 v3, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_c1

    .line 55652
    or-int/lit16 v2, v2, 0x2000

    .line 55654
    :cond_c1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76402(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    .line 55655
    and-int/lit16 v3, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_ce

    .line 55656
    or-int/lit16 v2, v2, 0x4000

    .line 55658
    :cond_ce
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76502(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;)Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    .line 55659
    and-int v3, v0, v5

    if-ne v3, v5, :cond_d8

    .line 55660
    or-int/2addr v2, v5

    .line 55662
    :cond_d8
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76602(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    .line 55663
    and-int v3, v0, v6

    if-ne v3, v6, :cond_e2

    .line 55664
    or-int/2addr v2, v6

    .line 55666
    :cond_e2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76702(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    .line 55667
    and-int v3, v0, v7

    if-ne v3, v7, :cond_ec

    .line 55668
    or-int/2addr v2, v7

    .line 55670
    :cond_ec
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76802(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    .line 55671
    and-int v3, v0, v8

    if-ne v3, v8, :cond_f6

    .line 55672
    or-int/2addr v2, v8

    .line 55674
    :cond_f6
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$76902(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    .line 55675
    and-int v3, v0, v9

    if-ne v3, v9, :cond_100

    .line 55676
    or-int/2addr v2, v9

    .line 55678
    :cond_100
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77002(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    .line 55679
    const/high16 v3, 0x10

    and-int/2addr v3, v0

    const/high16 v4, 0x10

    if-ne v3, v4, :cond_10f

    .line 55680
    const/high16 v3, 0x10

    or-int/2addr v2, v3

    .line 55682
    :cond_10f
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77102(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    .line 55683
    const/high16 v3, 0x20

    and-int/2addr v3, v0

    const/high16 v4, 0x20

    if-ne v3, v4, :cond_11e

    .line 55684
    const/high16 v3, 0x20

    or-int/2addr v2, v3

    .line 55686
    :cond_11e
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77202(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    .line 55687
    const/high16 v3, 0x40

    and-int/2addr v3, v0

    const/high16 v4, 0x40

    if-ne v3, v4, :cond_12d

    .line 55688
    const/high16 v3, 0x40

    or-int/2addr v2, v3

    .line 55690
    :cond_12d
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77302(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    .line 55691
    const/high16 v3, 0x80

    and-int/2addr v3, v0

    const/high16 v4, 0x80

    if-ne v3, v4, :cond_13c

    .line 55692
    const/high16 v3, 0x80

    or-int/2addr v2, v3

    .line 55694
    :cond_13c
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77402(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;)Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    .line 55695
    const/high16 v3, 0x100

    and-int/2addr v3, v0

    const/high16 v4, 0x100

    if-ne v3, v4, :cond_14b

    .line 55696
    const/high16 v3, 0x100

    or-int/2addr v2, v3

    .line 55698
    :cond_14b
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77502(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    .line 55699
    const/high16 v3, 0x200

    and-int/2addr v3, v0

    const/high16 v4, 0x200

    if-ne v3, v4, :cond_15a

    .line 55700
    const/high16 v3, 0x200

    or-int/2addr v2, v3

    .line 55702
    :cond_15a
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77602(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    .line 55703
    const/high16 v3, 0x400

    and-int/2addr v3, v0

    const/high16 v4, 0x400

    if-ne v3, v4, :cond_169

    .line 55704
    const/high16 v3, 0x400

    or-int/2addr v2, v3

    .line 55706
    :cond_169
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77702(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    .line 55707
    const/high16 v3, 0x800

    and-int/2addr v3, v0

    const/high16 v4, 0x800

    if-ne v3, v4, :cond_178

    .line 55708
    const/high16 v3, 0x800

    or-int/2addr v2, v3

    .line 55710
    :cond_178
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77802(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    .line 55711
    const/high16 v3, 0x1000

    and-int/2addr v3, v0

    const/high16 v4, 0x1000

    if-ne v3, v4, :cond_187

    .line 55712
    const/high16 v3, 0x1000

    or-int/2addr v2, v3

    .line 55714
    :cond_187
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$77902(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    .line 55715
    const/high16 v3, 0x2000

    and-int/2addr v3, v0

    const/high16 v4, 0x2000

    if-ne v3, v4, :cond_196

    .line 55716
    const/high16 v3, 0x2000

    or-int/2addr v2, v3

    .line 55718
    :cond_196
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$78002(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    .line 55719
    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->access$78102(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;I)I

    .line 55720
    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 55489
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 55489
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 55505
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 55506
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    .line 55507
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55508
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    .line 55509
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55510
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    .line 55511
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55512
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    .line 55513
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55514
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    .line 55515
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55516
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    .line 55517
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55518
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    .line 55519
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55520
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    .line 55521
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55522
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    .line 55523
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55524
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    .line 55525
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55526
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    .line 55527
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55528
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Error;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    .line 55529
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55530
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    .line 55531
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55532
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    .line 55533
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55534
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    .line 55535
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55536
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    .line 55537
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55538
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    .line 55539
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55540
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    .line 55541
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55542
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    .line 55543
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55544
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    .line 55545
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55546
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    .line 55547
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55548
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    .line 55549
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55550
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    .line 55551
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55552
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    .line 55553
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55554
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    .line 55555
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55556
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    .line 55557
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55558
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    .line 55559
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x4000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55560
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    .line 55561
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x8000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55562
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    .line 55563
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x10000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55564
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    .line 55565
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x20000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55566
    return-object p0
.end method

.method public final clearChatMessageResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56184
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    .line 56186
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56187
    return-object p0
.end method

.method public final clearConversationJoinResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57173
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    .line 57175
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57176
    return-object p0
.end method

.method public final clearConversationListResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56528
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    .line 56530
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56531
    return-object p0
.end method

.method public final clearConversationPreferenceResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 56786
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    .line 56788
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56789
    return-object p0
.end method

.method public final clearConversationRenameResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56700
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    .line 56702
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56703
    return-object p0
.end method

.method public final clearConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56356
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    .line 56358
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56359
    return-object p0
.end method

.method public final clearConversationSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57001
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    .line 57003
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57004
    return-object p0
.end method

.method public final clearDeviceRegistrationResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56442
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    .line 56444
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56445
    return-object p0
.end method

.method public final clearError()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56614
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Error;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    .line 56616
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56617
    return-object p0
.end method

.method public final clearEventSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57044
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    .line 57046
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57047
    return-object p0
.end method

.method public final clearEventSteamResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56399
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    .line 56401
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56402
    return-object p0
.end method

.method public final clearGlobalConversationPreferencesResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57130
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    .line 57132
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57133
    return-object p0
.end method

.method public final clearHangoutInviteFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57345
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    .line 57347
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x10000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57348
    return-object p0
.end method

.method public final clearHangoutInviteKeepAliveResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57259
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    .line 57261
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x4000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57262
    return-object p0
.end method

.method public final clearHangoutInviteReplyResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57302
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    .line 57304
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x8000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57305
    return-object p0
.end method

.method public final clearHangoutInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57216
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    .line 57218
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57219
    return-object p0
.end method

.method public final clearHangoutRingFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57388
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    .line 57390
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x20000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57391
    return-object p0
.end method

.method public final clearInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 56829
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    .line 56831
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56832
    return-object p0
.end method

.method public final clearLeaveConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56657
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    .line 56659
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56660
    return-object p0
.end method

.method public final clearPingResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56485
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    .line 56487
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56488
    return-object p0
.end method

.method public final clearPresenceResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56227
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    .line 56229
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56230
    return-object p0
.end method

.method public final clearReceiptResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56313
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    .line 56315
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56316
    return-object p0
.end method

.method public final clearReplyToInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 56872
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    .line 56874
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56875
    return-object p0
.end method

.method public final clearRequestClientId()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56136
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56137
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getRequestClientId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    .line 56139
    return-object p0
.end method

.method public final clearSetAclsResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 56915
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    .line 56917
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56918
    return-object p0
.end method

.method public final clearSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 57087
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    .line 57089
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57090
    return-object p0
.end method

.method public final clearTileEventResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56743
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    .line 56745
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56746
    return-object p0
.end method

.method public final clearTypingResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56270
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    .line 56272
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56273
    return-object p0
.end method

.method public final clearUserCreationResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 2

    .prologue
    .line 56571
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    .line 56573
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56574
    return-object p0
.end method

.method public final clearUserInfoResponse()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3

    .prologue
    .line 56958
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    .line 56960
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56961
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 55489
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 55489
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 55489
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getChatMessageResponse()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;
    .registers 2

    .prologue
    .line 56153
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    return-object v0
.end method

.method public final getConversationJoinResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;
    .registers 2

    .prologue
    .line 57142
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    return-object v0
.end method

.method public final getConversationListResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;
    .registers 2

    .prologue
    .line 56497
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    return-object v0
.end method

.method public final getConversationPreferenceResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;
    .registers 2

    .prologue
    .line 56755
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    return-object v0
.end method

.method public final getConversationRenameResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;
    .registers 2

    .prologue
    .line 56669
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    return-object v0
.end method

.method public final getConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .registers 2

    .prologue
    .line 56325
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    return-object v0
.end method

.method public final getConversationSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .registers 2

    .prologue
    .line 56970
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 55489
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 55489
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .registers 2

    .prologue
    .line 55574
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceRegistrationResponse()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;
    .registers 2

    .prologue
    .line 56411
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    return-object v0
.end method

.method public final getError()Lcom/google/wireless/realtimechat/proto/Client$Error;
    .registers 2

    .prologue
    .line 56583
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    return-object v0
.end method

.method public final getEventSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;
    .registers 2

    .prologue
    .line 57013
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    return-object v0
.end method

.method public final getEventSteamResponse()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
    .registers 2

    .prologue
    .line 56368
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    return-object v0
.end method

.method public final getGlobalConversationPreferencesResponse()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;
    .registers 2

    .prologue
    .line 57099
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    return-object v0
.end method

.method public final getHangoutInviteFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;
    .registers 2

    .prologue
    .line 57314
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    return-object v0
.end method

.method public final getHangoutInviteKeepAliveResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;
    .registers 2

    .prologue
    .line 57228
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    return-object v0
.end method

.method public final getHangoutInviteReplyResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;
    .registers 2

    .prologue
    .line 57271
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    return-object v0
.end method

.method public final getHangoutInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .registers 2

    .prologue
    .line 57185
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    return-object v0
.end method

.method public final getHangoutRingFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;
    .registers 2

    .prologue
    .line 57357
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    return-object v0
.end method

.method public final getInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
    .registers 2

    .prologue
    .line 56798
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    return-object v0
.end method

.method public final getLeaveConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;
    .registers 2

    .prologue
    .line 56626
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    return-object v0
.end method

.method public final getPingResponse()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;
    .registers 2

    .prologue
    .line 56454
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    return-object v0
.end method

.method public final getPresenceResponse()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;
    .registers 2

    .prologue
    .line 56196
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    return-object v0
.end method

.method public final getReceiptResponse()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;
    .registers 2

    .prologue
    .line 56282
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    return-object v0
.end method

.method public final getReplyToInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;
    .registers 2

    .prologue
    .line 56841
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    return-object v0
.end method

.method public final getRequestClientId()Ljava/lang/String;
    .registers 4

    .prologue
    .line 56117
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    .line 56118
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_f

    .line 56119
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 56120
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    .line 56123
    .end local v1           #s:Ljava/lang/String;
    :goto_e
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_e
.end method

.method public final getSetAclsResponse()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;
    .registers 2

    .prologue
    .line 56884
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    return-object v0
.end method

.method public final getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    .registers 2

    .prologue
    .line 57056
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    return-object v0
.end method

.method public final getTileEventResponse()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;
    .registers 2

    .prologue
    .line 56712
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    return-object v0
.end method

.method public final getTypingResponse()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;
    .registers 2

    .prologue
    .line 56239
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    return-object v0
.end method

.method public final getUserCreationResponse()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;
    .registers 2

    .prologue
    .line 56540
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    return-object v0
.end method

.method public final getUserInfoResponse()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
    .registers 2

    .prologue
    .line 56927
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    return-object v0
.end method

.method public final hasChatMessageResponse()Z
    .registers 3

    .prologue
    .line 56150
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasConversationJoinResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x100

    .line 57139
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasConversationListResponse()Z
    .registers 3

    .prologue
    .line 56494
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasConversationPreferenceResponse()Z
    .registers 3

    .prologue
    const v1, 0x8000

    .line 56752
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasConversationRenameResponse()Z
    .registers 3

    .prologue
    .line 56666
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasConversationResponse()Z
    .registers 3

    .prologue
    .line 56322
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasConversationSearchResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x10

    .line 56967
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasDeviceRegistrationResponse()Z
    .registers 3

    .prologue
    .line 56408
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasError()Z
    .registers 3

    .prologue
    .line 56580
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasEventSearchResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x20

    .line 57010
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasEventSteamResponse()Z
    .registers 3

    .prologue
    .line 56365
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasGlobalConversationPreferencesResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x80

    .line 57096
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasHangoutInviteFinishResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x1000

    .line 57311
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasHangoutInviteKeepAliveResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x400

    .line 57225
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasHangoutInviteReplyResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x800

    .line 57268
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasHangoutInviteResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x200

    .line 57182
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasHangoutRingFinishResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x2000

    .line 57354
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasInviteResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x1

    .line 56795
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasLeaveConversationResponse()Z
    .registers 3

    .prologue
    .line 56623
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasPingResponse()Z
    .registers 3

    .prologue
    .line 56451
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasPresenceResponse()Z
    .registers 3

    .prologue
    .line 56193
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasReceiptResponse()Z
    .registers 3

    .prologue
    .line 56279
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasReplyToInviteResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x2

    .line 56838
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasRequestClientId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 56114
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasSetAclsResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x4

    .line 56881
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasSuggestionsResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x40

    .line 57053
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasTileEventResponse()Z
    .registers 3

    .prologue
    .line 56709
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasTypingResponse()Z
    .registers 3

    .prologue
    .line 56236
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasUserCreationResponse()Z
    .registers 3

    .prologue
    .line 56537
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasUserInfoResponse()Z
    .registers 3

    .prologue
    const/high16 v1, 0x8

    .line 56924
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 55819
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55489
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 55489
    check-cast p1, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55489
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 10
    .parameter "other"

    .prologue
    const/high16 v7, 0x8

    const/high16 v6, 0x4

    const/high16 v5, 0x2

    const/high16 v4, 0x1

    const v3, 0x8000

    .line 55724
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    if-ne p1, v0, :cond_12

    .line 55815
    :cond_11
    :goto_11
    return-object p0

    .line 55725
    :cond_12
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasRequestClientId()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 55726
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getRequestClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setRequestClientId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    .line 55728
    :cond_1f
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasChatMessageResponse()Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 55729
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getChatMessageResponse()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_58f

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v2

    if-eq v1, v2, :cond_58f

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    :goto_48
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55731
    :cond_4e
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasPresenceResponse()Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 55732
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getPresenceResponse()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_593

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v2

    if-eq v1, v2, :cond_593

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    :goto_77
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55734
    :cond_7d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasTypingResponse()Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 55735
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getTypingResponse()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_597

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v2

    if-eq v1, v2, :cond_597

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;)Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;)Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    :goto_a7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55737
    :cond_ad
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasReceiptResponse()Z

    move-result v0

    if-eqz v0, :cond_dd

    .line 55738
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getReceiptResponse()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_59b

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v2

    if-eq v1, v2, :cond_59b

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    :goto_d7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55740
    :cond_dd
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationResponse()Z

    move-result v0

    if-eqz v0, :cond_10d

    .line 55741
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_59f

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v2

    if-eq v1, v2, :cond_59f

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    :goto_107
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55743
    :cond_10d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasEventSteamResponse()Z

    move-result v0

    if-eqz v0, :cond_13d

    .line 55744
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getEventSteamResponse()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_5a3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5a3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    :goto_137
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55746
    :cond_13d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasDeviceRegistrationResponse()Z

    move-result v0

    if-eqz v0, :cond_16d

    .line 55747
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getDeviceRegistrationResponse()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_5a7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5a7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    :goto_167
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55749
    :cond_16d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasPingResponse()Z

    move-result v0

    if-eqz v0, :cond_19d

    .line 55750
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getPingResponse()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_5ab

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5ab

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$PingResponse;)Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$PingResponse;)Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    :goto_197
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55752
    :cond_19d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationListResponse()Z

    move-result v0

    if-eqz v0, :cond_1cd

    .line 55753
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationListResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_5af

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5af

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    :goto_1c7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55755
    :cond_1cd
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasUserCreationResponse()Z

    move-result v0

    if-eqz v0, :cond_1fd

    .line 55756
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getUserCreationResponse()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_5b3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5b3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    :goto_1f7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55758
    :cond_1fd
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasError()Z

    move-result v0

    if-eqz v0, :cond_22d

    .line 55759
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getError()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_5b7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Error;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v2

    if-eq v1, v2, :cond_5b7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$Error;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$Error;)Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Error;)Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    :goto_227
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55761
    :cond_22d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasLeaveConversationResponse()Z

    move-result v0

    if-eqz v0, :cond_25d

    .line 55762
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getLeaveConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_5bb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5bb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    :goto_257
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55764
    :cond_25d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationRenameResponse()Z

    move-result v0

    if-eqz v0, :cond_28d

    .line 55765
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationRenameResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_5bf

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5bf

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    :goto_287
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55767
    :cond_28d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasTileEventResponse()Z

    move-result v0

    if-eqz v0, :cond_2bd

    .line 55768
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getTileEventResponse()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_5c3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5c3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;)Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;)Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    :goto_2b7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55770
    :cond_2bd
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationPreferenceResponse()Z

    move-result v0

    if-eqz v0, :cond_2e9

    .line 55771
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationPreferenceResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v1, v3

    if-ne v1, v3, :cond_5c7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5c7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    :goto_2e4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55773
    :cond_2e9
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasInviteResponse()Z

    move-result v0

    if-eqz v0, :cond_315

    .line 55774
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v1, v4

    if-ne v1, v4, :cond_5cb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5cb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    :goto_310
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/2addr v0, v4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55776
    :cond_315
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasReplyToInviteResponse()Z

    move-result v0

    if-eqz v0, :cond_341

    .line 55777
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getReplyToInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v1, v5

    if-ne v1, v5, :cond_5cf

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5cf

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    :goto_33c
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55779
    :cond_341
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSetAclsResponse()Z

    move-result v0

    if-eqz v0, :cond_36d

    .line 55780
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSetAclsResponse()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v1, v6

    if-ne v1, v6, :cond_5d3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5d3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    :goto_368
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55782
    :cond_36d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasUserInfoResponse()Z

    move-result v0

    if-eqz v0, :cond_399

    .line 55783
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getUserInfoResponse()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    and-int/2addr v1, v7

    if-ne v1, v7, :cond_5d7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5d7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    :goto_394
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/2addr v0, v7

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55785
    :cond_399
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationSearchResponse()Z

    move-result v0

    if-eqz v0, :cond_3cb

    .line 55786
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x10

    and-int/2addr v1, v2

    const/high16 v2, 0x10

    if-ne v1, v2, :cond_5db

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5db

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    :goto_3c4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55788
    :cond_3cb
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasEventSearchResponse()Z

    move-result v0

    if-eqz v0, :cond_3fd

    .line 55789
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getEventSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x20

    and-int/2addr v1, v2

    const/high16 v2, 0x20

    if-ne v1, v2, :cond_5df

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5df

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    :goto_3f6
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55791
    :cond_3fd
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSuggestionsResponse()Z

    move-result v0

    if-eqz v0, :cond_42f

    .line 55792
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x40

    and-int/2addr v1, v2

    const/high16 v2, 0x40

    if-ne v1, v2, :cond_5e3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5e3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    :goto_428
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x40

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55794
    :cond_42f
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasGlobalConversationPreferencesResponse()Z

    move-result v0

    if-eqz v0, :cond_461

    .line 55795
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getGlobalConversationPreferencesResponse()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x80

    and-int/2addr v1, v2

    const/high16 v2, 0x80

    if-ne v1, v2, :cond_5e7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5e7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;)Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;)Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    :goto_45a
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55797
    :cond_461
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationJoinResponse()Z

    move-result v0

    if-eqz v0, :cond_493

    .line 55798
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationJoinResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x100

    and-int/2addr v1, v2

    const/high16 v2, 0x100

    if-ne v1, v2, :cond_5eb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5eb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    :goto_48c
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55800
    :cond_493
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteResponse()Z

    move-result v0

    if-eqz v0, :cond_4c5

    .line 55801
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getHangoutInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x200

    and-int/2addr v1, v2

    const/high16 v2, 0x200

    if-ne v1, v2, :cond_5ef

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5ef

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    :goto_4be
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55803
    :cond_4c5
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteKeepAliveResponse()Z

    move-result v0

    if-eqz v0, :cond_4f7

    .line 55804
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getHangoutInviteKeepAliveResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x400

    and-int/2addr v1, v2

    const/high16 v2, 0x400

    if-ne v1, v2, :cond_5f3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5f3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    :goto_4f0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x400

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55806
    :cond_4f7
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteReplyResponse()Z

    move-result v0

    if-eqz v0, :cond_529

    .line 55807
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getHangoutInviteReplyResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x800

    and-int/2addr v1, v2

    const/high16 v2, 0x800

    if-ne v1, v2, :cond_5f7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5f7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    :goto_522
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x800

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55809
    :cond_529
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteFinishResponse()Z

    move-result v0

    if-eqz v0, :cond_55b

    .line 55810
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getHangoutInviteFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x1000

    and-int/2addr v1, v2

    const/high16 v2, 0x1000

    if-ne v1, v2, :cond_5fb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5fb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    :goto_554
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x1000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 55812
    :cond_55b
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutRingFinishResponse()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 55813
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getHangoutRingFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v2, 0x2000

    and-int/2addr v1, v2

    const/high16 v2, 0x2000

    if-ne v1, v2, :cond_5ff

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v2

    if-eq v1, v2, :cond_5ff

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    :goto_586
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x2000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    goto/16 :goto_11

    .line 55729
    :cond_58f
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    goto/16 :goto_48

    .line 55732
    :cond_593
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    goto/16 :goto_77

    .line 55735
    :cond_597
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    goto/16 :goto_a7

    .line 55738
    :cond_59b
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    goto/16 :goto_d7

    .line 55741
    :cond_59f
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    goto/16 :goto_107

    .line 55744
    :cond_5a3
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    goto/16 :goto_137

    .line 55747
    :cond_5a7
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    goto/16 :goto_167

    .line 55750
    :cond_5ab
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    goto/16 :goto_197

    .line 55753
    :cond_5af
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    goto/16 :goto_1c7

    .line 55756
    :cond_5b3
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    goto/16 :goto_1f7

    .line 55759
    :cond_5b7
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    goto/16 :goto_227

    .line 55762
    :cond_5bb
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    goto/16 :goto_257

    .line 55765
    :cond_5bf
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    goto/16 :goto_287

    .line 55768
    :cond_5c3
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    goto/16 :goto_2b7

    .line 55771
    :cond_5c7
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    goto/16 :goto_2e4

    .line 55774
    :cond_5cb
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    goto/16 :goto_310

    .line 55777
    :cond_5cf
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    goto/16 :goto_33c

    .line 55780
    :cond_5d3
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    goto/16 :goto_368

    .line 55783
    :cond_5d7
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    goto/16 :goto_394

    .line 55786
    :cond_5db
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    goto/16 :goto_3c4

    .line 55789
    :cond_5df
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    goto/16 :goto_3f6

    .line 55792
    :cond_5e3
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    goto/16 :goto_428

    .line 55795
    :cond_5e7
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    goto/16 :goto_45a

    .line 55798
    :cond_5eb
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    goto/16 :goto_48c

    .line 55801
    :cond_5ef
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    goto/16 :goto_4be

    .line 55804
    :cond_5f3
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    goto/16 :goto_4f0

    .line 55807
    :cond_5f7
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    goto/16 :goto_522

    .line 55810
    :cond_5fb
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    goto/16 :goto_554

    .line 55813
    :cond_5ff
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    goto :goto_586
.end method

.method public final setChatMessageResponse(Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56166
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    .line 56168
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56169
    return-object p0
.end method

.method public final setChatMessageResponse(Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56156
    if-nez p1, :cond_8

    .line 56157
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56159
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    .line 56161
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56162
    return-object p0
.end method

.method public final setConversationJoinResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57155
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    .line 57157
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57158
    return-object p0
.end method

.method public final setConversationJoinResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57145
    if-nez p1, :cond_8

    .line 57146
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57148
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    .line 57150
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57151
    return-object p0
.end method

.method public final setConversationListResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56510
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    .line 56512
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56513
    return-object p0
.end method

.method public final setConversationListResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56500
    if-nez p1, :cond_8

    .line 56501
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56503
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    .line 56505
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56506
    return-object p0
.end method

.method public final setConversationPreferenceResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 56768
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    .line 56770
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56771
    return-object p0
.end method

.method public final setConversationPreferenceResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 56758
    if-nez p1, :cond_8

    .line 56759
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56761
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    .line 56763
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56764
    return-object p0
.end method

.method public final setConversationRenameResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56682
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    .line 56684
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56685
    return-object p0
.end method

.method public final setConversationRenameResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56672
    if-nez p1, :cond_8

    .line 56673
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56675
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    .line 56677
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56678
    return-object p0
.end method

.method public final setConversationResponse(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56338
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    .line 56340
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56341
    return-object p0
.end method

.method public final setConversationResponse(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56328
    if-nez p1, :cond_8

    .line 56329
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56331
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    .line 56333
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56334
    return-object p0
.end method

.method public final setConversationSearchResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 56983
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    .line 56985
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56986
    return-object p0
.end method

.method public final setConversationSearchResponse(Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 56973
    if-nez p1, :cond_8

    .line 56974
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56976
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    .line 56978
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56979
    return-object p0
.end method

.method public final setDeviceRegistrationResponse(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56424
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    .line 56426
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56427
    return-object p0
.end method

.method public final setDeviceRegistrationResponse(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56414
    if-nez p1, :cond_8

    .line 56415
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56417
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    .line 56419
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56420
    return-object p0
.end method

.method public final setError(Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56596
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Error$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    .line 56598
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56599
    return-object p0
.end method

.method public final setError(Lcom/google/wireless/realtimechat/proto/Client$Error;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56586
    if-nez p1, :cond_8

    .line 56587
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56589
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    .line 56591
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56592
    return-object p0
.end method

.method public final setEventSearchResponse(Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57026
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    .line 57028
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57029
    return-object p0
.end method

.method public final setEventSearchResponse(Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57016
    if-nez p1, :cond_8

    .line 57017
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57019
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    .line 57021
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57022
    return-object p0
.end method

.method public final setEventSteamResponse(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56381
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    .line 56383
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56384
    return-object p0
.end method

.method public final setEventSteamResponse(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56371
    if-nez p1, :cond_8

    .line 56372
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56374
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    .line 56376
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56377
    return-object p0
.end method

.method public final setGlobalConversationPreferencesResponse(Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57112
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    .line 57114
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57115
    return-object p0
.end method

.method public final setGlobalConversationPreferencesResponse(Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57102
    if-nez p1, :cond_8

    .line 57103
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57105
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    .line 57107
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57108
    return-object p0
.end method

.method public final setHangoutInviteFinishResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57327
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    .line 57329
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x1000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57330
    return-object p0
.end method

.method public final setHangoutInviteFinishResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57317
    if-nez p1, :cond_8

    .line 57318
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57320
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    .line 57322
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x1000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57323
    return-object p0
.end method

.method public final setHangoutInviteKeepAliveResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57241
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    .line 57243
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x400

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57244
    return-object p0
.end method

.method public final setHangoutInviteKeepAliveResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57231
    if-nez p1, :cond_8

    .line 57232
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57234
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    .line 57236
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x400

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57237
    return-object p0
.end method

.method public final setHangoutInviteReplyResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57284
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    .line 57286
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x800

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57287
    return-object p0
.end method

.method public final setHangoutInviteReplyResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57274
    if-nez p1, :cond_8

    .line 57275
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57277
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    .line 57279
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x800

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57280
    return-object p0
.end method

.method public final setHangoutInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57198
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    .line 57200
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57201
    return-object p0
.end method

.method public final setHangoutInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57188
    if-nez p1, :cond_8

    .line 57189
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57191
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    .line 57193
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57194
    return-object p0
.end method

.method public final setHangoutRingFinishResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57370
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    .line 57372
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x2000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57373
    return-object p0
.end method

.method public final setHangoutRingFinishResponse(Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57360
    if-nez p1, :cond_8

    .line 57361
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57363
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    .line 57365
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x2000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57366
    return-object p0
.end method

.method public final setInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 56811
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    .line 56813
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56814
    return-object p0
.end method

.method public final setInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 56801
    if-nez p1, :cond_8

    .line 56802
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56804
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    .line 56806
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56807
    return-object p0
.end method

.method public final setLeaveConversationResponse(Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56639
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    .line 56641
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56642
    return-object p0
.end method

.method public final setLeaveConversationResponse(Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56629
    if-nez p1, :cond_8

    .line 56630
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56632
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    .line 56634
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56635
    return-object p0
.end method

.method public final setPingResponse(Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56467
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    .line 56469
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56470
    return-object p0
.end method

.method public final setPingResponse(Lcom/google/wireless/realtimechat/proto/Client$PingResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56457
    if-nez p1, :cond_8

    .line 56458
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56460
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    .line 56462
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56463
    return-object p0
.end method

.method public final setPresenceResponse(Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56209
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    .line 56211
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56212
    return-object p0
.end method

.method public final setPresenceResponse(Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56199
    if-nez p1, :cond_8

    .line 56200
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56202
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    .line 56204
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56205
    return-object p0
.end method

.method public final setReceiptResponse(Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56295
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    .line 56297
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56298
    return-object p0
.end method

.method public final setReceiptResponse(Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56285
    if-nez p1, :cond_8

    .line 56286
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56288
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    .line 56290
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56291
    return-object p0
.end method

.method public final setReplyToInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 56854
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    .line 56856
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56857
    return-object p0
.end method

.method public final setReplyToInviteResponse(Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 56844
    if-nez p1, :cond_8

    .line 56845
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56847
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    .line 56849
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56850
    return-object p0
.end method

.method public final setRequestClientId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56127
    if-nez p1, :cond_8

    .line 56128
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56130
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56131
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->requestClientId_:Ljava/lang/Object;

    .line 56133
    return-object p0
.end method

.method public final setSetAclsResponse(Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 56897
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    .line 56899
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x4

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56900
    return-object p0
.end method

.method public final setSetAclsResponse(Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 56887
    if-nez p1, :cond_8

    .line 56888
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56890
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    .line 56892
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x4

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56893
    return-object p0
.end method

.method public final setSuggestionsResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 57069
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    .line 57071
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x40

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57072
    return-object p0
.end method

.method public final setSuggestionsResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57059
    if-nez p1, :cond_8

    .line 57060
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57062
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    .line 57064
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x40

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 57065
    return-object p0
.end method

.method public final setTileEventResponse(Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56725
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    .line 56727
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56728
    return-object p0
.end method

.method public final setTileEventResponse(Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56715
    if-nez p1, :cond_8

    .line 56716
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56718
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    .line 56720
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56721
    return-object p0
.end method

.method public final setTypingResponse(Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56252
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    .line 56254
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56255
    return-object p0
.end method

.method public final setTypingResponse(Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56242
    if-nez p1, :cond_8

    .line 56243
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56245
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    .line 56247
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56248
    return-object p0
.end method

.method public final setUserCreationResponse(Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 56553
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    .line 56555
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56556
    return-object p0
.end method

.method public final setUserCreationResponse(Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 56543
    if-nez p1, :cond_8

    .line 56544
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56546
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    .line 56548
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56549
    return-object p0
.end method

.method public final setUserInfoResponse(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 56940
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    .line 56942
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56943
    return-object p0
.end method

.method public final setUserInfoResponse(Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 56930
    if-nez p1, :cond_8

    .line 56931
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56933
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    .line 56935
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->bitField0_:I

    .line 56936
    return-object p0
.end method
