.class public final Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequestOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HangoutInviteRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private inviteClientId_:Ljava/lang/Object;

.field private inviterJid_:Ljava/lang/Object;

.field private inviterMucJid_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private participantId_:Ljava/lang/Object;

.field private stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 42859
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;-><init>()V

    .line 42860
    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviteClientId_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->participantId_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterMucJid_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterJid_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    .line 42861
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 42211
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42368
    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedIsInitialized:B

    .line 42397
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedSerializedSize:I

    .line 42211
    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;)V
    .registers 4
    .parameter "builder"

    .prologue
    const/4 v1, -0x1

    .line 42209
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    .line 42368
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedIsInitialized:B

    .line 42397
    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedSerializedSize:I

    .line 42210
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 42204
    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;-><init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;)V

    return-void
.end method

.method static synthetic access$58702(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42204
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviteClientId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$58802(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42204
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->participantId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$58902(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42204
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterMucJid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59002(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42204
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterJid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59102(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42204
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object p1
.end method

.method static synthetic access$59202(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 42204
    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;
    .registers 1

    .prologue
    .line 42215
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    return-object v0
.end method

.method private getInviteClientIdBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 42244
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviteClientId_:Ljava/lang/Object;

    .line 42245
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 42246
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 42248
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviteClientId_:Ljava/lang/Object;

    .line 42251
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method private getInviterJidBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 42340
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterJid_:Ljava/lang/Object;

    .line 42341
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 42342
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 42344
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterJid_:Ljava/lang/Object;

    .line 42347
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method private getInviterMucJidBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 42308
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterMucJid_:Ljava/lang/Object;

    .line 42309
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 42310
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 42312
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterMucJid_:Ljava/lang/Object;

    .line 42315
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method private getParticipantIdBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 42276
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->participantId_:Ljava/lang/Object;

    .line 42277
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 42278
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 42280
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->participantId_:Ljava/lang/Object;

    .line 42283
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;
    .registers 1

    .prologue
    .line 42501
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;->access$58500()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;
    .registers 2
    .parameter "prototype"

    .prologue
    .line 42504
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;->access$58500()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 42204
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;
    .registers 2

    .prologue
    .line 42219
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    return-object v0
.end method

.method public final getInviteClientId()Ljava/lang/String;
    .registers 5

    .prologue
    .line 42230
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviteClientId_:Ljava/lang/Object;

    .line 42231
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 42232
    check-cast v1, Ljava/lang/String;

    .line 42240
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 42234
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 42236
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 42237
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 42238
    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviteClientId_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 42240
    goto :goto_8
.end method

.method public final getInviterJid()Ljava/lang/String;
    .registers 5

    .prologue
    .line 42326
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterJid_:Ljava/lang/Object;

    .line 42327
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 42328
    check-cast v1, Ljava/lang/String;

    .line 42336
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 42330
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 42332
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 42333
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 42334
    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterJid_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 42336
    goto :goto_8
.end method

.method public final getInviterMucJid()Ljava/lang/String;
    .registers 5

    .prologue
    .line 42294
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterMucJid_:Ljava/lang/Object;

    .line 42295
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 42296
    check-cast v1, Ljava/lang/String;

    .line 42304
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 42298
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 42300
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 42301
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 42302
    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->inviterMucJid_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 42304
    goto :goto_8
.end method

.method public final getParticipantId()Ljava/lang/String;
    .registers 5

    .prologue
    .line 42262
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->participantId_:Ljava/lang/Object;

    .line 42263
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 42264
    check-cast v1, Ljava/lang/String;

    .line 42272
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 42266
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 42268
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 42269
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 42270
    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->participantId_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 42272
    goto :goto_8
.end method

.method public final getSerializedSize()I
    .registers 7

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 42399
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedSerializedSize:I

    .line 42400
    .local v0, size:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_a

    move v1, v0

    .line 42424
    .end local v0           #size:I
    .local v1, size:I
    :goto_9
    return v1

    .line 42402
    .end local v1           #size:I
    .restart local v0       #size:I
    :cond_a
    const/4 v0, 0x0

    .line 42403
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1b

    .line 42404
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getInviteClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    .line 42407
    :cond_1b
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2a

    .line 42408
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getParticipantIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42411
    :cond_2a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3a

    .line 42412
    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getInviterMucJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42415
    :cond_3a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_49

    .line 42416
    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42419
    :cond_49
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_5b

    .line 42420
    const/4 v2, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getInviterJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42423
    :cond_5b
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedSerializedSize:I

    move v1, v0

    .line 42424
    .end local v0           #size:I
    .restart local v1       #size:I
    goto :goto_9
.end method

.method public final getStubbyInfo()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .registers 2

    .prologue
    .line 42358
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object v0
.end method

.method public final hasInviteClientId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 42227
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasInviterJid()Z
    .registers 3

    .prologue
    .line 42323
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasInviterMucJid()Z
    .registers 3

    .prologue
    .line 42291
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasParticipantId()Z
    .registers 3

    .prologue
    .line 42259
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasStubbyInfo()Z
    .registers 3

    .prologue
    .line 42355
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final isInitialized()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 42370
    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedIsInitialized:B

    .line 42371
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_b

    if-ne v0, v1, :cond_9

    .line 42374
    :goto_8
    return v1

    .line 42371
    :cond_9
    const/4 v1, 0x0

    goto :goto_8

    .line 42373
    :cond_b
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->memoizedIsInitialized:B

    goto :goto_8
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 42204
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;->access$58500()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 42204
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;->access$58500()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 42431
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 6
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 42379
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getSerializedSize()I

    .line 42380
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_13

    .line 42381
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getInviteClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 42383
    :cond_13
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_20

    .line 42384
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getParticipantIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 42386
    :cond_20
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2e

    .line 42387
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getInviterMucJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 42389
    :cond_2e
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3b

    .line 42390
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 42392
    :cond_3b
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4b

    .line 42393
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getInviterJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 42395
    :cond_4b
    return-void
.end method
