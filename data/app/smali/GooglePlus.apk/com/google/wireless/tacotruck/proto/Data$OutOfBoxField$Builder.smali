.class public final Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxFieldOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;",
        "Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;",
        ">;",
        "Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxFieldOrBuilder;"
    }
.end annotation


# instance fields
.field private action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

.field private bitField0_:I

.field private error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

.field private image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

.field private input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

.field private text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 31596
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 31768
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    .line 31811
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    .line 31854
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    .line 31897
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    .line 31940
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    .line 31597
    return-void
.end method

.method static synthetic access$42600()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 1

    .prologue
    .line 31591
    new-instance v0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    invoke-direct {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3

    .prologue
    .line 31622
    new-instance v0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    invoke-direct {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31704
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 31705
    .local v1, tag:I
    sparse-switch v1, :sswitch_data_9c

    .line 31710
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 31712
    :sswitch_d
    return-object p0

    .line 31717
    :sswitch_e
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;->newBuilder()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;

    move-result-object v0

    .line 31718
    .local v0, subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->hasText()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 31719
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->getText()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;

    .line 31721
    :cond_1f
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 31722
    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->setText(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    goto :goto_0

    .line 31726
    .end local v0           #subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;
    :sswitch_2a
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;->newBuilder()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;

    move-result-object v0

    .line 31727
    .local v0, subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->hasInput()Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 31728
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->getInput()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;

    .line 31730
    :cond_3b
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 31731
    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->setInput(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    goto :goto_0

    .line 31735
    .end local v0           #subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;
    :sswitch_46
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;->newBuilder()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;

    move-result-object v0

    .line 31736
    .local v0, subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->hasError()Z

    move-result v2

    if-eqz v2, :cond_57

    .line 31737
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->getError()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;

    .line 31739
    :cond_57
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 31740
    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->setError(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    goto :goto_0

    .line 31744
    .end local v0           #subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;
    :sswitch_62
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;->newBuilder()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;

    move-result-object v0

    .line 31745
    .local v0, subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->hasAction()Z

    move-result v2

    if-eqz v2, :cond_73

    .line 31746
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->getAction()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;

    .line 31748
    :cond_73
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 31749
    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->setAction(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    goto :goto_0

    .line 31753
    .end local v0           #subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;
    :sswitch_7e
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;->newBuilder()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;

    move-result-object v0

    .line 31754
    .local v0, subBuilder:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_8f

    .line 31755
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->getImage()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;

    .line 31757
    :cond_8f
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 31758
    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->setImage(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    goto/16 :goto_0

    .line 31705
    nop

    :sswitch_data_9c
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_2a
        0x1a -> :sswitch_46
        0x22 -> :sswitch_62
        0x2a -> :sswitch_7e
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 31591
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->build()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;
    .registers 3

    .prologue
    .line 31630
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v0

    .line 31631
    .local v0, result:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;
    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    .line 31632
    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    .line 31634
    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 31591
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;
    .registers 6

    .prologue
    .line 31648
    new-instance v1, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;-><init>(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;B)V

    .line 31649
    .local v1, result:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31650
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 31651
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    .line 31652
    const/4 v2, 0x1

    .line 31654
    :cond_f
    iget-object v3, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    #setter for: Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;
    invoke-static {v1, v3}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->access$42802(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    .line 31655
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1b

    .line 31656
    or-int/lit8 v2, v2, 0x2

    .line 31658
    :cond_1b
    iget-object v3, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    #setter for: Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;
    invoke-static {v1, v3}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->access$42902(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    .line 31659
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_27

    .line 31660
    or-int/lit8 v2, v2, 0x4

    .line 31662
    :cond_27
    iget-object v3, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    #setter for: Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;
    invoke-static {v1, v3}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->access$43002(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    .line 31663
    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_34

    .line 31664
    or-int/lit8 v2, v2, 0x8

    .line 31666
    :cond_34
    iget-object v3, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    #setter for: Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;
    invoke-static {v1, v3}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->access$43102(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    .line 31667
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_41

    .line 31668
    or-int/lit8 v2, v2, 0x10

    .line 31670
    :cond_41
    iget-object v3, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    #setter for: Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;
    invoke-static {v1, v3}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->access$43202(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    .line 31671
    #setter for: Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->access$43302(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;I)I

    .line 31672
    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 31591
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->clear()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 31591
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->clear()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 2

    .prologue
    .line 31607
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 31608
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    .line 31609
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31610
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    .line 31611
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31612
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    .line 31613
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31614
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    .line 31615
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31616
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    .line 31617
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31618
    return-object p0
.end method

.method public final clearAction()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 2

    .prologue
    .line 31933
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    .line 31935
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31936
    return-object p0
.end method

.method public final clearError()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 2

    .prologue
    .line 31890
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    .line 31892
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31893
    return-object p0
.end method

.method public final clearImage()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 2

    .prologue
    .line 31976
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    .line 31978
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31979
    return-object p0
.end method

.method public final clearInput()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 2

    .prologue
    .line 31847
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    .line 31849
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31850
    return-object p0
.end method

.method public final clearText()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 2

    .prologue
    .line 31804
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    .line 31806
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31807
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 31591
    invoke-direct {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->clone()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 31591
    invoke-direct {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->clone()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 31591
    invoke-direct {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->clone()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAction()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;
    .registers 2

    .prologue
    .line 31902
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 31591
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->getDefaultInstanceForType()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 31591
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->getDefaultInstanceForType()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;
    .registers 2

    .prologue
    .line 31626
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v0

    return-object v0
.end method

.method public final getError()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;
    .registers 2

    .prologue
    .line 31859
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    return-object v0
.end method

.method public final getImage()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;
    .registers 2

    .prologue
    .line 31945
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    return-object v0
.end method

.method public final getInput()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;
    .registers 2

    .prologue
    .line 31816
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    return-object v0
.end method

.method public final getText()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;
    .registers 2

    .prologue
    .line 31773
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    return-object v0
.end method

.method public final hasAction()Z
    .registers 3

    .prologue
    .line 31899
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasError()Z
    .registers 3

    .prologue
    .line 31856
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasImage()Z
    .registers 3

    .prologue
    .line 31942
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasInput()Z
    .registers 3

    .prologue
    .line 31813
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasText()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 31770
    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 31696
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31591
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 31591
    check-cast p1, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31591
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 5
    .parameter "other"

    .prologue
    .line 31676
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 31692
    :cond_6
    :goto_6
    return-object p0

    .line 31677
    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->hasText()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 31678
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->getText()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_f6

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v2

    if-eq v1, v2, :cond_f6

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    invoke-static {v1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;->newBuilder(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    :goto_30
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31680
    :cond_36
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->hasInput()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 31681
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->getInput()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_fa

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v2

    if-eq v1, v2, :cond_fa

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    invoke-static {v1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;->newBuilder(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    :goto_5f
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31683
    :cond_65
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->hasError()Z

    move-result v0

    if-eqz v0, :cond_94

    .line 31684
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->getError()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_fe

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v2

    if-eq v1, v2, :cond_fe

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    invoke-static {v1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;->newBuilder(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    :goto_8e
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31686
    :cond_94
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->hasAction()Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 31687
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->getAction()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_101

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v2

    if-eq v1, v2, :cond_101

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    invoke-static {v1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;->newBuilder(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    :goto_be
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31689
    :cond_c4
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 31690
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField;->getImage()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_104

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v2

    if-eq v1, v2, :cond_104

    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    invoke-static {v1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;->newBuilder(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;->buildPartial()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    :goto_ee
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    goto/16 :goto_6

    .line 31678
    :cond_f6
    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    goto/16 :goto_30

    .line 31681
    :cond_fa
    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    goto/16 :goto_5f

    .line 31684
    :cond_fe
    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    goto :goto_8e

    .line 31687
    :cond_101
    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    goto :goto_be

    .line 31690
    :cond_104
    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    goto :goto_ee
.end method

.method public final setAction(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 31915
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction$Builder;->build()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    .line 31917
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31918
    return-object p0
.end method

.method public final setAction(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 31905
    if-nez p1, :cond_8

    .line 31906
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31908
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->action_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxAction;

    .line 31910
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31911
    return-object p0
.end method

.method public final setError(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 31872
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError$Builder;->build()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    .line 31874
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31875
    return-object p0
.end method

.method public final setError(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 31862
    if-nez p1, :cond_8

    .line 31863
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31865
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->error_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxError;

    .line 31867
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31868
    return-object p0
.end method

.method public final setImage(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 31958
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField$Builder;->build()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    .line 31960
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31961
    return-object p0
.end method

.method public final setImage(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 31948
    if-nez p1, :cond_8

    .line 31949
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31951
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->image_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxImageField;

    .line 31953
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31954
    return-object p0
.end method

.method public final setInput(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 31829
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField$Builder;->build()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    .line 31831
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31832
    return-object p0
.end method

.method public final setInput(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 31819
    if-nez p1, :cond_8

    .line 31820
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31822
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->input_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxInputField;

    .line 31824
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31825
    return-object p0
.end method

.method public final setText(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 31786
    invoke-virtual {p1}, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField$Builder;->build()Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    .line 31788
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31789
    return-object p0
.end method

.method public final setText(Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;)Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 31776
    if-nez p1, :cond_8

    .line 31777
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31779
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->text_:Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxTextField;

    .line 31781
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$OutOfBoxField$Builder;->bitField0_:I

    .line 31782
    return-object p0
.end method
