.class public final Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/tacotruck/proto/Data$SearchQueryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/tacotruck/proto/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchQuery"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;,
        Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;,
        Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private filter_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private position_:Lcom/google/wireless/tacotruck/proto/Data$Point;

.field private queryText_:Ljava/lang/Object;

.field private searchRadiusMeters_:I

.field private sort_:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 40701
    new-instance v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;

    invoke-direct {v0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;-><init>()V

    .line 40702
    sput-object v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->defaultInstance:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->queryText_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    sget-object v1, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;->BEST:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;

    iput-object v1, v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->sort_:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$Point;->getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$Point;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->position_:Lcom/google/wireless/tacotruck/proto/Data$Point;

    const/16 v1, 0x2710

    iput v1, v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->searchRadiusMeters_:I

    .line 40703
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 39995
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 40177
    iput-byte v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedIsInitialized:B

    .line 40212
    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedSerializedSize:I

    .line 39995
    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;)V
    .registers 4
    .parameter "builder"

    .prologue
    const/4 v1, -0x1

    .line 39993
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    .line 40177
    iput-byte v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedIsInitialized:B

    .line 40212
    iput v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedSerializedSize:I

    .line 39994
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 39988
    invoke-direct {p0, p1}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;-><init>(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;)V

    return-void
.end method

.method static synthetic access$54102(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39988
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->queryText_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$54200(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39988
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$54202(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39988
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$54302(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;)Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39988
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->sort_:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;

    return-object p1
.end method

.method static synthetic access$54402(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;Lcom/google/wireless/tacotruck/proto/Data$Point;)Lcom/google/wireless/tacotruck/proto/Data$Point;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39988
    iput-object p1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->position_:Lcom/google/wireless/tacotruck/proto/Data$Point;

    return-object p1
.end method

.method static synthetic access$54502(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39988
    iput p1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->searchRadiusMeters_:I

    return p1
.end method

.method static synthetic access$54602(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39988
    iput p1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;
    .registers 1

    .prologue
    .line 39999
    sget-object v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->defaultInstance:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;

    return-object v0
.end method

.method private getQueryTextBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 40116
    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->queryText_:Ljava/lang/Object;

    .line 40117
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 40118
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40120
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->queryText_:Ljava/lang/Object;

    .line 40123
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method public static newBuilder()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;
    .registers 1

    .prologue
    .line 40321
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;->access$53900()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;)Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;
    .registers 2
    .parameter "prototype"

    .prologue
    .line 40324
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;->access$53900()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;)Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 39988
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->getDefaultInstanceForType()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;
    .registers 2

    .prologue
    .line 40003
    sget-object v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->defaultInstance:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;

    return-object v0
.end method

.method public final getFilter(I)Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;
    .registers 3
    .parameter "index"

    .prologue
    .line 40137
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;

    return-object v0
.end method

.method public final getFilterCount()I
    .registers 2

    .prologue
    .line 40134
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getFilterList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40131
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    return-object v0
.end method

.method public final getPosition()Lcom/google/wireless/tacotruck/proto/Data$Point;
    .registers 2

    .prologue
    .line 40157
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->position_:Lcom/google/wireless/tacotruck/proto/Data$Point;

    return-object v0
.end method

.method public final getQueryText()Ljava/lang/String;
    .registers 5

    .prologue
    .line 40102
    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->queryText_:Ljava/lang/Object;

    .line 40103
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 40104
    check-cast v1, Ljava/lang/String;

    .line 40112
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 40106
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40108
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 40109
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 40110
    iput-object v2, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->queryText_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 40112
    goto :goto_8
.end method

.method public final getSearchRadiusMeters()I
    .registers 2

    .prologue
    .line 40167
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->searchRadiusMeters_:I

    return v0
.end method

.method public final getSerializedSize()I
    .registers 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    .line 40214
    iget v2, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedSerializedSize:I

    .line 40215
    .local v2, size:I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_9

    move v3, v2

    .line 40244
    .end local v2           #size:I
    .local v3, size:I
    :goto_8
    return v3

    .line 40217
    .end local v3           #size:I
    .restart local v2       #size:I
    :cond_9
    const/4 v2, 0x0

    .line 40218
    iget v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1a

    .line 40219
    invoke-direct {p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->getQueryTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/lit8 v2, v4, 0x0

    .line 40223
    :cond_1a
    const/4 v0, 0x0

    .line 40224
    .local v0, dataSize:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1c
    iget-object v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_38

    .line 40225
    iget-object v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;

    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;->getNumber()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 40224
    add-int/lit8 v1, v1, 0x1

    goto :goto_1c

    .line 40228
    :cond_38
    add-int/2addr v2, v0

    .line 40229
    iget-object v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 40231
    iget v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_55

    .line 40232
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->sort_:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;

    invoke-virtual {v5}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 40235
    :cond_55
    iget v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v6, :cond_62

    .line 40236
    iget-object v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->position_:Lcom/google/wireless/tacotruck/proto/Data$Point;

    invoke-static {v6, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 40239
    :cond_62
    iget v4, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_72

    .line 40240
    const/4 v4, 0x5

    iget v5, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->searchRadiusMeters_:I

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 40243
    :cond_72
    iput v2, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedSerializedSize:I

    move v3, v2

    .line 40244
    .end local v2           #size:I
    .restart local v3       #size:I
    goto :goto_8
.end method

.method public final getSort()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;
    .registers 2

    .prologue
    .line 40147
    iget-object v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->sort_:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;

    return-object v0
.end method

.method public final hasPosition()Z
    .registers 3

    .prologue
    .line 40154
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasQueryText()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 40099
    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasSearchRadiusMeters()Z
    .registers 3

    .prologue
    .line 40164
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasSort()Z
    .registers 3

    .prologue
    .line 40144
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 40179
    iget-byte v0, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedIsInitialized:B

    .line 40180
    .local v0, isInitialized:B
    const/4 v3, -0x1

    if-eq v0, v3, :cond_c

    if-ne v0, v1, :cond_a

    .line 40189
    :goto_9
    return v1

    :cond_a
    move v1, v2

    .line 40180
    goto :goto_9

    .line 40182
    :cond_c
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->hasPosition()Z

    move-result v3

    if-eqz v3, :cond_20

    .line 40183
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->getPosition()Lcom/google/wireless/tacotruck/proto/Data$Point;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/wireless/tacotruck/proto/Data$Point;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_20

    .line 40184
    iput-byte v2, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedIsInitialized:B

    move v1, v2

    .line 40185
    goto :goto_9

    .line 40188
    :cond_20
    iput-byte v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 39988
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;->access$53900()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 39988
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;->access$53900()Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;)Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 40251
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 7
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 40194
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->getSerializedSize()I

    .line 40195
    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_13

    .line 40196
    invoke-direct {p0}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->getQueryTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40198
    :cond_13
    const/4 v0, 0x0

    .local v0, i:I
    :goto_14
    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2e

    .line 40199
    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->filter_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;

    invoke-virtual {v1}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Filter;->getNumber()I

    move-result v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 40198
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 40201
    :cond_2e
    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3e

    .line 40202
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->sort_:Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;

    invoke-virtual {v2}, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery$Sort;->getNumber()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 40204
    :cond_3e
    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_49

    .line 40205
    iget-object v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->position_:Lcom/google/wireless/tacotruck/proto/Data$Point;

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 40207
    :cond_49
    iget v1, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_57

    .line 40208
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/wireless/tacotruck/proto/Data$SearchQuery;->searchRadiusMeters_:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 40210
    :cond_57
    return-void
.end method
