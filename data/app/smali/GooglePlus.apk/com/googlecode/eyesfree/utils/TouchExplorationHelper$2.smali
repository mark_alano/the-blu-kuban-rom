.class final Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;
.super Ljava/lang/Object;
.source "TouchExplorationHelper.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)V
    .registers 2
    .parameter

    .prologue
    .line 352
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper.2;"
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;->this$0:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "view"
    .parameter "event"

    .prologue
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper.2;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 355
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;->this$0:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    #getter for: Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->access$000(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/view/accessibility/AccessibilityManagerCompat;->isTouchExplorationEnabled(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 370
    :goto_e
    return v1

    .line 359
    :cond_f
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_34

    :pswitch_16
    goto :goto_e

    .line 362
    :pswitch_17
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;->this$0:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getItemAt(FF)Ljava/lang/Object;

    move-result-object v0

    .line 363
    .local v0, item:Ljava/lang/Object;,"TT;"
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;->this$0:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    invoke-static {v1, v0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->access$100(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;Ljava/lang/Object;)V

    move v1, v2

    .line 364
    goto :goto_e

    .line 366
    .end local v0           #item:Ljava/lang/Object;,"TT;"
    :pswitch_2c
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;->this$0:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->access$100(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;Ljava/lang/Object;)V

    move v1, v2

    .line 367
    goto :goto_e

    .line 359
    :pswitch_data_34
    .packed-switch 0x7
        :pswitch_17
        :pswitch_16
        :pswitch_17
        :pswitch_2c
    .end packed-switch
.end method
