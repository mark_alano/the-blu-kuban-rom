.class public Lcom/twitter/android/CropActivity;
.super Landroid/support/v4/app/FragmentActivity;


# instance fields
.field a:I

.field b:I

.field c:I

.field private d:Landroid/net/Uri;

.field private e:Lcom/twitter/android/widget/CroppableImageView;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CropActivity;)Landroid/net/Uri;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->d:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/CropActivity;)Lcom/twitter/android/widget/CroppableImageView;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->e:Lcom/twitter/android/widget/CroppableImageView;

    return-object v0
.end method


# virtual methods
.method public onClickHandler(Landroid/view/View;)V
    .registers 4

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_1c

    :goto_8
    return-void

    :sswitch_9
    new-instance v0, Lcom/twitter/android/v;

    invoke-direct {v0, p0}, Lcom/twitter/android/v;-><init>(Lcom/twitter/android/CropActivity;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/v;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_8

    :sswitch_14
    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->finish()V

    goto :goto_8

    nop

    :sswitch_data_1c
    .sparse-switch
        0x7f070034 -> :sswitch_14
        0x7f070051 -> :sswitch_9
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030016

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/CropActivity;->d:Landroid/net/Uri;

    const v0, 0x7f070023

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/CroppableImageView;

    iput-object v0, p0, Lcom/twitter/android/CropActivity;->e:Lcom/twitter/android/widget/CroppableImageView;

    new-instance v0, Lcom/twitter/android/w;

    invoke-direct {v0, p0}, Lcom/twitter/android/w;-><init>(Lcom/twitter/android/CropActivity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/w;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 4

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    packed-switch p1, :pswitch_data_28

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_10
    return-object v0

    :pswitch_11
    const v1, 0x7f0b004c

    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_10

    :pswitch_1c
    const v1, 0x7f0b004e

    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_10

    nop

    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_11
        :pswitch_1c
    .end packed-switch
.end method
