.class public Lcom/twitter/android/EditProfileActivity;
.super Lcom/twitter/android/BaseFragmentActivity;

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/twitter/android/util/y;
.implements Lcom/twitter/android/widget/u;
.implements Lcom/twitter/android/widget/x;


# instance fields
.field private A:Z

.field e:Lcom/twitter/android/util/f;

.field f:Landroid/net/Uri;

.field g:Landroid/net/Uri;

.field h:Landroid/net/Uri;

.field i:Landroid/net/Uri;

.field j:Z

.field k:Z

.field l:J

.field m:Ljava/lang/String;

.field n:Landroid/widget/EditText;

.field o:Landroid/widget/EditText;

.field p:Landroid/widget/EditText;

.field q:Landroid/widget/EditText;

.field r:Landroid/widget/ImageView;

.field s:Landroid/widget/ImageView;

.field private t:Landroid/graphics/Rect;

.field private u:Landroid/widget/Button;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EditProfileActivity;)Landroid/graphics/Rect;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->t:Landroid/graphics/Rect;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;Lcom/twitter/android/api/TweetEntities;)V
    .registers 16

    iput-object p1, p0, Lcom/twitter/android/EditProfileActivity;->v:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz p9, :cond_4a

    iget-object v0, p9, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_4a

    iget-object v0, p9, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4a

    const/4 v0, 0x0

    iget-object v1, p9, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetEntities$Url;

    iget v3, v0, Lcom/twitter/android/api/TweetEntities$Url;->start:I

    add-int/2addr v3, v1

    iget v4, v0, Lcom/twitter/android/api/TweetEntities$Url;->end:I

    add-int/2addr v4, v1

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/twitter/android/api/TweetEntities$Url;->displayUrl:Ljava/lang/String;

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v3, v0, Lcom/twitter/android/api/TweetEntities$Url;->displayUrl:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget v4, v0, Lcom/twitter/android/api/TweetEntities$Url;->end:I

    iget v0, v0, Lcom/twitter/android/api/TweetEntities$Url;->start:I

    sub-int v0, v4, v0

    sub-int v0, v3, v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1d

    :cond_4a
    iput-object p2, p0, Lcom/twitter/android/EditProfileActivity;->w:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->o:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz p10, :cond_6e

    iget-object v0, p10, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_6e

    iget-object v0, p10, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6e

    iget-object v0, p10, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetEntities$Url;

    iget-object v1, v0, Lcom/twitter/android/api/TweetEntities$Url;->displayUrl:Ljava/lang/String;

    if-nez v1, :cond_ba

    iget-object p3, v0, Lcom/twitter/android/api/TweetEntities$Url;->url:Ljava/lang/String;

    :cond_6e
    :goto_6e
    iput-object p3, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iput-object p4, p0, Lcom/twitter/android/EditProfileActivity;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0, p4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b0110

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileActivity;->a(I[Ljava/lang/Object;)V

    iput-wide p5, p0, Lcom/twitter/android/EditProfileActivity;->l:J

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->a:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/EditProfileActivity;->l:J

    invoke-virtual {v0, v1, v2, p7}, Lcom/twitter/android/client/b;->c(JLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/EditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    invoke-static {p0, p5, p6}, Lcom/twitter/android/util/z;->b(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_bd

    new-instance v0, Lcom/twitter/android/ak;

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/ak;-><init>(Lcom/twitter/android/EditProfileActivity;I)V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/twitter/android/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/EditProfileActivity;->k:Z

    :goto_b9
    return-void

    :cond_ba
    iget-object p3, v0, Lcom/twitter/android/api/TweetEntities$Url;->expandedUrl:Ljava/lang/String;

    goto :goto_6e

    :cond_bd
    new-instance v1, Lcom/twitter/android/util/f;

    iget v2, v0, Lcom/twitter/android/client/b;->g:F

    invoke-static {p8, v2}, Lcom/twitter/android/util/z;->b(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/android/EditProfileActivity;->e:Lcom/twitter/android/util/f;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->e:Lcom/twitter/android/util/f;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_df

    const/4 v0, 0x1

    :goto_d3
    iput-boolean v0, p0, Lcom/twitter/android/EditProfileActivity;->k:Z

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getId()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/EditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    goto :goto_b9

    :cond_df
    const/4 v0, 0x0

    goto :goto_d3
.end method

.method private e()V
    .registers 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0110

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b011b

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ec

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method private f()Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/EditProfileActivity;->j:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/android/EditProfileActivity;->k:Z

    if-nez v0, :cond_16

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    if-nez v0, :cond_16

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method


# virtual methods
.method public final a(II)V
    .registers 8

    const/4 v2, 0x2

    const v4, 0x7f0b00e3

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_100

    :cond_a
    :goto_a
    return-void

    :pswitch_b
    const/4 v0, -0x1

    if-ne p2, v0, :cond_a

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    invoke-virtual {p0, v1}, Lcom/twitter/android/EditProfileActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->finish()V

    goto :goto_a

    :pswitch_1a
    if-nez p2, :cond_6c

    iput-boolean v1, p0, Lcom/twitter/android/EditProfileActivity;->j:Z

    invoke-static {p0}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2f

    const v0, 0x7f0b00e2

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_a

    :cond_2f
    const v0, 0x7f0b0041

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "title"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "description"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->i:Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "output"

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->i:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    :try_start_61
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_64
    .catch Landroid/content/ActivityNotFoundException; {:try_start_61 .. :try_end_64} :catch_65

    goto :goto_a

    :catch_65
    move-exception v0

    invoke-static {p0, v4}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/twitter/android/EditProfileActivity;->i:Landroid/net/Uri;

    goto :goto_a

    :cond_6c
    if-ne p2, v0, :cond_83

    iput-boolean v1, p0, Lcom/twitter/android/EditProfileActivity;->j:Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v1, 0x2

    :try_start_7a
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_7d
    .catch Landroid/content/ActivityNotFoundException; {:try_start_7a .. :try_end_7d} :catch_7e

    goto :goto_a

    :catch_7e
    move-exception v0

    invoke-static {p0, v4}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;I)V

    goto :goto_a

    :cond_83
    if-ne p2, v2, :cond_a

    iput-object v3, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    iput-object v3, p0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    iput-boolean v0, p0, Lcom/twitter/android/EditProfileActivity;->j:Z

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getId()I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/twitter/android/EditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    goto/16 :goto_a

    :pswitch_96
    if-nez p2, :cond_e9

    invoke-static {p0}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_aa

    const v0, 0x7f0b00e2

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_a

    :cond_aa
    const v0, 0x7f0b0042

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "title"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "description"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "output"

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    :try_start_dc
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_df
    .catch Landroid/content/ActivityNotFoundException; {:try_start_dc .. :try_end_df} :catch_e1

    goto/16 :goto_a

    :catch_e1
    move-exception v0

    invoke-static {p0, v4}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    goto/16 :goto_a

    :cond_e9
    if-ne p2, v0, :cond_a

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v1, 0x5

    :try_start_f5
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_f8
    .catch Landroid/content/ActivityNotFoundException; {:try_start_f5 .. :try_end_f8} :catch_fa

    goto/16 :goto_a

    :catch_fa
    move-exception v0

    invoke-static {p0, v4}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;I)V

    goto/16 :goto_a

    :pswitch_data_100
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_96
        :pswitch_b
    .end packed-switch
.end method

.method final a(Landroid/graphics/Bitmap;I)V
    .registers 5

    if-eqz p1, :cond_15

    const/4 v0, 0x1

    :goto_3
    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne p2, v1, :cond_20

    iput-boolean v0, p0, Lcom/twitter/android/EditProfileActivity;->z:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_14
    :goto_14
    return-void

    :cond_15
    const/4 v0, 0x0

    goto :goto_3

    :cond_17
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    const v1, 0x7f02001e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_14

    :cond_20
    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne p2, v1, :cond_14

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_14

    :cond_30
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_14
.end method

.method public final a(Lcom/twitter/android/util/x;Ljava/util/HashMap;)V
    .registers 5

    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/android/util/x;->h:I

    if-ne v0, v1, :cond_20

    iget-wide v0, p0, Lcom/twitter/android/EditProfileActivity;->l:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/r;

    if-eqz v0, :cond_20

    invoke-virtual {v0}, Lcom/twitter/android/util/r;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    :cond_20
    return-void
.end method

.method final a()Z
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->o:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/EditProfileActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/EditProfileActivity;->A:Z

    if-nez v4, :cond_84

    iget-object v4, p0, Lcom/twitter/android/EditProfileActivity;->v:Ljava/lang/String;

    if-nez v4, :cond_36

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_84

    :cond_36
    iget-object v4, p0, Lcom/twitter/android/EditProfileActivity;->v:Ljava/lang/String;

    if-eqz v4, :cond_42

    iget-object v4, p0, Lcom/twitter/android/EditProfileActivity;->v:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_84

    :cond_42
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->w:Ljava/lang/String;

    if-nez v0, :cond_4c

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_84

    :cond_4c
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->w:Ljava/lang/String;

    if-eqz v0, :cond_58

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_84

    :cond_58
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    if-nez v0, :cond_62

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_84

    :cond_62
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    if-eqz v0, :cond_6e

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_84

    :cond_6e
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->y:Ljava/lang/String;

    if-nez v0, :cond_78

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_84

    :cond_78
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->y:Ljava/lang/String;

    if-eqz v0, :cond_86

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_86

    :cond_84
    const/4 v0, 0x1

    :goto_85
    return v0

    :cond_86
    const/4 v0, 0x0

    goto :goto_85
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_1a
    return-void

    :cond_1b
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1a
.end method

.method protected final b()V
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/EditProfileActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/twitter/android/EditProfileActivity;->e()V

    :goto_9
    return-void

    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->setResult(I)V

    invoke-super {p0}, Lcom/twitter/android/BaseFragmentActivity;->b()V

    goto :goto_9
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 9

    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_a8

    :cond_a
    :goto_a
    return-void

    :pswitch_b
    if-ne p2, v1, :cond_37

    if-ne p1, v4, :cond_27

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->i:Landroid/net/Uri;

    :goto_11
    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    if-eqz v0, :cond_2c

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/CropActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "uri"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_a

    :cond_27
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_11

    :cond_2c
    const v0, 0x7f0b004a

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_a

    :cond_37
    if-ne p1, v4, :cond_a

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    goto :goto_a

    :pswitch_3c
    if-ne p2, v1, :cond_6e

    if-eqz p3, :cond_6e

    const-string v0, "uri"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    invoke-static {p0, v1}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    new-instance v1, Lcom/twitter/android/ak;

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/ak;-><init>(Lcom/twitter/android/EditProfileActivity;I)V

    new-array v2, v4, [Landroid/net/Uri;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    const-string v0, "cropped_rect"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->t:Landroid/graphics/Rect;

    goto :goto_a

    :cond_6e
    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    goto :goto_a

    :pswitch_73
    if-ne p2, v1, :cond_a3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_90

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    :goto_7a
    if-eqz v0, :cond_97

    new-instance v1, Lcom/twitter/android/ak;

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/ak;-><init>(Lcom/twitter/android/EditProfileActivity;I)V

    new-array v2, v4, [Landroid/net/Uri;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_a

    :cond_90
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    goto :goto_7a

    :cond_97
    const v0, 0x7f0b004b

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_a

    :cond_a3
    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    goto/16 :goto_a

    nop

    :pswitch_data_a8
    .packed-switch 0x1
        :pswitch_b
        :pswitch_b
        :pswitch_3c
        :pswitch_73
        :pswitch_73
    .end packed-switch
.end method

.method public onBackPressed()V
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/EditProfileActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/twitter/android/EditProfileActivity;->e()V

    :goto_9
    return-void

    :cond_a
    invoke-super {p0}, Lcom/twitter/android/BaseFragmentActivity;->onBackPressed()V

    goto :goto_9
.end method

.method public onClickHandler(Landroid/view/View;)V
    .registers 8

    const v5, 0x7f08000d

    const v4, 0x7f08000c

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_cc

    :goto_e
    return-void

    :sswitch_f
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ar:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b003b

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/EditProfileActivity;->z:Z

    if-eqz v1, :cond_39

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    :goto_31
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_e

    :cond_39
    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    goto :goto_31

    :sswitch_3d
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b003d

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_e

    :sswitch_55
    invoke-direct {p0}, Lcom/twitter/android/EditProfileActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_b3

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a7

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8a

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8a

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_8a
    sget-object v1, Ld;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_a2

    const v0, 0x7f0b0184

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_e

    :cond_a2
    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_a7
    new-instance v0, Lcom/twitter/android/al;

    invoke-direct {v0, p0}, Lcom/twitter/android/al;-><init>(Lcom/twitter/android/EditProfileActivity;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/al;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_e

    :cond_b3
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->finish()V

    goto/16 :goto_e

    :sswitch_b8
    invoke-direct {p0}, Lcom/twitter/android/EditProfileActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_c3

    invoke-direct {p0}, Lcom/twitter/android/EditProfileActivity;->e()V

    goto/16 :goto_e

    :cond_c3
    invoke-virtual {p0, v3}, Lcom/twitter/android/EditProfileActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->finish()V

    goto/16 :goto_e

    nop

    :sswitch_data_cc
    .sparse-switch
        0x7f070034 -> :sswitch_b8
        0x7f070055 -> :sswitch_f
        0x7f070058 -> :sswitch_3d
        0x7f07005f -> :sswitch_55
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 15

    const v0, 0x7f03001a

    const/4 v1, 0x1

    invoke-super {p0, p1, v0, v1}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    iget-object v11, p0, Lcom/twitter/android/EditProfileActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v11}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-nez v0, :cond_10

    :goto_f
    return-void

    :cond_10
    const v0, 0x7f07005b

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    const v0, 0x7f07005e

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->o:Landroid/widget/EditText;

    const v0, 0x7f07005c

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->p:Landroid/widget/EditText;

    const v0, 0x7f07005d

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->q:Landroid/widget/EditText;

    const v0, 0x7f070059

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    const v0, 0x7f070056

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    const v0, 0x7f07005f

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/Button;

    invoke-virtual {v11}, Lcom/twitter/android/client/b;->d()Lcom/twitter/android/api/ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    const-string v1, "failure"

    const/4 v2, 0x0

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/EditProfileActivity;->A:Z

    iget-boolean v1, p0, Lcom/twitter/android/EditProfileActivity;->A:Z

    if-eqz v1, :cond_136

    const-string v0, "account_name"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->m:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->m:Ljava/lang/String;

    invoke-virtual {v11, v0}, Lcom/twitter/android/client/b;->b(Ljava/lang/String;)Lcom/twitter/android/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/Session;->f()Lcom/twitter/android/api/ad;

    move-result-object v0

    const-string v1, "update_profile"

    const/4 v2, 0x0

    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_12c

    const-string v1, "name"

    invoke-virtual {v12, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "description"

    invoke-virtual {v12, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "url"

    invoke-virtual {v12, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "location"

    invoke-virtual {v12, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_a5
    iget-wide v5, v0, Lcom/twitter/android/api/ad;->a:J

    iget-object v7, v0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    iget-object v8, v0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/EditProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;Lcom/twitter/android/api/TweetEntities;)V

    const-string v0, "header_uri"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_e7

    const-string v0, "crop_rect"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->t:Landroid/graphics/Rect;

    const-string v0, "header_uri"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    new-instance v0, Lcom/twitter/android/ak;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ak;-><init>(Lcom/twitter/android/EditProfileActivity;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_e7
    const-string v0, "avatar_uri"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_10f

    const-string v0, "avatar_uri"

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    new-instance v0, Lcom/twitter/android/ak;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ak;-><init>(Lcom/twitter/android/EditProfileActivity;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_10f
    :goto_10f
    new-instance v0, Lcom/twitter/android/aj;

    invoke-direct {v0, p0}, Lcom/twitter/android/aj;-><init>(Lcom/twitter/android/EditProfileActivity;)V

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->d:Lcom/twitter/android/client/j;

    const/4 v0, 0x1

    invoke-virtual {v11, v0, p0}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/util/y;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto/16 :goto_f

    :cond_12c
    iget-object v1, v0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    iget-object v3, v0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    goto/16 :goto_a5

    :cond_136
    iget-object v1, v0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/android/EditProfileActivity;->m:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    iget-object v3, v0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    iget-wide v5, v0, Lcom/twitter/android/api/ad;->a:J

    iget-object v7, v0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    iget-object v8, v0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    iget-object v9, v0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    iget-object v10, v0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/EditProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;Lcom/twitter/android/api/TweetEntities;)V

    goto :goto_10f
.end method

.method protected onDestroy()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->a:Lcom/twitter/android/client/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/b;->b(ILcom/twitter/android/util/y;)V

    invoke-super {p0}, Lcom/twitter/android/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    return-void
.end method
