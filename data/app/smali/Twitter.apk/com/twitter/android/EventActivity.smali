.class public Lcom/twitter/android/EventActivity;
.super Lcom/twitter/android/BaseFragmentActivity;

# interfaces
.implements Lcom/twitter/android/widget/af;


# instance fields
.field private e:Lcom/twitter/android/aq;

.field private f:Lcom/twitter/android/aq;

.field private g:Lcom/twitter/android/ar;

.field private h:Lcom/twitter/android/api/ad;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/BaseFragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/EventActivity;->h:Lcom/twitter/android/api/ad;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventActivity;Lcom/twitter/android/api/ad;)Lcom/twitter/android/api/ad;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/EventActivity;->h:Lcom/twitter/android/api/ad;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/EventActivity;Ljava/lang/String;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/twitter/android/EventActivity;->e(Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/content/Intent;)[Lcom/twitter/android/aq;
    .registers 8

    const/4 v5, 0x0

    const/4 v4, 0x1

    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SegmentedControl;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/SegmentedControl;->a(Lcom/twitter/android/widget/af;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/SegmentedControl;->setVisibility(I)V

    new-instance v0, Lcom/twitter/android/aq;

    invoke-static {p1, v4}, Lcom/twitter/android/EventsLandingFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v1

    const-class v2, Lcom/twitter/android/EventsLandingFragment;

    const-string v3, "events_landing"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/aq;-><init>(Landroid/os/Bundle;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/EventActivity;->e:Lcom/twitter/android/aq;

    invoke-static {p1, v4}, Lcom/twitter/android/UsersFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "follow"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "type"

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lcom/twitter/android/aq;

    const-class v2, Lcom/twitter/android/UsersFragment;

    const-string v3, "events_people"

    invoke-direct {v1, v0, v2, v3}, Lcom/twitter/android/aq;-><init>(Landroid/os/Bundle;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/android/EventActivity;->f:Lcom/twitter/android/aq;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/aq;

    iget-object v1, p0, Lcom/twitter/android/EventActivity;->e:Lcom/twitter/android/aq;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/twitter/android/EventActivity;->f:Lcom/twitter/android/aq;

    aput-object v1, v0, v4

    return-object v0
.end method

.method private e(Ljava/lang/String;)V
    .registers 4

    const v0, 0x7f070035

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/Navbar;

    const v1, 0x7f070024

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/Navbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final a_(I)V
    .registers 7

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "event_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f070054

    if-ne p1, v1, :cond_50

    if-eqz v0, :cond_50

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/PostActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "selection"

    const/4 v3, 0x2

    new-array v3, v3, [I

    aput v4, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    aput v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.twitter.android.post.status"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventActivity;->startActivity(Landroid/content/Intent;)V

    :goto_4f
    return-void

    :cond_50
    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->a_(I)V

    goto :goto_4f
.end method

.method public final b(I)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ar;->a(I)Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5

    const v0, 0x7f03001d

    const/4 v1, 0x0

    invoke-super {p0, p1, v0, v1}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    new-instance v0, Lcom/twitter/android/an;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/an;-><init>(Lcom/twitter/android/EventActivity;Lcom/twitter/android/am;)V

    iput-object v0, p0, Lcom/twitter/android/EventActivity;->d:Lcom/twitter/android/client/j;

    invoke-virtual {p0}, Lcom/twitter/android/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/EventActivity;->a(Landroid/content/Intent;)[Lcom/twitter/android/aq;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ar;

    const v2, 0x7f070036

    invoke-direct {v1, p0, v2, v0}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;I[Lcom/twitter/android/aq;)V

    iput-object v1, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    if-eqz p1, :cond_56

    iget-object v0, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    const-string v1, "state_tag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->b(Ljava/lang/String;)V

    const-string v0, "state_mediator"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/ad;

    iput-object v0, p0, Lcom/twitter/android/EventActivity;->h:Lcom/twitter/android/api/ad;

    :goto_38
    iget-object v0, p0, Lcom/twitter/android/EventActivity;->h:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_43

    iget-object v0, p0, Lcom/twitter/android/EventActivity;->h:Lcom/twitter/android/api/ad;

    iget-object v0, v0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/EventActivity;->e(Ljava/lang/String;)V

    :cond_43
    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SegmentedControl;

    iget-object v1, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    invoke-virtual {v1}, Lcom/twitter/android/ar;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/SegmentedControl;->a(I)V

    return-void

    :cond_56
    iget-object v0, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    const-string v1, "events_landing"

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->a(Ljava/lang/String;)Z

    goto :goto_38
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 5

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/EventActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f100001

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 6

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/EventActivity;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EventActivity;->e:Lcom/twitter/android/aq;

    if-eqz v1, :cond_1f

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EventActivity;->e:Lcom/twitter/android/aq;

    invoke-virtual {v2, p0}, Lcom/twitter/android/aq;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_1f
    iget-object v1, p0, Lcom/twitter/android/EventActivity;->f:Lcom/twitter/android/aq;

    if-eqz v1, :cond_34

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EventActivity;->f:Lcom/twitter/android/aq;

    invoke-virtual {v2, p0}, Lcom/twitter/android/aq;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_34
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    iget-object v0, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    invoke-virtual {v0}, Lcom/twitter/android/ar;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/EventActivity;->a(Landroid/content/Intent;)[Lcom/twitter/android/aq;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ar;

    const v3, 0x7f070036

    invoke-direct {v2, p0, v3, v1}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;I[Lcom/twitter/android/aq;)V

    iput-object v2, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    const-string v1, "events_landing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6d

    iget-object v0, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    const-string v1, "events_landing"

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->a(Ljava/lang/String;)Z

    :goto_5a
    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SegmentedControl;

    iget-object v1, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    invoke-virtual {v1}, Lcom/twitter/android/ar;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/SegmentedControl;->a(I)V

    return-void

    :cond_6d
    iget-object v0, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    const-string v1, "events_people"

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->a(Ljava/lang/String;)Z

    goto :goto_5a
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 9

    const/4 v6, 0x0

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_86

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_d
    return v0

    :pswitch_e
    const-string v0, "search_tweets"

    iget-object v2, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    invoke-virtual {v2}, Lcom/twitter/android/ar;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "events_people"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5e

    const-string v0, "search_users"

    new-instance v2, Lcom/twitter/android/service/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/EventActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v3

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->F:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/android/service/ScribeLog;-><init>(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "event_name"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/android/service/ScribeLog;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/EventActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v3, v2}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/service/ScribeLog;)V

    :cond_3e
    :goto_3e
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/SearchActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "current_segment"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "event_redirect"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    goto :goto_d

    :cond_5e
    const-string v3, "search_tweets"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3e

    new-instance v2, Lcom/twitter/android/service/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/EventActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v3

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->E:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/android/service/ScribeLog;-><init>(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "event_name"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/android/service/ScribeLog;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/EventActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v3, v2}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/service/ScribeLog;)V

    goto :goto_3e

    nop

    :pswitch_data_86
    .packed-switch 0x7f0700fb
        :pswitch_e
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "state_tag"

    iget-object v1, p0, Lcom/twitter/android/EventActivity;->g:Lcom/twitter/android/ar;

    invoke-virtual {v1}, Lcom/twitter/android/ar;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "state_mediator"

    iget-object v1, p0, Lcom/twitter/android/EventActivity;->h:Lcom/twitter/android/api/ad;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
