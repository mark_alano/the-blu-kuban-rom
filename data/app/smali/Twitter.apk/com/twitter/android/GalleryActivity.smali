.class public Lcom/twitter/android/GalleryActivity;
.super Lcom/twitter/android/BaseFragmentActivity;

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/twitter/android/util/y;


# instance fields
.field private A:Z

.field private e:Landroid/net/Uri;

.field private f:Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:Lcom/twitter/android/av;

.field private j:Lcom/twitter/android/widget/TweetView;

.field private k:Landroid/support/v4/view/ViewPager;

.field private l:J

.field private m:I

.field private n:Z

.field private o:Landroid/view/animation/Animation;

.field private p:Landroid/view/animation/Animation;

.field private q:Landroid/view/ViewGroup;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/ImageButton;

.field private t:Landroid/widget/ImageButton;

.field private u:Lcom/twitter/android/client/Session;

.field private v:Lcom/twitter/android/provider/m;

.field private w:I

.field private x:Lcom/twitter/android/widget/au;

.field private y:Ljava/util/WeakHashMap;

.field private z:I


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/BaseFragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/GalleryActivity;->w:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;)Landroid/widget/ImageButton;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->t:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;I)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/twitter/android/GalleryActivity;->b(I)V

    return-void
.end method

.method private a(Z)V
    .registers 4

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->n:Z

    if-ne v0, p1, :cond_5

    :goto_4
    return-void

    :cond_5
    if-eqz p1, :cond_11

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_e
    iput-boolean p1, p0, Lcom/twitter/android/GalleryActivity;->n:Z

    goto :goto_4

    :cond_11
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->p:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_e
.end method

.method static synthetic b(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/provider/m;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->v:Lcom/twitter/android/provider/m;

    return-object v0
.end method

.method private b(I)V
    .registers 6

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v2, :cond_13

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_13
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/av;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/android/av;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/widget/au;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->x:Lcom/twitter/android/widget/au;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/GalleryActivity;)Ljava/util/WeakHashMap;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/GalleryActivity;)Landroid/view/animation/Animation;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/GalleryActivity;)Landroid/view/animation/Animation;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->p:Landroid/view/animation/Animation;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/twitter/android/util/x;Ljava/util/HashMap;)V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->v:Lcom/twitter/android/provider/m;

    if-eqz v0, :cond_17

    iget-wide v0, v0, Lcom/twitter/android/provider/m;->n:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/r;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/android/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetView;->b()V

    :cond_17
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 14

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->v:Lcom/twitter/android/provider/m;

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_ca

    :goto_d
    return-void

    :pswitch_e
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PostActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "reply_to_tweet"

    new-array v2, v6, [Lcom/twitter/android/provider/m;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->v:Lcom/twitter/android/provider/m;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.twitter.android.post.reply"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    invoke-virtual {v0}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v0

    sget-object v2, Lcom/twitter/android/service/ScribeEvent;->ac:Lcom/twitter/android/service/ScribeEvent;

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_d

    :pswitch_44
    iget-boolean v0, v3, Lcom/twitter/android/provider/m;->l:Z

    if-eqz v0, :cond_6d

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    iget-wide v1, v3, Lcom/twitter/android/provider/m;->o:J

    iget-object v5, v3, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v4, v0, v1, v2, v5}, Lcom/twitter/android/client/b;->c(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    iput-boolean v7, v3, Lcom/twitter/android/provider/m;->l:Z

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->s:Landroid/widget/ImageButton;

    const v2, 0x7f0200e9

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ag:Lcom/twitter/android/service/ScribeEvent;

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v1, v2, v3, v5}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    :goto_69
    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->c(Ljava/lang/String;)V

    goto :goto_d

    :cond_6d
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    iget-wide v1, v3, Lcom/twitter/android/provider/m;->o:J

    iget-object v5, v3, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v4, v0, v1, v2, v5}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    iput-boolean v6, v3, Lcom/twitter/android/provider/m;->l:Z

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->s:Landroid/widget/ImageButton;

    const v2, 0x7f0200ec

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->af:Lcom/twitter/android/service/ScribeEvent;

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v1, v2, v3, v5}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_69

    :pswitch_8f
    invoke-virtual {v4}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/twitter/android/provider/m;->a(J)Z

    move-result v2

    new-instance v0, Lcom/twitter/android/as;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/as;-><init>(Lcom/twitter/android/GalleryActivity;ZLcom/twitter/android/provider/m;Lcom/twitter/android/client/b;J)V

    invoke-static {p0, v2, v0}, Lcom/twitter/android/client/b;->a(Landroid/app/Activity;ZLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_d

    :pswitch_a6
    invoke-virtual {p0, v6}, Lcom/twitter/android/GalleryActivity;->showDialog(I)V

    goto/16 :goto_d

    :pswitch_ab
    invoke-virtual {v3}, Lcom/twitter/android/provider/m;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v3, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    iget-object v7, v3, Lcom/twitter/android/provider/m;->d:Ljava/lang/String;

    iget-wide v8, v3, Lcom/twitter/android/provider/m;->h:J

    iget-wide v10, v3, Lcom/twitter/android/provider/m;->o:J

    invoke-virtual/range {v4 .. v11}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    sget-object v2, Lcom/twitter/android/service/ScribeEvent;->ah:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v0, v1, v2}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_d

    nop

    :pswitch_data_ca
    .packed-switch 0x7f07001e
        :pswitch_e
        :pswitch_8f
        :pswitch_44
        :pswitch_ab
        :pswitch_a6
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 12

    const-wide/16 v8, 0x96

    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v0, 0x7f03003c

    invoke-super {p0, p1, v0, v2}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-nez v0, :cond_14

    :goto_13
    return-void

    :cond_14
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/net/Uri;

    const-string v4, "prj"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->h:[Ljava/lang/String;

    const-string v4, "sel"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->f:Ljava/lang/String;

    const-string v4, "selArgs"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->g:[Ljava/lang/String;

    const-string v4, "id"

    const-wide/high16 v5, -0x8000

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/GalleryActivity;->l:J

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    new-instance v0, Lcom/twitter/android/av;

    invoke-direct {v0, v3}, Lcom/twitter/android/av;-><init>(Lcom/twitter/android/client/b;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/android/av;

    const v0, 0x7f070085

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/android/av;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->k:Landroid/support/v4/view/ViewPager;

    const v0, 0x7f070086

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    const v4, 0x7f07001d

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/widget/LinearLayout;

    const v4, 0x7f070020

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->s:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/widget/LinearLayout;

    const v4, 0x7f07001f

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->t:Landroid/widget/ImageButton;

    new-instance v0, Lcom/twitter/android/widget/au;

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    const v5, 0x7f070088

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {v0, v4, v7}, Lcom/twitter/android/widget/au;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->x:Lcom/twitter/android/widget/au;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Ljava/util/WeakHashMap;

    if-eqz p1, :cond_ca

    const-string v0, "cv"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12b

    :cond_ca
    move v0, v2

    :goto_cb
    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->n:Z

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->q:Landroid/view/ViewGroup;

    const v4, 0x7f070087

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetView;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/android/widget/TweetView;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/android/widget/TweetView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/widget/az;)V

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/GalleryActivity;->m:I

    new-instance v0, Lcom/twitter/android/au;

    invoke-direct {v0, p0}, Lcom/twitter/android/au;-><init>(Lcom/twitter/android/GalleryActivity;)V

    const/high16 v4, 0x7f04

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v4, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {v4, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/view/animation/Animation;

    const v4, 0x7f040001

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v4, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {v4, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    iput-object v4, p0, Lcom/twitter/android/GalleryActivity;->p:Landroid/view/animation/Animation;

    new-instance v0, Lcom/twitter/android/ax;

    invoke-direct {v0, p0}, Lcom/twitter/android/ax;-><init>(Lcom/twitter/android/GalleryActivity;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/android/client/j;

    invoke-virtual {v3, v2, p0}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/util/y;)V

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v7, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->n:Z

    if-eqz v0, :cond_12d

    invoke-direct {p0, v1}, Lcom/twitter/android/GalleryActivity;->b(I)V

    goto/16 :goto_13

    :cond_12b
    move v0, v1

    goto :goto_cb

    :cond_12d
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/GalleryActivity;->b(I)V

    goto/16 :goto_13
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_45

    new-instance v1, Lcom/twitter/android/at;

    invoke-direct {v1, p0}, Lcom/twitter/android/at;-><init>(Lcom/twitter/android/GalleryActivity;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b001a

    invoke-virtual {p0, v3}, Lcom/twitter/android/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b005a

    invoke-virtual {p0, v3}, Lcom/twitter/android/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b0059

    invoke-virtual {p0, v3}, Lcom/twitter/android/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b00ec

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :cond_45
    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 10

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->e:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->h:[Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/GalleryActivity;->g:[Ljava/lang/String;

    const-string v6, "type DESC"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .registers 3

    invoke-super {p0}, Lcom/twitter/android/BaseFragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->a:Lcom/twitter/android/client/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/b;->b(ILcom/twitter/android/util/y;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 10

    const-wide/high16 v5, -0x8000

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/android/av;

    invoke-virtual {v0, p2}, Lcom/twitter/android/av;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget v0, p0, Lcom/twitter/android/GalleryActivity;->w:I

    iget-wide v1, p0, Lcom/twitter/android/GalleryActivity;->l:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_31

    if-eqz p2, :cond_2b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2b

    :cond_19
    const/16 v1, 0x17

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iget-wide v3, p0, Lcom/twitter/android/GalleryActivity;->l:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_35

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iput-wide v5, p0, Lcom/twitter/android/GalleryActivity;->l:J

    :cond_2b
    :goto_2b
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->k:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    :cond_31
    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->onPageSelected(I)V

    return-void

    :cond_35
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_19

    goto :goto_2b
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/android/av;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/av;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .registers 2

    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 4

    return-void
.end method

.method public onPageSelected(I)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Lcom/twitter/android/av;

    invoke-virtual {v0}, Lcom/twitter/android/av;->getCount()I

    move-result v1

    if-lez v1, :cond_61

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->x:Lcom/twitter/android/widget/au;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/au;->a(Lcom/twitter/android/api/a;)V

    const v2, 0x7f0b01ef

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v5, p1, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/GalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/GalleryActivity;->b(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/twitter/android/av;->a(I)Lcom/twitter/android/provider/m;

    move-result-object v1

    if-eqz v1, :cond_61

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->j:Lcom/twitter/android/widget/TweetView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/provider/m;)V

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->v:Lcom/twitter/android/provider/m;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/client/Session;

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->r:Landroid/widget/LinearLayout;

    const v4, 0x7f0200f8

    const v5, 0x7f0200f5

    const v6, 0x7f0200ec

    const v7, 0x7f0200e9

    move-object v3, p0

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/widget/at;->a(Lcom/twitter/android/client/Session;Lcom/twitter/android/provider/m;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;IIII)V

    iget-wide v1, v1, Lcom/twitter/android/provider/m;->o:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v3}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/a;

    if-eqz v0, :cond_64

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->x:Lcom/twitter/android/widget/au;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/au;->a(Lcom/twitter/android/api/a;)V

    :cond_61
    :goto_61
    iput p1, p0, Lcom/twitter/android/GalleryActivity;->w:I

    return-void

    :cond_64
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->y:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v3}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_61

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/b;->f(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->c(Ljava/lang/String;)V

    goto :goto_61
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "cv"

    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    packed-switch v2, :pswitch_data_40

    :cond_10
    :goto_10
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p2}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_17
    iput v3, p0, Lcom/twitter/android/GalleryActivity;->z:I

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->A:Z

    goto :goto_10

    :pswitch_1c
    iget-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->A:Z

    if-eqz v2, :cond_27

    iget-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->n:Z

    if-nez v2, :cond_2c

    :goto_24
    invoke-direct {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(Z)V

    :cond_27
    iput v1, p0, Lcom/twitter/android/GalleryActivity;->z:I

    iput-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->A:Z

    goto :goto_10

    :cond_2c
    move v0, v1

    goto :goto_24

    :pswitch_2e
    iget v0, p0, Lcom/twitter/android/GalleryActivity;->z:I

    sub-int v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/twitter/android/GalleryActivity;->m:I

    if-lt v0, v2, :cond_10

    iput-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->A:Z

    invoke-direct {p0, v1}, Lcom/twitter/android/GalleryActivity;->a(Z)V

    goto :goto_10

    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_17
        :pswitch_1c
        :pswitch_2e
    .end packed-switch
.end method
