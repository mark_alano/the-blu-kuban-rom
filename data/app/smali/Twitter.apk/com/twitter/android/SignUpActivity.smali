.class public Lcom/twitter/android/SignUpActivity;
.super Lcom/twitter/android/BaseActivity;

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field d:Landroid/widget/EditText;

.field e:Landroid/widget/EditText;

.field f:Lcom/twitter/android/widget/PopupEditText;

.field g:Landroid/widget/EditText;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/TextView;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Ljava/lang/String;

.field m:Landroid/graphics/Bitmap;

.field n:Ljava/lang/String;

.field o:I

.field p:I

.field q:I

.field r:I

.field s:Lcom/twitter/android/fb;

.field private t:I

.field private u:I

.field private v:Landroid/widget/Button;

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Lcom/twitter/android/fc;

.field private y:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/twitter/android/BaseActivity;-><init>(Z)V

    new-instance v0, Lcom/twitter/android/fc;

    invoke-direct {v0, p0}, Lcom/twitter/android/fc;-><init>(Lcom/twitter/android/SignUpActivity;)V

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->x:Lcom/twitter/android/fc;

    iput-boolean v1, p0, Lcom/twitter/android/SignUpActivity;->y:Z

    return-void
.end method

.method private a(Landroid/widget/EditText;Landroid/widget/TextView;I)I
    .registers 7

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Z)V

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v1}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-lt v1, p3, :cond_10

    const/4 v0, 0x2

    :cond_f
    :goto_f
    return v0

    :cond_10
    const/4 v2, 0x1

    if-le p3, v2, :cond_f

    if-ge v1, p3, :cond_f

    const/4 v0, 0x3

    goto :goto_f
.end method

.method private static a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z
    .registers 4

    if-nez p0, :cond_10

    invoke-virtual {p1}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_10

    invoke-virtual {p2}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method final a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 5

    if-eqz p3, :cond_f

    iget v0, p0, Lcom/twitter/android/SignUpActivity;->t:I

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_e
    return-void

    :cond_f
    iget v0, p0, Lcom/twitter/android/SignUpActivity;->u:I

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_e
.end method

.method final a(Landroid/widget/EditText;Z)V
    .registers 6

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/widget/EditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_10

    move-object v0, v1

    :goto_8
    if-eqz p2, :cond_14

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0, v1, v2, v1}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :goto_f
    return-void

    :cond_10
    const/4 v2, 0x0

    aget-object v0, v0, v2

    goto :goto_8

    :cond_14
    invoke-virtual {p1, v0, v1, v1, v1}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_f
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 8

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_45

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->h:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SignUpActivity;->o:I

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3a

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_3a

    iput-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->y:Z

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->f()V

    :cond_3a
    :goto_3a
    if-ne v2, v1, :cond_9b

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->x:Lcom/twitter/android/fc;

    invoke-virtual {v1, v0}, Lcom/twitter/android/fc;->a(I)V

    :goto_41
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->c()V

    return-void

    :cond_45
    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5e

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->i:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SignUpActivity;->p:I

    move v2, v0

    move v0, v1

    goto :goto_3a

    :cond_5e
    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v3}, Lcom/twitter/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8f

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_89

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PopupEditText;->a()V

    :goto_79
    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->j:Landroid/widget/TextView;

    invoke-direct {p0, v0, v3, v2}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SignUpActivity;->q:I

    iget v0, p0, Lcom/twitter/android/SignUpActivity;->q:I

    move v5, v2

    move v2, v0

    move v0, v5

    goto :goto_3a

    :cond_89
    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PopupEditText;->b()V

    goto :goto_79

    :cond_8f
    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->k:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SignUpActivity;->r:I

    const/4 v0, 0x4

    goto :goto_3a

    :cond_9b
    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->x:Lcom/twitter/android/fc;

    invoke-virtual {v1, v0}, Lcom/twitter/android/fc;->removeMessages(I)V

    goto :goto_41
.end method

.method final b()Z
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PopupEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->s:Lcom/twitter/android/fb;

    invoke-virtual {v0}, Lcom/twitter/android/fb;->getCount()I

    move-result v0

    if-lez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    return-void
.end method

.method final c()V
    .registers 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->v:Landroid/widget/Button;

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->o:I

    if-ne v2, v0, :cond_17

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->p:I

    if-ne v2, v0, :cond_17

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->q:I

    if-ne v2, v0, :cond_17

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->r:I

    if-ne v2, v0, :cond_17

    :goto_13
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_17
    const/4 v0, 0x0

    goto :goto_13
.end method

.method final d()V
    .registers 8

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->showDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->a:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v2}, Lcom/twitter/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/SignUpActivity;->l:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/SignUpActivity;->n:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method final e()V
    .registers 3

    const v0, 0x7f0b0149

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method final f()V
    .registers 4

    iget-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->y:Z

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2d

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2d

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/client/b;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->b(Ljava/lang/String;)V

    :cond_2d
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_32

    :cond_7
    :goto_7
    return-void

    :sswitch_8
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PopupEditText;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PopupEditText;->b()V

    goto :goto_7

    :cond_1c
    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PopupEditText;->a()V

    goto :goto_7

    :sswitch_22
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->d()V

    goto :goto_7

    :sswitch_26
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProxySettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_7

    nop

    :sswitch_data_32
    .sparse-switch
        0x7f070029 -> :sswitch_8
        0x7f070073 -> :sswitch_26
        0x7f0700a9 -> :sswitch_22
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6

    const/4 v2, 0x2

    const v0, 0x7f030049

    invoke-super {p0, p1, v0}, Lcom/twitter/android/BaseActivity;->a(Landroid/os/Bundle;I)V

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/SignUpActivity;->w:Landroid/graphics/drawable/Drawable;

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/SignUpActivity;->u:I

    const v1, 0x7f0a0016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SignUpActivity;->t:I

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    const v0, 0x7f0700aa

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v0, 0x7f070028

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    const v0, 0x7f0700a4

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    const v0, 0x7f070029

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PopupEditText;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    const v0, 0x7f0700a7

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    new-instance v0, Lcom/twitter/android/fb;

    invoke-direct {v0}, Lcom/twitter/android/fb;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->s:Lcom/twitter/android/fb;

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->s:Lcom/twitter/android/fb;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PopupEditText;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PopupEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f0700a3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->h:Landroid/widget/TextView;

    const v0, 0x7f0700a5

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->i:Landroid/widget/TextView;

    const v0, 0x7f0700a6

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->j:Landroid/widget/TextView;

    const v0, 0x7f0700a8

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PopupEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0700a9

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->v:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->v:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PopupEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_ef

    array-length v1, v0

    if-nez v1, :cond_112

    :cond_ef
    :goto_ef
    const v0, 0x7f070073

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/twitter/android/fa;

    invoke-direct {v0, p0}, Lcom/twitter/android/fa;-><init>(Lcom/twitter/android/SignUpActivity;)V

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->b:Lcom/twitter/android/client/j;

    if-nez p1, :cond_111

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->a:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ax:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    :cond_111
    return-void

    :cond_112
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_ef

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iput v2, p0, Lcom/twitter/android/SignUpActivity;->p:I

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->x:Lcom/twitter/android/fc;

    invoke-virtual {v0, v2}, Lcom/twitter/android/fc;->a(I)V

    goto :goto_ef
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 6

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_58

    invoke-super {p0, p1}, Lcom/twitter/android/BaseActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_8
    return-object v0

    :pswitch_9
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0b0146

    invoke-virtual {p0, v1}, Lcom/twitter/android/SignUpActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_8

    :pswitch_23
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ey;

    invoke-direct {v1, p0}, Lcom/twitter/android/ey;-><init>(Lcom/twitter/android/SignUpActivity;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b0147

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0b0137

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0b00ec

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_8

    nop

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_9
        :pswitch_23
    .end packed-switch
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .registers 8

    const v4, 0x7f0b01b5

    const/4 v3, 0x3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_b6

    :cond_b
    :goto_b
    return-void

    :sswitch_c
    if-nez p2, :cond_b

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->y:Z

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->f()V

    goto :goto_b

    :sswitch_29
    if-nez p2, :cond_b

    iget v0, p0, Lcom/twitter/android/SignUpActivity;->p:I

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->i:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/SignUpActivity;->a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->i:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/twitter/android/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_b

    :cond_4e
    iget v0, p0, Lcom/twitter/android/SignUpActivity;->p:I

    if-ne v0, v3, :cond_b

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->x:Lcom/twitter/android/fc;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/fc;->b(I)V

    goto :goto_b

    :sswitch_59
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_65

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PopupEditText;->a()V

    goto :goto_b

    :cond_65
    if-nez p2, :cond_b

    iget v0, p0, Lcom/twitter/android/SignUpActivity;->q:I

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->j:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/SignUpActivity;->a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_82

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->j:Landroid/widget/TextView;

    const v2, 0x7f0b01b6

    invoke-virtual {p0, v2}, Lcom/twitter/android/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_b

    :cond_82
    iget v0, p0, Lcom/twitter/android/SignUpActivity;->q:I

    if-ne v0, v3, :cond_b

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->x:Lcom/twitter/android/fc;

    invoke-virtual {v0, v3}, Lcom/twitter/android/fc;->b(I)V

    goto :goto_b

    :sswitch_8c
    if-nez p2, :cond_b

    iget v0, p0, Lcom/twitter/android/SignUpActivity;->r:I

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->k:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/SignUpActivity;->a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_aa

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->k:Landroid/widget/TextView;

    const v2, 0x7f0b01b7

    invoke-virtual {p0, v2}, Lcom/twitter/android/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto/16 :goto_b

    :cond_aa
    iget v0, p0, Lcom/twitter/android/SignUpActivity;->r:I

    if-ne v0, v3, :cond_b

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->x:Lcom/twitter/android/fc;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/fc;->b(I)V

    goto/16 :goto_b

    :sswitch_data_b6
    .sparse-switch
        0x7f070028 -> :sswitch_c
        0x7f070029 -> :sswitch_59
        0x7f0700a4 -> :sswitch_29
        0x7f0700a7 -> :sswitch_8c
    .end sparse-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 5

    packed-switch p1, :pswitch_data_26

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    :goto_6
    return-void

    :pswitch_7
    check-cast p2, Landroid/app/AlertDialog;

    const v0, 0x7f070045

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v0, 0x7f070046

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    nop

    :pswitch_data_26
    .packed-switch 0x2
        :pswitch_7
    .end packed-switch
.end method

.method public onSearchRequested()Z
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    return-void
.end method
