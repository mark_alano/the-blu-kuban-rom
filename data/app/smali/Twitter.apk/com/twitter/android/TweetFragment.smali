.class public Lcom/twitter/android/TweetFragment;
.super Lcom/twitter/android/TweetListFragment;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/widget/an;
.implements Lcom/twitter/android/widget/as;
.implements Lcom/twitter/android/widget/x;


# static fields
.field private static final v:Landroid/support/v4/util/LruCache;


# instance fields
.field m:Lcom/twitter/android/client/Session;

.field n:Lcom/twitter/android/provider/m;

.field o:Lcom/twitter/android/api/TweetMedia;

.field p:Landroid/content/Intent;

.field q:Lcom/twitter/android/api/PromotedContent;

.field r:Lcom/twitter/android/widget/TweetDetailView;

.field s:Z

.field t:Lcom/twitter/android/api/a;

.field u:Lcom/twitter/android/fu;

.field private w:Z

.field private x:I

.field private y:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/twitter/android/TweetFragment;->v:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "en_gest"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;)Landroid/widget/ImageButton;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->y:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private c(J)V
    .registers 6

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pc"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private g(I)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/api/PromotedContent;)V

    :cond_b
    return-void
.end method

.method private m()V
    .registers 8

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/PostActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.extra.TEXT"

    const/high16 v3, 0x7f0b

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v0, v0, Lcom/twitter/android/provider/m;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/TweetFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.twitter.android.post.quote"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ad:Lcom/twitter/android/service/ScribeEvent;

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .registers 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->l()V

    return-void
.end method

.method public final a(II)V
    .registers 9

    const/4 v3, -0x3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    packed-switch p1, :pswitch_data_58

    :cond_9
    :goto_9
    return-void

    :pswitch_a
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    if-ne p2, v2, :cond_2a

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v3, v3, Lcom/twitter/android/provider/m;->o:J

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->ab:Lcom/twitter/android/service/ScribeEvent;

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_9

    :cond_2a
    if-ne p2, v3, :cond_9

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->m()V

    goto :goto_9

    :pswitch_30
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    if-ne p2, v2, :cond_41

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v3, v3, Lcom/twitter/android/provider/m;->A:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;J)Ljava/lang/String;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_9

    :cond_41
    if-ne p2, v3, :cond_9

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->m()V

    goto :goto_9

    :pswitch_47
    if-ne p2, v2, :cond_9

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v3, v3, Lcom/twitter/android/provider/m;->A:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;J)Ljava/lang/String;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_9

    :pswitch_data_58
    .packed-switch 0x65
        :pswitch_a
        :pswitch_30
        :pswitch_47
    .end packed-switch
.end method

.method public final a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    invoke-virtual {v0}, Lcom/twitter/android/fu;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_30

    if-eqz p2, :cond_30

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_30

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    :cond_1a
    new-instance v1, Lcom/twitter/android/provider/m;

    invoke-direct {v1, p2}, Lcom/twitter/android/provider/m;-><init>(Landroid/database/Cursor;)V

    const/4 v2, 0x2

    iput v2, v1, Lcom/twitter/android/provider/m;->t:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1a

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    invoke-virtual {v1, v0}, Lcom/twitter/android/fu;->a(Ljava/util/ArrayList;)V

    :cond_30
    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->s:Z

    if-nez v0, :cond_3c

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->g:Lcom/twitter/android/bg;

    invoke-virtual {v0}, Lcom/twitter/android/bg;->b()Z

    move-result v0

    if-eqz v0, :cond_68

    :cond_3c
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    invoke-virtual {v0}, Lcom/twitter/android/fu;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v0, v0, Lcom/twitter/android/provider/m;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5a

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    invoke-virtual {v1, v2}, Lcom/twitter/android/fu;->a(Lcom/twitter/android/provider/m;)I

    move-result v1

    iget v2, p0, Lcom/twitter/android/TweetFragment;->x:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_5a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->s:Z

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/TweetFragment;->h:J

    const-string v3, "tweet:complete"

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/service/ScribeService;->a(Landroid/content/Context;JLjava/lang/String;)V

    :cond_68
    return-void
.end method

.method public final a(Landroid/view/View;I[J)V
    .registers 8

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/UsersActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "owner_id"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tag"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v2, v2, Lcom/twitter/android/provider/m;->o:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "user_ids"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 9

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    invoke-static {p4, p5, v1, v2}, Lcom/twitter/android/provider/o;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Lcom/twitter/android/api/TweetEntities$Url;)V
    .registers 8

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetFragment;->g(I)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->ae:Lcom/twitter/android/service/ScribeEvent;

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v3, v1, v2, v4, v5}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    invoke-static {v0, p1, v1, v2}, Lcom/twitter/android/BaseActivity;->a(Landroid/content/Context;Lcom/twitter/android/api/TweetEntities$Url;J)V

    return-void
.end method

.method public final a(Lcom/twitter/android/api/TweetMedia;)V
    .registers 9

    const v6, 0x7f0b00e3

    const/4 v5, 0x0

    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget v1, p1, Lcom/twitter/android/api/TweetMedia;->type:I

    packed-switch v1, :pswitch_data_f4

    :goto_12
    return-void

    :pswitch_13
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->W:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget v1, v1, Lcom/twitter/android/client/b;->g:F

    invoke-virtual {p1, v1}, Lcom/twitter/android/api/TweetMedia;->a(F)Lcom/twitter/android/api/m;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/android/api/m;->a:Ljava/lang/String;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/ImageActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.intent.extra.STREAM"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.extra.TEXT"

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-object v4, v4, Lcom/twitter/android/provider/m;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "image_url"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_12

    :pswitch_57
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->X:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    iget-object v1, p1, Lcom/twitter/android/api/TweetMedia;->playerStreamUrl:Ljava/lang/String;

    if-eqz v1, :cond_a7

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/MediaPlayerActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p1, Lcom/twitter/android/api/TweetMedia;->playerStreamUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "t"

    iget-object v3, p1, Lcom/twitter/android/api/TweetMedia;->title:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/api/TweetMedia;->authorUser:Lcom/twitter/android/api/TweetMedia$User;

    if-eqz v2, :cond_8e

    const-string v2, "a"

    iget-object v3, p1, Lcom/twitter/android/api/TweetMedia;->authorUser:Lcom/twitter/android/api/TweetMedia$User;

    iget-object v3, v3, Lcom/twitter/android/api/TweetMedia$User;->fullName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_8e
    iget v2, p1, Lcom/twitter/android/api/TweetMedia;->playerType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a2

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget v2, v2, Lcom/twitter/android/client/b;->g:F

    invoke-virtual {p1, v2}, Lcom/twitter/android/api/TweetMedia;->a(F)Lcom/twitter/android/api/m;

    move-result-object v2

    const-string v3, "image_url"

    iget-object v2, v2, Lcom/twitter/android/api/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_a2
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_12

    :cond_a7
    :try_start_a7
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/twitter/android/api/TweetMedia;->playerUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_bb
    .catch Landroid/content/ActivityNotFoundException; {:try_start_a7 .. :try_end_bb} :catch_bd

    goto/16 :goto_12

    :catch_bd
    move-exception v1

    invoke-static {v0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_12

    :pswitch_c7
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->V:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    :try_start_d4
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/twitter/android/api/TweetMedia;->url:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_e8
    .catch Landroid/content/ActivityNotFoundException; {:try_start_d4 .. :try_end_e8} :catch_ea

    goto/16 :goto_12

    :catch_ea
    move-exception v1

    invoke-static {v0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_12

    :pswitch_data_f4
    .packed-switch 0x1
        :pswitch_13
        :pswitch_57
        :pswitch_c7
    .end packed-switch
.end method

.method final a(Lcom/twitter/android/provider/m;Lcom/twitter/android/client/Session;)V
    .registers 15

    const/4 v3, 0x0

    const/4 v11, 0x1

    iput-object p2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-object v0, p1, Lcom/twitter/android/provider/m;->M:[Lcom/twitter/android/api/TweetMedia;

    if-eqz v0, :cond_11

    array-length v1, v0

    if-lez v1, :cond_11

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/android/api/TweetMedia;

    :cond_11
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v9, p0, Lcom/twitter/android/TweetFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03005f

    invoke-virtual {v1, v2, v9, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/TweetDetailView;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->r:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v2, v1, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    if-nez v2, :cond_42

    iget-object v2, p1, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    if-eqz v2, :cond_42

    iget-object v2, p1, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    iput-object v2, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    const/4 v2, 0x6

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetFragment;->g(I)V

    :cond_42
    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v10

    if-eqz v10, :cond_74

    iget-object v1, v1, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    const v2, 0x7f07001d

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/view/ViewGroup;

    const v1, 0x7f070020

    invoke-virtual {v8, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->y:Landroid/widget/ImageButton;

    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/provider/m;->d:Ljava/lang/String;

    iget-wide v4, p1, Lcom/twitter/android/provider/m;->h:J

    iget-wide v6, p1, Lcom/twitter/android/provider/m;->o:J

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->p:Landroid/content/Intent;

    invoke-static {p2, p1, v8, p0}, Lcom/twitter/android/widget/at;->a(Lcom/twitter/android/client/Session;Lcom/twitter/android/provider/m;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)V

    :cond_74
    new-instance v1, Lcom/twitter/android/bk;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->g:Lcom/twitter/android/bg;

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/bk;-><init>(Lcom/twitter/android/client/b;Lcom/twitter/android/bg;)V

    new-instance v2, Lcom/twitter/android/fu;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    invoke-direct {v2, p0, v1, v3}, Lcom/twitter/android/fu;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/widget/az;Lcom/twitter/android/provider/m;)V

    iput-object v2, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    invoke-virtual {v9, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iput-boolean v11, p0, Lcom/twitter/android/TweetFragment;->s:Z

    iget-wide v1, p1, Lcom/twitter/android/provider/m;->o:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    sget-object v1, Lcom/twitter/android/TweetFragment;->v:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v7}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-boolean v2, p0, Lcom/twitter/android/TweetFragment;->w:Z

    if-nez v2, :cond_af

    if-eqz v1, :cond_c3

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/32 v3, 0x493e0

    add-long/2addr v1, v3

    cmp-long v1, v8, v1

    if-gez v1, :cond_c3

    :cond_af
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v11, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :goto_b7
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/android/api/a;

    if-eqz v1, :cond_dd

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->r:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/android/api/a;

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/api/a;Lcom/twitter/android/widget/as;)V

    :cond_c2
    :goto_c2
    return-void

    :cond_c3
    iput-boolean v11, p0, Lcom/twitter/android/TweetFragment;->w:Z

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v3, p1, Lcom/twitter/android/provider/m;->o:J

    iget-wide v5, p1, Lcom/twitter/android/provider/m;->j:J

    move-object v2, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/Session;JJ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->b(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/android/TweetFragment;->v:Landroid/support/v4/util/LruCache;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b7

    :cond_dd
    if-eqz v10, :cond_c2

    iget-wide v1, p1, Lcom/twitter/android/provider/m;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/b;->f(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->b(Ljava/lang/String;)V

    goto :goto_c2
.end method

.method public final d(Ljava/lang/String;)V
    .registers 6

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_30

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x23

    if-ne v0, v2, :cond_31

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(I)V

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "context"

    const-string v2, "hashtag"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    :cond_30
    :goto_30
    return-void

    :cond_31
    const/16 v1, 0x40

    if-ne v0, v1, :cond_30

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(I)V

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "screen_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    if-eqz v1, :cond_59

    const-string v1, "pc"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_59
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_30
.end method

.method public final l()V
    .registers 3

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->s:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->g:Lcom/twitter/android/bg;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/bg;->a(Z)V

    :cond_a
    :goto_a
    return-void

    :cond_b
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Lcom/twitter/android/fu;

    invoke-virtual {v0}, Lcom/twitter/android/fu;->notifyDataSetChanged()V

    goto :goto_a
.end method

.method public onClick(Landroid/view/View;)V
    .registers 10

    const v7, 0x7f0b005c

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_1ac

    :goto_10
    return-void

    :sswitch_11
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/PostActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "reply_to_tweet"

    new-array v1, v1, [Lcom/twitter/android/provider/m;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    aput-object v4, v1, v2

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.twitter.android.post.reply"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ac:Lcom/twitter/android/service/ScribeEvent;

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_10

    :sswitch_49
    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-boolean v0, v3, Lcom/twitter/android/provider/m;->l:Z

    if-eqz v0, :cond_7a

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v4, v4, Lcom/twitter/android/provider/m;->o:J

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v4, v5, v6}, Lcom/twitter/android/client/b;->c(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, v3, Lcom/twitter/android/provider/m;->l:Z

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->y:Landroid/widget/ImageButton;

    const v2, 0x7f02007e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->ag:Lcom/twitter/android/service/ScribeEvent;

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    :goto_76
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->b(Ljava/lang/String;)V

    goto :goto_10

    :cond_7a
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v4, v4, Lcom/twitter/android/provider/m;->o:J

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->q:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v2, v4, v5, v6}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, v3, Lcom/twitter/android/provider/m;->l:Z

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->y:Landroid/widget/ImageButton;

    const v2, 0x7f020082

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->af:Lcom/twitter/android/service/ScribeEvent;

    sget-object v5, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_76

    :sswitch_a2
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v3, v0, Lcom/twitter/android/provider/m;->q:J

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v0}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_ee

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-boolean v0, v0, Lcom/twitter/android/provider/m;->r:Z

    if-eqz v0, :cond_ee

    move v0, v1

    :goto_b7
    if-eqz v0, :cond_f0

    const/16 v0, 0x66

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0107

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b005e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    :goto_cd
    invoke-virtual {v0, v7}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v3, 0x7f0b0105

    invoke-virtual {v1, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v3, 0x7f0b00ec

    invoke-virtual {v1, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {v0, p0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_10

    :cond_ee
    move v0, v2

    goto :goto_b7

    :cond_f0
    const/16 v0, 0x65

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0106

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    goto :goto_cd

    :sswitch_102
    const/16 v0, 0x67

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0059

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b005a

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ed

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ee

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_10

    :sswitch_13b
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->p:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ah:Lcom/twitter/android/service/ScribeEvent;

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->R:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_10

    :sswitch_151
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(I)V

    :cond_155
    :goto_155
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->c(J)V

    goto/16 :goto_10

    :sswitch_164
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v0, v0, Lcom/twitter/android/provider/m;->q:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->c(J)V

    goto/16 :goto_10

    :sswitch_172
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/android/api/TweetMedia;

    if-eqz v0, :cond_155

    iget v0, v0, Lcom/twitter/android/api/TweetMedia;->type:I

    packed-switch v0, :pswitch_data_1ce

    goto :goto_155

    :pswitch_181
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->Z:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_155

    :pswitch_18f
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aa:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_155

    :pswitch_19d
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->Y:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_155

    nop

    :sswitch_data_1ac
    .sparse-switch
        0x7f07001e -> :sswitch_11
        0x7f07001f -> :sswitch_a2
        0x7f070020 -> :sswitch_49
        0x7f070021 -> :sswitch_13b
        0x7f070022 -> :sswitch_102
        0x7f07007a -> :sswitch_151
        0x7f07008e -> :sswitch_172
        0x7f0700d1 -> :sswitch_164
    .end sparse-switch

    :pswitch_data_1ce
    .packed-switch 0x1
        :pswitch_181
        :pswitch_18f
        :pswitch_19d
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/android/ft;

    invoke-direct {v0, p0}, Lcom/twitter/android/ft;-><init>(Lcom/twitter/android/TweetFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/android/client/j;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/util/y;)V

    if-eqz p1, :cond_24

    const-string v0, "as"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/a;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/android/api/a;

    const-string v0, "f"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->w:Z

    :cond_24
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetFragment;->x:I

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 10

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/z;->n:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->n:Lcom/twitter/android/provider/m;

    iget-wide v5, v3, Lcom/twitter/android/provider/m;->o:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "ownerId"

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v5}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/provider/m;->b:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 5

    const v0, 0x7f030012

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/TweetFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .registers 3

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/b;->b(ILcom/twitter/android/util/y;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "as"

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->t:Lcom/twitter/android/api/a;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "f"

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 5

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    return-void
.end method
