.class public Lcom/twitter/android/UsersActivity;
.super Lcom/twitter/android/BaseFragmentActivity;


# instance fields
.field private e:I


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a_(I)V
    .registers 4

    const v0, 0x7f070054

    if-ne v0, p1, :cond_1f

    iget v0, p0, Lcom/twitter/android/UsersActivity;->e:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1f

    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070036

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->n()V

    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->finish()V

    :goto_1e
    return-void

    :cond_1f
    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->a_(I)V

    goto :goto_1e
.end method

.method public onBackPressed()V
    .registers 5

    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070036

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersFragment;

    iget v1, p0, Lcom/twitter/android/UsersActivity;->e:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_17

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->m()V

    :goto_16
    return-void

    :cond_17
    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->l()Lcom/twitter/android/util/FriendshipCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/util/FriendshipCache;->a()Z

    move-result v1

    if-nez v1, :cond_30

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "friendship_cache"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/UsersActivity;->setResult(ILandroid/content/Intent;)V

    :cond_30
    invoke-super {p0}, Lcom/twitter/android/BaseFragmentActivity;->onBackPressed()V

    goto :goto_16
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 10

    const v5, 0x7f03006c

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v0, "type"

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    packed-switch v4, :pswitch_data_102

    :pswitch_13
    invoke-super {p0, p1, v5, v1}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    const-string v0, "user_ids"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_64

    move v0, v2

    :goto_1f
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    if-nez p1, :cond_51

    invoke-static {v3, v0}, Lcom/twitter/android/UsersFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "follow"

    const-string v7, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v5, Lcom/twitter/android/UsersFragment;

    invoke-direct {v5}, Lcom/twitter/android/UsersFragment;-><init>()V

    invoke-virtual {v5, v0}, Lcom/twitter/android/UsersFragment;->setArguments(Landroid/os/Bundle;)V

    sparse-switch v4, :sswitch_data_120

    :goto_3f
    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v6, 0x7f070036

    invoke-virtual {v0, v6, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_51
    packed-switch v4, :pswitch_data_132

    :pswitch_54
    const v0, 0x7f0b011f

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->b(Ljava/lang/String;)V

    :goto_5e
    iput v4, p0, Lcom/twitter/android/UsersActivity;->e:I

    :cond_60
    return-void

    :pswitch_61
    invoke-super {p0, p1, v5, v1}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    :cond_64
    move v0, v1

    goto :goto_1f

    :pswitch_66
    invoke-super {p0, p1, v5, v2}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    iget-object v0, p0, Lcom/twitter/android/UsersActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-eqz v0, :cond_60

    move v0, v1

    goto :goto_1f

    :pswitch_73
    const v0, 0x7f030064

    invoke-super {p0, p1, v0, v2}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    move v0, v2

    goto :goto_1f

    :sswitch_7b
    const-string v6, "empty_desc"

    const v7, 0x7f0b01ce

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_3f

    :sswitch_84
    const-string v6, "empty_desc"

    const v7, 0x7f0b01cf

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_3f

    :sswitch_8d
    const-string v6, "empty_desc"

    const v7, 0x7f0b01ee

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_3f

    :pswitch_96
    const v0, 0x7f0b0033

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->b(Ljava/lang/String;)V

    goto :goto_5e

    :pswitch_a1
    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->b(Ljava/lang/String;)V

    goto :goto_5e

    :pswitch_ac
    const v0, 0x7f0b002f

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->b(Ljava/lang/String;)V

    goto :goto_5e

    :pswitch_b7
    const-string v0, "slug_name"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->b(Ljava/lang/String;)V

    goto :goto_5e

    :pswitch_c1
    const v0, 0x7f0b0144

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/UsersActivity;->a(I[Ljava/lang/Object;)V

    goto :goto_5e

    :pswitch_ca
    const v0, 0x7f0b0126

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/UsersActivity;->a(I[Ljava/lang/Object;)V

    goto :goto_5e

    :pswitch_d3
    const v0, 0x7f0b01c3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/UsersActivity;->a(I[Ljava/lang/Object;)V

    goto :goto_5e

    :pswitch_dc
    const v0, 0x7f0b01c4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/UsersActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_5e

    :pswitch_e6
    const v0, 0x7f0b01ec

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/UsersActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_5e

    :pswitch_f0
    const v0, 0x7f0b01f4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "username"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/UsersActivity;->a(I[Ljava/lang/Object;)V

    goto/16 :goto_5e

    :pswitch_data_102
    .packed-switch 0x6
        :pswitch_61
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_13
        :pswitch_13
        :pswitch_66
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_73
    .end packed-switch

    :sswitch_data_120
    .sparse-switch
        0x7 -> :sswitch_84
        0x9 -> :sswitch_7b
        0xa -> :sswitch_7b
        0x12 -> :sswitch_8d
    .end sparse-switch

    :pswitch_data_132
    .packed-switch 0x0
        :pswitch_96
        :pswitch_a1
        :pswitch_ac
        :pswitch_54
        :pswitch_54
        :pswitch_54
        :pswitch_b7
        :pswitch_c1
        :pswitch_54
        :pswitch_ca
        :pswitch_f0
        :pswitch_d3
        :pswitch_dc
        :pswitch_54
        :pswitch_54
        :pswitch_54
        :pswitch_54
        :pswitch_54
        :pswitch_e6
    .end packed-switch
.end method
