.class public Lcom/twitter/android/UsersFragment;
.super Lcom/twitter/android/BaseListFragment;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/cl;
.implements Lcom/twitter/android/fn;
.implements Lcom/twitter/android/widget/a;
.implements Lcom/twitter/android/widget/x;


# instance fields
.field A:I

.field private final B:Ljava/util/HashSet;

.field private C:Landroid/net/Uri;

.field private D:I

.field private E:[J

.field private F:Z

.field private G:[Ljava/lang/String;

.field private H:Ljava/lang/String;

.field m:J

.field n:Lcom/twitter/android/api/PromotedContent;

.field o:J

.field p:Ljava/util/ArrayList;

.field q:Ljava/util/HashSet;

.field r:Lcom/twitter/android/util/FriendshipCache;

.field s:Ljava/util/HashMap;

.field t:Lcom/twitter/android/gy;

.field u:Ljava/lang/String;

.field v:Lcom/twitter/android/widget/ad;

.field w:Z

.field x:Lcom/twitter/android/fd;

.field y:Lcom/twitter/android/ao;

.field z:Lcom/twitter/android/api/ad;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/BaseListFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->w:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/UsersFragment;->A:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->B:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/UsersFragment;)I
    .registers 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    return v0
.end method

.method private g(I)V
    .registers 14

    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->a(I)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    :goto_b
    return-void

    :cond_c
    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    packed-switch v0, :pswitch_data_de

    :pswitch_11
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->h(I)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/twitter/android/client/b;->a(JII)Ljava/lang/String;

    move-result-object v0

    :goto_1d
    if-eqz v0, :cond_b

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->b(I)V

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;I)V

    goto :goto_b

    :pswitch_26
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->h(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v11}, Lcom/twitter/android/client/b;->a(JII)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->h(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/b;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->b(Ljava/lang/String;)V

    goto :goto_1d

    :pswitch_40
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->i(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    :pswitch_4b
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget v1, p0, Lcom/twitter/android/UsersFragment;->D:I

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->h:J

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->o:J

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->h(I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/b;->a(IJJI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    :pswitch_5c
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget v1, p0, Lcom/twitter/android/UsersFragment;->D:I

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->h:J

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->o:J

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->h(I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/b;->a(IJJI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    :pswitch_6d
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->h(I)I

    move-result v1

    invoke-virtual {v0, v11, v1}, Lcom/twitter/android/client/b;->a(ZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    :pswitch_78
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    const/4 v3, 0x6

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->h:J

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/b;->a(ZIIJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    :pswitch_83
    const/4 v0, 0x3

    if-ne p1, v0, :cond_b

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v11, v7, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    move-object v0, v7

    goto :goto_1d

    :pswitch_8f
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->i(I)I

    move-result v2

    const/16 v3, 0x14

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1d

    :pswitch_a7
    if-eq p1, v11, :cond_db

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "event_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    move v6, v1

    move-wide v7, v4

    move v9, v1

    move v10, v1

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;JIJIZZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1d

    :pswitch_bf
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->E:[J

    iget v4, p0, Lcom/twitter/android/UsersFragment;->D:I

    iget-wide v5, p0, Lcom/twitter/android/UsersFragment;->o:J

    move-object v1, v7

    move-object v2, v7

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/b;->a([Ljava/lang/String;[Ljava/lang/String;[JIJ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1d

    :pswitch_cf
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->h(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->b(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1d

    :cond_db
    move-object v0, v7

    goto/16 :goto_1d

    :pswitch_data_de
    .packed-switch 0x1
        :pswitch_26
        :pswitch_40
        :pswitch_8f
        :pswitch_4b
        :pswitch_5c
        :pswitch_4b
        :pswitch_83
        :pswitch_11
        :pswitch_6d
        :pswitch_78
        :pswitch_bf
        :pswitch_bf
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_a7
        :pswitch_cf
    .end packed-switch
.end method

.method private h(I)I
    .registers 5

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_2a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid fetch type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_19
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_28

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_28

    const/4 v0, 0x1

    :cond_28
    :pswitch_28
    return v0

    nop

    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_19
        :pswitch_28
        :pswitch_28
    .end packed-switch
.end method

.method private i(I)I
    .registers 5

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_30

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid fetch type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_19
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2f

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2f

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    div-int/lit8 v0, v0, 0x14

    add-int/lit8 v0, v0, 0x1

    :cond_2f
    :pswitch_2f
    return v0

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_19
        :pswitch_2f
        :pswitch_2f
    .end packed-switch
.end method


# virtual methods
.method public final a(II)V
    .registers 9

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_20

    :cond_4
    :goto_4
    return-void

    :pswitch_5
    if-ne p2, v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->o:J

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->m:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/b;->b(IJJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->b(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_16
    if-ne p2, v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_4

    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_5
        :pswitch_16
    .end packed-switch
.end method

.method protected final a(Landroid/database/Cursor;)V
    .registers 5

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->b:Z

    if-eqz v0, :cond_16

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_16

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_16

    invoke-direct {p0, v2}, Lcom/twitter/android/UsersFragment;->g(I)V

    :cond_16
    return-void
.end method

.method public final a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 15

    const/4 v4, 0x7

    const/4 v11, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_fe

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    if-ne v0, v4, :cond_eb

    :cond_19
    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    if-eq v0, v11, :cond_eb

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_eb

    invoke-direct {p0, v11}, Lcom/twitter/android/UsersFragment;->g(I)V

    :cond_26
    :goto_26
    return-void

    :pswitch_27
    if-nez p2, :cond_33

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->t:Lcom/twitter/android/gy;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->t:Lcom/twitter/android/gy;

    invoke-interface {v0, v1}, Lcom/twitter/android/gy;->b(I)V

    goto :goto_26

    :cond_33
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_ca

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    move-result v6

    :goto_56
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_aa

    const-string v0, "vnd.android.cursor.item/phone_v2"

    const/4 v7, 0x1

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    packed-switch v6, :pswitch_data_106

    :goto_70
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    move v0, v1

    :goto_75
    if-ge v0, v8, :cond_93

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    move-result v10

    if-eqz v10, :cond_84

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_84
    add-int/lit8 v0, v0, 0x1

    goto :goto_75

    :pswitch_87
    const-string v0, "+1"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_70

    :pswitch_8d
    const/16 v0, 0x2b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_70

    :cond_93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-virtual {v2, v1, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_56

    :cond_a2
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_56

    :cond_aa
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    const/4 v3, 0x0

    const-wide/16 v5, -0x1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/b;->a([Ljava/lang/String;[Ljava/lang/String;[JIJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v11}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;I)V

    goto/16 :goto_26

    :cond_ca
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->t:Lcom/twitter/android/gy;

    if-eqz v0, :cond_d3

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->t:Lcom/twitter/android/gy;

    invoke-interface {v0, v1}, Lcom/twitter/android/gy;->b(I)V

    :cond_d3
    invoke-virtual {p0, v11}, Lcom/twitter/android/UsersFragment;->c(I)V

    goto/16 :goto_26

    :pswitch_d8
    if-eqz p2, :cond_26

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->x:Lcom/twitter/android/fd;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->x:Lcom/twitter/android/fd;

    iget v1, p0, Lcom/twitter/android/UsersFragment;->A:I

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/fd;->a(II)V

    goto/16 :goto_26

    :cond_eb
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->t:Lcom/twitter/android/gy;

    if-eqz v0, :cond_f8

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->t:Lcom/twitter/android/gy;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/android/gy;->b(I)V

    :cond_f8
    invoke-virtual {p0, v11}, Lcom/twitter/android/UsersFragment;->c(I)V

    goto/16 :goto_26

    nop

    :pswitch_data_fe
    .packed-switch 0x1
        :pswitch_27
        :pswitch_d8
    .end packed-switch

    :pswitch_data_106
    .packed-switch 0x1
        :pswitch_87
        :pswitch_8d
    .end packed-switch
.end method

.method public final synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .registers 5

    check-cast p2, Lcom/twitter/android/api/PromotedContent;

    if-eqz p2, :cond_14

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->B:Ljava/util/HashSet;

    iget-object v1, p2, Lcom/twitter/android/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/api/PromotedContent;)V

    :cond_14
    return-void
.end method

.method public final a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 12

    iget v0, p0, Lcom/twitter/android/UsersFragment;->A:I

    if-ne p3, v0, :cond_10

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_f

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->startActivity(Landroid/content/Intent;)V

    :cond_f
    :goto_f
    return-void

    :cond_10
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-lez v0, :cond_26

    add-int/lit8 v0, v0, -0x1

    if-gt p3, v0, :cond_26

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_f

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_f

    :cond_26
    invoke-virtual {p1}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-lez v0, :cond_42

    invoke-virtual {p1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    if-le p3, v0, :cond_42

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_f

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_f

    :cond_42
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_f

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "user_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "type"

    iget v2, p0, Lcom/twitter/android/UsersFragment;->D:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v1, p4, p5}, Lcom/twitter/android/util/FriendshipCache;->d(J)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_b0

    const-string v2, "friendship"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_6e
    :goto_6e
    check-cast p2, Lcom/twitter/android/widget/BaseUserView;

    invoke-virtual {p2}, Lcom/twitter/android/widget/BaseUserView;->a()Lcom/twitter/android/api/PromotedContent;

    move-result-object v1

    if-eqz v1, :cond_81

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/api/PromotedContent;)V

    const-string v2, "pc"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_81
    iget v1, p0, Lcom/twitter/android/UsersFragment;->D:I

    const/16 v2, 0x11

    if-ne v1, v2, :cond_aa

    new-instance v1, Lcom/twitter/android/service/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/service/ScribeEvent;->J:Lcom/twitter/android/service/ScribeEvent;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/twitter/android/service/ScribeLog;-><init>(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "event_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/util/z;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/android/service/ScribeLog;->query:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/service/ScribeLog;)V

    :cond_aa
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/UsersFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_f

    :cond_b0
    const/4 v1, 0x2

    iget v2, p0, Lcom/twitter/android/UsersFragment;->D:I

    if-ne v1, v2, :cond_6e

    const-string v1, "friendship"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_6e
.end method

.method final a(Lcom/twitter/android/gy;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/UsersFragment;->t:Lcom/twitter/android/gy;

    return-void
.end method

.method public final synthetic a(Lcom/twitter/android/widget/BaseUserView;J)V
    .registers 8

    const/4 v2, 0x0

    check-cast p1, Lcom/twitter/android/widget/UserView;

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3a

    iput-wide p2, p0, Lcom/twitter/android/UsersFragment;->m:J

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0080

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0081

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ed

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ee

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :goto_39
    return-void

    :cond_3a
    invoke-virtual {p1}, Lcom/twitter/android/widget/UserView;->a()Lcom/twitter/android/api/PromotedContent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/widget/UserView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_a2

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->q:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_55

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, p2, p3, v0}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    :cond_55
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/util/FriendshipCache;->c(J)V

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->F:Z

    if-eqz v0, :cond_8e

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    packed-switch v0, :pswitch_data_124

    :pswitch_63
    goto :goto_39

    :pswitch_64
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aQ:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_39

    :pswitch_72
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aK:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_39

    :pswitch_80
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aO:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_39

    :cond_8e
    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    packed-switch v0, :pswitch_data_130

    goto :goto_39

    :pswitch_94
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->bb:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_39

    :cond_a2
    if-eqz v0, :cond_c7

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, p2, p3, v2, v0}, Lcom/twitter/android/client/b;->a(JZLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    :goto_a9
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/util/FriendshipCache;->b(J)V

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->F:Z

    if-eqz v0, :cond_ef

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    packed-switch v0, :pswitch_data_136

    :pswitch_b7
    goto :goto_39

    :pswitch_b8
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aP:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_39

    :cond_c7
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->q:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_a9

    :pswitch_d1
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aJ:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_39

    :pswitch_e0
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aN:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_39

    :cond_ef
    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    packed-switch v0, :pswitch_data_142

    :pswitch_f4
    goto/16 :goto_39

    :pswitch_f6
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->bm:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_39

    :pswitch_105
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ba:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_39

    :pswitch_114
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->bd:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_39

    nop

    :pswitch_data_124
    .packed-switch 0x6
        :pswitch_64
        :pswitch_72
        :pswitch_63
        :pswitch_80
    .end packed-switch

    :pswitch_data_130
    .packed-switch 0x7
        :pswitch_94
    .end packed-switch

    :pswitch_data_136
    .packed-switch 0x6
        :pswitch_b8
        :pswitch_d1
        :pswitch_b7
        :pswitch_e0
    .end packed-switch

    :pswitch_data_142
    .packed-switch 0x6
        :pswitch_f6
        :pswitch_105
        :pswitch_f4
        :pswitch_114
    .end packed-switch
.end method

.method public final a_()V
    .registers 2

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/UsersFragment;->g(I)V

    return-void
.end method

.method public final d_()V
    .registers 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->w:Z

    return-void
.end method

.method protected final k()V
    .registers 1

    return-void
.end method

.method public final l()Lcom/twitter/android/util/FriendshipCache;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    return-object v0
.end method

.method final m()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_32

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b011b

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ec

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :goto_31
    return-void

    :cond_32
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_31
.end method

.method final n()V
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_35

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/twitter/android/client/b;->g(J)Ljava/lang/String;

    goto :goto_a

    :cond_35
    const/4 v3, 0x2

    if-ne v1, v3, :cond_a

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/twitter/android/client/b;->h(J)Ljava/lang/String;

    goto :goto_a

    :cond_48
    return-void
.end method

.method final o()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    :goto_a
    if-eqz v0, :cond_f

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    :cond_f
    return-void

    :cond_10
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 14

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    if-nez v0, :cond_7c

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->i()Z

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    sget-object v4, Lcom/twitter/android/provider/bd;->a:[Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v0

    if-eqz v0, :cond_8b

    const-string v0, "LOWER(username) ASC"

    :goto_21
    const/4 v2, 0x0

    iget v7, p0, Lcom/twitter/android/UsersFragment;->D:I

    packed-switch v7, :pswitch_data_2cc

    :pswitch_27
    sget-object v7, Lcom/twitter/android/provider/ab;->i:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    :cond_31
    move v11, v2

    move-object v2, v0

    move v0, v11

    :goto_34
    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->G:[Ljava/lang/String;

    iput-object v2, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    if-eqz v5, :cond_2c8

    const-string v2, "follow"

    const/4 v4, 0x0

    invoke-virtual {v6, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2c8

    const v0, 0x7f020049

    move v5, v0

    :goto_47
    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    sparse-switch v0, :sswitch_data_2f6

    new-instance v0, Lcom/twitter/android/gs;

    const/4 v2, 0x2

    iget v4, p0, Lcom/twitter/android/UsersFragment;->k:I

    if-eqz v4, :cond_2c5

    const/4 v4, 0x1

    :goto_54
    iget-object v7, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    const-string v8, "follow_all_title"

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string v9, "follow_all_subtitle"

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    move-object v6, p0

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/gs;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/gs;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/ad;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ad;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    :cond_7c
    :goto_7c
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/android/gz;

    invoke-direct {v0, p0}, Lcom/twitter/android/gz;-><init>(Lcom/twitter/android/UsersFragment;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->d:Lcom/twitter/android/client/j;

    return-void

    :cond_8b
    const-string v0, "_id ASC"

    goto :goto_21

    :pswitch_8e
    sget-object v7, Lcom/twitter/android/provider/ab;->g:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto :goto_34

    :pswitch_9c
    sget-object v7, Lcom/twitter/android/provider/ab;->h:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto :goto_34

    :pswitch_aa
    sget-object v7, Lcom/twitter/android/provider/ab;->f:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_b9
    sget-object v7, Lcom/twitter/android/provider/ac;->b:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_c8
    iget-wide v7, p0, Lcom/twitter/android/UsersFragment;->o:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_e2

    iget-wide v7, p0, Lcom/twitter/android/UsersFragment;->h:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_e2

    sget-object v7, Lcom/twitter/android/provider/ab;->c:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    :cond_e2
    if-eqz v5, :cond_31

    iget-wide v7, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-nez v7, :cond_31

    const v2, 0x7f020162

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_f6
    sget-object v7, Lcom/twitter/android/provider/ab;->m:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_105
    sget-object v7, Lcom/twitter/android/provider/ab;->b:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_114
    sget-object v7, Lcom/twitter/android/provider/ab;->j:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_123
    sget-object v0, Lcom/twitter/android/provider/ac;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/android/provider/bd;->b:[Ljava/lang/String;

    const/4 v0, 0x0

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_12f
    sget-object v7, Lcom/twitter/android/provider/ab;->n:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_13e
    sget-object v7, Lcom/twitter/android/provider/ab;->o:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_14d
    sget-object v7, Lcom/twitter/android/provider/ab;->d:Landroid/net/Uri;

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_156
    sget-object v7, Lcom/twitter/android/provider/ab;->e:Landroid/net/Uri;

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_15f
    sget-object v7, Lcom/twitter/android/provider/ab;->p:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_16e
    sget-object v7, Lcom/twitter/android/provider/ab;->q:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :pswitch_17d
    sget-object v7, Lcom/twitter/android/provider/ab;->r:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    if-eqz v5, :cond_31

    iget-wide v7, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-nez v7, :cond_31

    const v2, 0x7f020049

    move v11, v2

    move-object v2, v0

    move v0, v11

    goto/16 :goto_34

    :sswitch_19b
    new-instance v0, Lcom/twitter/android/el;

    const/4 v2, 0x2

    iget v4, p0, Lcom/twitter/android/UsersFragment;->k:I

    if-eqz v4, :cond_1d0

    const/4 v4, 0x1

    :goto_1a3
    iget-object v7, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    const-string v8, "follow_all_title"

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string v9, "follow_all_subtitle"

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    move-object v6, p0

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/el;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/el;->a(Lcom/twitter/android/cl;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/el;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/ad;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ad;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    goto/16 :goto_7c

    :cond_1d0
    const/4 v4, 0x0

    goto :goto_1a3

    :sswitch_1d2
    new-instance v0, Lcom/twitter/android/bf;

    const/4 v2, 0x2

    new-instance v4, Lcom/twitter/android/gx;

    invoke-direct {v4, p0}, Lcom/twitter/android/gx;-><init>(Lcom/twitter/android/UsersFragment;)V

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/bf;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;Lcom/twitter/android/widget/a;Ljava/util/HashMap;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/bf;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/ad;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ad;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    goto/16 :goto_7c

    :sswitch_1f5
    new-instance v0, Lcom/twitter/android/gs;

    const/4 v2, 0x2

    iget v4, p0, Lcom/twitter/android/UsersFragment;->k:I

    if-eqz v4, :cond_243

    const/4 v4, 0x1

    :goto_1fd
    iget-object v7, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    const-string v8, "follow_all_title"

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string v9, "follow_all_subtitle"

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    move-object v6, p0

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/gs;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/gs;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    new-instance v2, Lcom/twitter/android/ao;

    invoke-direct {v2, v1, v3}, Lcom/twitter/android/ao;-><init>(Landroid/content/Context;Lcom/twitter/android/client/b;)V

    iput-object v2, p0, Lcom/twitter/android/UsersFragment;->y:Lcom/twitter/android/ao;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->z:Lcom/twitter/android/api/ad;

    if-eqz v1, :cond_228

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->y:Lcom/twitter/android/ao;

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->z:Lcom/twitter/android/api/ad;

    invoke-virtual {v1, v2}, Lcom/twitter/android/ao;->a(Lcom/twitter/android/api/ad;)V

    :cond_228
    new-instance v1, Lcom/twitter/android/widget/ad;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_308

    const/4 v3, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/widget/BaseAdapter;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->y:Lcom/twitter/android/ao;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/android/widget/ad;-><init>([I[I[Landroid/widget/BaseAdapter;)V

    iput-object v1, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    goto/16 :goto_7c

    :cond_243
    const/4 v4, 0x0

    goto :goto_1fd

    :sswitch_245
    new-instance v0, Lcom/twitter/android/gs;

    const/4 v2, 0x2

    iget v4, p0, Lcom/twitter/android/UsersFragment;->k:I

    if-eqz v4, :cond_2b2

    const/4 v4, 0x1

    :goto_24d
    iget-object v7, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    const-string v8, "follow_all_title"

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string v9, "follow_all_subtitle"

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    move-object v6, p0

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/gs;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/gs;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->d()Lcom/twitter/android/api/ad;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/android/api/ad;->a:J

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->h:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2b4

    iget-boolean v0, v0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_2b4

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/UsersActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "type"

    const/16 v3, 0x12

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/fd;

    new-instance v3, Lcom/twitter/android/fe;

    const v4, 0x7f0b01ec

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4, v0}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-direct {v2, v3}, Lcom/twitter/android/fd;-><init>(Lcom/twitter/android/fe;)V

    new-instance v0, Lcom/twitter/android/widget/ad;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    aput-object v4, v1, v3

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ad;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->A:I

    iput-object v2, p0, Lcom/twitter/android/UsersFragment;->x:Lcom/twitter/android/fd;

    goto/16 :goto_7c

    :cond_2b2
    const/4 v4, 0x0

    goto :goto_24d

    :cond_2b4
    new-instance v0, Lcom/twitter/android/widget/ad;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ad;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->v:Lcom/twitter/android/widget/ad;

    goto/16 :goto_7c

    :cond_2c5
    const/4 v4, 0x0

    goto/16 :goto_54

    :cond_2c8
    move v5, v0

    goto/16 :goto_47

    nop

    :pswitch_data_2cc
    .packed-switch 0x0
        :pswitch_8e
        :pswitch_9c
        :pswitch_aa
        :pswitch_14d
        :pswitch_c8
        :pswitch_105
        :pswitch_f6
        :pswitch_114
        :pswitch_12f
        :pswitch_123
        :pswitch_b9
        :pswitch_15f
        :pswitch_16e
        :pswitch_13e
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_156
        :pswitch_17d
    .end packed-switch

    :sswitch_data_2f6
    .sparse-switch
        0x1 -> :sswitch_245
        0x9 -> :sswitch_19b
        0x11 -> :sswitch_1f5
        0x12 -> :sswitch_1d2
    .end sparse-switch

    :array_308
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 9

    const-wide/16 v2, 0x0

    packed-switch p1, :pswitch_data_36

    :cond_5
    :goto_5
    return-void

    :pswitch_6
    const/4 v0, -0x1

    if-ne v0, p2, :cond_5

    if-eqz p3, :cond_5

    const-string v0, "user_id"

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-lez v2, :cond_5

    const-string v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/util/FriendshipCache;->a(JI)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/util/FriendshipCache;->b(JI)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_5

    nop

    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 8

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_5

    :goto_4
    return-void

    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_64

    goto :goto_4

    :pswitch_d
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_54

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_54

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    :cond_1d
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/16 v5, 0x8

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    or-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/util/FriendshipCache;->b(JI)V

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->q:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1d

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->F:Z

    if-eqz v1, :cond_59

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aL:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    :cond_54
    :goto_54
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_4

    :cond_59
    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->bc:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_54

    nop

    :pswitch_data_64
    .packed-switch 0x7f070069
        :pswitch_d
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 7

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/UsersFragment;->D:I

    const-string v1, "tag"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/UsersFragment;->o:J

    const-string v1, "user_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_27

    array-length v2, v1

    if-lez v2, :cond_27

    iput-object v1, p0, Lcom/twitter/android/UsersFragment;->E:[J

    :cond_27
    const-string v1, "onboarding"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/UsersFragment;->F:Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/UsersFragment;->q:Ljava/util/HashSet;

    if-eqz p1, :cond_a6

    const-string v0, "state_dialog_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/UsersFragment;->m:J

    const-string v0, "state_dialog_pc"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->n:Lcom/twitter/android/api/PromotedContent;

    const-string v0, "state_checked_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->p:Ljava/util/ArrayList;

    const-string v0, "state_user_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->E:[J

    const-string v0, "mediator"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/ad;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->z:Lcom/twitter/android/api/ad;

    const-string v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_96

    const-string v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    :goto_77
    const-string v0, "incoming_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9e

    const-string v0, "incoming_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    :goto_89
    const-string v0, "state_search_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->u:Ljava/lang/String;

    :goto_91
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/UsersFragment;->a(ILcom/twitter/android/util/y;)V

    return-void

    :cond_96
    new-instance v0, Lcom/twitter/android/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/android/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    goto :goto_77

    :cond_9e
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    goto :goto_89

    :cond_a6
    const-string v1, "friendship_cache"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_cc

    const-string v1, "friendship_cache"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    :goto_b8
    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    sparse-switch v0, :sswitch_data_152

    goto :goto_91

    :sswitch_be
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->h:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_91

    :cond_cc
    new-instance v0, Lcom/twitter/android/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/android/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    goto :goto_b8

    :sswitch_d4
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->i:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_91

    :sswitch_e2
    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->F:Z

    if-eqz v0, :cond_f4

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aI:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_91

    :cond_f4
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->bo:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_91

    :sswitch_102
    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->F:Z

    if-eqz v0, :cond_115

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aM:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_91

    :cond_115
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aZ:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_91

    :sswitch_124
    new-instance v0, Lcom/twitter/android/service/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->G:Lcom/twitter/android/service/ScribeEvent;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/service/ScribeLog;-><init>(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "event_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/util/z;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/service/ScribeLog;->query:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/service/ScribeLog;)V

    goto/16 :goto_91

    :sswitch_149
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    goto/16 :goto_91

    :sswitch_data_152
    .sparse-switch
        0x0 -> :sswitch_be
        0x1 -> :sswitch_d4
        0x7 -> :sswitch_102
        0x9 -> :sswitch_e2
        0x11 -> :sswitch_124
        0x12 -> :sswitch_149
    .end sparse-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 14

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v10, 0x0

    packed-switch p1, :pswitch_data_108

    iget-wide v0, p0, Lcom/twitter/android/UsersFragment;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_87

    const-string v6, "type=? AND tag=?"

    new-array v5, v5, [Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    iget-wide v0, p0, Lcom/twitter/android/UsersFragment;->o:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    move-object v4, v6

    :goto_24
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "session_owner_id"

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    new-instance v0, Lcom/twitter/android/u;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    invoke-static {v6, v2, v3}, Lcom/twitter/android/provider/o;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->G:[Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/u;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_47
    return-object v0

    :pswitch_48
    new-instance v0, Lcom/twitter/android/u;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "data1"

    aput-object v4, v3, v10

    const-string v4, "mimetype"

    aput-object v4, v3, v7

    const-string v4, "data1 NOT NULL AND (mimetype=? OR mimetype=?)"

    new-array v5, v5, [Ljava/lang/String;

    const-string v8, "vnd.android.cursor.item/phone_v2"

    aput-object v8, v5, v10

    const-string v8, "vnd.android.cursor.item/email_v2"

    aput-object v8, v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/u;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_47

    :pswitch_6a
    sget-object v0, Lcom/twitter/android/provider/ab;->r:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v0, v1, v2}, Lcom/twitter/android/provider/o;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    new-instance v2, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v5, Lcom/twitter/android/provider/bd;->a:[Ljava/lang/String;

    move-object v7, v6

    move-object v8, v6

    invoke-direct/range {v2 .. v8}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_47

    :cond_87
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->E:[J

    if-eqz v0, :cond_be

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->E:[J

    array-length v2, v1

    new-array v5, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "user_id IN (?"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-wide v8, v1, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    move v0, v7

    :goto_a0
    if-ge v0, v2, :cond_b2

    const-string v4, ", ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v6, v1, v0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_a0

    :cond_b2
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v4, v6

    goto/16 :goto_24

    :cond_be
    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    sparse-switch v0, :sswitch_data_110

    move-object v4, v6

    move-object v5, v6

    goto/16 :goto_24

    :sswitch_c7
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/android/util/FriendshipCache;->a()Z

    move-result v0

    if-eqz v0, :cond_de

    const-string v6, "friendship IS NULL OR friendship NOT IN (1,3,9,10,11) AND user_id!=?"

    new-array v5, v7, [Ljava/lang/String;

    iget-wide v0, p0, Lcom/twitter/android/UsersFragment;->h:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    move-object v4, v6

    goto/16 :goto_24

    :cond_de
    move-object v4, v6

    move-object v5, v6

    goto/16 :goto_24

    :sswitch_e2
    const-string v6, "type=? AND tag=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->u:Ljava/lang/String;

    aput-object v0, v5, v7

    move-object v4, v6

    goto/16 :goto_24

    :sswitch_f4
    const-string v6, "type=? AND tag=?"

    new-array v5, v5, [Ljava/lang/String;

    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    const-string v0, "-1"

    aput-object v0, v5, v7

    move-object v4, v6

    goto/16 :goto_24

    nop

    :pswitch_data_108
    .packed-switch 0x1
        :pswitch_48
        :pswitch_6a
    .end packed-switch

    :sswitch_data_110
    .sparse-switch
        0x3 -> :sswitch_e2
        0x7 -> :sswitch_c7
        0x9 -> :sswitch_c7
        0x11 -> :sswitch_f4
    .end sparse-switch
.end method

.method public onDestroy()V
    .registers 2

    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onDestroy()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/UsersFragment;->b(ILcom/twitter/android/util/y;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/UsersFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .registers 6

    const/4 v2, 0x3

    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->C:Landroid/net/Uri;

    if-eqz v0, :cond_55

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    if-eq v0, v2, :cond_1a

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_1a

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_22

    :cond_1a
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->u:Ljava/lang/String;

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->z:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_49

    :cond_22
    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->b(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->g()V

    :cond_28
    :goto_28
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->d()Lcom/twitter/android/api/ad;

    move-result-object v0

    iget-wide v1, v0, Lcom/twitter/android/api/ad;->a:J

    iget-wide v3, p0, Lcom/twitter/android/UsersFragment;->h:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_48

    iget-boolean v0, v0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_48

    iget v0, p0, Lcom/twitter/android/UsersFragment;->D:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_48

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_48
    return-void

    :cond_49
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-direct {p0, v2}, Lcom/twitter/android/UsersFragment;->g(I)V

    goto :goto_28

    :cond_55
    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->c(I)V

    goto :goto_28
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "state_dialog_user"

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->m:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->n:Lcom/twitter/android/api/PromotedContent;

    if-eqz v0, :cond_15

    const-string v0, "state_dialog_pc"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->n:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_15
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_20

    const-string v0, "state_checked_users"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_20
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->E:[J

    if-eqz v0, :cond_2b

    const-string v0, "state_user_ids"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->E:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_2b
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/android/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_3a

    const-string v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->r:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_3a
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4d

    const-string v0, "incoming_friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->s:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_4d
    const-string v0, "state_search_id"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mediator"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->z:Lcom/twitter/android/api/ad;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onStop()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->q:Ljava/util/HashSet;

    invoke-static {v0}, Lcom/twitter/android/util/z;->b(Ljava/util/Collection;)[J

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/b;->b([J)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->q:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    :cond_12
    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onStop()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 8

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "browse_categories"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_35

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->j:Landroid/widget/ListView;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/SULActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "onboarding"

    iget-boolean v4, p0, Lcom/twitter/android/UsersFragment;->F:Z

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    :cond_35
    return-void
.end method
