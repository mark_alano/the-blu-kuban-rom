.class final Lcom/twitter/android/ap;
.super Lcom/twitter/android/ga;


# instance fields
.field final synthetic a:Lcom/twitter/android/EventsLandingFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/EventsLandingFragment;Landroid/content/Context;)V
    .registers 3

    iput-object p1, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/ga;-><init>(Lcom/twitter/android/TweetListFragment;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;ILcom/twitter/android/api/ad;)V
    .registers 12

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/EventsLandingFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v3

    if-eqz v3, :cond_4f

    iget v0, v3, Lcom/twitter/android/co;->b:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_50

    move v0, v1

    :goto_10
    iget-object v4, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v4, v1}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/EventsLandingFragment;Z)Z

    const/16 v4, 0xc8

    if-ne p3, v4, :cond_5a

    if-eqz v0, :cond_52

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0, p6}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/EventsLandingFragment;Lcom/twitter/android/api/ad;)Lcom/twitter/android/api/ad;

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-virtual {v0}, Lcom/twitter/android/EventsLandingFragment;->g()V

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/ao;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/twitter/android/ao;->a(Lcom/twitter/android/api/ad;)V

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->b(Lcom/twitter/android/EventsLandingFragment;)Z

    move-result v0

    if-eqz v0, :cond_3f

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->c(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/fo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fo;->notifyDataSetChanged()V

    :cond_3f
    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    sget-object v1, Lcom/twitter/android/service/ScribeEvent;->C:Lcom/twitter/android/service/ScribeEvent;

    invoke-static {v0, v1}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/EventsLandingFragment;Lcom/twitter/android/service/ScribeEvent;)V

    :goto_46
    if-lez p5, :cond_4f

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    add-int/lit8 v1, p5, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/EventsLandingFragment;I)I

    :cond_4f
    :goto_4f
    return-void

    :cond_50
    move v0, v2

    goto :goto_10

    :cond_52
    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    iget v1, v3, Lcom/twitter/android/co;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/EventsLandingFragment;->c(I)V

    goto :goto_46

    :cond_5a
    iget-object v0, p0, Lcom/twitter/android/ap;->b:Landroid/content/Context;

    const v4, 0x7f0b01eb

    invoke-static {v0, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    iget v1, v3, Lcom/twitter/android/co;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/EventsLandingFragment;->c(I)V

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/ao;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/ao;->a(Z)V

    goto :goto_4f
.end method

.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .registers 9

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/EventsLandingFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/EventsLandingFragment;->b(Lcom/twitter/android/EventsLandingFragment;Z)Z

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_23

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->d(Lcom/twitter/android/EventsLandingFragment;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->c(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/fo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fo;->notifyDataSetChanged()V

    :cond_23
    return-void
.end method

.method public final a(Ljava/util/HashMap;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/ao;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/ap;->a:Lcom/twitter/android/EventsLandingFragment;

    invoke-static {v0}, Lcom/twitter/android/EventsLandingFragment;->c(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/fo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fo;->notifyDataSetChanged()V

    return-void
.end method
