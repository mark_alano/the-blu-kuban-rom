.class final Lcom/twitter/android/as;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/twitter/android/provider/m;

.field final synthetic c:Lcom/twitter/android/client/b;

.field final synthetic d:J

.field final synthetic e:Lcom/twitter/android/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/GalleryActivity;ZLcom/twitter/android/provider/m;Lcom/twitter/android/client/b;J)V
    .registers 7

    iput-object p1, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    iput-boolean p2, p0, Lcom/twitter/android/as;->a:Z

    iput-object p3, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iput-object p4, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    iput-wide p5, p0, Lcom/twitter/android/as;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 10

    const/4 v0, -0x1

    if-ne p2, v0, :cond_78

    iget-boolean v0, p0, Lcom/twitter/android/as;->a:Z

    if-eqz v0, :cond_49

    iget-object v0, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iget-wide v0, v0, Lcom/twitter/android/provider/m;->E:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_33

    iget-object v0, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    iget-object v1, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iget-wide v3, v3, Lcom/twitter/android/provider/m;->E:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity;->c(Ljava/lang/String;)V

    :goto_26
    iget-object v0, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0200f5

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_32
    :goto_32
    return-void

    :cond_33
    iget-object v0, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    iget-object v1, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iget-wide v3, v3, Lcom/twitter/android/provider/m;->A:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity;->c(Ljava/lang/String;)V

    goto :goto_26

    :cond_49
    iget-object v0, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    iget-object v1, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iget-wide v3, v3, Lcom/twitter/android/provider/m;->o:J

    iget-object v5, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iget-object v5, v5, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/as;->d:J

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ab:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    iget-object v0, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0200f8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_32

    :cond_78
    const/4 v0, -0x3

    if-ne p2, v0, :cond_32

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    const-class v2, Lcom/twitter/android/PostActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    const/high16 v3, 0x7f0b

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iget-object v6, v6, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/twitter/android/as;->b:Lcom/twitter/android/provider/m;

    iget-object v6, v6, Lcom/twitter/android/provider/m;->d:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/twitter/android/GalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.twitter.android.post.quote"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/as;->d:J

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ad:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_32
.end method
