.class final Lcom/twitter/android/av;
.super Landroid/support/v4/view/PagerAdapter;


# instance fields
.field private a:Landroid/support/v4/util/LruCache;

.field private b:Landroid/database/Cursor;

.field private final c:Lcom/twitter/android/client/b;

.field private final d:Ljava/util/ArrayList;

.field private final e:Landroid/util/SparseArray;

.field private f:Lcom/twitter/android/aw;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/b;)V
    .registers 3

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/av;->c:Lcom/twitter/android/client/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av;->d:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/av;->e:Landroid/util/SparseArray;

    return-void
.end method

.method private a(Lcom/twitter/android/aw;)V
    .registers 9

    const/4 v1, 0x0

    iget v0, p1, Lcom/twitter/android/aw;->c:I

    iget-object v3, p1, Lcom/twitter/android/aw;->a:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    if-eqz v4, :cond_6b

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_6b

    const/16 v0, 0x17

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iget-object v0, p0, Lcom/twitter/android/av;->a:Landroid/support/v4/util/LruCache;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_6c

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetMedia;

    move-object v2, v0

    :goto_2a
    if-nez v2, :cond_70

    const/16 v0, 0x22

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/z;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/api/TweetMedia;

    if-eqz v0, :cond_70

    aget-object v0, v0, v1

    iget-object v2, p0, Lcom/twitter/android/av;->a:Landroid/support/v4/util/LruCache;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Ljava/lang/ref/SoftReference;

    invoke-direct {v5, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4a
    if-eqz v0, :cond_6b

    iget-object v2, p0, Lcom/twitter/android/av;->c:Lcom/twitter/android/client/b;

    iget v4, v2, Lcom/twitter/android/client/b;->g:F

    invoke-virtual {v0, v4}, Lcom/twitter/android/api/TweetMedia;->a(F)Lcom/twitter/android/api/m;

    move-result-object v0

    new-instance v4, Lcom/twitter/android/util/f;

    iget-object v0, v0, Lcom/twitter/android/api/m;->a:Ljava/lang/String;

    invoke-direct {v4, v0}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    iput-object v4, p1, Lcom/twitter/android/aw;->d:Lcom/twitter/android/util/f;

    iget-object v0, p1, Lcom/twitter/android/aw;->d:Lcom/twitter/android/util/f;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    if-nez v0, :cond_6e

    const/4 v0, 0x1

    :goto_69
    iput-boolean v0, p1, Lcom/twitter/android/aw;->b:Z

    :cond_6b
    return-void

    :cond_6c
    const/4 v2, 0x0

    goto :goto_2a

    :cond_6e
    move v0, v1

    goto :goto_69

    :cond_70
    move-object v0, v2

    goto :goto_4a
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    if-ne p1, v1, :cond_6

    :goto_5
    return-object v0

    :cond_6
    iput-object p1, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/twitter/android/av;->a:Landroid/support/v4/util/LruCache;

    if-eqz p1, :cond_1c

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_19

    new-instance v2, Landroid/support/v4/util/LruCache;

    invoke-direct {v2, v0}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v2, p0, Lcom/twitter/android/av;->a:Landroid/support/v4/util/LruCache;

    :cond_19
    invoke-virtual {p0}, Lcom/twitter/android/av;->notifyDataSetChanged()V

    :cond_1c
    move-object v0, v1

    goto :goto_5
.end method

.method public final a(I)Lcom/twitter/android/provider/m;
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_14

    new-instance v0, Lcom/twitter/android/provider/m;

    iget-object v1, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    invoke-direct {v0, v1}, Lcom/twitter/android/provider/m;-><init>(Landroid/database/Cursor;)V

    :goto_13
    return-object v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public final a()V
    .registers 5

    iget-object v2, p0, Lcom/twitter/android/av;->e:Landroid/util/SparseArray;

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1b

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/aw;

    iget-boolean v3, v0, Lcom/twitter/android/aw;->b:Z

    if-eqz v3, :cond_17

    invoke-direct {p0, v0}, Lcom/twitter/android/av;->a(Lcom/twitter/android/aw;)V

    :cond_17
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_1b
    return-void
.end method

.method public final destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 6

    check-cast p3, Lcom/twitter/android/aw;

    iget-object v0, p3, Lcom/twitter/android/aw;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/twitter/android/av;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/android/av;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public final finishUpdate(Landroid/view/ViewGroup;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/av;->f:Lcom/twitter/android/aw;

    if-eqz v0, :cond_b

    iget-boolean v1, v0, Lcom/twitter/android/aw;->b:Z

    if-eqz v1, :cond_b

    invoke-direct {p0, v0}, Lcom/twitter/android/av;->a(Lcom/twitter/android/aw;)V

    :cond_b
    return-void
.end method

.method public final getCount()I
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/av;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/av;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    :goto_1a
    new-instance v1, Lcom/twitter/android/aw;

    invoke-direct {v1}, Lcom/twitter/android/aw;-><init>()V

    iput p2, v1, Lcom/twitter/android/aw;->c:I

    iput-object v0, v1, Lcom/twitter/android/aw;->a:Landroid/widget/ImageView;

    invoke-direct {p0, v1}, Lcom/twitter/android/av;->a(Lcom/twitter/android/aw;)V

    iget-object v2, p0, Lcom/twitter/android/av;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v1

    :cond_2f
    iget-object v0, p0, Lcom/twitter/android/av;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_1a
.end method

.method public final isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 4

    check-cast p2, Lcom/twitter/android/aw;

    iget-object v0, p2, Lcom/twitter/android/aw;->a:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 4

    check-cast p3, Lcom/twitter/android/aw;

    iput-object p3, p0, Lcom/twitter/android/av;->f:Lcom/twitter/android/aw;

    return-void
.end method
