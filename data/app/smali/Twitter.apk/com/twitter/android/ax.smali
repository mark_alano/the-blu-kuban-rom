.class final Lcom/twitter/android/ax;
.super Lcom/twitter/android/client/j;


# instance fields
.field final synthetic a:Lcom/twitter/android/GalleryActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/GalleryActivity;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-direct {p0}, Lcom/twitter/android/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .registers 8

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v0, p2}, Lcom/twitter/android/GalleryActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    const v1, 0x7f0b005d

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_19
    return-void
.end method

.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;IJ)V
    .registers 11

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v0, p2}, Lcom/twitter/android/GalleryActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1d

    const/16 v0, 0x8b

    if-eq p5, v0, :cond_1d

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    const v1, 0x7f0b0057

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1d
    return-void
.end method

.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;JLcom/twitter/android/api/a;)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v0, p2}, Lcom/twitter/android/GalleryActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_2e

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->b(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/provider/m;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/android/provider/m;->o:J

    cmp-long v0, v0, p5

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->d(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/widget/au;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/twitter/android/widget/au;->a(Lcom/twitter/android/api/a;)V

    :cond_21
    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->e(Lcom/twitter/android/GalleryActivity;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p7}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2e
    return-void
.end method

.method public final a(Ljava/util/HashMap;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->c(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/av;->a()V

    return-void
.end method

.method public final b(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v0, p2}, Lcom/twitter/android/GalleryActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    const v1, 0x7f0b0058

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_19
    return-void
.end method

.method public final c(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v0, p2}, Lcom/twitter/android/GalleryActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/ax;->a:Lcom/twitter/android/GalleryActivity;

    const v1, 0x7f0b005b

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_19
    return-void
.end method
