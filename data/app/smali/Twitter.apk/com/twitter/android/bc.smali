.class final Lcom/twitter/android/bc;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/twitter/android/HomeTabActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/HomeTabActivity;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/bc;->a:Lcom/twitter/android/HomeTabActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/HomeTabActivity;Lcom/twitter/android/ay;)V
    .registers 3

    invoke-direct {p0, p1}, Lcom/twitter/android/bc;-><init>(Lcom/twitter/android/HomeTabActivity;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 8

    const/16 v5, 0x190

    const/4 v4, 0x0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/android/client/Session;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_48

    :goto_c
    return-void

    :pswitch_d
    iget-object v1, p0, Lcom/twitter/android/bc;->a:Lcom/twitter/android/HomeTabActivity;

    iget-object v1, v1, Lcom/twitter/android/HomeTabActivity;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, v0, v4, v4, v5}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/Session;III)Ljava/lang/String;

    new-instance v1, Lcom/twitter/android/client/a;

    iget-object v2, p0, Lcom/twitter/android/bc;->a:Lcom/twitter/android/HomeTabActivity;

    invoke-virtual {v0}, Lcom/twitter/android/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    const-string v3, "hometab"

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/twitter/android/client/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lcom/twitter/android/client/a;->a()Lcom/twitter/android/client/a;

    move-result-object v0

    const-string v1, "ft"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/a;->b(Ljava/lang/String;J)Lcom/twitter/android/client/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/a;->c()Z

    goto :goto_c

    :pswitch_33
    iget-object v1, p0, Lcom/twitter/android/bc;->a:Lcom/twitter/android/HomeTabActivity;

    const/4 v2, 0x1

    const/16 v3, 0x3e8

    const-string v4, "taut"

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/twitter/android/HomeTabActivity;->a(Lcom/twitter/android/client/Session;IILjava/lang/String;)V

    goto :goto_c

    :pswitch_3e
    iget-object v1, p0, Lcom/twitter/android/bc;->a:Lcom/twitter/android/HomeTabActivity;

    const/4 v2, 0x3

    const-string v3, "tatt"

    invoke-virtual {v1, v0, v2, v5, v3}, Lcom/twitter/android/HomeTabActivity;->a(Lcom/twitter/android/client/Session;IILjava/lang/String;)V

    goto :goto_c

    nop

    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_d
        :pswitch_33
        :pswitch_3e
    .end packed-switch
.end method
