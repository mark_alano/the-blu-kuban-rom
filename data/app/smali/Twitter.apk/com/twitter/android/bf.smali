.class public final Lcom/twitter/android/bf;
.super Lcom/twitter/android/gs;


# instance fields
.field private c:Ljava/util/HashMap;

.field private d:Lcom/twitter/android/widget/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/client/b;Lcom/twitter/android/widget/a;Ljava/util/HashMap;)V
    .registers 16

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v5, v4

    move-object v7, v6

    move v8, v4

    move v9, v4

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/gs;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V

    iput-object p5, p0, Lcom/twitter/android/bf;->c:Ljava/util/HashMap;

    iput-object p4, p0, Lcom/twitter/android/bf;->d:Lcom/twitter/android/widget/a;

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 12

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    check-cast p1, Lcom/twitter/android/widget/UserApprovalView;

    invoke-virtual {p0, p1, p3, v0, v1}, Lcom/twitter/android/bf;->a(Lcom/twitter/android/widget/BaseUserView;Landroid/database/Cursor;J)V

    iget-wide v2, p0, Lcom/twitter/android/bf;->a:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1a

    invoke-virtual {p1, v4, v7}, Lcom/twitter/android/widget/UserApprovalView;->c(II)V

    invoke-virtual {p1, v5, v7}, Lcom/twitter/android/widget/UserApprovalView;->c(II)V

    :cond_19
    :goto_19
    return-void

    :cond_1a
    invoke-virtual {p1, v4, v4}, Lcom/twitter/android/widget/UserApprovalView;->c(II)V

    invoke-virtual {p1, v5, v4}, Lcom/twitter/android/widget/UserApprovalView;->c(II)V

    iget-object v2, p0, Lcom/twitter/android/bf;->c:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_48

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v5, v1, :cond_3b

    invoke-virtual {p1, v4, v5}, Lcom/twitter/android/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p1, v5, v4}, Lcom/twitter/android/widget/UserApprovalView;->a(IZ)V

    goto :goto_19

    :cond_3b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v6, v0, :cond_19

    invoke-virtual {p1, v4, v4}, Lcom/twitter/android/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p1, v5, v5}, Lcom/twitter/android/widget/UserApprovalView;->a(IZ)V

    goto :goto_19

    :cond_48
    invoke-virtual {p1, v4, v4}, Lcom/twitter/android/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p1, v5, v4}, Lcom/twitter/android/widget/UserApprovalView;->a(IZ)V

    goto :goto_19
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13

    const/4 v8, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030065

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/UserApprovalView;

    const v2, 0x7f020034

    iget-object v3, p0, Lcom/twitter/android/bf;->d:Lcom/twitter/android/widget/a;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/widget/UserApprovalView;->a(IILcom/twitter/android/widget/a;)V

    const v2, 0x7f020042

    iget-object v3, p0, Lcom/twitter/android/bf;->d:Lcom/twitter/android/widget/a;

    invoke-virtual {v0, v8, v2, v3}, Lcom/twitter/android/widget/UserApprovalView;->a(IILcom/twitter/android/widget/a;)V

    const v2, 0x7f020035

    iget v3, p0, Lcom/twitter/android/bf;->b:I

    iget v5, p0, Lcom/twitter/android/bf;->b:I

    move v4, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/widget/UserApprovalView;->a(IIIIII)V

    const v4, 0x7f020043

    iget v5, p0, Lcom/twitter/android/bf;->b:I

    iget v7, p0, Lcom/twitter/android/bf;->b:I

    move-object v2, v0

    move v3, v8

    move v6, v1

    move v8, v1

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/android/widget/UserApprovalView;->a(IIIIII)V

    return-object v0
.end method
