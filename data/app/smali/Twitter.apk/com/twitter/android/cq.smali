.class public final Lcom/twitter/android/cq;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private a:Lcom/twitter/android/fo;

.field private b:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>(Lcom/twitter/android/fo;)V
    .registers 3

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/cq;->a:Lcom/twitter/android/fo;

    new-instance v0, Lcom/twitter/android/cr;

    invoke-direct {v0, p0}, Lcom/twitter/android/cr;-><init>(Lcom/twitter/android/cq;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/fo;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/cq;->b:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public final getCount()I
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/cq;->a:Lcom/twitter/android/fo;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/cq;->a:Lcom/twitter/android/fo;

    invoke-virtual {v0}, Lcom/twitter/android/fo;->getCount()I

    move-result v0

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    const/4 v2, 0x0

    check-cast p2, Lcom/twitter/android/widget/CardRowView;

    if-nez p2, :cond_17

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030034

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    move-object p2, v0

    :cond_17
    invoke-virtual {p2, v2}, Lcom/twitter/android/widget/CardRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->a()Landroid/widget/ListAdapter;

    move-result-object v1

    if-nez v1, :cond_2d

    iget-object v1, p0, Lcom/twitter/android/cq;->a:Lcom/twitter/android/fo;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/twitter/android/cq;->b:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_2d
    return-object p2
.end method
