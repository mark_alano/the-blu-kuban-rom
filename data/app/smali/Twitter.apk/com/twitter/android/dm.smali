.class final Lcom/twitter/android/dm;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/android/ProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;I)V
    .registers 3

    iput-object p1, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    iput p2, p0, Lcom/twitter/android/dm;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageScrollStateChanged(I)V
    .registers 2

    return-void
.end method

.method public final onPageScrolled(IFI)V
    .registers 6

    iget v0, p0, Lcom/twitter/android/dm;->a:I

    if-lt p3, v0, :cond_12

    iget-object v0, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->d(Lcom/twitter/android/ProfileFragment;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_12
    if-nez p1, :cond_1e

    iget-object v0, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->e(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/widget/ProfileHeader;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/ProfileHeader;->setAlpha(F)V

    :goto_1d
    return-void

    :cond_1e
    iget-object v0, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->e(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/widget/ProfileHeader;

    move-result-object v0

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProfileHeader;->setAlpha(F)V

    goto :goto_1d
.end method

.method public final onPageSelected(I)V
    .registers 10

    const v7, 0x7f02011b

    const v6, 0x7f02011a

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/android/ProfileFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/dt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/dt;->getCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_50

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    if-nez p1, :cond_3e

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_30
    iget-object v0, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->d(Lcom/twitter/android/ProfileFragment;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return-void

    :cond_3e
    const/4 v2, 0x1

    if-ne p1, v2, :cond_48

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_30

    :cond_48
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Number of items is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_50
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_30
.end method
