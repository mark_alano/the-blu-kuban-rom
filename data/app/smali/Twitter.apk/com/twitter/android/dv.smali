.class final Lcom/twitter/android/dv;
.super Lcom/twitter/android/client/j;


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;

.field private b:Landroid/os/Handler;

.field private c:Lcom/twitter/android/dx;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Lcom/twitter/android/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/twitter/android/client/Session;)V
    .registers 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, v1}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/android/ProfileFragment;I)I

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, v1}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/android/ProfileFragment;I)I

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {p1}, Lcom/twitter/android/client/Session;->f()Lcom/twitter/android/api/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/api/ad;)V

    return-void
.end method

.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .registers 13

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_1d

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1e

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0091

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1d
    :goto_1d
    return-void

    :cond_1e
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0092

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1d
.end method

.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;JII)V
    .registers 13

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_2f

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1b

    if-lez p8, :cond_1b

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1b
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->p:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_refresh"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2f
    return-void
.end method

.method public final a(Ljava/util/HashMap;)V
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->s(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/util/f;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/r;

    if-eqz v0, :cond_26

    iget-object v1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->e(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/widget/ProfileHeader;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v3}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/android/util/r;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_26
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->t(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/util/f;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/r;

    if-eqz v0, :cond_3d

    iget-object v1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/util/r;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Landroid/graphics/Bitmap;)V

    :cond_3d
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->u(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/fo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fo;->notifyDataSetChanged()V

    return-void
.end method

.method public final b(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;I)V
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_11

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_11

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->m()V

    :cond_11
    return-void
.end method

.method public final b(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;JI)V
    .registers 11

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_26

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_27

    const/16 v0, 0x191

    if-ne p3, v0, :cond_1f

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0}, Lcom/twitter/android/dy;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/gd;->a(I)V

    :cond_1f
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0, v2}, Lcom/twitter/android/dy;->a(Z)V

    :cond_26
    :goto_26
    return-void

    :cond_27
    if-nez p7, :cond_26

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0}, Lcom/twitter/android/dy;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gd;

    invoke-virtual {v0}, Lcom/twitter/android/gd;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3d

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/gd;->a(I)V

    :cond_3d
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0, v2}, Lcom/twitter/android/dy;->a(Z)V

    goto :goto_26
.end method

.method public final b(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/android/api/ad;)V
    .registers 10

    const/16 v0, 0xc9

    if-eq p3, v0, :cond_27

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->v(Lcom/twitter/android/ProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_27

    if-eqz p5, :cond_27

    invoke-virtual {p5}, Lcom/twitter/android/api/ad;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->q:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_27

    new-instance v0, Lcom/twitter/android/du;

    iget-object v1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {v0, v1}, Lcom/twitter/android/du;-><init>(Lcom/twitter/android/ProfileFragment;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/du;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_27
    return-void
.end method

.method public final c(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;JI)V
    .registers 9

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_15

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_15

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->u(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/fo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fo;->notifyDataSetChanged()V

    :cond_15
    return-void
.end method

.method public final c(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/android/api/ad;)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->v(Lcom/twitter/android/ProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_1b

    if-eqz p5, :cond_1b

    invoke-virtual {p5}, Lcom/twitter/android/api/ad;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->q:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1b

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p5}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/api/ad;)V

    :cond_1b
    return-void
.end method

.method public final d(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .registers 9

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v0, v0, Lcom/twitter/android/ProfileFragment;->q:J

    cmp-long v0, p5, v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->l()V

    :cond_13
    return-void
.end method

.method public final d(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/android/api/ad;)V
    .registers 10

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_26

    iget-object v0, p0, Lcom/twitter/android/dv;->b:Landroid/os/Handler;

    if-nez v0, :cond_f

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/dv;->b:Landroid/os/Handler;

    :cond_f
    iget-object v0, p0, Lcom/twitter/android/dv;->c:Lcom/twitter/android/dx;

    if-nez v0, :cond_1a

    new-instance v0, Lcom/twitter/android/dx;

    invoke-direct {v0, p0}, Lcom/twitter/android/dx;-><init>(Lcom/twitter/android/dv;)V

    iput-object v0, p0, Lcom/twitter/android/dv;->c:Lcom/twitter/android/dx;

    :cond_1a
    iget-object v0, p0, Lcom/twitter/android/dv;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/dv;->c:Lcom/twitter/android/dx;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_26
    return-void
.end method

.method public final e(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .registers 8

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_34

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_34

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->g(Lcom/twitter/android/ProfileFragment;)I

    move-result v1

    invoke-static {v1, v2}, Lcom/twitter/android/provider/ad;->b(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;I)I

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    const v1, 0x7f0b010e

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b0077

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_34
    return-void
.end method

.method public final e(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/android/api/ad;)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->v(Lcom/twitter/android/ProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_19

    if-eqz p5, :cond_19

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v0, v0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-wide v2, p5, Lcom/twitter/android/api/ad;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p5}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/api/ad;)V

    :cond_19
    return-void
.end method

.method public final f(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .registers 8

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_34

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_34

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->g(Lcom/twitter/android/ProfileFragment;)I

    move-result v1

    invoke-static {v1, v2}, Lcom/twitter/android/provider/ad;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;I)I

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    const v1, 0x7f0b010f

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b007a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_34
    return-void
.end method

.method public final f(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/android/api/ad;)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_1d

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1d

    if-eqz p5, :cond_1d

    iget-wide v0, p5, Lcom/twitter/android/api/ad;->a:J

    iget-object v2, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->q:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p5}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/api/ad;)V

    :cond_1d
    return-void
.end method

.method public final g(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .registers 12

    const/16 v4, 0x10

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-nez v0, :cond_d

    :goto_c
    return-void

    :cond_d
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->g(Lcom/twitter/android/ProfileFragment;)I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_2f

    move v0, v1

    :goto_18
    const/16 v3, 0xc8

    if-ne p3, v3, :cond_42

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0b007c

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_c

    :cond_2f
    move v0, v2

    goto :goto_18

    :cond_31
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0b007f

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_c

    :cond_42
    const/16 v3, 0x3e9

    if-ne p3, v3, :cond_9e

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/android/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/twitter/android/dw;

    invoke-direct {v4, p0, v3, v0}, Lcom/twitter/android/dw;-><init>(Lcom/twitter/android/dv;Landroid/app/Activity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_88

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-object v0, v0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    :goto_61
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0b007d

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-virtual {v3, v6, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b00eb

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b00ec

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_c

    :cond_88
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "@"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v5}, Lcom/twitter/android/ProfileFragment;->f(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_61

    :cond_9e
    iget-object v2, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v2}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0b01e0

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    if-eqz v0, :cond_c1

    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->g(Lcom/twitter/android/ProfileFragment;)I

    move-result v1

    invoke-static {v1, v4}, Lcom/twitter/android/provider/ad;->b(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;I)I

    goto/16 :goto_c

    :cond_c1
    iget-object v0, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/dv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->g(Lcom/twitter/android/ProfileFragment;)I

    move-result v1

    invoke-static {v1, v4}, Lcom/twitter/android/provider/ad;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;I)I

    goto/16 :goto_c
.end method
