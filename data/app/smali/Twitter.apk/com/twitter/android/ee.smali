.class public final Lcom/twitter/android/ee;
.super Ljava/lang/Object;


# static fields
.field public static final badge_spacing:I = 0x7f0c0014

.field public static final bounceTravelDistance:I = 0x7f0c0024

.field public static final button_padding:I = 0x7f0c0004

.field public static final card_inset:I = 0x7f0c0018

.field public static final card_radius:I = 0x7f0c000e

.field public static final chevron_padding:I = 0x7f0c0019

.field public static final divider_thickness:I = 0x7f0c000d

.field public static final event_header_image:I = 0x7f0c0023

.field public static final font_size_default:I = 0x7f0c0008

.field public static final font_size_large:I = 0x7f0c0007

.field public static final font_size_small:I = 0x7f0c0009

.field public static final font_size_xlarge:I = 0x7f0c0006

.field public static final font_size_xsmall:I = 0x7f0c000a

.field public static final font_size_xxlarge:I = 0x7f0c0005

.field public static final icon_spacing:I = 0x7f0c0015

.field public static final image_margin_right:I = 0x7f0c0017

.field public static final image_margin_top:I = 0x7f0c0016

.field public static final inline_map_height:I = 0x7f0c000f

.field public static final list_preferred_height:I = 0x7f0c000c

.field public static final list_row_padding:I = 0x7f0c0002

.field public static final map_height:I = 0x7f0c0010

.field public static final media_thumbnail_size:I = 0x7f0c0026

.field public static final mini_user_image_size:I = 0x7f0c0001

.field public static final onebox_news_image:I = 0x7f0c0022

.field public static final photo_attachment_size:I = 0x7f0c0011

.field public static final profile_avatar_size:I = 0x7f0c001a

.field public static final profile_button_height:I = 0x7f0c0020

.field public static final profile_button_margin:I = 0x7f0c0021

.field public static final profile_button_width:I = 0x7f0c001f

.field public static final profile_indicators_inner_padding:I = 0x7f0c001d

.field public static final profile_indicators_margin_bottom:I = 0x7f0c001b

.field public static final profile_indicators_outer_padding:I = 0x7f0c001e

.field public static final profile_indicators_padding_top:I = 0x7f0c001c

.field public static final standard_spacing:I = 0x7f0c0003

.field public static final tabbar_height:I = 0x7f0c0028

.field public static final tap_to_refresh_height:I = 0x7f0c0025

.field public static final text_single_line_height:I = 0x7f0c000b

.field public static final toolbar_button_width:I = 0x7f0c0012

.field public static final tweet_stats_height:I = 0x7f0c0027

.field public static final user_image_size:I = 0x7f0c0000

.field public static final widget_header_height:I = 0x7f0c0013
