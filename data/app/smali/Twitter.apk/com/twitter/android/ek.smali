.class public final Lcom/twitter/android/ek;
.super Ljava/lang/Object;


# static fields
.field public static final CardRowView:[I = null

.field public static final CardRowView_cardStyle:I = 0xb

.field public static final CardRowView_cornerRadius:I = 0x9

.field public static final CardRowView_dividerColor:I = 0xf

.field public static final CardRowView_dividerHeight:I = 0xe

.field public static final CardRowView_inset:I = 0x4

.field public static final CardRowView_insetBottom:I = 0x8

.field public static final CardRowView_insetLeft:I = 0x5

.field public static final CardRowView_insetRight:I = 0x7

.field public static final CardRowView_insetTop:I = 0x6

.field public static final CardRowView_shadowColor:I = 0x3

.field public static final CardRowView_shadowDx:I = 0x1

.field public static final CardRowView_shadowDy:I = 0x2

.field public static final CardRowView_shadowRadius:I = 0x0

.field public static final CardRowView_single:I = 0xa

.field public static final CardRowView_strokeColor:I = 0xd

.field public static final CardRowView_strokeWidth:I = 0xc

.field public static final CheckBoxListPreference:[I = null

.field public static final CheckBoxListPreference_offValue:I = 0x0

.field public static final CroppableImageView:[I = null

.field public static final CroppableImageView_cropRectPadding:I = 0x0

.field public static final CroppableImageView_cropRectStrokeColor:I = 0x1

.field public static final CroppableImageView_cropShadowColor:I = 0x2

.field public static final HorizontalListView:[I = null

.field public static final HorizontalListView_divider:I = 0x0

.field public static final HorizontalListView_dividerWidth:I = 0x1

.field public static final NotchView:[I = null

.field public static final NotchView_bg:I = 0x1

.field public static final NotchView_notch:I = 0x0

.field public static final NotchView_offsetLeft:I = 0x2

.field public static final OverlayImageView:[I = null

.field public static final OverlayImageView_overlayDrawable:I = 0x0

.field public static final PopupEditText:[I = null

.field public static final PopupEditText_dropDownHorizontalOffset:I = 0x2

.field public static final PopupEditText_dropDownRowHeight:I = 0x3

.field public static final PopupEditText_dropDownSelector:I = 0x0

.field public static final PopupEditText_dropDownVerticalOffset:I = 0x1

.field public static final ProfileHeader:[I = null

.field public static final ProfileHeader_filterColor:I = 0x2

.field public static final ProfileHeader_filterMaxOpacity:I = 0x1

.field public static final ProfileHeader_overlayDrawable:I = 0x0

.field public static final RefreshableListView:[I = null

.field public static final RefreshableListView_loadingText:I = 0x3

.field public static final RefreshableListView_pullBackgroundColor:I = 0x5

.field public static final RefreshableListView_pullDivider:I = 0x6

.field public static final RefreshableListView_pullText:I = 0x2

.field public static final RefreshableListView_refreshFooter:I = 0x1

.field public static final RefreshableListView_refreshHeader:I = 0x0

.field public static final RefreshableListView_releaseText:I = 0x4

.field public static final RefreshableListView_rotateDownAnim:I = 0x8

.field public static final RefreshableListView_rotateUpAnim:I = 0x7

.field public static final SegmentedControl:[I = null

.field public static final SegmentedControl_divider:I = 0x6

.field public static final SegmentedControl_segmentLabels:I = 0x8

.field public static final SegmentedControl_shadowColor:I = 0x3

.field public static final SegmentedControl_shadowDx:I = 0x1

.field public static final SegmentedControl_shadowDy:I = 0x2

.field public static final SegmentedControl_shadowRadius:I = 0x0

.field public static final SegmentedControl_src:I = 0x7

.field public static final SegmentedControl_textColor:I = 0x5

.field public static final SegmentedControl_textSize:I = 0x4

.field public static final ShadowTextView:[I = null

.field public static final ShadowTextView_shadowColor:I = 0x3

.field public static final ShadowTextView_shadowDx:I = 0x1

.field public static final ShadowTextView_shadowDy:I = 0x2

.field public static final ShadowTextView_shadowRadius:I = 0x0

.field public static final StoryView:[I = null

.field public static final StoryView_imageSize:I = 0x0

.field public static final TweetStatView:[I = null

.field public static final TweetStatView_nameText:I = 0x0

.field public static final TweetStatView_nameTextColor:I = 0x1

.field public static final TweetStatView_valueText:I = 0x2

.field public static final TweetStatView_valueTextColor:I = 0x3

.field public static final TweetView:[I = null

.field public static final TweetView_badgeSpacing:I = 0x1b

.field public static final TweetView_bylineColor:I = 0x7

.field public static final TweetView_bylineSize:I = 0x6

.field public static final TweetView_contentColor:I = 0x5

.field public static final TweetView_contentSize:I = 0x4

.field public static final TweetView_dogearFavDrawable:I = 0xe

.field public static final TweetView_dogearRTDrawable:I = 0xd

.field public static final TweetView_dogearRTFavDrawable:I = 0xc

.field public static final TweetView_iconSpacing:I = 0x9

.field public static final TweetView_lineSpacingExtra:I = 0x3

.field public static final TweetView_lineSpacingMultiplier:I = 0x2

.field public static final TweetView_locationIcon:I = 0x10

.field public static final TweetView_mediaIcon:I = 0x11

.field public static final TweetView_placeholderDrawable:I = 0xa

.field public static final TweetView_playerIcon:I = 0x13

.field public static final TweetView_politicalDrawable:I = 0x1

.field public static final TweetView_profileImageHeight:I = 0x1a

.field public static final TweetView_profileImageMarginLeft:I = 0x15

.field public static final TweetView_profileImageMarginRight:I = 0x17

.field public static final TweetView_profileImageMarginTop:I = 0x16

.field public static final TweetView_profileImageOverlayDrawable:I = 0x18

.field public static final TweetView_profileImageWidth:I = 0x19

.field public static final TweetView_promotedDrawable:I = 0x0

.field public static final TweetView_replyIcon:I = 0x12

.field public static final TweetView_retweetDrawable:I = 0xb

.field public static final TweetView_summaryIcon:I = 0x14

.field public static final TweetView_timestampColor:I = 0x8

.field public static final TweetView_topPillDrawable:I = 0xf

.field public static final UserView:[I = null

.field public static final UserView_defaultProfileImageDrawable:I = 0x2

.field public static final UserView_politicalDrawable:I = 0x1

.field public static final UserView_promotedDrawable:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    const/16 v6, 0x9

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_7c

    sput-object v0, Lcom/twitter/android/ek;->CardRowView:[I

    new-array v0, v4, [I

    const v1, 0x7f010056

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/android/ek;->CheckBoxListPreference:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_a0

    sput-object v0, Lcom/twitter/android/ek;->CroppableImageView:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_aa

    sput-object v0, Lcom/twitter/android/ek;->HorizontalListView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_b2

    sput-object v0, Lcom/twitter/android/ek;->NotchView:[I

    new-array v0, v4, [I

    const v1, 0x7f01001e

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/android/ek;->OverlayImageView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_bc

    sput-object v0, Lcom/twitter/android/ek;->PopupEditText:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_c8

    sput-object v0, Lcom/twitter/android/ek;->ProfileHeader:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_d2

    sput-object v0, Lcom/twitter/android/ek;->RefreshableListView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_e8

    sput-object v0, Lcom/twitter/android/ek;->SegmentedControl:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_fe

    sput-object v0, Lcom/twitter/android/ek;->ShadowTextView:[I

    new-array v0, v4, [I

    const v1, 0x7f010055

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/android/ek;->StoryView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_10a

    sput-object v0, Lcom/twitter/android/ek;->TweetStatView:[I

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_116

    sput-object v0, Lcom/twitter/android/ek;->TweetView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_152

    sput-object v0, Lcom/twitter/android/ek;->UserView:[I

    return-void

    nop

    :array_7c
    .array-data 0x4
        0x12t 0x0t 0x1t 0x7ft
        0x13t 0x0t 0x1t 0x7ft
        0x14t 0x0t 0x1t 0x7ft
        0x15t 0x0t 0x1t 0x7ft
        0x3at 0x0t 0x1t 0x7ft
        0x3bt 0x0t 0x1t 0x7ft
        0x3ct 0x0t 0x1t 0x7ft
        0x3dt 0x0t 0x1t 0x7ft
        0x3et 0x0t 0x1t 0x7ft
        0x3ft 0x0t 0x1t 0x7ft
        0x40t 0x0t 0x1t 0x7ft
        0x41t 0x0t 0x1t 0x7ft
        0x42t 0x0t 0x1t 0x7ft
        0x43t 0x0t 0x1t 0x7ft
        0x44t 0x0t 0x1t 0x7ft
        0x45t 0x0t 0x1t 0x7ft
    .end array-data

    :array_a0
    .array-data 0x4
        0x50t 0x0t 0x1t 0x7ft
        0x51t 0x0t 0x1t 0x7ft
        0x52t 0x0t 0x1t 0x7ft
    .end array-data

    :array_aa
    .array-data 0x4
        0x1dt 0x0t 0x1t 0x7ft
        0x4ft 0x0t 0x1t 0x7ft
    .end array-data

    :array_b2
    .array-data 0x4
        0x4at 0x0t 0x1t 0x7ft
        0x4bt 0x0t 0x1t 0x7ft
        0x4ct 0x0t 0x1t 0x7ft
    .end array-data

    :array_bc
    .array-data 0x4
        0x46t 0x0t 0x1t 0x7ft
        0x47t 0x0t 0x1t 0x7ft
        0x48t 0x0t 0x1t 0x7ft
        0x49t 0x0t 0x1t 0x7ft
    .end array-data

    :array_c8
    .array-data 0x4
        0x1et 0x0t 0x1t 0x7ft
        0x5bt 0x0t 0x1t 0x7ft
        0x5ct 0x0t 0x1t 0x7ft
    .end array-data

    :array_d2
    .array-data 0x4
        0x1t 0x0t 0x1t 0x7ft
        0x2t 0x0t 0x1t 0x7ft
        0x3t 0x0t 0x1t 0x7ft
        0x4t 0x0t 0x1t 0x7ft
        0x5t 0x0t 0x1t 0x7ft
        0x6t 0x0t 0x1t 0x7ft
        0x7t 0x0t 0x1t 0x7ft
        0x8t 0x0t 0x1t 0x7ft
        0x9t 0x0t 0x1t 0x7ft
    .end array-data

    :array_e8
    .array-data 0x4
        0x12t 0x0t 0x1t 0x7ft
        0x13t 0x0t 0x1t 0x7ft
        0x14t 0x0t 0x1t 0x7ft
        0x15t 0x0t 0x1t 0x7ft
        0x16t 0x0t 0x1t 0x7ft
        0x17t 0x0t 0x1t 0x7ft
        0x1dt 0x0t 0x1t 0x7ft
        0x4dt 0x0t 0x1t 0x7ft
        0x4et 0x0t 0x1t 0x7ft
    .end array-data

    :array_fe
    .array-data 0x4
        0x12t 0x0t 0x1t 0x7ft
        0x13t 0x0t 0x1t 0x7ft
        0x14t 0x0t 0x1t 0x7ft
        0x15t 0x0t 0x1t 0x7ft
    .end array-data

    :array_10a
    .array-data 0x4
        0x57t 0x0t 0x1t 0x7ft
        0x58t 0x0t 0x1t 0x7ft
        0x59t 0x0t 0x1t 0x7ft
        0x5at 0x0t 0x1t 0x7ft
    .end array-data

    :array_116
    .array-data 0x4
        0x1bt 0x0t 0x1t 0x7ft
        0x1ct 0x0t 0x1t 0x7ft
        0x1ft 0x0t 0x1t 0x7ft
        0x20t 0x0t 0x1t 0x7ft
        0x21t 0x0t 0x1t 0x7ft
        0x22t 0x0t 0x1t 0x7ft
        0x23t 0x0t 0x1t 0x7ft
        0x24t 0x0t 0x1t 0x7ft
        0x25t 0x0t 0x1t 0x7ft
        0x26t 0x0t 0x1t 0x7ft
        0x27t 0x0t 0x1t 0x7ft
        0x28t 0x0t 0x1t 0x7ft
        0x29t 0x0t 0x1t 0x7ft
        0x2at 0x0t 0x1t 0x7ft
        0x2bt 0x0t 0x1t 0x7ft
        0x2ct 0x0t 0x1t 0x7ft
        0x2dt 0x0t 0x1t 0x7ft
        0x2et 0x0t 0x1t 0x7ft
        0x2ft 0x0t 0x1t 0x7ft
        0x30t 0x0t 0x1t 0x7ft
        0x31t 0x0t 0x1t 0x7ft
        0x32t 0x0t 0x1t 0x7ft
        0x33t 0x0t 0x1t 0x7ft
        0x34t 0x0t 0x1t 0x7ft
        0x35t 0x0t 0x1t 0x7ft
        0x36t 0x0t 0x1t 0x7ft
        0x37t 0x0t 0x1t 0x7ft
        0x38t 0x0t 0x1t 0x7ft
    .end array-data

    :array_152
    .array-data 0x4
        0x1bt 0x0t 0x1t 0x7ft
        0x1ct 0x0t 0x1t 0x7ft
        0x39t 0x0t 0x1t 0x7ft
    .end array-data
.end method
