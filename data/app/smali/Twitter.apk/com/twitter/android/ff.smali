.class public final Lcom/twitter/android/ff;
.super Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .registers 4

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/ff;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/fe;

    iput p2, v0, Lcom/twitter/android/fe;->d:I

    invoke-virtual {p0}, Lcom/twitter/android/ff;->notifyDataSetChanged()V

    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    invoke-virtual {p0, p1}, Lcom/twitter/android/ff;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/fe;

    invoke-static {p2, p3, v0}, Lcom/twitter/android/fg;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/fe;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    invoke-virtual {p0}, Lcom/twitter/android/ff;->getCount()I

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/twitter/android/widget/CardRowView;->a(II)V

    return-object v1
.end method
