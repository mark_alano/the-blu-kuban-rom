.class public final Lcom/twitter/android/gd;
.super Landroid/support/v4/widget/CursorAdapter;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Lcom/twitter/android/client/b;

.field private final c:Ljava/util/ArrayList;

.field private final d:Lcom/twitter/android/widget/az;

.field private final e:Z

.field private final f:Lcom/twitter/android/widget/av;

.field private final g:Lcom/twitter/android/gf;

.field private h:Landroid/view/animation/Animation;

.field private i:Landroid/view/animation/Animation;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Lcom/twitter/android/cl;

.field private n:J

.field private o:J

.field private p:Lcom/twitter/android/provider/m;

.field private q:I

.field private r:Landroid/view/View;

.field private s:Landroid/view/ViewGroup;

.field private t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IZLcom/twitter/android/widget/az;Lcom/twitter/android/client/b;Lcom/twitter/android/widget/av;)V
    .registers 15

    const/4 v3, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/gd;-><init>(Landroid/content/Context;IZLcom/twitter/android/widget/az;Lcom/twitter/android/client/b;Lcom/twitter/android/widget/av;Lcom/twitter/android/gf;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZLcom/twitter/android/widget/az;Lcom/twitter/android/client/b;Lcom/twitter/android/widget/av;Lcom/twitter/android/gf;)V
    .registers 15

    const-wide/16 v5, 0xfa

    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/gd;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/gd;->c:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/twitter/android/gd;->h:Landroid/view/animation/Animation;

    iput-object v1, p0, Lcom/twitter/android/gd;->i:Landroid/view/animation/Animation;

    iput-wide v3, p0, Lcom/twitter/android/gd;->n:J

    iput-wide v3, p0, Lcom/twitter/android/gd;->o:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/gd;->q:I

    iput-boolean p3, p0, Lcom/twitter/android/gd;->e:Z

    iput-object p4, p0, Lcom/twitter/android/gd;->d:Lcom/twitter/android/widget/az;

    iput-object p5, p0, Lcom/twitter/android/gd;->b:Lcom/twitter/android/client/b;

    iput-object p6, p0, Lcom/twitter/android/gd;->f:Lcom/twitter/android/widget/av;

    iput-object p7, p0, Lcom/twitter/android/gd;->g:Lcom/twitter/android/gf;

    if-eqz p7, :cond_4b

    const v0, 0x7f040001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const/high16 v1, 0x7f04

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {v1, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iput-object v0, p0, Lcom/twitter/android/gd;->i:Landroid/view/animation/Animation;

    iput-object v1, p0, Lcom/twitter/android/gd;->h:Landroid/view/animation/Animation;

    :cond_4b
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .registers 4

    const/4 v1, 0x0

    if-eqz p1, :cond_b

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    :goto_a
    return-void

    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_a
.end method

.method private i()V
    .registers 3

    iget-wide v0, p0, Lcom/twitter/android/gd;->n:J

    iput-wide v0, p0, Lcom/twitter/android/gd;->o:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/gd;->n:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/gd;->p:Lcom/twitter/android/provider/m;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/gd;->q:I

    return-void
.end method


# virtual methods
.method public final a()J
    .registers 3

    iget-wide v0, p0, Lcom/twitter/android/gd;->n:J

    return-wide v0
.end method

.method public final a(I)V
    .registers 3

    iget v0, p0, Lcom/twitter/android/gd;->l:I

    and-int/2addr v0, p1

    if-eq v0, p1, :cond_d

    iget v0, p0, Lcom/twitter/android/gd;->l:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/android/gd;->l:I

    invoke-virtual {p0}, Lcom/twitter/android/gd;->notifyDataSetChanged()V

    :cond_d
    return-void
.end method

.method public final a(J)V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/gd;->a:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(JI)V
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/gd;->g:Lcom/twitter/android/gf;

    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "You must set an "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/twitter/android/gf;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1f
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/gd;->t:Z

    iput-wide p1, p0, Lcom/twitter/android/gd;->n:J

    iput p3, p0, Lcom/twitter/android/gd;->q:I

    invoke-virtual {p0}, Lcom/twitter/android/gd;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Lcom/twitter/android/cl;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/gd;->m:Lcom/twitter/android/cl;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/gd;->j:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/gd;->j:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_c
    return-void

    :cond_d
    iput-object p1, p0, Lcom/twitter/android/gd;->j:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/gd;->k:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/gd;->notifyDataSetChanged()V

    goto :goto_c
.end method

.method public final b()I
    .registers 2

    iget v0, p0, Lcom/twitter/android/gd;->q:I

    return v0
.end method

.method public final b(JI)V
    .registers 8

    iget-object v0, p0, Lcom/twitter/android/gd;->g:Lcom/twitter/android/gf;

    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "You must set an "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/twitter/android/gf;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1f
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/gd;->t:Z

    iget-wide v0, p0, Lcom/twitter/android/gd;->n:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_30

    iget v0, p0, Lcom/twitter/android/gd;->q:I

    if-eq v0, p3, :cond_2f

    invoke-virtual {p0}, Lcom/twitter/android/gd;->c()V

    :cond_2f
    :goto_2f
    return-void

    :cond_30
    iget-wide v0, p0, Lcom/twitter/android/gd;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3b

    invoke-direct {p0}, Lcom/twitter/android/gd;->i()V

    :cond_3b
    iput-wide p1, p0, Lcom/twitter/android/gd;->n:J

    iput p3, p0, Lcom/twitter/android/gd;->q:I

    invoke-virtual {p0}, Lcom/twitter/android/gd;->notifyDataSetChanged()V

    goto :goto_2f
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 13

    const/16 v4, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v0, 0x14

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_46

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ge;

    iget-object v3, p0, Lcom/twitter/android/gd;->a:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3b

    iget-object v1, v0, Lcom/twitter/android/ge;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/ge;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2d
    :goto_2d
    check-cast p1, Lcom/twitter/android/widget/CardRowView;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/android/widget/CardRowView;->a(II)V

    return-void

    :cond_3b
    iget-object v1, v0, Lcom/twitter/android/ge;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/ge;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2d

    :cond_46
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gc;

    iget-object v1, v0, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/gd;->b:Lcom/twitter/android/client/b;

    iget v2, v2, Lcom/twitter/android/client/b;->f:F

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/TweetView;->a(F)V

    new-instance v2, Lcom/twitter/android/provider/m;

    invoke-direct {v2, p3}, Lcom/twitter/android/provider/m;-><init>(Landroid/database/Cursor;)V

    iget-object v1, v0, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/provider/m;)V

    iget-object v1, p0, Lcom/twitter/android/gd;->m:Lcom/twitter/android/cl;

    if-eqz v1, :cond_68

    iget-object v1, p0, Lcom/twitter/android/gd;->m:Lcom/twitter/android/cl;

    invoke-interface {v1, p1, v2}, Lcom/twitter/android/cl;->a(Landroid/view/View;Ljava/lang/Object;)V

    :cond_68
    iget-object v1, v0, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TweetView;->clearAnimation()V

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/gd;->getItemId(I)J

    move-result-wide v3

    iget-wide v5, p0, Lcom/twitter/android/gd;->n:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_b7

    iget-object v1, v0, Lcom/twitter/android/gc;->b:Landroid/view/ViewGroup;

    if-nez v1, :cond_89

    iget-object v1, v0, Lcom/twitter/android/gc;->a:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v0, Lcom/twitter/android/gc;->b:Landroid/view/ViewGroup;

    :cond_89
    iput-object v2, p0, Lcom/twitter/android/gd;->p:Lcom/twitter/android/provider/m;

    iget-object v1, v0, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    iget-object v0, v0, Lcom/twitter/android/gc;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/gd;->i:Landroid/view/animation/Animation;

    iget-object v3, p0, Lcom/twitter/android/gd;->b:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/gd;->p:Lcom/twitter/android/provider/m;

    invoke-static {v3, v4, v0, p0}, Lcom/twitter/android/widget/at;->a(Lcom/twitter/android/client/Session;Lcom/twitter/android/provider/m;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/twitter/android/gd;->t:Z

    if-eqz v0, :cond_b2

    iget-object v0, p0, Lcom/twitter/android/gd;->g:Lcom/twitter/android/gf;

    invoke-interface {v0}, Lcom/twitter/android/gf;->a()V

    iput-object v1, p0, Lcom/twitter/android/gd;->r:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_2d

    :cond_b2
    invoke-static {v1, v7}, Lcom/twitter/android/gd;->a(Landroid/view/View;Z)V

    goto/16 :goto_2d

    :cond_b7
    iget-wide v1, p0, Lcom/twitter/android/gd;->o:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_d9

    iget-object v1, v0, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    iget-object v0, v0, Lcom/twitter/android/gc;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/gd;->h:Landroid/view/animation/Animation;

    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/twitter/android/gd;->o:J

    iget-object v3, p0, Lcom/twitter/android/gd;->g:Lcom/twitter/android/gf;

    invoke-interface {v3}, Lcom/twitter/android/gf;->b()V

    invoke-static {v1, v8}, Lcom/twitter/android/gd;->a(Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/twitter/android/gd;->s:Landroid/view/ViewGroup;

    invoke-virtual {v2, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_2d

    :cond_d9
    iget-object v1, v0, Lcom/twitter/android/gc;->b:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2d

    iget-object v1, v0, Lcom/twitter/android/gc;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2d

    iget-object v1, v0, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    invoke-static {v1, v8}, Lcom/twitter/android/gd;->a(Landroid/view/View;Z)V

    iget-object v0, v0, Lcom/twitter/android/gc;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_2d
.end method

.method public final c()V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/gd;->g:Lcom/twitter/android/gf;

    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "You must set an "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/twitter/android/gf;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1f
    iget-wide v0, p0, Lcom/twitter/android/gd;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2d

    invoke-direct {p0}, Lcom/twitter/android/gd;->i()V

    invoke-virtual {p0}, Lcom/twitter/android/gd;->notifyDataSetChanged()V

    :cond_2d
    return-void
.end method

.method public final d()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/gd;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gc;

    iget-object v0, v0, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetView;->b()V

    goto :goto_6

    :cond_18
    return-void
.end method

.method public final e()Ljava/util/ArrayList;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/gd;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final f()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/gd;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public final g()Z
    .registers 2

    iget v0, p0, Lcom/twitter/android/gd;->l:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final getCount()I
    .registers 2

    invoke-virtual {p0}, Lcom/twitter/android/gd;->g()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/twitter/android/gd;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    goto :goto_d
.end method

.method public final getItemId(I)J
    .registers 5

    const-wide/16 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/gd;->g()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/twitter/android/gd;->h()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    move-wide v0, v1

    :goto_f
    return-wide v0

    :cond_10
    invoke-virtual {p0, p1}, Lcom/twitter/android/gd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_1f

    const/16 v1, 0x17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_f

    :cond_1f
    move-wide v0, v1

    goto :goto_f
.end method

.method public final getItemViewType(I)I
    .registers 5

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/gd;->g()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/twitter/android/gd;->h()Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_d
    move v0, v1

    :goto_e
    return v0

    :cond_f
    invoke-virtual {p0, p1}, Lcom/twitter/android/gd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/16 v2, 0x14

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_1f

    const/4 v0, 0x1

    goto :goto_e

    :cond_1f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    const v6, 0x7f0b0066

    const v9, 0x7f070052

    const v3, 0x7f030060

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/gd;->g()Z

    move-result v0

    if-eqz v0, :cond_86

    iget-object v0, p0, Lcom/twitter/android/gd;->mContext:Landroid/content/Context;

    if-nez p2, :cond_c1

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_1e
    iget-object v2, p0, Lcom/twitter/android/gd;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/gd;->j:Ljava/lang/String;

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_68

    new-array v0, v8, [Ljava/lang/Object;

    aput-object v2, v0, v7

    invoke-virtual {v5, v6, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const v3, 0x7f0b0067

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v2, v6, v7

    invoke-virtual {v5, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_4e
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v3, 0x21

    invoke-virtual {v4, v2, v7, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    invoke-virtual {v0, v7, v8}, Lcom/twitter/android/widget/CardRowView;->a(II)V

    :goto_67
    return-object v1

    :cond_68
    new-array v0, v8, [Ljava/lang/Object;

    aput-object v3, v0, v7

    invoke-virtual {v5, v6, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const v2, 0x7f0b0067

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-virtual {v5, v2, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_4e

    :cond_86
    invoke-virtual {p0}, Lcom/twitter/android/gd;->h()Z

    move-result v0

    if-eqz v0, :cond_ba

    iget-object v2, p0, Lcom/twitter/android/gd;->mContext:Landroid/content/Context;

    if-nez p2, :cond_bf

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_99
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0065

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/gd;->j:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    invoke-virtual {v0, v7, v8}, Lcom/twitter/android/widget/CardRowView;->a(II)V

    goto :goto_67

    :cond_ba
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_67

    :cond_bf
    move-object v1, p2

    goto :goto_99

    :cond_c1
    move-object v1, p2

    goto/16 :goto_1e
.end method

.method public final getViewTypeCount()I
    .registers 2

    const/4 v0, 0x3

    return v0
.end method

.method public final h()Z
    .registers 2

    iget v0, p0, Lcom/twitter/android/gd;->l:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isEnabled(I)Z
    .registers 3

    invoke-virtual {p0}, Lcom/twitter/android/gd;->g()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/twitter/android/gd;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_d
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/16 v1, 0x14

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_35

    const v1, 0x7f03005d

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ge;

    invoke-direct {v2}, Lcom/twitter/android/ge;-><init>()V

    const v0, 0x7f070079

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v2, Lcom/twitter/android/ge;->a:Landroid/widget/ProgressBar;

    const v0, 0x7f0700ce

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/ge;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    :goto_34
    return-object v0

    :cond_35
    const v1, 0x7f030061

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/gc;

    invoke-direct {v1, v0}, Lcom/twitter/android/gc;-><init>(Landroid/view/View;)V

    iget-object v2, v1, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/gd;->d:Lcom/twitter/android/widget/az;

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/widget/az;)V

    iget-object v2, v1, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    iget-boolean v3, p0, Lcom/twitter/android/gd;->e:Z

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/TweetView;->a(Z)V

    iget-object v2, v1, Lcom/twitter/android/gc;->c:Lcom/twitter/android/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/gd;->f:Lcom/twitter/android/widget/av;

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/widget/av;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/twitter/android/gd;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_34
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/gd;->r:Landroid/view/View;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/gd;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iput-object v2, p0, Lcom/twitter/android/gd;->r:Landroid/view/View;

    :cond_d
    iget-object v0, p0, Lcom/twitter/android/gd;->s:Landroid/view/ViewGroup;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/gd;->s:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iput-object v2, p0, Lcom/twitter/android/gd;->s:Landroid/view/ViewGroup;

    :cond_19
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 2

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 2

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/gd;->p:Lcom/twitter/android/provider/m;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/gd;->g:Lcom/twitter/android/gf;

    iget-object v1, p0, Lcom/twitter/android/gd;->p:Lcom/twitter/android/provider/m;

    invoke-interface {v0, p1, v1}, Lcom/twitter/android/gf;->a(Landroid/view/View;Lcom/twitter/android/provider/m;)V

    :cond_b
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 3

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/gd;->l:I

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
