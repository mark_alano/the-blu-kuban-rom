.class public final Lcom/twitter/android/gh;
.super Ljava/lang/Object;


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Lcom/twitter/android/gk;

.field private final c:Lcom/twitter/android/gj;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/android/gk;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/gh;->b:Lcom/twitter/android/gk;

    iput-object p1, p0, Lcom/twitter/android/gh;->a:Landroid/app/Activity;

    new-instance v0, Lcom/twitter/android/gj;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/gj;-><init>(Lcom/twitter/android/gh;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/gh;->c:Lcom/twitter/android/gj;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .registers 6

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/gh;->c:Lcom/twitter/android/gj;

    invoke-virtual {v0, v2}, Lcom/twitter/android/gj;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0, v2}, Lcom/twitter/android/gj;->removeMessages(I)V

    :cond_c
    invoke-virtual {v0, v2, p1}, Lcom/twitter/android/gj;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/gj;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
