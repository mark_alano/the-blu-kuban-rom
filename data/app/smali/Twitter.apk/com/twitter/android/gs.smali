.class public Lcom/twitter/android/gs;
.super Landroid/support/v4/widget/CursorAdapter;


# instance fields
.field a:J

.field b:I

.field private final c:Z

.field private final d:Lcom/twitter/android/client/b;

.field private final e:I

.field private final f:Lcom/twitter/android/widget/a;

.field private final g:Lcom/twitter/android/util/FriendshipCache;

.field private final h:I

.field private final i:I

.field private final j:Z

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/view/View$OnClickListener;

.field private n:Lcom/twitter/android/cl;

.field private final o:Lcom/twitter/android/gt;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V
    .registers 12

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-object p3, p0, Lcom/twitter/android/gs;->d:Lcom/twitter/android/client/b;

    iput-boolean p4, p0, Lcom/twitter/android/gs;->c:Z

    iput p5, p0, Lcom/twitter/android/gs;->e:I

    iput-object p6, p0, Lcom/twitter/android/gs;->f:Lcom/twitter/android/widget/a;

    iput-object p7, p0, Lcom/twitter/android/gs;->g:Lcom/twitter/android/util/FriendshipCache;

    iput p8, p0, Lcom/twitter/android/gs;->h:I

    iput p9, p0, Lcom/twitter/android/gs;->i:I

    if-lez p8, :cond_32

    const/4 v0, 0x1

    :goto_15
    iput-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    invoke-virtual {p3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/gs;->a:J

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/gs;->b:I

    new-instance v0, Lcom/twitter/android/gt;

    invoke-direct {v0, p0}, Lcom/twitter/android/gt;-><init>(Lcom/twitter/android/gs;)V

    iput-object v0, p0, Lcom/twitter/android/gs;->o:Lcom/twitter/android/gt;

    return-void

    :cond_32
    const/4 v0, 0x0

    goto :goto_15
.end method

.method static synthetic a(Lcom/twitter/android/gs;)Lcom/twitter/android/widget/a;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/gs;->f:Lcom/twitter/android/widget/a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/gs;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/gs;)Lcom/twitter/android/util/FriendshipCache;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/gs;->g:Lcom/twitter/android/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/gs;)I
    .registers 2

    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/gs;)Landroid/widget/Button;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/gs;->k:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/gs;->m:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public final a(Lcom/twitter/android/cl;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/gs;->n:Lcom/twitter/android/cl;

    return-void
.end method

.method protected final a(Lcom/twitter/android/widget/BaseUserView;Landroid/database/Cursor;J)V
    .registers 10

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/widget/BaseUserView;->a(J)V

    if-eqz v0, :cond_51

    iget-object v3, p0, Lcom/twitter/android/gs;->d:Lcom/twitter/android/client/b;

    const/4 v4, 0x2

    invoke-virtual {v3, v4, p3, p4, v0}, Lcom/twitter/android/client/b;->a(IJLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/BaseUserView;->a(Landroid/graphics/Bitmap;)V

    :goto_16
    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Lcom/twitter/android/widget/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_56

    move v0, v1

    :goto_2b
    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/BaseUserView;->a(Z)V

    const/4 v0, 0x7

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_58

    :goto_35
    invoke-virtual {p1, v1}, Lcom/twitter/android/widget/BaseUserView;->b(Z)V

    const/16 v0, 0x9

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/z;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/BaseUserView;->a(Lcom/twitter/android/api/PromotedContent;)V

    iget-object v1, p0, Lcom/twitter/android/gs;->n:Lcom/twitter/android/cl;

    if-eqz v1, :cond_50

    iget-object v1, p0, Lcom/twitter/android/gs;->n:Lcom/twitter/android/cl;

    invoke-interface {v1, p1, v0}, Lcom/twitter/android/cl;->a(Landroid/view/View;Ljava/lang/Object;)V

    :cond_50
    return-void

    :cond_51
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/BaseUserView;->a(Landroid/graphics/Bitmap;)V

    goto :goto_16

    :cond_56
    move v0, v2

    goto :goto_2b

    :cond_58
    move v1, v2

    goto :goto_35
.end method

.method public areAllItemsEnabled()Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    if-nez v0, :cond_c

    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 9

    const/4 v0, 0x0

    check-cast p1, Lcom/twitter/android/widget/UserView;

    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p0, p1, p3, v1, v2}, Lcom/twitter/android/gs;->a(Lcom/twitter/android/widget/BaseUserView;Landroid/database/Cursor;J)V

    iget v3, p0, Lcom/twitter/android/gs;->e:I

    if-lez v3, :cond_19

    iget-wide v3, p0, Lcom/twitter/android/gs;->a:J

    cmp-long v3, v3, v1

    if-nez v3, :cond_1a

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/UserView;->b(I)V

    :cond_19
    :goto_19
    return-void

    :cond_1a
    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/UserView;->b(I)V

    iget-object v3, p0, Lcom/twitter/android/gs;->g:Lcom/twitter/android/util/FriendshipCache;

    if-eqz v3, :cond_19

    invoke-virtual {v3, v1, v2}, Lcom/twitter/android/util/FriendshipCache;->a(J)Z

    move-result v4

    if-eqz v4, :cond_2f

    invoke-virtual {v3, v1, v2}, Lcom/twitter/android/util/FriendshipCache;->e(J)Z

    move-result v0

    :cond_2b
    :goto_2b
    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/UserView;->setChecked(Z)V

    goto :goto_19

    :cond_2f
    const/16 v1, 0x8

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2b

    const/4 v0, 0x1

    goto :goto_2b
.end method

.method public getCount()I
    .registers 3

    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/gs;->j:Z

    if-eqz v1, :cond_c

    if-lez v0, :cond_d

    add-int/lit8 v0, v0, 0x1

    :cond_c
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3

    iget-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    if-eqz v0, :cond_f

    if-nez p1, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7

    :cond_f
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7
.end method

.method public getItemId(I)J
    .registers 5

    const-wide/16 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    if-eqz v0, :cond_c

    if-nez p1, :cond_a

    move-wide v0, v1

    :goto_9
    return-wide v0

    :cond_a
    add-int/lit8 p1, p1, -0x1

    :cond_c
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_1a

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_9

    :cond_1a
    move-wide v0, v1

    goto :goto_9
.end method

.method public getItemViewType(I)I
    .registers 3

    iget-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    if-eqz v0, :cond_8

    if-nez p1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    if-eqz v0, :cond_7f

    if-nez p1, :cond_78

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    if-nez p2, :cond_4b

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030020

    invoke-virtual {v0, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f070043

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v2, p0, Lcom/twitter/android/gs;->i:I

    if-lez v2, :cond_73

    iget v2, p0, Lcom/twitter/android/gs;->i:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_2a
    const v0, 0x7f070069

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/gs;->k:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/gs;->m:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_40

    iget-object v0, p0, Lcom/twitter/android/gs;->k:Landroid/widget/Button;

    iget-object v2, p0, Lcom/twitter/android/gs;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_40
    const v0, 0x7f070068

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/gs;->l:Landroid/widget/TextView;

    :cond_4b
    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/gs;->l:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v3, p0, Lcom/twitter/android/gs;->h:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, p2

    :goto_68
    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    invoke-virtual {p0}, Lcom/twitter/android/gs;->getCount()I

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/twitter/android/widget/CardRowView;->a(II)V

    return-object v1

    :cond_73
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2a

    :cond_78
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_68

    :cond_7f
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_68
.end method

.method public getViewTypeCount()I
    .registers 2

    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .registers 3

    iget-boolean v0, p0, Lcom/twitter/android/gs;->j:Z

    if-eqz v0, :cond_12

    if-eqz p1, :cond_10

    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/support/v4/widget/CursorAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f

    :cond_12
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_f
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030074

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/UserView;

    iget-boolean v1, p0, Lcom/twitter/android/gs;->c:Z

    if-eqz v1, :cond_17

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UserView;->c(Z)V

    :cond_16
    :goto_16
    return-object v0

    :cond_17
    iget v1, p0, Lcom/twitter/android/gs;->e:I

    if-lez v1, :cond_16

    iget v1, p0, Lcom/twitter/android/gs;->e:I

    iget-object v2, p0, Lcom/twitter/android/gs;->o:Lcom/twitter/android/gt;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/UserView;->a(ILcom/twitter/android/widget/a;)V

    const v1, 0x7f020049

    iget v2, p0, Lcom/twitter/android/gs;->e:I

    if-ne v1, v2, :cond_16

    const v1, 0x7f02004a

    iget v2, p0, Lcom/twitter/android/gs;->b:I

    iget v4, p0, Lcom/twitter/android/gs;->b:I

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/UserView;->a(IIIII)V

    goto :goto_16
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/gs;->d:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/gs;->a:J

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
