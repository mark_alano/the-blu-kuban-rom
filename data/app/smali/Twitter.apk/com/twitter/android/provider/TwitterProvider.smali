.class public Lcom/twitter/android/provider/TwitterProvider;
.super Landroid/content/ContentProvider;


# static fields
.field private static final a:Z

.field private static final b:Landroid/content/UriMatcher;

.field private static final c:Landroid/net/Uri;

.field private static final d:Landroid/net/Uri;

.field private static final e:Landroid/net/Uri;

.field private static final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    const/16 v8, 0x259

    const/16 v7, 0x258

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    sput-boolean v4, Lcom/twitter/android/provider/TwitterProvider;->a:Z

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "users"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "users/id/#"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups"

    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/#"

    const/16 v3, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/following/#"

    const/16 v3, 0x48

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/followers/#"

    const/16 v3, 0x49

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/blocked/#"

    const/16 v3, 0x4a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/members/#"

    const/16 v3, 0x4b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/subscribers/#"

    const/16 v3, 0x4c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/people/#"

    const/16 v3, 0x4d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/search"

    const/16 v3, 0x4e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/slug/#"

    const/16 v3, 0x4f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/contacts/#"

    const/16 v3, 0x50

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/activity_sources/#"

    const/16 v3, 0x51

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/favorited/#"

    const/16 v3, 0x52

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/retweeted/#"

    const/16 v3, 0x53

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/activity_targets/#"

    const/16 v3, 0x54

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/dm_contacts/#"

    const/16 v3, 0x55

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/notifications/#"

    const/16 v3, 0x56

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/event"

    const/16 v3, 0x57

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_groups_view/incoming_friendships/#"

    const/16 v3, 0x58

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "statuses"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "statuses/id/#"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups/#"

    const/16 v3, 0x6f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view"

    const/16 v3, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/#"

    const/16 v3, 0x79

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/id/#"

    const/16 v3, 0x7a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/timeline/#"

    const/16 v3, 0x7b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/favorites/#"

    const/16 v3, 0x7c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/home/#"

    const/16 v3, 0x7d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/replies/#"

    const/16 v3, 0x7e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/mentions/#"

    const/16 v3, 0x80

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/retweetedbyme/#"

    const/16 v3, 0x81

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/retweetedtome/#"

    const/16 v3, 0x82

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/retweetsofme/#"

    const/16 v3, 0x83

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/list/#"

    const/16 v3, 0x84

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/drafts/#"

    const/16 v3, 0x85

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/mentions_and_rts/#"

    const/16 v3, 0x86

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/conversation/#"

    const/16 v3, 0x88

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/search_results/#"

    const/16 v3, 0x89

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/discover_social_proof/#"

    const/16 v3, 0x8c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/activity_targets/#"

    const/16 v3, 0x8a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/activity_target_objects/#"

    const/16 v3, 0x8b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_retweets_view/rt_timeline/#"

    const/16 v3, 0x87

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/event/#"

    const/16 v3, 0x8d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/media/#"

    const/16 v3, 0x8e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_view/event_media/#"

    const/16 v3, 0x8f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_retweets_view/rt_media/#"

    const/16 v3, 0x90

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "status_groups_retweets_view/rt_event_media/#"

    const/16 v3, 0x91

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "messages"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "messages/id/#"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "messages_sent_view/#"

    const/16 v3, 0xd5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "messages_received_view/#"

    const/16 v3, 0xd6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "messages_threaded/#"

    const/16 v3, 0xd7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "messages_conversation"

    const/16 v3, 0xd8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "lists"

    const/16 v3, 0xdc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "lists/id/#"

    const/16 v3, 0xdd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "lists_view"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "lists_view/#"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "lists_view/id/#"

    const/16 v3, 0x12e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "search_queries"

    const/16 v3, 0x208

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "search_queries/#"

    const/16 v3, 0x209

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "search_suggest_query"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "search_suggest_query/*"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "search_suggest_shortcut"

    invoke-virtual {v0, v1, v2, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "search_suggest_shortcut/*"

    invoke-virtual {v0, v1, v2, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "activities"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "stories"

    const/16 v3, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "slug_users_view"

    const/16 v3, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_recommendations_view"

    const/16 v3, 0x385

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "user_recommendations_view/similar_to/#"

    const/16 v3, 0x386

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    const-string v1, "com.twitter.android.provider.TwitterProvider"

    const-string v2, "notifications"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v0, "android.resource://com.twitter.android/drawable/ic_search_default"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/TwitterProvider;->c:Landroid/net/Uri;

    const-string v0, "android.resource://com.twitter.android/drawable/ic_search_user"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/TwitterProvider;->d:Landroid/net/Uri;

    const-string v0, "android.resource://com.twitter.android/drawable/ic_search_saved"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/provider/TwitterProvider;->e:Landroid/net/Uri;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "suggest_icon_1"

    aput-object v1, v0, v5

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, "suggest_intent_query"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/provider/TwitterProvider;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;J)J
    .registers 12

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v1, "status_groups_view"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "in_r_status_id"

    aput-object v0, v2, v6

    const-string v3, "g_status_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_32

    :try_start_20
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2f

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2a
    .catchall {:try_start_20 .. :try_end_2a} :catchall_35

    move-result-wide v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_2e
    return-wide v0

    :cond_2f
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_32
    const-wide/16 v0, 0x0

    goto :goto_2e

    :catchall_35
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/provider/be;ZLjava/util/HashSet;)J
    .registers 22

    invoke-virtual/range {p9 .. p9}, Ljava/util/HashSet;->size()I

    move-result v2

    const/16 v3, 0x19

    if-ge v2, v3, :cond_dd

    const/4 v3, 0x1

    const-string v4, "tokens_user_view"

    sget-object v5, Lcom/twitter/android/provider/bh;->a:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    move-object v2, p0

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v10, p6

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_dd

    move-wide v4, p2

    :goto_23
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8d

    invoke-virtual/range {p9 .. p9}, Ljava/util/HashSet;->size()I

    move-result v2

    const/16 v3, 0x19

    if-ge v2, v3, :cond_8d

    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v2, Lcom/twitter/android/provider/be;

    invoke-direct {v2, v7}, Lcom/twitter/android/provider/be;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_db

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v8

    const-wide/16 v2, 0x1

    add-long/2addr v2, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    sget-object v4, Lcom/twitter/android/provider/TwitterProvider;->d:Landroid/net/Uri;

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    const/4 v4, 0x0

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v8, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    const/4 v4, 0x2

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v5, v4, :cond_87

    const-string v4, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    :goto_73
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "@"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    :goto_85
    move-wide v4, v2

    goto :goto_23

    :cond_87
    const-string v4, "com.twitter.android.action.USER_SHOW_SEARCH_SUGGESTION"

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_73

    :cond_8d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_90
    if-eqz p8, :cond_d9

    move-object/from16 v0, p9

    move-object/from16 v1, p7

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d9

    move-object/from16 v0, p9

    move-object/from16 v1, p7

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v6

    const-wide/16 v2, 0x1

    add-long/2addr v2, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    sget-object v4, Lcom/twitter/android/provider/TwitterProvider;->d:Landroid/net/Uri;

    invoke-virtual {v6, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "@"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p7

    iget-object v5, v0, Lcom/twitter/android/provider/be;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/twitter/android/provider/be;->a:Ljava/lang/String;

    invoke-virtual {v6, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    const-string v4, "com.twitter.android.action.USER_SHOW"

    invoke-virtual {v6, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    :goto_d8
    return-wide v2

    :cond_d9
    move-wide v2, v4

    goto :goto_d8

    :cond_db
    move-wide v2, v4

    goto :goto_85

    :cond_dd
    move-wide v4, p2

    goto :goto_90
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;I)J
    .registers 20

    const/4 v2, 0x1

    const-string v3, "tokens_topic_view"

    sget-object v4, Lcom/twitter/android/provider/bg;->a:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    move-object v1, p0

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v9, p6

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v1, 0x0

    if-eqz v6, :cond_62

    move-wide v4, p2

    :goto_1a
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5c

    const/4 v2, 0x2

    if-ge v1, v2, :cond_5c

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v2, Lcom/twitter/android/provider/be;

    invoke-direct {v2, v7}, Lcom/twitter/android/provider/be;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_60

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v8

    const-wide/16 v2, 0x1

    add-long/2addr v2, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    sget-object v4, Lcom/twitter/android/provider/TwitterProvider;->c:Landroid/net/Uri;

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v8, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v8, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    const-string v4, "com.twitter.android.action.SEARCH_TYPEAHEAD_TOPIC"

    invoke-virtual {v8, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    add-int/lit8 v1, v1, 0x1

    :goto_5a
    move-wide v4, v2

    goto :goto_1a

    :cond_5c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_5f
    return-wide v4

    :cond_60
    move-wide v2, v4

    goto :goto_5a

    :cond_62
    move-wide v4, p2

    goto :goto_5f
.end method

.method private a(Ljava/lang/String;)Landroid/database/Cursor;
    .registers 20

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    new-instance v11, Lcom/twitter/android/provider/be;

    invoke-direct {v11, v12}, Lcom/twitter/android/provider/be;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/twitter/android/provider/bi;->b(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {v13, v0, v1}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/provider/ae;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/twitter/android/provider/TwitterProvider;->f:[Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    const-wide/16 v2, 0x1

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26c

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    const/4 v4, 0x1

    const/4 v10, 0x0

    sget-object v5, Lcom/twitter/android/util/n;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v12}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_162

    const/4 v5, 0x0

    invoke-virtual {v12, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x40

    if-ne v5, v6, :cond_134

    const-wide/16 v2, 0x1

    const-string v4, "text LIKE ? AND username NOT NULL"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x25

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "graph_weight DESC, LOWER(username) ASC"

    new-instance v7, Lcom/twitter/android/provider/be;

    const/4 v8, 0x1

    invoke-virtual {v12, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/twitter/android/provider/be;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-static/range {v0 .. v9}, Lcom/twitter/android/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/provider/be;ZLjava/util/HashSet;)J

    move-result-wide v2

    const/4 v4, 0x0

    move v5, v4

    move v4, v10

    :goto_7e
    new-instance v8, Lcom/twitter/android/provider/be;

    invoke-direct {v8, v12}, Lcom/twitter/android/provider/be;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/twitter/android/ej;->search_for:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v12, v10, v15

    invoke-virtual {v6, v7, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v14, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_bd

    invoke-virtual {v1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v15

    invoke-virtual {v14, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-wide/16 v6, 0x1

    add-long/2addr v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    sget-object v2, Lcom/twitter/android/provider/TwitterProvider;->c:Landroid/net/Uri;

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v15, v10}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    iget-object v2, v8, Lcom/twitter/android/provider/be;->a:Ljava/lang/String;

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    const-string v2, "com.twitter.android.action.SEARCH"

    invoke-virtual {v15, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-wide v2, v6

    :cond_bd
    if-eqz v5, :cond_26c

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x25

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    if-eqz v4, :cond_16f

    const-string v4, "topic LIKE ?"

    const-string v6, "weight DESC, LOWER(topic) ASC"

    const/4 v8, 0x2

    move-object v7, v14

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;I)J

    move-result-wide v2

    const-string v4, "name LIKE ?"

    const-string v6, "graph_weight DESC, LOWER(name) ASC"

    const/4 v8, 0x0

    move-object v7, v11

    invoke-static/range {v0 .. v9}, Lcom/twitter/android/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/provider/be;ZLjava/util/HashSet;)J

    move-result-wide v2

    move-wide v10, v2

    :goto_ef
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v3, "search_queries"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v3, "name LIKE "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    const-string v3, " AND query!=\'\'"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v3, " AND query NOT LIKE \'place:%\'"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    sget-object v4, Lcom/twitter/android/provider/bf;->a:[Ljava/lang/String;

    const-string v5, "type IN (6,0,4)"

    const/4 v6, 0x0

    const-string v7, "name"

    const/4 v8, 0x0

    const-string v9, "type ASC, query_id DESC, time ASC"

    move-object v3, v0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_1be

    :goto_133
    return-object v1

    :cond_134
    const/16 v6, 0x23

    if-ne v5, v6, :cond_15e

    const-wide/16 v2, 0x1

    const-string v4, "text LIKE ? AND topic NOT NULL"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x25

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "weight DESC, LOWER(topic) ASC"

    const/4 v8, 0x2

    move-object v7, v14

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;I)J

    move-result-wide v2

    const/4 v4, 0x0

    :cond_15e
    move v5, v4

    move v4, v10

    goto/16 :goto_7e

    :cond_162
    sget-object v4, Lcom/twitter/android/util/n;->j:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v12}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    move v5, v4

    goto/16 :goto_7e

    :cond_16f
    const-string v4, "text LIKE ? AND topic NOT NULL"

    const-string v6, "weight DESC, LOWER(topic) ASC"

    const/4 v8, 0x2

    move-object v7, v14

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/util/HashSet;I)J

    move-result-wide v2

    const-string v4, "text LIKE ? AND username NOT NULL"

    const-string v6, "graph_weight DESC, LOWER(username) ASC"

    const/4 v8, 0x1

    move-object v7, v11

    invoke-static/range {v0 .. v9}, Lcom/twitter/android/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/MatrixCursor;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/provider/be;ZLjava/util/HashSet;)J

    move-result-wide v2

    move-wide v10, v2

    goto/16 :goto_ef

    :pswitch_186
    :try_start_186
    sget-object v2, Lcom/twitter/android/provider/TwitterProvider;->c:Landroid/net/Uri;

    const-string v0, "com.twitter.android.action.SEARCH_USERS"

    move-object v4, v2

    move-object v5, v3

    :goto_18c
    new-instance v2, Lcom/twitter/android/provider/be;

    invoke-direct {v2, v7}, Lcom/twitter/android/provider/be;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v9

    const-wide/16 v2, 0x1

    add-long/2addr v2, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v9, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v9, v7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    const/4 v4, 0x1

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v9, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v9, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-wide v10, v2

    :cond_1be
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_263

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v14, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1be

    sget-object v2, Lcom/twitter/android/provider/w;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v2, "type"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    packed-switch v4, :pswitch_data_270

    :pswitch_1e8
    sget-object v2, Lcom/twitter/android/provider/TwitterProvider;->c:Landroid/net/Uri;

    const/4 v5, 0x3

    invoke-interface {v6, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_25b

    const/4 v5, 0x4

    invoke-interface {v6, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_25b

    const-string v0, "com.twitter.android.action.SEARCH_NEARBY"

    const/4 v3, 0x3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    const/4 v3, 0x4

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v15

    const/4 v3, 0x5

    invoke-interface {v6, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_23e

    sget v3, Lcom/twitter/android/ej;->near_coords:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v17

    aput-object v17, v9, v12

    const/4 v12, 0x1

    invoke-static/range {v15 .. v16}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v17

    aput-object v17, v9, v12

    invoke-virtual {v13, v3, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_222
    const-string v9, "latitude"

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v9, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v4, "longitude"

    invoke-static/range {v15 .. v16}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-object v4, v2

    move-object v5, v3

    goto/16 :goto_18c

    :pswitch_238
    sget-object v2, Lcom/twitter/android/provider/TwitterProvider;->e:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v3

    goto/16 :goto_18c

    :cond_23e
    const/4 v3, 0x5

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget v3, Lcom/twitter/android/ej;->near_location:I

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/16 v17, 0x0

    aput-object v9, v12, v17

    invoke-virtual {v13, v3, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v12, "location"

    invoke-virtual {v8, v12, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    :try_end_255
    .catchall {:try_start_186 .. :try_end_255} :catchall_256

    goto :goto_222

    :catchall_256
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_25b
    if-nez v4, :cond_268

    :try_start_25d
    const-string v0, "com.twitter.android.action.SEARCH_RECENT"
    :try_end_25f
    .catchall {:try_start_25d .. :try_end_25f} :catchall_256

    move-object v4, v2

    move-object v5, v3

    goto/16 :goto_18c

    :cond_263
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_133

    :cond_268
    move-object v4, v2

    move-object v5, v3

    goto/16 :goto_18c

    :cond_26c
    move-wide v10, v2

    goto/16 :goto_ef

    nop

    :pswitch_data_270
    .packed-switch 0x4
        :pswitch_186
        :pswitch_1e8
        :pswitch_238
    .end packed-switch
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 7

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Delete not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5

    sget-object v0, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_5e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_1e
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.users"

    :goto_20
    return-object v0

    :sswitch_21
    const-string v0, "vnd.android.cursor.item/vnd.twitter.android.users"

    goto :goto_20

    :sswitch_24
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.users.groups"

    goto :goto_20

    :sswitch_27
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses"

    goto :goto_20

    :sswitch_2a
    const-string v0, "vnd.android.cursor.item/vnd.twitter.android.statuses"

    goto :goto_20

    :sswitch_2d
    const-string v0, "vnd.android.cursor.item/vnd.twitter.android.statuses"

    goto :goto_20

    :sswitch_30
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses"

    goto :goto_20

    :sswitch_33
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses.groups"

    goto :goto_20

    :sswitch_36
    const-string v0, "vnd.android.cursor.item/vnd.twitter.android.statuses.groups"

    goto :goto_20

    :sswitch_39
    const-string v0, "vnd.android.cursor.item/vnd.twitter.android.messages"

    goto :goto_20

    :sswitch_3c
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.messages"

    goto :goto_20

    :sswitch_3f
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.lists"

    goto :goto_20

    :sswitch_42
    const-string v0, "vnd.android.cursor.item/vnd.twitter.android.lists"

    goto :goto_20

    :sswitch_45
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.search.queries"

    goto :goto_20

    :sswitch_48
    const-string v0, "vnd.android.cursor.item/vnd.twitter.android.search.queries"

    goto :goto_20

    :sswitch_4b
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    goto :goto_20

    :sswitch_4e
    const-string v0, "vnd.android.cursor.item/vnd.android.search.suggest"

    goto :goto_20

    :sswitch_51
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.activities"

    goto :goto_20

    :sswitch_54
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.stories"

    goto :goto_20

    :sswitch_57
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.search.queries"

    goto :goto_20

    :sswitch_5a
    const-string v0, "vnd.android.cursor.dir/vnd.twitter.android.notifications"

    goto :goto_20

    nop

    :sswitch_data_5e
    .sparse-switch
        0x1 -> :sswitch_1e
        0x2 -> :sswitch_21
        0x14 -> :sswitch_27
        0x15 -> :sswitch_2a
        0x3c -> :sswitch_24
        0x46 -> :sswitch_24
        0x48 -> :sswitch_24
        0x49 -> :sswitch_24
        0x4a -> :sswitch_24
        0x4b -> :sswitch_24
        0x4c -> :sswitch_24
        0x4d -> :sswitch_24
        0x4e -> :sswitch_24
        0x51 -> :sswitch_24
        0x52 -> :sswitch_24
        0x53 -> :sswitch_24
        0x54 -> :sswitch_24
        0x55 -> :sswitch_24
        0x56 -> :sswitch_24
        0x57 -> :sswitch_24
        0x58 -> :sswitch_24
        0x6e -> :sswitch_33
        0x6f -> :sswitch_36
        0x78 -> :sswitch_30
        0x79 -> :sswitch_2d
        0x7a -> :sswitch_2d
        0x7b -> :sswitch_30
        0x7c -> :sswitch_30
        0x7d -> :sswitch_30
        0x7e -> :sswitch_30
        0x80 -> :sswitch_30
        0x81 -> :sswitch_30
        0x82 -> :sswitch_30
        0x83 -> :sswitch_30
        0x84 -> :sswitch_30
        0x85 -> :sswitch_30
        0x86 -> :sswitch_30
        0x87 -> :sswitch_30
        0x88 -> :sswitch_30
        0x89 -> :sswitch_30
        0x8a -> :sswitch_30
        0x8b -> :sswitch_30
        0x8c -> :sswitch_30
        0x8d -> :sswitch_30
        0x8e -> :sswitch_30
        0x8f -> :sswitch_30
        0xc8 -> :sswitch_39
        0xc9 -> :sswitch_3c
        0xd5 -> :sswitch_3c
        0xd6 -> :sswitch_3c
        0xd7 -> :sswitch_3c
        0xdc -> :sswitch_3f
        0xdd -> :sswitch_42
        0x12c -> :sswitch_3f
        0x12d -> :sswitch_42
        0x12e -> :sswitch_42
        0x208 -> :sswitch_45
        0x209 -> :sswitch_48
        0x258 -> :sswitch_4b
        0x259 -> :sswitch_4e
        0x2bc -> :sswitch_51
        0x320 -> :sswitch_54
        0x384 -> :sswitch_57
        0x3e8 -> :sswitch_5a
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 6

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Insert not supported "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .registers 2

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 22

    const/4 v3, 0x0

    const-string v1, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v1, "ownerId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_78

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v4, 0x0

    invoke-static {v1, v4, v5}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/provider/ae;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    :goto_25
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    sget-boolean v4, Lcom/twitter/android/provider/TwitterProvider;->a:Z

    if-eqz v4, :cond_56

    const-string v4, "TwitterProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "QUERY uri: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_56
    sget-object v4, Lcom/twitter/android/provider/TwitterProvider;->b:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    sparse-switch v4, :sswitch_data_c00

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_78
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/provider/ae;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    goto :goto_25

    :sswitch_89
    const-string v5, "users"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const/4 v5, 0x2

    if-ne v4, v5, :cond_a7

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "user_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_a7
    const-string v4, "profile_created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    :goto_ac
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_bea

    move-object/from16 v12, p5

    :goto_b4
    sget-boolean v3, Lcom/twitter/android/provider/TwitterProvider;->a:Z

    if-eqz v3, :cond_db

    const-string v13, "TwitterProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v3, "QUERY: "

    invoke-direct {v14, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v13, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_db
    const/4 v7, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object v6, v11

    move-object v8, v12

    move-object v9, v10

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    sget-boolean v2, Lcom/twitter/android/provider/TwitterProvider;->a:Z

    if-eqz v2, :cond_105

    const-string v2, "TwitterProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "QUERY results: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_105
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v1, v2, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :goto_112
    return-object v1

    :sswitch_113
    const-string v4, "user_groups"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type DESC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto :goto_ac

    :sswitch_126
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_148
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_178
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_1a8
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type=2"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_1c1
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type=4"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_1da
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type=5"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_1f3
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type=3"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_20c
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type IN (1,0,15"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",-1)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_244
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=6"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_276
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=7"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_2a8
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=8"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_2da
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=13"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_30c
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND (friendship&8"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " OR type=1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_356
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=16"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_386
    const-string v4, "user_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type=17"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_39f
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=11"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_3d1
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=12"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_403
    const-string v3, "user_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=18"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "_id ASC"

    const-string v3, "user_id"

    if-nez v12, :cond_bfa

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_435
    const-string v5, "statuses"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const/16 v5, 0x15

    if-ne v4, v5, :cond_454

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "status_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_454
    const-string v4, "created DESC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_463
    const-string v5, "status_groups"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const/16 v5, 0x6f

    if-ne v4, v5, :cond_482

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_482
    const-string v4, "type DESC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_491
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_4a5
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_4cf
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ref_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v12, "1"

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_4f3
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_523
    const-string v4, "status_groups_retweets_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_553
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=2"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_583
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "newer"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5c0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " AND updated_at>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_5c0
    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_5cf
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=3"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_5ff
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=5"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_627
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=6"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_657
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=7"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_687
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type=8"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_6a0
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "type=9"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_6b9
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=11"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_6e9
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND (type=5"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR (type=8"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND retweet_count > 0))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    if-nez v12, :cond_bf5

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_725
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v13}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    :goto_736
    invoke-static {v2, v3, v4}, Lcom/twitter/android/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;J)J

    move-result-wide v10

    const-wide/16 v3, 0x0

    cmp-long v3, v10, v3

    if-lez v3, :cond_772

    const-string v3, "status_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g_status_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v15, 0x0

    const-string v6, "g_status_id"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "1"

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v14, v15, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    move-wide v3, v10

    goto :goto_736

    :cond_772
    const-string v3, "status_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g_status_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "1"

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    const-string v3, "status_groups_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "in_r_status_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND (type!=11)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    if-nez v12, :cond_bf2

    const-wide/16 v3, 0x190

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    :goto_7c5
    const-string v6, "g_status_id"

    const/4 v7, 0x0

    const-string v8, "updated_at ASC, _id DESC"

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Landroid/database/Cursor;

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    new-instance v1, Landroid/database/MergeCursor;

    invoke-direct {v1, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0}, Landroid/database/MergeCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_112

    :sswitch_7f4
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND (type=13"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "tag DESC, updated_at DESC, created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_822
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=12"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_84a
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=14"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_872
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND (type=15"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "tag DESC, updated_at DESC, created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_8a0
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND (type=16"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "tag DESC, updated_at DESC, created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_8ce
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND (type=17"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_8fc
    const-string v4, "status_groups_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND (type=18"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_92a
    const-string v4, "status_groups_retweets_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=17"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_952
    const-string v4, "status_groups_retweets_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "owner_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND type=18"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "updated_at DESC, _id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_97a
    const-string v5, "lists"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const/16 v5, 0xdd

    if-ne v4, v5, :cond_999

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "list_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_999
    const-string v4, "full_name ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_9a0
    const-string v4, "lists_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "full_name ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_9ac
    const-string v4, "lists_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "full_name ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_9ce
    const-string v4, "lists_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "list_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "full_name ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_9f0
    const-string v4, "messages"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    if-nez v12, :cond_bed

    const-string v12, "1"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_a15
    const-string v4, "messages"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_a21
    const-string v4, "messages_sent_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "type=0 AND sender_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_a43
    const-string v4, "messages_received_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "type=1 AND recipient_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_a65
    const-string v4, "messages_threaded"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sender_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " OR recipient_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "created DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_a91
    const-string v4, "messages_conversation"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "created ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_a9d
    const-string v4, "search_queries"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "like"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_acd

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_acd

    const-string v5, "name LIKE "

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    :cond_acd
    const-string v4, "name ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_ad4
    const-string v4, "search_queries"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "name ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_af6
    const/4 v1, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_b06

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    :cond_b06
    if-nez v1, :cond_b0a

    const-string v1, ""

    :cond_b0a
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/twitter/android/provider/TwitterProvider;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_112

    :sswitch_b12
    const/4 v1, 0x0

    goto/16 :goto_112

    :sswitch_b15
    const-string v4, "activities"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "max_position DESC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_b21
    const-string v4, "stories"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "_id ASC "

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_b2d
    const-string v4, "slug_users_view"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "query ASC "

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :sswitch_b39
    const-string v3, "user_recommendations_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v3, "type=9"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    new-instance v10, Lcom/twitter/android/provider/k;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b78

    const-string v8, "_id ASC"

    :goto_b4f
    const/4 v9, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b7b

    const/16 v1, 0x190

    :goto_b62
    invoke-direct {v10, v2, v1}, Lcom/twitter/android/provider/k;-><init>(Landroid/database/Cursor;I)V

    invoke-virtual {v10}, Lcom/twitter/android/provider/k;->a()V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v10, v1, v0}, Lcom/twitter/android/provider/k;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object v1, v10

    goto/16 :goto_112

    :cond_b78
    move-object/from16 v8, p5

    goto :goto_b4f

    :cond_b7b
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_b62

    :sswitch_b80
    const-string v3, "user_recommendations_view"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "owner_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND type=10"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    new-instance v10, Lcom/twitter/android/provider/k;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_bd6

    const-string v8, "_id ASC"

    :goto_bad
    const/4 v9, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_bd9

    const/16 v1, 0x190

    :goto_bc0
    invoke-direct {v10, v2, v1}, Lcom/twitter/android/provider/k;-><init>(Landroid/database/Cursor;I)V

    invoke-virtual {v10}, Lcom/twitter/android/provider/k;->a()V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v10, v1, v0}, Lcom/twitter/android/provider/k;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object v1, v10

    goto/16 :goto_112

    :cond_bd6
    move-object/from16 v8, p5

    goto :goto_bad

    :cond_bd9
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_bc0

    :sswitch_bde
    const-string v4, "notifications"

    invoke-virtual {v1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "notif_id ASC"

    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :cond_bea
    move-object v12, v3

    goto/16 :goto_b4

    :cond_bed
    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :cond_bf2
    move-object v9, v12

    goto/16 :goto_7c5

    :cond_bf5
    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    :cond_bfa
    move-object v10, v12

    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_ac

    nop

    :sswitch_data_c00
    .sparse-switch
        0x1 -> :sswitch_89
        0x2 -> :sswitch_89
        0x14 -> :sswitch_435
        0x15 -> :sswitch_435
        0x3c -> :sswitch_113
        0x46 -> :sswitch_126
        0x48 -> :sswitch_148
        0x49 -> :sswitch_178
        0x4a -> :sswitch_1a8
        0x4b -> :sswitch_1c1
        0x4c -> :sswitch_1da
        0x4d -> :sswitch_20c
        0x4e -> :sswitch_1f3
        0x4f -> :sswitch_244
        0x50 -> :sswitch_276
        0x51 -> :sswitch_2a8
        0x52 -> :sswitch_39f
        0x53 -> :sswitch_3d1
        0x54 -> :sswitch_2da
        0x55 -> :sswitch_30c
        0x56 -> :sswitch_356
        0x57 -> :sswitch_386
        0x58 -> :sswitch_403
        0x6e -> :sswitch_463
        0x6f -> :sswitch_463
        0x78 -> :sswitch_491
        0x79 -> :sswitch_4a5
        0x7a -> :sswitch_4cf
        0x7b -> :sswitch_4f3
        0x7c -> :sswitch_553
        0x7d -> :sswitch_583
        0x7e -> :sswitch_5cf
        0x80 -> :sswitch_5ff
        0x81 -> :sswitch_627
        0x82 -> :sswitch_657
        0x83 -> :sswitch_687
        0x84 -> :sswitch_6a0
        0x85 -> :sswitch_6b9
        0x86 -> :sswitch_6e9
        0x87 -> :sswitch_523
        0x88 -> :sswitch_725
        0x89 -> :sswitch_7f4
        0x8a -> :sswitch_822
        0x8b -> :sswitch_84a
        0x8c -> :sswitch_872
        0x8d -> :sswitch_8a0
        0x8e -> :sswitch_8ce
        0x8f -> :sswitch_8fc
        0x90 -> :sswitch_92a
        0x91 -> :sswitch_952
        0xc8 -> :sswitch_9f0
        0xc9 -> :sswitch_a15
        0xd5 -> :sswitch_a21
        0xd6 -> :sswitch_a43
        0xd7 -> :sswitch_a65
        0xd8 -> :sswitch_a91
        0xdc -> :sswitch_97a
        0xdd -> :sswitch_97a
        0x12c -> :sswitch_9a0
        0x12d -> :sswitch_9ac
        0x12e -> :sswitch_9ce
        0x208 -> :sswitch_a9d
        0x209 -> :sswitch_ad4
        0x258 -> :sswitch_af6
        0x259 -> :sswitch_b12
        0x2bc -> :sswitch_b15
        0x320 -> :sswitch_b21
        0x384 -> :sswitch_b2d
        0x385 -> :sswitch_b39
        0x386 -> :sswitch_b80
        0x3e8 -> :sswitch_bde
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 8

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Update not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
