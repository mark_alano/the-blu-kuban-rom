.class public final Lcom/twitter/android/util/g;
.super Ljava/lang/Object;


# static fields
.field public static final a:Landroid/graphics/PorterDuffXfermode;

.field private static final b:Landroid/graphics/Paint;

.field private static final c:Landroid/graphics/Paint;

.field private static final d:Landroid/graphics/Paint;

.field private static final e:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    const v3, -0xf0f10

    const/high16 v2, 0x3f80

    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    sput-object v0, Lcom/twitter/android/util/g;->a:Landroid/graphics/PorterDuffXfermode;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/twitter/android/util/g;->b:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/twitter/android/util/g;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Lcom/twitter/android/util/g;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v0, Lcom/twitter/android/util/g;->c:Landroid/graphics/Paint;

    const v1, -0x1f1f20

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/twitter/android/util/g;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Lcom/twitter/android/util/g;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v0, Lcom/twitter/android/util/g;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/twitter/android/util/g;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x4040

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Lcom/twitter/android/util/g;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v0, Lcom/twitter/android/util/g;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .registers 5

    invoke-static {p0, p1, p2, p3}, Lcom/twitter/android/util/g;->b(Landroid/content/ContentResolver;Landroid/net/Uri;II)Lcom/twitter/android/util/h;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, v0, Lcom/twitter/android/util/h;->a:Landroid/graphics/Bitmap;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;FFI)Landroid/graphics/Bitmap;
    .registers 18

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v0, 0x0

    :try_start_6
    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_b2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_9} :catch_a8

    move-result-object v1

    :try_start_a
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v3, 0x0

    invoke-static {v1, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v5, v3

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v6, v0

    const/4 v0, 0x0

    sub-float v3, p2, v0

    const/4 v0, 0x0

    sub-float v0, p3, v0

    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_25
    .catchall {:try_start_a .. :try_end_25} :catchall_bd
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_25} :catch_bf

    move-result-object v2

    cmpl-float v4, v5, v3

    if-gtz v4, :cond_2e

    cmpl-float v4, v6, v0

    if-lez v4, :cond_98

    :cond_2e
    div-float v4, v5, v3

    div-float v7, v6, v0

    cmpl-float v8, v4, v7

    if-lez v8, :cond_8d

    div-float v0, v6, v4

    :goto_38
    div-float v4, v5, v3

    const/high16 v6, 0x3f80

    cmpl-float v4, v4, v6

    if-lez v4, :cond_90

    :try_start_40
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    div-float/2addr v5, v3

    float-to-int v5, v5

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v5, 0x0

    invoke-static {v2, v5, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object v5, v4

    move v4, v3

    move v3, v0

    :goto_51
    if-eqz v5, :cond_a0

    float-to-int v0, v4

    add-int/lit8 v0, v0, 0x0

    float-to-int v6, v3

    add-int/lit8 v6, v6, 0x0

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    float-to-int v3, v3

    invoke-direct {v8, v9, v10, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    sget-object v3, Lcom/twitter/android/util/g;->b:Landroid/graphics/Paint;

    invoke-virtual {v6, v5, v7, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_86
    .catchall {:try_start_40 .. :try_end_86} :catchall_bd
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_86} :catch_c3

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    :goto_8c
    return-object v0

    :cond_8d
    div-float v3, v5, v7

    goto :goto_38

    :cond_90
    :try_start_90
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object v5, v4

    move v4, v3

    move v3, v0

    goto :goto_51

    :cond_98
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_9b
    .catchall {:try_start_90 .. :try_end_9b} :catchall_bd
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_9b} :catch_c3

    move-result-object v4

    move-object v5, v4

    move v4, v3

    move v3, v0

    goto :goto_51

    :cond_a0
    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    goto :goto_8c

    :catch_a8
    move-exception v1

    move-object v1, v2

    :goto_aa
    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    goto :goto_8c

    :catchall_b2
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_b6
    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_bd
    move-exception v0

    goto :goto_b6

    :catch_bf
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_aa

    :catch_c3
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_aa
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;JLandroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .registers 15

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    :try_start_6
    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_77
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_9} :catch_6d

    move-result-object v1

    :try_start_a
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v3, 0x0

    invoke-static {v1, v3, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v3, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v3, v3

    iget v6, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v6, v6

    div-float/2addr v3, v6

    long-to-float v6, p2

    mul-float/2addr v6, v3

    invoke-static {v6}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v6

    long-to-float v7, p2

    const/high16 v8, 0x3f80

    div-float v3, v8, v3

    mul-float/2addr v3, v7

    invoke-static {v3}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v3

    iget v7, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v7, v7

    cmpl-float v7, v7, v6

    if-gtz v7, :cond_3b

    iget v7, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v7, v7

    cmpl-float v7, v7, v3

    if-lez v7, :cond_8f

    :cond_3b
    iget v2, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v7, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-le v2, v7, :cond_61

    iget v2, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    move v3, v2

    :goto_4c
    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_4f
    .catchall {:try_start_a .. :try_end_4f} :catchall_83
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_4f} :catch_8a

    move-result-object v2

    :try_start_50
    iput v3, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v3, 0x0

    iput-boolean v3, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v3, 0x0

    invoke-static {v2, v3, v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_59
    .catchall {:try_start_50 .. :try_end_59} :catchall_88
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_59} :catch_8d

    move-result-object v0

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    :goto_60
    return-object v0

    :cond_61
    :try_start_61
    iget v2, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v6

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D
    :try_end_69
    .catchall {:try_start_61 .. :try_end_69} :catchall_83
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_69} :catch_8a

    move-result-wide v2

    double-to-int v2, v2

    move v3, v2

    goto :goto_4c

    :catch_6d
    move-exception v1

    move-object v1, v0

    move-object v2, v0

    :goto_70
    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    goto :goto_60

    :catchall_77
    move-exception v1

    move-object v2, v0

    move-object v9, v0

    move-object v0, v1

    move-object v1, v9

    :goto_7c
    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_83
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_7c

    :catchall_88
    move-exception v0

    goto :goto_7c

    :catch_8a
    move-exception v2

    move-object v2, v0

    goto :goto_70

    :catch_8d
    move-exception v3

    goto :goto_70

    :cond_8f
    move v3, v2

    goto :goto_4c
.end method

.method public static a([BI)Landroid/graphics/Bitmap;
    .registers 11

    const/4 v8, 0x0

    const/4 v0, 0x0

    const/4 v7, 0x0

    invoke-static {p0, p1, p1}, Lcom/twitter/android/util/g;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_a

    :goto_9
    return-object v0

    :cond_a
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p1, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v4, Landroid/graphics/Paint;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(I)V

    new-instance v5, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/16 v6, 0x8

    new-array v6, v6, [F

    fill-array-data v6, :array_5e

    invoke-direct {v5, v6, v0, v0}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    int-to-float v0, p1

    int-to-float v6, p1

    invoke-virtual {v5, v0, v6}, Landroid/graphics/drawable/shapes/RoundRectShape;->resize(FF)V

    invoke-virtual {v5, v3, v4}, Landroid/graphics/drawable/shapes/RoundRectShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/twitter/android/util/g;->a:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p1, :cond_40

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, p1, :cond_5a

    :cond_40
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v0, v7, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v7, v7, p1, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v2, v0, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_55
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    goto :goto_9

    :cond_5a
    invoke-virtual {v3, v2, v8, v8, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_55

    :array_5e
    .array-data 0x4
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
        0x0t 0x0t 0xa0t 0x40t
    .end array-data
.end method

.method public static a([BII)Landroid/graphics/Bitmap;
    .registers 6

    const/4 v2, 0x0

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    array-length v1, p0

    invoke-static {p0, v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/util/g;->a(Landroid/graphics/BitmapFactory$Options;II)V

    array-length v1, p0

    invoke-static {p0, v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/Bitmap;J)Landroid/net/Uri;
    .registers 10

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v1, p2, p3}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;ZJ)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_26

    :try_start_8
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_15
    .catchall {:try_start_8 .. :try_end_15} :catchall_2d
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_15} :catch_27

    :try_start_15
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {p1, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_22
    .catchall {:try_start_15 .. :try_end_22} :catchall_35
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_22} :catch_37

    move-result-object v0

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    :cond_26
    :goto_26
    return-object v0

    :catch_27
    move-exception v1

    move-object v1, v0

    :goto_29
    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    goto :goto_26

    :catchall_2d
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_31
    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_35
    move-exception v0

    goto :goto_31

    :catch_37
    move-exception v2

    goto :goto_29
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;IJ)Landroid/net/Uri;
    .registers 10

    const/4 v0, 0x0

    if-nez p1, :cond_5

    move-object p1, v0

    :cond_4
    :goto_4
    return-object p1

    :cond_5
    mul-int/lit16 v1, p2, 0xfa

    int-to-long v1, v1

    invoke-static {p0, p1}, Lcom/twitter/android/util/g;->b(Landroid/content/Context;Landroid/net/Uri;)F

    move-result v3

    int-to-float v4, p2

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    invoke-static {p0, p1, v1, v2, v0}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;JLandroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_19

    move-object p1, v0

    goto :goto_4

    :cond_19
    invoke-static {p0, v1, p3, p4}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/graphics/Bitmap;J)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_22

    invoke-static {p0, p1}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    :cond_22
    move-object p1, v0

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;J)Landroid/net/Uri;
    .registers 6

    invoke-static {p0, p1, p2, p3}, Lcom/twitter/android/util/g;->b(Landroid/content/Context;Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const/16 v1, 0xc00

    invoke-static {p0, v0, v1, p2, p3}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;IJ)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ZJ)Ljava/lang/String;
    .registers 7

    invoke-static {p0}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    if-eqz p1, :cond_32

    const-string v0, "pic-r-"

    :goto_c
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_32
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "pic-"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_c
.end method

.method public static a(Landroid/content/Context;J)V
    .registers 11

    invoke-static {p0}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    :cond_6
    return-void

    :cond_7
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/twitter/android/util/i;

    invoke-direct {v2, p1, p2}, Lcom/twitter/android/util/i;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_17
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_17
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;II)V
    .registers 7

    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/4 v0, 0x1

    :goto_d
    div-int/lit8 v3, v1, 0x2

    if-le v3, v2, :cond_16

    div-int/lit8 v1, v1, 0x2

    mul-int/lit8 v0, v0, 0x2

    goto :goto_d

    :cond_16
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    iput v0, p0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .registers 6

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    :cond_7
    :goto_7
    return v0

    :cond_8
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v1, v1

    mul-long/2addr v1, v3

    const-wide/32 v3, 0x100000

    cmp-long v1, v1, v3

    if-lez v1, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 6

    const/4 v0, 0x0

    if-eqz p1, :cond_c

    invoke-static {p0}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    move v1, v0

    :goto_a
    if-nez v1, :cond_29

    :cond_c
    :goto_c
    return v0

    :cond_d
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/pic-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    goto :goto_a

    :cond_29
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    goto :goto_c
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;)F
    .registers 6

    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_1b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_8} :catch_15

    move-result-object v1

    :try_start_9
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_23
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_c} :catch_25

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x4480

    div-float/2addr v0, v2

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    :goto_14
    return v0

    :catch_15
    move-exception v1

    :goto_16
    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    goto :goto_14

    :catchall_1b
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1f
    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_23
    move-exception v0

    goto :goto_1f

    :catch_25
    move-exception v0

    move-object v0, v1

    goto :goto_16
.end method

.method public static b([BII)Landroid/graphics/Bitmap;
    .registers 10

    const/4 v1, 0x0

    invoke-static {p0, p1, p2}, Lcom/twitter/android/util/g;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_9

    const/4 v0, 0x0

    :cond_8
    :goto_8
    return-object v0

    :cond_9
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    if-le v6, v2, :cond_8

    rem-int v5, v6, v2

    if-eqz v5, :cond_8

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    int-to-float v2, v2

    int-to-float v6, v6

    div-float/2addr v2, v6

    invoke-virtual {v5, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    goto :goto_8
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;J)Landroid/net/Uri;
    .registers 10

    const/4 v0, 0x0

    if-nez p1, :cond_5

    move-object p1, v0

    :goto_4
    return-object p1

    :cond_5
    :try_start_5
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_6c
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_c} :catch_61

    move-result-object v2

    :try_start_d
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v4, 0x0

    invoke-static {v2, v4, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget-object v4, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v5, "image/jpeg"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_37

    iget-object v4, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v5, "image/gif"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_37

    iget-object v3, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v4, "image/png"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_34
    .catchall {:try_start_d .. :try_end_34} :catchall_77
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_34} :catch_7d

    move-result v3

    if-eqz v3, :cond_3e

    :cond_37
    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    goto :goto_4

    :cond_3e
    :try_start_3e
    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_41
    .catchall {:try_start_3e .. :try_end_41} :catchall_77
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_41} :catch_7d

    move-result-object v3

    :try_start_42
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_59

    invoke-static {p0, v1, p2, p3}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/graphics/Bitmap;J)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_51

    invoke-static {p0, p1}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z
    :try_end_51
    .catchall {:try_start_42 .. :try_end_51} :catchall_7b
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_51} :catch_81

    :cond_51
    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object p1, v1

    goto :goto_4

    :cond_59
    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object p1, v0

    goto :goto_4

    :catch_61
    move-exception v1

    move-object v1, v0

    move-object v2, v0

    :goto_64
    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object p1, v0

    goto :goto_4

    :catchall_6c
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    :goto_70
    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_77
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_70

    :catchall_7b
    move-exception v0

    goto :goto_70

    :catch_7d
    move-exception v1

    move-object v1, v2

    move-object v2, v0

    goto :goto_64

    :catch_81
    move-exception v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_64
.end method

.method public static b(Landroid/content/ContentResolver;Landroid/net/Uri;II)Lcom/twitter/android/util/h;
    .registers 12

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_45
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_4} :catch_2f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_4} :catch_3a

    move-result-object v3

    :try_start_5
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v0, 0x0

    invoke-static {v3, v0, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v4, p2, p3}, Lcom/twitter/android/util/g;->a(Landroid/graphics/BitmapFactory$Options;II)V

    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1b
    .catchall {:try_start_5 .. :try_end_1b} :catchall_4e
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_1b} :catch_58
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_1b} :catch_53

    move-result-object v2

    :try_start_1c
    new-instance v0, Lcom/twitter/android/util/h;

    const/4 v7, 0x0

    invoke-static {v2, v7, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-direct {v0, v7, v4, v5, v6}, Lcom/twitter/android/util/h;-><init>(Landroid/graphics/Bitmap;III)V
    :try_end_28
    .catchall {:try_start_1c .. :try_end_28} :catchall_50
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_28} :catch_5c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1c .. :try_end_28} :catch_56

    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    :goto_2e
    return-object v0

    :catch_2f
    move-exception v0

    move-object v0, v1

    move-object v2, v1

    :goto_32
    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_2e

    :catch_3a
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_3d
    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_2e

    :catchall_45
    move-exception v0

    move-object v3, v1

    :goto_47
    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_4e
    move-exception v0

    goto :goto_47

    :catchall_50
    move-exception v0

    move-object v1, v2

    goto :goto_47

    :catch_53
    move-exception v0

    move-object v2, v1

    goto :goto_3d

    :catch_56
    move-exception v0

    goto :goto_3d

    :catch_58
    move-exception v0

    move-object v0, v1

    move-object v2, v3

    goto :goto_32

    :catch_5c
    move-exception v0

    move-object v0, v2

    move-object v2, v3

    goto :goto_32
.end method
