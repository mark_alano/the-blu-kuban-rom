.class public final Lcom/twitter/android/util/x;
.super Lcom/twitter/android/util/s;


# instance fields
.field public final h:I

.field public final i:Landroid/graphics/drawable/Drawable;

.field private final j:Ljava/util/ArrayList;

.field private final k:Landroid/content/Context;

.field private final l:I

.field private final m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .registers 8

    const/4 v3, 0x0

    const/16 v0, 0x96

    const/16 v1, 0x32

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/util/s;-><init>(Landroid/content/Context;II)V

    iput p2, p0, Lcom/twitter/android/util/x;->h:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/util/x;->j:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/twitter/android/util/x;->k:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/twitter/android/ee;->user_image_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/util/x;->l:I

    iget v1, p0, Lcom/twitter/android/util/x;->h:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3a

    iget v1, p0, Lcom/twitter/android/util/x;->l:I

    iput v1, p0, Lcom/twitter/android/util/x;->m:I

    :goto_28
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object v0, p0, Lcom/twitter/android/util/x;->i:Landroid/graphics/drawable/Drawable;

    return-void

    :cond_3a
    sget v1, Lcom/twitter/android/ee;->mini_user_image_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/util/x;->m:I

    goto :goto_28
.end method

.method private a(Ljava/lang/Long;Ljava/lang/String;[B)Lcom/twitter/android/util/r;
    .registers 7

    const/4 v0, 0x0

    if-nez p3, :cond_4

    :cond_3
    :goto_3
    return-object v0

    :cond_4
    const/4 v1, 0x2

    iget v2, p0, Lcom/twitter/android/util/x;->h:I

    if-ne v1, v2, :cond_17

    iget v1, p0, Lcom/twitter/android/util/x;->m:I

    invoke-static {p3, v1, v1}, Lcom/twitter/android/util/g;->b([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_f
    if-eqz v1, :cond_3

    new-instance v0, Lcom/twitter/android/util/r;

    invoke-direct {v0, p2, v1}, Lcom/twitter/android/util/r;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_17
    const/4 v1, 0x0

    array-length v2, p3

    invoke-static {p3, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_f
.end method


# virtual methods
.method public final a(JJLjava/lang/String;)Landroid/graphics/Bitmap;
    .registers 12

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v4, p5

    invoke-super/range {v0 .. v5}, Lcom/twitter/android/util/s;->a(JLjava/lang/Object;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(JLjava/lang/Object;Ljava/lang/String;)Lcom/twitter/android/util/r;
    .registers 8

    check-cast p3, Ljava/lang/Long;

    iget-object v0, p0, Lcom/twitter/android/util/x;->k:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/util/x;->h:I

    packed-switch v1, :pswitch_data_32

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid cache type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/twitter/android/util/x;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_24
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/provider/ae;->e(J)[B

    move-result-object v0

    invoke-direct {p0, p3, p4, v0}, Lcom/twitter/android/util/x;->a(Ljava/lang/Long;Ljava/lang/String;[B)Lcom/twitter/android/util/r;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_24
        :pswitch_24
    .end packed-switch
.end method

.method protected final synthetic a(JLjava/lang/Object;Ljava/lang/String;[B)Lcom/twitter/android/util/r;
    .registers 12

    check-cast p3, Ljava/lang/Long;

    iget-object v0, p0, Lcom/twitter/android/util/x;->k:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/util/x;->h:I

    packed-switch v1, :pswitch_data_20

    const/4 v0, 0x0

    :goto_e
    return-object v0

    :pswitch_f
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget v5, p0, Lcom/twitter/android/util/x;->l:I

    move-object v3, p4

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/provider/ae;->a(JLjava/lang/String;[BI)[B

    move-result-object v0

    invoke-direct {p0, p3, p4, v0}, Lcom/twitter/android/util/x;->a(Ljava/lang/Long;Ljava/lang/String;[B)Lcom/twitter/android/util/r;

    move-result-object v0

    goto :goto_e

    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;[B)Lcom/twitter/android/util/r;
    .registers 5

    check-cast p1, Ljava/lang/Long;

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/util/x;->a(Ljava/lang/Long;Ljava/lang/String;[B)Lcom/twitter/android/util/r;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/twitter/android/util/y;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/util/x;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final a(Ljava/util/HashMap;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/util/x;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/y;

    invoke-interface {v0, p0, p1}, Lcom/twitter/android/util/y;->a(Lcom/twitter/android/util/x;Ljava/util/HashMap;)V

    goto :goto_6

    :cond_16
    return-void
.end method

.method public final b(Lcom/twitter/android/util/y;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/util/x;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
