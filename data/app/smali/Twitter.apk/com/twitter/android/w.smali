.class final Lcom/twitter/android/w;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lcom/twitter/android/CropActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/CropActivity;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    invoke-virtual {v0}, Lcom/twitter/android/CropActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    invoke-virtual {v1}, Lcom/twitter/android/CropActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    invoke-static {v2}, Lcom/twitter/android/CropActivity;->a(Lcom/twitter/android/CropActivity;)Landroid/net/Uri;

    move-result-object v2

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2, v3, v0}, Lcom/twitter/android/util/g;->b(Landroid/content/ContentResolver;Landroid/net/Uri;II)Lcom/twitter/android/util/h;

    move-result-object v0

    if-eqz v0, :cond_37

    iget v1, v0, Lcom/twitter/android/util/h;->b:I

    iget-object v2, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    iget v3, v0, Lcom/twitter/android/util/h;->c:I

    rem-int/2addr v3, v1

    iput v3, v2, Lcom/twitter/android/CropActivity;->b:I

    iget-object v2, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    iget v3, v0, Lcom/twitter/android/util/h;->d:I

    rem-int/2addr v3, v1

    iput v3, v2, Lcom/twitter/android/CropActivity;->c:I

    iget-object v2, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    iput v1, v2, Lcom/twitter/android/CropActivity;->a:I

    iget-object v0, v0, Lcom/twitter/android/util/h;->a:Landroid/graphics/Bitmap;

    :goto_36
    return-object v0

    :cond_37
    const/4 v0, 0x0

    goto :goto_36
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 5

    const/4 v2, 0x1

    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p1, :cond_14

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    invoke-static {v0}, Lcom/twitter/android/CropActivity;->b(Lcom/twitter/android/CropActivity;)Lcom/twitter/android/widget/CroppableImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/CroppableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_e
    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    invoke-virtual {v0, v2}, Lcom/twitter/android/CropActivity;->removeDialog(I)V

    return-void

    :cond_14
    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    const v1, 0x7f0b0168

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/CropActivity;->setResult(I)V

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    invoke-virtual {v0}, Lcom/twitter/android/CropActivity;->finish()V

    goto :goto_e
.end method

.method protected final onPreExecute()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/CropActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/CropActivity;->showDialog(I)V

    return-void
.end method
