.class public final Lcom/twitter/android/widget/HorizontalListView;
.super Landroid/widget/AdapterView;


# instance fields
.field private A:Lcom/twitter/android/widget/i;

.field a:Landroid/widget/ListAdapter;

.field b:I

.field c:Z

.field d:Lcom/twitter/android/util/j;

.field e:I

.field private f:Landroid/database/DataSetObserver;

.field private g:I

.field private h:Lcom/twitter/android/widget/k;

.field private final i:[Z

.field private j:I

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/Rect;

.field private m:I

.field private n:I

.field private o:Lcom/twitter/android/widget/h;

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:I

.field private r:Landroid/graphics/Paint;

.field private s:Ljava/lang/Runnable;

.field private t:Lcom/twitter/android/widget/j;

.field private u:Z

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/HorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    sget v0, Lcom/twitter/android/eb;->horizontalListViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/HorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9

    const/4 v4, 0x1

    const/high16 v1, -0x8000

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/twitter/android/util/j;

    invoke-direct {v0}, Lcom/twitter/android/util/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->d:Lcom/twitter/android/util/j;

    new-instance v0, Lcom/twitter/android/widget/k;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/k;-><init>(Lcom/twitter/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    new-array v0, v4, [Z

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->i:[Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    iput v3, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->m:I

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    new-instance v0, Lcom/twitter/android/widget/i;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/i;-><init>(Lcom/twitter/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->A:Lcom/twitter/android/widget/i;

    sget-object v0, Lcom/twitter/android/ek;->HorizontalListView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_45

    if-eqz v1, :cond_6d

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iput v2, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    :goto_3d
    iput-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->invalidate()V

    :cond_45
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    if-lez v1, :cond_53

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->invalidate()V

    :cond_53
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->v:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->w:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/HorizontalListView;->x:I

    return-void

    :cond_6d
    iput v3, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    goto :goto_3d
.end method

.method static synthetic a(Lcom/twitter/android/widget/HorizontalListView;)I
    .registers 2

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->n:I

    return v0
.end method

.method private a(IIIZ)Landroid/view/View;
    .registers 11

    iget-boolean v0, p0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/k;->a(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_15

    const/4 v5, 0x1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/view/View;IIZZ)V

    :goto_14
    return-object v1

    :cond_15
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->i:[Z

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/HorizontalListView;->a(I[Z)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->i:[Z

    const/4 v2, 0x0

    aget-boolean v5, v0, v2

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/view/View;IIZZ)V

    goto :goto_14
.end method

.method private a(I[Z)Landroid/view/View;
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/k;->b(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1b

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eq v0, v1, :cond_16

    iget-object v2, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    invoke-virtual {v2, v1, p1}, Lcom/twitter/android/widget/k;->a(Landroid/view/View;I)V

    :goto_15
    return-object v0

    :cond_16
    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-boolean v2, p2, v1

    goto :goto_15

    :cond_1b
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_15
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private a(Landroid/view/View;I)V
    .registers 7

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    if-nez v0, :cond_12

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_12
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->height:I

    invoke-static {p2, v1, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    iget v0, v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->width:I

    if-lez v0, :cond_2f

    const/high16 v2, 0x4000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_2b
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    return-void

    :cond_2f
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_2b
.end method

.method private a(Landroid/view/View;IIZZ)V
    .registers 12

    const/4 v3, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    if-nez v0, :cond_14

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_14
    move-object v4, v0

    if-eqz p5, :cond_5b

    if-eqz p4, :cond_59

    move v0, v1

    :goto_1a
    invoke-virtual {p0, p1, v0, v4}, Lcom/twitter/android/widget/HorizontalListView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_1d
    if-eqz p5, :cond_25

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_63

    :cond_25
    move v1, v3

    :goto_26
    if-eqz v1, :cond_6a

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->g:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v5

    add-int/2addr v3, v5

    iget v5, v4, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->height:I

    invoke-static {v0, v3, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v3

    iget v0, v4, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->width:I

    if-lez v0, :cond_65

    const/high16 v2, 0x4000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_43
    invoke-virtual {p1, v0, v3}, Landroid/view/View;->measure(II)V

    :goto_46
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-eqz p4, :cond_6e

    :goto_50
    if-eqz v1, :cond_70

    add-int/2addr v0, p2

    add-int v1, p3, v2

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    :goto_58
    return-void

    :cond_59
    move v0, v2

    goto :goto_1a

    :cond_5b
    if-eqz p4, :cond_61

    :goto_5d
    invoke-virtual {p0, p1, v1, v4, v3}, Lcom/twitter/android/widget/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_1d

    :cond_61
    move v1, v2

    goto :goto_5d

    :cond_63
    move v1, v2

    goto :goto_26

    :cond_65
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_43

    :cond_6a
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/HorizontalListView;->cleanupLayoutState(Landroid/view/View;)V

    goto :goto_46

    :cond_6e
    sub-int/2addr p2, v0

    goto :goto_50

    :cond_70
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v0, p2, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, p3, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_58
.end method

.method static synthetic a(Lcom/twitter/android/widget/HorizontalListView;Landroid/view/View;Z)V
    .registers 4

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/widget/HorizontalListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method private a(II)Z
    .registers 16

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v7

    if-gez p2, :cond_60

    move v0, v1

    :goto_9
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v8

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v4

    sub-int v9, v3, v4

    add-int/lit8 v3, v7, -0x1

    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v5

    if-eqz v0, :cond_62

    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->c()I

    move-result v3

    :goto_2f
    iget v10, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    if-eqz v0, :cond_69

    add-int v5, v10, v7

    iget v6, p0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    add-int/lit8 v6, v6, -0x1

    if-lt v5, v6, :cond_3e

    add-int/2addr v4, p1

    if-lt v4, v9, :cond_67

    :cond_3e
    :goto_3e
    move v6, v1

    :goto_3f
    if-eqz v6, :cond_d7

    if-eqz v0, :cond_91

    move v4, v2

    move v1, v2

    :goto_45
    if-ge v4, v7, :cond_73

    invoke-virtual {p0, v4}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v5

    add-int/2addr v5, p1

    if-ge v5, v8, :cond_73

    add-int/lit8 v5, v1, 0x1

    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    add-int v11, v10, v4

    invoke-virtual {v1, v9, v11}, Lcom/twitter/android/widget/k;->a(Landroid/view/View;I)V

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v5

    goto :goto_45

    :cond_60
    move v0, v2

    goto :goto_9

    :cond_62
    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->d()I

    move-result v3

    goto :goto_2f

    :cond_67
    move v1, v2

    goto :goto_3e

    :cond_69
    if-gtz v10, :cond_6f

    add-int v4, v5, p1

    if-gt v4, v8, :cond_71

    :cond_6f
    :goto_6f
    move v6, v1

    goto :goto_3f

    :cond_71
    move v1, v2

    goto :goto_6f

    :cond_73
    move v4, v2

    :cond_74
    if-lez v1, :cond_80

    invoke-virtual {p0, v4, v1}, Lcom/twitter/android/widget/HorizontalListView;->detachViewsFromParent(II)V

    if-eqz v0, :cond_80

    iget v4, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    add-int/2addr v1, v4

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    :cond_80
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v4

    move v1, v2

    :goto_85
    if-ge v1, v4, :cond_b3

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_85

    :cond_91
    add-int/lit8 v1, v7, -0x1

    move v5, v1

    move v4, v2

    move v1, v2

    :goto_96
    if-ltz v5, :cond_74

    invoke-virtual {p0, v5}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    add-int/2addr v8, p1

    if-le v8, v9, :cond_74

    add-int/lit8 v4, v1, 0x1

    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    add-int v8, v10, v5

    invoke-virtual {v1, v7, v8}, Lcom/twitter/android/widget/k;->a(Landroid/view/View;I)V

    add-int/lit8 v1, v5, -0x1

    move v12, v1

    move v1, v4

    move v4, v5

    move v5, v12

    goto :goto_96

    :cond_b3
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->invalidate()V

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v3, v1, :cond_d7

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    if-eqz v0, :cond_dd

    if-lez v1, :cond_d8

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget v2, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    add-int/2addr v0, v2

    :goto_d1
    iget v2, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    add-int/2addr v1, v2

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/HorizontalListView;->b(II)V

    :cond_d7
    :goto_d7
    return v6

    :cond_d8
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    goto :goto_d1

    :cond_dd
    if-lez v1, :cond_10d

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    sub-int/2addr v0, v1

    :goto_ea
    iget v1, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v5

    move v3, v1

    move v1, v0

    move v0, v2

    :goto_f9
    if-le v1, v5, :cond_10f

    if-ltz v3, :cond_10f

    invoke-direct {p0, v3, v1, v4, v2}, Lcom/twitter/android/widget/HorizontalListView;->a(IIIZ)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v7, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    sub-int/2addr v1, v7

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_f9

    :cond_10d
    move v0, v2

    goto :goto_ea

    :cond_10f
    iget v1, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    sub-int v0, v1, v0

    iput v0, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    goto :goto_d7
.end method

.method static synthetic a(Lcom/twitter/android/widget/HorizontalListView;II)Z
    .registers 4

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/HorizontalListView;->a(II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/HorizontalListView;)I
    .registers 2

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    return v0
.end method

.method private b()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->z:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->z:Landroid/view/VelocityTracker;

    return-void
.end method

.method private b(II)V
    .registers 7

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    :goto_d
    if-ge p2, v1, :cond_23

    iget v2, p0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    if-ge p1, v2, :cond_23

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/twitter/android/widget/HorizontalListView;->a(IIIZ)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget v3, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    add-int p2, v2, v3

    add-int/lit8 p1, p1, 0x1

    goto :goto_d

    :cond_23
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/HorizontalListView;Landroid/view/View;Z)V
    .registers 4

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/widget/HorizontalListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method private c()I
    .registers 3

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int v0, v1, v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/widget/HorizontalListView;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->e()V

    return-void
.end method

.method private d()I
    .registers 3

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/HorizontalListView;)Landroid/graphics/drawable/Drawable;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->k:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/widget/HorizontalListView;)I
    .registers 2

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method private e()V
    .registers 9

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    iget v2, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    iget-object v3, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v4

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-eqz v0, :cond_20

    move v0, v1

    :goto_12
    if-ge v0, v4, :cond_23

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    add-int v7, v2, v0

    invoke-virtual {v3, v6, v7}, Lcom/twitter/android/widget/k;->a(Landroid/view/View;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_20
    invoke-virtual {v3, v2, v4}, Lcom/twitter/android/widget/k;->a(II)V

    :cond_23
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->detachAllViewsFromParent()V

    if-nez v5, :cond_35

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/widget/HorizontalListView;->b(II)V

    :goto_2f
    invoke-virtual {v3}, Lcom/twitter/android/widget/k;->a()V

    iput-boolean v1, p0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    return-void

    :cond_35
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/widget/HorizontalListView;->b(II)V

    goto :goto_2f
.end method

.method static synthetic f(Lcom/twitter/android/widget/HorizontalListView;)I
    .registers 2

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/twitter/android/widget/HorizontalListView;)I
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->d()I

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/widget/HorizontalListView;)I
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Landroid/widget/ListAdapter;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public final a(Landroid/widget/ListAdapter;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    if-eq p1, v0, :cond_27

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->f:Landroid/database/DataSetObserver;

    if-nez v0, :cond_f

    new-instance v0, Lcom/twitter/android/widget/g;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/g;-><init>(Lcom/twitter/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->f:Landroid/database/DataSetObserver;

    :cond_f
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->f:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1a
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->f:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object p1, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    :cond_27
    return-void
.end method

.method protected final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    instance-of v0, p1, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    return v0
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 15

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v4, p0, Lcom/twitter/android/widget/HorizontalListView;->q:I

    if-lez v4, :cond_8c

    iget-object v2, p0, Lcom/twitter/android/widget/HorizontalListView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_8c

    move v2, v0

    :goto_b
    if-eqz v2, :cond_ac

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v2

    iput v2, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v6

    iget v7, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    iget-object v8, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getScrollX()I

    move-result v9

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->isOpaque()Z

    move-result v2

    if-eqz v2, :cond_8f

    invoke-super {p0}, Landroid/widget/AdapterView;->isOpaque()Z

    move-result v2

    if-nez v2, :cond_8f

    move v3, v0

    :goto_3c
    if-eqz v3, :cond_49

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->r:Landroid/graphics/Paint;

    if-nez v0, :cond_49

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->r:Landroid/graphics/Paint;

    :cond_49
    iget-object v10, p0, Lcom/twitter/android/widget/HorizontalListView;->r:Landroid/graphics/Paint;

    iget-boolean v0, p0, Lcom/twitter/android/widget/HorizontalListView;->u:Z

    if-eqz v0, :cond_91

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v0

    :goto_57
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWidth()I

    move-result v11

    add-int/2addr v11, v9

    sub-int v0, v11, v0

    :goto_5e
    if-ge v1, v6, :cond_a0

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v11

    if-le v11, v2, :cond_89

    add-int v12, v7, v1

    invoke-interface {v8, v12}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v12

    if-eqz v12, :cond_94

    add-int/lit8 v12, v6, -0x1

    if-eq v1, v12, :cond_80

    add-int v12, v7, v1

    add-int/lit8 v12, v12, 0x1

    invoke-interface {v8, v12}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v12

    if-eqz v12, :cond_94

    :cond_80
    sub-int v12, v11, v4

    iput v12, v5, Landroid/graphics/Rect;->left:I

    iput v11, v5, Landroid/graphics/Rect;->right:I

    invoke-direct {p0, p1, v5}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    :cond_89
    :goto_89
    add-int/lit8 v1, v1, 0x1

    goto :goto_5e

    :cond_8c
    move v2, v1

    goto/16 :goto_b

    :cond_8f
    move v3, v1

    goto :goto_3c

    :cond_91
    move v0, v1

    move v2, v1

    goto :goto_57

    :cond_94
    if-eqz v3, :cond_89

    sub-int v12, v11, v4

    iput v12, v5, Landroid/graphics/Rect;->left:I

    iput v11, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1, v5, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_89

    :cond_a0
    if-lez v6, :cond_ac

    if-lez v9, :cond_ac

    iput v0, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v4

    iput v0, v5, Landroid/graphics/Rect;->right:I

    invoke-direct {p0, p1, v5}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    :cond_ac
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    new-instance v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    new-instance v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    new-instance v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public final bridge synthetic getAdapter()Landroid/widget/Adapter;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public final getSelectedView()Landroid/view/View;
    .registers 2

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final onDetachedFromWindow()V
    .registers 2

    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    invoke-virtual {v0}, Lcom/twitter/android/widget/k;->b()V

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->o:Lcom/twitter/android/widget/h;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->o:Lcom/twitter/android/widget/h;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/HorizontalListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_11
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .registers 9

    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    if-eqz p1, :cond_16

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v1, :cond_16

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_16
    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->e()V

    return-void
.end method

.method protected final onMeasure(II)V
    .registers 15

    const/4 v10, -0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    if-nez v0, :cond_59

    move v0, v1

    :goto_17
    iput v0, p0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    if-lez v0, :cond_9f

    if-eqz v5, :cond_21

    if-nez v6, :cond_9f

    :cond_21
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->i:[Z

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/HorizontalListView;->a(I[Z)Landroid/view/View;

    move-result-object v7

    invoke-direct {p0, v7, p2}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/view/View;I)V

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v8, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    invoke-virtual {v8, v7, v10}, Lcom/twitter/android/widget/k;->a(Landroid/view/View;I)V

    move v11, v0

    move v0, v4

    move v4, v11

    :goto_3a
    if-nez v5, :cond_60

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    :cond_46
    :goto_46
    move v2, v0

    :cond_47
    if-nez v6, :cond_9d

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v4

    :goto_53
    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/widget/HorizontalListView;->setMeasuredDimension(II)V

    iput p2, p0, Lcom/twitter/android/widget/HorizontalListView;->g:I

    return-void

    :cond_59
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_17

    :cond_60
    const/high16 v0, -0x8000

    if-ne v5, v0, :cond_47

    iget-object v5, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    if-nez v5, :cond_72

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_46

    :cond_72
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v7

    add-int/2addr v0, v7

    invoke-interface {v5}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iget-object v7, p0, Lcom/twitter/android/widget/HorizontalListView;->h:Lcom/twitter/android/widget/k;

    iget-object v8, p0, Lcom/twitter/android/widget/HorizontalListView;->i:[Z

    :goto_85
    if-gt v1, v5, :cond_46

    invoke-direct {p0, v1, v8}, Lcom/twitter/android/widget/HorizontalListView;->a(I[Z)Landroid/view/View;

    move-result-object v9

    invoke-direct {p0, v9, p2}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/view/View;I)V

    invoke-virtual {v7, v9, v10}, Lcom/twitter/android/widget/k;->a(Landroid/view/View;I)V

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v0, v9

    if-lt v0, v2, :cond_9a

    move v0, v2

    goto :goto_46

    :cond_9a
    add-int/lit8 v1, v1, 0x1

    goto :goto_85

    :cond_9d
    move v0, v3

    goto :goto_53

    :cond_9f
    move v4, v1

    move v0, v1

    goto :goto_3a
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 12

    const/4 v7, 0x3

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/high16 v9, -0x8000

    const/4 v2, 0x1

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    if-nez v0, :cond_f

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->z:Landroid/view/VelocityTracker;

    if-nez v0, :cond_19

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->z:Landroid/view/VelocityTracker;

    :cond_19
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->z:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v5, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v6, v4

    packed-switch v0, :pswitch_data_1d2

    :cond_31
    :goto_31
    move v0, v2

    goto :goto_e

    :pswitch_33
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->l:Landroid/graphics/Rect;

    if-nez v0, :cond_40

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->l:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->l:Landroid/graphics/Rect;

    :cond_40
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    :goto_46
    if-ltz v4, :cond_97

    invoke-virtual {p0, v4}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_94

    invoke-virtual {v7, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_94

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    add-int/2addr v0, v4

    :goto_5e
    iget-boolean v1, p0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    if-nez v1, :cond_88

    iget v1, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    const/4 v4, 0x4

    if-eq v1, v4, :cond_88

    if-ltz v0, :cond_88

    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_88

    iput v3, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->o:Lcom/twitter/android/widget/h;

    if-nez v1, :cond_7e

    new-instance v1, Lcom/twitter/android/widget/h;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/h;-><init>(Lcom/twitter/android/widget/HorizontalListView;)V

    iput-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->o:Lcom/twitter/android/widget/h;

    :cond_7e
    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->o:Lcom/twitter/android/widget/h;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {p0, v1, v3, v4}, Lcom/twitter/android/widget/HorizontalListView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_88
    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->A:Lcom/twitter/android/widget/i;

    invoke-virtual {v1}, Lcom/twitter/android/widget/i;->a()V

    iput v5, p0, Lcom/twitter/android/widget/HorizontalListView;->m:I

    iput v9, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    iput v0, p0, Lcom/twitter/android/widget/HorizontalListView;->n:I

    goto :goto_31

    :cond_94
    add-int/lit8 v4, v4, -0x1

    goto :goto_46

    :cond_97
    move v0, v1

    goto :goto_5e

    :pswitch_99
    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    packed-switch v0, :pswitch_data_1de

    :goto_9e
    iput v9, p0, Lcom/twitter/android/widget/HorizontalListView;->m:I

    iput v9, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    goto :goto_31

    :pswitch_a3
    iget v4, p0, Lcom/twitter/android/widget/HorizontalListView;->n:I

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->j:I

    sub-int v0, v4, v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    if-le v5, v0, :cond_13e

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v7

    sub-int/2addr v0, v7

    if-ge v5, v0, :cond_13e

    move v0, v2

    :goto_bf
    if-eqz v6, :cond_152

    invoke-virtual {v6}, Landroid/view/View;->hasFocusable()Z

    move-result v5

    if-nez v5, :cond_152

    if-eqz v0, :cond_152

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    if-eqz v0, :cond_d0

    invoke-virtual {v6, v3}, Landroid/view/View;->setPressed(Z)V

    :cond_d0
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->t:Lcom/twitter/android/widget/j;

    if-nez v0, :cond_db

    new-instance v0, Lcom/twitter/android/widget/j;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/j;-><init>(Lcom/twitter/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->t:Lcom/twitter/android/widget/j;

    :cond_db
    iget-object v3, p0, Lcom/twitter/android/widget/HorizontalListView;->t:Lcom/twitter/android/widget/j;

    iput v4, v3, Lcom/twitter/android/widget/j;->a:I

    invoke-virtual {v3}, Lcom/twitter/android/widget/j;->a()V

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    if-eqz v0, :cond_ea

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    if-ne v0, v2, :cond_143

    :cond_ea
    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_f5

    iget-object v5, p0, Lcom/twitter/android/widget/HorizontalListView;->o:Lcom/twitter/android/widget/h;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_f5
    iget-boolean v0, p0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    if-nez v0, :cond_140

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_140

    iput v2, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->e()V

    invoke-virtual {v6, v2}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/HorizontalListView;->setPressed(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_121

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_121

    instance-of v1, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v1, :cond_121

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    :cond_121
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->s:Ljava/lang/Runnable;

    if-eqz v0, :cond_12a

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->s:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/HorizontalListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_12a
    new-instance v0, Lcom/twitter/android/widget/f;

    invoke-direct {v0, p0, v6, v3}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/HorizontalListView;Landroid/view/View;Lcom/twitter/android/widget/j;)V

    iput-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->s:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->s:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {p0, v0, v3, v4}, Lcom/twitter/android/widget/HorizontalListView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_13b
    move v0, v2

    goto/16 :goto_e

    :cond_13e
    move v0, v3

    goto :goto_bf

    :cond_140
    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    goto :goto_13b

    :cond_143
    iget-boolean v0, p0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    if-nez v0, :cond_152

    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_152

    invoke-virtual {v3}, Lcom/twitter/android/widget/j;->run()V

    :cond_152
    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    goto/16 :goto_9e

    :pswitch_156
    iget-object v0, p0, Lcom/twitter/android/widget/HorizontalListView;->z:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v3, p0, Lcom/twitter/android/widget/HorizontalListView;->x:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/twitter/android/widget/HorizontalListView;->w:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_176

    iget-object v1, p0, Lcom/twitter/android/widget/HorizontalListView;->A:Lcom/twitter/android/widget/i;

    neg-float v0, v0

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/i;->a(I)V

    :cond_176
    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->b()V

    goto/16 :goto_9e

    :pswitch_17b
    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->m:I

    if-eq v0, v9, :cond_1bf

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_18d

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_e

    :cond_18d
    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->m:I

    sub-int v1, v5, v0

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    if-eq v0, v7, :cond_1a8

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->v:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-gt v0, v3, :cond_31

    iput v7, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1a8

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_1a8
    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    if-eq v5, v0, :cond_31

    if-lez v1, :cond_1c3

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->v:I

    sub-int v0, v1, v0

    :goto_1b2
    iget v3, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    if-eq v3, v9, :cond_1ba

    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    sub-int v0, v5, v0

    :cond_1ba
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/HorizontalListView;->a(II)Z

    iput v5, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    :cond_1bf
    iput v5, p0, Lcom/twitter/android/widget/HorizontalListView;->m:I

    goto/16 :goto_31

    :cond_1c3
    iget v0, p0, Lcom/twitter/android/widget/HorizontalListView;->v:I

    add-int/2addr v0, v1

    goto :goto_1b2

    :pswitch_1c7
    invoke-direct {p0}, Lcom/twitter/android/widget/HorizontalListView;->b()V

    iput v9, p0, Lcom/twitter/android/widget/HorizontalListView;->m:I

    iput v9, p0, Lcom/twitter/android/widget/HorizontalListView;->y:I

    iput v1, p0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    goto/16 :goto_31

    :pswitch_data_1d2
    .packed-switch 0x0
        :pswitch_33
        :pswitch_99
        :pswitch_17b
        :pswitch_1c7
    .end packed-switch

    :pswitch_data_1de
    .packed-switch 0x0
        :pswitch_a3
        :pswitch_a3
        :pswitch_a3
        :pswitch_156
    .end packed-switch
.end method

.method public final synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 2

    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/HorizontalListView;->a(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final setClipToPadding(Z)V
    .registers 2

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setClipToPadding(Z)V

    iput-boolean p1, p0, Lcom/twitter/android/widget/HorizontalListView;->u:Z

    return-void
.end method

.method public final setPressed(Z)V
    .registers 3

    invoke-virtual {p0}, Lcom/twitter/android/widget/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_9

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setPressed(Z)V

    :cond_9
    return-void
.end method

.method public final setSelection(I)V
    .registers 2

    return-void
.end method
