.class public Lcom/twitter/android/widget/MultiTouchImageView;
.super Landroid/widget/ImageView;


# instance fields
.field protected final a:Landroid/graphics/Matrix;

.field protected b:Landroid/graphics/RectF;

.field private c:I

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:Landroid/graphics/RectF;

.field private o:Landroid/graphics/RectF;

.field private p:[F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    const/high16 v1, 0x3f80

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->h:F

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->p:[F

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5

    const/high16 v1, 0x3f80

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->h:F

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->p:[F

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6

    const/high16 v1, 0x3f80

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->h:F

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->p:[F

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->a()V

    return-void
.end method

.method private static a(Landroid/view/MotionEvent;)F
    .registers 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method private a()V
    .registers 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MultiTouchImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method private b()V
    .registers 9

    const/4 v7, 0x0

    const/high16 v6, 0x3f80

    const/high16 v5, 0x4000

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->c()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->left:F

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_2b

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_2b

    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget v3, v1, Landroid/graphics/RectF;->right:F

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_2b

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_83

    :cond_2b
    iget v2, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    iget v3, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v1, v5

    add-float/2addr v1, v3

    iget v3, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v0, v5

    add-float/2addr v0, v4

    sub-float/2addr v2, v3

    sub-float v0, v1, v0

    cmpl-float v1, v2, v7

    if-nez v1, :cond_56

    cmpl-float v1, v0, v7

    if-eqz v1, :cond_60

    :cond_56
    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    invoke-virtual {v1, v2, v0}, Landroid/graphics/RectF;->offset(FF)V

    :cond_60
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_7e

    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    div-float v0, v6, v0

    iput v6, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/twitter/android/widget/MultiTouchImageView;->i:F

    div-float/2addr v2, v5

    iget v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->j:F

    div-float/2addr v3, v5

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_7e
    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MultiTouchImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_83
    return-void
.end method

.method private c()Landroid/graphics/RectF;
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->p:[F

    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 v1, 0x2

    aget v1, v0, v1

    const/4 v2, 0x5

    aget v0, v0, v2

    iget-object v2, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget v4, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v1

    add-float/2addr v3, v0

    iget-object v4, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    invoke-virtual {v4, v1, v0, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    return-object v0
.end method

.method private d()V
    .registers 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->d:F

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->e:F

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->f:F

    iput v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->g:F

    return-void
.end method


# virtual methods
.method public final a(FFLandroid/widget/ImageView$ScaleType;)V
    .registers 13

    const/high16 v8, 0x4000

    const/4 v0, 0x0

    iget v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->i:F

    sub-float v1, v3, p1

    div-float/2addr v1, v8

    iget v4, p0, Lcom/twitter/android/widget/MultiTouchImageView;->j:F

    sub-float v2, v4, p2

    div-float/2addr v2, v8

    iget-object v5, p0, Lcom/twitter/android/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    if-nez v5, :cond_58

    new-instance v5, Landroid/graphics/RectF;

    add-float v6, v1, p1

    add-float v7, v2, p2

    invoke-direct {v5, v1, v2, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lcom/twitter/android/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    :goto_1c
    iget v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->k:F

    iget v5, p0, Lcom/twitter/android/widget/MultiTouchImageView;->l:F

    cmpl-float v2, v1, v0

    if-lez v2, :cond_6e

    cmpl-float v2, v5, v0

    if-lez v2, :cond_6e

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    if-ne p3, v0, :cond_65

    cmpl-float v0, p1, p2

    if-lez v0, :cond_62

    div-float v0, p1, v1

    :goto_32
    mul-float/2addr v1, v0

    sub-float v1, v3, v1

    div-float v2, v1, v8

    mul-float v1, v0, v5

    sub-float v1, v4, v1

    div-float/2addr v1, v8

    iget-object v5, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    invoke-virtual {v5, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0, v5}, Lcom/twitter/android/widget/MultiTouchImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    move v0, v1

    move v1, v2

    :goto_49
    iget-object v2, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    sub-float/2addr v3, v1

    sub-float/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->n:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    return-void

    :cond_58
    iget-object v5, p0, Lcom/twitter/android/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    add-float v6, v1, p1

    add-float v7, v2, p2

    invoke-virtual {v5, v1, v2, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1c

    :cond_62
    div-float v0, p2, v5

    goto :goto_32

    :cond_65
    div-float v0, p1, v1

    div-float v2, p2, v5

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_32

    :cond_6e
    move v1, v0

    goto :goto_49
.end method

.method protected onMeasure(II)V
    .registers 6

    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->i:F

    invoke-virtual {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->j:F

    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->i:F

    iget v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->j:F

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/widget/MultiTouchImageView;->a(FFLandroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_188

    :pswitch_9
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_d
    return v0

    :pswitch_e
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    if-nez v0, :cond_21

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->e:F

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    :cond_21
    :goto_21
    const/4 v0, 0x1

    goto :goto_d

    :pswitch_23
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    if-eqz v0, :cond_2c

    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_21

    :cond_2c
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->f:F

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->g:F

    invoke-static {p1}, Lcom/twitter/android/widget/MultiTouchImageView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->h:F

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    goto :goto_21

    :pswitch_56
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_104

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->d:F

    sub-float v2, v4, v0

    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->e:F

    sub-float v1, v5, v0

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    iget v7, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    iget v8, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    iget v9, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    iget v10, v0, Landroid/graphics/RectF;->left:F

    iget v11, v0, Landroid/graphics/RectF;->top:F

    iget v12, v0, Landroid/graphics/RectF;->right:F

    iget v13, v0, Landroid/graphics/RectF;->bottom:F

    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->e:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_ce

    const/4 v0, 0x1

    :goto_8c
    iget v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->d:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_d0

    const/4 v3, 0x1

    :goto_93
    if-eqz v3, :cond_d8

    cmpl-float v3, v7, v12

    if-lez v3, :cond_d2

    add-float v3, v7, v2

    cmpg-float v3, v3, v12

    if-gez v3, :cond_d2

    sub-float v2, v12, v7

    :cond_a1
    :goto_a1
    if-eqz v0, :cond_f1

    cmpl-float v0, v9, v13

    if-lez v0, :cond_eb

    add-float v0, v9, v1

    cmpg-float v0, v0, v13

    if-gez v0, :cond_eb

    sub-float v0, v13, v9

    :goto_af
    const/4 v1, 0x0

    cmpl-float v1, v2, v1

    if-nez v1, :cond_b9

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_c8

    :cond_b9
    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->o:Landroid/graphics/RectF;

    invoke-virtual {v1, v2, v0}, Landroid/graphics/RectF;->offset(FF)V

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MultiTouchImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_c8
    iput v4, p0, Lcom/twitter/android/widget/MultiTouchImageView;->d:F

    iput v5, p0, Lcom/twitter/android/widget/MultiTouchImageView;->e:F

    goto/16 :goto_21

    :cond_ce
    const/4 v0, 0x0

    goto :goto_8c

    :cond_d0
    const/4 v3, 0x0

    goto :goto_93

    :cond_d2
    cmpg-float v3, v7, v12

    if-gtz v3, :cond_a1

    const/4 v2, 0x0

    goto :goto_a1

    :cond_d8
    cmpg-float v3, v6, v10

    if-gez v3, :cond_e5

    add-float v3, v6, v2

    cmpl-float v3, v3, v10

    if-lez v3, :cond_e5

    sub-float v2, v10, v6

    goto :goto_a1

    :cond_e5
    cmpl-float v3, v6, v10

    if-ltz v3, :cond_a1

    const/4 v2, 0x0

    goto :goto_a1

    :cond_eb
    cmpg-float v0, v9, v13

    if-gtz v0, :cond_184

    const/4 v0, 0x0

    goto :goto_af

    :cond_f1
    cmpg-float v0, v8, v11

    if-gez v0, :cond_fe

    add-float v0, v8, v1

    cmpl-float v0, v0, v11

    if-lez v0, :cond_fe

    sub-float v0, v11, v8

    goto :goto_af

    :cond_fe
    cmpl-float v0, v8, v11

    if-ltz v0, :cond_184

    const/4 v0, 0x0

    goto :goto_af

    :cond_104
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_15a

    invoke-static {p1}, Lcom/twitter/android/widget/MultiTouchImageView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/MultiTouchImageView;->h:F

    div-float v1, v0, v1

    iget v2, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/twitter/android/widget/MultiTouchImageView;->m:F

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->c()Landroid/graphics/RectF;

    move-result-object v2

    iget v3, v2, Landroid/graphics/RectF;->left:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14a

    iget v3, v2, Landroid/graphics/RectF;->top:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14a

    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/twitter/android/widget/MultiTouchImageView;->i:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_14a

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->j:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_14a

    iget-object v2, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->f:F

    iget v4, p0, Lcom/twitter/android/widget/MultiTouchImageView;->g:F

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    :goto_141
    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->h:F

    iget-object v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/MultiTouchImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto/16 :goto_21

    :cond_14a
    iget-object v2, p0, Lcom/twitter/android/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/twitter/android/widget/MultiTouchImageView;->i:F

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    iget v4, p0, Lcom/twitter/android/widget/MultiTouchImageView;->j:F

    const/high16 v5, 0x4000

    div-float/2addr v4, v5

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_141

    :cond_15a
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_d

    :pswitch_160
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_21

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->d()V

    goto/16 :goto_21

    :pswitch_16a
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_172

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->b()V

    :cond_172
    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->d()V

    goto/16 :goto_21

    :pswitch_177
    iget v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_21

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->b()V

    invoke-direct {p0}, Lcom/twitter/android/widget/MultiTouchImageView;->d()V

    goto/16 :goto_21

    :cond_184
    move v0, v1

    goto/16 :goto_af

    nop

    :pswitch_data_188
    .packed-switch 0x0
        :pswitch_e
        :pswitch_160
        :pswitch_56
        :pswitch_16a
        :pswitch_9
        :pswitch_23
        :pswitch_177
    .end packed-switch
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .registers 3

    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    if-eqz p1, :cond_13

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->k:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/twitter/android/widget/MultiTouchImageView;->l:F

    :cond_13
    return-void
.end method
