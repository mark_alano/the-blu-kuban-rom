.class public Lcom/twitter/android/widget/TweetDetailView;
.super Lcom/twitter/android/widget/CardRowView;


# static fields
.field private static final g:Ljava/text/SimpleDateFormat;


# instance fields
.field a:Lcom/twitter/android/provider/m;

.field b:Lcom/twitter/android/widget/an;

.field public c:Landroid/widget/ImageView;

.field public d:Landroid/widget/Button;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/RelativeLayout;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/view/ViewGroup;

.field private p:Lcom/twitter/android/widget/au;

.field private q:Lcom/twitter/android/widget/as;

.field private r:Lcom/twitter/android/api/a;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/widget/TweetDetailView;->g:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/CardRowView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/CardRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/CardRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->a()V

    return-void
.end method

.method private a(ILcom/twitter/android/api/TweetMedia;)Landroid/view/ViewGroup;
    .registers 14

    const/4 v10, 0x1

    const/16 v9, 0x8

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x3

    const v3, 0x7f0700d0

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f070016

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    iget-object v2, p2, Lcom/twitter/android/api/TweetMedia;->authorUser:Lcom/twitter/android/api/TweetMedia$User;

    const v1, 0x7f070078

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v2, :cond_d0

    new-array v3, v10, [Landroid/text/style/StyleSpan;

    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v4, v3, v8

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b01df

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v2, Lcom/twitter/android/api/TweetMedia$User;->fullName:Ljava/lang/String;

    aput-object v7, v6, v8

    iget-object v2, v2, Lcom/twitter/android/api/TweetMedia$User;->screenName:Ljava/lang/String;

    aput-object v2, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x22

    invoke-static {v3, v2, v4}, Lcom/twitter/android/util/z;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_63
    iget-object v2, p2, Lcom/twitter/android/api/TweetMedia;->siteUser:Lcom/twitter/android/api/TweetMedia$User;

    const v1, 0x7f07008e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v2, :cond_d4

    const v1, 0x7f0700ad

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v4, v2, Lcom/twitter/android/api/TweetMedia$User;->fullName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0700ae

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "@"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/twitter/android/api/TweetMedia$User;->screenName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    iget-wide v1, v2, Lcom/twitter/android/api/TweetMedia$User;->userId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_a5
    iput-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/view/View;

    const v1, 0x7f070024

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p2, Lcom/twitter/android/api/TweetMedia;->title:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d8

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_bb
    const v1, 0x7f07008d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p2, Lcom/twitter/android/api/TweetMedia;->description:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e1

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_cf
    return-object v0

    :cond_d0
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_63

    :cond_d4
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_a5

    :cond_d8
    iget-object v2, p2, Lcom/twitter/android/api/TweetMedia;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_bb

    :cond_e1
    iget-object v2, p2, Lcom/twitter/android/api/TweetMedia;->description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_cf
.end method

.method static synthetic a(Lcom/twitter/android/widget/TweetDetailView;)Lcom/twitter/android/api/a;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Lcom/twitter/android/api/a;

    return-object v0
.end method

.method private a()V
    .registers 4

    sget-object v0, Lcom/twitter/android/widget/TweetDetailView;->g:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0015

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/android/provider/m;Landroid/content/res/Resources;)V
    .registers 8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    const v1, 0x7f0b0053

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p2, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/TweetDetailView;)Lcom/twitter/android/widget/as;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Lcom/twitter/android/widget/as;

    return-object v0
.end method

.method private b()V
    .registers 8

    const v6, 0x7f0700d0

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Lcom/twitter/android/widget/au;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Lcom/twitter/android/widget/au;

    iget-object v1, v0, Lcom/twitter/android/widget/au;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_4f

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1e
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    if-eqz v1, :cond_56

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :cond_2b
    :goto_2b
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_5a

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_3f
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Lcom/twitter/android/widget/au;

    if-eqz v1, :cond_63

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Lcom/twitter/android/widget/au;

    iget-object v1, v1, Lcom/twitter/android/widget/au;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_4e
    return-void

    :cond_4f
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    goto :goto_1e

    :cond_56
    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_2b

    :cond_5a
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    goto :goto_3f

    :cond_63
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    if-eqz v1, :cond_71

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_4e

    :cond_71
    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_4e
.end method


# virtual methods
.method public final a(Lcom/twitter/android/api/TweetMedia;Lcom/twitter/android/client/b;)V
    .registers 11

    const v7, 0x7f020169

    const v6, 0x7f0a001c

    const v4, 0x7f070023

    const/4 v5, 0x0

    if-nez p1, :cond_d

    :cond_c
    :goto_c
    return-void

    :cond_d
    iget v0, p1, Lcom/twitter/android/api/TweetMedia;->type:I

    packed-switch v0, :pswitch_data_162

    :cond_12
    :goto_12
    iget-object v0, p1, Lcom/twitter/android/api/TweetMedia;->siteUser:Lcom/twitter/android/api/TweetMedia$User;

    if-eqz v0, :cond_c

    const/4 v1, 0x2

    iget-wide v2, v0, Lcom/twitter/android/api/TweetMedia$User;->userId:J

    iget-object v0, v0, Lcom/twitter/android/api/TweetMedia$User;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3, v0}, Lcom/twitter/android/client/b;->a(IJLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    const v2, 0x7f0700ac

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_15c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_c

    :pswitch_30
    iget v0, p2, Lcom/twitter/android/client/b;->g:F

    invoke-virtual {p1, v0}, Lcom/twitter/android/api/TweetMedia;->a(F)Lcom/twitter/android/api/m;

    move-result-object v1

    iget-object v2, v1, Lcom/twitter/android/api/m;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    if-nez v0, :cond_80

    const v0, 0x7f03003d

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/android/api/TweetMedia;)Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/OverlayImageView;

    iget v4, v1, Lcom/twitter/android/api/m;->b:I

    iget v1, v1, Lcom/twitter/android/api/m;->c:I

    invoke-virtual {v0, v4, v1}, Lcom/twitter/android/widget/OverlayImageView;->a(II)V

    new-instance v1, Lcom/twitter/android/widget/ao;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/widget/ao;-><init>(Lcom/twitter/android/widget/TweetDetailView;Lcom/twitter/android/api/TweetMedia;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/OverlayImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iput-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    :goto_68
    new-instance v1, Lcom/twitter/android/util/f;

    invoke-direct {v1, v2}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_87

    const v2, 0x7f020164

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/OverlayImageView;->a(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/OverlayImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/OverlayImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_12

    :cond_80
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/OverlayImageView;

    goto :goto_68

    :cond_87
    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/OverlayImageView;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/OverlayImageView;->setBackgroundResource(I)V

    goto :goto_12

    :pswitch_91
    iget v0, p2, Lcom/twitter/android/client/b;->g:F

    invoke-virtual {p1, v0}, Lcom/twitter/android/api/TweetMedia;->a(F)Lcom/twitter/android/api/m;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    if-nez v0, :cond_dc

    const v0, 0x7f03003e

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/android/api/TweetMedia;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/OverlayImageView;

    iget v3, v1, Lcom/twitter/android/api/m;->b:I

    iget v4, v1, Lcom/twitter/android/api/m;->c:I

    invoke-virtual {v0, v3, v4}, Lcom/twitter/android/widget/OverlayImageView;->a(II)V

    new-instance v3, Lcom/twitter/android/widget/ap;

    invoke-direct {v3, p0, p1}, Lcom/twitter/android/widget/ap;-><init>(Lcom/twitter/android/widget/TweetDetailView;Lcom/twitter/android/api/TweetMedia;)V

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/OverlayImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    :goto_c1
    iget-object v1, v1, Lcom/twitter/android/api/m;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e3

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/OverlayImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v0, v7}, Lcom/twitter/android/widget/OverlayImageView;->a(I)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f0a001d

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/OverlayImageView;->setBackgroundResource(I)V

    goto/16 :goto_12

    :cond_dc
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/OverlayImageView;

    goto :goto_c1

    :cond_e3
    new-instance v2, Lcom/twitter/android/util/f;

    invoke-direct {v2, v1}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_f9

    invoke-virtual {v0, v7}, Lcom/twitter/android/widget/OverlayImageView;->a(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/OverlayImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/OverlayImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_12

    :cond_f9
    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/OverlayImageView;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/OverlayImageView;->setBackgroundResource(I)V

    goto/16 :goto_12

    :pswitch_104
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    iget v1, p2, Lcom/twitter/android/client/b;->g:F

    invoke-virtual {p1, v1}, Lcom/twitter/android/api/TweetMedia;->a(F)Lcom/twitter/android/api/m;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/android/api/m;->a:Ljava/lang/String;

    if-nez v0, :cond_155

    const v0, 0x7f030059

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/android/api/TweetMedia;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_151

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_128
    iget-object v3, p1, Lcom/twitter/android/api/TweetMedia;->url:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_138

    new-instance v3, Lcom/twitter/android/widget/aq;

    invoke-direct {v3, p0, p1}, Lcom/twitter/android/widget/aq;-><init>(Lcom/twitter/android/widget/TweetDetailView;Lcom/twitter/android/api/TweetMedia;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_138
    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Landroid/view/ViewGroup;

    :goto_13f
    if-eqz v0, :cond_12

    new-instance v2, Lcom/twitter/android/util/f;

    invoke-direct {v2, v1}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_12

    :cond_151
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    goto :goto_128

    :cond_155
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_13f

    :cond_15c
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_c

    nop

    :pswitch_data_162
    .packed-switch 0x1
        :pswitch_30
        :pswitch_91
        :pswitch_104
    .end packed-switch
.end method

.method public final a(Lcom/twitter/android/api/a;Lcom/twitter/android/widget/as;)V
    .registers 7

    iget-object v0, p1, Lcom/twitter/android/api/a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/api/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p1, Lcom/twitter/android/api/a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/api/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    :goto_10
    return-void

    :cond_11
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Lcom/twitter/android/api/a;

    iput-object p2, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Lcom/twitter/android/widget/as;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Lcom/twitter/android/widget/au;

    if-nez v0, :cond_35

    new-instance v0, Lcom/twitter/android/widget/au;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030063

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/ar;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/ar;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/au;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Lcom/twitter/android/widget/au;

    :cond_35
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->p:Lcom/twitter/android/widget/au;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/au;->a(Lcom/twitter/android/api/a;)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->b()V

    goto :goto_10
.end method

.method public final a(Lcom/twitter/android/client/b;)V
    .registers 6

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/android/provider/m;

    if-nez v1, :cond_5

    :goto_4
    return-void

    :cond_5
    const/4 v0, 0x0

    iget-object v2, v1, Lcom/twitter/android/provider/m;->k:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget v0, v1, Lcom/twitter/android/provider/m;->t:I

    iget-wide v2, v1, Lcom/twitter/android/provider/m;->n:J

    iget-object v1, v1, Lcom/twitter/android/provider/m;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v2, v3, v1}, Lcom/twitter/android/client/b;->a(IJLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_14
    if-eqz v0, :cond_1c

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_4

    :cond_1c
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/widget/ImageView;

    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4
.end method

.method public final a(Lcom/twitter/android/provider/m;Lcom/twitter/android/client/b;Lcom/twitter/android/widget/an;Lcom/twitter/android/api/TweetEntities;)V
    .registers 14

    const v8, 0x7f0b0109

    const/4 v5, 0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Lcom/twitter/android/provider/m;

    iput-object p3, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/android/widget/an;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->h:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/twitter/android/provider/m;->d:Ljava/lang/String;

    move-object v3, p4

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/widget/at;->a(Landroid/content/res/Resources;Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;Lcom/twitter/android/widget/an;Z)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "@"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->i:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/twitter/android/provider/m;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/widget/Button;

    invoke-virtual {p2}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/twitter/android/provider/m;->a(J)Z

    move-result v2

    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->i()Z

    move-result v3

    if-eqz v3, :cond_a9

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->r:Z

    if-eqz v1, :cond_51

    if-nez v2, :cond_51

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/provider/m;Landroid/content/res/Resources;)V

    :cond_51
    :goto_51
    invoke-virtual {p0, p2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/b;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/widget/ImageView;

    iget-wide v2, p1, Lcom/twitter/android/provider/m;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->B:Z

    if-eqz v1, :cond_107

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/ImageView;

    const v2, 0x7f020163

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_70
    sget-object v1, Lcom/twitter/android/widget/TweetDetailView;->g:Ljava/text/SimpleDateFormat;

    iget-wide v2, p1, Lcom/twitter/android/provider/m;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/provider/m;->D:Lcom/twitter/android/api/v;

    if-eqz v2, :cond_121

    iget-object v3, v2, Lcom/twitter/android/api/v;->b:Ljava/lang/String;

    if-eqz v3, :cond_121

    iget-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    const v4, 0x7f0b0060

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v7

    iget-object v1, v2, Lcom/twitter/android/api/v;->b:Ljava/lang/String;

    aput-object v1, v6, v5

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_99
    iget-object v0, p1, Lcom/twitter/android/provider/m;->M:[Lcom/twitter/android/api/TweetMedia;

    if-eqz v0, :cond_a5

    array-length v1, v0

    if-lez v1, :cond_a5

    aget-object v0, v0, v7

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/api/TweetMedia;Lcom/twitter/android/client/b;)V

    :cond_a5
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->b()V

    return-void

    :cond_a9
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->h()Z

    move-result v3

    if-eqz v3, :cond_cd

    if-nez v2, :cond_cd

    const v2, 0x7f0200aa

    invoke-virtual {v1, v2, v7, v7, v7}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/twitter/android/provider/m;->f:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v8, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_51

    :cond_cd
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->g()Z

    move-result v3

    if-eqz v3, :cond_f2

    if-nez v2, :cond_f2

    const v2, 0x7f0200ad

    invoke-virtual {v1, v2, v7, v7, v7}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/twitter/android/provider/m;->f:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v8, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_51

    :cond_f2
    iget-boolean v3, p1, Lcom/twitter/android/provider/m;->r:Z

    if-eqz v3, :cond_fd

    if-nez v2, :cond_fd

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/provider/m;Landroid/content/res/Resources;)V

    goto/16 :goto_51

    :cond_fd
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_51

    :cond_107
    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->m:Z

    if-eqz v1, :cond_11a

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/ImageView;

    const v2, 0x7f020101

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_70

    :cond_11a
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_70

    :cond_121
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_99
.end method

.method protected onFinishInflate()V
    .registers 5

    const v0, 0x7f0700cf

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const v1, 0x7f07006c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Landroid/widget/ImageView;

    const v1, 0x7f0700d6

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Landroid/widget/TextView;

    const v1, 0x7f070028

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->i:Landroid/widget/TextView;

    const v1, 0x7f07007a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/widget/ImageView;

    const v0, 0x7f070052

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->h:Landroid/widget/TextView;

    const v0, 0x7f07001f

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Landroid/widget/TextView;

    const v0, 0x7f0700d2

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    const v0, 0x7f070030

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v0, 0x7f0700d1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    return-void
.end method
