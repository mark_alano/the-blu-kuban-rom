.class public Lcom/twitter/android/widget/UserApprovalView;
.super Lcom/twitter/android/widget/BaseUserView;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final c:[I

.field private static final d:[I


# instance fields
.field private e:Lcom/twitter/android/widget/a;

.field private f:[Lcom/twitter/android/widget/ba;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/android/widget/UserApprovalView;->c:[I

    new-array v0, v3, [I

    sget v1, Lcom/twitter/android/eb;->state_checked_deny:I

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/android/widget/UserApprovalView;->d:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/widget/ba;

    iput-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    return-void
.end method

.method private c(I)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    iget-boolean v0, v0, Lcom/twitter/android/widget/ba;->c:Z

    if-nez v0, :cond_13

    const/4 v0, 0x1

    :goto_f
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/widget/UserApprovalView;->a(IZ)V

    :cond_12
    return-void

    :cond_13
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method public final a(IIIIII)V
    .registers 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/twitter/android/widget/ba;->a:Landroid/widget/ImageButton;

    if-lez p2, :cond_f

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    invoke-virtual {v0, p3, v1, p5, v1}, Landroid/widget/ImageButton;->setPadding(IIII)V

    :cond_f
    return-void
.end method

.method public final a(IILcom/twitter/android/widget/a;)V
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/twitter/android/widget/ba;->a:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/twitter/android/widget/ba;->b:Landroid/widget/FrameLayout;

    if-nez p2, :cond_14

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_13
    return-void

    :cond_14
    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setImageResource(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {v1, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p3, p0, Lcom/twitter/android/widget/UserApprovalView;->e:Lcom/twitter/android/widget/a;

    goto :goto_13
.end method

.method public final a(IZ)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    if-nez v0, :cond_7

    :goto_6
    return-void

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    iput-boolean p2, v0, Lcom/twitter/android/widget/ba;->c:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserApprovalView;->refreshDrawableState()V

    goto :goto_6
.end method

.method public final b(I)Z
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    iget-boolean v0, v0, Lcom/twitter/android/widget/ba;->c:Z

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final c(II)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/twitter/android/widget/ba;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void
.end method

.method protected drawableStateChanged()V
    .registers 7

    invoke-super {p0}, Lcom/twitter/android/widget/BaseUserView;->drawableStateChanged()V

    iget-object v1, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v2, :cond_18

    aget-object v3, v1, v0

    iget-object v3, v3, Lcom/twitter/android/widget/ba;->a:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserApprovalView;->getDrawableState()[I

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/widget/ImageButton;->setImageState([IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_18
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->e:Lcom/twitter/android/widget/a;

    if-eqz v0, :cond_27

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/twitter/android/eg;->action_button_deny:I

    if-eq v0, v1, :cond_12

    sget v1, Lcom/twitter/android/eg;->action_button_deny_frame:I

    if-ne v0, v1, :cond_28

    :cond_12
    invoke-direct {p0, v3}, Lcom/twitter/android/widget/UserApprovalView;->c(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, v2

    iget-boolean v0, v0, Lcom/twitter/android/widget/ba;->c:Z

    if-eqz v0, :cond_20

    invoke-direct {p0, v2}, Lcom/twitter/android/widget/UserApprovalView;->c(I)V

    :cond_20
    :goto_20
    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->e:Lcom/twitter/android/widget/a;

    iget-wide v1, p0, Lcom/twitter/android/widget/UserApprovalView;->a:J

    invoke-interface {v0, p0, v1, v2}, Lcom/twitter/android/widget/a;->a(Lcom/twitter/android/widget/BaseUserView;J)V

    :cond_27
    return-void

    :cond_28
    sget v1, Lcom/twitter/android/eg;->action_button:I

    if-eq v0, v1, :cond_30

    sget v1, Lcom/twitter/android/eg;->action_button_frame:I

    if-ne v0, v1, :cond_20

    :cond_30
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/UserApprovalView;->c(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    aget-object v0, v0, v3

    iget-boolean v0, v0, Lcom/twitter/android/widget/ba;->c:Z

    if-eqz v0, :cond_20

    invoke-direct {p0, v3}, Lcom/twitter/android/widget/UserApprovalView;->c(I)V

    goto :goto_20
.end method

.method public onCreateDrawableState(I)[I
    .registers 4

    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->onCreateDrawableState(I)[I

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/UserApprovalView;->b(I)Z

    move-result v1

    if-eqz v1, :cond_13

    sget-object v1, Lcom/twitter/android/widget/UserApprovalView;->c:[I

    invoke-static {v0, v1}, Lcom/twitter/android/widget/UserApprovalView;->mergeDrawableStates([I[I)[I

    :cond_12
    :goto_12
    return-object v0

    :cond_13
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/UserApprovalView;->b(I)Z

    move-result v1

    if-eqz v1, :cond_12

    sget-object v1, Lcom/twitter/android/widget/UserApprovalView;->d:[I

    invoke-static {v0, v1}, Lcom/twitter/android/widget/UserApprovalView;->mergeDrawableStates([I[I)[I

    goto :goto_12
.end method

.method protected onFinishInflate()V
    .registers 6

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/twitter/android/widget/BaseUserView;->onFinishInflate()V

    sget v0, Lcom/twitter/android/eg;->action_button_frame:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    new-instance v3, Lcom/twitter/android/widget/ba;

    invoke-direct {v3, p0, v1, v0}, Lcom/twitter/android/widget/ba;-><init>(Lcom/twitter/android/widget/UserApprovalView;Landroid/widget/ImageButton;Landroid/widget/FrameLayout;)V

    aput-object v3, v2, v4

    sget v0, Lcom/twitter/android/eg;->action_button_deny_frame:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/widget/UserApprovalView;->f:[Lcom/twitter/android/widget/ba;

    const/4 v3, 0x1

    new-instance v4, Lcom/twitter/android/widget/ba;

    invoke-direct {v4, p0, v1, v0}, Lcom/twitter/android/widget/ba;-><init>(Lcom/twitter/android/widget/UserApprovalView;Landroid/widget/ImageButton;Landroid/widget/FrameLayout;)V

    aput-object v4, v2, v3

    return-void
.end method
