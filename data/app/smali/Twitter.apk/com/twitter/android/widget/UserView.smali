.class public Lcom/twitter/android/widget/UserView;
.super Lcom/twitter/android/widget/BaseUserView;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;


# static fields
.field private static final e:[I


# instance fields
.field protected c:Lcom/twitter/android/widget/a;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/android/widget/UserView;->e:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final a(IIIII)V
    .registers 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    if-nez v0, :cond_6

    :goto_5
    return-void

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    const v1, 0x7f02004a

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p2, v2, p4, v2}, Landroid/widget/ImageButton;->setPadding(IIII)V

    goto :goto_5
.end method

.method public final a(ILcom/twitter/android/widget/a;)V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    if-nez v0, :cond_5

    :goto_4
    return-void

    :cond_5
    if-nez p1, :cond_f

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_4

    :cond_f
    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iput-object p2, p0, Lcom/twitter/android/widget/UserView;->c:Lcom/twitter/android/widget/a;

    goto :goto_4
.end method

.method public final b(I)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_9
    return-void
.end method

.method public final c(Z)V
    .registers 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    if-nez v0, :cond_6

    :goto_5
    return-void

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    sget v1, Lcom/twitter/android/ef;->btn_check:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    goto :goto_5
.end method

.method protected drawableStateChanged()V
    .registers 4

    invoke-super {p0}, Lcom/twitter/android/widget/BaseUserView;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->b:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageButton;->setImageState([IZ)V

    :cond_11
    return-void
.end method

.method public isChecked()Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserView;->d:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->c:Lcom/twitter/android/widget/a;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/widget/UserView;->c:Lcom/twitter/android/widget/a;

    iget-wide v1, p0, Lcom/twitter/android/widget/UserView;->a:J

    invoke-interface {v0, p0, v1, v2}, Lcom/twitter/android/widget/a;->a(Lcom/twitter/android/widget/BaseUserView;J)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserView;->toggle()V

    :cond_e
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .registers 4

    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->onCreateDrawableState(I)[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_11

    sget-object v1, Lcom/twitter/android/widget/UserView;->e:[I

    invoke-static {v0, v1}, Lcom/twitter/android/widget/UserView;->mergeDrawableStates([I[I)[I

    :cond_11
    return-object v0
.end method

.method public setChecked(Z)V
    .registers 3

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserView;->d:Z

    if-eq v0, p1, :cond_9

    iput-boolean p1, p0, Lcom/twitter/android/widget/UserView;->d:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserView;->refreshDrawableState()V

    :cond_9
    return-void
.end method

.method public toggle()V
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserView;->d:Z

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserView;->setChecked(Z)V

    return-void

    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method
