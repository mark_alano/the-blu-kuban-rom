.class public final Lcom/twitter/android/widget/at;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/res/Resources;Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;Lcom/twitter/android/widget/an;Z)V
    .registers 16

    const/16 v9, 0x21

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0006

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-eqz p5, :cond_46

    const v0, 0x7f0a0011

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_17
    sget-object v2, Lcom/twitter/android/util/n;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    :goto_1d
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_48

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    if-eqz p5, :cond_37

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v4, v6, v3, v7, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_37
    new-instance v6, Lcom/twitter/android/widget/b;

    invoke-virtual {p2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v1, v7, p4}, Lcom/twitter/android/widget/b;-><init>(ILjava/lang/String;Lcom/twitter/android/widget/an;)V

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v4, v6, v3, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1d

    :cond_46
    move v0, v1

    goto :goto_17

    :cond_48
    if-eqz p3, :cond_56

    iget-object v0, p3, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_56

    iget-object v0, p3, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7e

    :cond_56
    sget-object v0, Lcom/twitter/android/util/n;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    :goto_5c
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_bd

    new-instance v2, Lcom/twitter/android/api/TweetEntities$Url;

    invoke-direct {v2}, Lcom/twitter/android/api/TweetEntities$Url;-><init>()V

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/android/api/TweetEntities$Url;->url:Ljava/lang/String;

    new-instance v3, Lcom/twitter/android/widget/c;

    invoke-direct {v3, v1, p4, v2}, Lcom/twitter/android/widget/c;-><init>(ILcom/twitter/android/widget/an;Lcom/twitter/android/api/TweetEntities$Url;)V

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    invoke-virtual {v4, v3, v2, v5, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_5c

    :cond_7e
    const/4 v3, 0x0

    iget-object v0, p3, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_85
    :goto_85
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_bd

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetEntities$Url;

    iget v2, v0, Lcom/twitter/android/api/TweetEntities$Url;->start:I

    sub-int v6, v2, v3

    iget v2, v0, Lcom/twitter/android/api/TweetEntities$Url;->end:I

    sub-int/2addr v2, v3

    if-ltz v6, :cond_85

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-gt v2, v7, :cond_85

    iget-object v7, v0, Lcom/twitter/android/api/TweetEntities$Url;->displayUrl:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b4

    invoke-virtual {v4, v6, v2, v7}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v6

    sub-int v7, v2, v7

    sub-int/2addr v2, v7

    add-int/2addr v3, v7

    :cond_b4
    new-instance v7, Lcom/twitter/android/widget/c;

    invoke-direct {v7, v1, p4, v0}, Lcom/twitter/android/widget/c;-><init>(ILcom/twitter/android/widget/an;Lcom/twitter/android/api/TweetEntities$Url;)V

    invoke-virtual {v4, v7, v6, v2, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_85

    :cond_bd
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static a(Lcom/twitter/android/client/Session;Lcom/twitter/android/provider/m;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)V
    .registers 12

    const v4, 0x7f020093

    const v5, 0x7f02008f

    const v6, 0x7f020082

    const v7, 0x7f02007e

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/widget/at;->a(Lcom/twitter/android/client/Session;Lcom/twitter/android/provider/m;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;IIII)V

    return-void
.end method

.method public static a(Lcom/twitter/android/client/Session;Lcom/twitter/android/provider/m;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;IIII)V
    .registers 16

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v2, v0

    :goto_6
    if-ge v2, v3, :cond_94

    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iget-wide v4, p1, Lcom/twitter/android/provider/m;->q:J

    invoke-virtual {p0}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_27

    const/4 v1, 0x1

    :goto_19
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getId()I

    move-result v4

    sparse-switch v4, :sswitch_data_96

    :goto_20
    invoke-virtual {v0, p3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_27
    const/4 v1, 0x0

    goto :goto_19

    :sswitch_29
    if-eqz v1, :cond_3c

    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->r:Z

    if-eqz v1, :cond_37

    invoke-virtual {v0, p4}, Landroid/widget/ImageButton;->setImageResource(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_20

    :cond_37
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_20

    :cond_3c
    iget-wide v4, p1, Lcom/twitter/android/provider/m;->E:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_55

    move v1, p4

    :goto_45
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->m:Z

    if-nez v1, :cond_57

    const/4 v1, 0x1

    :goto_51
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_20

    :cond_55
    move v1, p5

    goto :goto_45

    :cond_57
    const/4 v1, 0x0

    goto :goto_51

    :sswitch_59
    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->l:Z

    if-eqz v1, :cond_61

    invoke-virtual {v0, p6}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_20

    :cond_61
    invoke-virtual {v0, p7}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_20

    :sswitch_65
    if-nez v1, :cond_6b

    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->m:Z

    if-nez v1, :cond_70

    :cond_6b
    const/4 v1, 0x1

    :goto_6c
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_20

    :cond_70
    const/4 v1, 0x0

    goto :goto_6c

    :sswitch_72
    if-eqz v1, :cond_7d

    iget-boolean v1, p1, Lcom/twitter/android/provider/m;->r:Z

    if-nez v1, :cond_7d

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_20

    :cond_7d
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_20

    :sswitch_83
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->k()Z

    move-result v1

    if-eqz v1, :cond_8e

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_20

    :cond_8e
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_20

    :cond_94
    return-void

    nop

    :sswitch_data_96
    .sparse-switch
        0x7f07001f -> :sswitch_29
        0x7f070020 -> :sswitch_59
        0x7f070021 -> :sswitch_65
        0x7f070022 -> :sswitch_72
        0x7f0700cc -> :sswitch_83
    .end sparse-switch
.end method
