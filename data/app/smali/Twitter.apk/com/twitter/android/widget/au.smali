.class public final Lcom/twitter/android/widget/au;
.super Ljava/lang/Object;


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Lcom/twitter/android/widget/TweetStatView;

.field public final c:Lcom/twitter/android/widget/TweetStatView;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/widget/au;->a:Landroid/view/View;

    const v0, 0x7f07008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/au;->b:Lcom/twitter/android/widget/TweetStatView;

    const v0, 0x7f07008b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/au;->c:Lcom/twitter/android/widget/TweetStatView;

    return-void
.end method

.method private static a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;)V
    .registers 3

    invoke-static {p1}, Lcom/twitter/android/api/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/TweetStatView;->a(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V

    :goto_d
    return-void

    :cond_e
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V

    goto :goto_d
.end method


# virtual methods
.method public final a(Lcom/twitter/android/api/a;)V
    .registers 4

    if-eqz p1, :cond_12

    iget-object v0, p1, Lcom/twitter/android/api/a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/api/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p1, Lcom/twitter/android/api/a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/api/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_12
    iget-object v0, p0, Lcom/twitter/android/widget/au;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_19
    return-void

    :cond_1a
    iget-object v0, p0, Lcom/twitter/android/widget/au;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/au;->b:Lcom/twitter/android/widget/TweetStatView;

    iget-object v1, p1, Lcom/twitter/android/api/a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/widget/au;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/widget/au;->c:Lcom/twitter/android/widget/TweetStatView;

    iget-object v1, p1, Lcom/twitter/android/api/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/widget/au;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;)V

    goto :goto_19
.end method
