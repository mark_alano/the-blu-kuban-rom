.class final Lcom/twitter/android/widget/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:I

.field final synthetic b:Lcom/twitter/android/widget/HorizontalListView;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/HorizontalListView;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/android/widget/HorizontalListView;->e(Lcom/twitter/android/widget/HorizontalListView;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/j;->c:I

    return-void
.end method

.method public final run()V
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    iget-boolean v0, v0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    if-eqz v0, :cond_7

    :cond_6
    :goto_6
    return-void

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    iget-object v1, v0, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    iget v2, p0, Lcom/twitter/android/widget/j;->a:I

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    iget v0, v0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    if-lez v0, :cond_6

    const/4 v0, -0x1

    if-eq v2, v0, :cond_6

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/android/widget/HorizontalListView;->f(Lcom/twitter/android/widget/HorizontalListView;)I

    move-result v0

    iget v3, p0, Lcom/twitter/android/widget/j;->c:I

    if-ne v0, v3, :cond_4d

    const/4 v0, 0x1

    :goto_31
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    iget-object v3, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v3}, Lcom/twitter/android/widget/HorizontalListView;->b(Lcom/twitter/android/widget/HorizontalListView;)I

    move-result v3

    sub-int v3, v2, v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/twitter/android/widget/j;->b:Lcom/twitter/android/widget/HorizontalListView;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v3, v0, v2, v4, v5}, Lcom/twitter/android/widget/HorizontalListView;->performItemClick(Landroid/view/View;IJ)Z

    goto :goto_6

    :cond_4d
    const/4 v0, 0x0

    goto :goto_31
.end method
