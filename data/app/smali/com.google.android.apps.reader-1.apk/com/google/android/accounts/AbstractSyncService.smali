.class public abstract Lcom/google/android/accounts/AbstractSyncService;
.super Lcom/google/android/accounts/IntentService;
.source "AbstractSyncService.java"


# static fields
.field static final ACTION_REQUEST_SYNC:Ljava/lang/String; = "android.content.SyncAdapter"

.field private static final ECLAIR:I = 0x5

.field static final EXTRA_ACCOUNT_NAME:Ljava/lang/String; = "com.google.android.accounts.intent.extra.ACCOUNT_NAME"

.field static final EXTRA_ACCOUNT_TYPE:Ljava/lang/String; = "com.google.android.accounts.intent.extra.ACCOUNT_TYPE"

.field static final EXTRA_AUTHORITY:Ljava/lang/String; = "com.google.android.accounts.intent.extra.AUTHORITY"

.field static final EXTRA_BUNDLE:Ljava/lang/String; = "com.google.android.accounts.intent.extra.BUNDLE"

.field static final EXTRA_SUPPORTS_UPLOADING:Ljava/lang/String; = "com.google.android.accounts.intent.extra.SUPPORTS_UPLOADING"

.field private static final SDK:I


# instance fields
.field private final mNotificationId:I

.field private final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 68
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/accounts/AbstractSyncService;->SDK:I

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter "name"
    .parameter "priority"
    .parameter "notificationId"

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/android/accounts/IntentService;-><init>(Ljava/lang/String;I)V

    .line 86
    iput-object p1, p0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    .line 87
    iput p3, p0, Lcom/google/android/accounts/AbstractSyncService;->mNotificationId:I

    .line 88
    return-void
.end method

.method private createNotification(Ljava/lang/String;)Landroid/app/Notification;
    .registers 13
    .parameter "authority"

    .prologue
    .line 222
    const v4, 0x108007c

    .line 223
    .local v4, icon:I
    const/4 v6, 0x0

    .line 224
    .local v6, tickerText:Ljava/lang/String;
    const-wide/16 v7, 0x0

    .line 225
    .local v7, when:J
    new-instance v5, Landroid/app/Notification;

    invoke-direct {v5, v4, v6, v7, v8}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 226
    .local v5, notification:Landroid/app/Notification;
    move-object v3, p0

    .line 227
    .local v3, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/accounts/AbstractSyncService;->createNotificationTitle()Ljava/lang/CharSequence;

    move-result-object v2

    .line 228
    .local v2, contentTitle:Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/google/android/accounts/AbstractSyncService;->createNotificationText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 229
    .local v1, contentText:Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/google/android/accounts/AbstractSyncService;->createNotificationIntent()Landroid/app/PendingIntent;

    move-result-object v0

    .line 230
    .local v0, contentIntent:Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iput-wide v9, v5, Landroid/app/Notification;->when:J

    .line 231
    iget v9, v5, Landroid/app/Notification;->flags:I

    or-int/lit8 v9, v9, 0x2

    iput v9, v5, Landroid/app/Notification;->flags:I

    .line 232
    invoke-virtual {v5, v3, v2, v1, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 233
    return-object v5
.end method

.method private isSyncEnabled()Z
    .registers 2

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method private performSync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .registers 23
    .parameter "accountName"
    .parameter "accountType"
    .parameter "authority"
    .parameter "supportsUploading"
    .parameter "extras"

    .prologue
    .line 149
    if-eqz p1, :cond_4

    if-nez p2, :cond_e

    .line 150
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    const-string v16, "not syncing because account was not specified"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :goto_d
    return-void

    .line 153
    :cond_e
    if-nez p3, :cond_1a

    .line 154
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    const-string v16, "not syncing because authority was not specified"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 157
    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/accounts/AbstractSyncService;->isSyncEnabled()Z

    move-result v15

    if-nez v15, :cond_25

    .line 158
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    goto :goto_d

    .line 162
    :cond_25
    const-string v15, "connectivity"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/accounts/AbstractSyncService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    .local v10, service:Ljava/lang/Object;
    move-object v4, v10

    .line 163
    check-cast v4, Landroid/net/ConnectivityManager;

    .line 164
    .local v4, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v3

    .line 166
    .local v3, backgroundDataUsageAllowed:Z
    if-nez p5, :cond_3b

    .line 167
    new-instance p5, Landroid/os/Bundle;

    .end local p5
    invoke-direct/range {p5 .. p5}, Landroid/os/Bundle;-><init>()V

    .line 170
    .restart local p5
    :cond_3b
    const-string v15, "upload"

    const/16 v16, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    .line 171
    .local v14, uploadOnly:Z
    const-string v15, "force"

    const/16 v16, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 173
    .local v9, manualSync:Z
    move-object/from16 v5, p0

    .line 174
    .local v5, context:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/accounts/ContentSyncer;->get(Landroid/content/Context;)Lcom/google/android/accounts/ContentSyncer;

    move-result-object v6

    .line 175
    .local v6, cs:Lcom/google/android/accounts/ContentSyncer;
    new-instance v2, Lcom/google/android/accounts/Account;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .local v2, account:Lcom/google/android/accounts/Account;
    move-object/from16 v0, p3

    invoke-virtual {v6, v2, v0}, Lcom/google/android/accounts/ContentSyncer;->getIsSyncable(Lcom/google/android/accounts/Account;Ljava/lang/String;)I

    move-result v8

    .line 177
    .local v8, isSyncable:I
    if-nez v8, :cond_6f

    .line 178
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    goto :goto_d

    .line 182
    :cond_6f
    if-nez p4, :cond_78

    if-eqz v14, :cond_78

    .line 183
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    goto :goto_d

    .line 187
    :cond_78
    move-object/from16 v0, p3

    invoke-virtual {v6, v2, v0}, Lcom/google/android/accounts/ContentSyncer;->getSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v13

    .line 188
    .local v13, syncAutomatically:Z
    if-nez v9, :cond_84

    if-eqz v3, :cond_c1

    if-eqz v13, :cond_c1

    :cond_84
    const/4 v12, 0x1

    .line 190
    .local v12, syncAllowed:Z
    :goto_85
    const/4 v7, 0x0

    .line 191
    .local v7, extrasCopy:Landroid/os/Bundle;
    if-gez v8, :cond_c3

    .line 192
    new-instance v7, Landroid/os/Bundle;

    .end local v7           #extrasCopy:Landroid/os/Bundle;
    move-object/from16 v0, p5

    invoke-direct {v7, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 195
    .restart local v7       #extrasCopy:Landroid/os/Bundle;
    const-string v15, "initialize"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 212
    :cond_98
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mNotificationId:I

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/accounts/AbstractSyncService;->createNotification(Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/google/android/accounts/AbstractSyncService;->startForegroundCompat(ILandroid/app/Notification;)V

    .line 214
    :try_start_ab
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/accounts/AbstractSyncService;->createSyncAdapter()Lcom/google/android/accounts/AbstractSyncAdapter;

    move-result-object v11

    .line 215
    .local v11, syncAdapter:Lcom/google/android/accounts/AbstractSyncAdapter;
    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-virtual {v11, v2, v0, v1}, Lcom/google/android/accounts/AbstractSyncAdapter;->onPerformSync(Lcom/google/android/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;)V
    :try_end_b6
    .catchall {:try_start_ab .. :try_end_b6} :catchall_101

    .line 217
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mNotificationId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/accounts/AbstractSyncService;->stopForegroundCompat(I)V

    goto/16 :goto_d

    .line 188
    .end local v7           #extrasCopy:Landroid/os/Bundle;
    .end local v11           #syncAdapter:Lcom/google/android/accounts/AbstractSyncAdapter;
    .end local v12           #syncAllowed:Z
    :cond_c1
    const/4 v12, 0x0

    goto :goto_85

    .line 198
    .restart local v7       #extrasCopy:Landroid/os/Bundle;
    .restart local v12       #syncAllowed:Z
    :cond_c3
    if-nez v12, :cond_98

    .line 199
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    const/16 v16, 0x3

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_fb

    .line 200
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "sync of "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " is not allowed, dropping request"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_d

    .line 203
    :cond_fb
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/AbstractSyncService;->mTag:Ljava/lang/String;

    goto/16 :goto_d

    .line 217
    :catchall_101
    move-exception v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/accounts/AbstractSyncService;->mNotificationId:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/accounts/AbstractSyncService;->stopForegroundCompat(I)V

    throw v15
.end method


# virtual methods
.method protected createNotificationIntent()Landroid/app/PendingIntent;
    .registers 6

    .prologue
    .line 120
    move-object v0, p0

    .line 121
    .local v0, context:Landroid/content/Context;
    const/4 v3, 0x0

    .line 122
    .local v3, requestCode:I
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 123
    .local v2, intent:Landroid/content/Intent;
    const/4 v1, 0x0

    .line 124
    .local v1, flags:I
    invoke-static {v0, v3, v2, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    return-object v4
.end method

.method protected abstract createNotificationText()Ljava/lang/CharSequence;
.end method

.method protected abstract createNotificationTitle()Ljava/lang/CharSequence;
.end method

.method protected abstract createSyncAdapter()Lcom/google/android/accounts/AbstractSyncAdapter;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 6
    .parameter "intent"

    .prologue
    .line 244
    sget v2, Lcom/google/android/accounts/AbstractSyncService;->SDK:I

    const/4 v3, 0x5

    if-lt v2, v3, :cond_14

    .line 245
    move-object v1, p0

    .line 246
    .local v1, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/accounts/AbstractSyncService;->createSyncAdapter()Lcom/google/android/accounts/AbstractSyncAdapter;

    move-result-object v0

    .line 247
    .local v0, adapter:Lcom/google/android/accounts/AbstractSyncAdapter;
    new-instance v2, Lcom/google/android/accounts/Adapter;

    invoke-direct {v2, v1, v0}, Lcom/google/android/accounts/Adapter;-><init>(Landroid/content/Context;Lcom/google/android/accounts/AbstractSyncAdapter;)V

    invoke-virtual {v2}, Lcom/google/android/accounts/Adapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v2

    .line 249
    .end local v0           #adapter:Lcom/google/android/accounts/AbstractSyncAdapter;
    .end local v1           #context:Landroid/content/Context;
    :goto_13
    return-object v2

    :cond_14
    const/4 v2, 0x0

    goto :goto_13
.end method

.method public bridge synthetic onCreate()V
    .registers 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/accounts/IntentService;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .registers 2

    .prologue
    .line 238
    iget v0, p0, Lcom/google/android/accounts/AbstractSyncService;->mNotificationId:I

    invoke-virtual {p0, v0}, Lcom/google/android/accounts/AbstractSyncService;->stopForegroundCompat(I)V

    .line 239
    invoke-super {p0}, Lcom/google/android/accounts/IntentService;->onDestroy()V

    .line 240
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .registers 10
    .parameter "intent"

    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 136
    .local v6, action:Ljava/lang/String;
    const-string v0, "android.content.SyncAdapter"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 137
    const-string v0, "com.google.android.accounts.intent.extra.ACCOUNT_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 138
    .local v1, accountName:Ljava/lang/String;
    const-string v0, "com.google.android.accounts.intent.extra.ACCOUNT_TYPE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, accountType:Ljava/lang/String;
    const-string v0, "com.google.android.accounts.intent.extra.AUTHORITY"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 140
    .local v3, authority:Ljava/lang/String;
    const-string v0, "com.google.android.accounts.intent.extra.SUPPORTS_UPLOADING"

    const/4 v7, 0x0

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 141
    .local v4, supportsUploading:Z
    const-string v0, "com.google.android.accounts.intent.extra.BUNDLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .local v5, extras:Landroid/os/Bundle;
    move-object v0, p0

    .line 142
    invoke-direct/range {v0 .. v5}, Lcom/google/android/accounts/AbstractSyncService;->performSync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 144
    .end local v1           #accountName:Ljava/lang/String;
    .end local v2           #accountType:Ljava/lang/String;
    .end local v3           #authority:Ljava/lang/String;
    .end local v4           #supportsUploading:Z
    .end local v5           #extras:Landroid/os/Bundle;
    :cond_2f
    return-void
.end method

.method public bridge synthetic onStart(Landroid/content/Intent;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 52
    invoke-super {p0, p1, p2}, Lcom/google/android/accounts/IntentService;->onStart(Landroid/content/Intent;I)V

    return-void
.end method

.method public bridge synthetic startForegroundCompat(ILandroid/app/Notification;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 52
    invoke-super {p0, p1, p2}, Lcom/google/android/accounts/IntentService;->startForegroundCompat(ILandroid/app/Notification;)V

    return-void
.end method

.method public bridge synthetic stopForegroundCompat(I)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/accounts/IntentService;->stopForegroundCompat(I)V

    return-void
.end method
