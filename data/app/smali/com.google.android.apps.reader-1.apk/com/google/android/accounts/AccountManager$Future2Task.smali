.class Lcom/google/android/accounts/AccountManager$Future2Task;
.super Lcom/google/android/accounts/AccountManager$BaseFutureTask;
.source "AccountManager.java"

# interfaces
.implements Lcom/google/android/accounts/AccountManagerFuture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/accounts/AccountManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Future2Task"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/accounts/AccountManager$BaseFutureTask",
        "<TT;>;",
        "Lcom/google/android/accounts/AccountManagerFuture",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final mCallback:Lcom/google/android/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/accounts/AccountManagerCallback",
            "<TT;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/accounts/AccountManager;


# direct methods
.method public constructor <init>(Lcom/google/android/accounts/AccountManager;Ljava/util/concurrent/Callable;Landroid/os/Handler;Lcom/google/android/accounts/AccountManagerCallback;)V
    .registers 5
    .parameter
    .parameter
    .parameter "handler"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;",
            "Landroid/os/Handler;",
            "Lcom/google/android/accounts/AccountManagerCallback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p0, this:Lcom/google/android/accounts/AccountManager$Future2Task;,"Lcom/google/android/accounts/AccountManager$Future2Task<TT;>;"
    .local p2, callable:Ljava/util/concurrent/Callable;,"Ljava/util/concurrent/Callable<TT;>;"
    .local p4, callback:Lcom/google/android/accounts/AccountManagerCallback;,"Lcom/google/android/accounts/AccountManagerCallback<TT;>;"
    iput-object p1, p0, Lcom/google/android/accounts/AccountManager$Future2Task;->this$0:Lcom/google/android/accounts/AccountManager;

    .line 146
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/accounts/AccountManager$BaseFutureTask;-><init>(Lcom/google/android/accounts/AccountManager;Ljava/util/concurrent/Callable;Landroid/os/Handler;)V

    .line 147
    iput-object p4, p0, Lcom/google/android/accounts/AccountManager$Future2Task;->mCallback:Lcom/google/android/accounts/AccountManagerCallback;

    .line 148
    return-void
.end method

.method private internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 8
    .parameter "timeout"
    .parameter "unit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Lcom/google/android/accounts/AuthenticatorException;
        }
    .end annotation

    .prologue
    .line 169
    .local p0, this:Lcom/google/android/accounts/AccountManager$Future2Task;,"Lcom/google/android/accounts/AccountManager$Future2Task<TT;>;"
    if-nez p1, :cond_b

    .line 170
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/accounts/AccountManager$Future2Task;->get()Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_3b
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_5} :catch_18
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_5} :catch_23
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_5} :catch_29
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_5} :catch_2f

    move-result-object v3

    .line 196
    const/4 v2, 0x1

    .line 197
    .local v2, mayInterruptIfRunning:Z
    invoke-virtual {p0, v2}, Lcom/google/android/accounts/AccountManager$Future2Task;->cancel(Z)Z

    .line 198
    :goto_a
    return-object v3

    .line 172
    .end local v2           #mayInterruptIfRunning:Z
    :cond_b
    :try_start_b
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4, p2}, Lcom/google/android/accounts/AccountManager$Future2Task;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_12
    .catchall {:try_start_b .. :try_end_12} :catchall_3b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_12} :catch_18
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_b .. :try_end_12} :catch_23
    .catch Ljava/util/concurrent/CancellationException; {:try_start_b .. :try_end_12} :catch_29
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_b .. :try_end_12} :catch_2f

    move-result-object v3

    .line 196
    const/4 v2, 0x1

    .line 197
    .restart local v2       #mayInterruptIfRunning:Z
    invoke-virtual {p0, v2}, Lcom/google/android/accounts/AccountManager$Future2Task;->cancel(Z)Z

    goto :goto_a

    .line 196
    .end local v2           #mayInterruptIfRunning:Z
    :catch_18
    move-exception v3

    const/4 v2, 0x1

    .line 197
    .restart local v2       #mayInterruptIfRunning:Z
    invoke-virtual {p0, v2}, Lcom/google/android/accounts/AccountManager$Future2Task;->cancel(Z)Z

    .line 199
    :goto_1d
    new-instance v3, Lcom/google/android/accounts/OperationCanceledException;

    invoke-direct {v3}, Lcom/google/android/accounts/OperationCanceledException;-><init>()V

    throw v3

    .line 196
    .end local v2           #mayInterruptIfRunning:Z
    :catch_23
    move-exception v3

    const/4 v2, 0x1

    .line 197
    .restart local v2       #mayInterruptIfRunning:Z
    invoke-virtual {p0, v2}, Lcom/google/android/accounts/AccountManager$Future2Task;->cancel(Z)Z

    goto :goto_1d

    .line 196
    .end local v2           #mayInterruptIfRunning:Z
    :catch_29
    move-exception v3

    const/4 v2, 0x1

    .line 197
    .restart local v2       #mayInterruptIfRunning:Z
    invoke-virtual {p0, v2}, Lcom/google/android/accounts/AccountManager$Future2Task;->cancel(Z)Z

    goto :goto_1d

    .line 180
    .end local v2           #mayInterruptIfRunning:Z
    :catch_2f
    move-exception v1

    .line 181
    .local v1, e:Ljava/util/concurrent/ExecutionException;
    :try_start_30
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 182
    .local v0, cause:Ljava/lang/Throwable;
    instance-of v3, v0, Ljava/io/IOException;

    if-eqz v3, :cond_41

    .line 183
    check-cast v0, Ljava/io/IOException;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0
    :try_end_3b
    .catchall {:try_start_30 .. :try_end_3b} :catchall_3b

    .line 196
    .end local v1           #e:Ljava/util/concurrent/ExecutionException;
    :catchall_3b
    move-exception v3

    const/4 v2, 0x1

    .line 197
    .restart local v2       #mayInterruptIfRunning:Z
    invoke-virtual {p0, v2}, Lcom/google/android/accounts/AccountManager$Future2Task;->cancel(Z)Z

    .line 198
    throw v3

    .line 184
    .end local v2           #mayInterruptIfRunning:Z
    .restart local v0       #cause:Ljava/lang/Throwable;
    .restart local v1       #e:Ljava/util/concurrent/ExecutionException;
    :cond_41
    :try_start_41
    instance-of v3, v0, Ljava/lang/UnsupportedOperationException;

    if-eqz v3, :cond_4b

    .line 185
    new-instance v3, Lcom/google/android/accounts/AuthenticatorException;

    invoke-direct {v3, v0}, Lcom/google/android/accounts/AuthenticatorException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 186
    :cond_4b
    instance-of v3, v0, Lcom/google/android/accounts/AuthenticatorException;

    if-eqz v3, :cond_52

    .line 187
    check-cast v0, Lcom/google/android/accounts/AuthenticatorException;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .line 188
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_52
    instance-of v3, v0, Ljava/lang/RuntimeException;

    if-eqz v3, :cond_59

    .line 189
    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .line 190
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_59
    instance-of v3, v0, Ljava/lang/Error;

    if-eqz v3, :cond_60

    .line 191
    check-cast v0, Ljava/lang/Error;

    .end local v0           #cause:Ljava/lang/Throwable;
    throw v0

    .line 193
    .restart local v0       #cause:Ljava/lang/Throwable;
    :cond_60
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_66
    .catchall {:try_start_41 .. :try_end_66} :catchall_3b
.end method


# virtual methods
.method protected done()V
    .registers 2

    .prologue
    .line 152
    .local p0, this:Lcom/google/android/accounts/AccountManager$Future2Task;,"Lcom/google/android/accounts/AccountManager$Future2Task<TT;>;"
    iget-object v0, p0, Lcom/google/android/accounts/AccountManager$Future2Task;->mCallback:Lcom/google/android/accounts/AccountManagerCallback;

    if-eqz v0, :cond_c

    .line 153
    new-instance v0, Lcom/google/android/accounts/AccountManager$Future2Task$1;

    invoke-direct {v0, p0}, Lcom/google/android/accounts/AccountManager$Future2Task$1;-><init>(Lcom/google/android/accounts/AccountManager$Future2Task;)V

    invoke-virtual {p0, v0}, Lcom/google/android/accounts/AccountManager$Future2Task;->postRunnableToHandler(Ljava/lang/Runnable;)V

    .line 159
    :cond_c
    return-void
.end method

.method public getResult()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Lcom/google/android/accounts/AuthenticatorException;
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/google/android/accounts/AccountManager$Future2Task;,"Lcom/google/android/accounts/AccountManager$Future2Task<TT;>;"
    const/4 v0, 0x0

    .line 206
    invoke-direct {p0, v0, v0}, Lcom/google/android/accounts/AccountManager$Future2Task;->internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5
    .parameter "timeout"
    .parameter "unit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/accounts/OperationCanceledException;,
            Ljava/io/IOException;,
            Lcom/google/android/accounts/AuthenticatorException;
        }
    .end annotation

    .prologue
    .line 214
    .local p0, this:Lcom/google/android/accounts/AccountManager$Future2Task;,"Lcom/google/android/accounts/AccountManager$Future2Task<TT;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/google/android/accounts/AccountManager$Future2Task;->internalGetResult(Ljava/lang/Long;Ljava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public start()Lcom/google/android/accounts/AccountManager$Future2Task;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/accounts/AccountManager$Future2Task",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 162
    .local p0, this:Lcom/google/android/accounts/AccountManager$Future2Task;,"Lcom/google/android/accounts/AccountManager$Future2Task<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/accounts/AccountManager$Future2Task;->startTask()V

    .line 163
    return-object p0
.end method
