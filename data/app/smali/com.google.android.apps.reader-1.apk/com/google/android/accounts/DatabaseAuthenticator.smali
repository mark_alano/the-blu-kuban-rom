.class public abstract Lcom/google/android/accounts/DatabaseAuthenticator;
.super Ljava/lang/Object;
.source "DatabaseAuthenticator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/accounts/DatabaseAuthenticator$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final COLUMN_ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field private static final COLUMN_ACCOUNT_TYPE:Ljava/lang/String; = "account_type"

.field private static final COLUMN_AUTHORITY:Ljava/lang/String; = "authority"

.field private static final COLUMN_AUTH_TOKEN:Ljava/lang/String; = "auth_token"

.field private static final COLUMN_AUTH_TOKEN_TYPE:Ljava/lang/String; = "auth_token_type"

.field private static final COLUMN_PASSWORD:Ljava/lang/String; = "password"

.field private static final COLUMN_SYNC_AUTOMATICALLY:Ljava/lang/String; = "sync_automatically"

.field private static final DATABASE_VERSION:I = 0x3

.field private static final KEY_DATABASE_AUTHENTICATOR:Ljava/lang/String; = "com.google.android.accounts.DatabaseAuthenticator"

.field private static final LOG_TAG:Ljava/lang/String; = "DatabaseAccountManager"

.field private static final NAMESPACE:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"

.field private static final TABLE_ACCOUNTS:Ljava/lang/String; = "accounts"

.field private static final TABLE_AUTH_TOKENS:Ljava/lang/String; = "auth_tokens"

.field private static final TABLE_SYNC_AUTOMATICALLY:Ljava/lang/String; = "sync_automatically"


# instance fields
.field private final mAuthenticatorActivity:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDatabaseName:Ljava/lang/String;

.field private mMaxSdkVersion:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)V
    .registers 5
    .parameter "context"
    .parameter "databaseName"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p3, authenticatorActivity:Ljava/lang/Class;,"Ljava/lang/Class<+Landroid/app/Activity;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mMaxSdkVersion:I

    .line 285
    if-eqz p1, :cond_c

    if-eqz p2, :cond_c

    if-nez p3, :cond_12

    .line 286
    :cond_c
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 288
    :cond_12
    iput-object p1, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mContext:Landroid/content/Context;

    .line 289
    iput-object p2, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mDatabaseName:Ljava/lang/String;

    .line 290
    iput-object p3, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mAuthenticatorActivity:Ljava/lang/Class;

    .line 291
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/accounts/DatabaseAuthenticator;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mDatabaseName:Ljava/lang/String;

    return-object v0
.end method

.method private static addAccount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 6
    .parameter "db"
    .parameter "account"
    .parameter "password"

    .prologue
    .line 181
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 182
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "account_name"

    iget-object v2, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v1, "account_type"

    iget-object v2, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    if-eqz p2, :cond_1a

    .line 185
    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_1a
    const-string v1, "accounts"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 188
    return-void
.end method

.method private static final beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .registers 6
    .parameter "parser"
    .parameter "firstElementName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 195
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .local v0, type:I
    if-eq v0, v2, :cond_a

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 198
    :cond_a
    if-eq v0, v2, :cond_14

    .line 199
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "No start tag found"

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 202
    :cond_14
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_45

    .line 203
    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected start tag: found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_45
    return-void
.end method

.method static createDatabaseAuthenticators(Landroid/content/Context;)Ljava/util/Map;
    .registers 22
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/accounts/DatabaseAuthenticator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 222
    .local v4, authenticators:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/android/accounts/DatabaseAuthenticator;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    .line 223
    .local v12, packageName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 224
    .local v14, pm:Landroid/content/pm/PackageManager;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 225
    .local v17, resources:Landroid/content/res/Resources;
    new-instance v8, Landroid/content/Intent;

    const-string v19, "com.google.android.accounts.DatabaseAuthenticator"

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 226
    .local v8, intent:Landroid/content/Intent;
    const/16 v6, 0x80

    .line 227
    .local v6, flags:I
    invoke-virtual {v14, v8, v6}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_24
    :goto_24
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_a8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    .line 228
    .local v16, resolveInfo:Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    move-object/from16 v18, v0

    .line 229
    .local v18, serviceInfo:Landroid/content/pm/ServiceInfo;
    move-object/from16 v0, v18

    iget-object v10, v0, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    .line 230
    .local v10, metaData:Landroid/os/Bundle;
    if-eqz v10, :cond_24

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_24

    .line 231
    const-string v19, "com.google.android.accounts.DatabaseAuthenticator"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 232
    .local v15, resId:I
    if-eqz v15, :cond_24

    .line 234
    :try_start_54
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v13

    .line 235
    .local v13, parser:Landroid/content/res/XmlResourceParser;
    const-string v19, "account-authenticator"

    move-object/from16 v0, v19

    invoke-static {v13, v0}, Lcom/google/android/accounts/DatabaseAuthenticator;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 236
    const-string v19, "http://schemas.android.com/apk/res/android"

    const-string v20, "name"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 237
    .local v11, name:Ljava/lang/String;
    const-string v19, "http://schemas.android.com/apk/res/android"

    const-string v20, "accountType"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, accountType:Ljava/lang/String;
    const-string v19, "http://schemas.android.com/apk/res/android"

    const-string v20, "maxSdkVersion"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 239
    .local v9, maxSdkVersion:Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/google/android/accounts/DatabaseAuthenticator;->newDatabaseAuthenticatorInstance(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/accounts/DatabaseAuthenticator;

    move-result-object v3

    .line 241
    .local v3, authenticator:Lcom/google/android/accounts/DatabaseAuthenticator;
    if-eqz v9, :cond_96

    .line 242
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/google/android/accounts/DatabaseAuthenticator;->setMaxSdkVersion(I)V

    .line 244
    :cond_96
    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_99
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_99} :catch_9a

    goto :goto_24

    .line 245
    .end local v2           #accountType:Ljava/lang/String;
    .end local v3           #authenticator:Lcom/google/android/accounts/DatabaseAuthenticator;
    .end local v9           #maxSdkVersion:Ljava/lang/String;
    .end local v11           #name:Ljava/lang/String;
    .end local v13           #parser:Landroid/content/res/XmlResourceParser;
    :catch_9a
    move-exception v5

    .line 246
    .local v5, e:Ljava/lang/Exception;
    const-string v19, "DatabaseAccountManager"

    const-string v20, "Failed  to create authenticator"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_24

    .line 251
    .end local v5           #e:Ljava/lang/Exception;
    .end local v10           #metaData:Landroid/os/Bundle;
    .end local v15           #resId:I
    .end local v16           #resolveInfo:Landroid/content/pm/ResolveInfo;
    .end local v18           #serviceInfo:Landroid/content/pm/ServiceInfo;
    :cond_a8
    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_b5

    .line 252
    const-string v19, "DatabaseAccountManager"

    const-string v20, "No authenticators found"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_b5
    return-object v4
.end method

.method private deleteAccountsNotIn([Lcom/google/android/accounts/Account;)V
    .registers 12
    .parameter "accounts"

    .prologue
    const/16 v9, 0x3f

    const/16 v8, 0x3d

    .line 722
    const/4 v5, 0x0

    .line 723
    .local v5, whereClause:Ljava/lang/String;
    const/4 v4, 0x0

    .line 724
    .local v4, whereArgs:[Ljava/lang/String;
    array-length v6, p1

    if-eqz v6, :cond_68

    .line 725
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 726
    .local v3, where:Ljava/lang/StringBuilder;
    array-length v6, p1

    mul-int/lit8 v6, v6, 0x2

    new-array v4, v6, [Ljava/lang/String;

    .line 727
    const-string v6, "NOT ("

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 728
    const/4 v2, 0x0

    .local v2, i:I
    :goto_19
    array-length v6, p1

    if-ge v2, v6, :cond_5f

    .line 729
    aget-object v0, p1, v2

    .line 730
    .local v0, account:Lcom/google/android/accounts/Account;
    if-eqz v2, :cond_25

    .line 731
    const-string v6, " OR "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 733
    :cond_25
    const-string v6, "("

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 734
    const-string v6, "account_name"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 735
    const-string v6, " AND "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 736
    const-string v6, "account_type"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 737
    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 738
    mul-int/lit8 v6, v2, 0x2

    iget-object v7, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v7, v4, v6

    .line 739
    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    iget-object v7, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v7, v4, v6

    .line 728
    add-int/lit8 v2, v2, 0x1

    goto :goto_19

    .line 741
    .end local v0           #account:Lcom/google/android/accounts/Account;
    :cond_5f
    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 745
    .end local v2           #i:I
    .end local v3           #where:Ljava/lang/StringBuilder;
    :cond_68
    invoke-direct {p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 747
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_6c
    const-string v6, "sync_automatically"

    invoke-virtual {v1, v6, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 748
    const-string v6, "auth_tokens"

    invoke-virtual {v1, v6, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 749
    const-string v6, "accounts"

    invoke-virtual {v1, v6, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7b
    .catchall {:try_start_6c .. :try_end_7b} :catchall_7f

    .line 751
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 753
    return-void

    .line 751
    :catchall_7f
    move-exception v6

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v6
.end method

.method private getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 309
    new-instance v0, Lcom/google/android/accounts/DatabaseAuthenticator$DatabaseHelper;

    invoke-direct {v0, p0}, Lcom/google/android/accounts/DatabaseAuthenticator$DatabaseHelper;-><init>(Lcom/google/android/accounts/DatabaseAuthenticator;)V

    invoke-virtual {v0}, Lcom/google/android/accounts/DatabaseAuthenticator$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method private getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 313
    new-instance v0, Lcom/google/android/accounts/DatabaseAuthenticator$DatabaseHelper;

    invoke-direct {v0, p0}, Lcom/google/android/accounts/DatabaseAuthenticator$DatabaseHelper;-><init>(Lcom/google/android/accounts/DatabaseAuthenticator;)V

    invoke-virtual {v0}, Lcom/google/android/accounts/DatabaseAuthenticator$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method private static hasAccount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)Z
    .registers 15
    .parameter "db"
    .parameter "account"

    .prologue
    const/4 v12, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 162
    new-array v2, v10, [Ljava/lang/String;

    .line 163
    .local v2, projection:[Ljava/lang/String;
    const-string v0, "%s=? AND %s=?"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v11, "account_name"

    aput-object v11, v1, v10

    const-string v11, "account_type"

    aput-object v11, v1, v9

    const-string v11, "auth_token_type"

    aput-object v11, v1, v12

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 165
    .local v3, selection:Ljava/lang/String;
    new-array v4, v12, [Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v4, v10

    iget-object v0, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v0, v4, v9

    .line 168
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 169
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 170
    .local v6, having:Ljava/lang/String;
    const/4 v7, 0x0

    .line 171
    .local v7, orderBy:Ljava/lang/String;
    const-string v1, "accounts"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 174
    .local v8, c:Landroid/database/Cursor;
    :try_start_2e
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_31
    .catchall {:try_start_2e .. :try_end_31} :catchall_3b

    move-result v0

    if-eqz v0, :cond_39

    move v0, v9

    .line 176
    :goto_35
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return v0

    :cond_39
    move v0, v10

    .line 174
    goto :goto_35

    .line 176
    :catchall_3b
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static newDatabaseAuthenticatorInstance(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/accounts/DatabaseAuthenticator;
    .registers 9
    .parameter "context"
    .parameter "packageName"
    .parameter "className"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/lang/NoSuchMethodException;,
            Ljava/lang/reflect/InvocationTargetException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 212
    const-string v2, "."

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 215
    :cond_1b
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 216
    .local v0, authenticatorClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    new-array v2, v5, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 217
    .local v1, constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    new-array v2, v5, [Ljava/lang/Object;

    aput-object p0, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/accounts/DatabaseAuthenticator;

    return-object v2
.end method


# virtual methods
.method public addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 9
    .parameter "accountType"
    .parameter "authTokenType"
    .parameter "requiredFeatures"
    .parameter "options"

    .prologue
    .line 322
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 324
    .local v1, result:Landroid/os/Bundle;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 325
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mAuthenticatorActivity:Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 326
    const-string v2, "accountType"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    const-string v2, "accountType"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 331
    return-object v1
.end method

.method public addAccountExplicitly(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 7
    .parameter "account"
    .parameter "password"
    .parameter "userdata"

    .prologue
    const/4 v1, 0x0

    .line 339
    if-eqz p1, :cond_b

    iget-object v2, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 360
    :cond_b
    :goto_b
    return v1

    .line 345
    :cond_c
    invoke-direct {p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 347
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_10
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_33

    .line 349
    :try_start_13
    invoke-static {v0, p1}, Lcom/google/android/accounts/DatabaseAuthenticator;->hasAccount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)Z
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_2e

    move-result v2

    if-eqz v2, :cond_20

    .line 357
    :try_start_19
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_1c
    .catchall {:try_start_19 .. :try_end_1c} :catchall_33

    .line 360
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_b

    .line 352
    :cond_20
    :try_start_20
    invoke-static {v0, p1, p2}, Lcom/google/android/accounts/DatabaseAuthenticator;->addAccount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_26
    .catchall {:try_start_20 .. :try_end_26} :catchall_2e

    .line 354
    const/4 v1, 0x1

    .line 357
    :try_start_27
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2a
    .catchall {:try_start_27 .. :try_end_2a} :catchall_33

    .line 360
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_b

    .line 357
    :catchall_2e
    move-exception v1

    :try_start_2f
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
    :try_end_33
    .catchall {:try_start_2f .. :try_end_33} :catchall_33

    .line 360
    :catchall_33
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v1
.end method

.method public confirmCredentials(Lcom/google/android/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 7
    .parameter "account"
    .parameter "options"

    .prologue
    .line 369
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 371
    .local v1, result:Landroid/os/Bundle;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 372
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mAuthenticatorActivity:Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 373
    const-string v2, "authAccount"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 374
    const-string v2, "accountType"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 376
    const-string v2, "authAccount"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v2, "accountType"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 380
    return-object v1
.end method

.method public editProperties(Ljava/lang/String;)Landroid/os/Bundle;
    .registers 3
    .parameter "accountType"

    .prologue
    .line 389
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    return-object v0
.end method

.method public getAccountsByType(Ljava/lang/String;)[Lcom/google/android/accounts/Account;
    .registers 21
    .parameter "type"

    .prologue
    .line 544
    if-nez p1, :cond_8

    .line 545
    new-instance v15, Ljava/lang/NullPointerException;

    invoke-direct {v15}, Ljava/lang/NullPointerException;-><init>()V

    throw v15

    .line 547
    :cond_8
    const-string v2, "accounts"

    .line 548
    .local v2, table:Ljava/lang/String;
    const/4 v15, 0x1

    new-array v3, v15, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "account_name"

    aput-object v16, v3, v15

    .line 551
    .local v3, columns:[Ljava/lang/String;
    const-string v15, "%s=?"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v18, "account_type"

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 552
    .local v4, selection:Ljava/lang/String;
    const/4 v15, 0x1

    new-array v5, v15, [Ljava/lang/String;

    const/4 v15, 0x0

    aput-object p1, v5, v15

    .line 555
    .local v5, selectionArgs:[Ljava/lang/String;
    const/4 v6, 0x0

    .line 556
    .local v6, groupBy:Ljava/lang/String;
    const/4 v7, 0x0

    .line 557
    .local v7, having:Ljava/lang/String;
    const/4 v8, 0x0

    .line 558
    .local v8, orderBy:Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 560
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_33
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_65

    move-result-object v12

    .line 563
    .local v12, cursor:Landroid/database/Cursor;
    :try_start_37
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v11

    .line 564
    .local v11, count:I
    new-array v9, v11, [Lcom/google/android/accounts/Account;

    .line 565
    .local v9, accounts:[Lcom/google/android/accounts/Account;
    const-string v15, "account_name"

    invoke-interface {v12, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 566
    .local v10, columnIndex:I
    const/4 v14, 0x0

    .local v14, pos:I
    :goto_44
    if-ge v14, v11, :cond_59

    .line 567
    invoke-interface {v12, v14}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 568
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 569
    .local v13, name:Ljava/lang/String;
    new-instance v15, Lcom/google/android/accounts/Account;

    move-object/from16 v0, p1

    invoke-direct {v15, v13, v0}, Lcom/google/android/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v15, v9, v14
    :try_end_56
    .catchall {:try_start_37 .. :try_end_56} :catchall_60

    .line 566
    add-int/lit8 v14, v14, 0x1

    goto :goto_44

    .line 573
    .end local v13           #name:Ljava/lang/String;
    :cond_59
    :try_start_59
    invoke-interface {v12}, Landroid/database/Cursor;->close()V
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_65

    .line 576
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-object v9

    .line 573
    .end local v9           #accounts:[Lcom/google/android/accounts/Account;
    .end local v10           #columnIndex:I
    .end local v11           #count:I
    .end local v14           #pos:I
    :catchall_60
    move-exception v15

    :try_start_61
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v15
    :try_end_65
    .catchall {:try_start_61 .. :try_end_65} :catchall_65

    .line 576
    .end local v12           #cursor:Landroid/database/Cursor;
    :catchall_65
    move-exception v15

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v15
.end method

.method public getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;)[Lcom/google/android/accounts/Account;
    .registers 4
    .parameter "type"
    .parameter "features"

    .prologue
    .line 585
    invoke-virtual {p0, p1}, Lcom/google/android/accounts/DatabaseAuthenticator;->getAccountsByType(Ljava/lang/String;)[Lcom/google/android/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getAuthToken(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 22
    .parameter "account"
    .parameter "authTokenType"
    .parameter "loginOptions"

    .prologue
    .line 397
    const-string v2, "auth_tokens"

    .line 398
    .local v2, table:Ljava/lang/String;
    const/4 v14, 0x1

    new-array v3, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "auth_token"

    aput-object v15, v3, v14

    .line 404
    .local v3, projection:[Ljava/lang/String;
    if-eqz p2, :cond_79

    .line 405
    const-string v14, "%s=? AND %s=? AND %s=?"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "account_name"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "account_type"

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "auth_token_type"

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 407
    .local v4, selection:Ljava/lang/String;
    const/4 v14, 0x3

    new-array v5, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v15, v5, v14

    const/4 v14, 0x1

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v15, v5, v14

    const/4 v14, 0x2

    aput-object p2, v5, v14

    .line 417
    .local v5, selectionArgs:[Ljava/lang/String;
    :goto_3b
    const/4 v6, 0x0

    .line 418
    .local v6, groupBy:Ljava/lang/String;
    const/4 v7, 0x0

    .line 419
    .local v7, having:Ljava/lang/String;
    const/4 v8, 0x0

    .line 420
    .local v8, orderBy:Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 422
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_42
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_d3

    move-result-object v11

    .line 425
    .local v11, cursor:Landroid/database/Cursor;
    :try_start_46
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 426
    .local v13, result:Landroid/os/Bundle;
    const-string v14, "authAccount"

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v13, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const-string v14, "accountType"

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v13, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v14

    if-eqz v14, :cond_a6

    .line 429
    const-string v14, "auth_token"

    invoke-interface {v11, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 430
    .local v10, columnIndex:I
    invoke-interface {v11, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 431
    .local v9, authToken:Ljava/lang/String;
    const-string v14, "authtoken"

    invoke-virtual {v13, v14, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_72
    .catchall {:try_start_46 .. :try_end_72} :catchall_ce

    .line 441
    .end local v9           #authToken:Ljava/lang/String;
    .end local v10           #columnIndex:I
    :goto_72
    :try_start_72
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_75
    .catchall {:try_start_72 .. :try_end_75} :catchall_d3

    .line 444
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-object v13

    .line 411
    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v4           #selection:Ljava/lang/String;
    .end local v5           #selectionArgs:[Ljava/lang/String;
    .end local v6           #groupBy:Ljava/lang/String;
    .end local v7           #having:Ljava/lang/String;
    .end local v8           #orderBy:Ljava/lang/String;
    .end local v11           #cursor:Landroid/database/Cursor;
    .end local v13           #result:Landroid/os/Bundle;
    :cond_79
    const-string v14, "%s=? AND %s=? AND %s IS NULL"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "account_name"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "account_type"

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "auth_token_type"

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 413
    .restart local v4       #selection:Ljava/lang/String;
    const/4 v14, 0x2

    new-array v5, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v15, v5, v14

    const/4 v14, 0x1

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v15, v5, v14

    .restart local v5       #selectionArgs:[Ljava/lang/String;
    goto :goto_3b

    .line 433
    .restart local v1       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v6       #groupBy:Ljava/lang/String;
    .restart local v7       #having:Ljava/lang/String;
    .restart local v8       #orderBy:Ljava/lang/String;
    .restart local v11       #cursor:Landroid/database/Cursor;
    .restart local v13       #result:Landroid/os/Bundle;
    :cond_a6
    :try_start_a6
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 434
    .local v12, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/accounts/DatabaseAuthenticator;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/accounts/DatabaseAuthenticator;->mAuthenticatorActivity:Ljava/lang/Class;

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 435
    const-string v14, "authAccount"

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const-string v14, "accountType"

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    const-string v14, "intent"

    invoke-virtual {v13, v14, v12}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_cd
    .catchall {:try_start_a6 .. :try_end_cd} :catchall_ce

    goto :goto_72

    .line 441
    .end local v12           #intent:Landroid/content/Intent;
    .end local v13           #result:Landroid/os/Bundle;
    :catchall_ce
    move-exception v14

    :try_start_cf
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v14
    :try_end_d3
    .catchall {:try_start_cf .. :try_end_d3} :catchall_d3

    .line 444
    .end local v11           #cursor:Landroid/database/Cursor;
    :catchall_d3
    move-exception v14

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v14
.end method

.method public abstract getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method getMaxSdkVersion()I
    .registers 2

    .prologue
    .line 294
    iget v0, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mMaxSdkVersion:I

    return v0
.end method

.method public getSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 16
    .parameter "account"
    .parameter "authority"

    .prologue
    .line 679
    if-eqz p1, :cond_c

    iget-object v9, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    if-eqz v9, :cond_c

    iget-object v9, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    if-eqz v9, :cond_c

    if-nez p2, :cond_12

    .line 680
    :cond_c
    new-instance v9, Ljava/lang/NullPointerException;

    invoke-direct {v9}, Ljava/lang/NullPointerException;-><init>()V

    throw v9

    .line 682
    :cond_12
    const-string v1, "sync_automatically"

    .line 683
    .local v1, table:Ljava/lang/String;
    const/4 v9, 0x0

    new-array v2, v9, [Ljava/lang/String;

    .line 684
    .local v2, columns:[Ljava/lang/String;
    const-string v9, "%s=? AND %s=? AND %s=?"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "account_name"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "account_type"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string v12, "authority"

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 686
    .local v3, selection:Ljava/lang/String;
    const/4 v9, 0x3

    new-array v4, v9, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v10, v4, v9

    const/4 v9, 0x1

    iget-object v10, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v10, v4, v9

    const/4 v9, 0x2

    aput-object p2, v4, v9

    .line 689
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 690
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 691
    .local v6, having:Ljava/lang/String;
    const/4 v7, 0x0

    .line 692
    .local v7, orderBy:Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 694
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_46
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_5a

    move-result-object v8

    .line 697
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_4a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_55

    move-result v9

    .line 699
    :try_start_4e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_51
    .catchall {:try_start_4e .. :try_end_51} :catchall_5a

    .line 702
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return v9

    .line 699
    :catchall_55
    move-exception v9

    :try_start_56
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v9
    :try_end_5a
    .catchall {:try_start_56 .. :try_end_5a} :catchall_5a

    .line 702
    .end local v8           #cursor:Landroid/database/Cursor;
    :catchall_5a
    move-exception v9

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v9
.end method

.method public hasFeatures(Lcom/google/android/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .registers 6
    .parameter "account"
    .parameter "features"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 461
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 462
    .local v0, bundle:Landroid/os/Bundle;
    if-eqz p2, :cond_a

    array-length v2, p2

    if-nez v2, :cond_11

    :cond_a
    const/4 v1, 0x1

    .line 463
    .local v1, featuresEmpty:Z
    :goto_b
    const-string v2, "booleanResult"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 464
    return-object v0

    .line 462
    .end local v1           #featuresEmpty:Z
    :cond_11
    const/4 v1, 0x0

    goto :goto_b
.end method

.method public invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "accountType"
    .parameter "authToken"

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 622
    if-eqz p1, :cond_7

    if-nez p2, :cond_d

    .line 623
    :cond_7
    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 625
    :cond_d
    const-string v3, "%s=? AND %s=?"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "account_type"

    aput-object v5, v4, v6

    const-string v5, "auth_token"

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 626
    .local v2, whereClause:Ljava/lang/String;
    new-array v1, v8, [Ljava/lang/String;

    aput-object p1, v1, v6

    aput-object p2, v1, v7

    .line 629
    .local v1, whereArgs:[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 631
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_27
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_2a
    .catchall {:try_start_27 .. :try_end_2a} :catchall_3e

    .line 633
    :try_start_2a
    const-string v3, "auth_tokens"

    invoke-virtual {v0, v3, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 634
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_32
    .catchall {:try_start_2a .. :try_end_32} :catchall_39

    .line 636
    :try_start_32
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_3e

    .line 639
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 641
    return-void

    .line 636
    :catchall_39
    move-exception v3

    :try_start_3a
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
    :try_end_3e
    .catchall {:try_start_3a .. :try_end_3e} :catchall_3e

    .line 639
    :catchall_3e
    move-exception v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v3
.end method

.method public onLoginAccountsChanged([Lcom/google/android/accounts/Account;)V
    .registers 2
    .parameter "accounts"

    .prologue
    .line 718
    invoke-direct {p0, p1}, Lcom/google/android/accounts/DatabaseAuthenticator;->deleteAccountsNotIn([Lcom/google/android/accounts/Account;)V

    .line 719
    return-void
.end method

.method public removeAccount(Lcom/google/android/accounts/Account;)Z
    .registers 11
    .parameter "account"

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 593
    if-eqz p1, :cond_d

    iget-object v3, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    if-eqz v3, :cond_d

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    if-nez v3, :cond_13

    .line 594
    :cond_d
    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 596
    :cond_13
    const-string v3, "%s=? AND %s=?"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "account_name"

    aput-object v5, v4, v7

    const-string v5, "account_type"

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 598
    .local v2, whereClause:Ljava/lang/String;
    new-array v1, v8, [Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v7

    iget-object v3, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v3, v1, v6

    .line 601
    .local v1, whereArgs:[Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 603
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_31
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_34
    .catchall {:try_start_31 .. :try_end_34} :catchall_4d

    .line 605
    :try_start_34
    const-string v3, "auth_tokens"

    invoke-virtual {v0, v3, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 606
    const-string v3, "sync_automatically"

    invoke-virtual {v0, v3, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 607
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_41
    .catchall {:try_start_34 .. :try_end_41} :catchall_48

    .line 610
    :try_start_41
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_4d

    .line 613
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return v6

    .line 610
    :catchall_48
    move-exception v3

    :try_start_49
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
    :try_end_4d
    .catchall {:try_start_49 .. :try_end_4d} :catchall_4d

    .line 613
    :catchall_4d
    move-exception v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v3
.end method

.method public setAuthToken(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 15
    .parameter "account"
    .parameter "authTokenType"
    .parameter "authToken"

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 499
    if-eqz p1, :cond_10

    iget-object v4, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    if-eqz v4, :cond_10

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    if-eqz v4, :cond_10

    if-nez p3, :cond_16

    .line 500
    :cond_10
    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    .line 505
    :cond_16
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 506
    .local v1, values:Landroid/content/ContentValues;
    const-string v4, "account_name"

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    const-string v4, "account_type"

    iget-object v5, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    if-eqz p2, :cond_71

    .line 509
    const-string v4, "auth_token_type"

    invoke-virtual {v1, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const-string v4, "%s=? AND %s=? AND %s=?"

    new-array v5, v10, [Ljava/lang/Object;

    const-string v6, "account_name"

    aput-object v6, v5, v8

    const-string v6, "account_type"

    aput-object v6, v5, v7

    const-string v6, "auth_token_type"

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 512
    .local v3, whereClause:Ljava/lang/String;
    new-array v2, v10, [Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v8

    iget-object v4, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v4, v2, v7

    aput-object p2, v2, v9

    .line 522
    .local v2, whereArgs:[Ljava/lang/String;
    :goto_50
    const-string v4, "auth_token"

    invoke-virtual {v1, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    invoke-direct {p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 526
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_59
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_95

    .line 528
    :try_start_5c
    const-string v4, "auth_tokens"

    invoke-virtual {v0, v4, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 529
    const-string v4, "auth_tokens"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 530
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6a
    .catchall {:try_start_5c .. :try_end_6a} :catchall_90

    .line 533
    :try_start_6a
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_6d
    .catchall {:try_start_6a .. :try_end_6d} :catchall_95

    .line 536
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return v7

    .line 516
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v2           #whereArgs:[Ljava/lang/String;
    .end local v3           #whereClause:Ljava/lang/String;
    :cond_71
    const-string v4, "%s=? AND %s=? AND %s IS NULL"

    new-array v5, v10, [Ljava/lang/Object;

    const-string v6, "account_name"

    aput-object v6, v5, v8

    const-string v6, "account_type"

    aput-object v6, v5, v7

    const-string v6, "auth_token_type"

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 518
    .restart local v3       #whereClause:Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v8

    iget-object v4, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v4, v2, v7

    .restart local v2       #whereArgs:[Ljava/lang/String;
    goto :goto_50

    .line 533
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    :catchall_90
    move-exception v4

    :try_start_91
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
    :try_end_95
    .catchall {:try_start_91 .. :try_end_95} :catchall_95

    .line 536
    :catchall_95
    move-exception v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v4
.end method

.method setMaxSdkVersion(I)V
    .registers 2
    .parameter "version"

    .prologue
    .line 298
    iput p1, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mMaxSdkVersion:I

    .line 299
    return-void
.end method

.method public setSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;Z)V
    .registers 13
    .parameter "account"
    .parameter "authority"
    .parameter "sync"

    .prologue
    .line 648
    if-eqz p1, :cond_c

    iget-object v5, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    if-eqz v5, :cond_c

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    if-eqz v5, :cond_c

    if-nez p2, :cond_12

    .line 649
    :cond_c
    new-instance v5, Ljava/lang/NullPointerException;

    invoke-direct {v5}, Ljava/lang/NullPointerException;-><init>()V

    throw v5

    .line 651
    :cond_12
    invoke-direct {p0}, Lcom/google/android/accounts/DatabaseAuthenticator;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 653
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    if-eqz p3, :cond_4a

    :try_start_18
    invoke-virtual {p0, p1, p2}, Lcom/google/android/accounts/DatabaseAuthenticator;->getSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4a

    .line 654
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 655
    .local v2, values:Landroid/content/ContentValues;
    const-string v5, "account_name"

    iget-object v6, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string v5, "account_type"

    iget-object v6, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    const-string v5, "authority"

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    const-string v5, "sync_automatically"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 659
    const-string v5, "sync_automatically"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_46
    .catchall {:try_start_18 .. :try_end_46} :catchall_78

    .line 670
    .end local v2           #values:Landroid/content/ContentValues;
    :goto_46
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 672
    return-void

    .line 661
    :cond_4a
    :try_start_4a
    const-string v1, "sync_automatically"

    .line 662
    .local v1, table:Ljava/lang/String;
    const-string v5, "%s=? AND %s=? AND %s=?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "account_name"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "account_type"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "authority"

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 664
    .local v4, whereClause:Ljava/lang/String;
    const/4 v5, 0x3

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    iget-object v6, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    aput-object v6, v3, v5

    const/4 v5, 0x2

    aput-object p2, v3, v5

    .line 667
    .local v3, whereArgs:[Ljava/lang/String;
    invoke-virtual {v0, v1, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_77
    .catchall {:try_start_4a .. :try_end_77} :catchall_78

    goto :goto_46

    .line 670
    .end local v1           #table:Ljava/lang/String;
    .end local v3           #whereArgs:[Ljava/lang/String;
    .end local v4           #whereClause:Ljava/lang/String;
    :catchall_78
    move-exception v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v5
.end method

.method public updateCredentials(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 8
    .parameter "account"
    .parameter "authTokenType"
    .parameter "loginOptions"

    .prologue
    .line 472
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 474
    .local v1, result:Landroid/os/Bundle;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 475
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/accounts/DatabaseAuthenticator;->mAuthenticatorActivity:Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 476
    const-string v2, "authAccount"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 477
    const-string v2, "accountType"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479
    const-string v2, "authAccount"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v2, "accountType"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 483
    return-object v1
.end method
