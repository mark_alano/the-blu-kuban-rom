.class Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;
.super Landroid/widget/BaseAdapter;
.source "BundleListActivity.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/app/BundleListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BundlesAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter$BundleFilter;,
        Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter$SubscriptionsObserver;
    }
.end annotation


# instance fields
.field private final mBundles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mFilter:Landroid/widget/Filter;

.field private mFilterPositions:Landroid/util/SparseIntArray;

.field private final mObserver:Landroid/database/DataSetObserver;

.field private final mPositionForSection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSectionForPosition:[I

.field private final mSections:[Ljava/lang/Object;

.field private mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 11
    .parameter "context"

    .prologue
    .line 154
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 155
    new-instance v7, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter$SubscriptionsObserver;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter$SubscriptionsObserver;-><init>(Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;Lcom/google/android/apps/reader/app/BundleListActivity$1;)V

    iput-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mObserver:Landroid/database/DataSetObserver;

    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 157
    .local v4, res:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/google/android/apps/reader/res/ReaderResources;->getBundles(Landroid/content/res/Resources;)Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    .line 158
    iget-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    .line 159
    .local v1, bundleCount:I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v5, sections:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mPositionForSection:Ljava/util/ArrayList;

    .line 161
    new-array v7, v1, [I

    iput-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSectionForPosition:[I

    .line 162
    const/4 v3, 0x0

    .local v3, position:I
    :goto_2c
    if-ge v3, v1, :cond_7e

    .line 163
    iget-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;

    .line 164
    .local v0, bundle:Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;
    invoke-virtual {v0}, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;->getTitle()Ljava/lang/String;

    move-result-object v6

    .line 165
    .local v6, title:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_7b

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 166
    .local v2, letter:Ljava/lang/String;
    :goto_46
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 167
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_62

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6e

    .line 168
    :cond_62
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mPositionForSection:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_6e
    iget-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSectionForPosition:[I

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    aput v8, v7, v3

    .line 162
    add-int/lit8 v3, v3, 0x1

    goto :goto_2c

    .line 165
    .end local v2           #letter:Ljava/lang/String;
    :cond_7b
    const-string v2, ""

    goto :goto_46

    .line 173
    .end local v0           #bundle:Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;
    .end local v6           #title:Ljava/lang/String;
    :cond_7e
    invoke-virtual {v5}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSections:[Ljava/lang/Object;

    .line 174
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;Landroid/util/SparseIntArray;)Landroid/util/SparseIntArray;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    return-object p1
.end method

.method private getBundleTitle(I)Ljava/lang/String;
    .registers 3
    .parameter "position"

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_a

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    .line 207
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;

    invoke-virtual {v0}, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSubscriptions(I)Ljava/util/List;
    .registers 3
    .parameter "position"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle$Data$Subscription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_a

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    .line 200
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;

    invoke-virtual {v0}, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;->getData()Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle$Data;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle$Data;->getSubscriptionList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public changeSubscriptions(Lcom/google/android/apps/reader/widget/SubscriptionsQuery;)V
    .registers 4
    .parameter "subscriptions"

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;

    if-eqz v0, :cond_b

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;

    iget-object v1, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/widget/SubscriptionsQuery;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 180
    :cond_b
    iput-object p1, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;

    if-eqz v0, :cond_18

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;

    iget-object v1, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/widget/SubscriptionsQuery;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 184
    :cond_18
    return-void
.end method

.method public getCount()I
    .registers 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_a
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilter:Landroid/widget/Filter;

    if-nez v0, :cond_c

    .line 296
    new-instance v0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter$BundleFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter$BundleFilter;-><init>(Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;Lcom/google/android/apps/reader/app/BundleListActivity$1;)V

    iput-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilter:Landroid/widget/Filter;

    .line 298
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilter:Landroid/widget/Filter;

    return-object v0
.end method

.method public getItem(I)Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;
    .registers 3
    .parameter "position"

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_a

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    .line 193
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mBundles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->getItem(I)Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_a

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    .line 231
    :cond_a
    int-to-long v0, p1

    return-wide v0
.end method

.method public getPositionForSection(I)I
    .registers 3
    .parameter "section"

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mPositionForSection:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .registers 3
    .parameter "position"

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_a

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mFilterPositions:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p1

    .line 281
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSectionForPosition:[I

    aget v0, v0, p1

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSections:[Ljava/lang/Object;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 19
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 238
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 239
    .local v6, res:Landroid/content/res/Resources;
    move-object/from16 v12, p2

    .line 240
    .local v12, view:Landroid/view/View;
    if-nez v12, :cond_1a

    .line 241
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 242
    .local v3, context:Landroid/content/Context;
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 243
    .local v5, inflater:Landroid/view/LayoutInflater;
    const v13, 0x7f03000c

    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v5, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 245
    .end local v3           #context:Landroid/content/Context;
    .end local v5           #inflater:Landroid/view/LayoutInflater;
    :cond_1a
    const v13, 0x1020014

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 246
    .local v9, text1:Landroid/widget/TextView;
    const v13, 0x1020015

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 248
    .local v10, text2:Landroid/widget/TextView;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->getBundleTitle(I)Ljava/lang/String;

    move-result-object v11

    .line 249
    .local v11, title:Ljava/lang/String;
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->getSubscriptions(I)Ljava/util/List;

    move-result-object v8

    .line 252
    .local v8, subscriptions:Ljava/util/List;,"Ljava/util/List<Lcom/google/feedreader/rpc/Storage$SubscriptionBundles$LocaleGroup$Bundle$Data$Subscription;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    .line 253
    .local v4, feedCount:I
    const/4 v13, 0x1

    new-array v1, v13, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v1, v13

    .line 256
    .local v1, args:[Ljava/lang/Object;
    const v13, 0x7f0e0002

    invoke-virtual {v6, v13, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v13, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;

    if-eqz v13, :cond_69

    iget-object v13, p0, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->mSubscriptions:Lcom/google/android/apps/reader/widget/SubscriptionsQuery;

    invoke-virtual {v13, v8}, Lcom/google/android/apps/reader/widget/SubscriptionsQuery;->containsAll(Ljava/lang/Iterable;)Z

    move-result v13

    if-eqz v13, :cond_69

    const/4 v7, 0x1

    .line 261
    .local v7, subscribed:Z
    :goto_5c
    const v13, 0x1020001

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 262
    .local v2, checkbox:Landroid/widget/CheckBox;
    invoke-virtual {v2, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 264
    return-object v12

    .line 258
    .end local v2           #checkbox:Landroid/widget/CheckBox;
    .end local v7           #subscribed:Z
    :cond_69
    const/4 v7, 0x0

    goto :goto_5c
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/BundleListActivity$BundlesAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method
