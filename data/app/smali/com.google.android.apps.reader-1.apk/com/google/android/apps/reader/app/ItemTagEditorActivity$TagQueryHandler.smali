.class Lcom/google/android/apps/reader/app/ItemTagEditorActivity$TagQueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "ItemTagEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/app/ItemTagEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TagQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/reader/app/ItemTagEditorActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/reader/app/ItemTagEditorActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/apps/reader/app/ItemTagEditorActivity$TagQueryHandler;->this$0:Lcom/google/android/apps/reader/app/ItemTagEditorActivity;

    .line 246
    invoke-virtual {p1}, Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 247
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .registers 13
    .parameter "token"
    .parameter "cookie"
    .parameter "cursor"

    .prologue
    .line 252
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/reader/app/ItemTagEditorActivity$TagQueryHandler;->this$0:Lcom/google/android/apps/reader/app/ItemTagEditorActivity;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->isFinishing()Z
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_2a

    move-result v6

    if-eqz v6, :cond_e

    .line 253
    if-eqz p3, :cond_d

    .line 288
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 291
    :cond_d
    :goto_d
    return-void

    .line 256
    :cond_e
    const/4 v6, 0x1

    if-eq p1, v6, :cond_31

    .line 257
    :try_start_11
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected token: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_2a
    .catchall {:try_start_11 .. :try_end_2a} :catchall_2a

    .line 287
    :catchall_2a
    move-exception v6

    if-eqz p3, :cond_30

    .line 288
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_30
    throw v6

    .line 259
    :cond_31
    :try_start_31
    iget-object v6, p0, Lcom/google/android/apps/reader/app/ItemTagEditorActivity$TagQueryHandler;->this$0:Lcom/google/android/apps/reader/app/ItemTagEditorActivity;

    #getter for: Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->mTarget:Landroid/net/Uri;
    invoke-static {v6}, Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->access$000(Lcom/google/android/apps/reader/app/ItemTagEditorActivity;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_3a
    .catchall {:try_start_31 .. :try_end_3a} :catchall_2a

    move-result v6

    if-nez v6, :cond_43

    .line 260
    if-eqz p3, :cond_d

    .line 288
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_d

    .line 263
    :cond_43
    if-nez p3, :cond_52

    .line 264
    :try_start_45
    const-string v6, "ItemTagEditor"

    const-string v7, "Query result is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4c
    .catchall {:try_start_45 .. :try_end_4c} :catchall_2a

    .line 287
    if-eqz p3, :cond_d

    .line 288
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_d

    .line 267
    :cond_52
    :try_start_52
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 268
    .local v0, capacity:I
    iget-object v6, p0, Lcom/google/android/apps/reader/app/ItemTagEditorActivity$TagQueryHandler;->this$0:Lcom/google/android/apps/reader/app/ItemTagEditorActivity;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    #setter for: Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->mInitial:Ljava/util/ArrayList;
    invoke-static {v6, v7}, Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->access$102(Lcom/google/android/apps/reader/app/ItemTagEditorActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 269
    .local v3, initial:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/google/android/apps/reader/app/ItemTagEditorActivity$TagQueryHandler;->this$0:Lcom/google/android/apps/reader/app/ItemTagEditorActivity;

    #getter for: Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->mTagEditor:Landroid/widget/MultiAutoCompleteTextView;
    invoke-static {v6}, Lcom/google/android/apps/reader/app/ItemTagEditorActivity;->access$200(Lcom/google/android/apps/reader/app/ItemTagEditorActivity;)Landroid/widget/MultiAutoCompleteTextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/MultiAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    .line 272
    .local v1, editable:Landroid/text/Editable;
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 273
    .local v2, enteredText:Ljava/lang/String;
    invoke-interface {v1}, Landroid/text/Editable;->clear()V

    .line 275
    const/4 v5, 0x0

    .local v5, position:I
    :goto_73
    invoke-interface {p3, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_8c

    .line 276
    const/4 v6, 0x0

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 277
    .local v4, label:Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    invoke-interface {v1, v4}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 279
    const-string v6, ", "

    invoke-interface {v1, v6}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 275
    add-int/lit8 v5, v5, 0x1

    goto :goto_73

    .line 285
    .end local v4           #label:Ljava/lang/String;
    :cond_8c
    invoke-interface {v1, v2}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;
    :try_end_8f
    .catchall {:try_start_52 .. :try_end_8f} :catchall_2a

    .line 287
    if-eqz p3, :cond_d

    .line 288
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_d
.end method
