.class public Lcom/google/android/apps/reader/app/ReaderApplication;
.super Landroid/app/Application;
.source "ReaderApplication.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ReaderApp"


# instance fields
.field private mImageLoader:Lcom/google/android/imageloader/ImageLoader;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method private static checkMainThread()V
    .registers 2

    .prologue
    .line 90
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 91
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method must be called from the main thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_12
    return-void
.end method

.method private static initAsyncTask()V
    .registers 4

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/apps/reader/app/ReaderApplication;->checkMainThread()V

    .line 64
    :try_start_3
    const-class v1, Landroid/os/AsyncTask;

    const-string v2, "init"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_15} :catch_16

    .line 68
    .local v0, t:Ljava/lang/Throwable;
    :goto_15
    return-void

    .line 65
    .end local v0           #t:Ljava/lang/Throwable;
    :catch_16
    move-exception v0

    .line 66
    .restart local v0       #t:Ljava/lang/Throwable;
    const-string v1, "ReaderApp"

    const-string v2, "Call to AsyncTask.init() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_15
.end method

.method private static initModernAsyncTask()V
    .registers 5

    .prologue
    .line 75
    invoke-static {}, Lcom/google/android/apps/reader/app/ReaderApplication;->checkMainThread()V

    .line 78
    :try_start_3
    const-string v2, "android.support.v4.content.ModernAsyncTask"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 79
    .local v0, modernAsyncTask:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-string v2, "init"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_19} :catch_1a

    .line 83
    :goto_19
    return-void

    .line 80
    :catch_1a
    move-exception v1

    .line 81
    .local v1, t:Ljava/lang/Throwable;
    const-string v2, "ReaderApp"

    const-string v3, "Call to ModernAsyncTask.init() failed"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_19
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "name"

    .prologue
    .line 49
    const-string v0, "com.google.android.imageloader"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/reader/app/ReaderApplication;->mImageLoader:Lcom/google/android/imageloader/ImageLoader;

    .line 52
    :goto_a
    return-object v0

    :cond_b
    invoke-super {p0, p1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_a
.end method

.method public onCreate()V
    .registers 3

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/reader/app/ReaderApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 39
    .local v0, resolver:Landroid/content/ContentResolver;
    new-instance v1, Lcom/google/android/imageloader/ImageLoader;

    invoke-direct {v1, v0}, Lcom/google/android/imageloader/ImageLoader;-><init>(Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/google/android/apps/reader/app/ReaderApplication;->mImageLoader:Lcom/google/android/imageloader/ImageLoader;

    .line 43
    invoke-static {}, Lcom/google/android/apps/reader/app/ReaderApplication;->initAsyncTask()V

    .line 44
    invoke-static {}, Lcom/google/android/apps/reader/app/ReaderApplication;->initModernAsyncTask()V

    .line 45
    return-void
.end method
