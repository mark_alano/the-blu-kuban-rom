.class public Lcom/google/android/apps/reader/app/ReaderConnectService;
.super Landroid/app/IntentService;
.source "ReaderConnectService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ReaderConnect"


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 55
    const-string v0, "ReaderConnect"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method private static getBackgroundData(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/apps/reader/util/SystemService;->getConnectivityManager(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    .line 51
    .local v0, manager:Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v1

    return v1
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .registers 23
    .parameter "intent"

    .prologue
    .line 61
    move-object/from16 v7, p0

    .line 62
    .local v7, context:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/reader/app/ReaderConnectService;->getBackgroundData(Landroid/content/Context;)Z

    move-result v6

    .line 63
    .local v6, backgroundData:Z
    invoke-static {v7}, Lcom/google/android/accounts/AccountManager;->get(Landroid/content/Context;)Lcom/google/android/accounts/AccountManager;

    move-result-object v4

    .line 64
    .local v4, am:Lcom/google/android/accounts/AccountManager;
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 65
    .local v8, cr:Landroid/content/ContentResolver;
    invoke-static {v7}, Lcom/google/android/accounts/ContentSyncer;->get(Landroid/content/Context;)Lcom/google/android/accounts/ContentSyncer;

    move-result-object v9

    .line 66
    .local v9, cs:Lcom/google/android/accounts/ContentSyncer;
    invoke-static {}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->unsynchronizedUri()Landroid/net/Uri;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v8, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->query(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/util/List;

    move-result-object v16

    .line 67
    .local v16, unsynchronized:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/accounts/Account;>;"
    const-string v17, "com.google"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/android/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Lcom/google/android/accounts/Account;

    move-result-object v3

    .line 68
    .local v3, accounts:[Lcom/google/android/accounts/Account;
    move-object v5, v3

    .local v5, arr$:[Lcom/google/android/accounts/Account;
    array-length v12, v5

    .local v12, len$:I
    const/4 v11, 0x0

    .local v11, i$:I
    :goto_27
    if-ge v11, v12, :cond_a0

    aget-object v2, v5, v11

    .line 69
    .local v2, account:Lcom/google/android/accounts/Account;
    const-string v17, "com.google.android.apps.reader"

    move-object/from16 v0, v17

    invoke-virtual {v9, v2, v0}, Lcom/google/android/accounts/ContentSyncer;->getSyncAutomatically(Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v13

    .line 70
    .local v13, syncAutomatically:Z
    invoke-static {v7, v2}, Lcom/google/android/apps/reader/app/ReaderTimestamps;->isSyncOverdue(Landroid/content/Context;Lcom/google/android/accounts/Account;)Z

    move-result v14

    .line 71
    .local v14, syncOverdue:Z
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    .line 72
    .local v15, syncRequired:Z
    if-eqz v6, :cond_66

    if-eqz v13, :cond_66

    if-eqz v14, :cond_66

    .line 73
    const-string v17, "Requesting overdue sync for %s"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    iget-object v0, v2, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 74
    const-string v17, "com.google.android.apps.reader"

    sget-object v18, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v2, v0, v1}, Lcom/google/android/accounts/ContentSyncer;->requestSync(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 68
    :cond_63
    :goto_63
    add-int/lit8 v11, v11, 0x1

    goto :goto_27

    .line 75
    :cond_66
    if-eqz v15, :cond_63

    .line 76
    const-string v17, "Upsyncing pending actions for %s"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    iget-object v0, v2, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 77
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v10, extras:Landroid/os/Bundle;
    const-string v17, "upload"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 84
    const-string v17, "force"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 86
    const-string v17, "com.google.android.apps.reader"

    move-object/from16 v0, v17

    invoke-virtual {v9, v2, v0, v10}, Lcom/google/android/accounts/ContentSyncer;->requestSync(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_63

    .line 89
    .end local v2           #account:Lcom/google/android/accounts/Account;
    .end local v10           #extras:Landroid/os/Bundle;
    .end local v13           #syncAutomatically:Z
    .end local v14           #syncOverdue:Z
    .end local v15           #syncRequired:Z
    :cond_a0
    return-void
.end method
