.class public Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;
.super Lcom/google/android/feeds/JsonContentHandler;
.source "DirectorySearchContentHandler.java"


# instance fields
.field private final mAccount:Lcom/google/android/accounts/Account;

.field private final mOutput:Landroid/database/MatrixCursor;


# direct methods
.method public constructor <init>(Landroid/database/MatrixCursor;Lcom/google/android/accounts/Account;)V
    .registers 3
    .parameter "cursor"
    .parameter "account"

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/feeds/JsonContentHandler;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;->mOutput:Landroid/database/MatrixCursor;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    .line 46
    return-void
.end method

.method private getContent(Lorg/json/JSONObject;)Ljava/lang/Object;
    .registers 16
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v6, p0, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;->mOutput:Landroid/database/MatrixCursor;

    .line 66
    .local v6, output:Landroid/database/MatrixCursor;
    invoke-virtual {v6}, Landroid/database/MatrixCursor;->getColumnCount()I

    move-result v2

    .line 67
    .local v2, columnCount:I
    const-string v11, "results"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 68
    .local v8, results:Lorg/json/JSONArray;
    const/4 v5, 0x0

    .local v5, i:I
    :goto_d
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v5, v11, :cond_9e

    .line 69
    invoke-virtual {v8, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 70
    .local v7, result:Lorg/json/JSONObject;
    const-string v11, "title"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 71
    .local v10, title:Ljava/lang/String;
    const-string v11, "streamid"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 72
    .local v9, streamId:Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-long v11, v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 73
    .local v0, baseId:Ljava/lang/Long;
    invoke-virtual {v6}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    .line 74
    .local v1, builder:Landroid/database/MatrixCursor$RowBuilder;
    const/4 v3, 0x0

    .local v3, columnIndex:I
    :goto_35
    if-ge v3, v2, :cond_9a

    .line 75
    invoke-virtual {v6, v3}, Landroid/database/MatrixCursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, columnName:Ljava/lang/String;
    const-string v11, "_id"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_49

    .line 77
    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 74
    :goto_46
    add-int/lit8 v3, v3, 0x1

    goto :goto_35

    .line 78
    :cond_49
    const-string v11, "id"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_55

    .line 79
    invoke-virtual {v1, v9}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_46

    .line 80
    :cond_55
    const-string v11, "title"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_61

    .line 81
    invoke-virtual {v1, v10}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_46

    .line 82
    :cond_61
    const-string v11, "account_name"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_71

    .line 83
    iget-object v11, p0, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v11, v11, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v11}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_46

    .line 84
    :cond_71
    const-string v11, "account_type"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_81

    .line 85
    iget-object v11, p0, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v11, v11, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v11}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_46

    .line 87
    :cond_81
    new-instance v11, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unknown column: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 68
    .end local v4           #columnName:Ljava/lang/String;
    :cond_9a
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_d

    .line 91
    .end local v0           #baseId:Ljava/lang/Long;
    .end local v1           #builder:Landroid/database/MatrixCursor$RowBuilder;
    .end local v3           #columnIndex:I
    .end local v7           #result:Lorg/json/JSONObject;
    .end local v9           #streamId:Ljava/lang/String;
    .end local v10           #title:Ljava/lang/String;
    :cond_9e
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v11

    invoke-static {v11}, Lcom/google/android/feeds/FeedLoader;->documentInfo(I)Ljava/lang/Object;

    move-result-object v11

    return-object v11
.end method


# virtual methods
.method protected getContent(Ljava/lang/String;)Ljava/lang/Object;
    .registers 8
    .parameter "source"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 50
    const-string v2, "var _DIRECTORY_SEARCH_DATA = "

    .line 51
    .local v2, prefix:Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 52
    .local v3, start:I
    if-ne v3, v5, :cond_11

    .line 53
    new-instance v4, Lorg/json/JSONException;

    const-string v5, "Directory search data is missing"

    invoke-direct {v4, v5}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 55
    :cond_11
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    .line 56
    const-string v4, "</script>"

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 57
    .local v1, end:I
    if-ne v1, v5, :cond_26

    .line 58
    new-instance v4, Lorg/json/JSONException;

    const-string v5, "Closing script tag is missing"

    invoke-direct {v4, v5}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 60
    :cond_26
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, data:Lorg/json/JSONObject;
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;->getContent(Lorg/json/JSONObject;)Ljava/lang/Object;

    move-result-object v4

    return-object v4
.end method
