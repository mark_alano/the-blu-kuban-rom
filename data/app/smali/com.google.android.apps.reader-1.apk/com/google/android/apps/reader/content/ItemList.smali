.class Lcom/google/android/apps/reader/content/ItemList;
.super Ljava/lang/Object;
.source "ItemList.java"


# instance fields
.field private final mAccount:Lcom/google/android/accounts/Account;

.field private final mExcludeTarget:Ljava/lang/String;

.field private final mId:Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;

.field private final mRanking:Ljava/lang/String;

.field private final mStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "account"
    .parameter "query"
    .parameter "streamId"

    .prologue
    const/4 v0, 0x0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    if-nez p1, :cond_c

    .line 170
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 172
    :cond_c
    if-nez p2, :cond_14

    .line 173
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 175
    :cond_14
    iput-object p1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    .line 176
    iput-object p2, p0, Lcom/google/android/apps/reader/content/ItemList;->mQuery:Ljava/lang/String;

    .line 177
    iput-object p3, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    .line 178
    iput-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    .line 179
    iput-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    .line 180
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ItemList;->createId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public constructor <init>(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "account"
    .parameter "streamId"
    .parameter "excludeTarget"
    .parameter "ranking"

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    if-nez p1, :cond_b

    .line 149
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 151
    :cond_b
    if-nez p2, :cond_13

    .line 152
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 154
    :cond_13
    if-nez p4, :cond_1b

    .line 155
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 157
    :cond_1b
    iput-object p1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    .line 158
    iput-object p2, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    .line 159
    iput-object p3, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    .line 160
    iput-object p4, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mQuery:Ljava/lang/String;

    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ItemList;->createId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    .line 163
    return-void
.end method

.method private appendAndItemsNotInBlacklist(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;)V
    .registers 4
    .parameter "builder"
    .parameter "categoryStreamId"

    .prologue
    .line 708
    const-string v0, " AND "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 709
    const-string v0, "items.id NOT IN ("

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 711
    const-string v0, "SELECT "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 712
    const-string v0, "item_positions_blacklist.item_id"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 713
    const-string v0, " FROM "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 714
    const-string v0, "item_positions_blacklist"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 715
    const-string v0, " WHERE "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 716
    const-string v0, "item_positions_blacklist.account_name = "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 718
    const-string v0, " AND "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 719
    const-string v0, "item_positions_blacklist.item_list_id = "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 721
    const-string v0, " AND "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 722
    const-string v0, "item_positions_blacklist.category_id = "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 723
    invoke-virtual {p1, p2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 725
    const-string v0, ")"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 726
    return-void
.end method

.method private appendAndItemsNotInPositionTable(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;)V
    .registers 4
    .parameter "builder"
    .parameter "categoryStreamId"

    .prologue
    .line 734
    const-string v0, " AND "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 735
    const-string v0, "items.id NOT IN ("

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 737
    const-string v0, "SELECT "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 738
    const-string v0, "item_positions.item_id"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 739
    const-string v0, " FROM "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 740
    const-string v0, "item_positions"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 741
    const-string v0, " WHERE "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 742
    const-string v0, "item_positions.account_name = "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 744
    const-string v0, " AND "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 745
    const-string v0, "item_positions.item_list_id = "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 748
    const-string v0, ")"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 749
    return-void
.end method

.method private copyTimestamps(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)V
    .registers 13
    .parameter "db"
    .parameter "values"

    .prologue
    const/4 v9, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    if-eqz v0, :cond_4e

    .line 642
    const-string v1, "unread_counts"

    .line 643
    .local v1, table:Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "newest_item_timestamp"

    aput-object v0, v2, v6

    const-string v0, "last_read_item_timestamp"

    aput-object v0, v2, v7

    .line 646
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "account_name = ? AND stream_id = ?"

    .line 647
    .local v3, selection:Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    aput-object v0, v4, v7

    .local v4, selectionArgs:[Ljava/lang/String;
    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    .line 650
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 652
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_29
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 653
    const-string v0, "newest_item_timestamp"

    const/4 v5, 0x0

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 654
    const-string v0, "last_read_item_timestamp"

    const/4 v5, 0x1

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_4b
    .catchall {:try_start_29 .. :try_end_4b} :catchall_4f

    .line 657
    :cond_4b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 660
    .end local v1           #table:Ljava/lang/String;
    .end local v2           #projection:[Ljava/lang/String;
    .end local v3           #selection:Ljava/lang/String;
    .end local v4           #selectionArgs:[Ljava/lang/String;
    .end local v8           #cursor:Landroid/database/Cursor;
    :cond_4e
    return-void

    .line 657
    .restart local v1       #table:Ljava/lang/String;
    .restart local v2       #projection:[Ljava/lang/String;
    .restart local v3       #selection:Ljava/lang/String;
    .restart local v4       #selectionArgs:[Ljava/lang/String;
    .restart local v8       #cursor:Landroid/database/Cursor;
    :catchall_4f
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private createId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ItemList;->getBaseUri()Landroid/net/Uri;

    move-result-object v0

    .line 247
    .local v0, uri:Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static deleteByStreamId(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 7
    .parameter "db"
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 71
    const-string v1, "account_name = ? AND stream_id = ?"

    .line 72
    .local v1, whereClause:Ljava/lang/String;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v0, v2

    const/4 v2, 0x1

    aput-object p2, v0, v2

    .line 73
    .local v0, whereArgs:[Ljava/lang/String;
    const-string v2, "item_lists"

    invoke-virtual {p0, v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 74
    return-void
.end method

.method private getBaseUri()Landroid/net/Uri;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mQuery:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mQuery:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/reader/net/ReaderUri;->searchItemsIds(Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    .line 238
    :goto_d
    return-object v0

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/reader/content/ItemList;->map(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/reader/net/ReaderUri;->streamContents(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_d
.end method

.method private getItemListColumnAsLong(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)J
    .registers 16
    .parameter "db"
    .parameter "columnName"
    .parameter "defaultValue"

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 464
    const-string v1, "item_lists"

    .line 465
    .local v1, table:Ljava/lang/String;
    new-array v2, v10, [Ljava/lang/String;

    aput-object p2, v2, v9

    .line 466
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "account_name = ? AND id = ?"

    .line 467
    .local v3, selection:Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v4, v9

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v0, v4, v10

    .line 468
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 469
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 470
    .local v6, having:Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, orderBy:Ljava/lang/String;
    move-object v0, p1

    .line 471
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 474
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1f
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 475
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_29
    .catchall {:try_start_1f .. :try_end_29} :catchall_32

    move-result-wide p3

    .line 480
    .end local p3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_2d
    return-wide p3

    .restart local p3
    :cond_2e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2d

    :catchall_32
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static invalidateByStreamId(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)I
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 55
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "timestamp"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 58
    const-string v2, "account_name = ? AND stream_id = ?"

    .line 59
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    aput-object p2, v1, v3

    .line 60
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "item_lists"

    invoke-virtual {p0, v3, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method public static invalidateByStreamIds(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;)I
    .registers 12
    .parameter "db"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 86
    .local p2, streamIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 87
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "timestamp"

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 89
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "account_name = ? AND stream_id IN ("

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 90
    .local v4, selectionBuilder:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_18
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_2d

    .line 91
    if-eqz v0, :cond_25

    .line 92
    const-string v6, ", "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    :cond_25
    const/16 v6, 0x3f

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 96
    :cond_2d
    const/16 v6, 0x29

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 98
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 99
    .local v3, selectionArgsList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 102
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103
    .local v1, selection:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 104
    .local v2, selectionArgs:[Ljava/lang/String;
    const-string v6, "item_lists"

    invoke-virtual {p0, v6, v5, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    return v6
.end method

.method private static map(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "ranking"

    .prologue
    .line 112
    if-nez p0, :cond_a

    .line 113
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Ranking is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_a
    const-string v0, "newest"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 116
    const-string v0, "n"

    .line 128
    :goto_14
    return-object v0

    .line 117
    :cond_15
    const-string v0, "oldest"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 118
    const-string v0, "o"

    goto :goto_14

    .line 119
    :cond_20
    const-string v0, "auto"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 120
    const-string v0, "a"

    goto :goto_14

    .line 121
    :cond_2b
    const-string v0, "hybrid"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 122
    const-string v0, "h"

    goto :goto_14

    .line 123
    :cond_36
    const-string v0, "date"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 124
    const-string v0, "n"

    goto :goto_14

    .line 125
    :cond_41
    const-string v0, "magic"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 126
    const-string v0, "a"

    goto :goto_14

    .line 128
    :cond_4c
    const-string v0, "n"

    goto :goto_14
.end method

.method private queryUnreadNotInPositionTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "db"
    .parameter "userId"

    .prologue
    const/4 v3, 0x0

    .line 789
    const-string v1, "user/-/state/com.google/read"

    invoke-static {v1, p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 790
    .local v8, readCategoryStreamId:Ljava/lang/String;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 791
    .local v0, builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "items"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 792
    const-string v1, "items.account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 793
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 794
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 795
    const-string v1, "items.id NOT IN ("

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 797
    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 798
    const-string v1, "item_categories.item_id"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 799
    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 800
    const-string v1, "item_categories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 801
    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 802
    const-string v1, "item_categories.account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 803
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 804
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 805
    const-string v1, "item_categories.stream_id = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 806
    invoke-virtual {v0, v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 808
    const-string v1, ")"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 809
    invoke-direct {p0, v0, v8}, Lcom/google/android/apps/reader/content/ItemList;->appendAndItemsNotInPositionTable(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;)V

    .line 810
    invoke-direct {p0, v0, v8}, Lcom/google/android/apps/reader/content/ItemList;->appendAndItemsNotInBlacklist(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;)V

    .line 811
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "items.id"

    aput-object v4, v2, v1

    .local v2, projection:[Ljava/lang/String;
    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    .line 812
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private querytagNotInPositionTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 13
    .parameter "db"
    .parameter "userId"

    .prologue
    const/4 v3, 0x0

    .line 756
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->getLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 757
    .local v8, streamLabel:Ljava/lang/String;
    invoke-static {p2, v8}, Lcom/google/android/apps/reader/provider/ReaderStream;->createTagId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 758
    .local v9, tagStreamId:Ljava/lang/String;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 759
    .local v0, builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "items, item_categories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 760
    const-string v1, "item_categories.account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 761
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 762
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 763
    const-string v1, "items.account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 764
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 765
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 766
    const-string v1, "item_categories.stream_id = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 767
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 768
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 769
    const-string v1, "item_categories.item_id = items.id"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 770
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 771
    const-string v1, "item_categories.stream_id = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 772
    invoke-virtual {v0, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 773
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ItemList;->excludesRead()Z

    move-result v1

    if-eqz v1, :cond_68

    .line 775
    const-string v1, " AND NOT "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 776
    const-string v1, "items.read"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 778
    :cond_68
    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/reader/content/ItemList;->appendAndItemsNotInPositionTable(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;)V

    .line 779
    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/reader/content/ItemList;->appendAndItemsNotInBlacklist(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;)V

    .line 780
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "items.id"

    aput-object v4, v2, v1

    .local v2, projection:[Ljava/lang/String;
    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    .line 781
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public cleanupAfterRemoteLoad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .registers 13
    .parameter "db"
    .parameter "userId"

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 672
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ItemList;->excludesRead()Z

    move-result v6

    if-eqz v6, :cond_44

    .line 673
    const-string v6, "user/-/state/com.google/read"

    invoke-static {v6, p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 674
    .local v0, categoryStreamId:Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/content/ItemList;->queryUnreadNotInPositionTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 675
    .local v1, cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    .local v3, position:I
    :goto_13
    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_44

    .line 676
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 677
    .local v2, itemId:Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 678
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "account_name"

    iget-object v7, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v7, v7, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    const-string v6, "item_list_id"

    iget-object v7, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    const-string v6, "item_id"

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    const-string v6, "category_id"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    const-string v6, "item_positions_blacklist"

    invoke-virtual {p1, v6, v9, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 675
    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    .line 685
    .end local v0           #categoryStreamId:Ljava/lang/String;
    .end local v1           #cursor:Landroid/database/Cursor;
    .end local v2           #itemId:Ljava/lang/String;
    .end local v3           #position:I
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_44
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ItemList;->excludesNonTagged()Z

    move-result v6

    if-eqz v6, :cond_8a

    .line 686
    iget-object v6, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/apps/reader/provider/ReaderStream;->getLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 687
    .local v4, streamLabel:Ljava/lang/String;
    invoke-static {p2, v4}, Lcom/google/android/apps/reader/provider/ReaderStream;->createTagId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 688
    .restart local v0       #categoryStreamId:Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/content/ItemList;->querytagNotInPositionTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 689
    .restart local v1       #cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    .restart local v3       #position:I
    :goto_59
    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_8a

    .line 690
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 691
    .restart local v2       #itemId:Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 692
    .restart local v5       #values:Landroid/content/ContentValues;
    const-string v6, "account_name"

    iget-object v7, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v7, v7, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v6, "item_list_id"

    iget-object v7, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    const-string v6, "item_id"

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    const-string v6, "category_id"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    const-string v6, "item_positions_blacklist"

    invoke-virtual {p1, v6, v9, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 689
    add-int/lit8 v3, v3, 0x1

    goto :goto_59

    .line 699
    .end local v0           #categoryStreamId:Ljava/lang/String;
    .end local v1           #cursor:Landroid/database/Cursor;
    .end local v2           #itemId:Ljava/lang/String;
    .end local v3           #position:I
    .end local v4           #streamLabel:Ljava/lang/String;
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_8a
    return-void
.end method

.method public createItemsIdsUri(I)Landroid/net/Uri;
    .registers 5
    .parameter "n"

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mQuery:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mQuery:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/reader/net/ReaderUri;->searchItemsIds(Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    .line 223
    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/reader/content/ItemList;->map(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/reader/net/ReaderUri;->streamItemsIds(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_c
.end method

.method public createStreamContentsUri(I)Landroid/net/Uri;
    .registers 5
    .parameter "n"

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/reader/content/ItemList;->map(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/reader/net/ReaderUri;->streamContents(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public deleteItemPositions(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 6
    .parameter "db"

    .prologue
    .line 261
    const-string v0, "account_name = ? AND item_list_id = ?"

    .line 262
    .local v0, where:Ljava/lang/String;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 265
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v2, "item_positions"

    invoke-virtual {p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 266
    return-void
.end method

.method public deleteNonTaggedItemPositions(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .registers 10
    .parameter "db"
    .parameter "userId"

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ItemList;->excludesNonTagged()Z

    move-result v5

    if-nez v5, :cond_e

    .line 379
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Tried to filter unfiltered stream"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 381
    :cond_e
    iget-object v5, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/reader/provider/ReaderStream;->getLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 382
    .local v2, streamLabel:Ljava/lang/String;
    invoke-static {p2, v2}, Lcom/google/android/apps/reader/provider/ReaderStream;->createTagId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 383
    .local v1, categoryStreamID:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 384
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v5, "account_name = ?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    const-string v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    const-string v5, "item_list_id = ?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    const-string v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    const-string v5, "item_id NOT IN ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    const-string v5, "SELECT "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    const-string v5, "item_id"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    const-string v5, " FROM "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    const-string v5, "item_categories"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    const-string v5, " WHERE "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    const-string v5, "item_categories.account_name = ?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    const-string v5, " AND "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    const-string v5, "item_categories.stream_id = ?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 401
    .local v3, where:Ljava/lang/String;
    const/4 v5, 0x4

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v6, v6, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v6, v6, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v1, v4, v5

    .line 402
    .local v4, whereArgs:[Ljava/lang/String;
    const-string v5, "item_positions"

    invoke-virtual {p1, v5, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 403
    return-void
.end method

.method public deleteReadItemPositions(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 7
    .parameter "db"

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ItemList;->excludesRead()Z

    move-result v3

    if-nez v3, :cond_e

    .line 306
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Tried to filter unfiltered stream"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 308
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v3, "account_name = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    const-string v3, "item_list_id = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    const-string v3, "item_id IN ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const-string v3, "SELECT "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const-string v3, "items.id"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    const-string v3, " FROM "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    const-string v3, "item_positions, items"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string v3, " WHERE "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string v3, "item_positions.account_name = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    const-string v3, "item_positions.item_list_id = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    const-string v3, "items.account_name = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    const-string v3, "items.read"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    const-string v3, "item_positions.account_name = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const-string v3, "item_positions.item_id = items.id"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 334
    .local v1, where:Ljava/lang/String;
    const/4 v3, 0x6

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 337
    .local v2, whereArgs:[Ljava/lang/String;
    const-string v3, "item_positions"

    invoke-virtual {p1, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 338
    return-void
.end method

.method public excludesNonTagged()Z
    .registers 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isLabel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public excludesRead()Z
    .registers 3

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 209
    const-string v0, "user/-/state/com.google/read"

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->unsetUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 211
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public getAccount()Lcom/google/android/accounts/Account;
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    return-object v0
.end method

.method public getCacheTimestamp(Landroid/database/sqlite/SQLiteDatabase;)J
    .registers 5
    .parameter "db"

    .prologue
    .line 494
    const-string v0, "timestamp"

    const-wide/16 v1, -0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/reader/content/ItemList;->getItemListColumnAsLong(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getCrawlTimestamp(Landroid/database/sqlite/SQLiteDatabase;J)J
    .registers 13
    .parameter "db"
    .parameter "defaultValue"

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 504
    const-string v1, "newest"

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_74

    .line 505
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 506
    .local v0, builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "item_positions, items"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 507
    const-string v1, "item_positions.account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 508
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 509
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 510
    const-string v1, "items.account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 511
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 512
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 513
    const-string v1, "item_positions.item_list_id = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 514
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 515
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 516
    const-string v1, "item_positions.item_id = items.id"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 517
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 518
    const-string v1, "item_positions.position = 0"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 519
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "crawl_time"

    aput-object v1, v2, v5

    .local v2, projection:[Ljava/lang/String;
    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    .line 522
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 524
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_66
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_75

    .line 525
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_70
    .catchall {:try_start_66 .. :try_end_70} :catchall_79

    move-result-wide p2

    .line 530
    .end local p2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 533
    .end local v0           #builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v2           #projection:[Ljava/lang/String;
    .end local v8           #cursor:Landroid/database/Cursor;
    :cond_74
    :goto_74
    return-wide p2

    .line 530
    .restart local v0       #builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2       #projection:[Ljava/lang/String;
    .restart local v8       #cursor:Landroid/database/Cursor;
    .restart local p2
    :cond_75
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_74

    :catchall_79
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getItemCount(Landroid/database/sqlite/SQLiteDatabase;)I
    .registers 13
    .parameter "db"

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 541
    const-string v1, "item_positions"

    .line 542
    .local v1, table:Ljava/lang/String;
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "COUNT(1)"

    aput-object v0, v2, v9

    .line 545
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "account_name = ? AND item_list_id = ?"

    .line 546
    .local v3, selection:Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v4, v9

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v0, v4, v10

    .line 549
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 550
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 551
    .local v6, having:Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, sortOrder:Ljava/lang/String;
    move-object v0, p1

    .line 552
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 555
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_21
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 556
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2b
    .catchall {:try_start_21 .. :try_end_2b} :catchall_35

    move-result v0

    .line 561
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_2f
    return v0

    :cond_30
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v9

    goto :goto_2f

    :catchall_35
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getLocalContent(Landroid/database/sqlite/SQLiteDatabase;Landroid/os/Bundle;)Ljava/lang/Object;
    .registers 18
    .parameter "db"
    .parameter "extras"

    .prologue
    .line 570
    if-nez p1, :cond_8

    .line 571
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 573
    :cond_8
    if-nez p2, :cond_10

    .line 574
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 576
    :cond_10
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ItemList;->getItemCount(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v10

    .line 578
    .local v10, count:I
    const-string v2, "item_lists"

    .line 579
    .local v2, table:Ljava/lang/String;
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v14, "continuation"

    aput-object v14, v3, v1

    const/4 v1, 0x1

    const-string v14, "title"

    aput-object v14, v3, v1

    const/4 v1, 0x2

    const-string v14, "description"

    aput-object v14, v3, v1

    .line 582
    .local v3, projection:[Ljava/lang/String;
    const-string v4, "account_name = ? AND id = ?"

    .line 583
    .local v4, selection:Ljava/lang/String;
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v14, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v14, v14, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v14, v5, v1

    const/4 v1, 0x1

    iget-object v14, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v14, v5, v1

    .line 586
    .local v5, selectionArgs:[Ljava/lang/String;
    const/4 v6, 0x0

    .line 587
    .local v6, groupBy:Ljava/lang/String;
    const/4 v7, 0x0

    .line 588
    .local v7, having:Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, sortOrder:Ljava/lang/String;
    move-object/from16 v1, p1

    .line 589
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 592
    .local v11, cursor:Landroid/database/Cursor;
    :try_start_42
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6d

    .line 593
    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 594
    .local v9, continuation:Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 595
    .local v13, title:Ljava/lang/String;
    const/4 v1, 0x2

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 596
    .local v12, description:Ljava/lang/String;
    const-string v1, "android.intent.extra.TITLE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v1, "com.google.reader.cursor.extra.DESCRIPTION"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    invoke-static {v10, v9}, Lcom/google/android/feeds/FeedLoader;->documentInfo(ILjava/lang/String;)Ljava/lang/Object;
    :try_end_68
    .catchall {:try_start_42 .. :try_end_68} :catchall_75

    move-result-object v1

    .line 603
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .end local v9           #continuation:Ljava/lang/String;
    .end local v12           #description:Ljava/lang/String;
    .end local v13           #title:Ljava/lang/String;
    :goto_6c
    return-object v1

    .line 600
    :cond_6d
    :try_start_6d
    invoke-static {v10}, Lcom/google/android/feeds/FeedLoader;->documentInfo(I)Ljava/lang/Object;
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_75

    move-result-object v1

    .line 603
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_6c

    :catchall_75
    move-exception v1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public getMaxPosition(Landroid/database/sqlite/SQLiteDatabase;)I
    .registers 13
    .parameter "db"

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 611
    const-string v1, "item_positions"

    .line 612
    .local v1, table:Ljava/lang/String;
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "MAX(position)"

    aput-object v0, v2, v9

    .line 615
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "account_name = ? AND item_list_id = ?"

    .line 616
    .local v3, selection:Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v4, v9

    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    aput-object v0, v4, v10

    .line 619
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 620
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 621
    .local v6, having:Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, sortOrder:Ljava/lang/String;
    move-object v0, p1

    .line 622
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 625
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_21
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 626
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2b
    .catchall {:try_start_21 .. :try_end_2b} :catchall_35

    move-result v0

    .line 631
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_2f
    return v0

    :cond_30
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v9

    goto :goto_2f

    :catchall_35
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getStreamId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    return-object v0
.end method

.method public insertTaggedItemPositions(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .registers 10
    .parameter "db"
    .parameter "userId"

    .prologue
    const/4 v6, 0x0

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ItemList;->excludesNonTagged()Z

    move-result v4

    if-nez v4, :cond_f

    .line 353
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Tried to filter unfiltered stream"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 355
    :cond_f
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/content/ItemList;->querytagNotInPositionTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 356
    .local v0, cursor:Landroid/database/Cursor;
    const/4 v2, 0x0

    .local v2, position:I
    :goto_14
    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_4a

    .line 357
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 358
    .local v1, itemid:Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 359
    .local v3, values:Landroid/content/ContentValues;
    const-string v4, "account_name"

    iget-object v5, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v5, v5, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v4, "item_list_id"

    iget-object v5, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v4, "item_id"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v4, "position"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 363
    const-string v4, "item_positions"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 356
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    .line 365
    .end local v1           #itemid:Ljava/lang/String;
    .end local v3           #values:Landroid/content/ContentValues;
    :cond_4a
    return-void
.end method

.method public insertUnreadItemPositions(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .registers 10
    .parameter "db"
    .parameter "userId"

    .prologue
    const/4 v6, 0x0

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ItemList;->excludesRead()Z

    move-result v4

    if-nez v4, :cond_f

    .line 280
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Tried to filter unfiltered stream"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 282
    :cond_f
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/content/ItemList;->queryUnreadNotInPositionTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 283
    .local v0, cursor:Landroid/database/Cursor;
    const/4 v2, 0x0

    .local v2, position:I
    :goto_14
    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_4a

    .line 284
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, itemid:Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 286
    .local v3, values:Landroid/content/ContentValues;
    const-string v4, "account_name"

    iget-object v5, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v5, v5, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v4, "item_list_id"

    iget-object v5, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v4, "item_id"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v4, "position"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 290
    const-string v4, "item_positions"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 283
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    .line 292
    .end local v1           #itemid:Ljava/lang/String;
    .end local v3           #values:Landroid/content/ContentValues;
    :cond_4a
    return-void
.end method

.method public isMagic()Z
    .registers 3

    .prologue
    .line 200
    const-string v0, "auto"

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isNewestFirstRanking()Z
    .registers 3

    .prologue
    .line 192
    const-string v0, "newest"

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isOldestFirstRanking()Z
    .registers 3

    .prologue
    .line 196
    const-string v0, "oldest"

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mRanking:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSearch()Z
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mQuery:Ljava/lang/String;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public notifyChange(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 663
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {p1, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->notifyChange(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 664
    return-void
.end method

.method public replaceItemList(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "db"
    .parameter "values"
    .parameter "timestamp"
    .parameter "date"
    .parameter "continuation"
    .parameter "title"
    .parameter "description"

    .prologue
    .line 438
    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    .line 439
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v2, v2, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v1, "timestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 442
    const-string v1, "date"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 443
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/content/ItemList;->copyTimestamps(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)V

    .line 444
    if-eqz p7, :cond_2f

    .line 445
    const-string v1, "continuation"

    invoke-virtual {p2, v1, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_2f
    if-eqz p8, :cond_36

    .line 448
    const-string v1, "title"

    invoke-virtual {p2, v1, p8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_36
    if-eqz p9, :cond_3d

    .line 451
    const-string v1, "description"

    invoke-virtual {p2, v1, p9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_3d
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    if-eqz v1, :cond_48

    .line 454
    const-string v1, "stream_id"

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemList;->mStreamId:Ljava/lang/String;

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_48
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    if-eqz v1, :cond_65

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mExcludeTarget:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isRead(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_65

    const/4 v0, 0x1

    .line 458
    .local v0, excludeRead:Z
    :goto_55
    const-string v1, "exclude_read"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 460
    const-string v1, "item_lists"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 461
    return-void

    .line 457
    .end local v0           #excludeRead:Z
    :cond_65
    const/4 v0, 0x0

    goto :goto_55
.end method

.method public setItemPosition(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;JI)V
    .registers 8
    .parameter "db"
    .parameter "values"
    .parameter "itemId"
    .parameter "position"

    .prologue
    .line 416
    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    .line 417
    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const-string v0, "item_list_id"

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemList;->mId:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string v0, "item_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 420
    const-string v0, "position"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 421
    const-string v0, "item_positions"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 422
    return-void
.end method
