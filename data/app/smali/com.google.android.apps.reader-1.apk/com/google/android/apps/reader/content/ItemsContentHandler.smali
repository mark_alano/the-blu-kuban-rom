.class Lcom/google/android/apps/reader/content/ItemsContentHandler;
.super Lcom/google/android/feeds/JsonContentHandler;
.source "ItemsContentHandler.java"


# static fields
.field protected static final ITEM_ALTERNATE:Ljava/lang/String; = "alternate"

.field protected static final ITEM_ANNOTATIONS:Ljava/lang/String; = "annotations"

.field protected static final ITEM_ANNOTATIONS_AUTHOR:Ljava/lang/String; = "author"

.field protected static final ITEM_ANNOTATIONS_CONTENT:Ljava/lang/String; = "content"

.field protected static final ITEM_ANNOTATIONS_PROFILE_ID:Ljava/lang/String; = "profileId"

.field protected static final ITEM_ANNOTATIONS_USER_ID:Ljava/lang/String; = "userId"

.field protected static final ITEM_AUTHOR:Ljava/lang/String; = "author"

.field protected static final ITEM_CATEGORIES:Ljava/lang/String; = "categories"

.field protected static final ITEM_CONTENT:Ljava/lang/String; = "content"

.field protected static final ITEM_CRAWL_TIME:Ljava/lang/String; = "crawlTimeMsec"

.field protected static final ITEM_CSS_URL:Ljava/lang/String; = "/android_res/raw/item_css"

.field protected static final ITEM_ENCLOSURE:Ljava/lang/String; = "enclosure"

.field protected static final ITEM_ID:Ljava/lang/String; = "id"

.field protected static final ITEM_LIKES:Ljava/lang/String; = "likingUsers"

.field protected static final ITEM_LOCKED:Ljava/lang/String; = "isReadStateLocked"

.field protected static final ITEM_ORIGIN:Ljava/lang/String; = "origin"

.field protected static final ITEM_PUBLISHED:Ljava/lang/String; = "published"

.field protected static final ITEM_RELATED:Ljava/lang/String; = "related"

.field protected static final ITEM_SUMMARY:Ljava/lang/String; = "summary"

.field protected static final ITEM_TITLE:Ljava/lang/String; = "title"

.field protected static final ITEM_UPDATED:Ljava/lang/String; = "updated"

.field protected static final JAVASCRIPT_URL:Ljava/lang/String; = "/android_res/raw/item_js"

.field protected static final LINK_HREF:Ljava/lang/String; = "href"

.field protected static final LINK_LENGTH:Ljava/lang/String; = "length"

.field protected static final LINK_TYPE:Ljava/lang/String; = "type"

.field protected static final ORIGIN_HTML_URL:Ljava/lang/String; = "htmlUrl"

.field protected static final ORIGIN_STREAM_ID:Ljava/lang/String; = "streamId"

.field protected static final ORIGIN_TITLE:Ljava/lang/String; = "title"

.field protected static final RELATIONS:[Ljava/lang/String; = null

.field protected static final STREAM_CONTINUATION:Ljava/lang/String; = "continuation"

.field protected static final STREAM_DESCRIPTION:Ljava/lang/String; = "description"

.field protected static final STREAM_ID:Ljava/lang/String; = "id"

.field protected static final STREAM_ITEMS:Ljava/lang/String; = "items"

.field protected static final STREAM_TITLE:Ljava/lang/String; = "title"

.field protected static final STREAM_UPDATED:Ljava/lang/String; = "updated"

.field protected static final TAG:Ljava/lang/String; = "ItemsContentHandler"

.field protected static final TEXT_CONTENT:Ljava/lang/String; = "content"


# instance fields
.field private final mAccount:Lcom/google/android/accounts/Account;

.field private final mContext:Landroid/content/Context;

.field private final mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private final mExtras:Landroid/os/Bundle;

.field private final mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

.field private mLiked:Ljava/lang/String;

.field private mLink:Ljava/lang/String;

.field private mPost:Ljava/lang/String;

.field private mRead:Ljava/lang/String;

.field private mShared:Ljava/lang/String;

.field private mStarred:Ljava/lang/String;

.field private mTableItemCategories:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTableItemLinks:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTableItems:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTimestamp:Ljava/lang/Long;

.field private mUserId:Ljava/lang/String;

.field private final mValues:Landroid/content/ContentValues;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 157
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "alternate"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "related"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "enclosure"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->RELATIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ReaderFileCache;Landroid/os/Bundle;)V
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "database"
    .parameter "fileCache"
    .parameter "extras"

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/feeds/JsonContentHandler;-><init>()V

    .line 215
    if-nez p1, :cond_d

    .line 216
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_d
    if-nez p2, :cond_17

    .line 219
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Account is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_17
    if-nez p3, :cond_21

    .line 222
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Database is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_21
    if-nez p4, :cond_2b

    .line 225
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "File cache is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_2b
    if-nez p5, :cond_35

    .line 228
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Extras bundle is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_35
    iput-object p1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mContext:Landroid/content/Context;

    .line 231
    iput-object p2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    .line 232
    iput-object p3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 233
    iput-object p4, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    .line 234
    iput-object p5, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mExtras:Landroid/os/Bundle;

    .line 235
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mValues:Landroid/content/ContentValues;

    .line 236
    return-void
.end method

.method private createInsertHelper(Ljava/lang/String;)Landroid/database/DatabaseUtils$InsertHelper;
    .registers 4
    .parameter "table"

    .prologue
    .line 648
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, v1, p1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    return-object v0
.end method

.method private deleteCategories(J)V
    .registers 7
    .parameter "itemId"

    .prologue
    .line 543
    const-string v1, "account_name = ? AND item_id = ?"

    .line 544
    .local v1, whereClause:Ljava/lang/String;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v0, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 547
    .local v0, whereArgs:[Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "item_categories"

    invoke-virtual {v2, v3, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 548
    return-void
.end method

.method private deleteLinks(J)V
    .registers 7
    .parameter "itemId"

    .prologue
    .line 597
    const-string v1, "account_name = ? AND item_id = ?"

    .line 598
    .local v1, whereClause:Ljava/lang/String;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v0, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 601
    .local v0, whereArgs:[Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "item_links"

    invoke-virtual {v2, v3, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 602
    return-void
.end method

.method private static escape(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "value"

    .prologue
    .line 531
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 532
    const-string v0, "<"

    const-string v1, "&lt;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 533
    const-string v0, ">"

    const-string v1, "&gt;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 534
    const-string v0, "\""

    const-string v1, "&quot;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 535
    return-object p0
.end method

.method private static getAlternateHref(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 4
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 351
    const-string v2, "alternate"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 352
    .local v0, alternate:Lorg/json/JSONArray;
    if-eqz v0, :cond_1a

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-eqz v2, :cond_1a

    .line 353
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 354
    .local v1, link:Lorg/json/JSONObject;
    const-string v2, "href"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 356
    .end local v1           #link:Lorg/json/JSONObject;
    :goto_19
    return-object v2

    :cond_1a
    const/4 v2, 0x0

    goto :goto_19
.end method

.method private static getEnclosureClass(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "type"

    .prologue
    .line 368
    if-eqz p0, :cond_d

    const-string v0, "audio/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 369
    const-string v0, "audio"

    .line 378
    :goto_c
    return-object v0

    .line 370
    :cond_d
    if-eqz p0, :cond_1a

    const-string v0, "image/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 371
    const-string v0, "image"

    goto :goto_c

    .line 372
    :cond_1a
    if-eqz p0, :cond_24

    const-string v0, "video/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2c

    :cond_24
    const-string v0, "application/x-shockwave-flash"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 376
    :cond_2c
    const-string v0, "video"

    goto :goto_c

    .line 378
    :cond_2f
    const-string v0, "other"

    goto :goto_c
.end method

.method private insertCategories(JLorg/json/JSONObject;)V
    .registers 12
    .parameter "itemId"
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 571
    const-string v6, "categories"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 572
    .local v0, categories:Lorg/json/JSONArray;
    if-eqz v0, :cond_19

    .line 573
    const/4 v2, 0x0

    .local v2, i:I
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    .local v3, n:I
    :goto_d
    if-ge v2, v3, :cond_19

    .line 574
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 575
    .local v1, category:Ljava/lang/String;
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->insertCategory(JLjava/lang/String;)V

    .line 573
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 579
    .end local v1           #category:Ljava/lang/String;
    .end local v2           #i:I
    .end local v3           #n:I
    :cond_19
    const-string v6, "origin"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 580
    .local v4, origin:Lorg/json/JSONObject;
    if-eqz v4, :cond_2d

    .line 585
    const-string v6, "streamId"

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 586
    .local v5, streamId:Ljava/lang/String;
    if-eqz v5, :cond_2d

    .line 587
    invoke-direct {p0, p1, p2, v5}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->insertCategory(JLjava/lang/String;)V

    .line 590
    .end local v5           #streamId:Ljava/lang/String;
    :cond_2d
    return-void
.end method

.method private insertCategory(JLjava/lang/String;)V
    .registers 7
    .parameter "itemId"
    .parameter "streamId"

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mValues:Landroid/content/ContentValues;

    .line 562
    .local v0, values:Landroid/content/ContentValues;
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 563
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v2, v2, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string v1, "item_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 565
    const-string v1, "stream_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemCategories:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 567
    return-void
.end method

.method private insertLink(JLjava/lang/String;Lorg/json/JSONObject;)V
    .registers 14
    .parameter "itemId"
    .parameter "rel"
    .parameter "link"

    .prologue
    .line 611
    iget-object v5, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mValues:Landroid/content/ContentValues;

    .line 613
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "href"

    invoke-virtual {p4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 614
    .local v2, href:Ljava/lang/String;
    const-string v6, "type"

    invoke-virtual {p4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 615
    .local v4, type:Ljava/lang/String;
    const-string v6, "length"

    invoke-virtual {p4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 619
    .local v3, length:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v7, p1

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6e

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_31
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-long v0, v6

    .line 621
    .local v0, baseId:J
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 622
    const-string v6, "account_name"

    iget-object v7, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v7, v7, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    const-string v6, "item_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 624
    const-string v6, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 625
    const-string v6, "rel"

    invoke-virtual {v5, v6, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    const-string v6, "href"

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const-string v6, "type"

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const-string v6, "length"

    invoke-virtual {v5, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    iget-object v6, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemLinks:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v6, v5}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 630
    return-void

    .line 619
    .end local v0           #baseId:J
    :cond_6e
    const/4 v6, 0x0

    goto :goto_31
.end method

.method private insertLinks(JLorg/json/JSONObject;)V
    .registers 12
    .parameter "itemId"
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 636
    sget-object v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->RELATIONS:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_4
    if-ge v2, v3, :cond_22

    aget-object v7, v0, v2

    .line 637
    .local v7, rel:Ljava/lang/String;
    invoke-virtual {p3, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 638
    .local v5, links:Lorg/json/JSONArray;
    if-eqz v5, :cond_1f

    .line 639
    const/4 v1, 0x0

    .local v1, i:I
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .local v6, n:I
    :goto_13
    if-ge v1, v6, :cond_1f

    .line 640
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 641
    .local v4, link:Lorg/json/JSONObject;
    invoke-direct {p0, p1, p2, v7, v4}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->insertLink(JLjava/lang/String;Lorg/json/JSONObject;)V

    .line 639
    add-int/lit8 v1, v1, 0x1

    goto :goto_13

    .line 636
    .end local v1           #i:I
    .end local v4           #link:Lorg/json/JSONObject;
    .end local v6           #n:I
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 645
    .end local v5           #links:Lorg/json/JSONArray;
    .end local v7           #rel:Ljava/lang/String;
    :cond_22
    return-void
.end method

.method private invalidateUnreadCounts()V
    .registers 11

    .prologue
    .line 772
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 773
    .local v4, values:Landroid/content/ContentValues;
    const-string v7, "timestamp"

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 775
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 776
    .local v0, now:J
    const-wide/32 v7, 0xea60

    sub-long v2, v0, v7

    .line 777
    .local v2, oneMinuteAgo:J
    const-string v6, "url = ? AND account_name = ? AND (timestamp < ? OR timestamp > ?)"

    .line 778
    .local v6, whereClause:Ljava/lang/String;
    const/4 v7, 0x4

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->unreadCount()Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v8, v8, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v5, v7

    const/4 v7, 0x2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x3

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    .line 782
    .local v5, whereArgs:[Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "timestamps"

    invoke-virtual {v7, v8, v4, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 783
    return-void
.end method

.method private notifyChange()V
    .registers 3

    .prologue
    .line 652
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->notifyChange(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 653
    return-void
.end method

.method private replaceItem(JLorg/json/JSONObject;)V
    .registers 47
    .parameter "itemId"
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTimestamp:Ljava/lang/Long;

    move-object/from16 v39, v0

    if-nez v39, :cond_10

    .line 240
    new-instance v39, Ljava/lang/IllegalStateException;

    const-string v40, "Timestamp was not set"

    invoke-direct/range {v39 .. v40}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 242
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mUserId:Ljava/lang/String;

    move-object/from16 v39, v0

    if-nez v39, :cond_20

    .line 243
    new-instance v39, Ljava/lang/IllegalStateException;

    const-string v40, "User ID was not set"

    invoke-direct/range {v39 .. v40}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 245
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mValues:Landroid/content/ContentValues;

    move-object/from16 v38, v0

    .line 248
    .local v38, values:Landroid/content/ContentValues;
    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    .line 249
    .local v8, baseId:J
    const-string v39, "id"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 250
    .local v15, externalId:Ljava/lang/String;
    const-string v39, "title"

    const-string v40, ""

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 251
    .local v34, title:Ljava/lang/String;
    const-string v39, "crawlTimeMsec"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 252
    .local v14, crawlTime:Ljava/lang/String;
    const-string v39, "published"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v39

    const-wide/16 v41, 0x3e8

    mul-long v26, v39, v41

    .line 253
    .local v26, published:J
    const-string v39, "updated"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v39

    const-wide/16 v41, 0x3e8

    mul-long v35, v39, v41

    .line 254
    .local v35, updated:J
    const-string v39, "isReadStateLocked"

    const/16 v40, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v22

    .line 256
    .local v22, locked:Z
    const-string v39, "origin"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    .line 257
    .local v23, origin:Lorg/json/JSONObject;
    const-string v39, "title"

    const-string v40, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 258
    .local v32, sourceTitle:Ljava/lang/String;
    const-string v39, "htmlUrl"

    const/16 v40, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 259
    .local v30, sourceAlternateHref:Ljava/lang/String;
    const-string v39, "streamId"

    const/16 v40, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 261
    .local v31, sourceStreamId:Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->getAlternateHref(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    .line 263
    .local v3, alternateHref:Ljava/lang/String;
    const-string v39, "author"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 265
    .local v7, author:Ljava/lang/String;
    const-string v39, "likingUsers"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v20

    .line 266
    .local v20, likingUsers:Lorg/json/JSONArray;
    if-eqz v20, :cond_102

    invoke-virtual/range {v20 .. v20}, Lorg/json/JSONArray;->length()I

    move-result v18

    .line 268
    .local v18, likeCount:I
    :goto_c8
    const/16 v28, 0x0

    .line 269
    .local v28, read:Z
    const/16 v33, 0x0

    .line 270
    .local v33, starred:Z
    const/16 v19, 0x0

    .line 271
    .local v19, liked:Z
    const/16 v29, 0x0

    .line 272
    .local v29, shared:Z
    const/16 v21, 0x0

    .line 273
    .local v21, link:Z
    const/16 v25, 0x0

    .line 274
    .local v25, post:Z
    const/4 v10, 0x0

    .line 275
    .local v10, broadcaster:Ljava/lang/String;
    const-string v39, "categories"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 276
    .local v11, categories:Lorg/json/JSONArray;
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v12

    .line 277
    .local v12, categoriesCount:I
    const/16 v17, 0x0

    .local v17, index:I
    :goto_e5
    move/from16 v0, v17

    if-ge v0, v12, :cond_161

    .line 278
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 279
    .local v13, category:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mStarred:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_105

    .line 280
    const/16 v33, 0x1

    .line 277
    :cond_ff
    :goto_ff
    add-int/lit8 v17, v17, 0x1

    goto :goto_e5

    .line 266
    .end local v10           #broadcaster:Ljava/lang/String;
    .end local v11           #categories:Lorg/json/JSONArray;
    .end local v12           #categoriesCount:I
    .end local v13           #category:Ljava/lang/String;
    .end local v17           #index:I
    .end local v18           #likeCount:I
    .end local v19           #liked:Z
    .end local v21           #link:Z
    .end local v25           #post:Z
    .end local v28           #read:Z
    .end local v29           #shared:Z
    .end local v33           #starred:Z
    :cond_102
    const/16 v18, 0x0

    goto :goto_c8

    .line 281
    .restart local v10       #broadcaster:Ljava/lang/String;
    .restart local v11       #categories:Lorg/json/JSONArray;
    .restart local v12       #categoriesCount:I
    .restart local v13       #category:Ljava/lang/String;
    .restart local v17       #index:I
    .restart local v18       #likeCount:I
    .restart local v19       #liked:Z
    .restart local v21       #link:Z
    .restart local v25       #post:Z
    .restart local v28       #read:Z
    .restart local v29       #shared:Z
    .restart local v33       #starred:Z
    :cond_105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mLiked:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_116

    .line 282
    const/16 v19, 0x1

    goto :goto_ff

    .line 283
    :cond_116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mLink:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_127

    .line 284
    const/16 v21, 0x1

    goto :goto_ff

    .line 285
    :cond_127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mPost:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_138

    .line 286
    const/16 v25, 0x1

    goto :goto_ff

    .line 287
    :cond_138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mRead:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_149

    .line 288
    const/16 v28, 0x1

    goto :goto_ff

    .line 289
    :cond_149
    invoke-static {v13}, Lcom/google/android/apps/reader/provider/ReaderStream;->isBroadcast(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_ff

    .line 290
    move-object v10, v13

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mShared:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_ff

    .line 293
    const/16 v29, 0x1

    goto :goto_ff

    .line 298
    .end local v13           #category:Ljava/lang/String;
    :cond_161
    const/4 v5, 0x0

    .line 299
    .local v5, annotationContent:Ljava/lang/String;
    if-eqz v10, :cond_1a6

    .line 300
    const-string v39, "annotations"

    move-object/from16 v0, p3

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 301
    .local v6, annotations:Lorg/json/JSONArray;
    const/16 v16, 0x0

    .local v16, i:I
    :goto_170
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v39

    move/from16 v0, v16

    move/from16 v1, v39

    if-ge v0, v1, :cond_1a6

    .line 302
    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 306
    .local v4, annotation:Lorg/json/JSONObject;
    const-string v39, "userId"

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 307
    .local v37, userId:Ljava/lang/String;
    if-eqz v37, :cond_2b3

    invoke-static/range {v37 .. v37}, Lcom/google/android/apps/reader/provider/ReaderStream;->createBroadcastId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_2b3

    .line 308
    const-string v39, "content"

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 310
    .local v24, possibleAnnotationContent:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v39

    if-nez v39, :cond_2b3

    .line 311
    move-object/from16 v5, v24

    .line 318
    .end local v4           #annotation:Lorg/json/JSONObject;
    .end local v6           #annotations:Lorg/json/JSONArray;
    .end local v16           #i:I
    .end local v24           #possibleAnnotationContent:Ljava/lang/String;
    .end local v37           #userId:Ljava/lang/String;
    :cond_1a6
    invoke-virtual/range {v38 .. v38}, Landroid/content/ContentValues;->clear()V

    .line 319
    const-string v39, "account_name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v39, "timestamp"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTimestamp:Ljava/lang/Long;

    move-object/from16 v40, v0

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 321
    const-string v39, "_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 322
    const-string v39, "id"

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 323
    const-string v39, "external_id"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const-string v39, "crawl_time"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-string v39, "title"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v39, "title_plaintext"

    invoke-static/range {v34 .. v34}, Lcom/google/android/apps/reader/content/PlainText;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const-string v39, "author"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v39, "alternate_href"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-string v39, "source_title"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v39, "source_title_plaintext"

    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/reader/content/PlainText;->valueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v39, "source_alternate_href"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v39, "source_stream_id"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v39, "published"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 334
    const-string v39, "updated"

    invoke-static/range {v35 .. v36}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 335
    const-string v39, "starred"

    invoke-static/range {v33 .. v33}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 336
    const-string v39, "read"

    invoke-static/range {v28 .. v28}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 337
    const-string v39, "liked"

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 338
    const-string v39, "shared"

    invoke-static/range {v29 .. v29}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 339
    const-string v39, "source_link"

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 340
    const-string v39, "source_post"

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 341
    const-string v39, "locked"

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 342
    const-string v39, "like_count"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 343
    const-string v39, "broadcaster"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v39, "annotation"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItems:Landroid/database/DatabaseUtils$InsertHelper;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->replace(Landroid/content/ContentValues;)J

    .line 348
    return-void

    .line 301
    .restart local v4       #annotation:Lorg/json/JSONObject;
    .restart local v6       #annotations:Lorg/json/JSONArray;
    .restart local v16       #i:I
    .restart local v37       #userId:Ljava/lang/String;
    :cond_2b3
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_170
.end method

.method private replaceItemContent(JLorg/json/JSONObject;)V
    .registers 36
    .parameter "itemId"
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mValues:Landroid/content/ContentValues;

    move-object/from16 v26, v0

    .line 384
    .local v26, values:Landroid/content/ContentValues;
    const-string v29, "summary"

    move-object/from16 v0, p3

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v22

    .line 385
    .local v22, summary:Lorg/json/JSONObject;
    const-string v29, "content"

    move-object/from16 v0, p3

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 386
    .local v5, content:Lorg/json/JSONObject;
    const-string v29, "title"

    const-string v30, ""

    move-object/from16 v0, p3

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 387
    .local v23, title:Ljava/lang/String;
    const-string v29, "origin"

    move-object/from16 v0, p3

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 388
    .local v17, origin:Lorg/json/JSONObject;
    const-string v29, "title"

    const-string v30, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 389
    .local v21, sourceTitle:Ljava/lang/String;
    const-string v29, "htmlUrl"

    const/16 v30, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 390
    .local v20, sourceAlternateHref:Ljava/lang/String;
    invoke-virtual/range {v26 .. v26}, Landroid/content/ContentValues;->clear()V

    .line 391
    const-string v29, "account_name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string v29, "item_id"

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v26

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 393
    const/4 v11, 0x0

    .line 394
    .local v11, html:Ljava/lang/String;
    if-nez v11, :cond_84

    if-eqz v5, :cond_84

    .line 395
    const-string v29, "content"

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 397
    :cond_84
    if-nez v11, :cond_92

    if-eqz v22, :cond_92

    .line 398
    const-string v29, "content"

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 400
    :cond_92
    if-nez v11, :cond_96

    .line 401
    const-string v11, ""

    .line 403
    :cond_96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mAccount:Lcom/google/android/accounts/Account;

    move-object/from16 v30, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-wide/from16 v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/reader/content/ReaderFileCache;->getItemHtmlFile(Lcom/google/android/accounts/Account;J)Ljava/io/File;

    move-result-object v9

    .line 404
    .local v9, file:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v19

    .line 405
    .local v19, parent:Ljava/io/File;
    if-eqz v19, :cond_da

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v29

    if-nez v29, :cond_da

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdirs()Z

    move-result v29

    if-nez v29, :cond_da

    .line 406
    const-string v29, "ItemsContentHandler"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Unable to make parent directory: "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_da
    :try_start_da
    new-instance v18, Ljava/io/FileOutputStream;

    move-object/from16 v0, v18

    invoke-direct {v0, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_e1
    .catch Ljava/io/IOException; {:try_start_da .. :try_end_e1} :catch_275

    .line 411
    .local v18, out:Ljava/io/OutputStream;
    :try_start_e1
    new-instance v27, Ljava/io/OutputStreamWriter;

    const-string v29, "UTF-8"

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 412
    .local v27, writer:Ljava/io/Writer;
    new-instance v28, Ljava/io/BufferedWriter;

    const/16 v29, 0x2000

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 413
    .end local v27           #writer:Ljava/io/Writer;
    .local v28, writer:Ljava/io/Writer;
    const-string v29, "<html>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 414
    const-string v29, "<head>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 417
    const-string v29, "<meta http-equiv=\'content-type\' content=\'text/html;charset="

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 418
    const-string v29, "UTF-8"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 419
    const-string v29, "\' />"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 422
    const-string v29, "<meta name=\"viewport\" content=\"user-scalable=no\" />"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 423
    const-string v29, "<meta name=\"viewport\" content=\"width=device-width\" />"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 424
    const-string v29, "<meta name=\"viewport\" content=\"initial-scale=1.0\" />"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 425
    const-string v29, "<meta name=\"viewport\" content=\"maximum-scale=1.0\" />"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 429
    const-string v29, "<meta name=\"viewport\" content=\"target-densitydpi=medium-dpi\" />"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 432
    const-string v29, "<link rel=\'stylesheet\' type=\'text/css\' href=\'"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 433
    const-string v29, "/android_res/raw/item_css"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 434
    const-string v29, "\' />"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 436
    const-string v29, "</head>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 437
    const-string v29, "<body>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 438
    const-string v29, "<div id=\'item\'>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 439
    const-string v29, "<div id=\'title\'>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 440
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->getAlternateHref(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v4

    .line 441
    .local v4, alternateHref:Ljava/lang/String;
    if-eqz v4, :cond_267

    .line 442
    const-string v29, "<a href=\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 443
    invoke-static {v4}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 444
    const-string v29, "\">"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 445
    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 446
    const-string v29, "</a>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 450
    :goto_173
    const-string v29, "</div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 451
    const-string v29, "<div id=\'subtitle\'>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 452
    if-eqz v20, :cond_285

    .line 453
    const-string v29, "<a href=\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 454
    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 455
    const-string v29, "\">"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 456
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 457
    const-string v29, "</a>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 461
    :goto_19c
    const-string v29, "</div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 462
    const-string v29, "<div id=\'liked\'></div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 463
    const-string v29, "<div id=\'shared\'></div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 464
    const-string v29, "<div id=\'annotation\'></div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 465
    const-string v29, "<div id=\'content\'>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 469
    const-string v29, "<!--[HTML]-->"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 470
    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 471
    const-string v29, "<!--[/HTML]-->"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 473
    const-string v29, "</div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 474
    const-string v29, "enclosure"

    move-object/from16 v0, p3

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 475
    .local v8, enclosures:Lorg/json/JSONArray;
    if-eqz v8, :cond_29f

    .line 476
    const-string v29, "<div id=\'enclosures\'>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 477
    const-string v29, "<ul>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 478
    const/4 v12, 0x0

    .local v12, i:I
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v15

    .local v15, n:I
    :goto_1e4
    if-ge v12, v15, :cond_295

    .line 479
    invoke-virtual {v8, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 480
    .local v7, enclosure:Lorg/json/JSONObject;
    const-string v29, "href"

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 481
    .local v10, href:Ljava/lang/String;
    const-string v29, "type"

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 482
    .local v24, type:Ljava/lang/String;
    if-eqz v10, :cond_263

    .line 483
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    .line 484
    .local v25, uri:Landroid/net/Uri;
    new-instance v13, Landroid/content/Intent;

    const-string v29, "android.intent.action.VIEW"

    move-object/from16 v0, v29

    invoke-direct {v13, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 485
    .local v13, intent:Landroid/content/Intent;
    if-eqz v24, :cond_28e

    .line 486
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 490
    :goto_212
    invoke-virtual/range {v25 .. v25}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v16

    .line 491
    .local v16, name:Ljava/lang/String;
    if-nez v16, :cond_21a

    .line 493
    move-object/from16 v16, v10

    .line 495
    :cond_21a
    const/16 v29, 0x1

    move/from16 v0, v29

    invoke-virtual {v13, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v14

    .line 496
    .local v14, intentUri:Ljava/lang/String;
    const-string v29, "<li class=\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 497
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->getEnclosureClass(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 498
    const-string v29, "\"><a href=\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 499
    invoke-static {v14}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 500
    const-string v29, "\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 501
    if-eqz v24, :cond_252

    .line 502
    const-string v29, " type=\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 503
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 504
    const-string v29, "\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 506
    :cond_252
    const-string v29, ">"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 507
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 508
    const-string v29, "</a></li>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 478
    .end local v13           #intent:Landroid/content/Intent;
    .end local v14           #intentUri:Ljava/lang/String;
    .end local v16           #name:Ljava/lang/String;
    .end local v25           #uri:Landroid/net/Uri;
    :cond_263
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1e4

    .line 448
    .end local v7           #enclosure:Lorg/json/JSONObject;
    .end local v8           #enclosures:Lorg/json/JSONArray;
    .end local v10           #href:Ljava/lang/String;
    .end local v12           #i:I
    .end local v15           #n:I
    .end local v24           #type:Ljava/lang/String;
    :cond_267
    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_26e
    .catchall {:try_start_e1 .. :try_end_26e} :catchall_270

    goto/16 :goto_173

    .line 522
    .end local v4           #alternateHref:Ljava/lang/String;
    .end local v28           #writer:Ljava/io/Writer;
    :catchall_270
    move-exception v29

    :try_start_271
    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V

    throw v29
    :try_end_275
    .catch Ljava/io/IOException; {:try_start_271 .. :try_end_275} :catch_275

    .line 524
    .end local v18           #out:Ljava/io/OutputStream;
    :catch_275
    move-exception v6

    .line 525
    .local v6, e:Ljava/io/IOException;
    const-string v29, "ItemsContentHandler"

    const-string v30, "Failed to write HTML to file"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 526
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 528
    .end local v6           #e:Ljava/io/IOException;
    :goto_284
    return-void

    .line 459
    .restart local v4       #alternateHref:Ljava/lang/String;
    .restart local v18       #out:Ljava/io/OutputStream;
    .restart local v28       #writer:Ljava/io/Writer;
    :cond_285
    :try_start_285
    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_19c

    .line 488
    .restart local v7       #enclosure:Lorg/json/JSONObject;
    .restart local v8       #enclosures:Lorg/json/JSONArray;
    .restart local v10       #href:Ljava/lang/String;
    .restart local v12       #i:I
    .restart local v13       #intent:Landroid/content/Intent;
    .restart local v15       #n:I
    .restart local v24       #type:Ljava/lang/String;
    .restart local v25       #uri:Landroid/net/Uri;
    :cond_28e
    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_212

    .line 511
    .end local v7           #enclosure:Lorg/json/JSONObject;
    .end local v10           #href:Ljava/lang/String;
    .end local v13           #intent:Landroid/content/Intent;
    .end local v24           #type:Ljava/lang/String;
    .end local v25           #uri:Landroid/net/Uri;
    :cond_295
    const-string v29, "</ul>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 512
    const-string v29, "</div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 514
    .end local v12           #i:I
    .end local v15           #n:I
    :cond_29f
    const-string v29, "</div>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 515
    const-string v29, "<script type=\"text/javascript\" src=\""

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 516
    const-string v29, "/android_res/raw/item_js"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 517
    const-string v29, "\"></script>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 518
    const-string v29, "</body>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 519
    const-string v29, "</html>"

    invoke-virtual/range {v28 .. v29}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 520
    invoke-virtual/range {v28 .. v28}, Ljava/io/Writer;->flush()V
    :try_end_2c0
    .catchall {:try_start_285 .. :try_end_2c0} :catchall_270

    .line 522
    :try_start_2c0
    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V
    :try_end_2c3
    .catch Ljava/io/IOException; {:try_start_2c0 .. :try_end_2c3} :catch_275

    goto :goto_284
.end method

.method private setUserId(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "category"

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mUserId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getContent(Ljava/lang/String;)Ljava/lang/Object;
    .registers 6
    .parameter "source"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 704
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 705
    .local v1, feed:Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 707
    :try_start_a
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->handleFeed(Lorg/json/JSONObject;)Ljava/lang/Object;

    move-result-object v0

    .line 715
    .local v0, content:Ljava/lang/Object;
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->invalidateUnreadCounts()V

    .line 717
    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_16
    .catchall {:try_start_a .. :try_end_16} :catchall_1c

    .line 721
    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object v0

    .end local v0           #content:Ljava/lang/Object;
    :catchall_1c
    move-exception v2

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public getContent(Ljava/net/URLConnection;)Ljava/lang/Object;
    .registers 6
    .parameter "connection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 659
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTimestamp:Ljava/lang/Long;

    .line 662
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mExtras:Landroid/os/Bundle;

    const-string v2, "com.google.reader.cursor.extra.USER_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mUserId:Ljava/lang/String;

    .line 663
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mUserId:Ljava/lang/String;

    if-nez v1, :cond_21

    .line 664
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "User ID is not set"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 667
    :cond_21
    const-string v1, "user/-/state/com.google/read"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->setUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mRead:Ljava/lang/String;

    .line 668
    const-string v1, "user/-/state/com.google/starred"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->setUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mStarred:Ljava/lang/String;

    .line 669
    const-string v1, "user/-/state/com.google/like"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->setUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mLiked:Ljava/lang/String;

    .line 670
    const-string v1, "user/-/state/com.google/broadcast"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->setUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mShared:Ljava/lang/String;

    .line 671
    const-string v1, "user/-/source/com.google/link"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->setUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mLink:Ljava/lang/String;

    .line 672
    const-string v1, "user/-/source/com.google/post"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->setUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mPost:Ljava/lang/String;

    .line 675
    :try_start_51
    const-string v1, "items"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->createInsertHelper(Ljava/lang/String;)Landroid/database/DatabaseUtils$InsertHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItems:Landroid/database/DatabaseUtils$InsertHelper;

    .line 676
    const-string v1, "item_categories"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->createInsertHelper(Ljava/lang/String;)Landroid/database/DatabaseUtils$InsertHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemCategories:Landroid/database/DatabaseUtils$InsertHelper;

    .line 677
    const-string v1, "item_links"

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->createInsertHelper(Ljava/lang/String;)Landroid/database/DatabaseUtils$InsertHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemLinks:Landroid/database/DatabaseUtils$InsertHelper;

    .line 679
    invoke-super {p0, p1}, Lcom/google/android/feeds/JsonContentHandler;->getContent(Ljava/net/URLConnection;)Ljava/lang/Object;

    move-result-object v0

    .line 680
    .local v0, content:Ljava/lang/Object;
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->notifyChange()V
    :try_end_70
    .catchall {:try_start_51 .. :try_end_70} :catchall_86

    .line 683
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItems:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 684
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemCategories:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 685
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemLinks:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 687
    iput-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItems:Landroid/database/DatabaseUtils$InsertHelper;

    .line 688
    iput-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemCategories:Landroid/database/DatabaseUtils$InsertHelper;

    .line 689
    iput-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemLinks:Landroid/database/DatabaseUtils$InsertHelper;

    return-object v0

    .line 683
    .end local v0           #content:Ljava/lang/Object;
    :catchall_86
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItems:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v2}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 684
    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemCategories:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v2}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 685
    iget-object v2, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemLinks:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v2}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 687
    iput-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItems:Landroid/database/DatabaseUtils$InsertHelper;

    .line 688
    iput-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemCategories:Landroid/database/DatabaseUtils$InsertHelper;

    .line 689
    iput-object v3, p0, Lcom/google/android/apps/reader/content/ItemsContentHandler;->mTableItemLinks:Landroid/database/DatabaseUtils$InsertHelper;

    throw v1
.end method

.method protected handleFeed(Lorg/json/JSONObject;)Ljava/lang/Object;
    .registers 7
    .parameter "feed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 739
    const-string v4, "items"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 740
    .local v3, items:Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 741
    .local v2, itemCount:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_b
    if-ge v0, v2, :cond_17

    .line 742
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 743
    .local v1, item:Lorg/json/JSONObject;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->handleItem(Lorg/json/JSONObject;)V

    .line 741
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 745
    .end local v1           #item:Lorg/json/JSONObject;
    :cond_17
    invoke-static {v2}, Lcom/google/android/feeds/FeedLoader;->documentInfo(I)Ljava/lang/Object;

    move-result-object v4

    return-object v4
.end method

.method protected handleItem(Lorg/json/JSONObject;)V
    .registers 6
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 749
    const-string v3, "id"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 750
    .local v0, externalId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderItem;->getItemId(Ljava/lang/String;)J

    move-result-wide v1

    .line 751
    .local v1, itemId:J
    invoke-direct {p0, v1, v2, p1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->replaceItem(JLorg/json/JSONObject;)V

    .line 752
    invoke-direct {p0, v1, v2, p1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->replaceItemContent(JLorg/json/JSONObject;)V

    .line 754
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->deleteCategories(J)V

    .line 755
    invoke-direct {p0, v1, v2, p1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->insertCategories(JLorg/json/JSONObject;)V

    .line 757
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->deleteLinks(J)V

    .line 758
    invoke-direct {p0, v1, v2, p1}, Lcom/google/android/apps/reader/content/ItemsContentHandler;->insertLinks(JLorg/json/JSONObject;)V

    .line 759
    return-void
.end method
