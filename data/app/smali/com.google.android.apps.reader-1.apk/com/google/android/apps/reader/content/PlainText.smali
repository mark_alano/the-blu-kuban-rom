.class Lcom/google/android/apps/reader/content/PlainText;
.super Ljava/lang/Object;
.source "PlainText.java"


# static fields
.field private static final HTML_CHARACTERS:Ljava/lang/String; = "&<>\r\n"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    return-void
.end method

.method static isPlainText(Ljava/lang/String;)Z
    .registers 6
    .parameter "source"

    .prologue
    const/4 v2, 0x0

    .line 39
    const/4 v1, 0x0

    .local v1, i:I
    :goto_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2d

    .line 40
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 43
    .local v0, c:C
    const-string v3, "&<>\r\n"

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_16

    .line 52
    .end local v0           #c:C
    :cond_15
    :goto_15
    return v2

    .line 48
    .restart local v0       #c:C
    :cond_16
    if-lez v1, :cond_2a

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-eqz v3, :cond_2a

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-nez v3, :cond_15

    .line 39
    :cond_2a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 52
    .end local v0           #c:C
    :cond_2d
    const/4 v2, 0x1

    goto :goto_15
.end method

.method public static valueOf(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "source"

    .prologue
    .line 62
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 63
    invoke-static {p0}, Lcom/google/android/apps/reader/content/PlainText;->isPlainText(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 68
    .end local p0
    :goto_a
    return-object p0

    .restart local p0
    :cond_b
    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_a
.end method
