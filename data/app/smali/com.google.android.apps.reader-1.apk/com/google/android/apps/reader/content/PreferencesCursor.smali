.class Lcom/google/android/apps/reader/content/PreferencesCursor;
.super Landroid/database/AbstractCursor;
.source "PreferencesCursor.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private final mChanges:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCursor:Landroid/database/Cursor;

.field private final mOnSharedPreferenceChangeListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/os/ResultReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreferences:Landroid/content/SharedPreferences;

.field private final mProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;Landroid/content/SharedPreferences;)V
    .registers 5
    .parameter "projection"
    .parameter "preferences"

    .prologue
    .line 87
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 78
    invoke-static {}, Lcom/google/android/apps/reader/content/PreferencesCursor;->createSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mChanges:Ljava/util/Set;

    .line 80
    invoke-static {}, Lcom/google/android/apps/reader/content/PreferencesCursor;->createSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mOnSharedPreferenceChangeListeners:Ljava/util/Set;

    .line 88
    if-nez p1, :cond_19

    .line 89
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Project is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_19
    if-nez p2, :cond_23

    .line 92
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Preferences are null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_23
    iput-object p1, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mProjection:[Ljava/lang/String;

    .line 95
    iput-object p2, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mPreferences:Landroid/content/SharedPreferences;

    .line 96
    invoke-static {p1, p2}, Lcom/google/android/apps/reader/content/PreferencesCursor;->copyToCursor([Ljava/lang/String;Landroid/content/SharedPreferences;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mCursor:Landroid/database/Cursor;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 98
    return-void
.end method

.method private static copyToCursor([Ljava/lang/String;Landroid/content/SharedPreferences;)Landroid/database/Cursor;
    .registers 13
    .parameter "projection"
    .parameter "preferences"

    .prologue
    .line 48
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, p0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 49
    .local v2, cursor:Landroid/database/MatrixCursor;
    invoke-interface {p1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_11
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_64

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 50
    .local v3, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-virtual {v2}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v8

    .line 51
    .local v8, row:Landroid/database/MatrixCursor$RowBuilder;
    move-object v0, p0

    .local v0, arr$:[Ljava/lang/String;
    array-length v7, v0

    .local v7, len$:I
    const/4 v5, 0x0

    .local v5, i$:I
    :goto_24
    if-ge v5, v7, :cond_11

    aget-object v1, v0, v5

    .line 52
    .local v1, columnName:Ljava/lang/String;
    const-string v10, "key"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3c

    .line 53
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 54
    .local v6, key:Ljava/lang/String;
    invoke-virtual {v8, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 51
    .end local v6           #key:Ljava/lang/String;
    :goto_39
    add-int/lit8 v5, v5, 0x1

    goto :goto_24

    .line 55
    :cond_3c
    const-string v10, "value"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5f

    .line 56
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    .line 57
    .local v9, value:Ljava/lang/Object;
    instance-of v10, v9, Ljava/lang/Boolean;

    if-eqz v10, :cond_59

    .line 59
    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v10, v9}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5d

    const/4 v10, 0x1

    :goto_55
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 61
    .end local v9           #value:Ljava/lang/Object;
    :cond_59
    invoke-virtual {v8, v9}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_39

    .line 59
    .restart local v9       #value:Ljava/lang/Object;
    :cond_5d
    const/4 v10, 0x0

    goto :goto_55

    .line 63
    .end local v9           #value:Ljava/lang/Object;
    :cond_5f
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_39

    .line 67
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #columnName:Ljava/lang/String;
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v5           #i$:I
    .end local v7           #len$:I
    .end local v8           #row:Landroid/database/MatrixCursor$RowBuilder;
    :cond_64
    return-object v2
.end method

.method private static createSet()Ljava/util/Set;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    return-object v0
.end method

.method private getPositionedCursor()Landroid/database/Cursor;
    .registers 4

    .prologue
    .line 106
    iget v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mPos:I

    .line 107
    .local v0, index:I
    iget-object v2, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 108
    iget-object v2, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mCursor:Landroid/database/Cursor;

    return-object v2

    .line 110
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getCount()I

    move-result v1

    .line 111
    .local v1, size:I
    new-instance v2, Landroid/database/CursorIndexOutOfBoundsException;

    invoke-direct {v2, v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(II)V

    throw v2
.end method

.method private notifyOnSharedPreferenceChangeListeners(Ljava/util/Set;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, changes:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_31

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 168
    .local v2, key:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mOnSharedPreferenceChangeListeners:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/ResultReceiver;

    .line 169
    .local v3, receiver:Landroid/os/ResultReceiver;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 170
    .local v4, resultData:Landroid/os/Bundle;
    const-string v5, "key"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto :goto_16

    .line 174
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #receiver:Landroid/os/ResultReceiver;
    .end local v4           #resultData:Landroid/os/Bundle;
    :cond_31
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 197
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 198
    return-void
.end method

.method public getColumnNames()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mProjection:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .registers 4
    .parameter "column"

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getPositionedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .registers 3
    .parameter "column"

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getPositionedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .registers 3
    .parameter "column"

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getPositionedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .registers 4
    .parameter "column"

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getPositionedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getShort(I)S
    .registers 3
    .parameter "column"

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getPositionedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .registers 3
    .parameter "column"

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getPositionedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNull(I)Z
    .registers 3
    .parameter "column"

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->getPositionedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 5
    .parameter "sharedPreferences"
    .parameter "key"

    .prologue
    .line 207
    iget-object v1, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mChanges:Ljava/util/Set;

    monitor-enter v1

    .line 208
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mChanges:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 209
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_e

    .line 210
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->onChange(Z)V

    .line 211
    return-void

    .line 209
    :catchall_e
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public requery()Z
    .registers 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mProjection:[Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mPreferences:Landroid/content/SharedPreferences;

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/content/PreferencesCursor;->copyToCursor([Ljava/lang/String;Landroid/content/SharedPreferences;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mCursor:Landroid/database/Cursor;

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mChanges:Ljava/util/Set;

    monitor-enter v1

    .line 159
    :try_start_d
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mChanges:Ljava/util/Set;

    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/content/PreferencesCursor;->notifyOnSharedPreferenceChangeListeners(Ljava/util/Set;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mChanges:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 161
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_d .. :try_end_18} :catchall_1d

    .line 163
    invoke-super {p0}, Landroid/database/AbstractCursor;->requery()Z

    move-result v0

    return v0

    .line 161
    :catchall_1d
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 4
    .parameter "extras"

    .prologue
    .line 180
    const-class v1, Lcom/google/android/apps/reader/provider/ReaderContract;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 182
    const-string v1, "registerOnSharedPreferenceChangeListener"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ResultReceiver;

    .line 183
    .local v0, receiver:Landroid/os/ResultReceiver;
    if-eqz v0, :cond_18

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/reader/content/PreferencesCursor;->mOnSharedPreferenceChangeListeners:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_18
    invoke-super {p0, p1}, Landroid/database/AbstractCursor;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    return-object v1
.end method
