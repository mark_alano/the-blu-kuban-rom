.class Lcom/google/android/apps/reader/content/ReaderActions;
.super Ljava/lang/Object;
.source "ReaderActions.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ReaderActions"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 1420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1421
    return-void
.end method

.method public static addItemTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "db"
    .parameter "account"
    .parameter "itemId"
    .parameter "externalId"
    .parameter "tagId"

    .prologue
    .line 1131
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/reader/content/ReaderActions;->toggleItemTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static addSubscriptionTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "db"
    .parameter "account"
    .parameter "subscriptionId"
    .parameter "tagId"

    .prologue
    .line 369
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->toggleSubscriptionTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static appendToStringArray([Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;
    .registers 7
    .parameter "whereArgs"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TT;>;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, args:Ljava/util/List;,"Ljava/util/List<TT;>;"
    const/4 v4, 0x0

    .line 952
    array-length v2, p0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v2, v3

    new-array v1, v2, [Ljava/lang/String;

    .line 953
    .local v1, newArray:[Ljava/lang/String;
    array-length v2, p0

    invoke-static {p0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 954
    const/4 v0, 0x0

    .local v0, i:I
    :goto_e
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_23

    .line 955
    array-length v2, p0

    add-int/2addr v2, v0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 954
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 957
    :cond_23
    return-object v1
.end method

.method private static clearUnreadCounts(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "streamId"

    .prologue
    const/4 v5, 0x0

    .line 844
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 845
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "unread_count"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 846
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "unread_counts.account_name = ? AND ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 848
    .local v1, where:Ljava/lang/StringBuilder;
    const-string v3, "unread_counts.stream_id = ?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 849
    const-string v3, " OR "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 850
    const-string v3, "unread_counts.stream_id IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    const-string v3, "SELECT subscription_id FROM subscription_categories WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 853
    const-string v3, "account_name = ? AND tag_id = ?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 855
    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 856
    const-string v3, " OR "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 857
    const-string v3, "unread_counts.stream_id IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    const-string v3, "SELECT stream FROM friends WHERE account_name = ?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 859
    const-string v3, ") AND ? LIKE \'user/%/state/com.google/broadcast-friends\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 861
    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    const/4 v3, 0x6

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object p2, v2, v3

    .line 865
    .local v2, whereArgs:[Ljava/lang/String;
    const-string v3, "unread_counts"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v0, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 866
    return-void
.end method

.method private static constructWhereClause(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .registers 8
    .parameter "itemClause"
    .parameter "operator"
    .parameter "itemCount"

    .prologue
    .line 940
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 941
    .local v2, whereClause:Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 942
    .local v1, operatorWithSpaces:Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1f
    if-ge v0, p2, :cond_2c

    .line 943
    if-eqz v0, :cond_26

    .line 944
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 946
    :cond_26
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 942
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 948
    :cond_2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static createNote(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Z)Z
    .registers 25
    .parameter "db"
    .parameter "account"
    .parameter "userId"
    .parameter "url"
    .parameter "title"
    .parameter "srcUrl"
    .parameter "srcTitle"
    .parameter "snippet"
    .parameter "annotation"
    .parameter "share"
    .parameter
    .parameter "linkify"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 1187
    .local p10, tags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p0, :cond_a

    .line 1188
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "SQLiteDatabase is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1190
    :cond_a
    if-nez p1, :cond_14

    .line 1191
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Account is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1193
    :cond_14
    if-nez p2, :cond_1e

    .line 1194
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "User ID is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1196
    :cond_1e
    if-nez p10, :cond_28

    .line 1197
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Tag list is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1199
    :cond_28
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1203
    :try_start_2b
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface/range {p10 .. p10}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {v12, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1204
    .local v12, streamIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "user/-/state/com.google/created"

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1205
    const-string v1, "user/-/state/com.google/self"

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1206
    move-object/from16 v0, p10

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1207
    invoke-static {v12, p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/util/List;Ljava/lang/String;)V

    .line 1208
    invoke-static {p0, p1, v12}, Lcom/google/android/apps/reader/content/ItemList;->invalidateByStreamIds(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;)I

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    .line 1210
    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueItemEdit(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Z)Z

    move-result v1

    if-eqz v1, :cond_6d

    .line 1212
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_68
    .catchall {:try_start_2b .. :try_end_68} :catchall_72

    .line 1213
    const/4 v1, 0x1

    .line 1218
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_6c
    return v1

    .line 1215
    :cond_6d
    const/4 v1, 0x0

    .line 1218
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_6c

    .end local v12           #streamIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_72
    move-exception v1

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public static createTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "tagId"

    .prologue
    .line 1149
    if-nez p0, :cond_a

    .line 1150
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "SQLiteDatabase is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1152
    :cond_a
    if-nez p1, :cond_14

    .line 1153
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Account is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1155
    :cond_14
    if-nez p2, :cond_1e

    .line 1156
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Tag ID is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1159
    :cond_1e
    invoke-static {p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->getLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1160
    .local v0, label:Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1161
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "_id"

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-long v3, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1162
    const-string v2, "account_name"

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    const-string v2, "id"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    const-string v2, "label"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    const-string v2, "sortid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1166
    const-string v2, "tags"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_60

    const/4 v2, 0x1

    :goto_5f
    return v2

    :cond_60
    const/4 v2, 0x0

    goto :goto_5f
.end method

.method private static createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)",
            "Lorg/apache/http/HttpEntity;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :try_start_0
    new-instance v1, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v2, "UTF-8"

    invoke-direct {v1, p0, v2}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_7} :catch_8

    return-object v1

    .line 78
    :catch_8
    move-exception v0

    .line 79
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected UnsupportedEncodingException"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static deleteItem(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "externalId"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 397
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 399
    :try_start_5
    const-string v1, "account_name = ? AND external_id = ?"

    .line 400
    .local v1, whereClause:Ljava/lang/String;
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v0, v4

    const/4 v4, 0x1

    aput-object p2, v0, v4

    .line 403
    .local v0, whereArgs:[Ljava/lang/String;
    const-string v4, "items"

    invoke-virtual {p0, v4, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 404
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueItemDelete(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 405
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_20
    .catchall {:try_start_5 .. :try_end_20} :catchall_29

    .line 411
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_23
    return v2

    :cond_24
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v2, v3

    goto :goto_23

    .end local v0           #whereArgs:[Ljava/lang/String;
    .end local v1           #whereClause:Ljava/lang/String;
    :catchall_29
    move-exception v2

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private static deleteTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 6
    .parameter "db"
    .parameter "account"
    .parameter "tagId"

    .prologue
    .line 1170
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 1173
    .local v0, selectionArgs:[Ljava/lang/String;
    const-string v1, "tags"

    const-string v2, "account_name = ? AND id = ?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1174
    const-string v1, "subscription_categories"

    const-string v2, "account_name = ? AND tag_id = ?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1176
    const-string v1, "item_categories"

    const-string v2, "account_name = ? AND stream_id = ?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1178
    const-string v1, "item_lists"

    const-string v2, "account_name = ? AND stream_id = ?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1180
    const-string v1, "unread_counts"

    const-string v2, "account_name = ? AND stream_id = ?"

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1182
    return-void
.end method

.method public static disableTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 6
    .parameter "db"
    .parameter "account"
    .parameter "tagId"

    .prologue
    .line 378
    invoke-static {p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 379
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User ID is not set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_1f
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 383
    :try_start_22
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/reader/content/ReaderActions;->deleteTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V

    .line 384
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/reader/content/ItemList;->deleteByStreamId(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V

    .line 385
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueDisableTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 386
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_31
    .catchall {:try_start_22 .. :try_end_31} :catchall_3b

    .line 387
    const/4 v0, 0x1

    .line 392
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_35
    return v0

    .line 389
    :cond_36
    const/4 v0, 0x0

    .line 392
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_35

    :catchall_3b
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private static enqueueDisableTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 8
    .parameter "db"
    .parameter "account"
    .parameter "tagId"

    .prologue
    .line 197
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v1, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "s"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->disableTag()Landroid/net/Uri;

    move-result-object v2

    .line 200
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 201
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v3

    return v3
.end method

.method private static enqueueEditSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 12
    .parameter "db"
    .parameter "account"
    .parameter "action"
    .parameter "streamId"
    .parameter "added"
    .parameter "removed"
    .parameter "title"

    .prologue
    .line 176
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v1, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "ac"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "s"

    invoke-direct {v3, v4, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_29

    .line 180
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "a"

    invoke-direct {v3, v4, p4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    :cond_29
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_39

    .line 183
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "r"

    invoke-direct {v3, v4, p5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    :cond_39
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_49

    .line 186
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "t"

    invoke-direct {v3, v4, p6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    :cond_49
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->editSubscription()Landroid/net/Uri;

    move-result-object v2

    .line 189
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 190
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v3

    return v3
.end method

.method private static enqueueEditTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Z)Z
    .registers 12
    .parameter "db"
    .parameter "account"
    .parameter
    .parameter "categoryId"
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 136
    .local p2, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .local v3, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 138
    .local v2, itemId:Ljava/lang/String;
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "i"

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 140
    .end local v2           #itemId:Ljava/lang/String;
    :cond_20
    if-eqz p4, :cond_39

    .line 141
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "a"

    invoke-direct {v5, v6, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    :goto_2c
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->editTag()Landroid/net/Uri;

    move-result-object v4

    .line 146
    .local v4, uri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 147
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v4, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v5

    return v5

    .line 143
    .end local v0           #data:Lorg/apache/http/HttpEntity;
    .end local v4           #uri:Landroid/net/Uri;
    :cond_39
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "r"

    invoke-direct {v5, v6, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2c
.end method

.method private static enqueueItemDelete(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 8
    .parameter "db"
    .parameter "account"
    .parameter "externalId"

    .prologue
    .line 209
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v1, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "i"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->itemDelete()Landroid/net/Uri;

    move-result-object v2

    .line 212
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 213
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v3

    return v3
.end method

.method private static enqueueItemEdit(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Z)Z
    .registers 19
    .parameter "db"
    .parameter "account"
    .parameter "url"
    .parameter "title"
    .parameter "srcUrl"
    .parameter "srcTitle"
    .parameter "snippet"
    .parameter "annotation"
    .parameter "share"
    .parameter
    .parameter "linkify"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 99
    .local p9, tags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v2, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 101
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "title"

    invoke-direct {v5, v6, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_15
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_25

    .line 104
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "url"

    invoke-direct {v5, v6, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_25
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_35

    .line 107
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "srcUrl"

    invoke-direct {v5, v6, p4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_35
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_45

    .line 110
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "srcTitle"

    invoke-direct {v5, v6, p5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_45
    invoke-static {p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_55

    .line 113
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "annotation"

    invoke-direct {v5, v6, p7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_55
    if-eqz p9, :cond_72

    .line 116
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_5b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_72

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 117
    .local v3, tag:Ljava/lang/String;
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "tags"

    invoke-direct {v5, v6, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5b

    .line 120
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #tag:Ljava/lang/String;
    :cond_72
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_82

    .line 121
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "snippet"

    invoke-direct {v5, v6, p6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_82
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "share"

    if-eqz p8, :cond_ab

    const-string v5, "true"

    :goto_8a
    invoke-direct {v6, v7, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "linkify"

    if-eqz p10, :cond_ae

    const-string v5, "true"

    :goto_98
    invoke-direct {v6, v7, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->itemEdit()Landroid/net/Uri;

    move-result-object v4

    .line 127
    .local v4, uri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 128
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v4, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v5

    return v5

    .line 123
    .end local v0           #data:Lorg/apache/http/HttpEntity;
    .end local v4           #uri:Landroid/net/Uri;
    :cond_ab
    const-string v5, "false"

    goto :goto_8a

    .line 124
    :cond_ae
    const-string v5, "false"

    goto :goto_98
.end method

.method private static enqueueMarkAllAsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;J)Z
    .registers 12
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "timestamp"

    .prologue
    .line 155
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v1, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "s"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    const-wide/16 v3, 0x0

    cmp-long v3, p3, v3

    if-lez v3, :cond_32

    .line 161
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "ts"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "999"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_32
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->markAllAsRead()Landroid/net/Uri;

    move-result-object v2

    .line 167
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 168
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v3

    return v3
.end method

.method private static enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "uri"
    .parameter "data"

    .prologue
    .line 85
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 86
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "account_name"

    iget-object v2, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v1, "url"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v1, "data"

    invoke-static {p3}, Lcom/google/android/apps/reader/content/ReaderActions;->httpEntityToString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v1, "created"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 90
    const-string v1, "pending_actions"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3a

    const/4 v1, 0x1

    :goto_39
    return v1

    :cond_3a
    const/4 v1, 0x0

    goto :goto_39
.end method

.method private static enqueueRenameSubscriptionAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "title"

    .prologue
    .line 281
    const-string v2, "edit"

    .line 282
    .local v2, action:Ljava/lang/String;
    const/4 v4, 0x0

    .line 283
    .local v4, added:Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, removed:Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v6, p3

    .line 284
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueEditSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static enqueueRenameTagAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "from"
    .parameter "to"

    .prologue
    .line 293
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v1, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "s"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "dest"

    invoke-direct {v3, v4, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->renameTag()Landroid/net/Uri;

    move-result-object v2

    .line 297
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 298
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v3

    return v3
.end method

.method private static enqueueSubscribeAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "title"

    .prologue
    .line 247
    const-string v2, "subscribe"

    .line 248
    .local v2, action:Ljava/lang/String;
    const/4 v4, 0x0

    .line 249
    .local v4, added:Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, removed:Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v6, p3

    .line 250
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueEditSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static enqueueSubscribeAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;[Ljava/lang/String;Ljava/lang/String;)Z
    .registers 14
    .parameter "db"
    .parameter "account"
    .parameter "streamIds"
    .parameter "tagId"

    .prologue
    .line 260
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 261
    .local v4, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "ac"

    const-string v9, "subscribe"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    move-object v0, p2

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_14
    if-ge v2, v3, :cond_25

    aget-object v5, v0, v2

    .line 263
    .local v5, streamId:Ljava/lang/String;
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "s"

    invoke-direct {v7, v8, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    .line 265
    .end local v5           #streamId:Ljava/lang/String;
    :cond_25
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_35

    .line 266
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "a"

    invoke-direct {v7, v8, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_35
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->editSubscription()Landroid/net/Uri;

    move-result-object v6

    .line 269
    .local v6, uri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 270
    .local v1, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v6, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v7

    return v7
.end method

.method private static enqueueSubscriptionTagEdit(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 12
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "categoryId"
    .parameter "value"

    .prologue
    const/4 v5, 0x0

    .line 316
    const-string v2, "edit"

    .line 317
    .local v2, action:Ljava/lang/String;
    if-eqz p4, :cond_12

    move-object v4, p3

    .line 318
    .local v4, added:Ljava/lang/String;
    :goto_6
    if-nez p4, :cond_9

    move-object v5, p3

    .line 319
    .local v5, removed:Ljava/lang/String;
    :cond_9
    const/4 v6, 0x0

    .local v6, title:Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    .line 320
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueEditSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    .end local v4           #added:Ljava/lang/String;
    .end local v5           #removed:Ljava/lang/String;
    .end local v6           #title:Ljava/lang/String;
    :cond_12
    move-object v4, v5

    .line 317
    goto :goto_6
.end method

.method private static enqueueUnsubscribeAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 10
    .parameter "db"
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 307
    const-string v2, "unsubscribe"

    .line 308
    .local v2, action:Ljava/lang/String;
    const/4 v4, 0x0

    .line 309
    .local v4, added:Ljava/lang/String;
    const/4 v5, 0x0

    .line 310
    .local v5, removed:Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, title:Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    .line 311
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueEditSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)I
    .registers 13
    .parameter "db"
    .parameter "account"
    .parameter "streamId"

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 726
    if-nez p0, :cond_c

    .line 727
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SQLiteDatabase is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 729
    :cond_c
    if-nez p1, :cond_16

    .line 730
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Account is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 732
    :cond_16
    if-nez p2, :cond_20

    .line 733
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 735
    :cond_20
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "unread_count"

    aput-object v0, v2, v1

    .line 738
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "account_name = ? AND stream_id = ? AND unread_count < max_unread_count"

    .line 739
    .local v3, selection:Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v4, v1

    aput-object p2, v4, v9

    .line 742
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 743
    .local v5, groupBy:Ljava/lang/String;
    const/4 v6, 0x0

    .line 744
    .local v6, having:Ljava/lang/String;
    const/4 v7, 0x0

    .line 745
    .local v7, orderBy:Ljava/lang/String;
    const-string v1, "unread_counts"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 748
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_3b
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 749
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_45
    .catchall {:try_start_3b .. :try_end_45} :catchall_4f

    move-result v0

    .line 754
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_49
    return v0

    .line 751
    :cond_4a
    const/4 v0, -0x1

    .line 754
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_49

    :catchall_4f
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static httpEntityToString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    .registers 4
    .parameter "data"

    .prologue
    .line 69
    :try_start_0
    invoke-static {p0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v1

    return-object v1

    .line 70
    :catch_5
    move-exception v0

    .line 71
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected IOException"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static insertSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "title"

    .prologue
    .line 221
    if-nez p3, :cond_3

    .line 224
    move-object p3, p2

    .line 228
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-long v0, v3

    .line 230
    .local v0, longId:J
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 231
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "account_name"

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v3, "_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 233
    const-string v3, "id"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v3, "title"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v3, "sortid"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 236
    const-string v3, "firstitemmsec"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 237
    const-string v3, "html_url"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 238
    const-string v3, "subscriptions"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_49

    const/4 v3, 0x1

    :goto_48
    return v3

    :cond_49
    const/4 v3, 0x0

    goto :goto_48
.end method

.method private static invalidateItemLists(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V
    .registers 8
    .parameter "db"
    .parameter "account"

    .prologue
    .line 1068
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1069
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "timestamp"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1070
    const-string v2, "account_name = ?"

    .line 1071
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    .line 1074
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "item_lists"

    invoke-virtual {p0, v3, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1075
    return-void
.end method

.method private static invalidateOverview(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V
    .registers 8
    .parameter "db"
    .parameter "account"

    .prologue
    .line 1081
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1082
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "timestamp"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1083
    const-string v2, "account_name = ? AND url = ?"

    .line 1084
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->overview()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 1087
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "timestamps"

    invoke-virtual {p0, v3, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1088
    return-void
.end method

.method private static invalidateUnreadCounts(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V
    .registers 8
    .parameter "db"
    .parameter "account"

    .prologue
    .line 1055
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1056
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "timestamp"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1057
    const-string v2, "account_name = ? AND url = ?"

    .line 1058
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->unreadCount()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 1061
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "timestamps"

    invoke-virtual {p0, v3, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1062
    return-void
.end method

.method private static invalidateUnreadState(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V
    .registers 2
    .parameter "db"
    .parameter "account"

    .prologue
    .line 1095
    invoke-static {p0, p1}, Lcom/google/android/apps/reader/content/ReaderActions;->invalidateUnreadCounts(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V

    .line 1096
    invoke-static {p0, p1}, Lcom/google/android/apps/reader/content/ReaderActions;->invalidateItemLists(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V

    .line 1097
    invoke-static {p0, p1}, Lcom/google/android/apps/reader/content/ReaderActions;->invalidateOverview(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V

    .line 1098
    return-void
.end method

.method public static markAllAsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ItemList;)Z
    .registers 4
    .parameter "db"
    .parameter "items"

    .prologue
    .line 1373
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->markAllAsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ItemList;J)Z

    move-result v0

    return v0
.end method

.method public static markAllAsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ItemList;J)Z
    .registers 13
    .parameter "db"
    .parameter "items"
    .parameter "offset"

    .prologue
    .line 1377
    invoke-virtual {p1}, Lcom/google/android/apps/reader/content/ItemList;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 1378
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-virtual {p1}, Lcom/google/android/apps/reader/content/ItemList;->getStreamId()Ljava/lang/String;

    move-result-object v3

    .line 1379
    .local v3, streamId:Ljava/lang/String;
    if-nez v3, :cond_13

    .line 1380
    const-string v7, "ReaderActions"

    const-string v8, "Could not mark-all-as-read because stream ID is missing"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    const/4 v7, 0x0

    .line 1416
    :goto_12
    return v7

    .line 1383
    :cond_13
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1387
    const-wide/16 v7, -0x1

    :try_start_18
    invoke-virtual {p1, p0, v7, v8}, Lcom/google/android/apps/reader/content/ItemList;->getCrawlTimestamp(Landroid/database/sqlite/SQLiteDatabase;J)J

    move-result-wide v4

    .line 1388
    .local v4, timestamp:J
    const-wide/16 v7, -0x1

    cmp-long v7, v4, v7

    if-eqz v7, :cond_5d

    const/4 v1, 0x1

    .line 1389
    .local v1, exact:Z
    :goto_23
    const-wide/16 v7, -0x1

    cmp-long v7, v4, v7

    if-nez v7, :cond_2d

    .line 1390
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1392
    :cond_2d
    add-long/2addr v4, p2

    .line 1393
    invoke-static {p0, v0, v3, v4, v5}, Lcom/google/android/apps/reader/content/ReaderActions;->markItemsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;J)V

    .line 1395
    const-wide/16 v7, 0x0

    cmp-long v7, p2, v7

    if-nez v7, :cond_44

    .line 1397
    invoke-static {p0, v0, v3}, Lcom/google/android/apps/reader/content/ReaderActions;->getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)I

    move-result v6

    .line 1399
    .local v6, unreadCount:I
    invoke-static {p0, v0, v3}, Lcom/google/android/apps/reader/content/ReaderActions;->clearUnreadCounts(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)V

    .line 1400
    const/4 v7, -0x1

    if-eq v6, v7, :cond_44

    .line 1401
    invoke-static {p0, v0, v3, v6}, Lcom/google/android/apps/reader/content/ReaderActions;->subtractUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;I)V

    .line 1410
    .end local v6           #unreadCount:I
    :cond_44
    if-nez v1, :cond_4c

    const-wide/16 v7, 0x0

    cmp-long v7, p2, v7

    if-eqz v7, :cond_5f

    :cond_4c
    const/4 v2, 0x1

    .line 1411
    .local v2, sendTimestamp:Z
    :goto_4d
    if-eqz v2, :cond_61

    .end local v4           #timestamp:J
    :goto_4f
    invoke-static {p0, v0, v3, v4, v5}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueMarkAllAsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;J)Z

    .line 1412
    invoke-static {p0, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->invalidateUnreadState(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V

    .line 1413
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_58
    .catchall {:try_start_18 .. :try_end_58} :catchall_64

    .line 1414
    const/4 v7, 0x1

    .line 1416
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_12

    .line 1388
    .end local v1           #exact:Z
    .end local v2           #sendTimestamp:Z
    .restart local v4       #timestamp:J
    :cond_5d
    const/4 v1, 0x0

    goto :goto_23

    .line 1410
    .restart local v1       #exact:Z
    :cond_5f
    const/4 v2, 0x0

    goto :goto_4d

    .line 1411
    .restart local v2       #sendTimestamp:Z
    :cond_61
    const-wide/16 v4, -0x1

    goto :goto_4f

    .line 1416
    .end local v1           #exact:Z
    .end local v2           #sendTimestamp:Z
    .end local v4           #timestamp:J
    :catchall_64
    move-exception v7

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v7
.end method

.method private static markItemsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;J)V
    .registers 11
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "maxCrawlTimestamp"

    .prologue
    .line 1359
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1360
    .local v1, values:Landroid/content/ContentValues;
    const-string v4, "read"

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1361
    const-string v4, "locked"

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1362
    const-string v0, "SELECT item_id FROM item_categories WHERE account_name = ? AND stream_id = ?"

    .line 1363
    .local v0, sq:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "crawl_time <= ? AND id IN ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1364
    .local v3, whereClause:Ljava/lang/String;
    const/4 v4, 0x3

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v2, v4

    const/4 v4, 0x2

    aput-object p2, v2, v4

    .line 1367
    .local v2, whereArgs:[Ljava/lang/String;
    const-string v4, "items"

    invoke-virtual {p0, v4, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1370
    return-void
.end method

.method public static removeItemTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "db"
    .parameter "account"
    .parameter "itemId"
    .parameter "externalId"
    .parameter "tagId"

    .prologue
    .line 1136
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/reader/content/ReaderActions;->toggleItemTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static removeSubscriptionTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "db"
    .parameter "account"
    .parameter "subscriptionId"
    .parameter "tagId"

    .prologue
    .line 374
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->toggleSubscriptionTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static renameSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "db"
    .parameter "account"
    .parameter "subscriptionId"
    .parameter "title"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 488
    if-nez p0, :cond_c

    .line 489
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "SQLiteDatabase is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 491
    :cond_c
    if-nez p1, :cond_16

    .line 492
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Account is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 494
    :cond_16
    if-nez p2, :cond_20

    .line 495
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Subscription ID is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 497
    :cond_20
    if-nez p3, :cond_2a

    .line 498
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Title is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 500
    :cond_2a
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 502
    :try_start_2d
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 503
    .local v0, values:Landroid/content/ContentValues;
    const-string v5, "title"

    invoke-virtual {v0, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    const-string v2, "account_name = ? AND id = ?"

    .line 505
    .local v2, whereClause:Ljava/lang/String;
    const/4 v5, 0x2

    new-array v1, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v1, v5

    const/4 v5, 0x1

    aput-object p2, v1, v5

    .line 508
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v5, "subscriptions"

    invoke-virtual {p0, v5, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 509
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueRenameSubscriptionAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_56

    .line 510
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_52
    .catchall {:try_start_2d .. :try_end_52} :catchall_5b

    .line 516
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_55
    return v3

    :cond_56
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v3, v4

    goto :goto_55

    .end local v0           #values:Landroid/content/ContentValues;
    .end local v1           #whereArgs:[Ljava/lang/String;
    .end local v2           #whereClause:Ljava/lang/String;
    :catchall_5b
    move-exception v3

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method public static renameTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8
    .parameter "db"
    .parameter "account"
    .parameter "from"
    .parameter "to"
    .parameter "label"

    .prologue
    .line 534
    if-nez p0, :cond_a

    .line 535
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SQLiteDatabase is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 537
    :cond_a
    if-nez p1, :cond_14

    .line 538
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Account is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 540
    :cond_14
    if-nez p2, :cond_1e

    .line 541
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Tag ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_1e
    if-nez p3, :cond_28

    .line 544
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Tag ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 546
    :cond_28
    if-nez p4, :cond_32

    .line 547
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Label is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 549
    :cond_32
    invoke-static {p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 550
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User ID is not set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 552
    :cond_51
    invoke-static {p3}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 553
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User ID is not set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 555
    :cond_70
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 557
    :try_start_73
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/reader/content/ReaderActions;->renameTagInTagsTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->renameTagInUnreadCountsTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->renameTagInSubscriptionCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->renameTagInItemCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueRenameTagAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 569
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_88
    .catchall {:try_start_73 .. :try_end_88} :catchall_92

    .line 570
    const/4 v0, 0x1

    .line 575
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_8c
    return v0

    .line 572
    :cond_8d
    const/4 v0, 0x0

    .line 575
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_8c

    :catchall_92
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private static renameTagInItemCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "from"
    .parameter "to"

    .prologue
    .line 629
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 630
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "stream_id"

    invoke-virtual {v0, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    const-string v2, "account_name = ? AND stream_id = ?"

    .line 632
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    aput-object p2, v1, v3

    .line 633
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "item_categories"

    invoke-virtual {p0, v3, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 636
    return-void
.end method

.method private static renameTagInSubscriptionCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "from"
    .parameter "to"

    .prologue
    .line 618
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 619
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "tag_id"

    invoke-virtual {v0, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    const-string v2, "account_name = ? AND tag_id = ?"

    .line 621
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    aput-object p2, v1, v3

    .line 622
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "subscription_categories"

    invoke-virtual {p0, v3, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 625
    return-void
.end method

.method private static renameTagInTagsTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "db"
    .parameter "account"
    .parameter "from"
    .parameter "to"
    .parameter "label"

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 581
    const-string v1, "tags"

    .line 582
    .local v1, table:Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 583
    .local v3, values:Landroid/content/ContentValues;
    const-string v5, "id"

    invoke-virtual {v3, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    const-string v5, "label"

    invoke-virtual {v3, v5, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const-string v4, "account_name = ? AND id = ?"

    .line 591
    .local v4, whereClause:Ljava/lang/String;
    new-array v0, v8, [Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v0, v6

    aput-object p3, v0, v7

    .line 592
    .local v0, deleteArgs:[Ljava/lang/String;
    invoke-virtual {p0, v1, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 594
    new-array v2, v8, [Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v2, v6

    aput-object p2, v2, v7

    .line 595
    .local v2, updateArgs:[Ljava/lang/String;
    invoke-virtual {p0, v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 596
    return-void
.end method

.method private static renameTagInUnreadCountsTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "db"
    .parameter "account"
    .parameter "from"
    .parameter "to"

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 600
    const-string v1, "unread_counts"

    .line 601
    .local v1, table:Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 602
    .local v3, values:Landroid/content/ContentValues;
    const-string v5, "stream_id"

    invoke-virtual {v3, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const-string v4, "account_name = ? AND stream_id = ?"

    .line 609
    .local v4, whereClause:Ljava/lang/String;
    new-array v0, v8, [Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v0, v6

    aput-object p3, v0, v7

    .line 610
    .local v0, deleteArgs:[Ljava/lang/String;
    invoke-virtual {p0, v1, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 612
    new-array v2, v8, [Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v2, v6

    aput-object p2, v2, v7

    .line 613
    .local v2, updateArgs:[Ljava/lang/String;
    invoke-virtual {p0, v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 614
    return-void
.end method

.method public static setPreference(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "key"
    .parameter "value"

    .prologue
    .line 669
    if-nez p0, :cond_a

    .line 670
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "SQLiteDatabase is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 672
    :cond_a
    if-nez p1, :cond_14

    .line 673
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Account is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 675
    :cond_14
    if-nez p2, :cond_1e

    .line 676
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Key is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 678
    :cond_1e
    if-nez p3, :cond_28

    .line 679
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Value is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 684
    :cond_28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 685
    .local v1, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "k"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 686
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "v"

    invoke-direct {v3, v4, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 687
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->setPreference()Landroid/net/Uri;

    move-result-object v2

    .line 688
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 689
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v3

    return v3
.end method

.method public static setStreamPreference(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "key"
    .parameter "value"

    .prologue
    .line 694
    if-nez p0, :cond_a

    .line 695
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "SQLiteDatabase is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 697
    :cond_a
    if-nez p1, :cond_14

    .line 698
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Account is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 700
    :cond_14
    if-nez p2, :cond_1e

    .line 701
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Stream ID is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 703
    :cond_1e
    if-nez p3, :cond_28

    .line 704
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Key is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 706
    :cond_28
    if-nez p4, :cond_32

    .line 707
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Value is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 712
    :cond_32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 713
    .local v1, parameters:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "s"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 714
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "k"

    invoke-direct {v3, v4, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 715
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "v"

    invoke-direct {v3, v4, p4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 716
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->setStreamPreference()Landroid/net/Uri;

    move-result-object v2

    .line 717
    .local v2, uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/reader/content/ReaderActions;->createUrlEncodedFormEntity(Ljava/util/List;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 718
    .local v0, data:Lorg/apache/http/HttpEntity;
    invoke-static {p0, p1, v2, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueuePendingAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/net/Uri;Lorg/apache/http/HttpEntity;)Z

    move-result v3

    return v3
.end method

.method private static setUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;I)V
    .registers 10
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "value"

    .prologue
    const/4 v5, 0x0

    .line 825
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 826
    .local v0, values:Landroid/content/ContentValues;
    const-string v3, "unread_count"

    invoke-static {v5, p3}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 827
    const-string v2, "account_name = ? AND stream_id = ?"

    .line 828
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v5

    const/4 v3, 0x1

    aput-object p2, v1, v3

    .line 831
    .local v1, whereArgs:[Ljava/lang/String;
    const-string v3, "unread_counts"

    invoke-virtual {p0, v3, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 832
    return-void
.end method

.method public static subscribe(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "title"

    .prologue
    .line 417
    if-nez p0, :cond_a

    .line 418
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SQLiteDatabase is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420
    :cond_a
    if-nez p1, :cond_14

    .line 421
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Account is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :cond_14
    if-nez p2, :cond_1e

    .line 424
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 426
    :cond_1e
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 428
    :try_start_21
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->insertSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueSubscribeAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 430
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_30
    .catchall {:try_start_21 .. :try_end_30} :catchall_3a

    .line 431
    const/4 v0, 0x1

    .line 436
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_34
    return v0

    .line 433
    :cond_35
    const/4 v0, 0x0

    .line 436
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_34

    :catchall_3a
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static subscribe(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;[Landroid/content/ContentValues;Ljava/lang/String;)Z
    .registers 12
    .parameter "db"
    .parameter "account"
    .parameter "values"
    .parameter "tagId"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 442
    if-nez p0, :cond_c

    .line 443
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "SQLiteDatabase is null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 445
    :cond_c
    if-nez p1, :cond_16

    .line 446
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "Account is null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 448
    :cond_16
    if-nez p2, :cond_20

    .line 449
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "ContentValues array is null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 451
    :cond_20
    if-eqz p3, :cond_30

    invoke-static {p3}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_30

    .line 452
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "User ID missing"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 454
    :cond_30
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 456
    :try_start_33
    array-length v6, p2

    new-array v2, v6, [Ljava/lang/String;

    .line 457
    .local v2, streamIds:[Ljava/lang/String;
    if-eqz p3, :cond_3b

    .line 458
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->createTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    .line 460
    :cond_3b
    const/4 v0, 0x0

    .local v0, i:I
    :goto_3c
    array-length v6, p2

    if-ge v0, v6, :cond_71

    .line 461
    aget-object v6, p2, v0

    const-string v7, "id"

    invoke-virtual {v6, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 462
    .local v1, streamId:Ljava/lang/String;
    aget-object v6, p2, v0

    const-string v7, "title"

    invoke-virtual {v6, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 463
    .local v3, title:Ljava/lang/String;
    if-nez v1, :cond_5e

    .line 464
    const-string v5, "ReaderActions"

    const-string v6, "Stream ID was not specified"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5a
    .catchall {:try_start_33 .. :try_end_5a} :catchall_83

    .line 482
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .end local v1           #streamId:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    :goto_5d
    return v4

    .line 467
    .restart local v1       #streamId:Ljava/lang/String;
    .restart local v3       #title:Ljava/lang/String;
    :cond_5e
    :try_start_5e
    invoke-static {p0, p1, v1, v3}, Lcom/google/android/apps/reader/content/ReaderActions;->insertSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6d

    .line 468
    if-eqz p3, :cond_6a

    .line 469
    const/4 v6, 0x1

    invoke-static {p0, p1, v1, p3, v6}, Lcom/google/android/apps/reader/content/ReaderActions;->updateSubscriptionCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_6a
    .catchall {:try_start_5e .. :try_end_6a} :catchall_83

    .line 460
    :cond_6a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3c

    .line 482
    :cond_6d
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_5d

    .line 475
    .end local v1           #streamId:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    :cond_71
    :try_start_71
    invoke-static {p0, p1, v2, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueSubscribeAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7f

    .line 476
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7a
    .catchall {:try_start_71 .. :try_end_7a} :catchall_83

    .line 482
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v4, v5

    goto :goto_5d

    :cond_7f
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_5d

    .end local v0           #i:I
    .end local v2           #streamIds:[Ljava/lang/String;
    :catchall_83
    move-exception v4

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method private static subtractUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;I)V
    .registers 16
    .parameter "db"
    .parameter "account"
    .parameter "streamId"
    .parameter "amount"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 878
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 879
    .local v0, builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "unread_counts"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 880
    const-string v1, "account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 881
    iget-object v1, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 882
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 883
    const-string v1, "("

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 886
    const-string v1, "stream_id LIKE \'user/%/state/com.google/reading-list\'"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 888
    const-string v1, " OR "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 891
    const-string v1, "stream_id IN ("

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 893
    const-string v1, "SELECT tag_id FROM subscription_categories WHERE "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 894
    const-string v1, "account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 895
    iget-object v1, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 896
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 897
    const-string v1, "subscription_id = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 898
    invoke-virtual {v0, p2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 900
    const-string v1, ")"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 902
    const-string v1, " OR "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 905
    const-string v1, "stream_id LIKE \'user/%/state/com.google/broadcast-friends\'"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 906
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 907
    invoke-virtual {v0, p2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 908
    const-string v1, " IN ("

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 912
    const-string v1, "SELECT stream FROM friends WHERE "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 913
    const-string v1, "account_name = "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 914
    iget-object v1, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 916
    const-string v1, ")"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 918
    const-string v1, ")"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 919
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 920
    const-string v1, "unread_count < max_unread_count"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 922
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "unread_count"

    aput-object v1, v2, v4

    const-string v1, "stream_id"

    aput-object v1, v2, v5

    .local v2, projection:[Ljava/lang/String;
    move-object v1, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    .line 925
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 927
    .local v8, cursor:Landroid/database/Cursor;
    const/4 v9, 0x0

    .local v9, position:I
    :goto_a0
    :try_start_a0
    invoke-interface {v8, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_b7

    .line 928
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 929
    .local v11, unreadCount:I
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 930
    .local v10, stream:Ljava/lang/String;
    sub-int/2addr v11, p3

    .line 931
    invoke-static {p0, p1, v10, v11}, Lcom/google/android/apps/reader/content/ReaderActions;->setUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;I)V
    :try_end_b4
    .catchall {:try_start_a0 .. :try_end_b4} :catchall_bb

    .line 927
    add-int/lit8 v9, v9, 0x1

    goto :goto_a0

    .line 934
    .end local v10           #stream:Ljava/lang/String;
    .end local v11           #unreadCount:I
    :cond_b7
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 936
    return-void

    .line 934
    :catchall_bb
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static toggleItemTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "itemId"
    .parameter "externalId"
    .parameter "tagId"
    .parameter "value"

    .prologue
    .line 1102
    invoke-static {p4}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User ID is not set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1105
    :cond_1f
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1107
    if-eqz p5, :cond_27

    .line 1108
    :try_start_24
    invoke-static {p0, p1, p4}, Lcom/google/android/apps/reader/content/ReaderActions;->createTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    .line 1114
    :cond_27
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, p1, v0, p4, p5}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItemCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Z)V

    .line 1115
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, p1, v0, p4, p5}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueEditTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Z)Z

    .line 1116
    sget-object v0, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/util/Experiment;->isDisabled()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 1120
    invoke-static {p0, p1, p4}, Lcom/google/android/apps/reader/content/ItemList;->invalidateByStreamId(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)I

    .line 1122
    :cond_40
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_43
    .catchall {:try_start_24 .. :try_end_43} :catchall_48

    .line 1123
    const/4 v0, 0x1

    .line 1125
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return v0

    :catchall_48
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private static toggleSubscriptionTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 8
    .parameter "db"
    .parameter "account"
    .parameter "subscriptionId"
    .parameter "tagId"
    .parameter "value"

    .prologue
    .line 345
    invoke-static {p3}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 346
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User ID is not set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_1f
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 350
    if-eqz p4, :cond_27

    .line 351
    :try_start_24
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/reader/content/ReaderActions;->createTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    .line 357
    :cond_27
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/reader/content/ReaderActions;->updateSubscriptionCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 358
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueSubscriptionTagEdit(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 359
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/reader/content/ItemList;->invalidateByStreamId(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)I

    .line 360
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_33
    .catchall {:try_start_24 .. :try_end_33} :catchall_38

    .line 361
    const/4 v0, 0x1

    .line 363
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return v0

    :catchall_38
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static unsubscribe(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z
    .registers 9
    .parameter "db"
    .parameter "account"
    .parameter "subscriptionId"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 639
    if-nez p0, :cond_c

    .line 640
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "SQLiteDatabase is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 642
    :cond_c
    if-nez p1, :cond_16

    .line 643
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Account is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 645
    :cond_16
    if-nez p2, :cond_20

    .line 646
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Subscription ID is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 648
    :cond_20
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 650
    :try_start_23
    const-string v1, "account_name = ? AND id = ?"

    .line 651
    .local v1, whereClause:Ljava/lang/String;
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v0, v4

    const/4 v4, 0x1

    aput-object p2, v0, v4

    .line 655
    .local v0, whereArgs:[Ljava/lang/String;
    const-string v4, "subscriptions"

    invoke-virtual {p0, v4, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 656
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueUnsubscribeAction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_42

    .line 657
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3e
    .catchall {:try_start_23 .. :try_end_3e} :catchall_47

    .line 663
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_41
    return v2

    :cond_42
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v2, v3

    goto :goto_41

    .end local v0           #whereArgs:[Ljava/lang/String;
    .end local v1           #whereClause:Ljava/lang/String;
    :catchall_47
    move-exception v2

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private static updateItemCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Z)V
    .registers 14
    .parameter "db"
    .parameter "account"
    .parameter
    .parameter "categoryId"
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 987
    .local p2, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1010
    :cond_6
    :goto_6
    return-void

    .line 991
    :cond_7
    if-eqz p4, :cond_36

    .line 992
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 993
    .local v1, itemId:Ljava/lang/Long;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 994
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "account_name"

    iget-object v7, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    const-string v6, "item_id"

    invoke-virtual {v5, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 996
    const-string v6, "stream_id"

    invoke-virtual {v5, v6, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const-string v6, "item_categories"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_d

    .line 1000
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #itemId:Ljava/lang/Long;
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_36
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1001
    .local v3, selection:Ljava/lang/StringBuilder;
    const-string v6, "account_name = ? AND stream_id = ? AND ("

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1002
    const-string v6, "item_id = ?"

    const-string v7, "OR"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/reader/content/ReaderActions;->constructWhereClause(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1003
    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1004
    const/4 v6, 0x2

    new-array v2, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v7, v2, v6

    const/4 v6, 0x1

    aput-object p3, v2, v6

    .line 1007
    .local v2, nonVariableSelectionArgs:[Ljava/lang/String;
    invoke-static {v2, p2}, Lcom/google/android/apps/reader/content/ReaderActions;->appendToStringArray([Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v4

    .line 1008
    .local v4, selectionArgs:[Ljava/lang/String;
    const-string v6, "item_categories"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_6
.end method

.method public static updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/reader/content/ItemList;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Z
    .registers 20
    .parameter "db"
    .parameter "account"
    .parameter "userId"
    .parameter "itemList"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "values"

    .prologue
    .line 1239
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 1240
    .local v12, tables:Ljava/lang/StringBuilder;
    const-string v3, "item_positions"

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1241
    const-string v3, ", "

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "items"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1243
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1244
    .local v1, queryBuilder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v3, "item_positions"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1245
    const-string v3, ".account_name = "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1246
    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1247
    const-string v3, " AND "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1248
    const-string v3, "items"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1249
    const-string v3, ".account_name = "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1250
    iget-object v3, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1251
    const-string v3, " AND "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1252
    const-string v3, "item_positions"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1253
    const-string v3, ".item_list_id = "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1254
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/reader/content/ItemList;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1255
    const-string v3, " AND "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1256
    const-string v3, "items"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1257
    const-string v3, "."

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1258
    const-string v3, "id"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1259
    const-string v3, " = "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1260
    const-string v3, "item_positions"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1261
    const-string v3, ".item_id "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1262
    const-string v3, " AND "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1263
    const-string v3, "items"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1264
    const-string v3, "."

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1265
    const-string v3, "read"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1266
    const-string v3, " = 0"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1269
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 1270
    .local v10, projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "id"

    const-string v4, "items.id"

    invoke-interface {v10, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1271
    const-string v3, "external_id"

    const-string v4, "items.external_id"

    invoke-interface {v10, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1272
    invoke-virtual {v1, v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1274
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "external_id"

    aput-object v4, v2, v3

    .line 1279
    .local v2, projection:[Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1281
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1283
    .local v11, query:Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-virtual {p0, v11, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1285
    .local v9, cursor:Landroid/database/Cursor;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1286
    .local v7, externalIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1288
    .local v6, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    if-nez v9, :cond_d9

    .line 1289
    const/4 v3, 0x0

    .line 1305
    :goto_d8
    return v3

    .line 1293
    :cond_d9
    :try_start_d9
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_106

    .line 1295
    :cond_df
    const/4 v3, 0x0

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1296
    const/4 v3, 0x1

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1297
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_f6
    .catchall {:try_start_d9 .. :try_end_f6} :catchall_10b

    move-result v3

    if-nez v3, :cond_df

    .line 1302
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v8, p6

    .line 1305
    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;)Z

    move-result v3

    goto :goto_d8

    .line 1299
    :cond_106
    const/4 v3, 0x0

    .line 1302
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_d8

    :catchall_10b
    move-exception v3

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v3
.end method

.method public static updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;)Z
    .registers 18
    .parameter "db"
    .parameter "account"
    .parameter "userId"
    .parameter
    .parameter
    .parameter "values"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/ContentValues;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1324
    .local p3, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    .local p4, externalIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1326
    const/4 v11, 0x0

    .line 1327
    .local v11, updated:Z
    :try_start_4
    const-string v7, "read"

    const-string v8, "user/-/state/com.google/read"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v11, v1

    .line 1329
    const-string v7, "starred"

    const-string v8, "user/-/state/com.google/starred"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v11, v1

    .line 1331
    const-string v7, "liked"

    const-string v8, "user/-/state/com.google/like"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v11, v1

    .line 1333
    const-string v7, "shared"

    const-string v8, "user/-/state/com.google/broadcast"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v11, v1

    .line 1335
    const-string v1, "read"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v9

    .line 1336
    .local v9, read:Ljava/lang/Boolean;
    if-eqz v9, :cond_5c

    .line 1339
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {p0, p1, p3, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->updateUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Z)V

    .line 1343
    invoke-static {p0, p1}, Lcom/google/android/apps/reader/content/ReaderActions;->invalidateOverview(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;)V

    .line 1345
    :cond_5c
    const-string v1, "starred"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v10

    .line 1346
    .local v10, starred:Ljava/lang/Boolean;
    if-eqz v10, :cond_6f

    .line 1347
    const-string v1, "user/-/state/com.google/starred"

    invoke-static {v1, p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/reader/content/ItemList;->invalidateByStreamId(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)I

    .line 1350
    :cond_6f
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_72
    .catchall {:try_start_4 .. :try_end_72} :catchall_76

    .line 1353
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return v11

    .end local v9           #read:Ljava/lang/Boolean;
    .end local v10           #starred:Ljava/lang/Boolean;
    :catchall_76
    move-exception v1

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method private static updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 10
    .parameter "db"
    .parameter "account"
    .parameter "userId"
    .parameter
    .parameter
    .parameter "values"
    .parameter "columnName"
    .parameter "categoryId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1034
    .local p3, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    .local p4, externalIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p5, p6}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    .line 1035
    .local v0, value:Ljava/lang/Boolean;
    if-nez v0, :cond_8

    .line 1037
    const/4 v1, 0x0

    .line 1048
    :goto_7
    return v1

    .line 1039
    :cond_8
    invoke-static {p7, p2}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p7

    .line 1042
    invoke-static {p0, p1, p3, p6, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItemsTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1043
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {p0, p1, p3, p7, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItemCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Z)V

    .line 1046
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {p0, p1, p4, p7, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->enqueueEditTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Z)Z

    .line 1048
    const/4 v1, 0x1

    goto :goto_7
.end method

.method private static updateItemsTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 12
    .parameter "db"
    .parameter "account"
    .parameter
    .parameter "columnName"
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 965
    .local p2, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 980
    :goto_6
    return-void

    .line 969
    :cond_7
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 970
    .local v1, values:Landroid/content/ContentValues;
    invoke-virtual {v1, p3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 971
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 972
    .local v3, whereClause:Ljava/lang/StringBuilder;
    const-string v4, "account_name = ? AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 973
    const-string v4, "id = ?"

    const-string v5, "OR"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/reader/content/ReaderActions;->constructWhereClause(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 974
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 975
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v0, v4

    .line 978
    .local v0, nonVariableWhereArgs:[Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/google/android/apps/reader/content/ReaderActions;->appendToStringArray([Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v2

    .line 979
    .local v2, whereArgs:[Ljava/lang/String;
    const-string v4, "items"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v1, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_6
.end method

.method private static updateSubscriptionCategoriesTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 10
    .parameter "db"
    .parameter "account"
    .parameter "subscriptionId"
    .parameter "categoryId"
    .parameter "value"

    .prologue
    .line 328
    if-eqz p4, :cond_1f

    .line 329
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 330
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "account_name"

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v3, "subscription_id"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v3, "tag_id"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v3, "subscription_categories"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 341
    .end local v2           #values:Landroid/content/ContentValues;
    :goto_1e
    return-void

    .line 335
    :cond_1f
    const-string v0, "account_name = ? AND subscription_id = ? AND tag_id = ?"

    .line 336
    .local v0, selection:Ljava/lang/String;
    const/4 v3, 0x3

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    aput-object p2, v1, v3

    const/4 v3, 0x2

    aput-object p3, v1, v3

    .line 339
    .local v1, selectionArgs:[Ljava/lang/String;
    const-string v3, "subscription_categories"

    invoke-virtual {p0, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1e
.end method

.method private static updateUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/util/List;Z)V
    .registers 26
    .parameter "db"
    .parameter "account"
    .parameter
    .parameter "read"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 767
    .local p2, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 818
    :goto_6
    return-void

    .line 771
    :cond_7
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stream_id"

    aput-object v5, v6, v4

    .line 774
    .local v6, columns:[Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 775
    .local v18, selection:Ljava/lang/StringBuilder;
    const-string v4, "account_name = ? AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 776
    const-string v4, "item_id = ?"

    const-string v5, "OR"

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v4, v5, v7}, Lcom/google/android/apps/reader/content/ReaderActions;->constructWhereClause(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    const-string v4, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 778
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v16, v4

    .line 781
    .local v16, nonVariableSelectionArgs:[Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->appendToStringArray([Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v8

    .line 782
    .local v8, selectionArgs:[Ljava/lang/String;
    const/4 v9, 0x0

    .line 783
    .local v9, groupBy:Ljava/lang/String;
    const/4 v10, 0x0

    .line 784
    .local v10, having:Ljava/lang/String;
    const/4 v11, 0x0

    .line 785
    .local v11, orderBy:Ljava/lang/String;
    const-string v5, "item_categories"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 787
    .local v14, cursor:Landroid/database/Cursor;
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 790
    .local v21, unreadCountChangeMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/16 v17, 0x0

    .local v17, position:I
    :goto_5d
    :try_start_5d
    move/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_90

    .line 791
    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 792
    .local v19, streamId:Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    .line 793
    .local v12, change:Ljava/lang/Integer;
    if-nez v12, :cond_85

    .line 794
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 798
    :goto_7b
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 790
    add-int/lit8 v17, v17, 0x1

    goto :goto_5d

    .line 796
    :cond_85
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto :goto_7b

    .line 801
    .end local v12           #change:Ljava/lang/Integer;
    .end local v19           #streamId:Ljava/lang/String;
    :cond_90
    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, i$:Ljava/util/Iterator;
    :cond_98
    :goto_98
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    .line 802
    .local v13, changeEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 803
    .restart local v19       #streamId:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/reader/content/ReaderActions;->getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)I

    move-result v20

    .line 804
    .local v20, unreadCount:I
    const/4 v4, -0x1

    move/from16 v0, v20

    if-eq v0, v4, :cond_98

    .line 805
    if-eqz p3, :cond_d8

    .line 806
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int v20, v20, v4

    .line 810
    :goto_c7
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/reader/content/ReaderActions;->setUnreadCount(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;I)V
    :try_end_d2
    .catchall {:try_start_5d .. :try_end_d2} :catchall_d3

    goto :goto_98

    .line 816
    .end local v13           #changeEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v15           #i$:Ljava/util/Iterator;
    .end local v19           #streamId:Ljava/lang/String;
    .end local v20           #unreadCount:I
    :catchall_d3
    move-exception v4

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v4

    .line 808
    .restart local v13       #changeEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v15       #i$:Ljava/util/Iterator;
    .restart local v19       #streamId:Ljava/lang/String;
    .restart local v20       #unreadCount:I
    :cond_d8
    :try_start_d8
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_e1
    .catchall {:try_start_d8 .. :try_end_e1} :catchall_d3

    move-result v4

    add-int v20, v20, v4

    goto :goto_c7

    .line 816
    .end local v13           #changeEntry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v19           #streamId:Ljava/lang/String;
    .end local v20           #unreadCount:I
    :cond_e5
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_6
.end method
