.class public Lcom/google/android/apps/reader/content/ReaderCleanup;
.super Ljava/lang/Object;
.source "ReaderCleanup.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ReaderCleanup"


# instance fields
.field protected final mAccount:Lcom/google/android/accounts/Account;

.field protected final mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field protected final mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

.field protected final mMaxAge:J


# direct methods
.method protected constructor <init>(Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ReaderFileCache;I)V
    .registers 7
    .parameter "account"
    .parameter "database"
    .parameter "fileCache"
    .parameter "priority"

    .prologue
    const/4 v1, 0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    if-nez p2, :cond_e

    .line 56
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Database is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_e
    if-nez p3, :cond_18

    .line 59
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "File cache is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_18
    if-nez p1, :cond_22

    .line 62
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Account is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_22
    if-ge p4, v1, :cond_2a

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 67
    :cond_2a
    const/16 v0, 0xa

    if-le p4, v0, :cond_34

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 70
    :cond_34
    iput-object p1, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    .line 71
    iput-object p2, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 72
    iput-object p3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    .line 73
    if-ne p4, v1, :cond_42

    .line 76
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mMaxAge:J

    .line 86
    :goto_41
    return-void

    .line 77
    :cond_42
    const/4 v0, 0x5

    if-gt p4, v0, :cond_4b

    .line 80
    const-wide/32 v0, 0xf731400

    iput-wide v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mMaxAge:J

    goto :goto_41

    .line 84
    :cond_4b
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mMaxAge:J

    goto :goto_41
.end method

.method private deleteItemCategories()V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 199
    const-string v2, "account_name = ? AND item_id NOT IN (SELECT id FROM items WHERE account_name = ?)"

    .line 200
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v6

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v5

    .line 203
    .local v1, whereArgs:[Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "item_categories"

    invoke-virtual {v3, v4, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 204
    .local v0, count:I
    const-string v3, "Deleted %d item categories"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 205
    return-void
.end method

.method private deleteItemLinks()V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 212
    const-string v2, "account_name = ? AND item_id NOT IN (SELECT id FROM items WHERE account_name = ?)"

    .line 213
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v6

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v5

    .line 216
    .local v1, whereArgs:[Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "item_links"

    invoke-virtual {v3, v4, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 217
    .local v0, count:I
    const-string v3, "Deleted %d item links"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 218
    return-void
.end method

.method private deleteItemPositions()V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 173
    const-string v2, "account_name = ? AND item_positions.item_list_id NOT IN (SELECT id FROM item_lists WHERE account_name = ?)"

    .line 174
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v6

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v5

    .line 177
    .local v1, whereArgs:[Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "item_positions"

    invoke-virtual {v3, v4, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 178
    .local v0, count:I
    const-string v3, "Deleted %d item positions"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 179
    return-void
.end method

.method private deleteItemPositionsBlacklist()V
    .registers 11

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 229
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v4, "account_name = ?"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    const-string v4, "category_id LIKE \'user/%/state/com.google/read\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    const-string v4, "item_id IN ("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    const-string v4, "SELECT "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    const-string v4, "item_categories.item_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const-string v4, " FROM "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    const-string v4, "item_categories"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    const-string v4, " WHERE "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    const-string v4, "item_categories.account_name = ?"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    const-string v4, "item_categories.item_id = item_positions_blacklist.item_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    const-string v4, "item_categories.stream_id = item_positions_blacklist.category_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 248
    .local v2, where:Ljava/lang/String;
    new-array v3, v9, [Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v8

    .line 249
    .local v3, whereArgs:[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "item_positions_blacklist"

    invoke-virtual {v4, v5, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 251
    .local v1, count:I
    const-string v4, "Deleted %d unread-rows in blacklist"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0           #builder:Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 257
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    const-string v4, "account_name = ?"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string v4, "category_id NOT LIKE \'user/%/state/com.google/read\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string v4, "item_id NOT IN ("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    const-string v4, "SELECT "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    const-string v4, "item_categories.item_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const-string v4, " FROM "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const-string v4, "item_categories"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const-string v4, " WHERE "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string v4, "item_categories.account_name = ?"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v4, "item_categories.item_id = item_positions_blacklist.item_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string v4, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string v4, "item_categories.stream_id = item_positions_blacklist.category_id"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 276
    new-array v3, v9, [Ljava/lang/String;

    .end local v3           #whereArgs:[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v8

    .line 277
    .restart local v3       #whereArgs:[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "item_positions_blacklist"

    invoke-virtual {v4, v5, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 278
    const-string v4, "Deleted %d star-rows in blacklist"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 283
    const-string v2, "account_name = ? AND item_id NOT IN (SELECT items.id FROM items WHERE items.account_name = ?)"

    .line 284
    new-array v3, v9, [Ljava/lang/String;

    .end local v3           #whereArgs:[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v4, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v3, v8

    .line 285
    .restart local v3       #whereArgs:[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "item_positions_blacklist"

    invoke-virtual {v4, v5, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 286
    const-string v4, "Deleted %d dangling rows in blacklist"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 287
    return-void
.end method

.method private deleteItems()V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 186
    const-string v2, "items.account_name = ? AND items.id NOT IN (SELECT item_id FROM item_positions WHERE account_name = ?)"

    .line 187
    .local v2, whereClause:Ljava/lang/String;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v6

    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v3, v3, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v5

    .line 190
    .local v1, whereArgs:[Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "items"

    invoke-virtual {v3, v4, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 191
    .local v0, count:I
    const-string v3, "Deleted %d items"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 192
    return-void
.end method


# virtual methods
.method protected deleteItemLists()V
    .registers 13

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderCleanup;->getMinDate()J

    move-result-wide v3

    .line 135
    .local v3, minDate:J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Removing unreferenced item lists and those predating "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 142
    .local v1, maxDate:J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .local v6, whereBuilder:Ljava/lang/StringBuilder;
    const-string v8, "account_name = ?"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const-string v8, " AND "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string v8, "("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string v8, "timestamp = 0 AND date < ?"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string v8, " OR "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const-string v8, "timestamp != 0 AND timestamp < ?"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string v8, " OR "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string v8, "timestamp > ?"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const-string v8, ")"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 162
    .local v7, whereClause:Ljava/lang/String;
    const/4 v8, 0x4

    new-array v5, v8, [Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v8, v8, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v5, v11

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v10

    const/4 v8, 0x2

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    const/4 v8, 0x3

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    .line 164
    .local v5, whereArgs:[Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v9, "item_lists"

    invoke-virtual {v8, v9, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 165
    .local v0, count:I
    const-string v8, "Deleted %d item lists"

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 166
    return-void
.end method

.method protected final getMinDate()J
    .registers 7

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mMaxAge:J

    .line 126
    .local v0, maxAge:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 127
    .local v2, now:J
    sub-long v4, v2, v0

    return-wide v4
.end method

.method public run()V
    .registers 4

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cleaning-up database for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mAccount:Lcom/google/android/accounts/Account;

    iget-object v1, v1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 97
    :try_start_1b
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderCleanup;->deleteItemLists()V

    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderCleanup;->deleteItemPositions()V

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderCleanup;->deleteItems()V

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderCleanup;->deleteItemCategories()V

    .line 103
    sget-object v0, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderCleanup;->deleteItemPositionsBlacklist()V

    .line 106
    :cond_32
    invoke-direct {p0}, Lcom/google/android/apps/reader/content/ReaderCleanup;->deleteItemLinks()V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3a
    .catchall {:try_start_1b .. :try_end_3a} :catchall_53

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "VACUUM;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/content/ReaderFileCache;->deleteDeprecatedFormats()V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    iget-wide v1, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mMaxAge:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/reader/content/ReaderFileCache;->deleteItemHtmlByAge(J)V

    .line 122
    return-void

    .line 110
    :catchall_53
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderCleanup;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
