.class public Lcom/google/android/apps/reader/content/ReaderDatabase;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ReaderDatabase.java"


# static fields
.field public static final TABLE_FRIENDS:Ljava/lang/String; = "friends"

.field public static final TABLE_ITEMS:Ljava/lang/String; = "items"

.field public static final TABLE_ITEM_CATEGORIES:Ljava/lang/String; = "item_categories"

.field public static final TABLE_ITEM_LINKS:Ljava/lang/String; = "item_links"

.field public static final TABLE_ITEM_LISTS:Ljava/lang/String; = "item_lists"

.field public static final TABLE_ITEM_POSITIONS:Ljava/lang/String; = "item_positions"

.field public static final TABLE_ITEM_POSITIONS_BLACKLIST:Ljava/lang/String; = "item_positions_blacklist"

.field public static final TABLE_OVERVIEW:Ljava/lang/String; = "overview"

.field public static final TABLE_PENDING_ACTIONS:Ljava/lang/String; = "pending_actions"

.field public static final TABLE_RECOMMENDATIONS:Ljava/lang/String; = "recommendations"

.field public static final TABLE_STREAM_DETAILS:Ljava/lang/String; = "stream_details"

.field public static final TABLE_SUBSCRIPTIONS:Ljava/lang/String; = "subscriptions"

.field public static final TABLE_SUBSCRIPTION_CATEGORIES:Ljava/lang/String; = "subscription_categories"

.field public static final TABLE_TAGS:Ljava/lang/String; = "tags"

.field public static final TABLE_TIMESTAMPS:Ljava/lang/String; = "timestamps"

.field public static final TABLE_UNREAD_COUNTS:Ljava/lang/String; = "unread_counts"

.field public static final TABLE_USAGE:Ljava/lang/String; = "usage"

.field public static final TABLE_USER_INFO:Ljava/lang/String; = "user_info"


# instance fields
.field private final mTables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/reader/content/Table;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .registers 26
    .parameter "context"
    .parameter "name"
    .parameter "factory"
    .parameter "version"

    .prologue
    .line 189
    invoke-direct/range {p0 .. p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 180
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/reader/content/ReaderDatabase;->mTables:Ljava/util/ArrayList;

    .line 190
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createTagsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v15

    .line 191
    .local v15, tags:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createSubscriptionsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v14

    .line 192
    .local v14, subscriptions:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createRecommendationsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v11

    .line 193
    .local v11, recommendations:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createFriendsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v2

    .line 194
    .local v2, friends:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createUnreadCountsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v17

    .line 195
    .local v17, unreadCounts:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createSubscriptionCategoriesTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v13

    .line 196
    .local v13, subscriptionCategories:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createItemsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v8

    .line 197
    .local v8, items:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createItemCategoriesTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v3

    .line 198
    .local v3, itemCategories:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createItemLinksTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v4

    .line 199
    .local v4, itemLinks:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createItemPositionsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v6

    .line 200
    .local v6, itemPositions:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createItemPositionsBlacklistTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v7

    .line 201
    .local v7, itemPositionsBlacklist:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createUserInfoTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v19

    .line 202
    .local v19, userInfo:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createOverviewTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v9

    .line 203
    .local v9, overview:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createStreamDetailsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v12

    .line 204
    .local v12, streamDetails:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createPendingActionsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v10

    .line 205
    .local v10, pendingActions:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createTimestampsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v16

    .line 206
    .local v16, timestamps:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createItemListsTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v5

    .line 207
    .local v5, itemLists:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createUsageTable()Lcom/google/android/apps/reader/content/Table;

    move-result-object v18

    .line 208
    .local v18, usage:Lcom/google/android/apps/reader/content/Table;
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 209
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 210
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 211
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 212
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 213
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 214
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 215
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 216
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 217
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 218
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 219
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 220
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 221
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 222
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 223
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 224
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 225
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->addTable(Lcom/google/android/apps/reader/content/Table;)V

    .line 226
    return-void
.end method


# virtual methods
.method protected addTable(Lcom/google/android/apps/reader/content/Table;)V
    .registers 3
    .parameter "table"

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderDatabase;->mTables:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    return-void
.end method

.method protected createAllTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 6
    .parameter "db"

    .prologue
    .line 603
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderDatabase;->mTables:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/reader/content/Table;

    .line 604
    .local v2, table:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual {v2}, Lcom/google/android/apps/reader/content/Table;->getCreateTableStatement()Ljava/lang/String;

    move-result-object v1

    .line 605
    .local v1, sql:Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_6

    .line 607
    .end local v1           #sql:Ljava/lang/String;
    .end local v2           #table:Lcom/google/android/apps/reader/content/Table;
    :cond_1a
    return-void
.end method

.method protected createFriendsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 416
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "friends"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 419
    .local v0, friends:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 420
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "photo_url"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 423
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 424
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "contact_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 425
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "display_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 426
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "photo_uri"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 427
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "stream"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 428
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "email_address"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 429
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "is_me"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 430
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "is_hidden"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 431
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "is_new"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 432
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "uses_reader"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 433
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "is_blocked"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 434
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "has_profile"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 435
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "is_ignored"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 436
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "is_new_follower"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 437
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "is_anonymous"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 438
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "has_shared_items"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 440
    return-object v0
.end method

.method protected createItemCategoriesTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 335
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "item_categories"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 336
    .local v0, table:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 337
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "item_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 338
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "stream_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 345
    const-string v1, "UNIQUE (account_name, item_id, stream_id) ON CONFLICT REPLACE"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addConstraint(Ljava/lang/String;)V

    .line 347
    return-object v0
.end method

.method protected createItemLinksTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 351
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "item_links"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 352
    .local v0, itemLinks:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 353
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "item_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 354
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 355
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "rel"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 356
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "href"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 357
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "type"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 358
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "length"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 359
    return-object v0
.end method

.method protected createItemListsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 502
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "item_lists"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 505
    .local v0, itemLists:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 509
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 515
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "timestamp"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 518
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "date"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 521
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "newest_item_timestamp"

    const-string v3, "INTEGER"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 522
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "last_read_item_timestamp"

    const-string v3, "INTEGER"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 525
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "continuation"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 528
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "title"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 531
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "description"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 534
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "stream_id"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 541
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "exclude_read"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 543
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 545
    return-object v0
.end method

.method protected createItemPositionsBlacklistTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 399
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "item_positions_blacklist"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 402
    .local v0, table:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "item_list_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 405
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 406
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "item_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 409
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "category_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 411
    const-string v1, "UNIQUE (account_name, item_list_id, item_id) ON CONFLICT IGNORE"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addConstraint(Ljava/lang/String;)V

    .line 412
    return-object v0
.end method

.method protected createItemPositionsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 363
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "item_positions"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 366
    .local v0, table:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "item_list_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 369
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 370
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "item_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 373
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "position"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 393
    const-string v1, "UNIQUE (account_name, item_list_id, item_id) ON CONFLICT IGNORE"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addConstraint(Ljava/lang/String;)V

    .line 395
    return-object v0
.end method

.method protected createItemsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 300
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "items"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 301
    .local v0, items:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 302
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 303
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 304
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "external_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 305
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "crawl_time"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 306
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "title"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 307
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "title_plaintext"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 308
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "author"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 309
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "alternate_href"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 310
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "source_title"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 311
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "source_title_plaintext"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 312
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "source_alternate_href"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 313
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "source_stream_id"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 314
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "published"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 315
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "updated"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 316
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "read"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 317
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "starred"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 318
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "liked"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 319
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "shared"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 320
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "locked"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 321
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "source_link"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 322
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "source_post"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 323
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "like_count"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 324
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "broadcaster"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 325
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "annotation"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 328
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "timestamp"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 330
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 331
    return-object v0
.end method

.method protected createOverviewTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 457
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "overview"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 458
    .local v0, overview:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 459
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 460
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_type"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 461
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "sid"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 462
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "summary"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 463
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "image_url"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 464
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "position"

    const-string v3, "INTEGER"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 465
    return-object v0
.end method

.method protected createPendingActionsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 549
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "pending_actions"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 552
    .local v0, pendingActions:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 555
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 558
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "url"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 561
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "data"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 564
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "created"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 567
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "committed"

    const-string v3, "INTEGER"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 569
    return-object v0
.end method

.method protected createRecommendationsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 258
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "recommendations"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 259
    .local v0, recommendations:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 260
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_type"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 261
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 262
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 263
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "position"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 264
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "title"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 265
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "snippet"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 266
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 267
    return-object v0
.end method

.method protected createStreamDetailsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 469
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "stream_details"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 470
    .local v0, details:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 471
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "stream_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 472
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 473
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_type"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 474
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "subscribers"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 475
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "updated"

    const-string v3, "INTEGER"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 476
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "feed_url"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 477
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "day_chart_url"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 478
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "hour_chart_url"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 479
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "week_chart_url"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 480
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "stream_id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 481
    return-object v0
.end method

.method protected createSubscriptionCategoriesTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 284
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "subscription_categories"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, table:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 286
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "subscription_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 287
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "tag_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 294
    const-string v1, "UNIQUE (account_name, subscription_id, tag_id) ON CONFLICT REPLACE"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addConstraint(Ljava/lang/String;)V

    .line 296
    return-object v0
.end method

.method protected createSubscriptionsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 245
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "subscriptions"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 246
    .local v0, subscriptions:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 247
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 248
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 249
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "title"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 250
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "sortid"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 251
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "firstitemmsec"

    const-string v3, "INTEGER"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 252
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "html_url"

    const-string v3, "TEXT"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 253
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 254
    return-object v0
.end method

.method protected createTagsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 231
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "tags"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, tags:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 233
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 234
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 235
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "label"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 236
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "sortid"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 239
    const-string v1, "UNIQUE (account_name, id) ON CONFLICT IGNORE"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addConstraint(Ljava/lang/String;)V

    .line 241
    return-object v0
.end method

.method protected createTimestampsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 485
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "timestamps"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 488
    .local v0, timestamps:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 491
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "url"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 494
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "timestamp"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 496
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "url"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 498
    return-object v0
.end method

.method protected createUnreadCountsTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 271
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "unread_counts"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 272
    .local v0, table:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 273
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 274
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "stream_id"

    const-string v3, "TEXT KEY NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 275
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "newest_item_timestamp"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 276
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "last_read_item_timestamp"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 277
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "unread_count"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 278
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "max_unread_count"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 279
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "stream_id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 280
    return-object v0
.end method

.method protected createUsageTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 573
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "usage"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 576
    .local v0, usage:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 579
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "stream_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 582
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "timestamp"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 584
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "stream_id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->setUnique([Ljava/lang/String;)V

    .line 586
    return-object v0
.end method

.method protected createUserInfoTable()Lcom/google/android/apps/reader/content/Table;
    .registers 5

    .prologue
    .line 444
    new-instance v0, Lcom/google/android/apps/reader/content/Table;

    const-string v1, "user_info"

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/Table;-><init>(Ljava/lang/String;)V

    .line 445
    .local v0, userInfo:Lcom/google/android/apps/reader/content/Table;
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "account_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 446
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "_id"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 447
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "user_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 448
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "user_name"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 449
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "user_profile_id"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 450
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "user_email"

    const-string v3, "TEXT NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 451
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "blogger_user"

    const-string v3, "BOOLEAN NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 452
    new-instance v1, Lcom/google/android/apps/reader/content/Column;

    const-string v2, "signup_time"

    const-string v3, "INTEGER NOT NULL"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/content/Column;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/Table;->addColumn(Lcom/google/android/apps/reader/content/Column;)V

    .line 453
    return-object v0
.end method

.method protected dropAllTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 6
    .parameter "db"

    .prologue
    .line 594
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderDatabase;->mTables:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/reader/content/Table;

    .line 595
    .local v2, table:Lcom/google/android/apps/reader/content/Table;
    invoke-virtual {v2}, Lcom/google/android/apps/reader/content/Table;->getDropTableStatement()Ljava/lang/String;

    move-result-object v1

    .line 596
    .local v1, sql:Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_6

    .line 599
    .end local v1           #sql:Ljava/lang/String;
    .end local v2           #table:Lcom/google/android/apps/reader/content/Table;
    :cond_1a
    const-string v3, "DROP TABLE IF EXISTS item_contents;"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 600
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "db"

    .prologue
    .line 611
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 613
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createAllTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 614
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_d

    .line 616
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 618
    return-void

    .line 616
    :catchall_d
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 5
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 635
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 637
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->dropAllTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 638
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createAllTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 639
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_10

    .line 641
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 643
    return-void

    .line 641
    :catchall_10
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 5
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 622
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 624
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->dropAllTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 625
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderDatabase;->createAllTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 626
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_10

    .line 628
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 630
    return-void

    .line 628
    :catchall_10
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
