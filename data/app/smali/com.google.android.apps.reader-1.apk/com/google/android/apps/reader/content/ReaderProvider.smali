.class public Lcom/google/android/apps/reader/content/ReaderProvider;
.super Landroid/content/ContentProvider;
.source "ReaderProvider.java"

# interfaces
.implements Lcom/google/android/apps/reader/content/ContentLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/content/ReaderProvider$1;
    }
.end annotation


# static fields
.field protected static final ACCOUNTS:I = 0x11

.field private static final ACCOUNT_STREAMS:[Ljava/lang/String; = null

#the value of this static final field might be set in the static constructor
.field private static final API_VERSION:I = 0x0

.field protected static final CLEANUP:I = 0xf

.field private static final DEFAULT_DATABASE_NAME:Ljava/lang/String; = "reader.db"

.field private static final DEFAULT_DATABASE_VERSION:I = 0x49

.field protected static final EMPTY_SUGGEST:I = 0x12

.field protected static final FAVICON:I = 0x1a

.field protected static final FRIENDS:I = 0xd

.field protected static final FRIEND_ID:I = 0xe

.field protected static final FRIEND_PHOTO:I = 0x10

.field private static final GINGERBREAD:I = 0x9

.field protected static final ITEMS:I = 0x7

.field protected static final ITEM_HTML:I = 0x13

.field protected static final ITEM_ID:I = 0x8

.field protected static final LINKS:I = 0xb

.field protected static final LINK_ID:I = 0x20

.field private static final NO_REFRESH:J = 0x7fffffffffffffffL

.field protected static final OVERVIEW:I = 0x1b

.field protected static final OVERVIEW_ID:I = 0x1c

.field private static final PATTERN_CRAWL_TIME_OFFSET:Ljava/util/regex/Pattern; = null

.field protected static final PREFERENCES:I = 0x14

.field protected static final PREFERENCE_ID:I = 0x16

.field protected static final RECOMMENDATIONS:I = 0x1d

.field protected static final RECOMMENDATION_ID:I = 0x1e

.field protected static final STREAMS:I = 0x18

.field protected static final STREAM_DETAILS:I = 0x1f

.field protected static final STREAM_ID:I = 0x19

.field protected static final STREAM_PREFERENCES:I = 0x15

.field protected static final STREAM_PREFERENCE_ID:I = 0x17

.field protected static final SUBSCRIPTIONS:I = 0x1

.field protected static final SUBSCRIPTION_ID:I = 0x2

.field protected static final SUGGEST_ITEMS:I = 0xc

.field protected static final SYNC:I = 0xa

.field private static final TAG:Ljava/lang/String; = "ReaderProvider"

.field protected static final TAGS:I = 0x3

.field protected static final TAG_ID:I = 0x4

.field private static final TIMESTAMP_ACCURACY:J = 0x927c0L

.field protected static final UNREAD_COUNTS:I = 0x5

.field protected static final UNREAD_COUNT_ID:I = 0x6

.field protected static final USER_INFO:I = 0x9


# instance fields
.field private mConfig:Lcom/google/android/apps/reader/util/Config;

.field private mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

.field private final mDatabaseName:Ljava/lang/String;

.field private final mDatabaseVersion:I

.field private mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

.field private mFriendsProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFriendsTables:Ljava/lang/String;

.field private mItemsProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mItemsTables:Ljava/lang/String;

.field private mLinksTables:Ljava/lang/String;

.field private final mNotifyAuthFailure:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mOverviewProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOverviewTables:Ljava/lang/String;

.field private mStreamsProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSubscriptionsProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSubscriptionsTables:Ljava/lang/String;

.field private mTagsProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTagsTables:Ljava/lang/String;

.field private mUnreadCountsProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUriMatcher:Landroid/content/UriMatcher;

.field private final mUserIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/accounts/Account;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 136
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "user/-/state/com.google/reading-list"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "user/-/state/com.google/starred"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/reader/content/ReaderProvider;->ACCOUNT_STREAMS:[Ljava/lang/String;

    .line 205
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/reader/content/ReaderProvider;->API_VERSION:I

    .line 209
    const-string v0, "crawl_time\\s*<\\s*(-\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/content/ReaderProvider;->PATTERN_CRAWL_TIME_OFFSET:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 277
    const-string v0, "reader.db"

    const/16 v1, 0x49

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;-><init>(Ljava/lang/String;I)V

    .line 278
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter "databaseName"
    .parameter "databaseVersion"

    .prologue
    .line 271
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 261
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUserIds:Ljava/util/Map;

    .line 269
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    .line 272
    iput-object p1, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabaseName:Ljava/lang/String;

    .line 273
    iput p2, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabaseVersion:I

    .line 274
    return-void
.end method

.method private blockingGetExternalId(Lcom/google/android/accounts/Account;J)Ljava/lang/String;
    .registers 6
    .parameter "account"
    .parameter "itemId"

    .prologue
    .line 2316
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->itemUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;

    move-result-object v0

    .line 2317
    .local v0, uri:Landroid/net/Uri;
    const-string v1, "external_id"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetStringColumn(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private blockingGetFriendPhotoUrl(Lcom/google/android/accounts/Account;J)Ljava/lang/String;
    .registers 8
    .parameter "account"
    .parameter "contactId"

    .prologue
    .line 2279
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/reader/provider/ReaderContract$Friends;->itemUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;

    move-result-object v1

    .line 2280
    .local v1, uri:Landroid/net/Uri;
    const-string v2, "photo_url"

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetStringColumn(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2281
    .local v0, photoUrl:Ljava/lang/String;
    if-eqz v0, :cond_27

    .line 2282
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 2283
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://www.google.com"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2289
    :cond_27
    :goto_27
    return-object v0

    .line 2284
    :cond_28
    const-string v2, "http:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 2286
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "http:"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_27
.end method

.method private blockingGetStringColumn(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "uri"
    .parameter "columnName"

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2261
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .local v2, projection:[Ljava/lang/String;
    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    .line 2264
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/reader/content/ReaderProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2266
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_22

    :try_start_11
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 2267
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_11 .. :try_end_1b} :catchall_28

    move-result-object v3

    .line 2272
    if-eqz v6, :cond_21

    .line 2273
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_21
    :goto_21
    return-object v3

    .line 2272
    :cond_22
    if-eqz v6, :cond_21

    .line 2273
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_21

    .line 2272
    :catchall_28
    move-exception v0

    if-eqz v6, :cond_2e

    .line 2273
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2e
    throw v0
.end method

.method private blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;
    .registers 6
    .parameter "account"

    .prologue
    .line 2297
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUserIds:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2298
    .local v1, userId:Ljava/lang/String;
    if-eqz v1, :cond_c

    move-object v2, v1

    .line 2306
    .end local v1           #userId:Ljava/lang/String;
    .local v2, userId:Ljava/lang/String;
    :goto_b
    return-object v2

    .line 2301
    .end local v2           #userId:Ljava/lang/String;
    .restart local v1       #userId:Ljava/lang/String;
    :cond_c
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$UserInfo;->itemUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    .line 2302
    .local v0, uri:Landroid/net/Uri;
    const-string v3, "user_id"

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetStringColumn(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2303
    if-eqz v1, :cond_1d

    .line 2304
    iget-object v3, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUserIds:Ljava/util/Map;

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1d
    move-object v2, v1

    .line 2306
    .end local v1           #userId:Ljava/lang/String;
    .restart local v2       #userId:Ljava/lang/String;
    goto :goto_b
.end method

.method private createHttpPostContentHandler(Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;
    .registers 4
    .parameter "account"

    .prologue
    .line 2000
    const/4 v0, 0x0

    .line 2001
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v1

    return-object v1
.end method

.method private createItemList(Landroid/net/Uri;)Lcom/google/android/apps/reader/content/ItemList;
    .registers 11
    .parameter "uri"

    .prologue
    const/4 v7, 0x0

    .line 2049
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v6

    .line 2050
    .local v6, type:I
    const/16 v8, 0xc

    if-ne v6, v8, :cond_1e

    .line 2051
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2052
    .local v1, context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/reader/preference/LocalPreferences;->getAccount(Landroid/content/Context;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 2053
    .local v0, account:Lcom/google/android/accounts/Account;
    const/4 v5, 0x0

    .line 2054
    .local v5, streamId:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 2055
    .local v3, query:Ljava/lang/String;
    if-eqz v0, :cond_1d

    .line 2056
    new-instance v7, Lcom/google/android/apps/reader/content/ItemList;

    invoke-direct {v7, v0, v3, v5}, Lcom/google/android/apps/reader/content/ItemList;-><init>(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 2072
    .end local v1           #context:Landroid/content/Context;
    :cond_1d
    :goto_1d
    return-object v7

    .line 2061
    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v3           #query:Ljava/lang/String;
    .end local v5           #streamId:Ljava/lang/String;
    :cond_1e
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 2062
    .restart local v0       #account:Lcom/google/android/accounts/Account;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 2063
    .restart local v5       #streamId:Ljava/lang/String;
    invoke-direct {p0, v5, v0}, Lcom/google/android/apps/reader/content/ReaderProvider;->setUserIdIfMissing(Ljava/lang/String;Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    .line 2064
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getExcludeTarget(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 2065
    .local v2, excludeTarget:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getRanking(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 2066
    .local v4, ranking:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 2067
    .restart local v3       #query:Ljava/lang/String;
    if-eqz v3, :cond_3e

    .line 2068
    new-instance v7, Lcom/google/android/apps/reader/content/ItemList;

    invoke-direct {v7, v0, v3, v5}, Lcom/google/android/apps/reader/content/ItemList;-><init>(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1d

    .line 2069
    :cond_3e
    if-eqz v5, :cond_1d

    if-eqz v4, :cond_1d

    .line 2070
    new-instance v7, Lcom/google/android/apps/reader/content/ItemList;

    invoke-direct {v7, v0, v5, v2, v4}, Lcom/google/android/apps/reader/content/ItemList;-><init>(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1d
.end method

.method private createItemRefsContentHandler(Lcom/google/android/apps/reader/content/ItemList;JLandroid/os/Bundle;)Ljava/net/ContentHandler;
    .registers 13
    .parameter "itemList"
    .parameter "maxAge"
    .parameter "extras"

    .prologue
    .line 1725
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 1727
    .local v5, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v0, Lcom/google/android/apps/reader/content/ItemRefsContentHandler;

    invoke-direct {v0, p1, v5}, Lcom/google/android/apps/reader/content/ItemRefsContentHandler;-><init>(Lcom/google/android/apps/reader/content/ItemList;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1730
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p1}, Lcom/google/android/apps/reader/content/ItemList;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v7

    .line 1731
    .local v7, account:Lcom/google/android/accounts/Account;
    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v0

    .line 1734
    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSynchronizedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v3

    .line 1736
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedStreamContentHandler;

    move-wide v1, p2

    move-object v4, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedStreamContentHandler;-><init>(JLjava/net/ContentHandler;Lcom/google/android/apps/reader/content/ItemList;Landroid/database/sqlite/SQLiteDatabase;Landroid/os/Bundle;)V

    .line 1738
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    return-object v0
.end method

.method private createItemsContentHandler(Lcom/google/android/accounts/Account;Landroid/os/Bundle;)Lcom/google/android/apps/reader/net/HttpContentHandler;
    .registers 9
    .parameter "account"
    .parameter "extras"

    .prologue
    .line 1747
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1748
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getFileCache()Lcom/google/android/apps/reader/content/ReaderFileCache;

    move-result-object v4

    .line 1749
    .local v4, fileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1750
    .local v1, context:Landroid/content/Context;
    new-instance v0, Lcom/google/android/apps/reader/content/ItemsContentHandler;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/reader/content/ItemsContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ReaderFileCache;Landroid/os/Bundle;)V

    .line 1751
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v2

    return-object v2
.end method

.method private createPreferencesContentHandler(Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;
    .registers 4
    .parameter "account"

    .prologue
    .line 1965
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1966
    .local v0, context:Landroid/content/Context;
    new-instance v1, Lcom/google/android/apps/reader/content/PreferencesContentHandler;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/reader/content/PreferencesContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 1967
    .local v1, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v1

    .line 1968
    return-object v1
.end method

.method private createQuickAddContentHandler(Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;
    .registers 4
    .parameter "account"

    .prologue
    .line 1989
    new-instance v0, Lcom/google/android/apps/reader/content/QuickAddContentHandler;

    invoke-direct {v0}, Lcom/google/android/apps/reader/content/QuickAddContentHandler;-><init>()V

    .line 1990
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v1

    return-object v1
.end method

.method private createStreamPreferencesContentHandler(Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;
    .registers 4
    .parameter "account"

    .prologue
    .line 1978
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1979
    .local v0, context:Landroid/content/Context;
    new-instance v1, Lcom/google/android/apps/reader/content/StreamPreferencesContentHandler;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/reader/content/StreamPreferencesContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 1980
    .local v1, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v1

    .line 1981
    return-object v1
.end method

.method private createTokenContentHandler(Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;
    .registers 3
    .parameter "account"

    .prologue
    .line 1953
    new-instance v0, Lcom/google/android/apps/reader/content/TokenContentHandler;

    invoke-direct {v0}, Lcom/google/android/apps/reader/content/TokenContentHandler;-><init>()V

    .line 1954
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v0

    .line 1955
    return-object v0
.end method

.method private createVirtualStreamTables(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .registers 6
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 605
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "(SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 607
    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 608
    iget-object v1, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 609
    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    const-string v1, "com.google"

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 611
    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    invoke-static {v0, p2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 613
    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 614
    invoke-static {v0, p2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 615
    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "label"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    const-string v1, "NULL AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sortid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    const-string v1, "NULL AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "html_url"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    const-string v1, "NULL AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "subscription_count"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    const-string v1, "NULL AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sort_key_alpha"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    const-string v1, "NULL AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sort_key_manual"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const-string v1, ") AS streams LEFT JOIN unread_counts ON ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    const-string v1, "unread_counts.account_name = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    iget-object v1, p1, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 624
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    const-string v1, "unread_counts.stream_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    invoke-static {v0, p2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 627
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    return-object v0
.end method

.method private feedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Landroid/database/MatrixCursor;Landroid/os/Bundle;)Landroid/database/Cursor;
    .registers 41
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "output"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2095
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v5

    .line 2097
    .local v5, account:Lcom/google/android/accounts/Account;
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/apps/reader/provider/ReaderContract;->getItemCount(Landroid/net/Uri;I)I

    move-result v7

    .line 2098
    .local v7, itemCount:I
    invoke-static {v7}, Lcom/google/android/apps/reader/net/ReaderUri;->roundUpItemCount(I)I

    move-result v7

    .line 2103
    const-wide v24, 0x7fffffffffffffffL

    .line 2104
    .local v24, defaultMaxAge:J
    move-object/from16 v0, p1

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/reader/provider/ReaderContract;->getMaxAge(Landroid/net/Uri;J)J

    move-result-wide v12

    .line 2105
    .local v12, maxAge:J
    const-wide v14, 0x7fffffffffffffffL

    cmp-long v4, v12, v14

    if-nez v4, :cond_5a

    const/16 v30, 0x1

    .line 2106
    .local v30, requery:Z
    :goto_27
    const-string v4, "max-age"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5d

    const/16 v27, 0x1

    .line 2107
    .local v27, hasMaxAge:Z
    :goto_33
    if-eqz v30, :cond_60

    if-eqz v27, :cond_60

    const/16 v26, 0x1

    .line 2110
    .local v26, explicitRequery:Z
    :goto_39
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v31

    .line 2111
    .local v31, type:I
    const/4 v4, 0x1

    move/from16 v0, v31

    if-ne v0, v4, :cond_63

    .line 2112
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->getQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 2113
    .local v6, query:Ljava/lang/String;
    if-eqz v6, :cond_63

    .line 2114
    const/4 v4, 0x0

    new-instance v8, Landroid/database/MatrixCursor;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    aput-object v8, p6, v4

    .local v8, cursor:Landroid/database/MatrixCursor;
    move-object/from16 v4, p0

    move-object/from16 v9, p7

    .line 2115
    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadDirectorySearch(Lcom/google/android/accounts/Account;Ljava/lang/String;ILandroid/database/MatrixCursor;Landroid/os/Bundle;)V

    .line 2246
    .end local v6           #query:Ljava/lang/String;
    .end local v8           #cursor:Landroid/database/MatrixCursor;
    .end local v12           #maxAge:J
    :goto_59
    return-object v8

    .line 2105
    .end local v26           #explicitRequery:Z
    .end local v27           #hasMaxAge:Z
    .end local v30           #requery:Z
    .end local v31           #type:I
    .restart local v12       #maxAge:J
    :cond_5a
    const/16 v30, 0x0

    goto :goto_27

    .line 2106
    .restart local v30       #requery:Z
    :cond_5d
    const/16 v27, 0x0

    goto :goto_33

    .line 2107
    .restart local v27       #hasMaxAge:Z
    :cond_60
    const/16 v26, 0x0

    goto :goto_39

    .line 2121
    .restart local v26       #explicitRequery:Z
    .restart local v31       #type:I
    :cond_63
    packed-switch v31, :pswitch_data_2ce

    .line 2249
    :pswitch_66
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unexpected URI: "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2132
    :pswitch_81
    const-wide v14, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v14, v15, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUserInfo(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2133
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadTags(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2134
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadSubscriptions(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2135
    const/16 v4, 0x18

    move/from16 v0, v31

    if-ne v0, v4, :cond_b0

    .line 2136
    sget-object v4, Lcom/google/android/apps/reader/content/ReaderProvider$1;->$SwitchMap$com$google$android$apps$reader$provider$ReaderContract$Streams$Filter:[I

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->getFilter(Landroid/net/Uri;)Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;->ordinal()I

    move-result v9

    aget v4, v4, v9

    sparse-switch v4, :sswitch_data_310

    .line 2145
    :cond_b0
    :goto_b0
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUnreadCounts(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    move-object/from16 v15, p7

    .line 2146
    invoke-virtual/range {v9 .. v15}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    .end local v12           #maxAge:J
    move-result-object v8

    goto :goto_59

    .line 2139
    .restart local v12       #maxAge:J
    :sswitch_ca
    sget-object v4, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/util/Experiment;->isDisabled()Z

    move-result v4

    if-eqz v4, :cond_b0

    .line 2140
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadFriends(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    goto :goto_b0

    .line 2152
    :pswitch_da
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadFriends(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2153
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUnreadCounts(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    move-object/from16 v15, p7

    .line 2154
    invoke-virtual/range {v9 .. v15}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    .end local v12           #maxAge:J
    move-result-object v8

    goto/16 :goto_59

    .line 2158
    .restart local v12       #maxAge:J
    :pswitch_fc
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUnreadCounts(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    move-object/from16 v15, p7

    .line 2159
    invoke-virtual/range {v9 .. v15}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    .end local v12           #maxAge:J
    move-result-object v8

    goto/16 :goto_59

    .line 2163
    .restart local v12       #maxAge:J
    :pswitch_117
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemList(Landroid/net/Uri;)Lcom/google/android/apps/reader/content/ItemList;

    move-result-object v10

    .line 2164
    .local v10, itemList:Lcom/google/android/apps/reader/content/ItemList;
    if-nez v10, :cond_127

    .line 2165
    new-instance v8, Landroid/database/MatrixCursor;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-direct {v8, v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    goto/16 :goto_59

    .line 2167
    :cond_127
    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->excludesRead()Z

    move-result v4

    if-eqz v4, :cond_159

    if-nez v26, :cond_159

    .line 2170
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/google/android/apps/reader/content/ItemList;->deleteReadItemPositions(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2174
    sget-object v4, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_159

    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->isNewestFirstRanking()Z

    move-result v4

    if-nez v4, :cond_14a

    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->isOldestFirstRanking()Z

    move-result v4

    if-eqz v4, :cond_159

    .line 2177
    :cond_14a
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v32

    .line 2178
    .local v32, userId:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v10, v4, v0}, Lcom/google/android/apps/reader/content/ItemList;->insertUnreadItemPositions(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2181
    .end local v32           #userId:Ljava/lang/String;
    :cond_159
    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v5

    .line 2182
    sget-object v4, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_191

    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->excludesNonTagged()Z

    move-result v4

    if-eqz v4, :cond_191

    if-nez v26, :cond_191

    .line 2184
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v32

    .line 2185
    .restart local v32       #userId:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v10, v4, v0}, Lcom/google/android/apps/reader/content/ItemList;->deleteNonTaggedItemPositions(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2186
    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->isNewestFirstRanking()Z

    move-result v4

    if-nez v4, :cond_188

    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->isOldestFirstRanking()Z

    move-result v4

    if-eqz v4, :cond_191

    .line 2187
    :cond_188
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v10, v4, v0}, Lcom/google/android/apps/reader/content/ItemList;->insertTaggedItemPositions(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2193
    .end local v32           #userId:Ljava/lang/String;
    :cond_191
    const-wide v14, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v14, v15, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUserInfo(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2195
    sget-object v4, Lcom/google/android/apps/reader/util/Experiment;->GOOGLE_PLUS:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/util/Experiment;->isDisabled()Z

    move-result v4

    if-eqz v4, :cond_1b1

    .line 2197
    const-wide v14, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v14, v15, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadFriends(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2203
    :cond_1b1
    const-wide/32 v14, 0x927c0

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v14, v15, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUnreadCounts(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2205
    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->isSearch()Z

    move-result v4

    if-nez v4, :cond_1c7

    invoke-virtual {v10}, Lcom/google/android/apps/reader/content/ItemList;->isMagic()Z

    move-result v4

    if-eqz v4, :cond_1e3

    :cond_1c7
    move-object/from16 v9, p0

    move v11, v7

    move-object/from16 v14, p7

    .line 2206
    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadItems(Lcom/google/android/apps/reader/content/ItemList;IJLandroid/os/Bundle;)V

    :goto_1cf
    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p7

    .line 2211
    invoke-virtual/range {v14 .. v20}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_59

    :cond_1e3
    move-object/from16 v9, p0

    move v11, v7

    move-object/from16 v14, p7

    .line 2208
    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadStream(Lcom/google/android/apps/reader/content/ItemList;IJLandroid/os/Bundle;)V

    goto :goto_1cf

    .line 2214
    .end local v10           #itemList:Lcom/google/android/apps/reader/content/ItemList;
    :pswitch_1ec
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v28

    .line 2215
    .local v28, itemId:J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    move-object/from16 v3, p7

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadItem(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p7

    .line 2216
    invoke-virtual/range {v14 .. v20}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_59

    .line 2219
    .end local v28           #itemId:J
    :pswitch_20d
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Links;->getItemId(Landroid/net/Uri;)Ljava/lang/Long;

    move-result-object v28

    .line 2220
    .local v28, itemId:Ljava/lang/Long;
    if-nez v28, :cond_22e

    .line 2221
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Item ID is missing: "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2223
    :cond_22e
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v14, v15, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadItem(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p7

    .line 2224
    invoke-virtual/range {v14 .. v20}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_59

    .line 2227
    .end local v28           #itemId:Ljava/lang/Long;
    :pswitch_24d
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUserInfo(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p7

    .line 2228
    invoke-virtual/range {v14 .. v20}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_59

    .line 2231
    :pswitch_268
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v16

    .local v16, streamId:Ljava/lang/String;
    move-object/from16 v14, p0

    move-object v15, v5

    move-wide/from16 v17, v12

    move-object/from16 v19, p7

    .line 2232
    invoke-direct/range {v14 .. v19}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadStreamDetails(Lcom/google/android/accounts/Account;Ljava/lang/String;JLandroid/os/Bundle;)V

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    move-object/from16 v19, p2

    move-object/from16 v20, p3

    move-object/from16 v21, p4

    move-object/from16 v22, p5

    move-object/from16 v23, p7

    .line 2233
    invoke-virtual/range {v17 .. v23}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_59

    .line 2237
    .end local v16           #streamId:Ljava/lang/String;
    :pswitch_28a
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadSubscriptions(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2238
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUnreadCounts(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 2240
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadOverview(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    move-object/from16 v19, p2

    move-object/from16 v20, p3

    move-object/from16 v21, p4

    move-object/from16 v22, p5

    move-object/from16 v23, p7

    .line 2241
    invoke-virtual/range {v17 .. v23}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_59

    .line 2245
    :pswitch_2b3
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v12, v13, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadRecommendations(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    move-object/from16 v19, p2

    move-object/from16 v20, p3

    move-object/from16 v21, p4

    move-object/from16 v22, p5

    move-object/from16 v23, p7

    .line 2246
    invoke-virtual/range {v17 .. v23}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_59

    .line 2121
    :pswitch_data_2ce
    .packed-switch 0x1
        :pswitch_81
        :pswitch_81
        :pswitch_81
        :pswitch_81
        :pswitch_fc
        :pswitch_fc
        :pswitch_117
        :pswitch_1ec
        :pswitch_24d
        :pswitch_66
        :pswitch_20d
        :pswitch_117
        :pswitch_da
        :pswitch_da
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_66
        :pswitch_81
        :pswitch_81
        :pswitch_66
        :pswitch_28a
        :pswitch_66
        :pswitch_2b3
        :pswitch_2b3
        :pswitch_268
    .end packed-switch

    .line 2136
    :sswitch_data_310
    .sparse-switch
        0x1 -> :sswitch_ca
        0x8 -> :sswitch_ca
    .end sparse-switch
.end method

.method private getSubscriptionOrdering(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 844
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/reader/content/ReaderProvider;->streamPreferences(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 845
    .local v0, preferences:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/reader/preference/StreamPreferences;->getSubscriptionOrdering(Landroid/content/SharedPreferences;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v1

    .line 849
    .end local v0           #preferences:Landroid/content/SharedPreferences;
    :goto_8
    return-object v1

    :catch_9
    move-exception v1

    const/4 v1, 0x0

    goto :goto_8
.end method

.method private loadDirectorySearch(Lcom/google/android/accounts/Account;Ljava/lang/String;ILandroid/database/MatrixCursor;Landroid/os/Bundle;)V
    .registers 13
    .parameter "account"
    .parameter "query"
    .parameter "itemCount"
    .parameter "cursor"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1680
    new-instance v0, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;

    invoke-direct {v0, p4, p1}, Lcom/google/android/apps/reader/content/DirectorySearchContentHandler;-><init>(Landroid/database/MatrixCursor;Lcom/google/android/accounts/Account;)V

    .line 1683
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v0

    .line 1686
    invoke-static {p2}, Lcom/google/android/apps/reader/net/ReaderUri;->directorySearch(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1687
    .local v1, baseDocumentUri:Landroid/net/Uri;
    const-string v2, "start"

    .line 1688
    .local v2, indexParameter:Ljava/lang/String;
    const/4 v3, 0x0

    .line 1689
    .local v3, firstIndex:I
    const/16 v4, 0xa

    .local v4, pageSize:I
    move v5, p3

    move-object v6, p5

    .line 1690
    invoke-static/range {v0 .. v6}, Lcom/google/android/feeds/FeedLoader;->loadIndexedFeed(Ljava/net/ContentHandler;Landroid/net/Uri;Ljava/lang/String;IIILandroid/os/Bundle;)V

    .line 1692
    return-void
.end method

.method private loadFriends(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 14
    .parameter "account"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1628
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1629
    .local v7, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1630
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->friendList()Landroid/net/Uri;

    move-result-object v8

    .line 1633
    .local v8, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/reader/content/FriendsContentHandler;

    invoke-direct {v0, v7, p1, v4}, Lcom/google/android/apps/reader/content/FriendsContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1636
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v3

    .line 1639
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedContentHandler;

    move-wide v1, p2

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1641
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v8}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1642
    return-void
.end method

.method private loadItem(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 15
    .parameter "account"
    .parameter "itemId"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide v1, 0x7fffffffffffffffL

    .line 1788
    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemsContentHandler(Lcom/google/android/accounts/Account;Landroid/os/Bundle;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v3

    .line 1789
    .local v3, http:Lcom/google/android/apps/reader/net/HttpContentHandler;
    const/4 v5, 0x1

    new-array v5, v5, [J

    const/4 v6, 0x0

    aput-wide p2, v5, v6

    invoke-static {v5}, Lcom/google/android/apps/reader/net/ReaderUri;->createStreamItemsContentsData([J)Lorg/apache/http/HttpEntity;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/reader/net/HttpContentHandler;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1791
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1795
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, p1, v1, v2, p4}, Lcom/google/android/apps/reader/content/ReaderProvider;->loadUserInfo(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 1798
    new-instance v0, Lcom/google/android/apps/reader/content/CachedItemContentHandler;

    move-object v5, p1

    move-wide v6, p2

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/reader/content/CachedItemContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V

    .line 1801
    .local v0, handler:Ljava/net/ContentHandler;
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/apps/reader/net/ReaderUri;->streamItemsContents(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1802
    .local v9, uri:Landroid/net/Uri;
    invoke-static {v0, v9}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1803
    return-void
.end method

.method private loadItems(Lcom/google/android/apps/reader/content/ItemList;IJLandroid/os/Bundle;)V
    .registers 15
    .parameter "itemList"
    .parameter "itemCount"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1769
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 1770
    .local v6, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p1}, Lcom/google/android/apps/reader/content/ItemList;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v8

    .line 1772
    .local v8, account:Lcom/google/android/accounts/Account;
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemRefsContentHandler(Lcom/google/android/apps/reader/content/ItemList;JLandroid/os/Bundle;)Ljava/net/ContentHandler;

    move-result-object v4

    .line 1774
    .local v4, refsHandler:Ljava/net/ContentHandler;
    invoke-direct {p0, v8, p5}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemsContentHandler(Lcom/google/android/accounts/Account;Landroid/os/Bundle;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v5

    .local v5, itemsHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;
    move-object v0, p1

    move v1, p2

    move-wide v2, p3

    move-object v7, p5

    .line 1777
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/reader/content/ItemsFeedResolver;->loadItems(Lcom/google/android/apps/reader/content/ItemList;IJLjava/net/ContentHandler;Lcom/google/android/apps/reader/net/HttpContentHandler;Landroid/database/sqlite/SQLiteDatabase;Landroid/os/Bundle;)V

    .line 1779
    return-void
.end method

.method private loadOverview(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 14
    .parameter "account"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1905
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1906
    .local v7, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1907
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->overview()Landroid/net/Uri;

    move-result-object v8

    .line 1910
    .local v8, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/reader/content/OverviewContentHandler;

    invoke-direct {v0, v7, p1, v4}, Lcom/google/android/apps/reader/content/OverviewContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1913
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v3

    .line 1916
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedContentHandler;

    move-wide v1, p2

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1918
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v8}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1919
    return-void
.end method

.method private loadRecommendations(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 15
    .parameter "account"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1928
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1929
    .local v7, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1932
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    const/16 v8, 0x32

    .line 1933
    .local v8, n:I
    invoke-static {v8}, Lcom/google/android/apps/reader/net/ReaderUri;->recommendationList(I)Landroid/net/Uri;

    move-result-object v9

    .line 1936
    .local v9, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;

    invoke-direct {v0, v7, p1, v4}, Lcom/google/android/apps/reader/content/RecommendationListContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1939
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v3

    .line 1942
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedContentHandler;

    move-wide v1, p2

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1944
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v9}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1945
    return-void
.end method

.method private loadStream(Lcom/google/android/apps/reader/content/ItemList;IJLandroid/os/Bundle;)V
    .registers 27
    .parameter "itemList"
    .parameter "itemCount"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1816
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 1817
    .local v5, context:Landroid/content/Context;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/reader/content/ItemList;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v16

    .line 1818
    .local v16, account:Lcom/google/android/accounts/Account;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 1819
    .local v7, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getFileCache()Lcom/google/android/apps/reader/content/ReaderFileCache;

    move-result-object v8

    .line 1820
    .local v8, cache:Lcom/google/android/apps/reader/content/ReaderFileCache;
    new-instance v4, Lcom/google/android/apps/reader/content/StreamContentHandler;

    move-object/from16 v6, p1

    move-object/from16 v9, p5

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/reader/content/StreamContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/apps/reader/content/ItemList;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ReaderFileCache;Landroid/os/Bundle;)V

    .line 1821
    .local v4, handler:Ljava/net/ContentHandler;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v4

    .line 1822
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSynchronizedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v4

    .line 1823
    new-instance v9, Lcom/google/android/apps/reader/content/CachedStreamContentHandler;

    move-wide/from16 v10, p3

    move-object v12, v4

    move-object/from16 v13, p1

    move-object v14, v7

    move-object/from16 v15, p5

    invoke-direct/range {v9 .. v15}, Lcom/google/android/apps/reader/content/CachedStreamContentHandler;-><init>(JLjava/net/ContentHandler;Lcom/google/android/apps/reader/content/ItemList;Landroid/database/sqlite/SQLiteDatabase;Landroid/os/Bundle;)V

    .line 1824
    .end local v4           #handler:Ljava/net/ContentHandler;
    .local v9, handler:Ljava/net/ContentHandler;
    const/16 v6, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/apps/reader/content/ItemList;->createStreamContentsUri(I)Landroid/net/Uri;

    move-result-object v19

    .line 1825
    .local v19, uri:Landroid/net/Uri;
    const-string v17, "c"

    .line 1827
    .local v17, paramContinuation:Ljava/lang/String;
    sget-object v6, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_74

    move-object v6, v9

    .line 1828
    check-cast v6, Lcom/google/android/apps/reader/content/CachedStreamContentHandler;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v6, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->requireNetworkRefresh(Lcom/google/android/apps/reader/content/CachedStreamContentHandler;Landroid/net/Uri;)Z

    move-result v18

    .line 1830
    .local v18, requireNetworkRefresh:Z
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move/from16 v2, p2

    move-object/from16 v3, p5

    invoke-static {v9, v0, v1, v2, v3}, Lcom/google/android/feeds/FeedLoader;->loadContinuedFeed(Ljava/net/ContentHandler;Landroid/net/Uri;Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 1831
    if-eqz v18, :cond_73

    .line 1832
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v20

    .line 1833
    .local v20, userId:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v6, v1}, Lcom/google/android/apps/reader/content/ItemList;->cleanupAfterRemoteLoad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 1838
    .end local v18           #requireNetworkRefresh:Z
    .end local v20           #userId:Ljava/lang/String;
    :cond_73
    :goto_73
    return-void

    .line 1836
    :cond_74
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move/from16 v2, p2

    move-object/from16 v3, p5

    invoke-static {v9, v0, v1, v2, v3}, Lcom/google/android/feeds/FeedLoader;->loadContinuedFeed(Ljava/net/ContentHandler;Landroid/net/Uri;Ljava/lang/String;ILandroid/os/Bundle;)V

    goto :goto_73
.end method

.method private loadStreamDetails(Lcom/google/android/accounts/Account;Ljava/lang/String;JLandroid/os/Bundle;)V
    .registers 16
    .parameter "account"
    .parameter "streamId"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1882
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1883
    .local v7, context:Landroid/content/Context;
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 1884
    .local v8, resolver:Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1885
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {p2}, Lcom/google/android/apps/reader/net/ReaderUri;->streamDetails(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1888
    .local v9, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/reader/content/StreamDetailsContentHandler;

    invoke-direct {v0, p1, p2, v8, v4}, Lcom/google/android/apps/reader/content/StreamDetailsContentHandler;-><init>(Lcom/google/android/accounts/Account;Ljava/lang/String;Landroid/content/ContentResolver;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1891
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v3

    .line 1894
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedContentHandler;

    move-wide v1, p3

    move-object v5, p1

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1896
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v9}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1897
    return-void
.end method

.method private loadSubscriptions(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 15
    .parameter "account"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1648
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1649
    .local v7, context:Landroid/content/Context;
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 1650
    .local v8, resolver:Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1651
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->subscriptionList()Landroid/net/Uri;

    move-result-object v9

    .line 1654
    .local v9, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/reader/content/SubscriptionListContentHandler;

    invoke-direct {v0, p1, v8, v4}, Lcom/google/android/apps/reader/content/SubscriptionListContentHandler;-><init>(Lcom/google/android/accounts/Account;Landroid/content/ContentResolver;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1657
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v0

    .line 1660
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSynchronizedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v3

    .line 1663
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedContentHandler;

    move-wide v1, p2

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1665
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v9}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1666
    return-void
.end method

.method private loadTags(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 14
    .parameter "account"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1605
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1606
    .local v7, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1607
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->tagList()Landroid/net/Uri;

    move-result-object v8

    .line 1610
    .local v8, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/reader/content/TagListContentHandler;

    invoke-direct {v0, v7, p1, v4}, Lcom/google/android/apps/reader/content/TagListContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1613
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v0

    .line 1616
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSynchronizedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v3

    .line 1619
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedContentHandler;

    move-wide v1, p2

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1621
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v8}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1622
    return-void
.end method

.method private loadUnreadCounts(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 14
    .parameter "account"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1698
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1699
    .local v7, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1700
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->unreadCount()Landroid/net/Uri;

    move-result-object v8

    .line 1703
    .local v8, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/reader/content/UnreadCountContentHandler;

    invoke-direct {v0, v7, p1, v4}, Lcom/google/android/apps/reader/content/UnreadCountContentHandler;-><init>(Landroid/content/Context;Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1706
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v0

    .line 1709
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSynchronizedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v3

    .line 1712
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedContentHandler;

    move-wide v1, p2

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1714
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v8}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1715
    return-void
.end method

.method private loadUserInfo(Lcom/google/android/accounts/Account;JLandroid/os/Bundle;)V
    .registers 16
    .parameter "account"
    .parameter "maxAge"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1855
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 1856
    .local v8, context:Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 1857
    .local v7, contentResolver:Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1858
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Lcom/google/android/apps/reader/net/ReaderUri;->userInfo()Landroid/net/Uri;

    move-result-object v9

    .line 1860
    .local v9, uri:Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUserIds:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1861
    .local v10, userId:Ljava/lang/String;
    if-eqz v10, :cond_1f

    .line 1864
    const-string v1, "com.google.reader.cursor.extra.USER_ID"

    invoke-virtual {p4, v1, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1868
    :cond_1f
    new-instance v0, Lcom/google/android/apps/reader/content/UserInfoContentHandler;

    invoke-direct {v0, p1, v7, v4, p4}, Lcom/google/android/apps/reader/content/UserInfoContentHandler;-><init>(Lcom/google/android/accounts/Account;Landroid/content/ContentResolver;Landroid/database/sqlite/SQLiteDatabase;Landroid/os/Bundle;)V

    .line 1871
    .local v0, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v3

    .line 1875
    .end local v0           #handler:Ljava/net/ContentHandler;
    .local v3, handler:Ljava/net/ContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/CachedUserInfoContentHandler;

    move-wide v1, p2

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/reader/content/CachedUserInfoContentHandler;-><init>(JLjava/net/ContentHandler;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Landroid/os/Bundle;)V

    .line 1877
    .end local v3           #handler:Ljava/net/ContentHandler;
    .restart local v0       #handler:Ljava/net/ContentHandler;
    invoke-static {v0, v9}, Lcom/google/android/feeds/FeedLoader;->loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V

    .line 1878
    return-void
.end method

.method private notifyChange(Landroid/net/Uri;)V
    .registers 5
    .parameter "uri"

    .prologue
    .line 2575
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2578
    .local v1, context:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 2579
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getSyncToNetwork(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2580
    invoke-static {v1, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->requestSyncUpload(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 2583
    :cond_11
    invoke-static {v1, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->notifyChange(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 2584
    return-void
.end method

.method private requireNetworkRefresh(Lcom/google/android/apps/reader/content/CachedStreamContentHandler;Landroid/net/Uri;)Z
    .registers 8
    .parameter "handler"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1842
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1843
    .local v2, spec:Ljava/lang/String;
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1844
    .local v3, url:Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 1845
    .local v0, connection:Ljava/net/URLConnection;
    invoke-virtual {p1, v0}, Lcom/google/android/apps/reader/content/CachedStreamContentHandler;->isLocal(Ljava/net/URLConnection;)Z

    move-result v1

    .line 1846
    .local v1, loadLocally:Z
    if-nez v1, :cond_15

    const/4 v4, 0x1

    :goto_14
    return v4

    :cond_15
    const/4 v4, 0x0

    goto :goto_14
.end method

.method private retrieveFile(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .registers 10
    .parameter "account"
    .parameter "authTokenType"
    .parameter "url"
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2823
    if-nez p1, :cond_a

    .line 2824
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Account is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2826
    :cond_a
    if-nez p3, :cond_14

    .line 2827
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "URL is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2829
    :cond_14
    if-nez p4, :cond_1e

    .line 2830
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "File is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2834
    :cond_1e
    :try_start_1e
    new-instance v2, Lcom/google/android/apps/reader/net/FileOutputContentHandler;

    invoke-direct {v2, p4}, Lcom/google/android/apps/reader/net/FileOutputContentHandler;-><init>(Ljava/io/File;)V

    .line 2837
    .local v2, handler:Ljava/net/ContentHandler;
    invoke-virtual {p0, v2, p1, p2}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;Ljava/lang/String;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v2

    .line 2840
    invoke-static {p3, v2}, Lcom/google/android/apps/reader/net/HttpContentHandler;->getContent(Ljava/lang/String;Ljava/net/ContentHandler;)Ljava/lang/Object;
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_2a} :catch_2b

    .line 2847
    return-void

    .line 2841
    .end local v2           #handler:Ljava/net/ContentHandler;
    :catch_2b
    move-exception v0

    .line 2843
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    .line 2844
    .local v1, fileNotFound:Ljava/io/FileNotFoundException;
    invoke-virtual {v1, v0}, Ljava/io/FileNotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 2845
    throw v1
.end method

.method private retrieveFile(Ljava/lang/String;Ljava/io/File;)V
    .registers 8
    .parameter "url"
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2857
    if-nez p1, :cond_a

    .line 2858
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "URL is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2860
    :cond_a
    if-nez p2, :cond_14

    .line 2861
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "File is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2865
    :cond_14
    :try_start_14
    new-instance v2, Lcom/google/android/apps/reader/net/FileOutputContentHandler;

    invoke-direct {v2, p2}, Lcom/google/android/apps/reader/net/FileOutputContentHandler;-><init>(Ljava/io/File;)V

    .line 2868
    .local v2, handler:Ljava/net/ContentHandler;
    invoke-static {p1, v2}, Lcom/google/android/apps/reader/net/HttpContentHandler;->getContent(Ljava/lang/String;Ljava/net/ContentHandler;)Ljava/lang/Object;
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_1c} :catch_1d

    .line 2875
    return-void

    .line 2869
    .end local v2           #handler:Ljava/net/ContentHandler;
    :catch_1d
    move-exception v0

    .line 2871
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    .line 2872
    .local v1, fileNotFound:Ljava/io/FileNotFoundException;
    invoke-virtual {v1, v0}, Ljava/io/FileNotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 2873
    throw v1
.end method

.method private setUserIdIfMissing(Ljava/lang/String;Lcom/google/android/accounts/Account;)Ljava/lang/String;
    .registers 6
    .parameter "streamId"
    .parameter "account"

    .prologue
    .line 2321
    if-eqz p1, :cond_15

    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2322
    invoke-direct {p0, p2}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 2323
    .local v0, userId:Ljava/lang/String;
    if-nez v0, :cond_16

    .line 2324
    const-string v1, "ReaderProvider"

    const-string v2, "Failed to get user ID"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2329
    .end local v0           #userId:Ljava/lang/String;
    :cond_15
    :goto_15
    return-object p1

    .line 2326
    .restart local v0       #userId:Ljava/lang/String;
    :cond_16
    invoke-static {p1, v0}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_15
.end method

.method private sortKeyAlpha()Ljava/lang/String;
    .registers 5

    .prologue
    .line 863
    const-string v1, "(id LIKE \'%/label/%\' AND subscription_categories.tag_id is NULL)"

    .line 864
    .local v1, isTag:Ljava/lang/String;
    const-string v0, "(id LIKE \'feed/%\' OR id LIKE \'webfeed/%\')"

    .line 867
    .local v0, isSubscription:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " * 2) + "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private sortKeyManual(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;
    .registers 15
    .parameter "account"
    .parameter "streamId"

    .prologue
    const/16 v11, 0x29

    const/16 v10, 0x28

    .line 879
    if-nez p1, :cond_e

    .line 880
    new-instance v8, Ljava/lang/NullPointerException;

    const-string v9, "Account is null"

    invoke-direct {v8, v9}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 882
    :cond_e
    if-nez p2, :cond_18

    .line 883
    new-instance v8, Ljava/lang/NullPointerException;

    const-string v9, "Stream ID is null"

    invoke-direct {v8, v9}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 885
    :cond_18
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/reader/content/ReaderProvider;->getSubscriptionOrdering(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 886
    .local v7, subscriptionOrdering:Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_25

    .line 887
    const-string v8, "0"

    .line 926
    :goto_24
    return-object v8

    .line 889
    :cond_25
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v3

    .line 890
    .local v3, length:I
    rem-int/lit8 v8, v3, 0x8

    if-eqz v8, :cond_48

    .line 891
    const-string v8, "ReaderProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid subscription-ordering: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    const-string v8, "0"

    goto :goto_24

    .line 894
    :cond_48
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    div-int/lit8 v4, v8, 0x8

    .line 898
    .local v4, n:I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 899
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 900
    const/4 v2, 0x0

    .local v2, i:I
    :goto_57
    if-ge v2, v4, :cond_93

    .line 901
    if-eqz v2, :cond_60

    .line 902
    const-string v8, " + "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    :cond_60
    mul-int/lit8 v6, v2, 0x8

    .line 905
    .local v6, start:I
    add-int/lit8 v1, v6, 0x8

    .line 906
    .local v1, end:I
    invoke-virtual {v7, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 907
    .local v5, sortId:Ljava/lang/String;
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 910
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 911
    const-string v8, "sortid = \'"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 912
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 920
    const-string v8, " * "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    sub-int v8, v4, v2

    add-int/lit8 v8, v8, 0x1

    neg-int v8, v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 923
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 900
    add-int/lit8 v2, v2, 0x1

    goto :goto_57

    .line 925
    .end local v1           #end:I
    .end local v5           #sortId:Ljava/lang/String;
    .end local v6           #start:I
    :cond_93
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 926
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_24
.end method


# virtual methods
.method protected buildDatabaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    .registers 30
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "extras"

    .prologue
    .line 934
    const/4 v6, 0x0

    .line 935
    .local v6, groupBy:Ljava/lang/String;
    const/4 v7, 0x0

    .line 936
    .local v7, having:Ljava/lang/String;
    const/4 v9, 0x0

    .line 937
    .local v9, limit:Ljava/lang/String;
    const/4 v11, 0x0

    .line 938
    .local v11, defaultOrder:Ljava/lang/String;
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 939
    .local v2, builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v10

    .line 940
    .local v10, account:Lcom/google/android/accounts/Account;
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v21

    .line 941
    .local v21, type:I
    packed-switch v21, :pswitch_data_786

    .line 1343
    :pswitch_14
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 943
    :pswitch_2f
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->isUnsynchronized(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_56

    .line 945
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 946
    const-string v3, "committed IS NULL"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 947
    const-string v3, "pending_actions"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1345
    :goto_43
    if-eqz v11, :cond_49

    .line 1346
    if-nez p5, :cond_76b

    move-object/from16 p5, v11

    :cond_49
    :goto_49
    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    .line 1348
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 950
    :cond_56
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Full account list query is not supported"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 953
    :pswitch_5e
    const-string v3, "stream_id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 954
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 955
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 956
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 959
    :pswitch_74
    const-string v3, "unread_counts.account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 960
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 961
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 962
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUnreadCountsProjectionMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 963
    const-string v3, "unread_counts"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_43

    .line 966
    :pswitch_90
    const-string v3, "subscriptions"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 967
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 968
    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 969
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 970
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 971
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 974
    :pswitch_b0
    const-string v3, "subscriptions.account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 975
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 976
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 978
    new-instance v19, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mSubscriptionsTables:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 979
    .local v19, tables:Ljava/lang/StringBuilder;
    new-instance v14, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mSubscriptionsProjectionMap:Ljava/util/Map;

    invoke-direct {v14, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 982
    .local v14, projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->getQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v15

    .line 983
    .local v15, query:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->getTagId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v20

    .line 984
    .local v20, tagId:Ljava/lang/String;
    if-eqz v15, :cond_f8

    .line 987
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not a database query"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 989
    :cond_f8
    if-eqz v20, :cond_1ff

    .line 990
    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_111

    .line 991
    const-string v3, "com.google.reader.cursor.extra.USER_ID"

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 992
    .local v22, userId:Ljava/lang/String;
    if-nez v22, :cond_1f5

    .line 993
    const-string v3, "ReaderProvider"

    const-string v4, "Failed to get user ID"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    .end local v22           #userId:Ljava/lang/String;
    :cond_111
    :goto_111
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1000
    const-string v3, "subscription_categories.account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1001
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1002
    const-string v3, "subscriptions.account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1003
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1004
    const-string v3, "subscription_categories.subscription_id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1005
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1006
    const-string v3, "subscriptions.id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1007
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1008
    const-string v3, "subscription_categories.tag_id = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1009
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1012
    const-string v3, ", "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "subscription_categories"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1035
    :goto_155
    if-eqz v20, :cond_24d

    move-object/from16 v17, v20

    .line 1036
    .local v17, streamId:Ljava/lang/String;
    :goto_159
    const-string v3, "sort_key_manual"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v10, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->sortKeyManual(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sort_key_manual"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1038
    const-string v3, "sort_key_alpha"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->sortKeyAlpha()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sort_key_alpha"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1040
    const-string v3, "sort_key_manual"

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b5

    const-string v3, "sort_key_alpha"

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e9

    .line 1042
    :cond_1b5
    const-string v3, "sort_key_manual"

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d4

    .line 1045
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sort_key_alpha"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 1049
    :cond_1d4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", title COLLATE NOCASE ASC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 1051
    :cond_1e9
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1052
    invoke-virtual {v2, v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_43

    .line 995
    .end local v17           #streamId:Ljava/lang/String;
    .restart local v22       #userId:Ljava/lang/String;
    :cond_1f5
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_111

    .line 1015
    .end local v22           #userId:Ljava/lang/String;
    :cond_1ff
    const-string v3, " LEFT JOIN subscription_categories ON ("

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1016
    const-string v3, "subscriptions.account_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1017
    const-string v3, " = "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1018
    const-string v3, "subscription_categories.account_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1019
    const-string v3, " AND "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1020
    const-string v3, "subscriptions.id = subscription_categories.subscription_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1021
    const-string v3, ")"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1023
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->isUntagged(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_242

    .line 1025
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1026
    const-string v3, "subscription_categories.subscription_id is NULL"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_155

    .line 1029
    :cond_242
    const-string v6, "subscriptions.id, subscription_categories.subscription_id"

    .line 1030
    const-string v3, "tag_count"

    const-string v4, "COUNT(subscription_categories.tag_id) AS tag_count"

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_155

    .line 1035
    :cond_24d
    const-string v17, "user/-/state/com.google/root"

    goto/16 :goto_159

    .line 1056
    .end local v14           #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v15           #query:Ljava/lang/String;
    .end local v19           #tables:Ljava/lang/StringBuilder;
    .end local v20           #tagId:Ljava/lang/String;
    :pswitch_251
    const-string v3, "tags"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1057
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1058
    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1059
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1060
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1061
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1064
    :pswitch_271
    const-string v3, "tags"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1065
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1066
    const-string v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1067
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1068
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1072
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1073
    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1074
    const-string v3, " LIKE \'%/label/%\'"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1076
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->getSubscriptionId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v18

    .line 1077
    .local v18, subscriptionId:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->getItemId(Landroid/net/Uri;)Ljava/lang/Long;

    move-result-object v12

    .line 1078
    .local v12, itemId:Ljava/lang/Long;
    new-instance v19, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mTagsTables:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1079
    .restart local v19       #tables:Ljava/lang/StringBuilder;
    new-instance v14, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mTagsProjectionMap:Ljava/util/Map;

    invoke-direct {v14, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1081
    .restart local v14       #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v18, :cond_396

    .line 1082
    const-string v3, " LEFT JOIN subscription_categories ON ("

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1084
    const-string v3, "tags.account_name = subscription_categories.account_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1085
    const-string v3, " AND "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1086
    const-string v3, "tags.id = subscription_categories.tag_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1087
    const-string v3, " AND "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1088
    const-string v3, "subscription_categories.subscription_id = "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1089
    invoke-static/range {v18 .. v18}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1091
    const-string v3, ")"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1092
    const-string v6, "tags.id, subscription_categories.tag_id"

    .line 1093
    const-string v3, "subscription_count"

    const-string v4, "COUNT(subscription_categories.subscription_id) AS subscription_count"

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1128
    :goto_2fa
    const-string v3, "sort_key_manual"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "user/-/state/com.google/root"

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v5}, Lcom/google/android/apps/reader/content/ReaderProvider;->sortKeyManual(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sort_key_manual"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1131
    const-string v3, "sort_key_alpha"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->sortKeyAlpha()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sort_key_alpha"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1133
    const-string v3, "sort_key_manual"

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_356

    const-string v3, "sort_key_alpha"

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_38a

    .line 1135
    :cond_356
    const-string v3, "sort_key_manual"

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_375

    .line 1138
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sort_key_alpha"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 1142
    :cond_375
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", label COLLATE NOCASE ASC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 1144
    :cond_38a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1145
    invoke-virtual {v2, v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_43

    .line 1096
    :cond_396
    if-eqz v12, :cond_3c2

    .line 1099
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1100
    const-string v3, "item_categories.item_id = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1101
    invoke-virtual {v12}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1102
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1103
    const-string v3, "item_categories.stream_id = tags.id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1104
    const-string v3, ", "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "item_categories"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2fa

    .line 1106
    :cond_3c2
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->withoutSubscriptions(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_404

    .line 1107
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1108
    const-string v3, "subscription_categories.subscription_id is NULL"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1109
    const-string v3, "subscription_count"

    const-string v4, "0"

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1117
    :goto_3d9
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->withSubscriptions(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_40e

    .line 1122
    :goto_3df
    const-string v3, " JOIN subscription_categories ON ("

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1123
    const-string v3, "tags.account_name = subscription_categories.account_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1124
    const-string v3, " AND "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1125
    const-string v3, "tags.id = subscription_categories.tag_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1126
    const-string v3, ")"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2fa

    .line 1112
    :cond_404
    const-string v6, "tags.id, subscription_categories.tag_id"

    .line 1113
    const-string v3, "subscription_count"

    const-string v4, "COUNT(subscription_categories.subscription_id) AS subscription_count"

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3d9

    .line 1120
    :cond_40e
    const-string v3, " LEFT"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3df

    .line 1149
    .end local v12           #itemId:Ljava/lang/Long;
    .end local v14           #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v18           #subscriptionId:Ljava/lang/String;
    .end local v19           #tables:Ljava/lang/StringBuilder;
    :pswitch_416
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v17

    .line 1150
    .restart local v17       #streamId:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v10, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createVirtualStreamTables(Lcom/google/android/accounts/Account;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 1151
    .restart local v19       #tables:Ljava/lang/StringBuilder;
    new-instance v14, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mStreamsProjectionMap:Ljava/util/Map;

    invoke-direct {v14, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1153
    .restart local v14       #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/reader/provider/ReaderStream;->isLabel(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_45d

    .line 1155
    const-string v6, "streams.id, subscription_categories.tag_id"

    .line 1156
    const-string v3, "subscription_count"

    const-string v4, "COUNT(subscription_categories.subscription_id) AS subscription_count"

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1159
    const-string v3, "LEFT JOIN subscription_categories ON ("

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1160
    const-string v3, "streams.account_name = subscription_categories.account_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1161
    const-string v3, " AND "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1162
    const-string v3, "streams.id = subscription_categories.tag_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1163
    const-string v3, ")"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1165
    :cond_45d
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1166
    invoke-virtual {v2, v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_43

    .line 1170
    .end local v14           #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v17           #streamId:Ljava/lang/String;
    .end local v19           #tables:Ljava/lang/StringBuilder;
    :pswitch_469
    const-string v3, "friends"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1171
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1172
    const-string v3, "contact_id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1173
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1174
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1175
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1178
    :pswitch_489
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Friends;->isFollowing(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_4bc

    .line 1179
    const-string v3, "stream"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1180
    const-string v3, " IS NOT NULL"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1181
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1182
    const-string v3, "has_shared_items"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1183
    const-string v3, " != 0"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1184
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1185
    const-string v3, "is_me"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1186
    const-string v3, " = 0"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1187
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1189
    :cond_4bc
    const-string v3, "friends.account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1190
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1191
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1192
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mFriendsTables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1193
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mFriendsProjectionMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_43

    .line 1196
    :pswitch_4db
    const-string v3, "items"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1197
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1198
    const-string v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1199
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1200
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1201
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1202
    const-string v3, "items"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1203
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1204
    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1205
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1206
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1207
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsTables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1208
    new-instance v14, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsProjectionMap:Ljava/util/Map;

    invoke-direct {v14, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1210
    .restart local v14       #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "position"

    const-string v4, "0"

    invoke-interface {v14, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211
    invoke-virtual {v2, v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_43

    .line 1216
    .end local v14           #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :pswitch_530
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemList(Landroid/net/Uri;)Lcom/google/android/apps/reader/content/ItemList;

    move-result-object v13

    .line 1217
    .local v13, itemList:Lcom/google/android/apps/reader/content/ItemList;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v17

    .line 1218
    .restart local v17       #streamId:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v10}, Lcom/google/android/apps/reader/content/ReaderProvider;->setUserIdIfMissing(Ljava/lang/String;Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v17

    .line 1219
    if-eqz v13, :cond_5dc

    .line 1220
    invoke-virtual {v13}, Lcom/google/android/apps/reader/content/ItemList;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v10

    .line 1221
    const-string v3, "items.account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1222
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1223
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1224
    const-string v3, "item_positions.account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1225
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1226
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1227
    const-string v3, "item_positions.item_list_id = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1228
    invoke-virtual {v13}, Lcom/google/android/apps/reader/content/ItemList;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1229
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1230
    const-string v3, "item_positions.item_id = items.id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1231
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "item_positions, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsTables:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1232
    const/16 v3, 0xc

    move/from16 v0, v21

    if-ne v0, v3, :cond_5be

    .line 1233
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSuggestProjectionMap(Lcom/google/android/accounts/Account;)Ljava/util/Map;

    move-result-object v14

    .line 1234
    .restart local v14       #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v2, v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1238
    .end local v14           #projectionMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_5a3
    if-eqz p5, :cond_5ac

    .line 1239
    const-string v3, "ReaderProvider"

    const-string v4, "Sort order ignored"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1241
    :cond_5ac
    sget-object v3, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_5c6

    invoke-virtual {v13}, Lcom/google/android/apps/reader/content/ItemList;->isNewestFirstRanking()Z

    move-result v3

    if-eqz v3, :cond_5c6

    .line 1243
    const-string p5, "crawl_time DESC "

    goto/16 :goto_43

    .line 1236
    :cond_5be
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsProjectionMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_5a3

    .line 1244
    :cond_5c6
    sget-object v3, Lcom/google/android/apps/reader/util/Experiment;->OFFLINE_MARKING_TAGS_AND_UNREAD:Lcom/google/android/apps/reader/util/Experiment;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/util/Experiment;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_5d8

    invoke-virtual {v13}, Lcom/google/android/apps/reader/content/ItemList;->isOldestFirstRanking()Z

    move-result v3

    if-eqz v3, :cond_5d8

    .line 1246
    const-string p5, "crawl_time"

    goto/16 :goto_43

    .line 1248
    :cond_5d8
    const-string p5, "item_positions.position"

    goto/16 :goto_43

    .line 1250
    :cond_5dc
    if-eqz v17, :cond_638

    const/4 v3, 0x7

    move/from16 v0, v21

    if-ne v0, v3, :cond_638

    .line 1251
    const-string v3, "items.account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1252
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1253
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1254
    const-string v3, "item_categories.account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1255
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1256
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1257
    const-string v3, "item_categories.item_id = items.id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1258
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1259
    const-string v3, "item_categories.stream_id = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1260
    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1261
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "item_categories, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsTables:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1262
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsProjectionMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_43

    .line 1266
    :cond_638
    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsTables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1268
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsProjectionMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_43

    .line 1273
    .end local v13           #itemList:Lcom/google/android/apps/reader/content/ItemList;
    .end local v17           #streamId:Ljava/lang/String;
    :pswitch_64d
    const-string v3, "item_links.account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1274
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1275
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Links;->getItemId(Landroid/net/Uri;)Ljava/lang/Long;

    move-result-object v12

    .line 1276
    .restart local v12       #itemId:Ljava/lang/Long;
    if-eqz v12, :cond_678

    .line 1277
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1278
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "item_links.item_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1280
    :cond_678
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Links;->getRel(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v16

    .line 1281
    .local v16, rel:Ljava/lang/String;
    if-eqz v16, :cond_68d

    .line 1282
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1283
    const-string v3, "item_links.rel = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1284
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1286
    :cond_68d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mLinksTables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_43

    .line 1289
    .end local v12           #itemId:Ljava/lang/Long;
    .end local v16           #rel:Ljava/lang/String;
    :pswitch_696
    const-string v3, "item_links.account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1290
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1291
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1292
    const-string v3, "item_links.href = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1293
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1294
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mLinksTables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_43

    .line 1298
    :pswitch_6ba
    const-string v3, "account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1299
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1300
    const-string v3, "user_info"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_43

    .line 1303
    :pswitch_6cb
    const-string v3, "stream_id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1304
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1305
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1306
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1307
    const-string v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1308
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1309
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1310
    const-string v3, "stream_details"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_43

    .line 1313
    :pswitch_6f7
    const-string v3, "overview"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1314
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1315
    const-string v3, "sid"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1316
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1317
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1318
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1321
    :pswitch_717
    const-string v3, "overview"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1322
    const-string v3, "."

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1323
    const-string v3, "account_name = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1324
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1325
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mOverviewTables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1326
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mOverviewProjectionMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1327
    const-string v11, "position ASC"

    .line 1328
    goto/16 :goto_43

    .line 1330
    :pswitch_73d
    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1331
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1332
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1333
    const-string v3, " AND "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1336
    :pswitch_753
    const-string v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1337
    const-string v3, " = "

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1338
    iget-object v3, v10, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1339
    const-string v3, "recommendations"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1340
    const-string v11, "position ASC"

    .line 1341
    goto/16 :goto_43

    .line 1346
    :cond_76b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    goto/16 :goto_49

    .line 941
    :pswitch_data_786
    .packed-switch 0x1
        :pswitch_b0
        :pswitch_90
        :pswitch_271
        :pswitch_251
        :pswitch_74
        :pswitch_5e
        :pswitch_530
        :pswitch_4db
        :pswitch_6ba
        :pswitch_14
        :pswitch_64d
        :pswitch_530
        :pswitch_489
        :pswitch_469
        :pswitch_14
        :pswitch_14
        :pswitch_2f
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_416
        :pswitch_14
        :pswitch_717
        :pswitch_6f7
        :pswitch_753
        :pswitch_73d
        :pswitch_6cb
        :pswitch_696
    .end packed-switch
.end method

.method protected buildUnionQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    .registers 29
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "extras"

    .prologue
    .line 1354
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v9

    .line 1355
    .local v9, account:Lcom/google/android/accounts/Account;
    new-instance v20, Ljava/util/ArrayList;

    const/16 v2, 0x8

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1356
    .local v20, uris:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_1cc

    .line 1412
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not a UNION query"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1358
    :pswitch_2f
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v18

    .line 1359
    .local v18, streamId:Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-static {v9, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1415
    .end local v18           #streamId:Ljava/lang/String;
    :cond_3e
    :goto_3e
    const-string v2, "sort_key_manual"

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_52

    const-string v2, "sort_key_alpha"

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_86

    .line 1417
    :cond_52
    const-string v2, "sort_key_manual"

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_71

    .line 1420
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sort_key_alpha"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 1424
    :cond_71
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", label COLLATE NOCASE ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 1426
    :cond_86
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_173

    .line 1428
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/reader/content/ReaderProvider;->buildDatabaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 1446
    :goto_a6
    return-object v2

    .line 1363
    :pswitch_a7
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->getFilter(Landroid/net/Uri;)Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;

    move-result-object v17

    .line 1364
    .local v17, streamFilter:Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;
    sget-object v2, Lcom/google/android/apps/reader/content/ReaderProvider$1;->$SwitchMap$com$google$android$apps$reader$provider$ReaderContract$Streams$Filter:[I

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1d4

    .line 1408
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected filter: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1366
    :pswitch_d1
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1367
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1368
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Friends;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1371
    :pswitch_ee
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v21

    .line 1372
    .local v21, userId:Ljava/lang/String;
    if-eqz v21, :cond_114

    .line 1373
    sget-object v10, Lcom/google/android/apps/reader/content/ReaderProvider;->ACCOUNT_STREAMS:[Ljava/lang/String;

    .local v10, arr$:[Ljava/lang/String;
    array-length v14, v10

    .local v14, len$:I
    const/4 v13, 0x0

    .local v13, i$:I
    :goto_fa
    if-ge v13, v14, :cond_3e

    aget-object v18, v10, v13

    .line 1374
    .restart local v18       #streamId:Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1375
    move-object/from16 v0, v18

    invoke-static {v9, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1373
    add-int/lit8 v13, v13, 0x1

    goto :goto_fa

    .line 1378
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v13           #i$:I
    .end local v14           #len$:I
    .end local v18           #streamId:Ljava/lang/String;
    :cond_114
    const-string v2, "ReaderProvider"

    const-string v3, "Unable to query streams because User ID is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1384
    const-string p3, "0"

    .line 1385
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1389
    .end local v21           #userId:Ljava/lang/String;
    :pswitch_128
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1390
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->untaggedUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1393
    :pswitch_13c
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1396
    :pswitch_147
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1399
    :pswitch_152
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->withSubscriptions(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1402
    :pswitch_15d
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->withoutSubscriptions(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1405
    :pswitch_168
    invoke-static {v9}, Lcom/google/android/apps/reader/provider/ReaderContract$Friends;->followingUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3e

    .line 1432
    .end local v17           #streamFilter:Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;
    :cond_173
    if-eqz p4, :cond_192

    .line 1434
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selection arguments not supported by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1435
    .local v16, message:Ljava/lang/String;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    move-object/from16 v0, v16

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1438
    .end local v16           #message:Ljava/lang/String;
    :cond_192
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 1439
    .local v19, subQueries:[Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, i:I
    :goto_19b
    move-object/from16 v0, v19

    array-length v2, v0

    if-ge v12, v2, :cond_1bb

    .line 1440
    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v8, p6

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/reader/content/ReaderProvider;->buildDatabaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v19, v12

    .line 1439
    add-int/lit8 v12, v12, 0x1

    goto :goto_19b

    .line 1444
    :cond_1bb
    new-instance v11, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v11}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1445
    .local v11, builder:Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v15, 0x0

    .line 1446
    .local v15, limit:Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, p5

    invoke-virtual {v11, v0, v1, v15}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildUnionQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a6

    .line 1356
    nop

    :pswitch_data_1cc
    .packed-switch 0x18
        :pswitch_a7
        :pswitch_2f
    .end packed-switch

    .line 1364
    :pswitch_data_1d4
    .packed-switch 0x1
        :pswitch_d1
        :pswitch_ee
        :pswitch_128
        :pswitch_13c
        :pswitch_147
        :pswitch_152
        :pswitch_15d
        :pswitch_168
    .end packed-switch
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .registers 10
    .parameter "uri"
    .parameter "values"

    .prologue
    const/4 v4, 0x0

    .line 2520
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2521
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_38

    .line 2540
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v4

    :cond_10
    :goto_10
    return v4

    .line 2523
    :pswitch_11
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 2524
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->getTagId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 2525
    .local v2, tagId:Ljava/lang/String;
    if-eqz v2, :cond_2d

    .line 2526
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    .line 2527
    .local v3, userId:Ljava/lang/String;
    if-nez v3, :cond_29

    .line 2528
    const-string v5, "ReaderProvider"

    const-string v6, "Failed to get user ID"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_10

    .line 2531
    :cond_29
    invoke-static {v2, v3}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2533
    .end local v3           #userId:Ljava/lang/String;
    :cond_2d
    invoke-static {v1, v0, p2, v2}, Lcom/google/android/apps/reader/content/ReaderActions;->subscribe(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;[Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 2534
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2535
    array-length v4, p2

    goto :goto_10

    .line 2521
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_11
    .end packed-switch
.end method

.method protected final createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;
    .registers 4
    .parameter "handler"
    .parameter "account"

    .prologue
    .line 2045
    const-string v0, "reader"

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;Ljava/lang/String;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v0

    return-object v0
.end method

.method protected createAuthenticatedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;Ljava/lang/String;)Lcom/google/android/apps/reader/net/HttpContentHandler;
    .registers 10
    .parameter "handler"
    .parameter "account"
    .parameter "authTokenType"

    .prologue
    .line 2032
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 2033
    .local v5, notifyAuthFailure:Z
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2034
    .local v2, context:Landroid/content/Context;
    new-instance v0, Lcom/google/android/apps/reader/net/ReaderContentHandler;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/reader/net/ReaderContentHandler;-><init>(Ljava/net/ContentHandler;Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;Z)V

    return-object v0
.end method

.method protected createFileCache()Lcom/google/android/apps/reader/content/ReaderFileCache;
    .registers 5

    .prologue
    .line 682
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 683
    .local v1, context:Landroid/content/Context;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "filecache"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 684
    .local v0, cacheDir:Ljava/io/File;
    new-instance v2, Lcom/google/android/apps/reader/content/ReaderFileCache;

    iget v3, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabaseVersion:I

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/reader/content/ReaderFileCache;-><init>(Ljava/io/File;I)V

    return-object v2
.end method

.method protected createFriendsProjectionMap()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 438
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 441
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "friends._id AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    const-string v1, "account_name"

    const-string v2, "friends.account_name AS account_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    const-string v1, "account_type"

    const-string v2, "\'com.google\' AS account_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    const-string v1, "contact_id"

    const-string v2, "contact_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    const-string v1, "display_name"

    const-string v2, "display_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    const-string v1, "photo_uri"

    const-string v2, "photo_uri"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    const-string v1, "stream"

    const-string v2, "stream"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    const-string v1, "email_address"

    const-string v2, "email_address"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    const-string v1, "is_me"

    const-string v2, "is_me"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    const-string v1, "is_hidden"

    const-string v2, "is_hidden"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    const-string v1, "is_new"

    const-string v2, "is_new"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    const-string v1, "uses_reader"

    const-string v2, "uses_reader"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    const-string v1, "is_blocked"

    const-string v2, "is_blocked"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    const-string v1, "has_profile"

    const-string v2, "has_profile"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    const-string v1, "is_ignored"

    const-string v2, "is_ignored"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    const-string v1, "is_new_follower"

    const-string v2, "is_new_follower"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    const-string v1, "is_anonymous"

    const-string v2, "is_anonymous"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    const-string v1, "has_shared_items"

    const-string v2, "has_shared_items"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    const-string v1, "max_unread_count"

    const-string v2, "max_unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    const-string v1, "newest_item_timestamp"

    const-string v2, "newest_item_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    const-string v1, "photo_url"

    const-string v2, "photo_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    const-string v1, "id"

    const-string v2, "stream AS id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    const-string v1, "label"

    const-string v2, "display_name AS label"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    const-string v1, "sortid"

    const-string v2, "NULL AS sortid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    const-string v1, "html_url"

    const-string v2, "NULL AS html_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    const-string v1, "subscription_count"

    const-string v2, "NULL AS subscription_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    return-object v0
.end method

.method protected createFriendsTables()Ljava/lang/String;
    .registers 2

    .prologue
    .line 637
    const-string v0, "friends LEFT JOIN unread_counts ON (friends.account_name = unread_counts.account_name AND friends.stream = unread_counts.stream_id)"

    return-object v0
.end method

.method protected createItemLinksTables()Ljava/lang/String;
    .registers 3

    .prologue
    .line 657
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "item_links"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 658
    .local v0, builder:Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected createItemsProjectionMap()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 482
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createFriendsProjectionMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 483
    const-string v1, "_id"

    const-string v2, "items._id AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    const-string v1, "account_name"

    const-string v2, "items.account_name AS account_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    const-string v1, "account_type"

    const-string v2, "\'com.google\' AS account_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    const-string v1, "id"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    const-string v1, "external_id"

    const-string v2, "external_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    const-string v1, "crawl_time"

    const-string v2, "crawl_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    const-string v1, "title_plaintext"

    const-string v2, "title_plaintext"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    const-string v1, "author"

    const-string v2, "author"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    const-string v1, "alternate_href"

    const-string v2, "alternate_href"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    const-string v1, "source_title"

    const-string v2, "source_title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    const-string v1, "source_title_plaintext"

    const-string v2, "source_title_plaintext"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    const-string v1, "source_alternate_href"

    const-string v2, "source_alternate_href"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    const-string v1, "source_stream_id"

    const-string v2, "source_stream_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    const-string v1, "published"

    const-string v2, "published"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    const-string v1, "updated"

    const-string v2, "updated"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    const-string v1, "locked"

    const-string v2, "locked"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    const-string v1, "read"

    const-string v2, "read"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    const-string v1, "starred"

    const-string v2, "starred"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    const-string v1, "shared"

    const-string v2, "shared"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    const-string v1, "source_link"

    const-string v2, "source_link"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    const-string v1, "source_post"

    const-string v2, "source_post"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    const-string v1, "liked"

    const-string v2, "liked"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    const-string v1, "like_count"

    const-string v2, "like_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    const-string v1, "broadcaster"

    const-string v2, "broadcaster"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    const-string v1, "position"

    const-string v2, "position"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    const-string v1, "annotation"

    const-string v2, "annotation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    return-object v0
.end method

.method protected createItemsTables()Ljava/lang/String;
    .registers 3

    .prologue
    .line 643
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "items LEFT JOIN friends ON ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 645
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v1, "items.account_name = friends.account_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 646
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 647
    const-string v1, "items.broadcaster = friends.stream"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 650
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected createOverviewProjectionMap()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 346
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "overview._id AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    const-string v1, "account_name"

    const-string v2, "overview.account_name AS account_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    const-string v1, "account_type"

    const-string v2, "overview.account_type AS account_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    const-string v1, "sid"

    const-string v2, "sid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    const-string v1, "max_unread_count"

    const-string v2, "max_unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    const-string v1, "newest_item_timestamp"

    const-string v2, "newest_item_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    const-string v1, "summary"

    const-string v2, "summary"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const-string v1, "image_url"

    const-string v2, "image_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    const-string v1, "position"

    const-string v2, "position"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    return-object v0
.end method

.method protected createOverviewTables()Ljava/lang/String;
    .registers 2

    .prologue
    .line 586
    const-string v0, "overview LEFT JOIN subscriptions ON (overview.account_name = subscriptions.account_name AND overview.sid = subscriptions.id) LEFT JOIN unread_counts ON (overview.account_name = unread_counts.account_name AND overview.sid = unread_counts.stream_id)"

    return-object v0
.end method

.method protected createSQLiteOpenHelper()Landroid/database/sqlite/SQLiteOpenHelper;
    .registers 6

    .prologue
    .line 670
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 671
    .local v0, context:Landroid/content/Context;
    new-instance v1, Lcom/google/android/apps/reader/content/ReaderDatabase;

    iget-object v2, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabaseName:Ljava/lang/String;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabaseVersion:I

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/apps/reader/content/ReaderDatabase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-object v1
.end method

.method protected createStreamsProjectionMap()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 401
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "streams._id AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    const-string v1, "account_name"

    const-string v2, "streams.account_name AS account_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    const-string v1, "account_type"

    const-string v2, "\'com.google\' AS account_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    const-string v1, "id"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    const-string v1, "label"

    const-string v2, "label"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    const-string v1, "sortid"

    const-string v2, "NULL AS sortid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    const-string v1, "html_url"

    const-string v2, "NULL AS html_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    const-string v1, "subscription_count"

    const-string v2, "NULL AS subscription_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    const-string v1, "max_unread_count"

    const-string v2, "max_unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    const-string v1, "newest_item_timestamp"

    const-string v2, "newest_item_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    return-object v0
.end method

.method protected createSubscriptionsProjectionMap()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 315
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "subscriptions._id AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    const-string v1, "account_name"

    const-string v2, "subscriptions.account_name AS account_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    const-string v1, "account_type"

    const-string v2, "\'com.google\' AS account_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    const-string v1, "id"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    const-string v1, "sortid"

    const-string v2, "sortid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    const-string v1, "firstitemmsec"

    const-string v2, "firstitemmsec"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    const-string v1, "html_url"

    const-string v2, "html_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    const-string v1, "max_unread_count"

    const-string v2, "max_unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    const-string v1, "newest_item_timestamp"

    const-string v2, "newest_item_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    const-string v1, "id"

    const-string v2, "id AS id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    const-string v1, "label"

    const-string v2, "title AS label"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    const-string v1, "sortid"

    const-string v2, "sortid AS sortid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    const-string v1, "html_url"

    const-string v2, "html_url AS html_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    const-string v1, "subscription_count"

    const-string v2, "NULL AS subscription_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    return-object v0
.end method

.method protected createSubscriptionsTables()Ljava/lang/String;
    .registers 2

    .prologue
    .line 564
    const-string v0, "subscriptions LEFT JOIN unread_counts ON (subscriptions.account_name = unread_counts.account_name AND subscriptions.id = unread_counts.stream_id)"

    return-object v0
.end method

.method protected createSuggestProjectionMap(Lcom/google/android/accounts/Account;)Ljava/util/Map;
    .registers 14
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/accounts/Account;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 517
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 518
    .local v2, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v9, "_id"

    const-string v10, "items._id AS _id"

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    const-string v9, "suggest_text_1"

    const-string v10, "title_plaintext AS suggest_text_1"

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    const-string v9, "suggest_text_2"

    const-string v10, "source_title_plaintext AS suggest_text_2"

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 533
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 534
    .local v6, res:Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 535
    .local v3, packageName:Ljava/lang/String;
    const-string v9, "subscription"

    const-string v10, "drawable"

    invoke-virtual {v6, v9, v10, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 536
    .local v7, resId:I
    const-string v9, "suggest_icon_1"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " AS "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "suggest_icon_1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    const-wide/16 v9, 0x0

    invoke-static {p1, v9, v10}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->itemUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 540
    .local v8, templateUri:Ljava/lang/String;
    const-string v9, "/0?"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 541
    .local v1, index:I
    if-gez v1, :cond_66

    .line 542
    new-instance v9, Ljava/lang/RuntimeException;

    const-string v10, "Item ID not found"

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 544
    :cond_66
    const/4 v9, 0x0

    add-int/lit8 v10, v1, 0x1

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 545
    .local v4, part1:Ljava/lang/String;
    add-int/lit8 v9, v1, 0x2

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 546
    .local v5, part2:Ljava/lang/String;
    const-string v9, "suggest_intent_data"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " || "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " || "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " AS "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "suggest_intent_data"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    const-string v9, "suggest_shortcut_id"

    const-string v10, "\'_-1\' AS suggest_shortcut_id"

    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    return-object v2
.end method

.method protected createSyncCallable(Lcom/google/android/accounts/Account;I)Ljava/util/concurrent/Callable;
    .registers 12
    .parameter "account"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/accounts/Account;",
            "I)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2552
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2553
    .local v1, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2554
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    move-object v4, p0

    .line 2555
    .local v4, loader:Lcom/google/android/apps/reader/content/ContentLoader;
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createTokenContentHandler(Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v5

    .line 2556
    .local v5, tokenHandler:Ljava/net/ContentHandler;
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createPreferencesContentHandler(Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v6

    .line 2557
    .local v6, preferencesHandler:Ljava/net/ContentHandler;
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createStreamPreferencesContentHandler(Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v7

    .line 2558
    .local v7, streamPreferencesHandler:Ljava/net/ContentHandler;
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createHttpPostContentHandler(Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v8

    .line 2559
    .local v8, httpPostHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;
    new-instance v0, Lcom/google/android/apps/reader/content/ReaderSync;

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/reader/content/ReaderSync;-><init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Lcom/google/android/apps/reader/content/ContentLoader;Ljava/net/ContentHandler;Ljava/net/ContentHandler;Ljava/net/ContentHandler;Lcom/google/android/apps/reader/net/HttpContentHandler;)V

    .line 2561
    .local v0, sync:Lcom/google/android/apps/reader/content/ReaderSync;
    invoke-virtual {v0, p2}, Lcom/google/android/apps/reader/content/ReaderSync;->setMode(I)V

    .line 2562
    return-object v0
.end method

.method protected createSynchronizedContentHandler(Ljava/net/ContentHandler;Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;
    .registers 5
    .parameter "handler"
    .parameter "account"

    .prologue
    .line 2014
    const/4 v1, 0x2

    invoke-virtual {p0, p2, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSyncCallable(Lcom/google/android/accounts/Account;I)Ljava/util/concurrent/Callable;

    move-result-object v0

    .line 2015
    .local v0, precondition:Ljava/util/concurrent/Callable;,"Ljava/util/concurrent/Callable<Ljava/lang/Void;>;"
    new-instance v1, Lcom/google/android/apps/reader/content/PreconditionContentHandler;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/reader/content/PreconditionContentHandler;-><init>(Ljava/net/ContentHandler;Ljava/util/concurrent/Callable;)V

    return-object v1
.end method

.method protected createTagsProjectionMap()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 368
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "tags._id AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    const-string v1, "account_name"

    const-string v2, "tags.account_name AS account_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    const-string v1, "account_type"

    const-string v2, "\'com.google\' AS account_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    const-string v1, "id"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    const-string v1, "label"

    const-string v2, "label"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    const-string v1, "sortid"

    const-string v2, "sortid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    const-string v1, "max_unread_count"

    const-string v2, "max_unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    const-string v1, "newest_item_timestamp"

    const-string v2, "newest_item_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    const-string v1, "id"

    const-string v2, "id AS id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    const-string v1, "label"

    const-string v2, "label AS label"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    const-string v1, "sortid"

    const-string v2, "sortid AS sortid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    const-string v1, "html_url"

    const-string v2, "NULL AS html_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    const-string v1, "subscription_count"

    const-string v2, "subscription_count AS subscription_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    return-object v0
.end method

.method protected createTagsTables()Ljava/lang/String;
    .registers 2

    .prologue
    .line 575
    const-string v0, "tags LEFT JOIN unread_counts ON (tags.account_name = unread_counts.account_name AND tags.id = unread_counts.stream_id)"

    return-object v0
.end method

.method protected createUnreadCountsProjectionMap()Ljava/util/Map;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 420
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 421
    .local v0, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    const-string v1, "account_name"

    const-string v2, "unread_counts.account_name AS account_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    const-string v1, "account_type"

    const-string v2, "\'com.google\' AS account_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    const-string v1, "stream_id"

    const-string v2, "stream_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    const-string v1, "max_unread_count"

    const-string v2, "max_unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    const-string v1, "newest_item_timestamp"

    const-string v2, "newest_item_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    const-string v1, "last_read_item_timestamp"

    const-string v2, "last_read_item_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    return-object v0
.end method

.method protected createUriMatcher(Ljava/lang/String;)Landroid/content/UriMatcher;
    .registers 5
    .parameter "authority"

    .prologue
    .line 733
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 734
    .local v0, matcher:Landroid/content/UriMatcher;
    const-string v1, "accounts"

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 735
    const-string v1, "sync"

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 736
    const-string v1, "cleanup"

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 737
    const-string v1, "preferences"

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 738
    const-string v1, "preferences/*"

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 739
    const-string v1, "stream-preferences"

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 740
    const-string v1, "stream-preferences/*"

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 741
    const-string v1, "subscriptions"

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 742
    const-string v1, "subscriptions/*"

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 743
    const-string v1, "recommendations"

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 744
    const-string v1, "recommendations/*"

    const/16 v2, 0x1e

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 745
    const-string v1, "tags"

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 746
    const-string v1, "tags/*"

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 747
    const-string v1, "streams"

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 748
    const-string v1, "streams/*"

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 749
    const-string v1, "friends"

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 750
    const-string v1, "friends/#"

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 751
    const-string v1, "friend-photos/#"

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 752
    const-string v1, "unread-counts"

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 753
    const-string v1, "unread-counts/*"

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 754
    const-string v1, "items"

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 755
    const-string v1, "items/*"

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 756
    const-string v1, "item_html/*"

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 757
    const-string v1, "links"

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 758
    const-string v1, "links/*"

    const/16 v2, 0x20

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 759
    const-string v1, "user-info"

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 760
    const-string v1, "overview"

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 761
    const-string v1, "overview/*"

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 762
    const-string v1, "stream-details/*"

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 763
    const-string v1, "favicons/*"

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 764
    const-string v1, "suggest_items/search_suggest_query"

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 765
    const-string v1, "suggest_items/search_suggest_query/*"

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 766
    return-object v0
.end method

.method public databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;
    .registers 16
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"
    .parameter "extras"

    .prologue
    .line 1460
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 1461
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v7

    packed-switch v7, :pswitch_data_4a

    .line 1468
    invoke-virtual/range {p0 .. p6}, Lcom/google/android/apps/reader/content/ReaderProvider;->buildDatabaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    .line 1472
    .local v6, sql:Ljava/lang/String;
    :goto_f
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1473
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v7, "ReaderProvider"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2e

    .line 1474
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Performing query: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1476
    :cond_2e
    invoke-virtual {v4, v6, p4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1477
    .local v3, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_43

    .line 1478
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1479
    .local v1, context:Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1480
    .local v2, cr:Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->notifyUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v5

    .line 1481
    .local v5, notificationUri:Landroid/net/Uri;
    invoke-interface {v3, v2, v5}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1483
    .end local v1           #context:Landroid/content/Context;
    .end local v2           #cr:Landroid/content/ContentResolver;
    .end local v5           #notificationUri:Landroid/net/Uri;
    :cond_43
    return-object v3

    .line 1464
    .end local v3           #cursor:Landroid/database/Cursor;
    .end local v4           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v6           #sql:Ljava/lang/String;
    :pswitch_44
    invoke-virtual/range {p0 .. p6}, Lcom/google/android/apps/reader/content/ReaderProvider;->buildUnionQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    .line 1466
    .restart local v6       #sql:Ljava/lang/String;
    goto :goto_f

    .line 1461
    nop

    :pswitch_data_4a
    .packed-switch 0x18
        :pswitch_44
        :pswitch_44
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 16
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2740
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2741
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v10

    sparse-switch v10, :sswitch_data_ae

    move v8, v9

    .line 2808
    :goto_e
    return v8

    .line 2743
    :sswitch_f
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 2744
    .local v0, account:Lcom/google/android/accounts/Account;
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    .line 2745
    .local v5, subscriptionId:Ljava/lang/String;
    invoke-static {v1, v0, v5}, Lcom/google/android/apps/reader/content/ReaderActions;->unsubscribe(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_21

    .line 2746
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_e

    :cond_21
    move v8, v9

    .line 2749
    goto :goto_e

    .line 2753
    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v5           #subscriptionId:Ljava/lang/String;
    :sswitch_23
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 2754
    .restart local v0       #account:Lcom/google/android/accounts/Account;
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    .line 2755
    .local v6, tagId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->getItemId(Landroid/net/Uri;)Ljava/lang/Long;

    move-result-object v3

    .line 2756
    .local v3, itemId:Ljava/lang/Long;
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->getSubscriptionId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 2757
    .restart local v5       #subscriptionId:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4c

    .line 2758
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v7

    .line 2759
    .local v7, userId:Ljava/lang/String;
    if-nez v7, :cond_48

    .line 2760
    const-string v8, "ReaderProvider"

    const-string v10, "Failed to get user ID"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 2761
    goto :goto_e

    .line 2763
    :cond_48
    invoke-static {v6, v7}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2765
    .end local v7           #userId:Ljava/lang/String;
    :cond_4c
    if-eqz v3, :cond_6d

    .line 2766
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-direct {p0, v0, v10, v11}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetExternalId(Lcom/google/android/accounts/Account;J)Ljava/lang/String;

    move-result-object v2

    .line 2767
    .local v2, externalId:Ljava/lang/String;
    if-nez v2, :cond_61

    .line 2768
    const-string v8, "ReaderProvider"

    const-string v10, "Cannot update without external ID"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 2769
    goto :goto_e

    .line 2771
    :cond_61
    invoke-static {v1, v0, v3, v2, v6}, Lcom/google/android/apps/reader/content/ReaderActions;->removeItemTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6b

    .line 2772
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_e

    :cond_6b
    move v8, v9

    .line 2775
    goto :goto_e

    .line 2777
    .end local v2           #externalId:Ljava/lang/String;
    :cond_6d
    if-eqz v5, :cond_7b

    .line 2778
    invoke-static {v1, v0, v5, v6}, Lcom/google/android/apps/reader/content/ReaderActions;->removeSubscriptionTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_79

    .line 2779
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_e

    :cond_79
    move v8, v9

    .line 2782
    goto :goto_e

    .line 2785
    :cond_7b
    invoke-static {v1, v0, v6}, Lcom/google/android/apps/reader/content/ReaderActions;->disableTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_85

    .line 2786
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    goto :goto_e

    :cond_85
    move v8, v9

    .line 2789
    goto :goto_e

    .line 2794
    .end local v0           #account:Lcom/google/android/accounts/Account;
    .end local v3           #itemId:Ljava/lang/Long;
    .end local v5           #subscriptionId:Ljava/lang/String;
    .end local v6           #tagId:Ljava/lang/String;
    :sswitch_87
    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 2795
    .restart local v0       #account:Lcom/google/android/accounts/Account;
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v3

    .line 2796
    .local v3, itemId:J
    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetExternalId(Lcom/google/android/accounts/Account;J)Ljava/lang/String;

    move-result-object v2

    .line 2797
    .restart local v2       #externalId:Ljava/lang/String;
    if-nez v2, :cond_9f

    .line 2798
    const-string v8, "ReaderProvider"

    const-string v10, "Cannot delete without external ID"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 2799
    goto/16 :goto_e

    .line 2801
    :cond_9f
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/reader/content/ReaderActions;->deleteItem(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_aa

    .line 2802
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    goto/16 :goto_e

    :cond_aa
    move v8, v9

    .line 2805
    goto/16 :goto_e

    .line 2741
    nop

    :sswitch_data_ae
    .sparse-switch
        0x2 -> :sswitch_f
        0x4 -> :sswitch_23
        0x8 -> :sswitch_87
    .end sparse-switch
.end method

.method protected final getFileCache()Lcom/google/android/apps/reader/content/ReaderFileCache;
    .registers 2

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    if-nez v0, :cond_a

    .line 718
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 720
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    return-object v0
.end method

.method protected final getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    if-nez v0, :cond_a

    .line 694
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 696
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter "uri"

    .prologue
    .line 779
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_4c

    .line 827
    :pswitch_7
    const/4 v0, 0x0

    :goto_8
    return-object v0

    .line 782
    :pswitch_9
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Preferences;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 785
    :pswitch_c
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Preferences;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 787
    :pswitch_f
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 789
    :pswitch_12
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 791
    :pswitch_15
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Recommendations;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 793
    :pswitch_18
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Recommendations;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 795
    :pswitch_1b
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 797
    :pswitch_1e
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 799
    :pswitch_21
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 801
    :pswitch_24
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 803
    :pswitch_27
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 805
    :pswitch_2a
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 807
    :pswitch_2d
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Links;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 809
    :pswitch_30
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Links;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 811
    :pswitch_33
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$UserInfo;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 813
    :pswitch_36
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Friends;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 815
    :pswitch_39
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Friends;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 817
    :pswitch_3c
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Overview;->CONTENT_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 819
    :pswitch_3f
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Overview;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 821
    :pswitch_42
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$StreamDetails;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    goto :goto_8

    .line 823
    :pswitch_45
    const-string v0, "text/html"

    goto :goto_8

    .line 825
    :pswitch_48
    const-string v0, "image/png"

    goto :goto_8

    .line 779
    nop

    :pswitch_data_4c
    .packed-switch 0x1
        :pswitch_f
        :pswitch_12
        :pswitch_1b
        :pswitch_1e
        :pswitch_7
        :pswitch_7
        :pswitch_27
        :pswitch_2a
        :pswitch_33
        :pswitch_7
        :pswitch_2d
        :pswitch_7
        :pswitch_36
        :pswitch_39
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_45
        :pswitch_9
        :pswitch_9
        :pswitch_c
        :pswitch_c
        :pswitch_21
        :pswitch_24
        :pswitch_48
        :pswitch_3c
        :pswitch_3f
        :pswitch_15
        :pswitch_18
        :pswitch_42
        :pswitch_30
    .end packed-switch
.end method

.method protected final getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 705
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    if-nez v0, :cond_a

    .line 706
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 708
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 42
    .parameter "uri"
    .parameter "values"

    .prologue
    .line 2334
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    .line 2335
    .local v10, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v11

    sparse-switch v11, :sswitch_data_2e2

    .line 2514
    const/16 p1, 0x0

    .end local p1
    :goto_d
    return-object p1

    .line 2337
    .restart local p1
    :sswitch_e
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v4

    .line 2338
    .local v4, account:Lcom/google/android/accounts/Account;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->getQuickAddUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 2339
    .local v5, url:Ljava/lang/String;
    if-eqz v5, :cond_43

    .line 2340
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->createTokenContentHandler(Lcom/google/android/accounts/Account;)Ljava/net/ContentHandler;

    move-result-object v6

    .line 2341
    .local v6, tokenHandler:Ljava/net/ContentHandler;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->createQuickAddContentHandler(Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v7

    .line 2342
    .local v7, quickAddHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->createHttpPostContentHandler(Lcom/google/android/accounts/Account;)Lcom/google/android/apps/reader/net/HttpContentHandler;

    move-result-object v8

    .line 2343
    .local v8, postHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;
    move-object/from16 v9, p0

    .line 2344
    .local v9, loader:Lcom/google/android/apps/reader/content/ContentLoader;
    new-instance v3, Lcom/google/android/apps/reader/content/QuickAdd;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/reader/content/QuickAdd;-><init>(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/net/ContentHandler;Lcom/google/android/apps/reader/net/HttpContentHandler;Lcom/google/android/apps/reader/net/HttpContentHandler;Lcom/google/android/apps/reader/content/ContentLoader;)V

    .line 2347
    .local v3, quickAdd:Lcom/google/android/apps/reader/content/QuickAdd;
    :try_start_31
    invoke-virtual {v3}, Lcom/google/android/apps/reader/content/QuickAdd;->run()Landroid/net/Uri;
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_34} :catch_36

    move-result-object p1

    goto :goto_d

    .line 2348
    :catch_36
    move-exception v23

    .line 2349
    .local v23, e:Ljava/io/IOException;
    const-string v11, "ReaderProvider"

    const-string v13, "quick add failed"

    move-object/from16 v0, v23

    invoke-static {v11, v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2350
    const/16 p1, 0x0

    goto :goto_d

    .line 2353
    .end local v3           #quickAdd:Lcom/google/android/apps/reader/content/QuickAdd;
    .end local v6           #tokenHandler:Ljava/net/ContentHandler;
    .end local v7           #quickAddHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;
    .end local v8           #postHandler:Lcom/google/android/apps/reader/net/HttpContentHandler;
    .end local v9           #loader:Lcom/google/android/apps/reader/content/ContentLoader;
    .end local v23           #e:Ljava/io/IOException;
    :cond_43
    const-string v11, "id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 2354
    .local v32, streamId:Ljava/lang/String;
    const-string v11, "title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2355
    .local v14, title:Ljava/lang/String;
    if-nez v32, :cond_5f

    .line 2356
    const-string v11, "ReaderProvider"

    const-string v13, "Stream ID was not specified"

    invoke-static {v11, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2357
    const/16 p1, 0x0

    goto :goto_d

    .line 2359
    :cond_5f
    move-object/from16 v0, v32

    invoke-static {v10, v4, v0, v14}, Lcom/google/android/apps/reader/content/ReaderActions;->subscribe(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_79

    .line 2360
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2361
    const-string v11, "id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 2362
    .local v33, subscriptionId:Ljava/lang/String;
    move-object/from16 v0, v33

    invoke-static {v4, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_d

    .line 2364
    .end local v33           #subscriptionId:Ljava/lang/String;
    :cond_79
    const/16 p1, 0x0

    goto :goto_d

    .line 2369
    .end local v4           #account:Lcom/google/android/accounts/Account;
    .end local v5           #url:Ljava/lang/String;
    .end local v14           #title:Ljava/lang/String;
    .end local v32           #streamId:Ljava/lang/String;
    :sswitch_7c
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v4

    .line 2370
    .restart local v4       #account:Lcom/google/android/accounts/Account;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->getItemId(Landroid/net/Uri;)Ljava/lang/Long;

    move-result-object v27

    .line 2371
    .local v27, itemId:Ljava/lang/Long;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->getSubscriptionId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v33

    .line 2372
    .restart local v33       #subscriptionId:Ljava/lang/String;
    const-string v11, "id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 2373
    .local v35, tagId:Ljava/lang/String;
    const-string v11, "label"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 2374
    .local v29, label:Ljava/lang/String;
    if-nez v35, :cond_a0

    if-eqz v29, :cond_a0

    .line 2375
    invoke-static/range {v29 .. v29}, Lcom/google/android/apps/reader/provider/ReaderStream;->createTagId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 2377
    :cond_a0
    if-nez v35, :cond_ad

    .line 2378
    const-string v11, "ReaderProvider"

    const-string v13, "Must specify either a tag ID or a label"

    invoke-static {v11, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2379
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2381
    :cond_ad
    invoke-static/range {v35 .. v35}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_cc

    .line 2382
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v12

    .line 2383
    .local v12, userId:Ljava/lang/String;
    if-nez v12, :cond_c6

    .line 2384
    const-string v11, "ReaderProvider"

    const-string v13, "Failed to get user ID"

    invoke-static {v11, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2385
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2387
    :cond_c6
    move-object/from16 v0, v35

    invoke-static {v0, v12}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 2389
    .end local v12           #userId:Ljava/lang/String;
    :cond_cc
    if-eqz v27, :cond_102

    .line 2390
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Long;->longValue()J

    move-result-wide v37

    move-object/from16 v0, p0

    move-wide/from16 v1, v37

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetExternalId(Lcom/google/android/accounts/Account;J)Ljava/lang/String;

    move-result-object v25

    .line 2391
    .local v25, externalId:Ljava/lang/String;
    if-nez v25, :cond_e7

    .line 2392
    const-string v11, "ReaderProvider"

    const-string v13, "Cannot update without external ID"

    invoke-static {v11, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2393
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2395
    :cond_e7
    move-object/from16 v0, v27

    move-object/from16 v1, v25

    move-object/from16 v2, v35

    invoke-static {v10, v4, v0, v1, v2}, Lcom/google/android/apps/reader/content/ReaderActions;->addItemTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_fe

    .line 2396
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2397
    move-object/from16 v0, v35

    invoke-static {v4, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_d

    .line 2399
    :cond_fe
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2401
    .end local v25           #externalId:Ljava/lang/String;
    :cond_102
    if-eqz v33, :cond_11d

    .line 2402
    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-static {v10, v4, v0, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->addSubscriptionTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_119

    .line 2403
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2404
    move-object/from16 v0, v35

    invoke-static {v4, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_d

    .line 2406
    :cond_119
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2409
    :cond_11d
    move-object/from16 v0, v35

    invoke-static {v10, v4, v0}, Lcom/google/android/apps/reader/content/ReaderActions;->createTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_130

    .line 2410
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2411
    move-object/from16 v0, v35

    invoke-static {v4, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Tags;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_d

    .line 2413
    :cond_130
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2418
    .end local v4           #account:Lcom/google/android/accounts/Account;
    .end local v27           #itemId:Ljava/lang/Long;
    .end local v29           #label:Ljava/lang/String;
    .end local v33           #subscriptionId:Ljava/lang/String;
    .end local v35           #tagId:Ljava/lang/String;
    :sswitch_134
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v4

    .line 2419
    .restart local v4       #account:Lcom/google/android/accounts/Account;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v12

    .line 2420
    .restart local v12       #userId:Ljava/lang/String;
    if-nez v12, :cond_14b

    .line 2421
    const-string v11, "ReaderProvider"

    const-string v13, "Failed to get user ID"

    invoke-static {v11, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2422
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2424
    :cond_14b
    const-string v11, "alternate_href"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2425
    .restart local v5       #url:Ljava/lang/String;
    const-string v11, "title_plaintext"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2426
    .restart local v14       #title:Ljava/lang/String;
    const-string v11, "source_alternate_href"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 2427
    .local v15, srcUrl:Ljava/lang/String;
    const-string v11, "source_title_plaintext"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2428
    .local v16, srcTitle:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getSnippet(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v17

    .line 2429
    .local v17, snippet:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getAnnotation(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v18

    .line 2430
    .local v18, annotation:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getShare(Landroid/net/Uri;)Z

    move-result v19

    .line 2431
    .local v19, share:Z
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getTags(Landroid/net/Uri;)Ljava/util/List;

    move-result-object v20

    .line 2432
    .local v20, tags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/16 v21, 0x0

    .local v21, linkify:Z
    move-object v11, v4

    move-object v13, v5

    .line 2433
    invoke-static/range {v10 .. v21}, Lcom/google/android/apps/reader/content/ReaderActions;->createNote(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Z)Z

    move-result v11

    if-eqz v11, :cond_192

    .line 2435
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2440
    const-string v11, "-"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_d

    .line 2447
    :cond_192
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2451
    .end local v4           #account:Lcom/google/android/accounts/Account;
    .end local v5           #url:Ljava/lang/String;
    .end local v12           #userId:Ljava/lang/String;
    .end local v14           #title:Ljava/lang/String;
    .end local v15           #srcUrl:Ljava/lang/String;
    .end local v16           #srcTitle:Ljava/lang/String;
    .end local v17           #snippet:Ljava/lang/String;
    .end local v18           #annotation:Ljava/lang/String;
    .end local v19           #share:Z
    .end local v20           #tags:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v21           #linkify:Z
    :sswitch_196
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v4

    .line 2452
    .restart local v4       #account:Lcom/google/android/accounts/Account;
    const-string v11, "key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 2453
    .local v28, key:Ljava/lang/String;
    const-string v11, "value"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 2454
    .local v36, value:Ljava/lang/String;
    move-object/from16 v0, v28

    move-object/from16 v1, v36

    invoke-static {v10, v4, v0, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->setPreference(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1d5

    .line 2455
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->preferences(Lcom/google/android/accounts/Account;)Landroid/content/SharedPreferences;

    move-result-object v11

    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    .line 2456
    .local v24, editor:Landroid/content/SharedPreferences$Editor;
    move-object/from16 v0, v24

    move-object/from16 v1, v28

    move-object/from16 v2, v36

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2457
    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2458
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2459
    move-object/from16 v0, v28

    invoke-static {v4, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Preferences;->itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_d

    .line 2461
    .end local v24           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_1d5
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2465
    .end local v4           #account:Lcom/google/android/accounts/Account;
    .end local v28           #key:Ljava/lang/String;
    .end local v36           #value:Ljava/lang/String;
    :sswitch_1d9
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v4

    .line 2466
    .restart local v4       #account:Lcom/google/android/accounts/Account;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Preferences;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v32

    .line 2467
    .restart local v32       #streamId:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/reader/content/ReaderProvider;->setUserIdIfMissing(Ljava/lang/String;Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v32

    .line 2468
    const-string v11, "key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 2469
    .restart local v28       #key:Ljava/lang/String;
    const-string v11, "value"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 2470
    .restart local v36       #value:Ljava/lang/String;
    move-object/from16 v0, v32

    move-object/from16 v1, v28

    move-object/from16 v2, v36

    invoke-static {v10, v4, v0, v1, v2}, Lcom/google/android/apps/reader/content/ReaderActions;->setStreamPreference(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_22a

    .line 2471
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->streamPreferences(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v11

    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v24

    .line 2472
    .restart local v24       #editor:Landroid/content/SharedPreferences$Editor;
    move-object/from16 v0, v24

    move-object/from16 v1, v28

    move-object/from16 v2, v36

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2473
    invoke-interface/range {v24 .. v24}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2474
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2475
    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-static {v4, v0, v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Preferences;->streamItemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_d

    .line 2477
    .end local v24           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_22a
    const/16 p1, 0x0

    goto/16 :goto_d

    .line 2481
    .end local v4           #account:Lcom/google/android/accounts/Account;
    .end local v28           #key:Ljava/lang/String;
    .end local v32           #streamId:Ljava/lang/String;
    .end local v36           #value:Ljava/lang/String;
    :sswitch_22e
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v4

    .line 2482
    .restart local v4       #account:Lcom/google/android/accounts/Account;
    const/16 v30, 0x0

    .line 2483
    .local v30, mode:I
    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v13, "upload"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_246

    .line 2484
    or-int/lit8 v30, v30, 0x2

    .line 2486
    :cond_246
    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v13, "preferences"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_258

    .line 2487
    or-int/lit8 v30, v30, 0x4

    .line 2489
    :cond_258
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    sget-object v13, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v11, v13}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 2491
    :try_start_261
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSyncCallable(Lcom/google/android/accounts/Account;I)Ljava/util/concurrent/Callable;

    move-result-object v34

    .line 2492
    .local v34, sync:Ljava/util/concurrent/Callable;,"Ljava/util/concurrent/Callable<Ljava/lang/Void;>;"
    invoke-interface/range {v34 .. v34}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_26c
    .catchall {:try_start_261 .. :try_end_26c} :catchall_2b9
    .catch Ljava/lang/Exception; {:try_start_261 .. :try_end_26c} :catch_276

    .line 2502
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto/16 :goto_d

    .line 2494
    .end local v34           #sync:Ljava/util/concurrent/Callable;,"Ljava/util/concurrent/Callable<Ljava/lang/Void;>;"
    :catch_276
    move-exception v23

    .line 2495
    .local v23, e:Ljava/lang/Exception;
    :try_start_277
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mConfig:Lcom/google/android/apps/reader/util/Config;

    invoke-virtual {v11}, Lcom/google/android/apps/reader/util/Config;->logd()Z

    move-result v11

    if-eqz v11, :cond_2af

    .line 2496
    const-string v11, "ReaderProvider"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "Synchronization failed for account: "

    move-object/from16 v0, v37

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v0, v4, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v23

    invoke-static {v11, v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2a3
    .catchall {:try_start_277 .. :try_end_2a3} :catchall_2b9

    .line 2500
    :goto_2a3
    const/16 p1, 0x0

    .line 2502
    .end local p1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto/16 :goto_d

    .line 2498
    .restart local p1
    :cond_2af
    :try_start_2af
    const-string v11, "ReaderProvider"

    const-string v13, "Synchronization failed for account"

    move-object/from16 v0, v23

    invoke-static {v11, v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2b8
    .catchall {:try_start_2af .. :try_end_2b8} :catchall_2b9

    goto :goto_2a3

    .line 2502
    .end local v23           #e:Ljava/lang/Exception;
    :catchall_2b9
    move-exception v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    const/16 v37, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v13, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v11

    .line 2506
    .end local v4           #account:Lcom/google/android/accounts/Account;
    .end local v30           #mode:I
    :sswitch_2c6
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v4

    .line 2507
    .restart local v4       #account:Lcom/google/android/accounts/Account;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getFileCache()Lcom/google/android/apps/reader/content/ReaderFileCache;

    move-result-object v26

    .line 2508
    .local v26, fileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getCleanupPriority(Landroid/content/ContentValues;)I

    move-result v31

    .line 2509
    .local v31, priority:I
    new-instance v22, Lcom/google/android/apps/reader/content/ReaderCleanup;

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move/from16 v2, v31

    invoke-direct {v0, v4, v10, v1, v2}, Lcom/google/android/apps/reader/content/ReaderCleanup;-><init>(Lcom/google/android/accounts/Account;Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ReaderFileCache;I)V

    .line 2510
    .local v22, cleanup:Lcom/google/android/apps/reader/content/ReaderCleanup;
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/reader/content/ReaderCleanup;->run()V

    goto/16 :goto_d

    .line 2335
    :sswitch_data_2e2
    .sparse-switch
        0x1 -> :sswitch_e
        0x3 -> :sswitch_7c
        0x7 -> :sswitch_134
        0xa -> :sswitch_22e
        0xf -> :sswitch_2c6
        0x14 -> :sswitch_196
        0x15 -> :sswitch_1d9
    .end sparse-switch
.end method

.method public loadContent(Landroid/net/Uri;)V
    .registers 11
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2084
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 2085
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "0"

    .line 2086
    .local v3, selection:Ljava/lang/String;
    const/4 v4, 0x0

    .line 2087
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v5, 0x0

    .line 2088
    .local v5, sortOrder:Ljava/lang/String;
    new-array v6, v8, [Landroid/database/MatrixCursor;

    const/4 v0, 0x0

    aput-object v0, v6, v1

    .line 2089
    .local v6, output:[Landroid/database/MatrixCursor;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .local v7, extras:Landroid/os/Bundle;
    move-object v0, p0

    move-object v1, p1

    .line 2090
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/reader/content/ReaderProvider;->feedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Landroid/database/MatrixCursor;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2091
    return-void
.end method

.method protected final match(Landroid/net/Uri;)I
    .registers 3
    .parameter "uri"

    .prologue
    .line 774
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public onCreate()Z
    .registers 3

    .prologue
    .line 282
    sget v0, Lcom/google/android/apps/reader/content/ReaderProvider;->API_VERSION:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_d

    .line 284
    const-string v0, "http.keepAlive"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 287
    :cond_d
    const-string v0, "com.google.android.apps.reader"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createUriMatcher(Ljava/lang/String;)Landroid/content/UriMatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSQLiteOpenHelper()Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mDatabase:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createFileCache()Lcom/google/android/apps/reader/content/ReaderFileCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mFileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSubscriptionsProjectionMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mSubscriptionsProjectionMap:Ljava/util/Map;

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createTagsProjectionMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mTagsProjectionMap:Ljava/util/Map;

    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createStreamsProjectionMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mStreamsProjectionMap:Ljava/util/Map;

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createUnreadCountsProjectionMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUnreadCountsProjectionMap:Ljava/util/Map;

    .line 294
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemsProjectionMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsProjectionMap:Ljava/util/Map;

    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createOverviewProjectionMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mOverviewProjectionMap:Ljava/util/Map;

    .line 296
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createSubscriptionsTables()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mSubscriptionsTables:Ljava/lang/String;

    .line 297
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createFriendsTables()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mFriendsTables:Ljava/lang/String;

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createFriendsProjectionMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mFriendsProjectionMap:Ljava/util/Map;

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createTagsTables()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mTagsTables:Ljava/lang/String;

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createOverviewTables()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mOverviewTables:Ljava/lang/String;

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemsTables()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mItemsTables:Ljava/lang/String;

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemLinksTables()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mLinksTables:Ljava/lang/String;

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/reader/util/Config;->get(Landroid/content/Context;)Lcom/google/android/apps/reader/util/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mConfig:Lcom/google/android/apps/reader/util/Config;

    .line 305
    const/4 v0, 0x1

    return v0
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .registers 12
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2940
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    .line 2941
    .local v5, segments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_47

    const-string v7, "android_res"

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_47

    .line 2953
    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2954
    .local v6, type:Ljava/lang/String;
    const/4 v7, 0x2

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2955
    .local v1, name:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2956
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2957
    .local v3, res:Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 2958
    .local v2, packageName:Ljava/lang/String;
    invoke-virtual {v3, v1, v6, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 2959
    .local v4, resId:I
    if-eqz v4, :cond_3d

    .line 2960
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v7

    .line 2965
    .end local v0           #context:Landroid/content/Context;
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #packageName:Ljava/lang/String;
    .end local v3           #res:Landroid/content/res/Resources;
    .end local v4           #resId:I
    .end local v6           #type:Ljava/lang/String;
    :goto_3c
    return-object v7

    .line 2962
    .restart local v0       #context:Landroid/content/Context;
    .restart local v1       #name:Ljava/lang/String;
    .restart local v2       #packageName:Ljava/lang/String;
    .restart local v3       #res:Landroid/content/res/Resources;
    .restart local v4       #resId:I
    .restart local v6       #type:Ljava/lang/String;
    :cond_3d
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 2965
    .end local v0           #context:Landroid/content/Context;
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #packageName:Ljava/lang/String;
    .end local v3           #res:Landroid/content/res/Resources;
    .end local v4           #resId:I
    .end local v6           #type:Ljava/lang/String;
    :cond_47
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v7

    goto :goto_3c
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 18
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2879
    const-string v12, "r"

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_25

    .line 2880
    new-instance v12, Ljava/io/FileNotFoundException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unsupported mode: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 2882
    :cond_25
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getFileCache()Lcom/google/android/apps/reader/content/ReaderFileCache;

    move-result-object v6

    .line 2883
    .local v6, fileCache:Lcom/google/android/apps/reader/content/ReaderFileCache;
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v12

    sparse-switch v12, :sswitch_data_d8

    .line 2934
    invoke-super/range {p0 .. p2}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v12

    :goto_34
    return-object v12

    .line 2885
    :sswitch_35
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v1

    .line 2886
    .local v1, account:Lcom/google/android/accounts/Account;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 2887
    .local v2, contactId:J
    invoke-virtual {v6, v1, v2, v3}, Lcom/google/android/apps/reader/content/ReaderFileCache;->getFriendPhotoFile(Lcom/google/android/accounts/Account;J)Ljava/io/File;

    move-result-object v5

    .line 2888
    .local v5, file:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_4e

    .line 2909
    :goto_47
    const/high16 v12, 0x1000

    invoke-static {v5, v12}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v12

    goto :goto_34

    .line 2892
    :cond_4e
    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetFriendPhotoUrl(Lcom/google/android/accounts/Account;J)Ljava/lang/String;

    move-result-object v11

    .line 2893
    .local v11, url:Ljava/lang/String;
    if-eqz v11, :cond_6f

    .line 2897
    iget-object v12, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    sget-object v13, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v12, v13}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 2899
    :try_start_5b
    const-string v12, "cp"

    invoke-direct {p0, v1, v12, v11, v5}, Lcom/google/android/apps/reader/content/ReaderProvider;->retrieveFile(Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    :try_end_60
    .catchall {:try_start_5b .. :try_end_60} :catchall_67

    .line 2901
    iget-object v12, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_47

    :catchall_67
    move-exception v12

    iget-object v13, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mNotifyAuthFailure:Ljava/lang/ThreadLocal;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v12

    .line 2906
    :cond_6f
    new-instance v12, Ljava/io/FileNotFoundException;

    invoke-direct {v12}, Ljava/io/FileNotFoundException;-><init>()V

    throw v12

    .line 2912
    .end local v1           #account:Lcom/google/android/accounts/Account;
    .end local v2           #contactId:J
    .end local v5           #file:Ljava/io/File;
    .end local v11           #url:Ljava/lang/String;
    :sswitch_75
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 2913
    .local v8, htmlUrl:Ljava/lang/String;
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 2914
    .local v7, htmlUri:Landroid/net/Uri;
    invoke-virtual {v7}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    .line 2915
    .local v4, domain:Ljava/lang/String;
    if-nez v4, :cond_9c

    .line 2916
    new-instance v12, Ljava/io/FileNotFoundException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " has no domain"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 2918
    :cond_9c
    invoke-virtual {v6, v4}, Lcom/google/android/apps/reader/content/ReaderFileCache;->getFaviconFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    .line 2919
    .restart local v5       #file:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_ad

    .line 2925
    :goto_a6
    const/high16 v12, 0x1000

    invoke-static {v5, v12}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v12

    goto :goto_34

    .line 2922
    :cond_ad
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "http://s2.googleusercontent.com/s2/favicons?domain="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2923
    .restart local v11       #url:Ljava/lang/String;
    invoke-direct {p0, v11, v5}, Lcom/google/android/apps/reader/content/ReaderProvider;->retrieveFile(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_a6

    .line 2928
    .end local v4           #domain:Ljava/lang/String;
    .end local v5           #file:Ljava/io/File;
    .end local v7           #htmlUri:Landroid/net/Uri;
    .end local v8           #htmlUrl:Ljava/lang/String;
    .end local v11           #url:Ljava/lang/String;
    :sswitch_c4
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v1

    .line 2929
    .restart local v1       #account:Lcom/google/android/accounts/Account;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v9

    .line 2930
    .local v9, itemId:J
    invoke-virtual {v6, v1, v9, v10}, Lcom/google/android/apps/reader/content/ReaderFileCache;->getItemHtmlFile(Lcom/google/android/accounts/Account;J)Ljava/io/File;

    move-result-object v5

    .line 2931
    .restart local v5       #file:Ljava/io/File;
    const/high16 v12, 0x1000

    invoke-static {v5, v12}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v12

    goto/16 :goto_34

    .line 2883
    :sswitch_data_d8
    .sparse-switch
        0x10 -> :sswitch_35
        0x13 -> :sswitch_c4
        0x1a -> :sswitch_75
    .end sparse-switch
.end method

.method preferences(Lcom/google/android/accounts/Account;)Landroid/content/SharedPreferences;
    .registers 4
    .parameter "account"

    .prologue
    .line 1590
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1591
    .local v0, context:Landroid/content/Context;
    invoke-static {v0, p1}, Lcom/google/android/apps/reader/content/PreferencesContentHandler;->getSharedPreferences(Landroid/content/Context;Lcom/google/android/accounts/Account;)Landroid/content/SharedPreferences;

    move-result-object v1

    return-object v1
.end method

.method putUserId(Lcom/google/android/accounts/Account;Ljava/lang/String;)V
    .registers 4
    .parameter "account"
    .parameter "userId"

    .prologue
    .line 2310
    iget-object v0, p0, Lcom/google/android/apps/reader/content/ReaderProvider;->mUserIds:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2311
    return-void
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 33
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    .line 1489
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v26

    .line 1490
    .local v26, type:I
    sparse-switch v26, :sswitch_data_140

    .line 1509
    :cond_7
    :goto_7
    packed-switch v26, :pswitch_data_152

    .line 1532
    :pswitch_a
    const/4 v3, 0x1

    new-array v0, v3, [Landroid/database/MatrixCursor;

    move-object/from16 v16, v0

    .line 1533
    .local v16, output:[Landroid/database/MatrixCursor;
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 1536
    .local v9, extras:Landroid/os/Bundle;
    const-string v3, "com.google.reader.cursor.extra.URI"

    move-object/from16 v0, p1

    invoke-virtual {v9, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1539
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v18

    .line 1540
    .local v18, account:Lcom/google/android/accounts/Account;
    if-eqz v18, :cond_33

    .line 1541
    const-string v3, "com.google.reader.cursor.extra.ACCOUNT_NAME"

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    const-string v3, "com.google.reader.cursor.extra.ACCOUNT_TYPE"

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_33
    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    move-object/from16 v17, v9

    .line 1548
    :try_start_41
    invoke-direct/range {v10 .. v17}, Lcom/google/android/apps/reader/content/ReaderProvider;->feedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Landroid/database/MatrixCursor;Landroid/os/Bundle;)Landroid/database/Cursor;
    :try_end_44
    .catch Ljava/net/HttpRetryException; {:try_start_41 .. :try_end_44} :catch_df
    .catch Ljava/lang/Throwable; {:try_start_41 .. :try_end_44} :catch_fd

    move-result-object v19

    .line 1559
    .local v19, cursor:Landroid/database/Cursor;
    :goto_45
    :try_start_45
    move-object/from16 v0, v19

    invoke-static {v0, v9}, Lcom/google/android/feeds/FeedProvider;->feedCursor(Landroid/database/Cursor;Landroid/os/Bundle;)Landroid/database/Cursor;
    :try_end_4a
    .catch Ljava/lang/Throwable; {:try_start_45 .. :try_end_4a} :catch_fd

    move-result-object v3

    .line 1581
    .end local v9           #extras:Landroid/os/Bundle;
    .end local v16           #output:[Landroid/database/MatrixCursor;
    .end local v18           #account:Lcom/google/android/accounts/Account;
    .end local v19           #cursor:Landroid/database/Cursor;
    :goto_4b
    return-object v3

    .line 1493
    :sswitch_4c
    if-nez p2, :cond_7

    .line 1494
    const/4 v3, 0x6

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 p2, v0

    .end local p2
    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, p2, v3

    const/4 v3, 0x1

    const-string v4, "suggest_text_1"

    aput-object v4, p2, v3

    const/4 v3, 0x2

    const-string v4, "suggest_text_2"

    aput-object v4, p2, v3

    const/4 v3, 0x3

    const-string v4, "suggest_icon_1"

    aput-object v4, p2, v3

    const/4 v3, 0x4

    const-string v4, "suggest_intent_data"

    aput-object v4, p2, v3

    const/4 v3, 0x5

    const-string v4, "suggest_shortcut_id"

    aput-object v4, p2, v3

    .restart local p2
    goto :goto_7

    .line 1504
    :sswitch_72
    if-nez p2, :cond_7

    .line 1505
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 p2, v0

    .end local p2
    const/4 v3, 0x0

    const-string v4, "key"

    aput-object v4, p2, v3

    const/4 v3, 0x1

    const-string v4, "value"

    aput-object v4, p2, v3

    .restart local p2
    goto :goto_7

    .line 1511
    :pswitch_84
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .restart local v9       #extras:Landroid/os/Bundle;
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 1512
    invoke-virtual/range {v3 .. v9}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_4b

    .line 1516
    .end local v9           #extras:Landroid/os/Bundle;
    :pswitch_9a
    new-instance v3, Landroid/database/MatrixCursor;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    goto :goto_4b

    .line 1518
    :pswitch_a2
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v18

    .line 1519
    .restart local v18       #account:Lcom/google/android/accounts/Account;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/content/ReaderProvider;->preferences(Lcom/google/android/accounts/Account;)Landroid/content/SharedPreferences;

    move-result-object v21

    .line 1520
    .local v21, preferences:Landroid/content/SharedPreferences;
    new-instance v3, Lcom/google/android/apps/reader/content/PreferencesCursor;

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/reader/content/PreferencesCursor;-><init>([Ljava/lang/String;Landroid/content/SharedPreferences;)V

    goto :goto_4b

    .line 1523
    .end local v18           #account:Lcom/google/android/accounts/Account;
    .end local v21           #preferences:Landroid/content/SharedPreferences;
    :pswitch_b8
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v18

    .line 1524
    .restart local v18       #account:Lcom/google/android/accounts/Account;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Preferences;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v23

    .line 1525
    .local v23, streamId:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/reader/content/ReaderProvider;->setUserIdIfMissing(Ljava/lang/String;Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v23

    .line 1526
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/reader/content/ReaderProvider;->streamPreferences(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v21

    .line 1527
    .restart local v21       #preferences:Landroid/content/SharedPreferences;
    new-instance v3, Lcom/google/android/apps/reader/content/PreferencesCursor;

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/reader/content/PreferencesCursor;-><init>([Ljava/lang/String;Landroid/content/SharedPreferences;)V

    goto/16 :goto_4b

    .line 1550
    .end local v21           #preferences:Landroid/content/SharedPreferences;
    .end local v23           #streamId:Ljava/lang/String;
    .restart local v9       #extras:Landroid/os/Bundle;
    .restart local v16       #output:[Landroid/database/MatrixCursor;
    :catch_df
    move-exception v20

    .line 1555
    .local v20, e:Ljava/net/HttpRetryException;
    :try_start_e0
    const-string v3, "ReaderProvider"

    const-string v4, "Retrying query..."

    move-object/from16 v0, v20

    invoke-static {v3, v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    move-object/from16 v17, v9

    .line 1556
    invoke-direct/range {v10 .. v17}, Lcom/google/android/apps/reader/content/ReaderProvider;->feedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Landroid/database/MatrixCursor;Landroid/os/Bundle;)Landroid/database/Cursor;
    :try_end_fa
    .catch Ljava/lang/Throwable; {:try_start_e0 .. :try_end_fa} :catch_fd

    move-result-object v19

    .restart local v19       #cursor:Landroid/database/Cursor;
    goto/16 :goto_45

    .line 1560
    .end local v19           #cursor:Landroid/database/Cursor;
    .end local v20           #e:Ljava/net/HttpRetryException;
    :catch_fd
    move-exception v24

    .line 1561
    .local v24, t:Ljava/lang/Throwable;
    const-string v3, "ReaderProvider"

    const-string v4, "Query failed"

    move-object/from16 v0, v24

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1564
    const/4 v3, 0x0

    aget-object v19, v16, v3

    .line 1566
    .restart local v19       #cursor:Landroid/database/Cursor;
    if-nez v19, :cond_11c

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 1570
    :try_start_118
    invoke-virtual/range {v3 .. v9}, Lcom/google/android/apps/reader/content/ReaderProvider;->databaseQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/database/Cursor;
    :try_end_11b
    .catch Ljava/lang/Throwable; {:try_start_118 .. :try_end_11b} :catch_12c

    move-result-object v19

    .line 1580
    :cond_11c
    :goto_11c
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/reader/net/ReaderContentHandler;->getSolutionForError(Ljava/lang/Throwable;)Landroid/content/Intent;

    move-result-object v22

    .line 1581
    .local v22, solution:Landroid/content/Intent;
    move-object/from16 v0, v19

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-static {v0, v9, v1, v2}, Lcom/google/android/feeds/FeedProvider;->errorCursor(Landroid/database/Cursor;Landroid/os/Bundle;Ljava/lang/Throwable;Landroid/content/Intent;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_4b

    .line 1572
    .end local v22           #solution:Landroid/content/Intent;
    :catch_12c
    move-exception v25

    .line 1575
    .local v25, t2:Ljava/lang/Throwable;
    const-string v3, "ReaderProvider"

    const-string v4, "Database query failed"

    move-object/from16 v0, v25

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1576
    new-instance v19, Landroid/database/MatrixCursor;

    .end local v19           #cursor:Landroid/database/Cursor;
    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .restart local v19       #cursor:Landroid/database/Cursor;
    goto :goto_11c

    .line 1490
    :sswitch_data_140
    .sparse-switch
        0xc -> :sswitch_4c
        0x12 -> :sswitch_4c
        0x14 -> :sswitch_72
        0x15 -> :sswitch_72
    .end sparse-switch

    .line 1509
    :pswitch_data_152
    .packed-switch 0x11
        :pswitch_84
        :pswitch_9a
        :pswitch_a
        :pswitch_a2
        :pswitch_b8
    .end packed-switch
.end method

.method streamPreferences(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/SharedPreferences;
    .registers 5
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 1597
    invoke-virtual {p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1598
    .local v0, context:Landroid/content/Context;
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/reader/content/StreamPreferencesContentHandler;->getSharedPreferences(Landroid/content/Context;Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    return-object v1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 27
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    .line 2588
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/reader/content/ReaderProvider;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2589
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->match(Landroid/net/Uri;)I

    move-result v6

    packed-switch v6, :pswitch_data_242

    .line 2734
    :pswitch_b
    const/4 v6, 0x0

    :goto_c
    return v6

    .line 2591
    :pswitch_d
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v3

    .line 2592
    .local v3, account:Lcom/google/android/accounts/Account;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v11

    .line 2593
    .local v11, itemId:J
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11, v12}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetExternalId(Lcom/google/android/accounts/Account;J)Ljava/lang/String;

    move-result-object v9

    .line 2594
    .local v9, externalId:Ljava/lang/String;
    if-nez v9, :cond_26

    .line 2595
    const-string v6, "ReaderProvider"

    const-string v7, "Cannot update without external ID"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2596
    const/4 v6, 0x0

    goto :goto_c

    .line 2598
    :cond_26
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 2599
    .local v4, userId:Ljava/lang/String;
    if-nez v4, :cond_37

    .line 2600
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to get user ID"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2601
    const/4 v6, 0x0

    goto :goto_c

    .line 2603
    :cond_37
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-static {v9}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    move-object/from16 v7, p2

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/content/ContentValues;)Z

    move-result v6

    if-eqz v6, :cond_50

    .line 2606
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2607
    const/4 v6, 0x1

    goto :goto_c

    .line 2609
    :cond_50
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mConfig:Lcom/google/android/apps/reader/util/Config;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/util/Config;->logd()Z

    move-result v6

    if-eqz v6, :cond_72

    .line 2610
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to update %s with %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v21, 0x0

    aput-object p1, v8, v21

    const/16 v21, 0x1

    aput-object p2, v8, v21

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2614
    :goto_70
    const/4 v6, 0x0

    goto :goto_c

    .line 2612
    :cond_72
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to update uri with values"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_70

    .line 2618
    .end local v3           #account:Lcom/google/android/accounts/Account;
    .end local v4           #userId:Ljava/lang/String;
    .end local v9           #externalId:Ljava/lang/String;
    .end local v11           #itemId:J
    :pswitch_7a
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v17

    .line 2619
    .local v17, streamId:Ljava/lang/String;
    if-nez v17, :cond_af

    .line 2620
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mConfig:Lcom/google/android/apps/reader/util/Config;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/util/Config;->logd()Z

    move-result v6

    if-eqz v6, :cond_a7

    .line 2621
    const-string v6, "ReaderProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cannot update without stream ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2625
    :goto_a4
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2623
    :cond_a7
    const-string v6, "ReaderProvider"

    const-string v7, "Cannot update without stream ID"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a4

    .line 2628
    :cond_af
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->createItemList(Landroid/net/Uri;)Lcom/google/android/apps/reader/content/ItemList;

    move-result-object v5

    .line 2629
    .local v5, itemList:Lcom/google/android/apps/reader/content/ItemList;
    if-nez v5, :cond_e4

    .line 2630
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mConfig:Lcom/google/android/apps/reader/util/Config;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/util/Config;->logd()Z

    move-result v6

    if-eqz v6, :cond_dc

    .line 2631
    const-string v6, "ReaderProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported content URI: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2635
    :goto_d9
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2633
    :cond_dc
    const-string v6, "ReaderProvider"

    const-string v7, "Unsupported content URI"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d9

    .line 2638
    :cond_e4
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v3

    .line 2640
    .restart local v3       #account:Lcom/google/android/accounts/Account;
    if-eqz p3, :cond_16d

    .line 2642
    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v7, "read"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11e

    .line 2643
    sget-object v6, Lcom/google/android/apps/reader/content/ReaderProvider;->PATTERN_CRAWL_TIME_OFFSET:Ljava/util/regex/Pattern;

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v14

    .line 2644
    .local v14, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v14}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_11e

    .line 2645
    const/4 v6, 0x1

    invoke-virtual {v14, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v15

    .line 2646
    .local v15, offset:J
    move-wide v0, v15

    invoke-static {v2, v5, v0, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->markAllAsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ItemList;J)Z

    move-result v6

    if-eqz v6, :cond_11e

    .line 2647
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2649
    const/4 v6, -0x1

    goto/16 :goto_c

    .line 2654
    .end local v14           #matcher:Ljava/util/regex/Matcher;
    .end local v15           #offset:J
    :cond_11e
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 2655
    .restart local v4       #userId:Ljava/lang/String;
    if-nez v4, :cond_130

    .line 2656
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to get user ID"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2657
    const/4 v6, 0x0

    goto/16 :goto_c

    :cond_130
    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p2

    .line 2659
    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/reader/content/ReaderActions;->updateItems(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/reader/content/ItemList;Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v6

    if-eqz v6, :cond_142

    .line 2661
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2664
    const/4 v6, -0x1

    goto/16 :goto_c

    .line 2666
    :cond_142
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mConfig:Lcom/google/android/apps/reader/util/Config;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/util/Config;->logd()Z

    move-result v6

    if-eqz v6, :cond_165

    .line 2667
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to update %s with %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v21, 0x0

    aput-object p1, v8, v21

    const/16 v21, 0x1

    aput-object p2, v8, v21

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2671
    :goto_162
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2669
    :cond_165
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to update uri with values"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_162

    .line 2674
    .end local v4           #userId:Ljava/lang/String;
    :cond_16d
    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v7, "read"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_189

    invoke-static {v2, v5}, Lcom/google/android/apps/reader/content/ReaderActions;->markAllAsRead(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/reader/content/ItemList;)Z

    move-result v6

    if-eqz v6, :cond_189

    .line 2676
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2679
    const/4 v6, -0x1

    goto/16 :goto_c

    .line 2681
    :cond_189
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/reader/content/ReaderProvider;->mConfig:Lcom/google/android/apps/reader/util/Config;

    invoke-virtual {v6}, Lcom/google/android/apps/reader/util/Config;->logd()Z

    move-result v6

    if-eqz v6, :cond_1ac

    .line 2682
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to update %s with %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v21, 0x0

    aput-object p1, v8, v21

    const/16 v21, 0x1

    aput-object p2, v8, v21

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2686
    :goto_1a9
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2684
    :cond_1ac
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to update uri with values"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1a9

    .line 2691
    .end local v3           #account:Lcom/google/android/accounts/Account;
    .end local v5           #itemList:Lcom/google/android/apps/reader/content/ItemList;
    .end local v17           #streamId:Ljava/lang/String;
    :pswitch_1b4
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v3

    .line 2692
    .restart local v3       #account:Lcom/google/android/accounts/Account;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v18

    .line 2693
    .local v18, subscriptionId:Ljava/lang/String;
    const-string v6, "title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2694
    .local v19, title:Ljava/lang/String;
    if-nez v19, :cond_1d0

    .line 2695
    const-string v6, "ReaderProvider"

    const-string v7, "Cannot rename without title"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2696
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2698
    :cond_1d0
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/reader/content/ReaderActions;->renameSubscription(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1e0

    .line 2699
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2700
    const/4 v6, 0x1

    goto/16 :goto_c

    .line 2702
    :cond_1e0
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2706
    .end local v3           #account:Lcom/google/android/accounts/Account;
    .end local v18           #subscriptionId:Ljava/lang/String;
    .end local v19           #title:Ljava/lang/String;
    :pswitch_1e3
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v3

    .line 2707
    .restart local v3       #account:Lcom/google/android/accounts/Account;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    .line 2708
    .local v10, from:Ljava/lang/String;
    const-string v6, "label"

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 2709
    .local v13, label:Ljava/lang/String;
    if-nez v13, :cond_1ff

    .line 2710
    const-string v6, "ReaderProvider"

    const-string v7, "Cannot rename without label"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2711
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2713
    :cond_1ff
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/reader/content/ReaderProvider;->blockingGetUserId(Lcom/google/android/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 2714
    .restart local v4       #userId:Ljava/lang/String;
    if-nez v4, :cond_211

    .line 2715
    const-string v6, "ReaderProvider"

    const-string v7, "Failed to get user ID"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2716
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2718
    :cond_211
    invoke-static {v10}, Lcom/google/android/apps/reader/provider/ReaderStream;->isUserIdMissing(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_21b

    .line 2719
    invoke-static {v10, v4}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2721
    :cond_21b
    invoke-static {v4, v13}, Lcom/google/android/apps/reader/provider/ReaderStream;->createTagId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 2722
    .local v20, to:Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_231

    .line 2723
    const-string v6, "ReaderProvider"

    const-string v7, "Tag ID did not change"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2724
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2726
    :cond_231
    move-object/from16 v0, v20

    invoke-static {v2, v3, v10, v0, v13}, Lcom/google/android/apps/reader/content/ReaderActions;->renameTag(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_23f

    .line 2727
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/reader/content/ReaderProvider;->notifyChange(Landroid/net/Uri;)V

    .line 2728
    const/4 v6, 0x1

    goto/16 :goto_c

    .line 2730
    :cond_23f
    const/4 v6, 0x0

    goto/16 :goto_c

    .line 2589
    :pswitch_data_242
    .packed-switch 0x2
        :pswitch_1b4
        :pswitch_b
        :pswitch_1e3
        :pswitch_b
        :pswitch_b
        :pswitch_7a
        :pswitch_d
    .end packed-switch
.end method
