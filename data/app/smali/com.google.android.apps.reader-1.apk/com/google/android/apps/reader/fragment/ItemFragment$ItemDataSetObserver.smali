.class Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "ItemFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/fragment/ItemFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/reader/fragment/ItemFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 670
    iput-object p1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/reader/fragment/ItemFragment;Lcom/google/android/apps/reader/fragment/ItemFragment$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 670
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;-><init>(Lcom/google/android/apps/reader/fragment/ItemFragment;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .registers 2

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->markReadIfNotMarkedRead()V

    .line 677
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateViews()V

    .line 678
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->changeQueryIfItemNotFound()V

    .line 679
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;

    #calls: Lcom/google/android/apps/reader/fragment/ItemFragment;->loadMoreIfNearEnd()V
    invoke-static {v0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->access$100(Lcom/google/android/apps/reader/fragment/ItemFragment;)V

    .line 680
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;

    #getter for: Lcom/google/android/apps/reader/fragment/ItemFragment;->mDataSetObservers:Landroid/database/DataSetObservable;
    invoke-static {v0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->access$200(Lcom/google/android/apps/reader/fragment/ItemFragment;)Landroid/database/DataSetObservable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 681
    return-void
.end method

.method public onInvalidated()V
    .registers 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->this$0:Lcom/google/android/apps/reader/fragment/ItemFragment;

    #getter for: Lcom/google/android/apps/reader/fragment/ItemFragment;->mDataSetObservers:Landroid/database/DataSetObservable;
    invoke-static {v0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->access$200(Lcom/google/android/apps/reader/fragment/ItemFragment;)Landroid/database/DataSetObservable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    .line 686
    return-void
.end method
