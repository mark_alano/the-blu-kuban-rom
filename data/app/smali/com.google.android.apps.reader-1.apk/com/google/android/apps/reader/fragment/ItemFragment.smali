.class public Lcom/google/android/apps/reader/fragment/ItemFragment;
.super Landroid/support/v4/app/Fragment;
.source "ItemFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/fragment/ItemFragment$1;,
        Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;,
        Lcom/google/android/apps/reader/fragment/ItemFragment$Observer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/support/v4/view/ViewPager$OnPageChangeListener;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/CompoundButton$OnCheckedChangeListener;"
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final LOADER_ITEMS:I = 0x1

.field private static final LOAD_MORE_AMOUNT:I = 0x14

.field private static final LOAD_MORE_DISTANCE:I = 0xa

.field private static final STATE_ITEM:Ljava/lang/String; = "reader:item"

.field private static final STATE_MARKED_READ:Ljava/lang/String; = "reader:marked_read"

.field private static final STATE_URI:Ljava/lang/String; = "reader:uri"

.field private static final TAG:Ljava/lang/String; = "ItemActivity"


# instance fields
.field private final mDataSetObservers:Landroid/database/DataSetObservable;

.field private mHintSwipe:Landroid/view/View;

.field private mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

.field private mItemId:J

.field private mItemObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;

.field private mItemView:Landroid/support/v4/view/ViewPager;

.field private mItems:Lcom/google/android/apps/reader/fragment/Loadable;

.field private mLocalPreferences:Landroid/content/SharedPreferences;

.field mMarkedRead:Z

.field private mObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$Observer;

.field private mStarToggle:Landroid/widget/CheckBox;

.field private mStreamLoading:Z

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 58
    const-class v0, Lcom/google/android/apps/reader/fragment/ItemFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/reader/fragment/ItemFragment;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 93
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mDataSetObservers:Landroid/database/DataSetObservable;

    .line 670
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/reader/fragment/ItemFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->loadMoreIfNearEnd()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/reader/fragment/ItemFragment;)Landroid/database/DataSetObservable;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mDataSetObservers:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method private findCurrentItem(Landroid/database/Cursor;)I
    .registers 7
    .parameter "data"

    .prologue
    .line 221
    if-eqz p1, :cond_19

    .line 222
    const/4 v0, 0x0

    .local v0, position:I
    :goto_3
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 223
    iget-wide v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getItemId(Landroid/database/Cursor;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_16

    .line 228
    .end local v0           #position:I
    :goto_15
    return v0

    .line 222
    .restart local v0       #position:I
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 228
    .end local v0           #position:I
    :cond_19
    const/4 v0, -0x1

    goto :goto_15
.end method

.method private isDifferentUri(Landroid/net/Uri;)Z
    .registers 3
    .parameter "uri"

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/reader/provider/ReaderContract;->equalsIgnoreMaxAgeAndItemCount(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private isVolumeKeyNavigationEnabled()Z
    .registers 4

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mLocalPreferences:Landroid/content/SharedPreferences;

    const-string v1, "volume_key_navigation"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private loadMoreIfNearEnd()V
    .registers 8

    .prologue
    .line 440
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v5, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 441
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_27

    .line 442
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 443
    .local v3, position:I
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 444
    .local v2, end:I
    sub-int v1, v2, v3

    .line 445
    .local v1, distanceToEnd:I
    const/16 v4, 0xa

    if-ge v1, v4, :cond_27

    .line 446
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v4}, Lcom/google/android/apps/reader/fragment/Loadable;->isReadyToLoadMore()Z

    move-result v4

    if-eqz v4, :cond_27

    .line 447
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Lcom/google/android/apps/reader/fragment/Loadable;->loadMore(I)Z

    .line 451
    .end local v1           #distanceToEnd:I
    .end local v2           #end:I
    .end local v3           #position:I
    :cond_27
    return-void
.end method

.method private moveToNextItemOrPage()Z
    .registers 2

    .prologue
    .line 608
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->moveToNext()Z

    move-result v0

    return v0
.end method

.method private onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "state"

    .prologue
    .line 378
    const-string v0, "reader:uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    .line 379
    const-string v0, "reader:item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    .line 380
    const-string v0, "reader:marked_read"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mMarkedRead:Z

    .line 381
    return-void
.end method

.method private onVolumeKeyNavigationPreferenceChanged()V
    .registers 3

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->isVolumeKeyNavigationEnabled()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setVolumeControlStream(I)V

    .line 325
    :goto_e
    return-void

    .line 323
    :cond_f
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/high16 v1, -0x8000

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setVolumeControlStream(I)V

    goto :goto_e
.end method

.method private static slideInOutBottom(Landroid/view/View;Z)V
    .registers 7
    .parameter "view"
    .parameter "visible"

    .prologue
    .line 359
    if-eqz p1, :cond_1c

    const/4 v3, 0x0

    .line 360
    .local v3, visibility:I
    :goto_3
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v3, v4, :cond_1b

    .line 361
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 362
    .local v2, context:Landroid/content/Context;
    if-eqz p1, :cond_1f

    const/high16 v1, 0x7f04

    .line 363
    .local v1, animationId:I
    :goto_11
    invoke-static {v2, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 364
    .local v0, animation:Landroid/view/animation/Animation;
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 365
    invoke-virtual {p0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 367
    .end local v0           #animation:Landroid/view/animation/Animation;
    .end local v1           #animationId:I
    .end local v2           #context:Landroid/content/Context;
    :cond_1b
    return-void

    .line 359
    .end local v3           #visibility:I
    :cond_1c
    const/16 v3, 0x8

    goto :goto_3

    .line 362
    .restart local v2       #context:Landroid/content/Context;
    .restart local v3       #visibility:I
    :cond_1f
    const v1, 0x7f040001

    goto :goto_11
.end method

.method private swapCursor(Landroid/database/Cursor;)V
    .registers 6
    .parameter "data"

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/google/android/apps/reader/fragment/ItemFragment;->findCurrentItem(Landroid/database/Cursor;)I

    move-result v0

    .line 233
    .local v0, newPosition:I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v0, v1, :cond_40

    .line 236
    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->unregisterObserver(Landroid/database/DataSetObserver;)V

    .line 237
    new-instance v1, Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V

    iput-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->swapCursor(Landroid/database/Cursor;)V

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 240
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 241
    if-eqz p1, :cond_3f

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;->onChanged()V

    .line 251
    :cond_3f
    :goto_3f
    return-void

    .line 249
    :cond_40
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->swapCursor(Landroid/database/Cursor;)V

    goto :goto_3f
.end method

.method private updateActionBar()V
    .registers 2

    .prologue
    .line 619
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/reader/widget/ReaderWindow;->invalidateOptionsMenu(Landroid/support/v4/app/FragmentActivity;)V

    .line 620
    return-void
.end method

.method private updateStarCheckBox()V
    .registers 5

    .prologue
    .line 602
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 603
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_17

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->isStarred(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_17

    const/4 v1, 0x1

    :goto_13
    invoke-virtual {p0, v1}, Lcom/google/android/apps/reader/fragment/ItemFragment;->setStarChecked(Z)V

    .line 604
    return-void

    .line 603
    :cond_17
    const/4 v1, 0x0

    goto :goto_13
.end method

.method private updateSwipeHint()V
    .registers 7

    .prologue
    const/4 v4, 0x1

    .line 343
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 344
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/reader/preference/LocalPreferences;->get(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 345
    .local v1, preferences:Landroid/content/SharedPreferences;
    const/4 v3, 0x1

    .line 346
    .local v3, showByDefault:Z
    const-string v5, "show_swipe_hint"

    invoke-interface {v1, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 350
    .local v2, show:Z
    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getCount()I

    move-result v5

    if-le v5, v4, :cond_1f

    :goto_18
    and-int/2addr v2, v4

    .line 352
    iget-object v4, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mHintSwipe:Landroid/view/View;

    invoke-static {v4, v2}, Lcom/google/android/apps/reader/fragment/ItemFragment;->slideInOutBottom(Landroid/view/View;Z)V

    .line 353
    return-void

    .line 350
    :cond_1f
    const/4 v4, 0x0

    goto :goto_18
.end method

.method private updateViewPager()V
    .registers 5

    .prologue
    .line 612
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItemPosition(J)I

    move-result v0

    .line 613
    .local v0, position:I
    const/4 v1, -0x2

    if-eq v0, v1, :cond_18

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v0, v1, :cond_18

    .line 614
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 616
    :cond_18
    return-void
.end method


# virtual methods
.method final changeQueryIfItemNotFound()V
    .registers 7

    .prologue
    .line 642
    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_9

    .line 656
    :cond_8
    :goto_8
    return-void

    .line 646
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v0

    .line 647
    .local v0, account:Lcom/google/android/accounts/Account;
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 648
    .local v1, uri:Landroid/net/Uri;
    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/fragment/ItemFragment;->isDifferentUri(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 652
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v3, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_8

    .line 654
    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->itemUri(Lcom/google/android/accounts/Account;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/reader/fragment/ItemFragment;->changeUri(Landroid/net/Uri;Landroid/net/Uri;)V

    goto :goto_8
.end method

.method public changeUri(Landroid/net/Uri;Landroid/net/Uri;)V
    .registers 7
    .parameter "itemUri"
    .parameter "streamUri"

    .prologue
    .line 264
    if-nez p1, :cond_8

    .line 265
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 268
    :cond_8
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    .line 271
    if-eqz p2, :cond_2d

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItemPosition(J)I

    move-result v0

    .line 274
    .local v0, position:I
    const/4 v1, -0x2

    if-eq v0, v1, :cond_29

    .line 276
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 295
    .end local v0           #position:I
    .end local p2
    :goto_28
    return-void

    .line 280
    .restart local v0       #position:I
    .restart local p2
    :cond_29
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->changeQueryIfItemNotFound()V

    goto :goto_28

    .line 286
    .end local v0           #position:I
    :cond_2d
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/fragment/ItemFragment;->swapCursor(Landroid/database/Cursor;)V

    .line 291
    if-eqz p2, :cond_3b

    .end local p2
    :goto_33
    iput-object p2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/fragment/Loadable;->restartLoader()V

    goto :goto_28

    .restart local p2
    :cond_3b
    move-object p2, p1

    .line 291
    goto :goto_33
.end method

.method public getAccount()Lcom/google/android/accounts/Account;
    .registers 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Accounts;->getAccount(Landroid/net/Uri;)Lcom/google/android/accounts/Account;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getItemId()J
    .registers 3

    .prologue
    .line 710
    iget-wide v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    return-wide v0
.end method

.method public getItemUri()Landroid/net/Uri;
    .registers 5

    .prologue
    .line 714
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 715
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_11

    .line 716
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->itemUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v1

    .line 718
    :goto_10
    return-object v1

    :cond_11
    const/4 v1, 0x0

    goto :goto_10
.end method

.method public getItemsUri()Landroid/net/Uri;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 723
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_1d

    .line 724
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 725
    .local v0, type:Ljava/lang/String;
    sget-object v2, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 726
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    .line 735
    .end local v0           #type:Ljava/lang/String;
    :cond_1d
    return-object v1

    .line 731
    .restart local v0       #type:Ljava/lang/String;
    :cond_1e
    sget-boolean v2, Lcom/google/android/apps/reader/fragment/ItemFragment;->$assertionsDisabled:Z

    if-nez v2, :cond_1d

    sget-object v2, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public getStreamId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_b

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->getStreamId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 697
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method markRead()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 557
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 558
    .local v0, cursor:Landroid/database/Cursor;
    iget-boolean v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStreamLoading:Z

    if-eqz v1, :cond_11

    .line 560
    iput-boolean v4, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mMarkedRead:Z

    .line 575
    :goto_10
    return-void

    .line 561
    :cond_11
    if-eqz v0, :cond_2d

    .line 564
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->isUnread(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 565
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/reader/widget/ItemAdapter;->setSilent(Z)V

    .line 566
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->markRead(Landroid/database/Cursor;)V

    .line 567
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/reader/widget/ItemAdapter;->setSilent(Z)V

    .line 571
    :cond_2a
    iput-boolean v5, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mMarkedRead:Z

    goto :goto_10

    .line 573
    :cond_2d
    iput-boolean v4, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mMarkedRead:Z

    goto :goto_10
.end method

.method markReadIfNotMarkedRead()V
    .registers 2

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mMarkedRead:Z

    if-nez v0, :cond_7

    .line 579
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->markRead()V

    .line 581
    :cond_7
    return-void
.end method

.method public moveToNext()Z
    .registers 4

    .prologue
    .line 536
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 537
    .local v0, currentItem:I
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_19

    .line 538
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 539
    const/4 v1, 0x1

    .line 541
    :goto_18
    return v1

    :cond_19
    const/4 v1, 0x0

    goto :goto_18
.end method

.method public moveToPrevious()Z
    .registers 4

    .prologue
    .line 526
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 527
    .local v0, currentItem:I
    if-lez v0, :cond_11

    .line 528
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 529
    const/4 v1, 0x1

    .line 531
    :goto_10
    return v1

    :cond_11
    const/4 v1, 0x0

    goto :goto_10
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .registers 7
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    .line 486
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    if-ne p1, v1, :cond_13

    .line 487
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 488
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_13

    .line 489
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->setStarred(Landroid/database/Cursor;Z)V

    .line 492
    .end local v0           #cursor:Landroid/database/Cursor;
    :cond_13
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "v"

    .prologue
    .line 461
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_24

    .line 473
    :goto_7
    return-void

    .line 463
    :sswitch_8
    iget-object v3, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {v3}, Lcom/google/android/apps/reader/fragment/Loadable;->retry()V

    goto :goto_7

    .line 466
    :sswitch_e
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 467
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/reader/preference/LocalPreferences;->get(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 468
    .local v2, preferences:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 469
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "show_swipe_hint"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 470
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_7

    .line 461
    :sswitch_data_24
    .sparse-switch
        0x7f0b002f -> :sswitch_8
        0x7f0b0034 -> :sswitch_e
    .end sparse-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 5
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    sget-boolean v0, Lcom/google/android/apps/reader/fragment/ItemFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    if-eq v0, p1, :cond_d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 193
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->createLoader(Landroid/net/Uri;)Landroid/support/v4/content/Loader;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 4
    .parameter "menu"
    .parameter "inflater"

    .prologue
    .line 385
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 387
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .parameter "inflater"
    .parameter "root"
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 160
    const v2, 0x7f03001b

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 161
    .local v1, view:Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 163
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/reader/preference/LocalPreferences;->get(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mLocalPreferences:Landroid/content/SharedPreferences;

    .line 165
    new-instance v2, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v5}, Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;-><init>(Lcom/google/android/apps/reader/fragment/ItemFragment;Lcom/google/android/apps/reader/fragment/ItemFragment$1;)V

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;

    .line 166
    new-instance v2, Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-direct {v2, v0, v5}, Lcom/google/android/apps/reader/widget/ItemAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    .line 167
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$ItemDataSetObserver;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/reader/widget/ItemAdapter;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 169
    new-instance v2, Lcom/google/android/apps/reader/fragment/Loadable;

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/reader/fragment/ItemStateObserver;

    invoke-direct {v6, p0, v1, p0}, Lcom/google/android/apps/reader/fragment/ItemStateObserver;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;Landroid/view/View;Landroid/view/View$OnClickListener;)V

    invoke-direct {v2, v0, v5, v3, v6}, Lcom/google/android/apps/reader/fragment/Loadable;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;ILandroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    .line 171
    const v2, 0x7f0b003a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    .line 172
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v2, v5}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 173
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemView:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 175
    const v2, 0x7f0b0033

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mHintSwipe:Landroid/view/View;

    .line 176
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mHintSwipe:Landroid/view/View;

    const v5, 0x7f0b0034

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    invoke-virtual {p0, v3}, Lcom/google/android/apps/reader/fragment/ItemFragment;->setHasOptionsMenu(Z)V

    .line 180
    if-eqz p3, :cond_7b

    .line 181
    invoke-direct {p0, p3}, Lcom/google/android/apps/reader/fragment/ItemFragment;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 182
    iget-object v5, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItems:Lcom/google/android/apps/reader/fragment/Loadable;

    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_7c

    move v2, v3

    :goto_78
    invoke-virtual {v5, v2}, Lcom/google/android/apps/reader/fragment/Loadable;->initLoaderIf(Z)V

    .line 185
    :cond_7b
    return-object v1

    :cond_7c
    move v2, v4

    .line 182
    goto :goto_78
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v1, 0x1

    .line 495
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->isVolumeKeyNavigationEnabled()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 496
    packed-switch p1, :pswitch_data_36

    .line 505
    :cond_a
    sparse-switch p1, :sswitch_data_3e

    .line 516
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v3, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 517
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_34

    .line 518
    iget-object v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v2, v0, p1, p2}, Lcom/google/android/apps/reader/widget/ItemAdapter;->onKeyDown(Landroid/database/Cursor;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 522
    .end local v0           #cursor:Landroid/database/Cursor;
    :goto_1f
    return v1

    .line 498
    :pswitch_20
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->moveToNext()Z

    goto :goto_1f

    .line 501
    :pswitch_24
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->moveToPrevious()Z

    goto :goto_1f

    .line 507
    :sswitch_28
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->moveToNext()Z

    goto :goto_1f

    .line 510
    :sswitch_2c
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->moveToPrevious()Z

    goto :goto_1f

    .line 513
    :sswitch_30
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->moveToNextItemOrPage()Z

    goto :goto_1f

    .line 522
    .restart local v0       #cursor:Landroid/database/Cursor;
    :cond_34
    const/4 v1, 0x0

    goto :goto_1f

    .line 496
    :pswitch_data_36
    .packed-switch 0x18
        :pswitch_24
        :pswitch_20
    .end packed-switch

    .line 505
    :sswitch_data_3e
    .sparse-switch
        0x26 -> :sswitch_28
        0x27 -> :sswitch_2c
        0x3e -> :sswitch_30
    .end sparse-switch
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 5
    .parameter
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-boolean v0, Lcom/google/android/apps/reader/fragment/ItemFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_11

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    if-eq v0, v1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 201
    :cond_11
    invoke-direct {p0, p2}, Lcom/google/android/apps/reader/fragment/ItemFragment;->swapCursor(Landroid/database/Cursor;)V

    .line 202
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/reader/fragment/ItemFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-boolean v0, Lcom/google/android/apps/reader/fragment/ItemFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_11

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    if-eq v0, v1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 209
    :cond_11
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->swapCursor(Landroid/database/Cursor;)V

    .line 210
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 6
    .parameter "item"

    .prologue
    .line 398
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 399
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_23

    .line 400
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->onOptionsItemSelected(Landroid/view/MenuItem;Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 401
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_28

    .line 406
    :goto_19
    const/4 v1, 0x1

    .line 409
    :goto_1a
    return v1

    .line 403
    :pswitch_1b
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_19

    .line 409
    :cond_23
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_1a

    .line 401
    :pswitch_data_28
    .packed-switch 0x7f0b008e
        :pswitch_1b
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .registers 2
    .parameter "state"

    .prologue
    .line 422
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 4
    .parameter "position"
    .parameter "positionOffset"
    .parameter "positionOffsetPixels"

    .prologue
    .line 416
    return-void
.end method

.method public onPageSelected(I)V
    .registers 5
    .parameter "position"

    .prologue
    .line 428
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v0

    .line 429
    .local v0, cursor:Landroid/database/Cursor;
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getItemId(Landroid/database/Cursor;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    .line 430
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->markRead()V

    .line 431
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateViews()V

    .line 432
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->changeQueryIfItemNotFound()V

    .line 433
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->loadMoreIfNearEnd()V

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$Observer;

    if-eqz v1, :cond_23

    .line 435
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$Observer;

    invoke-interface {v1}, Lcom/google/android/apps/reader/fragment/ItemFragment$Observer;->onItemChanged()V

    .line 437
    :cond_23
    return-void
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mLocalPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->syncChanges()V

    .line 312
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 313
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 6
    .parameter "menu"

    .prologue
    .line 391
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 392
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 393
    .local v0, cursor:Landroid/database/Cursor;
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->onPrepareOptionsMenu(Landroid/view/Menu;Landroid/database/Cursor;)V

    .line 394
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 299
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mLocalPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateViews()V

    .line 304
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->onVolumeKeyNavigationPreferenceChanged()V

    .line 305
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 371
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 372
    const-string v0, "reader:uri"

    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 373
    const-string v0, "reader:item"

    iget-wide v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 374
    const-string v0, "reader:marked_read"

    iget-boolean v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mMarkedRead:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 375
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 4
    .parameter "sharedPreferences"
    .parameter "key"

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mLocalPreferences:Landroid/content/SharedPreferences;

    if-eq p1, v0, :cond_5

    .line 340
    :cond_4
    :goto_4
    return-void

    .line 335
    :cond_5
    const-string v0, "volume_key_navigation"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 336
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->onVolumeKeyNavigationPreferenceChanged()V

    goto :goto_4

    .line 337
    :cond_11
    const-string v0, "show_swipe_hint"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 338
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateSwipeHint()V

    goto :goto_4
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter "observer"

    .prologue
    .line 706
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mDataSetObservers:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 707
    return-void
.end method

.method public send()V
    .registers 5

    .prologue
    .line 476
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    iget-wide v2, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemId:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/reader/widget/ItemAdapter;->findItem(J)Landroid/database/Cursor;

    move-result-object v0

    .line 477
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_f

    .line 478
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/reader/widget/ItemAdapter;->sendItem(Landroid/database/Cursor;)V

    .line 480
    :cond_f
    return-void
.end method

.method public setObserver(Lcom/google/android/apps/reader/fragment/ItemFragment$Observer;)V
    .registers 2
    .parameter "observer"

    .prologue
    .line 702
    iput-object p1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mObserver:Lcom/google/android/apps/reader/fragment/ItemFragment$Observer;

    .line 703
    return-void
.end method

.method setStarChecked(Z)V
    .registers 4
    .parameter "starred"

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1c

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_1c

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 629
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 633
    :cond_1c
    return-void
.end method

.method public setStarToggle(Landroid/widget/CheckBox;)V
    .registers 4
    .parameter "toggle"

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    if-eqz v0, :cond_a

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 257
    :cond_a
    iput-object p1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    if-eqz v0, :cond_15

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStarToggle:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 261
    :cond_15
    return-void
.end method

.method public setStreamLoading(Z)V
    .registers 3
    .parameter "streamLoading"

    .prologue
    .line 587
    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStreamLoading:Z

    if-eq p1, v0, :cond_d

    .line 588
    iput-boolean p1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStreamLoading:Z

    .line 589
    iget-boolean v0, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mStreamLoading:Z

    if-nez v0, :cond_d

    .line 591
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->markReadIfNotMarkedRead()V

    .line 594
    :cond_d
    return-void
.end method

.method public showMainScreen()V
    .registers 3

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getAccount()Lcom/google/android/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/reader/fragment/MainScreen;->show(Landroid/content/Context;Lcom/google/android/accounts/Account;)V

    .line 741
    return-void
.end method

.method public updateProgress()V
    .registers 3

    .prologue
    .line 597
    iget-object v1, p0, Lcom/google/android/apps/reader/fragment/ItemFragment;->mItemAdapter:Lcom/google/android/apps/reader/widget/ItemAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/reader/widget/ItemAdapter;->getProgress()I

    move-result v1

    mul-int/lit16 v1, v1, 0x2710

    div-int/lit8 v0, v1, 0x64

    .line 598
    .local v0, value:I
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/reader/widget/ReaderWindow;->setProgress(Landroid/app/Activity;I)V

    .line 599
    return-void
.end method

.method updateViews()V
    .registers 1

    .prologue
    .line 549
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateViewPager()V

    .line 550
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateActionBar()V

    .line 551
    invoke-virtual {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateProgress()V

    .line 552
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateStarCheckBox()V

    .line 553
    invoke-direct {p0}, Lcom/google/android/apps/reader/fragment/ItemFragment;->updateSwipeHint()V

    .line 554
    return-void
.end method
