.class public Lcom/google/android/apps/reader/provider/ReaderContract$Streams;
.super Ljava/lang/Object;
.source "ReaderContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/google/android/apps/reader/provider/ReaderContract$StreamsColumns;
.implements Lcom/google/android/apps/reader/provider/ReaderContract$StreamsSortKeys;
.implements Lcom/google/android/apps/reader/provider/ReaderContract$SyncColumns;
.implements Lcom/google/android/apps/reader/provider/ReaderContract$UnreadCountsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/provider/ReaderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Streams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;
    }
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = null

.field public static final CONTENT_TYPE:Ljava/lang/String; = null

.field public static final EXTRA_DESCRIPTION:Ljava/lang/String; = "com.google.reader.cursor.extra.DESCRIPTION"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1414
    const-string v0, "stream"

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract;->contentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->CONTENT_TYPE:Ljava/lang/String;

    .line 1416
    const-string v0, "stream"

    invoke-static {v0}, Lcom/google/android/apps/reader/provider/ReaderContract;->contentItemType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->CONTENT_ITEM_TYPE:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 1475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1476
    return-void
.end method

.method public static contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;
    .registers 2
    .parameter "account"

    .prologue
    .line 1446
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams;->contentUri(Lcom/google/android/accounts/Account;Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static contentUri(Lcom/google/android/accounts/Account;Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;)Landroid/net/Uri;
    .registers 6
    .parameter "account"
    .parameter "streamFilter"

    .prologue
    .line 1432
    if-nez p0, :cond_a

    .line 1433
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Account is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1435
    :cond_a
    sget-object v2, Lcom/google/android/apps/reader/provider/ReaderContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1436
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v2, "streams"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1437
    const-string v2, "account_name"

    iget-object v3, p0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1438
    if-eqz p1, :cond_2b

    .line 1439
    invoke-virtual {p1}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 1440
    .local v1, value:Ljava/lang/String;
    const-string v2, "stream_filter"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1442
    .end local v1           #value:Ljava/lang/String;
    :cond_2b
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public static getFilter(Landroid/net/Uri;)Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;
    .registers 4
    .parameter "uri"

    .prologue
    .line 1464
    if-nez p0, :cond_a

    .line 1465
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "URI is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1467
    :cond_a
    const-string v1, "stream_filter"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1468
    .local v0, name:Ljava/lang/String;
    if-eqz v0, :cond_1b

    .line 1469
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;

    move-result-object v1

    .line 1471
    :goto_1a
    return-object v1

    :cond_1b
    sget-object v1, Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;->NONE:Lcom/google/android/apps/reader/provider/ReaderContract$Streams$Filter;

    goto :goto_1a
.end method

.method public static itemUri(Lcom/google/android/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;
    .registers 5
    .parameter "account"
    .parameter "streamId"

    .prologue
    .line 1450
    if-nez p0, :cond_a

    .line 1451
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Account is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1453
    :cond_a
    if-nez p1, :cond_14

    .line 1454
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Stream ID is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1456
    :cond_14
    sget-object v1, Lcom/google/android/apps/reader/provider/ReaderContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1457
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "streams"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1458
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1459
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1460
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method
