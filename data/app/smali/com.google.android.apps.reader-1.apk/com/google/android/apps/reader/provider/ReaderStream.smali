.class public final Lcom/google/android/apps/reader/provider/ReaderStream;
.super Ljava/lang/Object;
.source "ReaderStream.java"


# static fields
.field public static final CATEGORY_BROADCAST:Ljava/lang/String; = "user/-/state/com.google/broadcast"

.field public static final CATEGORY_CREATED:Ljava/lang/String; = "user/-/state/com.google/created"

.field public static final CATEGORY_FOLLOWING:Ljava/lang/String; = "user/-/state/com.google/broadcast-friends"

.field public static final CATEGORY_LIKE:Ljava/lang/String; = "user/-/state/com.google/like"

.field public static final CATEGORY_LINK:Ljava/lang/String; = "user/-/source/com.google/link"

.field public static final CATEGORY_POST:Ljava/lang/String; = "user/-/source/com.google/post"

.field public static final CATEGORY_READ:Ljava/lang/String; = "user/-/state/com.google/read"

.field public static final CATEGORY_READING_LIST:Ljava/lang/String; = "user/-/state/com.google/reading-list"

.field public static final CATEGORY_ROOT:Ljava/lang/String; = "user/-/state/com.google/root"

.field public static final CATEGORY_SELF:Ljava/lang/String; = "user/-/state/com.google/self"

.field public static final CATEGORY_STARRED:Ljava/lang/String; = "user/-/state/com.google/starred"

.field private static final INFIX_LABEL:Ljava/lang/String; = "/label/"

.field private static final INFIX_LANGUAGE:Ljava/lang/String; = "/language/"

.field private static final INFIX_RECOMMENDATIONS:Ljava/lang/String; = "/state/com.google/itemrecs/"

.field private static final PATTERN_ILLEGAL:Ljava/util/regex/Pattern; = null

.field private static final PATTERN_WHITESPACE:Ljava/util/regex/Pattern; = null

.field private static final PREFIX_POPULAR:Ljava/lang/String; = "pop/topic/"

.field private static final PREFIX_SPLICE:Ljava/lang/String; = "splice/"

.field private static final PREFIX_SUBSCRIPTION:Ljava/lang/String; = "feed/"

.field private static final PREFIX_USER:Ljava/lang/String; = "user/"

.field private static final PREFIX_WEBFEED:Ljava/lang/String; = "webfeed/"

.field private static final SUFFIX_BROADCAST:Ljava/lang/String; = "/state/com.google/broadcast"

.field private static final SUFFIX_CREATED:Ljava/lang/String; = "/state/com.google/created"

.field private static final SUFFIX_FOLLOWING:Ljava/lang/String; = "/state/com.google/broadcast-friends"

.field private static final SUFFIX_FRESH:Ljava/lang/String; = "/state/com.google/fresh"

.field private static final SUFFIX_LIKE:Ljava/lang/String; = "/state/com.google/like"

.field private static final SUFFIX_MY_STUFF:Ljava/lang/String; = "/state/com.google/self"

.field private static final SUFFIX_NOTE:Ljava/lang/String; = "/source/com.google/link"

.field private static final SUFFIX_READ:Ljava/lang/String; = "/state/com.google/read"

.field private static final SUFFIX_READING_LIST:Ljava/lang/String; = "/state/com.google/reading-list"

.field private static final SUFFIX_ROOT:Ljava/lang/String; = "/state/com.google/root"

.field private static final SUFFIX_STARRED:Ljava/lang/String; = "/state/com.google/starred"

.field public static final TOPIC_TOP:Ljava/lang/String; = "top"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 89
    const-string v0, "[\"^<>?&\\\\/,]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/provider/ReaderStream;->PATTERN_ILLEGAL:Ljava/util/regex/Pattern;

    .line 91
    const-string v0, "\\s+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/reader/provider/ReaderStream;->PATTERN_WHITESPACE:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    return-void
.end method

.method public static canShowReadStateWhenUnsubscribed(Ljava/lang/String;)Z
    .registers 2
    .parameter "streamId"

    .prologue
    .line 371
    if-eqz p0, :cond_14

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isPopular(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isSplice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isRecommendation(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public static createBroadcastId(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "userId"

    .prologue
    .line 185
    if-nez p0, :cond_a

    .line 186
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "User ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "user/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/state/com.google/broadcast"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createPopularStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "topic"
    .parameter "language"

    .prologue
    .line 203
    if-nez p0, :cond_a

    .line 204
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Topic is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_a
    if-nez p1, :cond_14

    .line 207
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Language is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pop/topic/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/language/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createRecommendationsStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "userId"
    .parameter "language"

    .prologue
    .line 213
    if-nez p0, :cond_a

    .line 214
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "User ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_a
    if-nez p1, :cond_14

    .line 217
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Language is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "user/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/state/com.google/itemrecs/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs createSpliceStreamId([Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "streamIds"

    .prologue
    .line 456
    if-nez p0, :cond_8

    .line 457
    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 459
    :cond_8
    array-length v3, p0

    const/4 v4, 0x1

    if-ge v3, v4, :cond_12

    .line 460
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    .line 462
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "splice/"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 463
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1a
    array-length v3, p0

    if-ge v1, v3, :cond_34

    .line 464
    if-eqz v1, :cond_24

    .line 465
    const/16 v3, 0x7c

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 468
    :cond_24
    aget-object v2, p0, v1

    .line 469
    .local v2, streamId:Ljava/lang/String;
    if-nez v2, :cond_2e

    .line 470
    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 472
    :cond_2e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    .line 474
    .end local v2           #streamId:Ljava/lang/String;
    :cond_34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static createSplicedRecommendationsStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "userId"
    .parameter "language"

    .prologue
    .line 223
    if-nez p0, :cond_a

    .line 224
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "User ID is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 226
    :cond_a
    if-nez p1, :cond_14

    .line 227
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Language is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 229
    :cond_14
    invoke-static {p0, p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->createRecommendationsStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 230
    .local v1, recommendations:Ljava/lang/String;
    const-string v2, "top"

    invoke-static {v2, p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->createPopularStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, popular:Ljava/lang/String;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/google/android/apps/reader/provider/ReaderStream;->createSpliceStreamId([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static createSubscriptionId(Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter "uri"

    .prologue
    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "feed/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createTagId(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "label"

    .prologue
    .line 192
    const-string v0, "-"

    invoke-static {v0, p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->createTagId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createTagId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "userId"
    .parameter "label"

    .prologue
    .line 175
    if-nez p0, :cond_a

    .line 176
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "User ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_a
    if-nez p1, :cond_14

    .line 179
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Label is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "user/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/label/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->sanitizeTagName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLabel(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "streamId"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 105
    const-string v1, "user/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 106
    const-string v1, "/label/"

    const-string v2, "user/"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 107
    .local v0, index:I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_22

    .line 108
    const-string v1, "/label/"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 111
    .end local v0           #index:I
    .end local p0
    :cond_22
    return-object p0
.end method

.method public static getReadItemsVisible(Ljava/lang/String;)Z
    .registers 2
    .parameter "streamId"

    .prologue
    .line 482
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isStarred(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isMyStuff(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isNotes(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public static getSubscriptionUri(Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "streamId"

    .prologue
    .line 115
    if-nez p0, :cond_a

    .line 116
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_a
    const-string v0, "feed/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 119
    const-string v0, "feed/"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 121
    :cond_21
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a subscription ID"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static hasReadState(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 243
    if-nez p0, :cond_a

    .line 244
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_a
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isLabel(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isSubscription(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isWebFeed(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isStarred(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isReadingList(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isFollowing(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->isBroadcast(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    :cond_34
    const/4 v0, 0x1

    :goto_35
    return v0

    :cond_36
    const/4 v0, 0x0

    goto :goto_35
.end method

.method public static isBroadcast(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 287
    if-nez p0, :cond_a

    .line 288
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_a
    const-string v0, "user/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const-string v0, "/state/com.google/broadcast"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public static isFollowing(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 350
    if-nez p0, :cond_a

    .line 351
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353
    :cond_a
    const-string v0, "/state/com.google/broadcast-friends"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isFresh(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 315
    if-nez p0, :cond_a

    .line 316
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_a
    const-string v0, "/state/com.google/fresh"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isLabel(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 336
    if-nez p0, :cond_a

    .line 337
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :cond_a
    const-string v0, "user/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const-string v0, "/label/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1d

    const/4 v0, 0x1

    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method public static isLiked(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 294
    if-nez p0, :cond_a

    .line 295
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :cond_a
    const-string v0, "/state/com.google/like"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isMyStuff(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 252
    if-nez p0, :cond_a

    .line 253
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_a
    const-string v0, "/state/com.google/self"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isNote(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 259
    if-nez p0, :cond_a

    .line 260
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_a
    const-string v0, "/source/com.google/link"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isNotes(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 273
    if-nez p0, :cond_a

    .line 274
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_a
    const-string v0, "/state/com.google/created"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isPopular(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 357
    if-nez p0, :cond_a

    .line 358
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360
    :cond_a
    const-string v0, "pop/topic/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isRead(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 308
    if-nez p0, :cond_a

    .line 309
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_a
    const-string v0, "/state/com.google/read"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isReadingList(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 280
    if-nez p0, :cond_a

    .line 281
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_a
    const-string v0, "/state/com.google/reading-list"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isRecommendation(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 343
    if-nez p0, :cond_a

    .line 344
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_a
    const-string v0, "user/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const-string v0, "/state/com.google/itemrecs/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1d

    const/4 v0, 0x1

    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method public static isRoot(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 266
    if-nez p0, :cond_a

    .line 267
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_a
    const-string v0, "/state/com.google/root"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isSplice(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 364
    if-nez p0, :cond_a

    .line 365
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_a
    const-string v0, "splice/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isStarred(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 301
    if-nez p0, :cond_a

    .line 302
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_a
    const-string v0, "/state/com.google/starred"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isSubscription(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 322
    if-nez p0, :cond_a

    .line 323
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :cond_a
    const-string v0, "feed/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isUserIdMissing(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 425
    if-nez p0, :cond_a

    .line 426
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_a
    const-string v0, "user/-/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isWebFeed(Ljava/lang/String;)Z
    .registers 3
    .parameter "streamId"

    .prologue
    .line 329
    if-nez p0, :cond_a

    .line 330
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_a
    const-string v0, "webfeed/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static normalizeLabelName(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "labelName"

    .prologue
    .line 151
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->stripIllegalControlCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 153
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderStream;->PATTERN_WHITESPACE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 155
    return-object p0
.end method

.method private static sanitizeTagName(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "tag"

    .prologue
    .line 164
    invoke-static {p0}, Lcom/google/android/apps/reader/provider/ReaderStream;->normalizeLabelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 167
    sget-object v0, Lcom/google/android/apps/reader/provider/ReaderStream;->PATTERN_ILLEGAL:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 171
    return-object p0
.end method

.method public static setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "streamId"
    .parameter "userId"

    .prologue
    .line 384
    if-nez p0, :cond_a

    .line 385
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Stream ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_a
    if-nez p1, :cond_14

    .line 388
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "User ID is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 390
    :cond_14
    const-string v0, "user/-/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "user/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "user/-"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 393
    .end local p0
    :cond_3d
    return-object p0
.end method

.method public static setUserId(Ljava/util/List;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 405
    .local p0, streamIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p0, :cond_a

    .line 406
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Stream ID list is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 408
    :cond_a
    if-nez p1, :cond_14

    .line 409
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "User ID is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 411
    :cond_14
    const/4 v0, 0x0

    .local v0, i:I
    :goto_15
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2b

    .line 412
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 413
    .local v1, streamId:Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/google/android/apps/reader/provider/ReaderStream;->setUserId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 414
    invoke-interface {p0, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 411
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 416
    .end local v1           #streamId:Ljava/lang/String;
    :cond_2b
    return-void
.end method

.method private static stripIllegalControlCharacters(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "s"

    .prologue
    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .local v2, sb:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_26

    .line 133
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 134
    .local v0, c:C
    const/16 v3, 0x20

    if-ge v0, v3, :cond_20

    const/16 v3, 0x9

    if-eq v0, v3, :cond_20

    const/16 v3, 0xa

    if-eq v0, v3, :cond_20

    const/16 v3, 0xd

    if-ne v0, v3, :cond_23

    .line 135
    :cond_20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 132
    :cond_23
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 139
    .end local v0           #c:C
    :cond_26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static unsetUserId(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "streamId"

    .prologue
    .line 439
    if-nez p0, :cond_a

    .line 440
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Stream ID is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 442
    :cond_a
    const-string v2, "user/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_49

    const-string v2, "user/-/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_49

    .line 443
    const-string v2, "user/"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 444
    .local v1, start:I
    const/16 v2, 0x2f

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 445
    .local v0, index:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_49

    .line 446
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 449
    .end local v0           #index:I
    .end local v1           #start:I
    .end local p0
    :cond_49
    return-object p0
.end method
