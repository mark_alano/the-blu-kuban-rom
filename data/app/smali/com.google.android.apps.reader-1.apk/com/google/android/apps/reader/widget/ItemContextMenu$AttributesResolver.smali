.class Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "ItemContextMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/widget/ItemContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AttributesResolver"
.end annotation


# instance fields
.field private final mAttributeName:Ljava/lang/String;

.field private final mAttributeValue:Ljava/lang/String;

.field private mAttributes:Lorg/xml/sax/Attributes;

.field private final mTagName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "tagName"
    .parameter "attributeName"
    .parameter "attributeValue"

    .prologue
    .line 541
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 542
    if-nez p1, :cond_d

    .line 543
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Tag name is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 545
    :cond_d
    if-nez p2, :cond_17

    .line 546
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Attribute name is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548
    :cond_17
    if-nez p3, :cond_21

    .line 549
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Attribute value is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 551
    :cond_21
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mTagName:Ljava/lang/String;

    .line 552
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mAttributeName:Ljava/lang/String;

    .line 553
    iput-object p3, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mAttributeValue:Ljava/lang/String;

    .line 554
    return-void
.end method

.method public static findAttributes(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xml/sax/Attributes;
    .registers 7
    .parameter "in"
    .parameter "enc"
    .parameter "tag"
    .parameter "attribute"
    .parameter "value"

    .prologue
    .line 502
    :try_start_0
    new-instance v0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    .local v0, handler:Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->parse(Ljava/io/InputStream;Ljava/lang/String;Lorg/xml/sax/ContentHandler;)V

    .line 504
    iget-object v1, v0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mAttributes:Lorg/xml/sax/Attributes;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_b

    .line 506
    .end local v0           #handler:Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;
    :goto_a
    return-object v1

    :catch_b
    move-exception v1

    const/4 v1, 0x0

    goto :goto_a
.end method

.method private static parse(Ljava/io/InputStream;Ljava/lang/String;Lorg/xml/sax/ContentHandler;)V
    .registers 12
    .parameter "in"
    .parameter "enc"
    .parameter "handler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 524
    const-string v4, "org.ccil.cowan.tagsoup.Parser"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 525
    .local v2, parser:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-string v4, "parse"

    new-array v5, v8, [Ljava/lang/Class;

    const-class v6, Lorg/xml/sax/InputSource;

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 526
    .local v1, parse:Ljava/lang/reflect/Method;
    const-string v4, "setContentHandler"

    new-array v5, v8, [Ljava/lang/Class;

    const-class v6, Lorg/xml/sax/ContentHandler;

    aput-object v6, v5, v7

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 528
    .local v3, setContentHandler:Ljava/lang/reflect/Method;
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    .line 529
    .local v0, instance:Ljava/lang/Object;
    new-array v4, v8, [Ljava/lang/Object;

    aput-object p2, v4, v7

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    new-array v4, v8, [Ljava/lang/Object;

    new-instance v5, Lorg/xml/sax/InputSource;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, p0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    aput-object v5, v4, v7

    invoke-virtual {v1, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    return-void
.end method


# virtual methods
.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .registers 7
    .parameter "uri"
    .parameter "localName"
    .parameter "qualifiedName"
    .parameter "attributes"

    .prologue
    .line 559
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mTagName:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 560
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mAttributeName:Ljava/lang/String;

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 561
    .local v0, value:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mAttributeValue:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 562
    iput-object p4, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$AttributesResolver;->mAttributes:Lorg/xml/sax/Attributes;

    .line 565
    .end local v0           #value:Ljava/lang/String;
    :cond_18
    return-void
.end method
