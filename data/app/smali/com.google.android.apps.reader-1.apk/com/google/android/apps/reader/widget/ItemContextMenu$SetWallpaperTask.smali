.class Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;
.super Landroid/os/AsyncTask;
.source "ItemContextMenu.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/widget/ItemContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SetWallpaperTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Landroid/content/DialogInterface$OnCancelListener;",
        "Landroid/view/MenuItem$OnMenuItemClickListener;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mUrl:Ljava/lang/String;

.field private final mWallpaperProgress:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "url"

    .prologue
    const/4 v2, 0x1

    .line 321
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 322
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mContext:Landroid/content/Context;

    .line 323
    iput-object p2, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mUrl:Ljava/lang/String;

    .line 325
    const v1, 0x7f0d0127

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 326
    .local v0, message:Ljava/lang/CharSequence;
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    .line 327
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1, p0}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 331
    return-void
.end method

.method private getWallpaper()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getWallpaper()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private restoreWallpaper(Landroid/graphics/drawable/Drawable;)Z
    .registers 10
    .parameter "wallpaper"

    .prologue
    const/4 v5, 0x0

    .line 364
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 365
    .local v4, width:I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 366
    .local v3, height:I
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v3, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 367
    .local v0, bm:Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 368
    .local v1, canvas:Landroid/graphics/Canvas;
    invoke-virtual {p1, v5, v5, v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 369
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 371
    :try_start_1a
    iget-object v6, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v0}, Landroid/content/Context;->setWallpaper(Landroid/graphics/Bitmap;)V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1f} :catch_21

    .line 372
    const/4 v5, 0x1

    .line 375
    :goto_20
    return v5

    .line 373
    :catch_21
    move-exception v2

    .line 374
    .local v2, e:Ljava/io/IOException;
    const-string v6, "WebViewContextMenu"

    const-string v7, "Failed to restore wallpaper"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_20
.end method

.method private setWallpaper(Ljava/lang/String;)Z
    .registers 6
    .parameter "url"

    .prologue
    .line 349
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_8} :catch_18

    move-result-object v1

    .line 351
    .local v1, in:Ljava/io/InputStream;
    :try_start_9
    iget-object v2, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->setWallpaper(Ljava/io/InputStream;)V
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_13

    .line 353
    :try_start_e
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 355
    const/4 v2, 0x1

    .line 358
    .end local v1           #in:Ljava/io/InputStream;
    :goto_12
    return v2

    .line 353
    .restart local v1       #in:Ljava/io/InputStream;
    :catchall_13
    move-exception v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v2
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_18} :catch_18

    .line 356
    .end local v1           #in:Ljava/io/InputStream;
    :catch_18
    move-exception v0

    .line 357
    .local v0, e:Ljava/io/IOException;
    const-string v2, "WebViewContextMenu"

    const-string v3, "Failed to set wallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 358
    const/4 v2, 0x0

    goto :goto_12
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 312
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .registers 4
    .parameter "params"

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->getWallpaper()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 395
    .local v0, wallpaper:Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mUrl:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->setWallpaper(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 396
    :cond_12
    invoke-direct {p0, v0}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->restoreWallpaper(Landroid/graphics/drawable/Drawable;)Z

    .line 398
    :cond_15
    const/4 v1, 0x0

    return-object v1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "dialog"

    .prologue
    .line 384
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->cancel(Z)Z

    .line 385
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    .line 337
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 338
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 312
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .registers 3
    .parameter "result"

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 406
    :cond_d
    return-void
.end method

.method protected onPreExecute()V
    .registers 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$SetWallpaperTask;->mWallpaperProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 390
    return-void
.end method
