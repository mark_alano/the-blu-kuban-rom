.class Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;
.super Ljava/lang/Object;
.source "ItemContextMenu.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/reader/widget/ItemContextMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewCaptionListener"
.end annotation


# instance fields
.field private final mAlt:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mSrc:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "src"
    .parameter "title"
    .parameter "alt"

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    if-nez p1, :cond_b

    .line 247
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 249
    :cond_b
    if-nez p2, :cond_13

    .line 250
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 252
    :cond_13
    iput-object p1, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mContext:Landroid/content/Context;

    .line 253
    iput-object p2, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mSrc:Ljava/lang/String;

    .line 254
    iput-object p3, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mTitle:Ljava/lang/String;

    .line 255
    iput-object p4, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mAlt:Ljava/lang/String;

    .line 256
    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .registers 16
    .parameter "item"

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 262
    iget-object v4, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mContext:Landroid/content/Context;

    .line 263
    .local v4, context:Landroid/content/Context;
    iget-object v9, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mSrc:Ljava/lang/String;

    .line 264
    .local v9, src:Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mTitle:Ljava/lang/String;

    .line 265
    .local v10, title:Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/reader/widget/ItemContextMenu$ViewCaptionListener;->mAlt:Ljava/lang/String;

    .line 266
    .local v0, alt:Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_44

    move v8, v11

    .line 267
    .local v8, hasTitle:Z
    :goto_11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_46

    move v6, v11

    .line 268
    .local v6, hasAlt:Z
    :goto_18
    if-nez v8, :cond_1c

    if-eqz v6, :cond_43

    .line 269
    :cond_1c
    if-eqz v6, :cond_48

    if-eqz v8, :cond_48

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_48

    move v7, v11

    .line 270
    .local v7, hasShortAlt:Z
    :goto_27
    if-eqz v7, :cond_4a

    move-object v3, v0

    .line 271
    .local v3, captionTitle:Ljava/lang/String;
    :goto_2a
    if-eqz v8, :cond_4c

    move-object v2, v10

    .line 272
    .local v2, captionText:Ljava/lang/String;
    :goto_2d
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 273
    .local v1, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 274
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 275
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    .line 276
    .local v5, dialog:Landroid/app/Dialog;
    invoke-virtual {v5, v11}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 277
    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    move v12, v11

    .line 280
    .end local v1           #builder:Landroid/app/AlertDialog$Builder;
    .end local v2           #captionText:Ljava/lang/String;
    .end local v3           #captionTitle:Ljava/lang/String;
    .end local v5           #dialog:Landroid/app/Dialog;
    .end local v7           #hasShortAlt:Z
    :cond_43
    return v12

    .end local v6           #hasAlt:Z
    .end local v8           #hasTitle:Z
    :cond_44
    move v8, v12

    .line 266
    goto :goto_11

    .restart local v8       #hasTitle:Z
    :cond_46
    move v6, v12

    .line 267
    goto :goto_18

    .restart local v6       #hasAlt:Z
    :cond_48
    move v7, v12

    .line 269
    goto :goto_27

    .restart local v7       #hasShortAlt:Z
    :cond_4a
    move-object v3, v9

    .line 270
    goto :goto_2a

    .restart local v3       #captionTitle:Ljava/lang/String;
    :cond_4c
    move-object v2, v0

    .line 271
    goto :goto_2d
.end method
