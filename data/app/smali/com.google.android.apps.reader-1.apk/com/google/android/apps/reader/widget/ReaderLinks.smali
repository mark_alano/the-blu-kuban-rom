.class public final Lcom/google/android/apps/reader/widget/ReaderLinks;
.super Ljava/lang/Object;
.source "ReaderLinks.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/reader/widget/ReaderLinks$IntentSpan;
    }
.end annotation


# static fields
.field private static final ADD:Ljava/lang/String; = "#add"

.field private static final POP:Ljava/lang/String; = "#pop"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method private static getLanguage(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "context"

    .prologue
    .line 111
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 112
    .local v2, resources:Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 113
    .local v0, configuration:Landroid/content/res/Configuration;
    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, language:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_15

    .line 117
    .end local v1           #language:Ljava/lang/String;
    :goto_14
    return-object v1

    .restart local v1       #language:Ljava/lang/String;
    :cond_15
    const-string v1, "en"

    goto :goto_14
.end method

.method public static linkify(Landroid/widget/TextView;ILcom/google/android/accounts/Account;)V
    .registers 18
    .parameter "textView"
    .parameter "text"
    .parameter "account"

    .prologue
    .line 62
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 63
    .local v2, context:Landroid/content/Context;
    new-instance v8, Landroid/text/SpannableString;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-direct {v8, v12}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 64
    .local v8, spannable:Landroid/text/Spannable;
    const/4 v12, 0x0

    invoke-interface {v8}, Landroid/text/Spannable;->length()I

    move-result v13

    const-class v14, Landroid/text/style/URLSpan;

    invoke-interface {v8, v12, v13, v14}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/URLSpan;

    .local v1, arr$:[Landroid/text/style/URLSpan;
    array-length v6, v1

    .local v6, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_1e
    if-ge v3, v6, :cond_6f

    aget-object v7, v1, v3

    .line 65
    .local v7, span:Landroid/text/style/URLSpan;
    invoke-virtual {v7}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v11

    .line 66
    .local v11, url:Ljava/lang/String;
    const-string v12, "#add"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_44

    .line 67
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/reader/provider/ReaderContract$Subscriptions;->contentUri(Lcom/google/android/accounts/Account;)Landroid/net/Uri;

    move-result-object v10

    .line 68
    .local v10, uri:Landroid/net/Uri;
    new-instance v4, Landroid/content/Intent;

    const-string v12, "android.intent.action.INSERT"

    invoke-direct {v4, v12, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 69
    .local v4, intent:Landroid/content/Intent;
    new-instance v12, Lcom/google/android/apps/reader/widget/ReaderLinks$IntentSpan;

    invoke-direct {v12, v4}, Lcom/google/android/apps/reader/widget/ReaderLinks$IntentSpan;-><init>(Landroid/content/Intent;)V

    invoke-static {v8, v7, v12}, Lcom/google/android/apps/reader/widget/ReaderLinks;->replaceSpan(Landroid/text/Spannable;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64
    .end local v4           #intent:Landroid/content/Intent;
    .end local v10           #uri:Landroid/net/Uri;
    :cond_41
    :goto_41
    add-int/lit8 v3, v3, 0x1

    goto :goto_1e

    .line 70
    :cond_44
    const-string v12, "#pop"

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_41

    .line 71
    invoke-static {v2}, Lcom/google/android/apps/reader/widget/ReaderLinks;->getLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, lang:Ljava/lang/String;
    const-string v12, "top"

    invoke-static {v12, v5}, Lcom/google/android/apps/reader/provider/ReaderStream;->createPopularStreamId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 73
    .local v9, streamId:Ljava/lang/String;
    const/4 v12, 0x1

    const-string v13, "auto"

    move-object/from16 v0, p2

    invoke-static {v0, v9, v12, v13}, Lcom/google/android/apps/reader/provider/ReaderContract$Items;->streamUri(Lcom/google/android/accounts/Account;Ljava/lang/String;ZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 74
    .restart local v10       #uri:Landroid/net/Uri;
    new-instance v4, Landroid/content/Intent;

    const-string v12, "android.intent.action.VIEW"

    invoke-direct {v4, v12, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 75
    .restart local v4       #intent:Landroid/content/Intent;
    new-instance v12, Lcom/google/android/apps/reader/widget/ReaderLinks$IntentSpan;

    invoke-direct {v12, v4}, Lcom/google/android/apps/reader/widget/ReaderLinks$IntentSpan;-><init>(Landroid/content/Intent;)V

    invoke-static {v8, v7, v12}, Lcom/google/android/apps/reader/widget/ReaderLinks;->replaceSpan(Landroid/text/Spannable;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_41

    .line 80
    .end local v4           #intent:Landroid/content/Intent;
    .end local v5           #lang:Ljava/lang/String;
    .end local v7           #span:Landroid/text/style/URLSpan;
    .end local v9           #streamId:Ljava/lang/String;
    .end local v10           #uri:Landroid/net/Uri;
    .end local v11           #url:Ljava/lang/String;
    :cond_6f
    invoke-virtual {p0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v12

    invoke-virtual {p0, v12}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 86
    const/4 v12, 0x0

    invoke-virtual {p0, v12}, Landroid/widget/TextView;->setSaveEnabled(Z)V

    .line 87
    return-void
.end method

.method private static replaceSpan(Landroid/text/Spannable;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter "spannable"
    .parameter "target"
    .parameter "replacement"

    .prologue
    .line 97
    invoke-interface {p0, p1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    .line 98
    .local v2, start:I
    invoke-interface {p0, p1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    .line 99
    .local v0, end:I
    invoke-interface {p0, p1}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v1

    .line 100
    .local v1, flags:I
    invoke-interface {p0, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 101
    invoke-interface {p0, p2, v2, v0, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 102
    return-void
.end method
