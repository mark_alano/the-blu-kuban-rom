.class public final Lcom/google/android/apps/reader/widget/ReaderViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "ReaderViewPager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected canScroll(Landroid/view/View;ZIII)Z
    .registers 9
    .parameter "v"
    .parameter "checkV"
    .parameter "dx"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 41
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_1a

    instance-of v1, p1, Lcom/google/android/apps/reader/widget/ScrollableView;

    if-eqz v1, :cond_1a

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/google/android/apps/reader/widget/ScrollableView;

    .line 43
    .local v0, scrollable:Lcom/google/android/apps/reader/widget/ScrollableView;
    if-eqz p2, :cond_18

    neg-int v1, p3

    invoke-interface {v0, v1}, Lcom/google/android/apps/reader/widget/ScrollableView;->isHorizontallyScrollable(I)Z

    move-result v1

    if-eqz v1, :cond_18

    const/4 v1, 0x1

    .line 45
    .end local v0           #scrollable:Lcom/google/android/apps/reader/widget/ScrollableView;
    :goto_17
    return v1

    .line 43
    .restart local v0       #scrollable:Lcom/google/android/apps/reader/widget/ScrollableView;
    :cond_18
    const/4 v1, 0x0

    goto :goto_17

    .line 45
    .end local v0           #scrollable:Lcom/google/android/apps/reader/widget/ScrollableView;
    :cond_1a
    invoke-super/range {p0 .. p5}, Landroid/support/v4/view/ViewPager;->canScroll(Landroid/view/View;ZIII)Z

    move-result v1

    goto :goto_17
.end method
