.class public final Lcom/google/android/apps/reader/widget/ReaderWebView;
.super Landroid/webkit/WebView;
.source "ReaderWebView.java"

# interfaces
.implements Lcom/google/android/apps/reader/widget/ScrollableView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public isHorizontallyScrollable(I)Z
    .registers 8
    .parameter "direction"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ReaderWebView;->computeHorizontalScrollOffset()I

    move-result v0

    .line 45
    .local v0, offset:I
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ReaderWebView;->computeHorizontalScrollRange()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ReaderWebView;->computeHorizontalScrollExtent()I

    move-result v5

    sub-int v1, v4, v5

    .line 46
    .local v1, range:I
    if-nez v1, :cond_14

    move v2, v3

    .line 51
    :cond_13
    :goto_13
    return v2

    .line 48
    :cond_14
    if-gez p1, :cond_1a

    .line 49
    if-gtz v0, :cond_13

    move v2, v3

    goto :goto_13

    .line 51
    :cond_1a
    add-int/lit8 v4, v1, -0x1

    if-lt v0, v4, :cond_13

    move v2, v3

    goto :goto_13
.end method

.method public isVerticallyScrollable(I)Z
    .registers 8
    .parameter "direction"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ReaderWebView;->computeVerticalScrollOffset()I

    move-result v0

    .line 60
    .local v0, offset:I
    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ReaderWebView;->computeVerticalScrollRange()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/reader/widget/ReaderWebView;->computeVerticalScrollExtent()I

    move-result v5

    sub-int v1, v4, v5

    .line 61
    .local v1, range:I
    if-nez v1, :cond_14

    move v2, v3

    .line 66
    :cond_13
    :goto_13
    return v2

    .line 63
    :cond_14
    if-gez p1, :cond_1a

    .line 64
    if-gtz v0, :cond_13

    move v2, v3

    goto :goto_13

    .line 66
    :cond_1a
    add-int/lit8 v4, v1, -0x1

    if-lt v0, v4, :cond_13

    move v2, v3

    goto :goto_13
.end method
