.class public Lcom/google/android/feeds/FeedLoader;
.super Ljava/lang/Object;
.source "FeedLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/feeds/FeedLoader$DocumentInfo;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319
    return-void
.end method

.method public static documentInfo()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 270
    new-instance v0, Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    invoke-direct {v0}, Lcom/google/android/feeds/FeedLoader$DocumentInfo;-><init>()V

    return-object v0
.end method

.method public static documentInfo(I)Ljava/lang/Object;
    .registers 2
    .parameter "itemCount"

    .prologue
    .line 293
    new-instance v0, Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    invoke-direct {v0, p0}, Lcom/google/android/feeds/FeedLoader$DocumentInfo;-><init>(I)V

    return-object v0
.end method

.method public static documentInfo(ILjava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "itemCount"
    .parameter "continuationToken"

    .prologue
    .line 315
    new-instance v0, Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    invoke-direct {v0, p0, p1}, Lcom/google/android/feeds/FeedLoader$DocumentInfo;-><init>(ILjava/lang/String;)V

    return-object v0
.end method

.method public static loadContinuedFeed(Ljava/net/ContentHandler;Landroid/net/Uri;Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 12
    .parameter "handler"
    .parameter "baseDocumentUri"
    .parameter "continuationParameter"
    .parameter "itemCount"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    const/4 v4, 0x0

    .line 242
    .local v4, totalCount:I
    const/4 v0, 0x0

    .line 244
    .local v0, continuation:Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 245
    .local v3, documentUri:Landroid/net/Uri$Builder;
    if-eqz v0, :cond_b

    .line 246
    invoke-virtual {v3, p2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 248
    :cond_b
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/google/android/feeds/FeedLoader;->loadDocument(Ljava/net/ContentHandler;Landroid/net/Uri;)Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    move-result-object v1

    .line 250
    .local v1, document:Lcom/google/android/feeds/FeedLoader$DocumentInfo;
    invoke-virtual {v1}, Lcom/google/android/feeds/FeedLoader$DocumentInfo;->continuationToken()Ljava/lang/String;

    move-result-object v0

    .line 251
    const-string v6, "com.google.feeds.cursor.extra.MORE"

    if-eqz v0, :cond_2d

    const/4 v5, 0x1

    :goto_1c
    invoke-virtual {p4, v6, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 253
    invoke-virtual {v1}, Lcom/google/android/feeds/FeedLoader$DocumentInfo;->itemCount()I

    move-result v2

    .line 254
    .local v2, documentItemCount:I
    if-gez v2, :cond_2f

    .line 255
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Invalid document info: item count is unset or invalid"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 251
    .end local v2           #documentItemCount:I
    :cond_2d
    const/4 v5, 0x0

    goto :goto_1c

    .line 257
    .restart local v2       #documentItemCount:I
    :cond_2f
    add-int/2addr v4, v2

    .line 258
    if-ge v4, p3, :cond_34

    if-nez v0, :cond_2

    .line 259
    :cond_34
    return-void
.end method

.method private static loadDocument(Ljava/net/ContentHandler;Landroid/net/Uri;)Lcom/google/android/feeds/FeedLoader$DocumentInfo;
    .registers 8
    .parameter "handler"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 76
    .local v3, spec:Ljava/lang/String;
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 77
    .local v4, url:Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 78
    .local v0, connection:Ljava/net/URLConnection;
    invoke-virtual {p0, v0}, Ljava/net/ContentHandler;->getContent(Ljava/net/URLConnection;)Ljava/lang/Object;

    move-result-object v1

    .line 79
    .local v1, content:Ljava/lang/Object;
    instance-of v5, v1, Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    if-eqz v5, :cond_18

    .line 80
    check-cast v1, Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    .end local v1           #content:Ljava/lang/Object;
    return-object v1

    .line 82
    .restart local v1       #content:Ljava/lang/Object;
    :cond_18
    const-string v2, "ContentHandler must return FeedLoader.documentInfo(...)"

    .line 83
    .local v2, message:Ljava/lang/String;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public static loadFeed(Ljava/net/ContentHandler;Landroid/net/Uri;)V
    .registers 2
    .parameter "handler"
    .parameter "documentUri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p0, p1}, Lcom/google/android/feeds/FeedLoader;->loadDocument(Ljava/net/ContentHandler;Landroid/net/Uri;)Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    .line 101
    return-void
.end method

.method public static loadIndexedFeed(Ljava/net/ContentHandler;Landroid/net/Uri;Ljava/lang/String;IIILandroid/os/Bundle;)V
    .registers 15
    .parameter "handler"
    .parameter "baseDocumentUri"
    .parameter "indexParameter"
    .parameter "firstIndex"
    .parameter "pageSize"
    .parameter "itemCount"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    const/4 v5, 0x0

    .line 138
    .local v5, totalCount:I
    move v4, p3

    .line 142
    .local v4, index:I
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 143
    .local v2, documentUri:Landroid/net/Uri$Builder;
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, p2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 144
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/google/android/feeds/FeedLoader;->loadDocument(Ljava/net/ContentHandler;Landroid/net/Uri;)Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    move-result-object v0

    .line 146
    .local v0, document:Lcom/google/android/feeds/FeedLoader$DocumentInfo;
    invoke-virtual {v0}, Lcom/google/android/feeds/FeedLoader$DocumentInfo;->itemCount()I

    move-result v1

    .line 147
    .local v1, documentItemCount:I
    if-gez v1, :cond_23

    .line 148
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Invalid document info: item count is unset or invalid"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 152
    :cond_23
    if-lt v1, p4, :cond_32

    const/4 v3, 0x1

    .line 153
    .local v3, hasMore:Z
    :goto_26
    const-string v6, "com.google.feeds.cursor.extra.MORE"

    invoke-virtual {p6, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 156
    add-int/2addr v4, v1

    .line 157
    add-int/2addr v5, v1

    .line 158
    if-ge v5, p5, :cond_31

    if-nez v3, :cond_2

    .line 159
    :cond_31
    return-void

    .line 152
    .end local v3           #hasMore:Z
    :cond_32
    const/4 v3, 0x0

    goto :goto_26
.end method

.method public static loadPagedFeed(Ljava/net/ContentHandler;Landroid/net/Uri;Ljava/lang/String;IIILandroid/os/Bundle;)V
    .registers 15
    .parameter "handler"
    .parameter "baseDocumentUri"
    .parameter "pageParameter"
    .parameter "firstPage"
    .parameter "pageSize"
    .parameter "itemCount"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    const/4 v5, 0x0

    .line 195
    .local v5, totalCount:I
    move v4, p3

    .line 200
    .local v4, page:I
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 201
    .local v2, documentUri:Landroid/net/Uri$Builder;
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, p2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 202
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/google/android/feeds/FeedLoader;->loadDocument(Ljava/net/ContentHandler;Landroid/net/Uri;)Lcom/google/android/feeds/FeedLoader$DocumentInfo;

    move-result-object v0

    .line 204
    .local v0, document:Lcom/google/android/feeds/FeedLoader$DocumentInfo;
    invoke-virtual {v0}, Lcom/google/android/feeds/FeedLoader$DocumentInfo;->itemCount()I

    move-result v1

    .line 205
    .local v1, documentItemCount:I
    if-gez v1, :cond_23

    .line 206
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Invalid document info: item count is unset or invalid"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 210
    :cond_23
    if-lt v1, p4, :cond_33

    const/4 v3, 0x1

    .line 211
    .local v3, morePages:Z
    :goto_26
    const-string v6, "com.google.feeds.cursor.extra.MORE"

    invoke-virtual {p6, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 214
    add-int/lit8 v4, v4, 0x1

    .line 215
    add-int/2addr v5, v1

    .line 216
    if-ge v5, p5, :cond_32

    if-nez v3, :cond_2

    .line 217
    :cond_32
    return-void

    .line 210
    .end local v3           #morePages:Z
    :cond_33
    const/4 v3, 0x0

    goto :goto_26
.end method
