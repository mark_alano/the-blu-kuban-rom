.class Lcom/google/android/imageloader/ImageLoader$ImageRequest;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/imageloader/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageRequest"
.end annotation


# instance fields
.field private final mAdapterReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/BaseAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mCallback:Lcom/google/android/imageloader/ImageLoader$Callback;

.field private mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

.field private final mImageViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private final mLoadBitmap:Z

.field private final mUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/imageloader/ImageLoader;


# direct methods
.method private constructor <init>(Lcom/google/android/imageloader/ImageLoader;Landroid/widget/BaseAdapter;Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/imageloader/ImageLoader$Callback;Z)V
    .registers 9
    .parameter
    .parameter "adapter"
    .parameter "view"
    .parameter "url"
    .parameter "callback"
    .parameter "loadBitmap"

    .prologue
    const/4 v1, 0x0

    .line 690
    iput-object p1, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 691
    if-eqz p2, :cond_1f

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :goto_d
    iput-object v0, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mAdapterReference:Ljava/lang/ref/WeakReference;

    .line 692
    if-eqz p3, :cond_16

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :cond_16
    iput-object v1, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mImageViewReference:Ljava/lang/ref/WeakReference;

    .line 693
    iput-object p4, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    .line 694
    iput-object p5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mCallback:Lcom/google/android/imageloader/ImageLoader$Callback;

    .line 695
    iput-boolean p6, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mLoadBitmap:Z

    .line 696
    return-void

    :cond_1f
    move-object v0, v1

    .line 691
    goto :goto_d
.end method

.method public constructor <init>(Lcom/google/android/imageloader/ImageLoader;Landroid/widget/BaseAdapter;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter "adapter"
    .parameter "url"

    .prologue
    const/4 v3, 0x0

    .line 703
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/imageloader/ImageLoader$ImageRequest;-><init>(Lcom/google/android/imageloader/ImageLoader;Landroid/widget/BaseAdapter;Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/imageloader/ImageLoader$Callback;Z)V

    .line 704
    return-void
.end method

.method public constructor <init>(Lcom/google/android/imageloader/ImageLoader;Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/imageloader/ImageLoader$Callback;)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "url"
    .parameter "callback"

    .prologue
    .line 711
    const/4 v2, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/imageloader/ImageLoader$ImageRequest;-><init>(Lcom/google/android/imageloader/ImageLoader;Landroid/widget/BaseAdapter;Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/imageloader/ImageLoader$Callback;Z)V

    .line 712
    return-void
.end method

.method public constructor <init>(Lcom/google/android/imageloader/ImageLoader;Ljava/lang/String;Z)V
    .registers 11
    .parameter
    .parameter "url"
    .parameter "loadBitmap"

    .prologue
    const/4 v2, 0x0

    .line 718
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    move-object v5, v2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/imageloader/ImageLoader$ImageRequest;-><init>(Lcom/google/android/imageloader/ImageLoader;Landroid/widget/BaseAdapter;Landroid/widget/ImageView;Ljava/lang/String;Lcom/google/android/imageloader/ImageLoader$Callback;Z)V

    .line 719
    return-void
.end method

.method private loadImage(Ljava/net/URL;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 722
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 723
    .local v0, connection:Ljava/net/URLConnection;
    iget-object v1, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    #getter for: Lcom/google/android/imageloader/ImageLoader;->mBitmapContentHandler:Ljava/net/ContentHandler;
    invoke-static {v1}, Lcom/google/android/imageloader/ImageLoader;->access$100(Lcom/google/android/imageloader/ImageLoader;)Ljava/net/ContentHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/net/ContentHandler;->getContent(Ljava/net/URLConnection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    return-object v1
.end method


# virtual methods
.method public execute()Z
    .registers 10

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 734
    :try_start_2
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mAdapterReference:Ljava/lang/ref/WeakReference;

    if-eqz v7, :cond_f

    .line 736
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mAdapterReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_f

    .line 800
    :cond_e
    :goto_e
    return v5

    .line 743
    :cond_f
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mImageViewReference:Ljava/lang/ref/WeakReference;

    if-eqz v7, :cond_1b

    .line 745
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mImageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_e

    .line 753
    :cond_1b
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    iget-object v8, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    #calls: Lcom/google/android/imageloader/ImageLoader;->getError(Ljava/lang/String;)Lcom/google/android/imageloader/ImageLoader$ImageError;
    invoke-static {v7, v8}, Lcom/google/android/imageloader/ImageLoader;->access$200(Lcom/google/android/imageloader/ImageLoader;Ljava/lang/String;)Lcom/google/android/imageloader/ImageLoader$ImageError;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    .line 754
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    if-eqz v7, :cond_2b

    move v5, v6

    .line 755
    goto :goto_e

    .line 759
    :cond_2b
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    iget-object v8, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    #calls: Lcom/google/android/imageloader/ImageLoader;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v7, v8}, Lcom/google/android/imageloader/ImageLoader;->access$300(Lcom/google/android/imageloader/ImageLoader;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;

    .line 760
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_3b

    move v5, v6

    .line 762
    goto :goto_e

    .line 765
    :cond_3b
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    #calls: Lcom/google/android/imageloader/ImageLoader;->getProtocol(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/imageloader/ImageLoader;->access$400(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 766
    .local v2, protocol:Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    #calls: Lcom/google/android/imageloader/ImageLoader;->getURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;
    invoke-static {v7, v2}, Lcom/google/android/imageloader/ImageLoader;->access$500(Lcom/google/android/imageloader/ImageLoader;Ljava/lang/String;)Ljava/net/URLStreamHandler;

    move-result-object v3

    .line 767
    .local v3, streamHandler:Ljava/net/URLStreamHandler;
    new-instance v4, Ljava/net/URL;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    invoke-direct {v4, v7, v8, v3}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;Ljava/net/URLStreamHandler;)V

    .line 769
    .local v4, url:Ljava/net/URL;
    iget-boolean v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mLoadBitmap:Z
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_51} :catch_65
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_51} :catch_7a
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_51} :catch_a0

    if-eqz v7, :cond_86

    .line 771
    :try_start_53
    invoke-direct {p0, v4}, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->loadImage(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_59
    .catch Ljava/lang/OutOfMemoryError; {:try_start_53 .. :try_end_59} :catch_6f
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_59} :catch_65
    .catch Ljava/lang/RuntimeException; {:try_start_53 .. :try_end_59} :catch_7a
    .catch Ljava/lang/Error; {:try_start_53 .. :try_end_59} :catch_a0

    .line 779
    :goto_59
    :try_start_59
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v5, :cond_84

    .line 780
    new-instance v5, Ljava/lang/NullPointerException;

    const-string v7, "ContentHandler returned null"

    invoke-direct {v5, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_65
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_65} :catch_65
    .catch Ljava/lang/RuntimeException; {:try_start_59 .. :try_end_65} :catch_7a
    .catch Ljava/lang/Error; {:try_start_59 .. :try_end_65} :catch_a0

    .line 792
    .end local v2           #protocol:Ljava/lang/String;
    .end local v3           #streamHandler:Ljava/net/URLStreamHandler;
    .end local v4           #url:Ljava/net/URL;
    :catch_65
    move-exception v1

    .line 793
    .local v1, e:Ljava/io/IOException;
    new-instance v5, Lcom/google/android/imageloader/ImageLoader$ImageError;

    invoke-direct {v5, v1}, Lcom/google/android/imageloader/ImageLoader$ImageError;-><init>(Ljava/lang/Throwable;)V

    iput-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    move v5, v6

    .line 794
    goto :goto_e

    .line 776
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #protocol:Ljava/lang/String;
    .restart local v3       #streamHandler:Ljava/net/URLStreamHandler;
    .restart local v4       #url:Ljava/net/URL;
    :catch_6f
    move-exception v5

    :try_start_70
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 777
    invoke-direct {p0, v4}, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->loadImage(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_79
    .catch Ljava/io/IOException; {:try_start_70 .. :try_end_79} :catch_65
    .catch Ljava/lang/RuntimeException; {:try_start_70 .. :try_end_79} :catch_7a
    .catch Ljava/lang/Error; {:try_start_70 .. :try_end_79} :catch_a0

    goto :goto_59

    .line 795
    .end local v2           #protocol:Ljava/lang/String;
    .end local v3           #streamHandler:Ljava/net/URLStreamHandler;
    .end local v4           #url:Ljava/net/URL;
    :catch_7a
    move-exception v1

    .line 796
    .local v1, e:Ljava/lang/RuntimeException;
    new-instance v5, Lcom/google/android/imageloader/ImageLoader$ImageError;

    invoke-direct {v5, v1}, Lcom/google/android/imageloader/ImageLoader$ImageError;-><init>(Ljava/lang/Throwable;)V

    iput-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    move v5, v6

    .line 797
    goto :goto_e

    .end local v1           #e:Ljava/lang/RuntimeException;
    .restart local v2       #protocol:Ljava/lang/String;
    .restart local v3       #streamHandler:Ljava/net/URLStreamHandler;
    .restart local v4       #url:Ljava/net/URL;
    :cond_84
    move v5, v6

    .line 782
    goto :goto_e

    .line 784
    :cond_86
    :try_start_86
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    #getter for: Lcom/google/android/imageloader/ImageLoader;->mPrefetchContentHandler:Ljava/net/ContentHandler;
    invoke-static {v7}, Lcom/google/android/imageloader/ImageLoader;->access$600(Lcom/google/android/imageloader/ImageLoader;)Ljava/net/ContentHandler;

    move-result-object v7

    if-eqz v7, :cond_9b

    .line 786
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 787
    .local v0, connection:Ljava/net/URLConnection;
    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    #getter for: Lcom/google/android/imageloader/ImageLoader;->mPrefetchContentHandler:Ljava/net/ContentHandler;
    invoke-static {v7}, Lcom/google/android/imageloader/ImageLoader;->access$600(Lcom/google/android/imageloader/ImageLoader;)Ljava/net/ContentHandler;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/net/ContentHandler;->getContent(Ljava/net/URLConnection;)Ljava/lang/Object;

    .line 789
    .end local v0           #connection:Ljava/net/URLConnection;
    :cond_9b
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_9e
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_9e} :catch_65
    .catch Ljava/lang/RuntimeException; {:try_start_86 .. :try_end_9e} :catch_7a
    .catch Ljava/lang/Error; {:try_start_86 .. :try_end_9e} :catch_a0

    goto/16 :goto_e

    .line 798
    .end local v2           #protocol:Ljava/lang/String;
    .end local v3           #streamHandler:Ljava/net/URLStreamHandler;
    .end local v4           #url:Ljava/net/URL;
    :catch_a0
    move-exception v1

    .line 799
    .local v1, e:Ljava/lang/Error;
    new-instance v5, Lcom/google/android/imageloader/ImageLoader$ImageError;

    invoke-direct {v5, v1}, Lcom/google/android/imageloader/ImageLoader$ImageError;-><init>(Ljava/lang/Throwable;)V

    iput-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    move v5, v6

    .line 800
    goto/16 :goto_e
.end method

.method public publishResult()V
    .registers 9

    .prologue
    .line 805
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_25

    .line 806
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    iget-object v6, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;

    #calls: Lcom/google/android/imageloader/ImageLoader;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    invoke-static {v5, v6, v7}, Lcom/google/android/imageloader/ImageLoader;->access$700(Lcom/google/android/imageloader/ImageLoader;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 812
    :cond_d
    :goto_d
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mAdapterReference:Ljava/lang/ref/WeakReference;

    if-eqz v5, :cond_5d

    .line 813
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mAdapterReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/BaseAdapter;

    .line 814
    .local v1, adapter:Landroid/widget/BaseAdapter;
    if-eqz v1, :cond_24

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_24

    .line 818
    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 856
    .end local v1           #adapter:Landroid/widget/BaseAdapter;
    :cond_24
    :goto_24
    return-void

    .line 807
    :cond_25
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    iget-object v6, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    #calls: Lcom/google/android/imageloader/ImageLoader;->hasError(Ljava/lang/String;)Z
    invoke-static {v5, v6}, Lcom/google/android/imageloader/ImageLoader;->access$800(Lcom/google/android/imageloader/ImageLoader;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 808
    const-string v5, "ImageLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to load "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    invoke-virtual {v7}, Lcom/google/android/imageloader/ImageLoader$ImageError;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-static {v5, v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 809
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    iget-object v6, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    #calls: Lcom/google/android/imageloader/ImageLoader;->putError(Ljava/lang/String;Lcom/google/android/imageloader/ImageLoader$ImageError;)V
    invoke-static {v5, v6, v7}, Lcom/google/android/imageloader/ImageLoader;->access$900(Lcom/google/android/imageloader/ImageLoader;Ljava/lang/String;Lcom/google/android/imageloader/ImageLoader$ImageError;)V

    goto :goto_d

    .line 826
    :cond_5d
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mImageViewReference:Ljava/lang/ref/WeakReference;

    if-eqz v5, :cond_24

    .line 827
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mImageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 828
    .local v4, view:Landroid/widget/ImageView;
    if-eqz v4, :cond_24

    .line 829
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->this$0:Lcom/google/android/imageloader/ImageLoader;

    #getter for: Lcom/google/android/imageloader/ImageLoader;->mImageViewBinding:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/imageloader/ImageLoader;->access$1000(Lcom/google/android/imageloader/ImageLoader;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 830
    .local v2, binding:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 835
    invoke-virtual {v4}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 836
    .local v3, context:Landroid/content/Context;
    instance-of v5, v3, Landroid/app/Activity;

    if-eqz v5, :cond_90

    move-object v0, v3

    .line 837
    check-cast v0, Landroid/app/Activity;

    .line 838
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_24

    .line 842
    .end local v0           #activity:Landroid/app/Activity;
    :cond_90
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_a5

    .line 843
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 844
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mCallback:Lcom/google/android/imageloader/ImageLoader$Callback;

    if-eqz v5, :cond_24

    .line 845
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mCallback:Lcom/google/android/imageloader/ImageLoader$Callback;

    iget-object v6, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    invoke-interface {v5, v4, v6}, Lcom/google/android/imageloader/ImageLoader$Callback;->onImageLoaded(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_24

    .line 847
    :cond_a5
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    if-eqz v5, :cond_24

    .line 848
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mCallback:Lcom/google/android/imageloader/ImageLoader$Callback;

    if-eqz v5, :cond_24

    .line 849
    iget-object v5, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mCallback:Lcom/google/android/imageloader/ImageLoader$Callback;

    iget-object v6, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/imageloader/ImageLoader$ImageRequest;->mError:Lcom/google/android/imageloader/ImageLoader$ImageError;

    invoke-virtual {v7}, Lcom/google/android/imageloader/ImageLoader$ImageError;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-interface {v5, v4, v6, v7}, Lcom/google/android/imageloader/ImageLoader$Callback;->onImageError(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_24
.end method
