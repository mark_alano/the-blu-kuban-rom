// Copyright 2012 Google Inc. All Rights Reserved.

/**
 * @fileoverview Implementation of the communication protocol between the Punch
 * webview and the native layer. See google3/javascript/apps/punch/viewer/
 * mobilenative/api/webview/externs/jstonative/webvieweventlistener.js for
 * more detail of what methods need to be defined.
 * @author brianpark@google.com (Brian Park)
 */

// Javascript -> Native callbacks.
window.punchWebViewEventListener = (function() {
  var dataApi_;
  var interceptClicks_ = true;

  return {
    /**
     * Called when the Punch APIs are exported.
     */
    onApiExported: function(dataApi, controlApi) {
      dataApi_ = dataApi;
      window.PUNCH_WEBVIEW_CONTROL_API = controlApi;
      var pageCount = dataApi_.getNumSlides();
      var pageWidth = dataApi_.getPageWidth();
      var pageHeight = dataApi_.getPageHeight();

      // Sets a listener to detect tap action and report them to hosting
      // application: we do not report click action which relate to interaction
      // with the slide content, such as links or video control buttons.
      document.addEventListener("click", function(event) {

        var supportsSVG = typeof SVGAElement != "undefined";
        // Flag telling whether we should ignore this click event or not.
        var ignore = false;
        for (var current = event.target;
            current != null;
            current = current.parentNode) {
          if (current == document) {
            break;
          }

          if (!!current.onclick) {
            ignore = true;
            break;
          }

          if (current instanceof HTMLAnchorElement) {
            ignore = true;
            break;
          }

          if (supportsSVG && current instanceof SVGAElement) {
            ignore = true;
            break;
          }
        }

        if (interceptClicks_ && !ignore) {
          event.stopPropagation();
          window.WebViewApi.onTap();
        }
      }, true);

      window.WebViewApi.onApiExported(pageCount, pageWidth, pageHeight);

      var containsNonSupportedTransition = false;
      if (dataApi_.containsNonSupportedTransition) {
        containsNonSupportedTransition =
          dataApi_.containsNonSupportedTransition();
      }

      var containsAnimationOrTransition = false;
      if (dataApi_.containsAnimationOrTransition) {
        containsAnimationOrTransition =
          dataApi_.containsAnimationOrTransition();
      }

      window.WebViewApi.onNotifyMissingFeatures(containsNonSupportedTransition,
                                                containsAnimationOrTransition);
    },

    /**
     * Called when there is a change in a slide's load state.
     * @see punch.viewer.mobilenative.LOAD_STATE_MAPPING
     */
    onSlideLoadStateChange: function(slideIndex, loadState) {
      // If the slide's load state is LOADED then fill in title and thumbnail
      // content.
      var slideTitle = null;
      var slideSpeakerNotes = null;
      if (loadState == 2) {
        slideTitle = dataApi_.getSlideTitle(slideIndex);

        if (dataApi_.hasSpeakerNotes(slideIndex)) {
          slideSpeakerNotes = dataApi_.getSpeakerNotes(slideIndex);
        }
      }
      window.WebViewApi.onSlideLoadStateChange(slideIndex, loadState,
          slideTitle, slideSpeakerNotes);
    },

    /**
     * Called when requests for all slides have completed.
     */
    onFinishedLoading: function() {
      window.WebViewApi.onFinishedLoading(dataApi_.isLoaded());
    },

    /**
     * Called when it navigates to a different slide or a different part (when a
     * slide contains multiple animations) of a slide.
     */
    onNavigation: function() {
      window.WebViewApi.onNavigation(dataApi_.getCurrentSlideIndex());
    },

    /**
     * Called when there is a change in the action's state.
     */
    onActionEnabledStateChange: function(actionId) {
      window.WebViewApi.onActionEnabledStateChange(
          actionId, dataApi_.isActionEnabled(actionId));
    },

    /**
     * Called to notify when there is a change in previous preview.
     */
    onPreviousPreviewChange: function() {
      var prevContent = null;
      if (dataApi_.hasPrevContent()) {
        prevContent = dataApi_.getPrevContent();
      }
      window.WebViewApi.onPreviousPreviewChange(prevContent);
    },

    /**
     * Called to notify when there is a change in next preview.
     */
    onNextPreviewChange: function() {
      var nextContent = null;
      if (dataApi_.hasNextContent()) {
        nextContent = dataApi_.getNextContent();
      }
      window.WebViewApi.onNextPreviewChange(nextContent);
    },

    /**
     * Called (from app) to access content for a slide.
     */
    requestSlideContent: function(requestId, slideIndex) {
      var slideContent = dataApi_.getSlideContent(slideIndex);
      var slideContentType = dataApi_.getContentType();
      window.WebViewApi.onSlideContentRequestCompleted(
        requestId, slideIndex, slideContent, slideContentType);
    },

    /**
     * Configures the app to allow or block touch events from reaching punch.
     *
     * Intended to be called by the client app.
     */
    setInterceptClicks: function(interceptClicks) {
      interceptClicks_ = interceptClicks;
    }
  };
})();
