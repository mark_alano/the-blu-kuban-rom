.class LA/e;
.super LA/s;
.source "SourceFile"


# instance fields
.field private final A:Z


# direct methods
.method private constructor <init>(LA/f;)V
    .registers 3
    .parameter

    .prologue
    .line 654
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LA/s;-><init>(LA/t;LA/d;)V

    .line 656
    invoke-static {p1}, LA/f;->a(LA/f;)Z

    move-result v0

    iput-boolean v0, p0, LA/e;->A:Z

    .line 657
    return-void
.end method

.method synthetic constructor <init>(LA/f;LA/d;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 649
    invoke-direct {p0, p1}, LA/e;-><init>(LA/f;)V

    return-void
.end method


# virtual methods
.method public a(ILcom/google/android/maps/driveabout/vector/q;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 669
    iget-boolean v0, p0, LA/e;->A:Z

    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_11

    .line 670
    :cond_8
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v0, :cond_10

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->d:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_12

    .line 671
    :cond_10
    const/4 p1, 0x0

    .line 680
    :cond_11
    :goto_11
    return p1

    .line 672
    :cond_12
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v0, :cond_11

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p2, v0, :cond_11

    .line 673
    and-int/lit16 p1, p1, -0x1a07

    goto :goto_11
.end method

.method public a(Lo/ad;)Lo/T;
    .registers 3
    .parameter

    .prologue
    .line 696
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lo/ad;->a(I)Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 691
    const v0, 0x3e99999a

    invoke-interface {p1, v1, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 692
    return-void
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 661
    const/4 v0, 0x1

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 685
    const/4 v0, 0x1

    return v0
.end method

.method public j()LF/O;
    .registers 4

    .prologue
    .line 701
    new-instance v0, LF/O;

    sget-object v1, LF/N;->f:LF/N;

    sget-object v2, LF/s;->b:LF/s;

    invoke-direct {v0, v1, v2}, LF/O;-><init>(LF/N;LF/s;)V

    return-object v0
.end method

.method public k()Lo/aj;
    .registers 2

    .prologue
    .line 707
    invoke-static {}, LA/c;->n()Lo/aj;

    move-result-object v0

    return-object v0
.end method
