.class abstract LA/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method private constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iput v1, p0, LA/g;->b:I

    .line 284
    const-string v0, ""

    iput-object v0, p0, LA/g;->c:Ljava/lang/String;

    .line 285
    iput-boolean v1, p0, LA/g;->d:Z

    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, LA/g;->e:Z

    .line 287
    iput-boolean v1, p0, LA/g;->f:Z

    .line 288
    iput-boolean v1, p0, LA/g;->g:Z

    .line 291
    iput p1, p0, LA/g;->a:I

    .line 292
    return-void
.end method

.method synthetic constructor <init>(ILA/d;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 281
    invoke-direct {p0, p1}, LA/g;-><init>(I)V

    return-void
.end method

.method static synthetic a(LA/g;)I
    .registers 2
    .parameter

    .prologue
    .line 281
    iget v0, p0, LA/g;->a:I

    return v0
.end method

.method static synthetic b(LA/g;)I
    .registers 2
    .parameter

    .prologue
    .line 281
    iget v0, p0, LA/g;->b:I

    return v0
.end method

.method static synthetic c(LA/g;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 281
    iget-object v0, p0, LA/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(LA/g;)Z
    .registers 2
    .parameter

    .prologue
    .line 281
    iget-boolean v0, p0, LA/g;->d:Z

    return v0
.end method

.method static synthetic e(LA/g;)Z
    .registers 2
    .parameter

    .prologue
    .line 281
    iget-boolean v0, p0, LA/g;->e:Z

    return v0
.end method

.method static synthetic f(LA/g;)Z
    .registers 2
    .parameter

    .prologue
    .line 281
    iget-boolean v0, p0, LA/g;->f:Z

    return v0
.end method

.method static synthetic g(LA/g;)Z
    .registers 2
    .parameter

    .prologue
    .line 281
    iget-boolean v0, p0, LA/g;->g:Z

    return v0
.end method


# virtual methods
.method abstract a()LA/c;
.end method

.method a(I)LA/g;
    .registers 2
    .parameter

    .prologue
    .line 295
    iput p1, p0, LA/g;->b:I

    .line 296
    return-object p0
.end method

.method a(Ljava/lang/String;)LA/g;
    .registers 2
    .parameter

    .prologue
    .line 300
    iput-object p1, p0, LA/g;->c:Ljava/lang/String;

    .line 301
    return-object p0
.end method

.method b(Z)LA/g;
    .registers 2
    .parameter

    .prologue
    .line 305
    iput-boolean p1, p0, LA/g;->d:Z

    .line 306
    return-object p0
.end method

.method c(Z)LA/g;
    .registers 2
    .parameter

    .prologue
    .line 310
    iput-boolean p1, p0, LA/g;->e:Z

    .line 311
    return-object p0
.end method

.method d(Z)LA/g;
    .registers 2
    .parameter

    .prologue
    .line 315
    iput-boolean p1, p0, LA/g;->f:Z

    .line 316
    return-object p0
.end method

.method e(Z)LA/g;
    .registers 2
    .parameter

    .prologue
    .line 320
    iput-boolean p1, p0, LA/g;->g:Z

    .line 321
    return-object p0
.end method
