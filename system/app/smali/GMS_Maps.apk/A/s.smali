.class LA/s;
.super LA/c;
.source "SourceFile"


# instance fields
.field private final A:Z


# direct methods
.method private constructor <init>(LA/t;)V
    .registers 3
    .parameter

    .prologue
    .line 616
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LA/c;-><init>(LA/g;LA/d;)V

    .line 618
    invoke-static {p1}, LA/t;->a(LA/t;)Z

    move-result v0

    iput-boolean v0, p0, LA/s;->A:Z

    .line 619
    return-void
.end method

.method synthetic constructor <init>(LA/t;LA/d;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 551
    invoke-direct {p0, p1}, LA/s;-><init>(LA/t;)V

    return-void
.end method


# virtual methods
.method a()I
    .registers 2

    .prologue
    .line 612
    const/16 v0, 0x1000

    return v0
.end method

.method public a(Law/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lr/z;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 567
    sget-object v0, LA/s;->n:LA/c;

    if-ne p0, v0, :cond_10

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    if-nez v0, :cond_10

    .line 568
    const/4 v0, 0x0

    .line 602
    :cond_f
    :goto_f
    return-object v0

    .line 570
    :cond_10
    invoke-static {p0}, LA/c;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 571
    :goto_1c
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;I)I

    move-result v3

    .line 575
    sget-object v0, LA/c;->u:LA/c;

    if-ne p0, v0, :cond_3a

    .line 576
    invoke-static {}, LA/c;->m()LA/r;

    move-result-object v0

    if-nez v0, :cond_31

    .line 577
    const/4 v0, 0x0

    goto :goto_f

    .line 570
    :cond_2e
    const/high16 v4, 0x3f80

    goto :goto_1c

    .line 579
    :cond_31
    invoke-static {}, LA/c;->m()LA/r;

    move-result-object v0

    invoke-interface {v0, p0, v3, v4, p4}, LA/r;->a(LA/c;IFLjava/util/Locale;)Lr/z;

    move-result-object v0

    goto :goto_f

    .line 582
    :cond_3a
    if-eqz p6, :cond_56

    invoke-static {p0}, LA/c;->c(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_56

    const/4 v6, 0x1

    .line 584
    :goto_43
    const/4 v9, 0x0

    check-cast v9, Lt/g;

    .line 586
    iget-boolean v0, p0, LA/c;->x:Z

    if-eqz v0, :cond_58

    .line 587
    new-instance v0, Lr/D;

    sget-object v8, Lr/D;->i:Lr/H;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v9}, Lr/D;-><init>(Law/p;LA/c;IFLjava/util/Locale;ZLjava/io/File;Lr/H;Lt/g;)V

    goto :goto_f

    .line 582
    :cond_56
    const/4 v6, 0x0

    goto :goto_43

    .line 591
    :cond_58
    new-instance v0, Lr/I;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p4

    move-object v7, p5

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lr/I;-><init>(Law/p;LA/c;IFLjava/util/Locale;ZLjava/io/File;Lt/g;)V

    .line 594
    sget-object v1, LA/c;->f:LA/c;

    if-eq p0, v1, :cond_6a

    sget-object v1, LA/c;->g:LA/c;

    if-ne p0, v1, :cond_70

    .line 595
    :cond_6a
    const-wide/32 v1, 0x1d4c0

    invoke-virtual {v0, v1, v2}, Lr/I;->a(J)V

    .line 598
    :cond_70
    iget-boolean v1, p0, LA/s;->A:Z

    if-eqz v1, :cond_f

    if-eqz p7, :cond_f

    .line 599
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr/I;->a(Lcom/google/googlenav/bE;)V

    goto :goto_f
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 551
    check-cast p1, LA/c;

    invoke-super {p0, p1}, LA/c;->a(LA/c;)I

    move-result v0

    return v0
.end method

.method public l()Lt/J;
    .registers 2

    .prologue
    .line 607
    new-instance v0, LA/u;

    invoke-direct {v0, p0}, LA/u;-><init>(LA/c;)V

    return-object v0
.end method
