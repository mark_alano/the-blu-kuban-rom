.class public LC/a;
.super Lz/e;
.source "SourceFile"


# static fields
.field private static final F:Ljava/util/concurrent/atomic/AtomicLong;

.field static final c:F

.field public static final d:LC/b;

.field private static final e:F

.field private static final f:Lo/T;

.field private static final g:F


# instance fields
.field private A:[F

.field private B:[F

.field private C:[F

.field private final D:[F

.field private final E:Lo/T;

.field private volatile G:J

.field private final h:Ljava/lang/Thread;

.field private i:LC/b;

.field private j:Z

.field private k:Lo/T;

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:I

.field private q:I

.field private r:F

.field private s:Lo/T;

.field private t:Lo/T;

.field private u:Lo/T;

.field private v:Lo/aQ;

.field private w:F

.field private x:[D

.field private y:[F

.field private z:[F


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 29
    const-wide/high16 v0, 0x3ff0

    const-wide/high16 v4, 0x4000

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v0, v4

    double-to-float v0, v0

    sput v0, LC/a;->e:F

    .line 47
    const/high16 v0, 0x4880

    const-wide v1, 0x3ff4f1a6c638d03fL

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    sput v0, LC/a;->c:F

    .line 51
    new-instance v0, Lo/T;

    const/4 v1, 0x1

    invoke-direct {v0, v6, v6, v1}, Lo/T;-><init>(III)V

    sput-object v0, LC/a;->f:Lo/T;

    .line 82
    new-instance v0, LC/b;

    new-instance v1, Lo/T;

    invoke-direct {v1, v6, v6}, Lo/T;-><init>(II)V

    const/high16 v2, 0x41a0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    sput-object v0, LC/a;->d:LC/b;

    .line 92
    const-wide/high16 v0, 0x3fe0

    const-wide v2, 0x3fd0c152382d7365L

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, LC/a;->g:F

    .line 179
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x1

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, LC/a;->F:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(LC/b;IIF)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 201
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LC/a;-><init>(LC/b;IIFLjava/lang/Thread;)V

    .line 202
    return-void
.end method

.method public constructor <init>(LC/b;IIFLjava/lang/Thread;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 244
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v0, 0x10

    new-array v8, v0, [F

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, LC/a;-><init>(LC/b;IIFLjava/lang/Thread;Lz/A;I[F)V

    .line 246
    return-void
.end method

.method public constructor <init>(LC/b;IIFLjava/lang/Thread;Lz/A;I[F)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-direct {p0, p6, p7, p8}, Lz/e;-><init>(Lz/A;I[F)V

    .line 133
    sget v0, LC/a;->g:F

    iput v0, p0, LC/a;->n:F

    .line 163
    iput-object v1, p0, LC/a;->y:[F

    .line 164
    iput-object v1, p0, LC/a;->z:[F

    .line 165
    iput-object v1, p0, LC/a;->A:[F

    .line 166
    iput-object v1, p0, LC/a;->B:[F

    .line 167
    iput-object v1, p0, LC/a;->C:[F

    .line 170
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, LC/a;->D:[F

    .line 173
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LC/a;->E:Lo/T;

    .line 185
    sget-object v0, LC/a;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, LC/a;->G:J

    .line 226
    iput p3, p0, LC/a;->q:I

    .line 227
    iput p2, p0, LC/a;->p:I

    .line 228
    iput p4, p0, LC/a;->r:F

    .line 229
    iput-object p5, p0, LC/a;->h:Ljava/lang/Thread;

    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, LC/a;->j:Z

    .line 231
    const/high16 v0, 0x41f0

    invoke-virtual {p0, v0}, LC/a;->b(F)V

    .line 232
    invoke-direct {p0, p1}, LC/a;->b(LC/b;)V

    .line 233
    return-void
.end method

.method private D()V
    .registers 13

    .prologue
    const-wide v6, 0x3f91df46a2529d39L

    .line 405
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->e()F

    move-result v0

    float-to-double v0, v0

    mul-double/2addr v0, v6

    .line 406
    iget-object v2, p0, LC/a;->i:LC/b;

    invoke-virtual {v2}, LC/b;->d()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v2, v6

    .line 408
    iget-object v4, p0, LC/a;->i:LC/b;

    invoke-virtual {v4}, LC/b;->f()F

    move-result v4

    const/high16 v5, 0x3f00

    mul-float/2addr v4, v5

    iget v5, p0, LC/a;->o:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v4, v6

    .line 410
    iget v6, p0, LC/a;->l:F

    float-to-double v6, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    mul-double/2addr v2, v10

    sub-double v2, v8, v2

    mul-double/2addr v2, v6

    .line 413
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    .line 414
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 415
    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->c()Lo/T;

    move-result-object v1

    .line 416
    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {v1}, Lo/T;->g()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {v2, v3, v0}, Lo/T;->d(II)V

    .line 417
    return-void
.end method

.method private E()V
    .registers 7

    .prologue
    .line 420
    const-wide/high16 v0, 0x4000

    const/high16 v2, 0x41f0

    iget-object v3, p0, LC/a;->i:LC/b;

    invoke-virtual {v3}, LC/b;->a()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 426
    iget v2, p0, LC/a;->q:I

    int-to-double v2, v2

    const/high16 v4, 0x4380

    iget v5, p0, LC/a;->r:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    div-double/2addr v2, v4

    .line 428
    mul-double/2addr v0, v2

    .line 429
    double-to-float v0, v0

    iget v1, p0, LC/a;->n:F

    mul-float/2addr v0, v1

    iput v0, p0, LC/a;->l:F

    .line 430
    return-void
.end method

.method private F()[D
    .registers 11

    .prologue
    const-wide v6, 0x3f91df46a2529d39L

    const-wide v4, 0x4076800000000000L

    .line 461
    invoke-direct {p0}, LC/a;->K()V

    .line 462
    iget-object v0, p0, LC/a;->x:[D

    if-nez v0, :cond_55

    .line 465
    const-wide v0, 0x4056800000000000L

    iget-object v2, p0, LC/a;->i:LC/b;

    invoke-virtual {v2}, LC/b;->e()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    .line 466
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_58

    .line 467
    add-double/2addr v0, v4

    .line 471
    :cond_25
    :goto_25
    iget-object v2, p0, LC/a;->i:LC/b;

    invoke-virtual {v2}, LC/b;->d()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v2, v6

    .line 473
    mul-double/2addr v0, v6

    .line 474
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    .line 475
    const/4 v6, 0x3

    new-array v6, v6, [D

    iput-object v6, p0, LC/a;->x:[D

    .line 476
    iget-object v6, p0, LC/a;->x:[D

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v8, v4

    aput-wide v8, v6, v7

    .line 477
    iget-object v6, p0, LC/a;->x:[D

    const/4 v7, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    aput-wide v0, v6, v7

    .line 478
    iget-object v0, p0, LC/a;->x:[D

    const/4 v1, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    neg-double v2, v2

    aput-wide v2, v0, v1

    .line 480
    :cond_55
    iget-object v0, p0, LC/a;->x:[D

    return-object v0

    .line 468
    :cond_58
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_25

    .line 469
    sub-double/2addr v0, v4

    goto :goto_25
.end method

.method private G()V
    .registers 9

    .prologue
    const v6, 0x3dcccccd

    .line 1066
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LC/a;->z:[F

    .line 1069
    invoke-virtual {p0}, LC/a;->x()F

    move-result v0

    .line 1070
    iget v1, p0, LC/a;->o:F

    float-to-double v1, v1

    const-wide v3, 0x3f91df46a2529d39L

    mul-double/2addr v1, v3

    const-wide/high16 v3, 0x3fe0

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->tan(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float v5, v6, v1

    .line 1072
    neg-float v4, v5

    .line 1073
    mul-float v2, v4, v0

    .line 1074
    mul-float v3, v5, v0

    .line 1075
    iget-object v0, p0, LC/a;->z:[F

    const/4 v1, 0x0

    const/high16 v7, 0x41a0

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    .line 1077
    return-void
.end method

.method private H()V
    .registers 8

    .prologue
    .line 1086
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LC/a;->y:[F

    .line 1088
    invoke-virtual {p0}, LC/a;->w()F

    move-result v0

    .line 1089
    iget-object v1, p0, LC/a;->k:Lo/T;

    .line 1091
    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v2

    .line 1092
    invoke-virtual {v2, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    .line 1094
    invoke-virtual {v3}, Lo/T;->f()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    .line 1095
    invoke-virtual {v3}, Lo/T;->g()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 1096
    invoke-virtual {v3}, Lo/T;->h()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    .line 1098
    invoke-virtual {p0}, LC/a;->v()Lo/T;

    move-result-object v6

    .line 1099
    iget-object v0, p0, LC/a;->y:[F

    invoke-virtual {v6}, Lo/T;->f()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v6}, Lo/T;->g()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v6}, Lo/T;->h()I

    move-result v6

    int-to-float v6, v6

    invoke-static/range {v0 .. v6}, LC/a;->b([FFFFFFF)V

    .line 1100
    return-void
.end method

.method private I()V
    .registers 7

    .prologue
    const/high16 v5, 0x3f80

    const/high16 v4, 0x3f00

    .line 1103
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LC/a;->A:[F

    .line 1106
    iget-object v0, p0, LC/a;->A:[F

    const/4 v1, 0x0

    iget v2, p0, LC/a;->p:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    aput v2, v0, v1

    .line 1107
    iget-object v0, p0, LC/a;->A:[F

    const/4 v1, 0x5

    iget v2, p0, LC/a;->q:I

    int-to-float v2, v2

    const/high16 v3, -0x4100

    mul-float/2addr v2, v3

    aput v2, v0, v1

    .line 1108
    iget-object v0, p0, LC/a;->A:[F

    const/16 v1, 0xa

    aput v5, v0, v1

    .line 1109
    iget-object v0, p0, LC/a;->A:[F

    const/16 v1, 0xf

    aput v5, v0, v1

    .line 1112
    iget-object v0, p0, LC/a;->A:[F

    const/16 v1, 0xc

    iget v2, p0, LC/a;->p:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    aput v2, v0, v1

    .line 1113
    iget-object v0, p0, LC/a;->A:[F

    const/16 v1, 0xd

    iget v2, p0, LC/a;->q:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    aput v2, v0, v1

    .line 1114
    return-void
.end method

.method private J()V
    .registers 9

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 1121
    iget-object v0, p0, LC/a;->y:[F

    if-nez v0, :cond_a

    .line 1122
    invoke-direct {p0}, LC/a;->H()V

    .line 1124
    :cond_a
    iget-object v0, p0, LC/a;->z:[F

    if-nez v0, :cond_11

    .line 1125
    invoke-direct {p0}, LC/a;->G()V

    .line 1127
    :cond_11
    iget-object v0, p0, LC/a;->A:[F

    if-nez v0, :cond_18

    .line 1128
    invoke-direct {p0}, LC/a;->I()V

    .line 1131
    :cond_18
    new-array v0, v2, [F

    iput-object v0, p0, LC/a;->B:[F

    .line 1133
    new-array v0, v2, [F

    .line 1134
    iget-object v2, p0, LC/a;->A:[F

    iget-object v4, p0, LC/a;->z:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1135
    iget-object v2, p0, LC/a;->B:[F

    iget-object v6, p0, LC/a;->y:[F

    move v3, v1

    move-object v4, v0

    move v5, v1

    move v7, v1

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1137
    iput-object v0, p0, LC/a;->C:[F

    .line 1138
    iget-object v0, p0, LC/a;->C:[F

    iget-object v2, p0, LC/a;->B:[F

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 1139
    return-void
.end method

.method private final K()V
    .registers 1

    .prologue
    .line 1217
    return-void
.end method

.method public static a(F)F
    .registers 3
    .parameter

    .prologue
    .line 262
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    sget v1, LC/a;->e:F

    mul-float/2addr v0, v1

    return v0
.end method

.method private b(LC/b;)V
    .registers 4
    .parameter

    .prologue
    .line 374
    iget-object v0, p0, LC/a;->i:LC/b;

    if-eqz v0, :cond_d

    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0, p1}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 392
    :goto_c
    return-void

    .line 377
    :cond_d
    sget-object v0, LC/a;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, LC/a;->G:J

    .line 378
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LC/a;->b(Z)V

    .line 379
    iget-object v0, p0, LC/a;->i:LC/b;

    if-eqz v0, :cond_39

    invoke-virtual {p1}, LC/b;->e()F

    move-result v0

    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->e()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_39

    invoke-virtual {p1}, LC/b;->d()F

    move-result v0

    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3c

    .line 381
    :cond_39
    const/4 v0, 0x0

    iput-object v0, p0, LC/a;->x:[D

    .line 383
    :cond_3c
    iput-object p1, p0, LC/a;->i:LC/b;

    .line 384
    invoke-direct {p0}, LC/a;->E()V

    .line 385
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->f()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5a

    .line 386
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LC/a;->k:Lo/T;

    .line 387
    invoke-direct {p0}, LC/a;->D()V

    .line 391
    :goto_56
    invoke-virtual {p0}, LC/a;->a()V

    goto :goto_c

    .line 389
    :cond_5a
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->c()Lo/T;

    move-result-object v0

    iput-object v0, p0, LC/a;->k:Lo/T;

    goto :goto_56
.end method

.method private b(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1049
    iput-object v1, p0, LC/a;->s:Lo/T;

    .line 1050
    iput-object v1, p0, LC/a;->t:Lo/T;

    .line 1051
    iput-object v1, p0, LC/a;->u:Lo/T;

    .line 1052
    iput-object v1, p0, LC/a;->v:Lo/aQ;

    .line 1053
    const/high16 v0, -0x4080

    iput v0, p0, LC/a;->w:F

    .line 1055
    iput-object v1, p0, LC/a;->y:[F

    .line 1056
    iput-object v1, p0, LC/a;->B:[F

    .line 1057
    iput-object v1, p0, LC/a;->C:[F

    .line 1059
    if-eqz p1, :cond_19

    .line 1060
    iput-object v1, p0, LC/a;->z:[F

    .line 1061
    iput-object v1, p0, LC/a;->A:[F

    .line 1063
    :cond_19
    return-void
.end method

.method private static b([FFFFFFF)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1158
    neg-float v0, p1

    .line 1159
    neg-float v1, p2

    .line 1160
    neg-float v2, p3

    .line 1163
    const/high16 v3, 0x3f80

    invoke-static {v0, v1, v2}, Landroid/opengl/Matrix;->length(FFF)F

    move-result v4

    div-float/2addr v3, v4

    .line 1164
    mul-float/2addr v0, v3

    .line 1165
    mul-float/2addr v1, v3

    .line 1166
    mul-float/2addr v2, v3

    .line 1169
    mul-float v3, v1, p6

    mul-float v4, v2, p5

    sub-float/2addr v3, v4

    .line 1170
    mul-float v4, v2, p4

    mul-float v5, v0, p6

    sub-float/2addr v4, v5

    .line 1171
    mul-float v5, v0, p5

    mul-float v6, v1, p4

    sub-float/2addr v5, v6

    .line 1174
    const/high16 v6, 0x3f80

    invoke-static {v3, v4, v5}, Landroid/opengl/Matrix;->length(FFF)F

    move-result v7

    div-float/2addr v6, v7

    .line 1175
    mul-float/2addr v3, v6

    .line 1176
    mul-float/2addr v4, v6

    .line 1177
    mul-float/2addr v5, v6

    .line 1180
    mul-float v6, v4, v2

    mul-float v7, v5, v1

    sub-float/2addr v6, v7

    .line 1181
    mul-float v7, v5, v0

    mul-float v8, v3, v2

    sub-float/2addr v7, v8

    .line 1182
    mul-float v8, v3, v1

    mul-float v9, v4, v0

    sub-float/2addr v8, v9

    .line 1184
    const/4 v9, 0x0

    aput v3, p0, v9

    .line 1185
    const/4 v3, 0x1

    aput v6, p0, v3

    .line 1186
    const/4 v3, 0x2

    neg-float v0, v0

    aput v0, p0, v3

    .line 1187
    const/4 v0, 0x3

    const/4 v3, 0x0

    aput v3, p0, v0

    .line 1189
    const/4 v0, 0x4

    aput v4, p0, v0

    .line 1190
    const/4 v0, 0x5

    aput v7, p0, v0

    .line 1191
    const/4 v0, 0x6

    neg-float v1, v1

    aput v1, p0, v0

    .line 1192
    const/4 v0, 0x7

    const/4 v1, 0x0

    aput v1, p0, v0

    .line 1194
    const/16 v0, 0x8

    aput v5, p0, v0

    .line 1195
    const/16 v0, 0x9

    aput v8, p0, v0

    .line 1196
    const/16 v0, 0xa

    neg-float v1, v2

    aput v1, p0, v0

    .line 1197
    const/16 v0, 0xb

    const/4 v1, 0x0

    aput v1, p0, v0

    .line 1199
    const/16 v0, 0xc

    const/4 v1, 0x0

    aput v1, p0, v0

    .line 1200
    const/16 v0, 0xd

    const/4 v1, 0x0

    aput v1, p0, v0

    .line 1201
    const/16 v0, 0xe

    const/4 v1, 0x0

    aput v1, p0, v0

    .line 1202
    const/16 v0, 0xf

    const/high16 v1, 0x3f80

    aput v1, p0, v0

    .line 1204
    const/4 v0, 0x0

    neg-float v1, p1

    neg-float v2, p2

    neg-float v3, p3

    invoke-static {p0, v0, v1, v2, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1205
    return-void
.end method

.method public static c(F)F
    .registers 3
    .parameter

    .prologue
    .line 938
    sget v0, LC/a;->c:F

    cmpg-float v0, p0, v0

    if-gez v0, :cond_15

    .line 941
    const/high16 v0, 0x4880

    div-float v0, p0, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    double-to-float v0, v0

    const v1, 0x42652ee1

    mul-float/2addr v0, v1

    .line 947
    :goto_14
    return v0

    :cond_15
    const/high16 v0, 0x4296

    goto :goto_14
.end method


# virtual methods
.method public A()[F
    .registers 2

    .prologue
    .line 666
    invoke-direct {p0}, LC/a;->K()V

    .line 667
    iget-object v0, p0, LC/a;->z:[F

    if-nez v0, :cond_a

    .line 668
    invoke-direct {p0}, LC/a;->G()V

    .line 670
    :cond_a
    iget-object v0, p0, LC/a;->z:[F

    return-object v0
.end method

.method public B()Lo/aQ;
    .registers 4

    .prologue
    .line 899
    invoke-direct {p0}, LC/a;->K()V

    .line 900
    iget-object v0, p0, LC/a;->v:Lo/aQ;

    if-nez v0, :cond_19

    .line 902
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->d()F

    move-result v0

    iget v1, p0, LC/a;->o:F

    const/high16 v2, 0x3f00

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p0, v0}, LC/a;->d(F)Lo/aQ;

    move-result-object v0

    iput-object v0, p0, LC/a;->v:Lo/aQ;

    .line 905
    :cond_19
    iget-object v0, p0, LC/a;->v:Lo/aQ;

    return-object v0
.end method

.method public C()Lo/aQ;
    .registers 7

    .prologue
    .line 915
    invoke-direct {p0}, LC/a;->K()V

    .line 916
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->d()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_13

    .line 917
    invoke-virtual {p0}, LC/a;->B()Lo/aQ;

    move-result-object v0

    .line 926
    :goto_12
    return-object v0

    .line 921
    :cond_13
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->d()F

    move-result v0

    const v1, 0x3c8efa35

    mul-float/2addr v0, v1

    .line 923
    const/high16 v1, 0x3f00

    iget v2, p0, LC/a;->m:F

    div-float/2addr v1, v2

    .line 924
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    float-to-double v4, v1

    add-double v1, v2, v4

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float v0, v1, v0

    .line 926
    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->d()F

    move-result v1

    const v2, 0x42652ee1

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-virtual {p0, v0}, LC/a;->d(F)Lo/aQ;

    move-result-object v0

    goto :goto_12
.end method

.method public a(FF)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 595
    invoke-direct {p0}, LC/a;->K()V

    .line 596
    mul-float v0, p2, p1

    iget v1, p0, LC/a;->m:F

    iget v2, p0, LC/a;->q:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public a(Lo/T;Z)F
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 624
    invoke-direct {p0}, LC/a;->F()[D

    move-result-object v0

    .line 625
    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v1

    iget-object v2, p0, LC/a;->E:Lo/T;

    invoke-static {p1, v1, v2}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 626
    if-eqz p2, :cond_16

    .line 627
    iget-object v1, p0, LC/a;->E:Lo/T;

    iget-object v2, p0, LC/a;->E:Lo/T;

    invoke-virtual {v1, v2}, Lo/T;->i(Lo/T;)V

    .line 629
    :cond_16
    iget-object v1, p0, LC/a;->E:Lo/T;

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v1

    int-to-double v1, v1

    const/4 v3, 0x0

    aget-wide v3, v0, v3

    mul-double/2addr v1, v3

    iget-object v3, p0, LC/a;->E:Lo/T;

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    int-to-double v3, v3

    const/4 v5, 0x1

    aget-wide v5, v0, v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    iget-object v3, p0, LC/a;->E:Lo/T;

    invoke-virtual {v3}, Lo/T;->h()I

    move-result v3

    int-to-double v3, v3

    const/4 v5, 0x2

    aget-wide v5, v0, v5

    mul-double/2addr v3, v5

    add-double v0, v1, v3

    double-to-float v0, v0

    return v0
.end method

.method public a(FFFF)Lo/t;
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 735
    invoke-direct/range {p0 .. p0}, LC/a;->K()V

    .line 736
    move-object/from16 v0, p0

    iget-object v2, v0, LC/a;->C:[F

    if-nez v2, :cond_c

    .line 737
    invoke-direct/range {p0 .. p0}, LC/a;->J()V

    .line 740
    :cond_c
    invoke-virtual/range {p0 .. p0}, LC/a;->t()Lo/T;

    move-result-object v2

    .line 741
    invoke-virtual {v2}, Lo/T;->f()I

    move-result v8

    .line 742
    invoke-virtual {v2}, Lo/T;->g()I

    move-result v9

    .line 743
    invoke-virtual {v2}, Lo/T;->h()I

    move-result v10

    .line 744
    move-object/from16 v0, p0

    iget-object v2, v0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    sub-int v11, v2, v8

    .line 745
    move-object/from16 v0, p0

    iget-object v2, v0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    sub-int v12, v2, v9

    .line 748
    move-object/from16 v0, p0

    iget-object v2, v0, LC/a;->D:[F

    .line 752
    const/4 v3, 0x0

    aput p1, v2, v3

    .line 753
    const/4 v3, 0x1

    aput p3, v2, v3

    .line 754
    const/4 v3, 0x2

    const/high16 v4, 0x3f80

    aput v4, v2, v3

    .line 755
    const/4 v3, 0x3

    const/high16 v4, 0x3f80

    aput v4, v2, v3

    .line 756
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, LC/a;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 759
    const/high16 v3, 0x3f80

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 760
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 761
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 762
    const/4 v13, 0x6

    aget v13, v2, v13

    mul-float/2addr v3, v13

    float-to-double v13, v3

    .line 764
    const-wide/high16 v15, 0x3ff0

    cmpl-double v3, v13, v15

    if-ltz v3, :cond_6c

    .line 766
    const/4 v2, 0x0

    .line 833
    :goto_6b
    return-object v2

    .line 770
    :cond_6c
    const-wide/high16 v15, 0x3ff0

    const-wide/high16 v17, 0x3ff0

    sub-double v13, v17, v13

    div-double v13, v15, v13

    .line 771
    int-to-double v15, v10

    mul-double v3, v4, v15

    int-to-double v15, v11

    add-double/2addr v3, v15

    mul-double/2addr v3, v13

    int-to-double v15, v8

    add-double/2addr v3, v15

    .line 772
    int-to-double v15, v10

    mul-double v5, v6, v15

    int-to-double v15, v12

    add-double/2addr v5, v15

    mul-double/2addr v5, v13

    int-to-double v13, v9

    add-double/2addr v5, v13

    .line 773
    new-instance v13, Lo/T;

    double-to-int v3, v3

    double-to-int v4, v5

    invoke-direct {v13, v3, v4}, Lo/T;-><init>(II)V

    .line 779
    const/4 v3, 0x0

    aput p2, v2, v3

    .line 780
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, LC/a;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 781
    const/high16 v3, 0x3f80

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 782
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 783
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 784
    const/4 v14, 0x6

    aget v14, v2, v14

    mul-float/2addr v3, v14

    float-to-double v14, v3

    .line 786
    const-wide/high16 v16, 0x3ff0

    cmpl-double v3, v14, v16

    if-ltz v3, :cond_b6

    .line 787
    const/4 v2, 0x0

    goto :goto_6b

    .line 789
    :cond_b6
    const-wide/high16 v16, 0x3ff0

    const-wide/high16 v18, 0x3ff0

    sub-double v14, v18, v14

    div-double v14, v16, v14

    .line 790
    int-to-double v0, v10

    move-wide/from16 v16, v0

    mul-double v3, v4, v16

    int-to-double v0, v11

    move-wide/from16 v16, v0

    add-double v3, v3, v16

    mul-double/2addr v3, v14

    int-to-double v0, v8

    move-wide/from16 v16, v0

    add-double v3, v3, v16

    .line 791
    int-to-double v0, v10

    move-wide/from16 v16, v0

    mul-double v5, v6, v16

    int-to-double v0, v12

    move-wide/from16 v16, v0

    add-double v5, v5, v16

    mul-double/2addr v5, v14

    int-to-double v14, v9

    add-double/2addr v5, v14

    .line 792
    new-instance v14, Lo/T;

    double-to-int v3, v3

    double-to-int v4, v5

    invoke-direct {v14, v3, v4}, Lo/T;-><init>(II)V

    .line 798
    const/4 v3, 0x1

    aput p4, v2, v3

    .line 799
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, LC/a;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 800
    const/high16 v3, 0x3f80

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 801
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 802
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 803
    const/4 v15, 0x6

    aget v15, v2, v15

    mul-float/2addr v3, v15

    float-to-double v15, v3

    .line 805
    const-wide/high16 v17, 0x3ff0

    cmpl-double v3, v15, v17

    if-ltz v3, :cond_10e

    .line 806
    const/4 v2, 0x0

    goto/16 :goto_6b

    .line 808
    :cond_10e
    const-wide/high16 v17, 0x3ff0

    const-wide/high16 v19, 0x3ff0

    sub-double v15, v19, v15

    div-double v15, v17, v15

    .line 809
    int-to-double v0, v10

    move-wide/from16 v17, v0

    mul-double v3, v4, v17

    int-to-double v0, v11

    move-wide/from16 v17, v0

    add-double v3, v3, v17

    mul-double/2addr v3, v15

    int-to-double v0, v8

    move-wide/from16 v17, v0

    add-double v3, v3, v17

    .line 810
    int-to-double v0, v10

    move-wide/from16 v17, v0

    mul-double v5, v6, v17

    int-to-double v0, v12

    move-wide/from16 v17, v0

    add-double v5, v5, v17

    mul-double/2addr v5, v15

    int-to-double v15, v9

    add-double/2addr v5, v15

    .line 811
    new-instance v15, Lo/T;

    double-to-int v3, v3

    double-to-int v4, v5

    invoke-direct {v15, v3, v4}, Lo/T;-><init>(II)V

    .line 817
    const/4 v3, 0x0

    aput p1, v2, v3

    .line 818
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, LC/a;->C:[F

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v6, v2

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 819
    const/high16 v3, 0x3f80

    const/4 v4, 0x7

    aget v4, v2, v4

    div-float/2addr v3, v4

    .line 820
    const/4 v4, 0x4

    aget v4, v2, v4

    mul-float/2addr v4, v3

    float-to-double v4, v4

    .line 821
    const/4 v6, 0x5

    aget v6, v2, v6

    mul-float/2addr v6, v3

    float-to-double v6, v6

    .line 822
    const/16 v16, 0x6

    aget v2, v2, v16

    mul-float/2addr v2, v3

    float-to-double v2, v2

    .line 824
    const-wide/high16 v16, 0x3ff0

    cmpl-double v16, v2, v16

    if-ltz v16, :cond_167

    .line 825
    const/4 v2, 0x0

    goto/16 :goto_6b

    .line 827
    :cond_167
    const-wide/high16 v16, 0x3ff0

    const-wide/high16 v18, 0x3ff0

    sub-double v2, v18, v2

    div-double v2, v16, v2

    .line 828
    int-to-double v0, v10

    move-wide/from16 v16, v0

    mul-double v4, v4, v16

    int-to-double v0, v11

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    mul-double/2addr v4, v2

    int-to-double v0, v8

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    .line 829
    int-to-double v10, v10

    mul-double/2addr v6, v10

    int-to-double v10, v12

    add-double/2addr v6, v10

    mul-double/2addr v2, v6

    int-to-double v6, v9

    add-double/2addr v2, v6

    .line 830
    new-instance v6, Lo/T;

    double-to-int v4, v4

    double-to-int v2, v2

    invoke-direct {v6, v4, v2}, Lo/T;-><init>(II)V

    .line 833
    invoke-static {v6, v15, v13, v14}, Lo/t;->a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/t;

    move-result-object v2

    goto/16 :goto_6b
.end method

.method public a(IIF)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 329
    invoke-direct {p0}, LC/a;->K()V

    .line 330
    sget-object v0, LC/a;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, LC/a;->G:J

    .line 331
    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LC/a;->p:I

    .line 332
    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LC/a;->q:I

    .line 333
    iput p3, p0, LC/a;->r:F

    .line 334
    invoke-direct {p0}, LC/a;->E()V

    .line 335
    invoke-direct {p0}, LC/a;->D()V

    .line 336
    invoke-direct {p0, v2}, LC/a;->b(Z)V

    .line 337
    return-void
.end method

.method public a(LC/b;)V
    .registers 2
    .parameter

    .prologue
    .line 396
    invoke-direct {p0}, LC/a;->K()V

    .line 397
    invoke-direct {p0, p1}, LC/a;->b(LC/b;)V

    .line 398
    return-void
.end method

.method public a(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 276
    invoke-direct {p0}, LC/a;->K()V

    .line 277
    iget-object v0, p0, LC/a;->k:Lo/T;

    invoke-virtual {p1, v0}, Lo/T;->b(Lo/T;)V

    .line 278
    return-void
.end method

.method public a(Lo/T;[F)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4000

    const/4 v1, 0x4

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 866
    iget-object v0, p0, LC/a;->B:[F

    if-nez v0, :cond_c

    .line 867
    invoke-direct {p0}, LC/a;->J()V

    .line 870
    :cond_c
    invoke-virtual {p1}, Lo/T;->f()I

    move-result v0

    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    sub-int/2addr v0, v2

    .line 871
    const/high16 v2, 0x2000

    if-lt v0, v2, :cond_5b

    .line 872
    sub-int/2addr v0, v4

    .line 880
    :cond_1c
    :goto_1c
    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->h()I

    move-result v2

    .line 881
    int-to-float v0, v0

    aput v0, p2, v3

    .line 882
    invoke-virtual {p1}, Lo/T;->g()I

    move-result v0

    iget-object v4, p0, LC/a;->k:Lo/T;

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v0, v0

    aput v0, p2, v6

    .line 883
    const/4 v0, 0x2

    invoke-virtual {p1}, Lo/T;->h()I

    move-result v4

    int-to-float v4, v4

    aput v4, p2, v0

    .line 884
    const/4 v0, 0x3

    int-to-float v2, v2

    aput v2, p2, v0

    .line 887
    iget-object v2, p0, LC/a;->B:[F

    move-object v0, p2

    move-object v4, p2

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 890
    const/high16 v0, 0x3f80

    const/4 v2, 0x7

    aget v2, p2, v2

    div-float/2addr v0, v2

    .line 891
    aget v1, p2, v1

    mul-float/2addr v1, v0

    aput v1, p2, v3

    .line 892
    const/4 v1, 0x5

    aget v1, p2, v1

    mul-float/2addr v0, v1

    aput v0, p2, v6

    .line 893
    return-void

    .line 873
    :cond_5b
    const/high16 v2, -0x2000

    if-ge v0, v2, :cond_1c

    .line 874
    add-int/2addr v0, v4

    goto :goto_1c
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 367
    iget-boolean v0, p0, LC/a;->j:Z

    if-eq v0, p1, :cond_e

    .line 368
    iput-boolean p1, p0, LC/a;->j:Z

    .line 369
    sget-object v0, LC/a;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, LC/a;->G:J

    .line 371
    :cond_e
    return-void
.end method

.method public b(FF)F
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 610
    invoke-direct {p0}, LC/a;->K()V

    .line 611
    iget v0, p0, LC/a;->m:F

    mul-float/2addr v0, p1

    iget v1, p0, LC/a;->q:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    div-float/2addr v0, p2

    return v0
.end method

.method public b(F)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const-wide/high16 v4, 0x3fe0

    .line 353
    invoke-static {v6, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x42b4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 354
    iget v1, p0, LC/a;->o:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_4c

    .line 355
    iput v0, p0, LC/a;->o:F

    .line 356
    sget-object v0, LC/a;->F:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, LC/a;->G:J

    .line 357
    iget v0, p0, LC/a;->o:F

    float-to-double v0, v0

    const-wide v2, 0x3f91df46a2529d39L

    mul-double/2addr v0, v2

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    div-double v0, v4, v0

    double-to-float v0, v0

    iput v0, p0, LC/a;->m:F

    .line 358
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LC/a;->b(Z)V

    .line 359
    iget-object v0, p0, LC/a;->i:LC/b;

    if-eqz v0, :cond_4c

    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->f()F

    move-result v0

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_4c

    .line 360
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LC/a;->k:Lo/T;

    .line 361
    invoke-direct {p0}, LC/a;->D()V

    .line 364
    :cond_4c
    return-void
.end method

.method public b(Lo/T;)[I
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 845
    invoke-direct {p0}, LC/a;->K()V

    .line 846
    const/16 v0, 0x8

    new-array v0, v0, [F

    .line 847
    invoke-virtual {p0, p1, v0}, LC/a;->a(Lo/T;[F)V

    .line 848
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 849
    aget v2, v0, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v1, v3

    .line 850
    aget v0, v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, v1, v4

    .line 851
    return-object v1
.end method

.method public c(FF)F
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 638
    const/high16 v0, 0x41f0

    div-float v1, p1, p2

    const/high16 v2, 0x4380

    iget v3, p0, LC/a;->r:F

    mul-float/2addr v2, v3

    mul-float/2addr v1, v2

    invoke-static {v1}, LC/a;->a(F)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public d(FF)Lo/T;
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x0

    const/high16 v6, 0x3f80

    const-wide/high16 v9, 0x3ff0

    .line 679
    invoke-direct {p0}, LC/a;->K()V

    .line 680
    iget-object v0, p0, LC/a;->C:[F

    if-nez v0, :cond_10

    .line 681
    invoke-direct {p0}, LC/a;->J()V

    .line 684
    :cond_10
    iget-object v0, p0, LC/a;->D:[F

    .line 688
    aput p1, v0, v3

    .line 689
    const/4 v2, 0x1

    aput p2, v0, v2

    .line 690
    const/4 v2, 0x2

    aput v6, v0, v2

    .line 691
    const/4 v2, 0x3

    aput v6, v0, v2

    .line 692
    iget-object v2, p0, LC/a;->C:[F

    move-object v4, v0

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 697
    const/4 v2, 0x7

    aget v2, v0, v2

    div-float v2, v6, v2

    .line 698
    aget v1, v0, v1

    mul-float/2addr v1, v2

    float-to-double v3, v1

    .line 699
    const/4 v1, 0x5

    aget v1, v0, v1

    mul-float/2addr v1, v2

    float-to-double v5, v1

    .line 700
    const/4 v1, 0x6

    aget v0, v0, v1

    mul-float/2addr v0, v2

    float-to-double v0, v0

    .line 702
    cmpl-double v2, v0, v9

    if-ltz v2, :cond_3d

    .line 704
    const/4 v0, 0x0

    .line 716
    :goto_3c
    return-object v0

    .line 706
    :cond_3d
    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v2

    .line 707
    invoke-virtual {v2}, Lo/T;->f()I

    move-result v7

    .line 708
    invoke-virtual {v2}, Lo/T;->g()I

    move-result v8

    .line 709
    invoke-virtual {v2}, Lo/T;->h()I

    move-result v2

    .line 713
    sub-double v0, v9, v0

    div-double v0, v9, v0

    .line 714
    int-to-double v9, v2

    mul-double/2addr v3, v9

    iget-object v9, p0, LC/a;->k:Lo/T;

    invoke-virtual {v9}, Lo/T;->f()I

    move-result v9

    int-to-double v9, v9

    add-double/2addr v3, v9

    int-to-double v9, v7

    sub-double/2addr v3, v9

    mul-double/2addr v3, v0

    int-to-double v9, v7

    add-double/2addr v3, v9

    .line 715
    int-to-double v9, v2

    mul-double/2addr v5, v9

    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    int-to-double v9, v2

    add-double/2addr v5, v9

    int-to-double v9, v8

    sub-double/2addr v5, v9

    mul-double/2addr v0, v5

    int-to-double v5, v8

    add-double v1, v0, v5

    .line 716
    new-instance v0, Lo/T;

    double-to-int v3, v3

    double-to-int v1, v1

    invoke-direct {v0, v3, v1}, Lo/T;-><init>(II)V

    goto :goto_3c
.end method

.method public d(F)Lo/aQ;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 958
    invoke-direct {p0}, LC/a;->K()V

    .line 962
    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->h()I

    move-result v0

    int-to-float v0, v0

    .line 963
    invoke-static {v0}, LC/a;->c(F)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 965
    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->d()F

    move-result v1

    sub-float v1, v0, v1

    .line 966
    const v2, 0x3c8efa35

    mul-float/2addr v1, v2

    .line 974
    iget v2, p0, LC/a;->q:I

    int-to-float v2, v2

    iget v3, p0, LC/a;->m:F

    mul-float/2addr v2, v3

    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v1, v3

    mul-float/2addr v1, v2

    .line 976
    iget v2, p0, LC/a;->q:I

    int-to-float v2, v2

    const/high16 v3, 0x3f00

    mul-float/2addr v2, v3

    sub-float v1, v2, v1

    invoke-static {v1}, Landroid/util/FloatMath;->ceil(F)F

    move-result v1

    .line 979
    iget v2, p0, LC/a;->q:I

    int-to-float v2, v2

    invoke-virtual {p0, v5, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v2

    .line 980
    iget v3, p0, LC/a;->p:I

    int-to-float v3, v3

    iget v4, p0, LC/a;->q:I

    int-to-float v4, v4

    invoke-virtual {p0, v3, v4}, LC/a;->d(FF)Lo/T;

    move-result-object v3

    .line 982
    invoke-virtual {p0, v5, v1}, LC/a;->d(FF)Lo/T;

    move-result-object v4

    .line 983
    iget v5, p0, LC/a;->p:I

    int-to-float v5, v5

    invoke-virtual {p0, v5, v1}, LC/a;->d(FF)Lo/T;

    move-result-object v5

    .line 986
    if-eqz v2, :cond_5e

    if-eqz v3, :cond_5e

    if-eqz v4, :cond_5e

    if-nez v5, :cond_a9

    .line 987
    :cond_5e
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LC/a;->i:LC/b;

    invoke-virtual {v4}, LC/b;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " farAngle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " size: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LC/a;->p:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LC/a;->q:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " top:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 992
    :cond_a9
    invoke-static {v2, v3, v4, v5}, Lo/aQ;->a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/aQ;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1024
    invoke-direct {p0}, LC/a;->K()V

    .line 1025
    if-ne p0, p1, :cond_8

    .line 1038
    :cond_7
    :goto_7
    return v0

    .line 1028
    :cond_8
    instance-of v2, p1, LC/a;

    if-eqz v2, :cond_3e

    .line 1029
    check-cast p1, LC/a;

    .line 1030
    iget v2, p0, LC/a;->l:F

    iget v3, p1, LC/a;->l:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3c

    iget-object v2, p0, LC/a;->i:LC/b;

    iget-object v3, p1, LC/a;->i:LC/b;

    invoke-virtual {v2, v3}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3c

    iget v2, p0, LC/a;->o:F

    iget v3, p1, LC/a;->o:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3c

    iget v2, p0, LC/a;->q:I

    iget v3, p1, LC/a;->q:I

    if-ne v2, v3, :cond_3c

    iget v2, p0, LC/a;->p:I

    iget v3, p1, LC/a;->p:I

    if-ne v2, v3, :cond_3c

    iget v2, p0, LC/a;->r:F

    iget v3, p1, LC/a;->r:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_7

    :cond_3c
    move v0, v1

    goto :goto_7

    :cond_3e
    move v0, v1

    .line 1038
    goto :goto_7
.end method

.method public g()J
    .registers 3

    .prologue
    .line 256
    invoke-direct {p0}, LC/a;->K()V

    .line 257
    iget-wide v0, p0, LC/a;->G:J

    return-wide v0
.end method

.method public h()Lo/T;
    .registers 2

    .prologue
    .line 267
    invoke-direct {p0}, LC/a;->K()V

    .line 268
    iget-object v0, p0, LC/a;->k:Lo/T;

    invoke-static {v0}, Lo/T;->a(Lo/T;)Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 1010
    .line 1012
    iget v0, p0, LC/a;->l:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 1013
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LC/a;->q:I

    add-int/2addr v0, v1

    .line 1014
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LC/a;->p:I

    add-int/2addr v0, v1

    .line 1015
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LC/a;->o:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 1016
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LC/a;->r:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 1017
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, LC/a;->i:LC/b;

    if-nez v0, :cond_2d

    const/4 v0, 0x0

    :goto_2b
    add-int/2addr v0, v1

    .line 1019
    return v0

    .line 1017
    :cond_2d
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->hashCode()I

    move-result v0

    goto :goto_2b
.end method

.method public i()Lo/T;
    .registers 2

    .prologue
    .line 282
    invoke-direct {p0}, LC/a;->K()V

    .line 283
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->c()Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 294
    iget-boolean v0, p0, LC/a;->j:Z

    return v0
.end method

.method public k()I
    .registers 2

    .prologue
    .line 299
    invoke-direct {p0}, LC/a;->K()V

    .line 300
    iget v0, p0, LC/a;->p:I

    return v0
.end method

.method public l()I
    .registers 2

    .prologue
    .line 305
    invoke-direct {p0}, LC/a;->K()V

    .line 306
    iget v0, p0, LC/a;->q:I

    return v0
.end method

.method public m()F
    .registers 2

    .prologue
    .line 316
    invoke-direct {p0}, LC/a;->K()V

    .line 317
    iget v0, p0, LC/a;->r:F

    return v0
.end method

.method public n()F
    .registers 2

    .prologue
    .line 322
    invoke-direct {p0}, LC/a;->K()V

    .line 323
    iget v0, p0, LC/a;->o:F

    return v0
.end method

.method public o()F
    .registers 2

    .prologue
    .line 344
    invoke-direct {p0}, LC/a;->K()V

    .line 345
    iget v0, p0, LC/a;->l:F

    return v0
.end method

.method public p()F
    .registers 2

    .prologue
    .line 433
    invoke-direct {p0}, LC/a;->K()V

    .line 434
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->e()F

    move-result v0

    return v0
.end method

.method public q()F
    .registers 2

    .prologue
    .line 438
    invoke-direct {p0}, LC/a;->K()V

    .line 439
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->d()F

    move-result v0

    return v0
.end method

.method public r()F
    .registers 2

    .prologue
    .line 443
    invoke-direct {p0}, LC/a;->K()V

    .line 444
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    return v0
.end method

.method public s()F
    .registers 2

    .prologue
    .line 451
    invoke-direct {p0}, LC/a;->K()V

    .line 452
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    return v0
.end method

.method public t()Lo/T;
    .registers 9

    .prologue
    .line 488
    invoke-direct {p0}, LC/a;->K()V

    .line 489
    iget-object v0, p0, LC/a;->s:Lo/T;

    if-nez v0, :cond_42

    .line 490
    invoke-direct {p0}, LC/a;->F()[D

    move-result-object v0

    .line 491
    new-instance v1, Lo/T;

    iget v2, p0, LC/a;->l:F

    neg-float v2, v2

    float-to-double v2, v2

    const/4 v4, 0x0

    aget-wide v4, v0, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iget v3, p0, LC/a;->l:F

    neg-float v3, v3

    float-to-double v3, v3

    const/4 v5, 0x1

    aget-wide v5, v0, v5

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    iget v4, p0, LC/a;->l:F

    neg-float v4, v4

    float-to-double v4, v4

    const/4 v6, 0x2

    aget-wide v6, v0, v6

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v1, v2, v3, v0}, Lo/T;-><init>(III)V

    iput-object v1, p0, LC/a;->s:Lo/T;

    .line 495
    iget-object v0, p0, LC/a;->s:Lo/T;

    iget-object v1, p0, LC/a;->k:Lo/T;

    iget-object v2, p0, LC/a;->s:Lo/T;

    invoke-static {v0, v1, v2}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 497
    :cond_42
    iget-object v0, p0, LC/a;->s:Lo/T;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 997
    new-instance v0, Lo/T;

    iget-object v1, p0, LC/a;->k:Lo/T;

    invoke-virtual {v1}, Lo/T;->f()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, LC/a;->k:Lo/T;

    invoke-virtual {v3}, Lo/T;->h()I

    move-result v3

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lo/T;-><init>(III)V

    .line 1000
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LC/a;->l:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->e()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LC/a;->o:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Lo/T;
    .registers 9

    .prologue
    const-wide/high16 v6, 0x40f0

    const-wide v4, 0x4076800000000000L

    .line 505
    invoke-direct {p0}, LC/a;->K()V

    .line 506
    iget-object v0, p0, LC/a;->u:Lo/T;

    if-nez v0, :cond_5d

    .line 507
    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v0

    .line 508
    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->d()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_35

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v1

    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    if-ne v1, v2, :cond_66

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v1

    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    if-ne v1, v2, :cond_66

    .line 513
    :cond_35
    iget-object v0, p0, LC/a;->i:LC/b;

    invoke-virtual {v0}, LC/b;->e()F

    move-result v0

    neg-float v0, v0

    float-to-double v0, v0

    .line 514
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_60

    .line 515
    add-double/2addr v0, v4

    .line 519
    :cond_44
    :goto_44
    const-wide v2, 0x3f91df46a2529d39L

    mul-double/2addr v0, v2

    .line 520
    new-instance v2, Lo/T;

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v3, v6

    double-to-int v3, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v6

    double-to-int v0, v0

    invoke-direct {v2, v3, v0}, Lo/T;-><init>(II)V

    iput-object v2, p0, LC/a;->u:Lo/T;

    .line 530
    :cond_5d
    :goto_5d
    iget-object v0, p0, LC/a;->u:Lo/T;

    return-object v0

    .line 516
    :cond_60
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_44

    .line 517
    sub-double/2addr v0, v4

    goto :goto_44

    .line 524
    :cond_66
    iget-object v1, p0, LC/a;->k:Lo/T;

    invoke-virtual {v1, v0}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    .line 525
    sget-object v1, LC/a;->f:Lo/T;

    invoke-virtual {v0, v1}, Lo/T;->g(Lo/T;)Lo/T;

    move-result-object v0

    .line 526
    const/high16 v1, 0x4780

    invoke-static {v0, v1, v0}, Lo/T;->b(Lo/T;FLo/T;)V

    .line 527
    iput-object v0, p0, LC/a;->u:Lo/T;

    goto :goto_5d
.end method

.method public v()Lo/T;
    .registers 11

    .prologue
    const-wide/high16 v8, 0x40f0

    const-wide/16 v6, 0x0

    const-wide v4, 0x4076800000000000L

    .line 537
    invoke-direct {p0}, LC/a;->K()V

    .line 538
    iget-object v0, p0, LC/a;->t:Lo/T;

    if-nez v0, :cond_62

    .line 539
    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v0

    .line 540
    iget-object v1, p0, LC/a;->i:LC/b;

    invoke-virtual {v1}, LC/b;->d()F

    move-result v1

    float-to-double v1, v1

    cmpl-double v1, v1, v6

    if-eqz v1, :cond_37

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v1

    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    if-ne v1, v2, :cond_6b

    invoke-virtual {v0}, Lo/T;->g()I

    move-result v1

    iget-object v2, p0, LC/a;->k:Lo/T;

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    if-ne v1, v2, :cond_6b

    .line 545
    :cond_37
    const-wide v0, 0x4056800000000000L

    iget-object v2, p0, LC/a;->i:LC/b;

    invoke-virtual {v2}, LC/b;->e()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    .line 546
    cmpg-double v2, v0, v6

    if-gez v2, :cond_65

    .line 547
    add-double/2addr v0, v4

    .line 551
    :cond_49
    :goto_49
    const-wide v2, 0x3f91df46a2529d39L

    mul-double/2addr v0, v2

    .line 552
    new-instance v2, Lo/T;

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v3, v8

    double-to-int v3, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v8

    double-to-int v0, v0

    invoke-direct {v2, v3, v0}, Lo/T;-><init>(II)V

    iput-object v2, p0, LC/a;->t:Lo/T;

    .line 563
    :cond_62
    :goto_62
    iget-object v0, p0, LC/a;->t:Lo/T;

    return-object v0

    .line 548
    :cond_65
    cmpl-double v2, v0, v4

    if-ltz v2, :cond_49

    .line 549
    sub-double/2addr v0, v4

    goto :goto_49

    .line 555
    :cond_6b
    iget-object v1, p0, LC/a;->k:Lo/T;

    invoke-virtual {v1, v0}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    .line 557
    const/high16 v1, 0x4680

    invoke-static {v0, v1, v0}, Lo/T;->b(Lo/T;FLo/T;)V

    .line 558
    invoke-virtual {p0}, LC/a;->u()Lo/T;

    move-result-object v1

    invoke-virtual {v1, v0}, Lo/T;->g(Lo/T;)Lo/T;

    move-result-object v0

    .line 559
    const/high16 v1, 0x4780

    invoke-static {v0, v1, v0}, Lo/T;->b(Lo/T;FLo/T;)V

    .line 560
    iput-object v0, p0, LC/a;->t:Lo/T;

    goto :goto_62
.end method

.method public w()F
    .registers 3

    .prologue
    .line 571
    iget v0, p0, LC/a;->w:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_15

    .line 572
    const/high16 v0, 0x3f80

    invoke-virtual {p0}, LC/a;->t()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->h()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, LC/a;->w:F

    .line 574
    :cond_15
    iget v0, p0, LC/a;->w:F

    return v0
.end method

.method public x()F
    .registers 3

    .prologue
    .line 581
    invoke-direct {p0}, LC/a;->K()V

    .line 582
    iget v0, p0, LC/a;->p:I

    int-to-float v0, v0

    const/high16 v1, 0x3f80

    mul-float/2addr v0, v1

    iget v1, p0, LC/a;->q:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public y()F
    .registers 3

    .prologue
    .line 646
    const/high16 v0, 0x3f80

    iget v1, p0, LC/a;->l:F

    invoke-virtual {p0, v0, v1}, LC/a;->a(FF)F

    move-result v0

    return v0
.end method

.method public z()[F
    .registers 2

    .prologue
    .line 654
    invoke-direct {p0}, LC/a;->K()V

    .line 655
    iget-object v0, p0, LC/a;->y:[F

    if-nez v0, :cond_a

    .line 656
    invoke-direct {p0}, LC/a;->H()V

    .line 658
    :cond_a
    iget-object v0, p0, LC/a;->y:[F

    return-object v0
.end method
