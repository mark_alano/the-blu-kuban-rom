.class public final enum LF/n;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LF/n;

.field public static final enum b:LF/n;

.field public static final enum c:LF/n;

.field private static final synthetic d:[LF/n;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, LF/n;

    const-string v1, "NAVIGATION_LOCATION_KNOWN"

    invoke-direct {v0, v1, v2}, LF/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/n;->a:LF/n;

    .line 32
    new-instance v0, LF/n;

    const-string v1, "NAVIGATION_LOCATION_LOST"

    invoke-direct {v0, v1, v3}, LF/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/n;->b:LF/n;

    .line 33
    new-instance v0, LF/n;

    const-string v1, "NAVIGATION_OFF_ROUTE"

    invoke-direct {v0, v1, v4}, LF/n;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/n;->c:LF/n;

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [LF/n;

    sget-object v1, LF/n;->a:LF/n;

    aput-object v1, v0, v2

    sget-object v1, LF/n;->b:LF/n;

    aput-object v1, v0, v3

    sget-object v1, LF/n;->c:LF/n;

    aput-object v1, v0, v4

    sput-object v0, LF/n;->d:[LF/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LF/n;
    .registers 2
    .parameter

    .prologue
    .line 30
    const-class v0, LF/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LF/n;

    return-object v0
.end method

.method public static values()[LF/n;
    .registers 1

    .prologue
    .line 30
    sget-object v0, LF/n;->d:[LF/n;

    invoke-virtual {v0}, [LF/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LF/n;

    return-object v0
.end method
