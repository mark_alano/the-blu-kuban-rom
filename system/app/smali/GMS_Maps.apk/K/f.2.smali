.class LK/f;
.super LK/a;
.source "SourceFile"


# instance fields
.field private a:LK/a;

.field private b:Ljava/lang/String;

.field private c:Ljava/io/File;

.field private final d:Landroid/os/Handler;

.field private e:I

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Landroid/os/Handler;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 213
    invoke-direct {p0}, LK/a;-><init>()V

    .line 214
    iput-object p1, p0, LK/f;->f:Landroid/content/Context;

    .line 215
    iput-object p2, p0, LK/f;->b:Ljava/lang/String;

    .line 216
    iput-object p3, p0, LK/f;->c:Ljava/io/File;

    .line 217
    iput-object p4, p0, LK/f;->d:Landroid/os/Handler;

    .line 218
    return-void
.end method

.method static synthetic a(LK/f;LK/a;)LK/a;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 205
    iput-object p1, p0, LK/f;->a:LK/a;

    return-object p1
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 249
    iget-object v0, p0, LK/f;->a:LK/a;

    if-eqz v0, :cond_9

    .line 250
    iget-object v0, p0, LK/f;->a:LK/a;

    invoke-virtual {v0}, LK/a;->a()V

    .line 252
    :cond_9
    return-void
.end method

.method public a(LK/b;)V
    .registers 5
    .parameter

    .prologue
    .line 222
    iget-object v0, p0, LK/f;->c:Ljava/io/File;

    if-nez v0, :cond_f

    .line 225
    const-string v0, "AlertGenerator"

    const-string v1, "mFile=null"

    invoke-static {v0, v1}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-interface {p1, p0}, LK/b;->a(LK/a;)V

    .line 243
    :goto_e
    return-void

    .line 230
    :cond_f
    iget-object v0, p0, LK/f;->f:Landroid/content/Context;

    iget-object v1, p0, LK/f;->c:Ljava/io/File;

    iget-object v2, p0, LK/f;->d:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, LK/P;->a(Landroid/content/Context;Ljava/io/File;Landroid/os/Handler;)LK/a;

    move-result-object v0

    iput-object v0, p0, LK/f;->a:LK/a;

    .line 231
    iget-object v0, p0, LK/f;->a:LK/a;

    if-nez v0, :cond_29

    .line 232
    invoke-interface {p1, p0}, LK/b;->a(LK/a;)V

    .line 242
    :goto_22
    iget v0, p0, LK/f;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LK/f;->e:I

    goto :goto_e

    .line 234
    :cond_29
    iget-object v0, p0, LK/f;->a:LK/a;

    new-instance v1, LK/g;

    invoke-direct {v1, p0, p1}, LK/g;-><init>(LK/f;LK/b;)V

    invoke-virtual {v0, v1}, LK/a;->a(LK/b;)V

    goto :goto_22
.end method

.method public b()V
    .registers 2

    .prologue
    .line 256
    iget-object v0, p0, LK/f;->a:LK/a;

    if-eqz v0, :cond_9

    .line 257
    iget-object v0, p0, LK/f;->a:LK/a;

    invoke-virtual {v0}, LK/a;->b()V

    .line 259
    :cond_9
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 270
    iput-object v0, p0, LK/f;->c:Ljava/io/File;

    .line 271
    iput-object v0, p0, LK/f;->b:Ljava/lang/String;

    .line 272
    return-void
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 275
    iget-object v0, p0, LK/f;->b:Ljava/lang/String;

    return-object v0
.end method
