.class public final enum Lm/m;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lm/m;

.field public static final enum b:Lm/m;

.field public static final enum c:Lm/m;

.field private static final synthetic d:[Lm/m;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    new-instance v0, Lm/m;

    const-string v1, "EMPTY_MESH"

    invoke-direct {v0, v1, v2}, Lm/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/m;->a:Lm/m;

    new-instance v0, Lm/m;

    const-string v1, "TESSELLATE_MONOTONE"

    invoke-direct {v0, v1, v3}, Lm/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/m;->b:Lm/m;

    new-instance v0, Lm/m;

    const-string v1, "CUT_AND_TESSELLATE"

    invoke-direct {v0, v1, v4}, Lm/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/m;->c:Lm/m;

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [Lm/m;

    sget-object v1, Lm/m;->a:Lm/m;

    aput-object v1, v0, v2

    sget-object v1, Lm/m;->b:Lm/m;

    aput-object v1, v0, v3

    sget-object v1, Lm/m;->c:Lm/m;

    aput-object v1, v0, v4

    sput-object v0, Lm/m;->d:[Lm/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lm/m;
    .registers 2
    .parameter

    .prologue
    .line 72
    const-class v0, Lm/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lm/m;

    return-object v0
.end method

.method public static values()[Lm/m;
    .registers 1

    .prologue
    .line 72
    sget-object v0, Lm/m;->d:[Lm/m;

    invoke-virtual {v0}, [Lm/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lm/m;

    return-object v0
.end method
