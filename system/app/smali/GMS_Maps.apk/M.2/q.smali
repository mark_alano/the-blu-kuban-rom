.class public final enum Lm/q;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lm/q;

.field public static final enum b:Lm/q;

.field public static final enum c:Lm/q;

.field public static final enum d:Lm/q;

.field public static final enum e:Lm/q;

.field private static final synthetic f:[Lm/q;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lm/q;

    const-string v1, "NO_VERIFICATION"

    invoke-direct {v0, v1, v2}, Lm/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/q;->a:Lm/q;

    .line 41
    new-instance v0, Lm/q;

    const-string v1, "NO_VERIFICATION_WITH_REASON"

    invoke-direct {v0, v1, v3}, Lm/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/q;->b:Lm/q;

    .line 42
    new-instance v0, Lm/q;

    const-string v1, "AREA_VERIFICATION"

    invoke-direct {v0, v1, v4}, Lm/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/q;->c:Lm/q;

    .line 43
    new-instance v0, Lm/q;

    const-string v1, "AREA_VERIFICATION_WITH_REASON"

    invoke-direct {v0, v1, v5}, Lm/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/q;->d:Lm/q;

    .line 44
    new-instance v0, Lm/q;

    const-string v1, "COMPLETE_VERIFICATION"

    invoke-direct {v0, v1, v6}, Lm/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm/q;->e:Lm/q;

    .line 39
    const/4 v0, 0x5

    new-array v0, v0, [Lm/q;

    sget-object v1, Lm/q;->a:Lm/q;

    aput-object v1, v0, v2

    sget-object v1, Lm/q;->b:Lm/q;

    aput-object v1, v0, v3

    sget-object v1, Lm/q;->c:Lm/q;

    aput-object v1, v0, v4

    sget-object v1, Lm/q;->d:Lm/q;

    aput-object v1, v0, v5

    sget-object v1, Lm/q;->e:Lm/q;

    aput-object v1, v0, v6

    sput-object v0, Lm/q;->f:[Lm/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lm/q;
    .registers 2
    .parameter

    .prologue
    .line 39
    const-class v0, Lm/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lm/q;

    return-object v0
.end method

.method public static values()[Lm/q;
    .registers 1

    .prologue
    .line 39
    sget-object v0, Lm/q;->f:[Lm/q;

    invoke-virtual {v0}, [Lm/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lm/q;

    return-object v0
.end method
