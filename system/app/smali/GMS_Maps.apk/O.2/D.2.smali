.class public LO/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LO/H;


# instance fields
.field private a:LO/z;

.field private b:Lo/T;

.field private c:D

.field private d:D

.field private e:I


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LO/A;)V
    .registers 2
    .parameter

    .prologue
    .line 98
    invoke-direct {p0}, LO/D;-><init>()V

    return-void
.end method

.method static synthetic a(LO/D;D)D
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 98
    iput-wide p1, p0, LO/D;->d:D

    return-wide p1
.end method

.method static synthetic a(LO/D;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 98
    iput p1, p0, LO/D;->e:I

    return p1
.end method

.method static synthetic a(LO/D;LO/z;)LO/z;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, LO/D;->a:LO/z;

    return-object p1
.end method

.method static synthetic a(LO/D;)Lo/T;
    .registers 2
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, LO/D;->b:Lo/T;

    return-object v0
.end method

.method static synthetic a(LO/D;Lo/T;)Lo/T;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, LO/D;->b:Lo/T;

    return-object p1
.end method

.method static synthetic b(LO/D;)D
    .registers 3
    .parameter

    .prologue
    .line 98
    iget-wide v0, p0, LO/D;->d:D

    return-wide v0
.end method

.method static synthetic b(LO/D;D)D
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 98
    iput-wide p1, p0, LO/D;->c:D

    return-wide p1
.end method


# virtual methods
.method public a(D)LO/D;
    .registers 6
    .parameter

    .prologue
    .line 125
    new-instance v0, LO/D;

    invoke-direct {v0}, LO/D;-><init>()V

    .line 126
    iget-object v1, p0, LO/D;->a:LO/z;

    iput-object v1, v0, LO/D;->a:LO/z;

    .line 127
    iget-object v1, p0, LO/D;->b:Lo/T;

    iput-object v1, v0, LO/D;->b:Lo/T;

    .line 128
    iget-wide v1, p0, LO/D;->c:D

    iput-wide v1, v0, LO/D;->c:D

    .line 129
    iput-wide p1, v0, LO/D;->d:D

    .line 130
    iget v1, p0, LO/D;->e:I

    iput v1, v0, LO/D;->e:I

    .line 131
    return-object v0
.end method

.method public a()LO/z;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, LO/D;->a:LO/z;

    return-object v0
.end method

.method public b()Lo/T;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, LO/D;->b:Lo/T;

    return-object v0
.end method

.method public c()D
    .registers 3

    .prologue
    .line 115
    iget-wide v0, p0, LO/D;->c:D

    return-wide v0
.end method

.method public d()D
    .registers 3

    .prologue
    .line 118
    iget-wide v0, p0, LO/D;->d:D

    return-wide v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 121
    iget v0, p0, LO/D;->e:I

    return v0
.end method
