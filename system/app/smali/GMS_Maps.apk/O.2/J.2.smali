.class public LO/J;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Law/p;

.field private final b:LO/t;

.field private final c:LO/L;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object v1, p0, LO/J;->a:Law/p;

    .line 79
    iput-object v1, p0, LO/J;->b:LO/t;

    .line 80
    new-instance v0, LO/L;

    invoke-direct {v0, p0, v1}, LO/L;-><init>(LO/J;LO/K;)V

    iput-object v0, p0, LO/J;->c:LO/L;

    .line 81
    return-void
.end method

.method constructor <init>(Law/p;LO/t;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, LO/J;->a:Law/p;

    .line 85
    iput-object p2, p0, LO/J;->b:LO/t;

    .line 86
    new-instance v0, LO/L;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LO/L;-><init>(LO/J;LO/K;)V

    iput-object v0, p0, LO/J;->c:LO/L;

    .line 87
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, LO/J;->c:LO/L;

    invoke-virtual {v0, v1}, Law/h;->a(Law/q;)V

    .line 88
    return-void
.end method

.method protected static a(LaH/h;)LO/U;
    .registers 20
    .parameter

    .prologue
    .line 325
    invoke-virtual/range {p0 .. p0}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual/range {p0 .. p0}, LaH/h;->hasBearing()Z

    move-result v0

    if-nez v0, :cond_e

    .line 326
    :cond_c
    const/4 v0, 0x0

    .line 414
    :goto_d
    return-object v0

    .line 328
    :cond_e
    invoke-virtual/range {p0 .. p0}, LaH/h;->getSpeed()F

    move-result v0

    const/high16 v1, 0x4120

    mul-float/2addr v0, v1

    float-to-double v0, v0

    .line 329
    const-wide v2, 0x4051800000000000L

    cmpg-double v2, v0, v2

    if-gez v2, :cond_21

    .line 330
    const/4 v0, 0x0

    goto :goto_d

    .line 331
    :cond_21
    const-wide v2, 0x4072c00000000000L

    cmpl-double v2, v0, v2

    if-lez v2, :cond_2f

    .line 332
    const-wide v0, 0x4072c00000000000L

    .line 335
    :cond_2f
    invoke-virtual/range {p0 .. p0}, LaH/h;->l()Lo/af;

    move-result-object v7

    .line 336
    invoke-virtual/range {p0 .. p0}, LaH/h;->i()Z

    move-result v2

    if-eqz v2, :cond_41

    if-eqz v7, :cond_41

    invoke-virtual {v7}, Lo/af;->c()I

    move-result v2

    if-nez v2, :cond_43

    .line 337
    :cond_41
    const/4 v0, 0x0

    goto :goto_d

    .line 339
    :cond_43
    invoke-virtual {v7}, Lo/af;->b()Lo/X;

    move-result-object v9

    .line 340
    invoke-virtual {v9}, Lo/X;->b()I

    move-result v10

    .line 341
    invoke-virtual/range {p0 .. p0}, LaH/h;->getLatitude()D

    move-result-wide v2

    invoke-virtual/range {p0 .. p0}, LaH/h;->getLongitude()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lo/T;->a(DD)Lo/T;

    move-result-object v2

    .line 344
    invoke-virtual {v2}, Lo/T;->b()D

    move-result-wide v3

    invoke-static {v3, v4}, Lo/T;->a(D)D

    move-result-wide v11

    .line 345
    new-instance v3, Lo/T;

    invoke-direct {v3}, Lo/T;-><init>()V

    .line 346
    const/4 v6, 0x0

    :goto_65
    add-int/lit8 v4, v10, -0x1

    if-ge v6, v4, :cond_126

    .line 347
    invoke-virtual {v9, v6}, Lo/X;->a(I)Lo/T;

    move-result-object v8

    .line 348
    add-int/lit8 v4, v6, 0x1

    invoke-virtual {v9, v4}, Lo/X;->a(I)Lo/T;

    move-result-object v13

    .line 350
    invoke-static {v8, v13, v2, v3}, Lo/T;->a(Lo/T;Lo/T;Lo/T;Lo/T;)F

    move-result v4

    float-to-double v4, v4

    .line 355
    const-wide/high16 v14, 0x4014

    cmpg-double v4, v4, v14

    if-gtz v4, :cond_122

    .line 356
    const-wide/16 v4, 0x0

    .line 360
    invoke-static {v8, v13}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v2

    float-to-double v13, v2

    .line 361
    invoke-virtual/range {p0 .. p0}, LaH/h;->getBearing()F

    move-result v2

    float-to-double v15, v2

    .line 362
    sub-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v13

    .line 364
    const-wide v15, 0x4056800000000000L

    cmpg-double v2, v13, v15

    if-ltz v2, :cond_a1

    const-wide v15, 0x4070e00000000000L

    cmpl-double v2, v13, v15

    if-lez v2, :cond_b9

    .line 365
    :cond_a1
    const/4 v2, 0x1

    move v8, v2

    move v2, v6

    .line 375
    :goto_a4
    const/4 v6, 0x1

    if-ne v8, v6, :cond_ad

    invoke-virtual {v7}, Lo/af;->k()Z

    move-result v6

    if-nez v6, :cond_b6

    :cond_ad
    const/4 v6, -0x1

    if-ne v8, v6, :cond_bf

    invoke-virtual {v7}, Lo/af;->n()Z

    move-result v6

    if-eqz v6, :cond_bf

    .line 377
    :cond_b6
    const/4 v0, 0x0

    goto/16 :goto_d

    .line 369
    :cond_b9
    add-int/lit8 v6, v6, 0x1

    .line 370
    const/4 v2, -0x1

    move v8, v2

    move v2, v6

    goto :goto_a4

    :cond_bf
    move v7, v2

    move-object v2, v3

    move-wide/from16 v17, v4

    move-wide/from16 v5, v17

    move-object v4, v3

    .line 383
    :goto_c6
    cmpg-double v13, v5, v0

    if-gez v13, :cond_e6

    if-lez v8, :cond_e4

    add-int/lit8 v13, v10, -0x1

    if-ge v7, v13, :cond_e6

    .line 385
    :cond_d0
    add-int v4, v7, v8

    invoke-virtual {v9, v4}, Lo/X;->a(I)Lo/T;

    move-result-object v4

    .line 386
    invoke-virtual {v2, v4}, Lo/T;->c(Lo/T;)F

    move-result v13

    float-to-double v13, v13

    div-double/2addr v13, v11

    .line 388
    add-double/2addr v5, v13

    .line 390
    add-int/2addr v7, v8

    move-object/from16 v17, v4

    move-object v4, v2

    move-object/from16 v2, v17

    .line 391
    goto :goto_c6

    .line 383
    :cond_e4
    if-gtz v7, :cond_d0

    .line 393
    :cond_e6
    cmpl-double v7, v5, v0

    if-lez v7, :cond_129

    .line 394
    invoke-virtual {v4}, Lo/T;->b()D

    move-result-wide v7

    invoke-static {v7, v8}, Lo/T;->a(D)D

    move-result-wide v7

    sub-double v0, v5, v0

    mul-double/2addr v0, v7

    double-to-float v0, v0

    .line 396
    invoke-virtual {v2, v4}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    .line 397
    invoke-static {v1, v0, v1}, Lo/T;->b(Lo/T;FLo/T;)V

    .line 398
    invoke-virtual {v2, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v0

    .line 400
    :goto_101
    invoke-virtual {v0, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10a

    .line 404
    const/4 v0, 0x0

    goto/16 :goto_d

    .line 406
    :cond_10a
    new-instance v1, Lo/u;

    invoke-virtual {v0}, Lo/T;->a()I

    move-result v2

    invoke-virtual {v0}, Lo/T;->c()I

    move-result v0

    invoke-direct {v1, v2, v0}, Lo/u;-><init>(II)V

    .line 408
    new-instance v0, LO/U;

    invoke-direct {v0, v1}, LO/U;-><init>(Lo/u;)V

    .line 409
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LO/U;->a(I)V

    goto/16 :goto_d

    .line 346
    :cond_122
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_65

    .line 414
    :cond_126
    const/4 v0, 0x0

    goto/16 :goto_d

    :cond_129
    move-object v0, v2

    goto :goto_101
.end method

.method private a(LO/z;ILandroid/location/Location;)LO/g;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 280
    invoke-virtual {p1}, LO/z;->v()[LO/W;

    move-result-object v1

    .line 281
    const/4 v0, 0x0

    .line 282
    array-length v2, v1

    const/4 v3, 0x2

    if-le v2, v3, :cond_14

    aget-object v2, v1, v4

    invoke-virtual {v2}, LO/W;->m()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 283
    aget-object v0, v1, v4

    .line 286
    :cond_14
    new-instance v2, LO/i;

    invoke-virtual {p1}, LO/z;->d()I

    move-result v3

    invoke-direct {v2, v1, v3, p2}, LO/i;-><init>([LO/U;II)V

    invoke-virtual {p1}, LO/z;->e()F

    move-result v1

    invoke-virtual {p1}, LO/z;->f()F

    move-result v3

    invoke-virtual {v2, v1, v3}, LO/i;->a(FF)LO/i;

    move-result-object v1

    invoke-virtual {p1}, LO/z;->j()Z

    move-result v2

    invoke-virtual {v1, v2}, LO/i;->a(Z)LO/i;

    move-result-object v1

    invoke-virtual {v1, v0}, LO/i;->a(LO/U;)LO/i;

    move-result-object v0

    invoke-virtual {p1}, LO/z;->r()I

    move-result v1

    invoke-virtual {v0, v1}, LO/i;->a(I)LO/i;

    move-result-object v0

    invoke-virtual {v0, v4}, LO/i;->b(I)LO/i;

    move-result-object v0

    invoke-virtual {p1}, LO/z;->D()[LO/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/i;->a([LO/b;)LO/i;

    move-result-object v0

    invoke-virtual {v0, p3}, LO/i;->a(Landroid/location/Location;)LO/i;

    move-result-object v0

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    .line 295
    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    .line 296
    return-object v0
.end method

.method static synthetic a(LO/J;)LO/t;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LO/J;->b:LO/t;

    return-object v0
.end method

.method private b(LaH/h;)F
    .registers 3
    .parameter

    .prologue
    .line 422
    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p1}, LaH/h;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 424
    invoke-virtual {p1}, LaH/h;->getBearing()F

    move-result v0

    .line 426
    :goto_16
    return v0

    :cond_17
    const/high16 v0, -0x4080

    goto :goto_16
.end method

.method private c(LaH/h;)F
    .registers 3
    .parameter

    .prologue
    .line 435
    invoke-virtual {p1}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 436
    invoke-virtual {p1}, LaH/h;->getSpeed()F

    move-result v0

    .line 438
    :goto_a
    return v0

    :cond_b
    const/high16 v0, -0x4080

    goto :goto_a
.end method


# virtual methods
.method public a(LO/z;)LO/g;
    .registers 4
    .parameter

    .prologue
    .line 264
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LO/J;->a(LO/z;ILandroid/location/Location;)LO/g;

    move-result-object v0

    return-object v0
.end method

.method public a(LO/z;Landroid/location/Location;)LO/g;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 254
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, LO/J;->a(LO/z;ILandroid/location/Location;)LO/g;

    move-result-object v0

    return-object v0
.end method

.method public a(LaH/h;LO/z;I)LO/g;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 206
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 208
    new-instance v0, LO/U;

    invoke-virtual {p1}, LaH/h;->q()Lo/u;

    move-result-object v1

    invoke-direct {v0, v1}, LO/U;-><init>(Lo/u;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    const/4 v0, 0x0

    .line 211
    invoke-virtual {p2}, LO/z;->d()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_a3

    const/4 v1, 0x4

    if-ne p3, v1, :cond_a3

    .line 213
    invoke-static {p1}, LO/J;->a(LaH/h;)LO/U;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_25

    .line 215
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_25
    move-object v1, v0

    .line 219
    :goto_26
    const-wide/16 v2, 0x0

    .line 220
    invoke-virtual {p1}, LaH/h;->n()LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    .line 222
    if-eqz v0, :cond_3f

    invoke-virtual {v0}, LO/D;->a()LO/z;

    move-result-object v5

    if-ne v5, p2, :cond_3f

    .line 223
    invoke-virtual {p2, v0}, LO/z;->a(LO/D;)D

    move-result-wide v2

    invoke-virtual {v0}, LO/D;->d()D

    move-result-wide v5

    add-double/2addr v2, v5

    .line 226
    :cond_3f
    const-wide/high16 v5, 0x4069

    invoke-virtual {p1}, LaH/h;->getLatitude()D

    move-result-wide v7

    invoke-static {v7, v8}, Lo/T;->a(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    add-double/2addr v2, v5

    invoke-virtual {p2, v2, v3}, LO/z;->c(D)Ljava/util/Collection;

    move-result-object v0

    .line 229
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 230
    invoke-virtual {p2}, LO/z;->m()LO/U;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    new-instance v2, LO/i;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [LO/U;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/U;

    invoke-virtual {p2}, LO/z;->d()I

    move-result v3

    invoke-direct {v2, v0, v3, p3}, LO/i;-><init>([LO/U;II)V

    invoke-direct {p0, p1}, LO/J;->b(LaH/h;)F

    move-result v0

    invoke-direct {p0, p1}, LO/J;->c(LaH/h;)F

    move-result v3

    invoke-virtual {v2, v0, v3}, LO/i;->a(FF)LO/i;

    move-result-object v2

    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v0

    if-nez v0, :cond_a1

    const/4 v0, 0x1

    :goto_81
    invoke-virtual {v2, v0}, LO/i;->a(Z)LO/i;

    move-result-object v0

    invoke-virtual {v0, v1}, LO/i;->a(LO/U;)LO/i;

    move-result-object v0

    invoke-virtual {p2}, LO/z;->r()I

    move-result v1

    invoke-virtual {v0, v1}, LO/i;->a(I)LO/i;

    move-result-object v0

    invoke-virtual {p2}, LO/z;->D()[LO/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/i;->a([LO/b;)LO/i;

    move-result-object v0

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    .line 240
    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    .line 241
    return-object v0

    .line 232
    :cond_a1
    const/4 v0, 0x0

    goto :goto_81

    :cond_a3
    move-object v1, v0

    goto :goto_26
.end method

.method public a(LaH/h;[LO/U;II[LO/b;)LO/g;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    const/4 v0, 0x2

    if-ne p3, v0, :cond_42

    move v0, v1

    .line 138
    :goto_6
    array-length v3, p2

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [LO/U;

    .line 140
    new-instance v4, LO/U;

    invoke-virtual {p1}, LaH/h;->q()Lo/u;

    move-result-object v5

    invoke-direct {v4, v5}, LO/U;-><init>(Lo/u;)V

    aput-object v4, v3, v2

    .line 141
    array-length v4, p2

    invoke-static {p2, v2, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    new-instance v4, LO/i;

    invoke-direct {v4, v3, p3, p4}, LO/i;-><init>([LO/U;II)V

    invoke-virtual {p1}, LaH/h;->h()Z

    move-result v3

    if-nez v3, :cond_44

    :goto_25
    invoke-virtual {v4, v1}, LO/i;->a(Z)LO/i;

    move-result-object v1

    invoke-virtual {v1, p5}, LO/i;->a([LO/b;)LO/i;

    move-result-object v1

    .line 148
    if-nez v0, :cond_3a

    .line 149
    invoke-direct {p0, p1}, LO/J;->b(LaH/h;)F

    move-result v0

    invoke-direct {p0, p1}, LO/J;->c(LaH/h;)F

    move-result v2

    invoke-virtual {v1, v0, v2}, LO/i;->a(FF)LO/i;

    .line 151
    :cond_3a
    invoke-virtual {v1}, LO/i;->a()LO/g;

    move-result-object v0

    .line 152
    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    .line 153
    return-object v0

    :cond_42
    move v0, v2

    .line 136
    goto :goto_6

    :cond_44
    move v1, v2

    .line 144
    goto :goto_25
.end method

.method public a([LO/U;I[LO/b;)LO/g;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 188
    new-instance v0, LO/i;

    const/4 v1, 0x7

    invoke-direct {v0, p1, p2, v1}, LO/i;-><init>([LO/U;II)V

    invoke-virtual {v0, p3}, LO/i;->a([LO/b;)LO/i;

    move-result-object v0

    invoke-virtual {v0}, LO/i;->a()LO/g;

    move-result-object v0

    .line 191
    invoke-virtual {p0, v0}, LO/J;->a(LO/g;)V

    .line 192
    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 443
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, LO/J;->c:LO/L;

    invoke-virtual {v0, v1}, Law/h;->b(Law/q;)V

    .line 444
    return-void
.end method

.method protected a(LO/g;)V
    .registers 4
    .parameter

    .prologue
    .line 300
    invoke-virtual {p1}, LO/g;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 302
    new-instance v0, Ll/g;

    const-string v1, "addRequest"

    invoke-direct {v0, v1, p1}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 304
    iget-object v0, p0, LO/J;->a:Law/p;

    invoke-interface {v0, p1}, Law/p;->c(Law/g;)V

    .line 310
    :goto_15
    return-void

    .line 306
    :cond_16
    const-string v0, "Router"

    const-string v1, "Invalid request"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, LO/J;->b:LO/t;

    invoke-virtual {v0, p1}, LO/t;->a(LO/g;)V

    goto :goto_15
.end method
