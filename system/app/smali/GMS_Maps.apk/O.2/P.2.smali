.class public LO/P;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:LO/N;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, LO/P;->a:I

    .line 81
    iput-object p2, p0, LO/P;->c:Ljava/lang/String;

    .line 82
    iput-object p3, p0, LO/P;->d:Ljava/lang/String;

    .line 83
    iput-object p4, p0, LO/P;->e:Ljava/lang/String;

    .line 84
    iput-object p5, p0, LO/P;->f:Ljava/lang/String;

    .line 85
    iput-boolean p6, p0, LO/P;->g:Z

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/P;
    .registers 2
    .parameter

    .prologue
    .line 34
    invoke-static {p0}, LO/P;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/P;

    move-result-object v0

    return-object v0
.end method

.method private a(LO/N;)V
    .registers 2
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, LO/P;->b:LO/N;

    .line 109
    return-void
.end method

.method static synthetic a(LO/P;LO/N;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1}, LO/P;->a(LO/N;)V

    return-void
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/P;
    .registers 9
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 89
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 90
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 91
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 95
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 96
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 98
    :goto_20
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ne v0, v7, :cond_2c

    .line 100
    :goto_26
    new-instance v0, LO/P;

    invoke-direct/range {v0 .. v6}, LO/P;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    .line 98
    :cond_2c
    const/4 v6, 0x0

    goto :goto_26

    :cond_2e
    move-object v4, v5

    goto :goto_20
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 112
    iget v0, p0, LO/P;->a:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, LO/P;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, LO/P;->d:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object v0, p0, LO/P;->c:Ljava/lang/String;

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, LO/P;->d:Ljava/lang/String;

    goto :goto_6
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 124
    iget-object v0, p0, LO/P;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, LO/P;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 132
    iget-boolean v0, p0, LO/P;->g:Z

    return v0
.end method

.method public g()LO/N;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, LO/P;->b:LO/N;

    return-object v0
.end method
