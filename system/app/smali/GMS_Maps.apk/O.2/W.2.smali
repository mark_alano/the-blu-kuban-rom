.class public LO/W;
.super LO/U;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:D

.field private c:D


# direct methods
.method public constructor <init>(LO/U;LO/z;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, LO/U;-><init>(LO/U;)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, LO/W;->a:Z

    .line 25
    invoke-direct {p0, p2}, LO/W;->a(LO/z;)V

    .line 26
    return-void
.end method

.method private a(LO/z;)V
    .registers 7
    .parameter

    .prologue
    const-wide/high16 v3, -0x4010

    .line 35
    invoke-virtual {p0}, LO/W;->c()Lo/u;

    move-result-object v0

    .line 36
    if-nez v0, :cond_d

    .line 37
    iput-wide v3, p0, LO/W;->b:D

    .line 38
    iput-wide v3, p0, LO/W;->c:D

    .line 54
    :goto_c
    return-void

    .line 41
    :cond_d
    invoke-virtual {v0}, Lo/u;->a()I

    move-result v1

    invoke-virtual {v0}, Lo/u;->b()I

    move-result v0

    invoke-static {v1, v0}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    .line 43
    const-wide/high16 v1, 0x4059

    invoke-virtual {p1, v0, v1, v2}, LO/z;->a(Lo/T;D)LO/H;

    move-result-object v0

    check-cast v0, LO/D;

    .line 47
    if-eqz v0, :cond_30

    .line 48
    invoke-virtual {p1, v0}, LO/z;->a(LO/D;)D

    move-result-wide v1

    iput-wide v1, p0, LO/W;->b:D

    .line 49
    invoke-virtual {p1, v0}, LO/z;->b(LO/D;)D

    move-result-wide v0

    iput-wide v0, p0, LO/W;->c:D

    goto :goto_c

    .line 51
    :cond_30
    iput-wide v3, p0, LO/W;->b:D

    .line 52
    iput-wide v3, p0, LO/W;->c:D

    goto :goto_c
.end method


# virtual methods
.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 69
    iput-boolean p1, p0, LO/W;->a:Z

    .line 70
    return-void
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 57
    iget-boolean v0, p0, LO/W;->a:Z

    return v0
.end method

.method public n()D
    .registers 3

    .prologue
    .line 61
    iget-wide v0, p0, LO/W;->b:D

    return-wide v0
.end method
