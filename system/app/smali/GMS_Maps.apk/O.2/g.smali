.class public LO/g;
.super Law/a;
.source "SourceFile"


# static fields
.field private static l:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:[LO/U;

.field private final c:[LO/b;

.field private final d:[B

.field private final e:Z

.field private final f:I

.field private final g:I

.field private final h:F

.field private final i:F

.field private final j:LO/U;

.field private final k:I

.field private m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private n:I

.field private o:[LO/z;

.field private p:[LO/U;

.field private q:[LO/b;

.field private final r:Landroid/location/Location;


# direct methods
.method private constructor <init>([LO/U;IFFZLO/U;III[LO/b;[BLandroid/location/Location;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 156
    invoke-direct {p0}, Law/a;-><init>()V

    .line 105
    const/4 v0, -0x1

    iput v0, p0, LO/g;->n:I

    .line 157
    iput-object p1, p0, LO/g;->b:[LO/U;

    .line 158
    iput p2, p0, LO/g;->a:I

    .line 159
    iput p9, p0, LO/g;->k:I

    .line 160
    iput p3, p0, LO/g;->h:F

    .line 161
    iput p4, p0, LO/g;->i:F

    .line 162
    iput-boolean p5, p0, LO/g;->e:Z

    .line 163
    iput-object p6, p0, LO/g;->j:LO/U;

    .line 164
    iput p8, p0, LO/g;->g:I

    .line 165
    iput-object p10, p0, LO/g;->c:[LO/b;

    .line 166
    iput-object p11, p0, LO/g;->d:[B

    .line 167
    iput p7, p0, LO/g;->f:I

    .line 168
    iput-object p12, p0, LO/g;->r:Landroid/location/Location;

    .line 170
    sget-object v0, LO/g;->l:Ljava/lang/String;

    if-nez v0, :cond_25

    .line 171
    invoke-direct {p0}, LO/g;->v()V

    .line 173
    :cond_25
    return-void
.end method

.method synthetic constructor <init>([LO/U;IFFZLO/U;III[LO/b;[BLandroid/location/Location;LO/h;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct/range {p0 .. p12}, LO/g;-><init>([LO/U;IFFZLO/U;III[LO/b;[BLandroid/location/Location;)V

    return-void
.end method

.method static synthetic a(LO/g;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 43
    iput p1, p0, LO/g;->n:I

    return p1
.end method

.method static synthetic a([LO/U;)Z
    .registers 2
    .parameter

    .prologue
    .line 43
    invoke-static {p0}, LO/g;->b([LO/U;)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)[LO/U;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/16 v9, 0xf

    const/4 v8, 0x5

    const/4 v1, 0x0

    .line 602
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 603
    new-array v3, v2, [LO/U;

    move v0, v1

    .line 604
    :goto_b
    if-ge v0, v2, :cond_3c

    .line 605
    invoke-virtual {p0, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 607
    const/4 v5, 0x7

    invoke-virtual {v4, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 609
    add-int/lit8 v6, v2, -0x1

    if-ne v0, v6, :cond_33

    .line 610
    new-instance v6, LO/U;

    invoke-direct {v6, v5, p1}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v6, v3, v0

    .line 615
    :goto_21
    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_30

    .line 616
    aget-object v5, v3, v0

    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LO/U;->a(Ljava/lang/String;)V

    .line 604
    :cond_30
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 612
    :cond_33
    new-instance v6, LO/U;

    const/4 v7, 0x0

    invoke-direct {v6, v5, v7}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v6, v3, v0

    goto :goto_21

    .line 620
    :cond_3c
    return-object v3
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LO/U;)[LO/z;
    .registers 16
    .parameter
    .parameter

    .prologue
    const/16 v12, 0x8

    const/4 v2, 0x2

    const/4 v0, 0x0

    .line 547
    invoke-virtual {p1, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v10

    .line 548
    new-array v11, v10, [LO/z;

    .line 549
    const/16 v1, 0xd

    invoke-static {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v7

    .line 554
    if-le v7, v2, :cond_13

    move v7, v0

    .line 559
    :cond_13
    if-ne v7, v2, :cond_1e

    const/16 v1, 0x17

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 562
    const/4 v7, 0x3

    :cond_1e
    move v9, v0

    .line 565
    :goto_1f
    if-ge v9, v10, :cond_3c

    .line 566
    invoke-virtual {p1, v12, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 567
    iget v1, p0, LO/g;->k:I

    iget v3, p0, LO/g;->h:F

    iget v4, p0, LO/g;->i:F

    iget-boolean v5, p0, LO/g;->e:Z

    iget-object v6, p0, LO/g;->j:LO/U;

    iget-object v8, p0, LO/g;->q:[LO/b;

    move-object v2, p2

    invoke-static/range {v0 .. v8}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I[LO/U;FFZLO/U;I[LO/b;)LO/z;

    move-result-object v0

    aput-object v0, v11, v9

    .line 565
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1f

    .line 571
    :cond_3c
    return-object v11
.end method

.method private b(I)I
    .registers 4
    .parameter

    .prologue
    .line 783
    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method private static b([LO/U;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 300
    array-length v2, p0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_40

    .line 304
    aget-object v2, p0, v0

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    .line 305
    aget-object v3, p0, v1

    invoke-virtual {v3}, LO/U;->c()Lo/u;

    move-result-object v3

    .line 306
    if-eqz v2, :cond_40

    if-eqz v3, :cond_40

    .line 307
    invoke-virtual {v2}, Lo/u;->a()I

    move-result v4

    invoke-virtual {v2}, Lo/u;->b()I

    move-result v2

    invoke-static {v4, v2}, Lo/T;->b(II)Lo/T;

    move-result-object v2

    .line 308
    invoke-virtual {v3}, Lo/u;->a()I

    move-result v4

    invoke-virtual {v3}, Lo/u;->b()I

    move-result v3

    invoke-static {v4, v3}, Lo/T;->b(II)Lo/T;

    move-result-object v3

    .line 309
    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v2}, Lo/T;->e()D

    move-result-wide v5

    div-double v2, v3, v5

    .line 310
    const-wide/high16 v4, 0x4000

    cmpg-double v2, v2, v4

    if-gez v2, :cond_40

    .line 315
    :goto_3f
    return v0

    :cond_40
    move v0, v1

    goto :goto_3f
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/b;
    .registers 8
    .parameter

    .prologue
    const/16 v6, 0xe

    const/4 v2, 0x0

    .line 628
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 629
    new-array v0, v3, [LO/b;

    move v1, v2

    .line 630
    :goto_a
    if-ge v1, v3, :cond_39

    .line 631
    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 632
    invoke-static {v4}, LO/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/b;

    move-result-object v5

    aput-object v5, v0, v1

    .line 633
    aget-object v5, v0, v1

    if-nez v5, :cond_3a

    .line 636
    const-string v0, "DirectionsRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Option with no value: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    new-array v0, v2, [LO/b;

    .line 641
    :cond_39
    return-object v0

    .line 630
    :cond_3a
    add-int/lit8 v1, v1, 0x1

    goto :goto_a
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/U;
    .registers 9
    .parameter

    .prologue
    const/4 v7, 0x7

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 580
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 581
    if-nez v0, :cond_c

    .line 582
    new-array v0, v1, [LO/U;

    .line 594
    :cond_b
    return-object v0

    .line 584
    :cond_c
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 586
    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 588
    new-array v0, v3, [LO/U;

    .line 589
    :goto_18
    if-ge v1, v3, :cond_b

    .line 590
    invoke-virtual {v2, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 592
    new-instance v5, LO/U;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v5, v0, v1

    .line 589
    add-int/lit8 v1, v1, 0x1

    goto :goto_18
.end method

.method private u()V
    .registers 4

    .prologue
    const/4 v2, 0x2

    .line 516
    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LO/g;->n:I

    .line 518
    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, LO/g;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/b;

    move-result-object v0

    iput-object v0, p0, LO/g;->q:[LO/b;

    .line 521
    invoke-direct {p0}, LO/g;->w()V

    .line 523
    invoke-virtual {p0}, LO/g;->k()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 524
    iget-object v0, p0, LO/g;->b:[LO/U;

    iget-object v1, p0, LO/g;->b:[LO/U;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    .line 525
    iget-object v1, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1, v0}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)[LO/U;

    move-result-object v0

    .line 526
    iget-object v1, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v1, v0}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LO/U;)[LO/z;

    move-result-object v0

    iput-object v0, p0, LO/g;->o:[LO/z;

    .line 527
    iget v0, p0, LO/g;->k:I

    if-ne v0, v2, :cond_42

    iget-object v0, p0, LO/g;->o:[LO/z;

    array-length v0, v0

    if-ge v0, v2, :cond_42

    .line 530
    const/4 v0, 0x4

    iput v0, p0, LO/g;->n:I

    .line 544
    :cond_42
    :goto_42
    return-void

    .line 532
    :cond_43
    invoke-virtual {p0}, LO/g;->o()Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 533
    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, LO/g;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/U;

    move-result-object v0

    iput-object v0, p0, LO/g;->p:[LO/U;

    .line 537
    iget-object v0, p0, LO/g;->p:[LO/U;

    array-length v0, v0

    if-nez v0, :cond_42

    .line 538
    const/4 v0, 0x3

    iput v0, p0, LO/g;->n:I

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, LO/g;->p:[LO/U;

    goto :goto_42

    .line 542
    :cond_5d
    const-string v0, "DirectionsRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LO/g;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_42
.end method

.method private v()V
    .registers 2

    .prologue
    .line 739
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->g()Lcom/google/android/maps/driveabout/app/NavigationService;

    move-result-object v0

    .line 740
    if-eqz v0, :cond_10

    .line 741
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->i()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LO/g;->l:Ljava/lang/String;

    .line 743
    :cond_10
    return-void
.end method

.method private w()V
    .registers 5

    .prologue
    const/4 v3, 0x2

    .line 755
    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    .line 779
    :cond_6
    :goto_6
    return-void

    .line 758
    :cond_7
    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 759
    if-lt v1, v3, :cond_6

    .line 765
    const/4 v0, 0x0

    :goto_11
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_1e

    .line 766
    invoke-direct {p0, v0}, LO/g;->b(I)I

    move-result v2

    if-nez v2, :cond_6

    .line 765
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 774
    :cond_1e
    add-int/lit8 v0, v1, -0x1

    invoke-direct {p0, v0}, LO/g;->b(I)I

    move-result v0

    .line 775
    if-ne v0, v3, :cond_6

    .line 777
    iput v3, p0, LO/g;->n:I

    goto :goto_6
.end method


# virtual methods
.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 824
    iput p1, p0, LO/g;->n:I

    .line 825
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 473
    iput-object p1, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 474
    invoke-direct {p0}, LO/g;->u()V

    .line 475
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 507
    invoke-virtual {p0}, LO/g;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 508
    return-void
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 292
    iget v0, p0, LO/g;->n:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 5
    .parameter

    .prologue
    .line 483
    :try_start_0
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_9} :catch_b
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_9} :catch_17
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_22

    .line 499
    const/4 v0, 0x1

    return v0

    .line 485
    :catch_b
    move-exception v0

    .line 486
    const-string v1, "DA:DirectionsRequest"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 487
    const-string v1, "DirectionsRequest: Handling request failed"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 488
    throw v0

    .line 489
    :catch_17
    move-exception v0

    .line 492
    const-string v1, "DirectionsRequest"

    invoke-virtual {v0}, Ljava/io/InterruptedIOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    throw v0

    .line 494
    :catch_22
    move-exception v0

    .line 495
    const-string v1, "DA:DirectionsRequest"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 496
    const-string v1, "DirectionsRequest: Parse failed"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 497
    throw v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 464
    const/16 v0, 0x1c

    return v0
.end method

.method public d()[LO/z;
    .registers 2

    .prologue
    .line 323
    iget-object v0, p0, LO/g;->o:[LO/z;

    return-object v0
.end method

.method public f()[LO/b;
    .registers 2

    .prologue
    .line 338
    iget-object v0, p0, LO/g;->q:[LO/b;

    return-object v0
.end method

.method public g()[LO/U;
    .registers 2

    .prologue
    .line 368
    iget-object v0, p0, LO/g;->p:[LO/U;

    return-object v0
.end method

.method public h()Z
    .registers 3

    .prologue
    .line 389
    iget v0, p0, LO/g;->k:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public j()I
    .registers 2

    .prologue
    .line 403
    iget v0, p0, LO/g;->f:I

    return v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 410
    iget v0, p0, LO/g;->n:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public l()Z
    .registers 3

    .prologue
    .line 417
    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public m()Z
    .registers 3

    .prologue
    .line 425
    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public n()Z
    .registers 3

    .prologue
    .line 434
    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public o()Z
    .registers 3

    .prologue
    .line 443
    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public p()Z
    .registers 3

    .prologue
    .line 450
    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public q()I
    .registers 2

    .prologue
    .line 454
    iget v0, p0, LO/g;->k:I

    return v0
.end method

.method public r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 458
    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public r_()Z
    .registers 3

    .prologue
    .line 396
    iget v0, p0, LO/g;->k:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public s()Z
    .registers 2

    .prologue
    .line 469
    const/4 v0, 0x1

    return v0
.end method

.method t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 10

    .prologue
    const v4, 0x186a0

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 649
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 650
    iget v0, p0, LO/g;->a:I

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    move v0, v1

    .line 651
    :goto_14
    iget-object v3, p0, LO/g;->b:[LO/U;

    array-length v3, v3

    if-ge v0, v3, :cond_27

    .line 652
    iget-object v3, p0, LO/g;->b:[LO/U;

    aget-object v3, v3, v0

    invoke-virtual {v3}, LO/U;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 651
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 655
    :cond_27
    const/4 v0, 0x5

    iget v3, p0, LO/g;->g:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 657
    iget v0, p0, LO/g;->g:I

    if-le v0, v6, :cond_4b

    .line 660
    const/16 v0, 0x16

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 662
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->g()Lcom/google/android/maps/driveabout/app/NavigationService;

    move-result-object v0

    .line 663
    if-eqz v0, :cond_4b

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->j()Z

    move-result v0

    if-nez v0, :cond_4b

    .line 666
    const/16 v0, 0x20

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 669
    :cond_4b
    const/4 v0, 0x7

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 671
    const/16 v0, 0xd

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 672
    const/16 v0, 0xf

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 674
    const/16 v0, 0x14

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 675
    const/16 v0, 0xb

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 676
    const/16 v0, 0x26

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 677
    const/16 v0, 0x19

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 678
    const/16 v0, 0x15

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 682
    const/16 v0, 0x10

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 683
    const/16 v0, 0x1d

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 684
    const/16 v0, 0x1f

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 685
    const/16 v0, 0x17

    const/16 v3, 0x32

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 686
    const/16 v0, 0x18

    iget v3, p0, LO/g;->k:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 697
    sget-object v0, LO/g;->l:Ljava/lang/String;

    if-eqz v0, :cond_9a

    .line 698
    const/16 v0, 0x23

    sget-object v3, LO/g;->l:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 704
    :cond_9a
    iget v0, p0, LO/g;->k:I

    if-nez v0, :cond_c0

    .line 705
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/dp;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 706
    iget-object v3, p0, LO/g;->b:[LO/U;

    aget-object v3, v3, v1

    invoke-virtual {v3}, LO/U;->c()Lo/u;

    move-result-object v3

    invoke-static {v3}, LR/e;->b(Lo/u;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 708
    invoke-virtual {v0, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 709
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 710
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 713
    :cond_c0
    iget-object v0, p0, LO/g;->c:[LO/b;

    if-eqz v0, :cond_d7

    .line 714
    iget-object v0, p0, LO/g;->c:[LO/b;

    array-length v3, v0

    :goto_c7
    if-ge v1, v3, :cond_d7

    aget-object v4, v0, v1

    .line 715
    const/16 v5, 0xa

    invoke-virtual {v4}, LO/b;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 714
    add-int/lit8 v1, v1, 0x1

    goto :goto_c7

    .line 720
    :cond_d7
    iget-object v0, p0, LO/g;->d:[B

    if-eqz v0, :cond_e2

    .line 721
    const/16 v0, 0x2a

    iget-object v1, p0, LO/g;->d:[B

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 724
    :cond_e2
    iget v0, p0, LO/g;->h:F

    cmpl-float v0, v0, v7

    if-gez v0, :cond_ee

    iget v0, p0, LO/g;->i:F

    cmpl-float v0, v0, v7

    if-ltz v0, :cond_112

    .line 725
    :cond_ee
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->A:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 726
    iget v1, p0, LO/g;->h:F

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_101

    .line 727
    iget v1, p0, LO/g;->h:F

    float-to-int v1, v1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 729
    :cond_101
    iget v1, p0, LO/g;->i:F

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_10d

    .line 730
    iget v1, p0, LO/g;->i:F

    float-to-int v1, v1

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 732
    :cond_10d
    const/16 v1, 0x11

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 735
    :cond_112
    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 789
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 790
    const-string v0, " mode:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->a:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 791
    const-string v0, " action:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->k:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 792
    const-string v0, " startBearing:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->h:F

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 793
    const-string v0, " startSpeed:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->i:F

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 794
    const-string v0, " waypoints: ["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 795
    :goto_3a
    iget-object v3, p0, LO/g;->b:[LO/U;

    array-length v3, v3

    if-ge v0, v3, :cond_60

    .line 796
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LO/g;->b:[LO/U;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 797
    iget-object v3, p0, LO/g;->b:[LO/U;

    aget-object v3, v3, v0

    iget-object v4, p0, LO/g;->j:LO/U;

    invoke-virtual {v3, v4}, LO/U;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5d

    .line 798
    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    :cond_5d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3a

    .line 801
    :cond_60
    iget-boolean v0, p0, LO/g;->e:Z

    if-eqz v0, :cond_69

    .line 802
    const-string v0, " hasUncertainFromPoint:true"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804
    :cond_69
    const-string v0, " maxTripCount:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->g:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 805
    iget-object v0, p0, LO/g;->c:[LO/b;

    if-eqz v0, :cond_8b

    .line 806
    iget-object v0, p0, LO/g;->c:[LO/b;

    array-length v3, v0

    :goto_7b
    if-ge v1, v3, :cond_8b

    aget-object v4, v0, v1

    .line 807
    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 806
    add-int/lit8 v1, v1, 0x1

    goto :goto_7b

    .line 810
    :cond_8b
    iget v0, p0, LO/g;->f:I

    if-eqz v0, :cond_9a

    .line 811
    const-string v0, " previousAlternateExtraMeters:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/g;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 813
    :cond_9a
    const-string v0, " ] ]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
