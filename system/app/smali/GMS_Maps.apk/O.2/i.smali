.class public LO/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[LO/U;

.field private final b:I

.field private final c:I

.field private d:F

.field private e:F

.field private f:Z

.field private g:LO/U;

.field private h:I

.field private i:I

.field private j:[LO/b;

.field private k:[B

.field private l:Landroid/location/Location;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 12
    .parameter

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v0, 0x0

    const/high16 v1, -0x4080

    const/4 v7, 0x2

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput v1, p0, LO/i;->d:F

    .line 186
    iput v1, p0, LO/i;->e:F

    .line 192
    const/16 v1, 0x2710

    iput v1, p0, LO/i;->h:I

    .line 193
    const/4 v1, 0x3

    iput v1, p0, LO/i;->i:I

    .line 205
    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, LO/i;->b:I

    .line 206
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 207
    new-array v1, v2, [LO/U;

    iput-object v1, p0, LO/i;->a:[LO/U;

    move v1, v0

    .line 208
    :goto_24
    if-ge v1, v2, :cond_37

    .line 209
    invoke-virtual {p1, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 210
    iget-object v4, p0, LO/i;->a:[LO/U;

    new-instance v5, LO/U;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v6}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v5, v4, v1

    .line 208
    add-int/lit8 v1, v1, 0x1

    goto :goto_24

    .line 212
    :cond_37
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, LO/i;->i:I

    .line 213
    const/16 v1, 0x18

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    iput v1, p0, LO/i;->c:I

    .line 214
    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 215
    new-array v2, v1, [LO/b;

    iput-object v2, p0, LO/i;->j:[LO/b;

    .line 216
    :goto_4e
    if-ge v0, v1, :cond_5f

    .line 217
    invoke-virtual {p1, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 218
    iget-object v3, p0, LO/i;->j:[LO/b;

    invoke-static {v2}, LO/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/b;

    move-result-object v2

    aput-object v2, v3, v0

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_4e

    .line 220
    :cond_5f
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_75

    .line 222
    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LO/i;->d:F

    .line 223
    invoke-virtual {v0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LO/i;->e:F

    .line 233
    :cond_75
    return-void
.end method

.method public constructor <init>([LO/U;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, -0x4080

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput v0, p0, LO/i;->d:F

    .line 186
    iput v0, p0, LO/i;->e:F

    .line 192
    const/16 v0, 0x2710

    iput v0, p0, LO/i;->h:I

    .line 193
    const/4 v0, 0x3

    iput v0, p0, LO/i;->i:I

    .line 199
    iput-object p1, p0, LO/i;->a:[LO/U;

    .line 200
    iput p2, p0, LO/i;->b:I

    .line 201
    iput p3, p0, LO/i;->c:I

    .line 202
    return-void
.end method


# virtual methods
.method public a()LO/g;
    .registers 15

    .prologue
    .line 277
    new-instance v0, LO/g;

    iget-object v1, p0, LO/i;->a:[LO/U;

    iget v2, p0, LO/i;->b:I

    iget v3, p0, LO/i;->d:F

    iget v4, p0, LO/i;->e:F

    iget-boolean v5, p0, LO/i;->f:Z

    iget-object v6, p0, LO/i;->g:LO/U;

    iget v7, p0, LO/i;->h:I

    iget v8, p0, LO/i;->i:I

    iget v9, p0, LO/i;->c:I

    iget-object v10, p0, LO/i;->j:[LO/b;

    iget-object v11, p0, LO/i;->k:[B

    iget-object v12, p0, LO/i;->l:Landroid/location/Location;

    const/4 v13, 0x0

    invoke-direct/range {v0 .. v13}, LO/g;-><init>([LO/U;IFFZLO/U;III[LO/b;[BLandroid/location/Location;LO/h;)V

    .line 280
    iget-object v1, p0, LO/i;->a:[LO/U;

    invoke-static {v1}, LO/g;->a([LO/U;)Z

    move-result v1

    if-nez v1, :cond_2a

    .line 281
    const/4 v1, 0x1

    invoke-static {v0, v1}, LO/g;->a(LO/g;I)I

    .line 283
    :cond_2a
    return-object v0
.end method

.method public a(FF)LO/i;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 236
    iput p1, p0, LO/i;->d:F

    .line 237
    iput p2, p0, LO/i;->e:F

    .line 238
    return-object p0
.end method

.method public a(I)LO/i;
    .registers 2
    .parameter

    .prologue
    .line 252
    iput p1, p0, LO/i;->h:I

    .line 253
    return-object p0
.end method

.method public a(LO/U;)LO/i;
    .registers 2
    .parameter

    .prologue
    .line 247
    iput-object p1, p0, LO/i;->g:LO/U;

    .line 248
    return-object p0
.end method

.method public a(Landroid/location/Location;)LO/i;
    .registers 2
    .parameter

    .prologue
    .line 272
    iput-object p1, p0, LO/i;->l:Landroid/location/Location;

    .line 273
    return-object p0
.end method

.method public a(Z)LO/i;
    .registers 2
    .parameter

    .prologue
    .line 242
    iput-boolean p1, p0, LO/i;->f:Z

    .line 243
    return-object p0
.end method

.method public a([LO/b;)LO/i;
    .registers 2
    .parameter

    .prologue
    .line 262
    iput-object p1, p0, LO/i;->j:[LO/b;

    .line 263
    return-object p0
.end method

.method public b(I)LO/i;
    .registers 2
    .parameter

    .prologue
    .line 257
    iput p1, p0, LO/i;->i:I

    .line 258
    return-object p0
.end method
