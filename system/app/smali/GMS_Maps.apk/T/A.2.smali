.class Lt/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt/s;


# instance fields
.field private a:LA/c;

.field private b:Ljava/util/Queue;

.field private c:Ljava/util/Map;

.field private d:Lt/g;


# direct methods
.method public constructor <init>(LA/c;Lt/g;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    iput-object p1, p0, Lt/A;->a:LA/c;

    .line 289
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    .line 290
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lt/A;->c:Ljava/util/Map;

    .line 291
    iput-object p2, p0, Lt/A;->d:Lt/g;

    .line 292
    return-void
.end method

.method static synthetic a(Lt/A;)LA/c;
    .registers 2
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lt/A;->a:LA/c;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 355
    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 356
    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v1, Lt/C;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lt/C;-><init>(Lt/A;Lt/z;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 357
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 350
    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v1, Lt/E;

    invoke-direct {v1, p0, p1}, Lt/E;-><init>(Lt/A;I)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 351
    return-void
.end method

.method public a(J)V
    .registers 6
    .parameter

    .prologue
    .line 342
    invoke-static {p1, p2}, LJ/a;->b(J)Lo/aq;

    move-result-object v0

    .line 343
    if-eqz v0, :cond_10

    .line 344
    iget-object v1, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v2, Lt/H;

    invoke-direct {v2, p0, v0}, Lt/H;-><init>(Lt/A;Lo/aq;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 346
    :cond_10
    return-void
.end method

.method public a(JI)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 324
    invoke-static {p1, p2}, LJ/a;->b(J)Lo/aq;

    move-result-object v1

    .line 325
    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ap;

    .line 327
    if-eqz v1, :cond_1a

    if-eqz v0, :cond_1a

    .line 328
    iget-object v1, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v2, Lt/F;

    invoke-direct {v2, p0, v0, p3}, Lt/F;-><init>(Lt/A;Lo/ap;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 330
    :cond_1a
    return-void
.end method

.method public a(Lo/ap;)V
    .registers 4
    .parameter

    .prologue
    .line 303
    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {p1}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    return-void
.end method

.method public b()V
    .registers 4

    .prologue
    .line 310
    :goto_0
    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_16

    .line 311
    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/B;

    iget-object v1, p0, Lt/A;->d:Lt/g;

    invoke-interface {v0, v1}, Lt/B;->a(Lt/g;)V

    goto :goto_0

    .line 315
    :cond_16
    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_41

    .line 316
    const-string v0, "SDCardTileCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tiles were not inserted into the disk cache."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lt/A;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 320
    :cond_41
    return-void
.end method

.method public b(JI)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 334
    invoke-static {p1, p2}, LJ/a;->b(J)Lo/aq;

    move-result-object v0

    .line 335
    if-eqz v0, :cond_10

    .line 336
    iget-object v1, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v2, Lt/G;

    invoke-direct {v2, p0, v0, p3}, Lt/G;-><init>(Lt/A;Lo/aq;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 338
    :cond_10
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 360
    iget-object v0, p0, Lt/A;->b:Ljava/util/Queue;

    new-instance v1, Lt/D;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lt/D;-><init>(Lt/A;Lt/z;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 361
    return-void
.end method
