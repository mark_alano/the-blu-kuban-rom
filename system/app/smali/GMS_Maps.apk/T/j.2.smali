.class Lt/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[B

.field final b:I

.field c:I

.field d:Ljava/util/concurrent/locks/ReentrantReadWriteLock;


# direct methods
.method constructor <init>(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2646
    mul-int/lit16 v0, p1, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lt/j;->a:[B

    .line 2647
    iput p1, p0, Lt/j;->b:I

    .line 2648
    iput p2, p0, Lt/j;->c:I

    .line 2649
    return-void
.end method

.method static a(J)[I
    .registers 12
    .parameter

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x1fd3

    const/16 v7, 0x16

    .line 2707
    const/4 v1, 0x3

    new-array v1, v1, [I

    .line 2708
    invoke-static {p0, p1, v7}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    .line 2709
    rem-long v4, v2, v8

    long-to-int v4, v4

    aput v4, v1, v0

    .line 2710
    invoke-static {v2, v3, v7}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    .line 2711
    const/4 v4, 0x1

    rem-long v5, v2, v8

    long-to-int v5, v5

    aput v5, v1, v4

    .line 2712
    invoke-static {v2, v3, v7}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    .line 2713
    const/4 v4, 0x2

    rem-long/2addr v2, v8

    long-to-int v2, v2

    aput v2, v1, v4

    .line 2714
    :goto_24
    array-length v2, v1

    if-ge v0, v2, :cond_34

    .line 2715
    aget v2, v1, v0

    if-gez v2, :cond_31

    .line 2716
    aget v2, v1, v0

    add-int/lit16 v2, v2, 0x1fd3

    aput v2, v1, v0

    .line 2714
    :cond_31
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 2719
    :cond_34
    return-object v1
.end method


# virtual methods
.method a()V
    .registers 3

    .prologue
    .line 2701
    invoke-virtual {p0}, Lt/j;->b()V

    .line 2702
    iget-object v0, p0, Lt/j;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 2703
    return-void
.end method

.method a(I)V
    .registers 2
    .parameter

    .prologue
    .line 2656
    invoke-virtual {p0}, Lt/j;->b()V

    .line 2657
    iput p1, p0, Lt/j;->c:I

    .line 2658
    return-void
.end method

.method a(JI)V
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 2724
    invoke-virtual {p0}, Lt/j;->b()V

    .line 2725
    invoke-static {p1, p2}, Lt/j;->a(J)[I

    move-result-object v1

    .line 2726
    mul-int/lit16 v2, p3, 0x400

    .line 2727
    const/4 v0, 0x0

    :goto_a
    array-length v3, v1

    if-ge v0, v3, :cond_24

    .line 2728
    aget v3, v1, v0

    shr-int/lit8 v3, v3, 0x3

    .line 2729
    aget v4, v1, v0

    and-int/lit8 v4, v4, 0x7

    .line 2730
    iget-object v5, p0, Lt/j;->a:[B

    add-int/2addr v3, v2

    aget-byte v6, v5, v3

    const/4 v7, 0x1

    shl-int v4, v7, v4

    or-int/2addr v4, v6

    int-to-byte v4, v4

    aput-byte v4, v5, v3

    .line 2727
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2732
    :cond_24
    return-void
.end method

.method a(Lj/c;)V
    .registers 4
    .parameter

    .prologue
    .line 2661
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lt/j;->c:I

    if-ge v0, v1, :cond_b

    .line 2662
    invoke-virtual {p0, v0}, Lt/j;->d(I)V

    .line 2661
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2664
    :cond_b
    iget-object v0, p0, Lt/j;->a:[B

    invoke-interface {p1, v0}, Lj/c;->b([B)V

    .line 2665
    return-void
.end method

.method a(Lj/c;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2668
    invoke-virtual {p0, p2}, Lt/j;->d(I)V

    .line 2669
    iget-object v0, p0, Lt/j;->a:[B

    mul-int/lit16 v1, p2, 0x400

    const/16 v2, 0x400

    invoke-interface {p1, v0, v1, v2}, Lj/c;->b([BII)V

    .line 2670
    return-void
.end method

.method a(Ljava/util/concurrent/locks/ReentrantReadWriteLock;)V
    .registers 2
    .parameter

    .prologue
    .line 2652
    iput-object p1, p0, Lt/j;->d:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 2653
    return-void
.end method

.method a(Lt/p;)V
    .registers 7
    .parameter

    .prologue
    .line 2679
    invoke-virtual {p0}, Lt/j;->b()V

    .line 2680
    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v0

    invoke-virtual {p0, v0}, Lt/j;->b(I)V

    .line 2681
    const/4 v0, 0x0

    :goto_b
    invoke-virtual {p1}, Lt/p;->b()I

    move-result v1

    if-ge v0, v1, :cond_25

    .line 2682
    invoke-virtual {p1, v0}, Lt/p;->b(I)J

    move-result-wide v1

    .line 2683
    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-eqz v3, :cond_22

    .line 2684
    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lt/j;->a(JI)V

    .line 2681
    :cond_22
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 2687
    :cond_25
    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v0

    iget v1, p0, Lt/j;->c:I

    if-lt v0, v1, :cond_35

    .line 2688
    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/j;->c:I

    .line 2690
    :cond_35
    return-void
.end method

.method a([II)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2739
    mul-int/lit16 v3, p2, 0x400

    move v0, v1

    .line 2740
    :goto_5
    array-length v4, p1

    if-ge v0, v4, :cond_1e

    .line 2741
    aget v4, p1, v0

    shr-int/lit8 v4, v4, 0x3

    .line 2742
    aget v5, p1, v0

    and-int/lit8 v5, v5, 0x7

    .line 2743
    iget-object v6, p0, Lt/j;->a:[B

    add-int/2addr v4, v3

    aget-byte v4, v6, v4

    shl-int v5, v2, v5

    and-int/2addr v4, v5

    if-nez v4, :cond_1b

    .line 2747
    :goto_1a
    return v1

    .line 2740
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_1e
    move v1, v2

    .line 2747
    goto :goto_1a
.end method

.method b()V
    .registers 3

    .prologue
    .line 2765
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lt/j;->d:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lt/j;->d:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->isWriteLockedByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 2767
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "State write lock needed to modify BloomFilters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2769
    :cond_1a
    return-void
.end method

.method b(I)V
    .registers 6
    .parameter

    .prologue
    .line 2694
    invoke-virtual {p0}, Lt/j;->b()V

    .line 2695
    mul-int/lit16 v0, p1, 0x400

    .line 2696
    iget-object v1, p0, Lt/j;->a:[B

    add-int/lit16 v2, v0, 0x3fc

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Ljava/util/Arrays;->fill([BIIB)V

    .line 2697
    return-void
.end method

.method b(Lj/c;)V
    .registers 5
    .parameter

    .prologue
    .line 2673
    invoke-virtual {p0}, Lt/j;->b()V

    .line 2674
    iget-object v0, p0, Lt/j;->a:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lt/j;->a:[B

    array-length v2, v2

    invoke-interface {p1, v0, v1, v2}, Lj/c;->a([BII)V

    .line 2675
    return-void
.end method

.method b(JI)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2735
    invoke-static {p1, p2}, Lt/j;->a(J)[I

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lt/j;->a([II)Z

    move-result v0

    return v0
.end method

.method c(I)Z
    .registers 5
    .parameter

    .prologue
    .line 2751
    mul-int/lit16 v0, p1, 0x400

    .line 2752
    iget-object v1, p0, Lt/j;->a:[B

    const/16 v2, 0x3fc

    invoke-static {v1, v0, v2}, Lt/h;->c([BII)I

    move-result v1

    .line 2753
    iget-object v2, p0, Lt/j;->a:[B

    add-int/lit16 v0, v0, 0x3fc

    invoke-static {v2, v0}, Lt/h;->a([BI)I

    move-result v0

    .line 2754
    if-ne v1, v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method d(I)V
    .registers 5
    .parameter

    .prologue
    .line 2759
    mul-int/lit16 v0, p1, 0x400

    .line 2760
    iget-object v1, p0, Lt/j;->a:[B

    const/16 v2, 0x3fc

    invoke-static {v1, v0, v2}, Lt/h;->c([BII)I

    move-result v1

    .line 2761
    iget-object v2, p0, Lt/j;->a:[B

    add-int/lit16 v0, v0, 0x3fc

    invoke-static {v2, v0, v1}, Lt/h;->a([BII)V

    .line 2762
    return-void
.end method
