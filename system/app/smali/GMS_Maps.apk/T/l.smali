.class public Lt/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lt/m;

.field public final b:[B

.field public final c:I

.field public final d:[B


# direct methods
.method private constructor <init>(Lt/m;I[B)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409
    array-length v0, p3

    const v1, 0xffffff

    if-le v0, v1, :cond_11

    .line 1410
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "data too large"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1412
    :cond_11
    iput-object p1, p0, Lt/l;->a:Lt/m;

    .line 1413
    invoke-virtual {p1}, Lt/m;->b()[B

    move-result-object v0

    iput-object v0, p0, Lt/l;->b:[B

    .line 1414
    iput p2, p0, Lt/l;->c:I

    .line 1415
    iput-object p3, p0, Lt/l;->d:[B

    .line 1416
    return-void
.end method

.method synthetic constructor <init>(Lt/m;I[BLt/i;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1397
    invoke-direct {p0, p1, p2, p3}, Lt/l;-><init>(Lt/m;I[B)V

    return-void
.end method

.method private constructor <init>(Lt/m;[B)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1405
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lt/l;-><init>(Lt/m;I[B)V

    .line 1406
    return-void
.end method

.method synthetic constructor <init>(Lt/m;[BLt/i;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1397
    invoke-direct {p0, p1, p2}, Lt/l;-><init>(Lt/m;[B)V

    return-void
.end method
