.class public LU/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/x;


# instance fields
.field private final a:Lbi/d;

.field private final b:Ljava/util/ArrayList;

.field private c:Lbi/a;

.field private d:F

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/lang/Runnable;

.field private g:Z


# direct methods
.method public constructor <init>(Lbi/d;LU/L;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, LU/I;->a:Lbi/d;

    .line 85
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LU/I;->e:Landroid/os/Handler;

    .line 87
    new-instance v0, LU/J;

    invoke-direct {v0, p0, p2}, LU/J;-><init>(LU/I;LU/L;)V

    iput-object v0, p0, LU/I;->f:Ljava/lang/Runnable;

    .line 118
    return-void
.end method

.method static synthetic a(LU/I;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LU/I;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(LU/I;LU/z;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, LU/I;->a(LU/z;)V

    return-void
.end method

.method private a(LU/z;)V
    .registers 4
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/y;

    .line 156
    invoke-interface {v0, p1}, LU/y;->a(LU/z;)V

    goto :goto_6

    .line 158
    :cond_16
    return-void
.end method

.method static synthetic b(LU/I;)Z
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-boolean v0, p0, LU/I;->g:Z

    return v0
.end method

.method private c()LU/z;
    .registers 5

    .prologue
    const-wide/high16 v2, 0x3fe0

    .line 121
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_12

    .line 122
    invoke-direct {p0}, LU/I;->h()V

    .line 123
    invoke-direct {p0}, LU/I;->d()LU/z;

    move-result-object v0

    .line 127
    :goto_11
    return-object v0

    .line 124
    :cond_12
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1f

    .line 125
    invoke-direct {p0}, LU/I;->e()LU/z;

    move-result-object v0

    goto :goto_11

    .line 127
    :cond_1f
    invoke-direct {p0}, LU/I;->f()LU/z;

    move-result-object v0

    goto :goto_11
.end method

.method static synthetic c(LU/I;)LU/z;
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LU/I;->c()LU/z;

    move-result-object v0

    return-object v0
.end method

.method private d()LU/z;
    .registers 6

    .prologue
    .line 135
    invoke-direct {p0}, LU/I;->g()LU/R;

    move-result-object v0

    iget-object v1, p0, LU/I;->c:Lbi/a;

    new-instance v2, Lbi/c;

    iget-object v3, p0, LU/I;->c:Lbi/a;

    iget-object v4, p0, LU/I;->c:Lbi/a;

    invoke-direct {v2, v3, v4}, Lbi/c;-><init>(Lbi/a;Lbi/a;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LU/z;->a(LU/R;Lbi/a;Lbi/c;Z)LU/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(LU/I;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LU/I;->h()V

    return-void
.end method

.method private e()LU/z;
    .registers 2

    .prologue
    .line 140
    invoke-direct {p0}, LU/I;->g()LU/R;

    move-result-object v0

    invoke-static {v0}, LU/z;->a(LU/R;)LU/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(LU/I;)LU/z;
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LU/I;->d()LU/z;

    move-result-object v0

    return-object v0
.end method

.method private f()LU/z;
    .registers 3

    .prologue
    .line 144
    invoke-direct {p0}, LU/I;->g()LU/R;

    move-result-object v0

    sget-object v1, LU/B;->c:LU/B;

    invoke-static {v0, v1}, LU/z;->a(LU/R;LU/B;)LU/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(LU/I;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LU/I;->i()V

    return-void
.end method

.method private g()LU/R;
    .registers 4

    .prologue
    .line 148
    new-instance v0, LU/R;

    new-instance v1, Landroid/location/Location;

    const-string v2, "gps"

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LU/R;-><init>(Landroid/location/Location;)V

    .line 149
    sget-object v1, LU/S;->a:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    .line 150
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LU/R;->setTime(J)V

    .line 151
    return-object v0
.end method

.method static synthetic g(LU/I;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, LU/I;->e:Landroid/os/Handler;

    return-object v0
.end method

.method private h()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 186
    iget-object v0, p0, LU/I;->c:Lbi/a;

    if-nez v0, :cond_e

    .line 187
    new-instance v0, Lbi/a;

    invoke-direct {v0, v1, v1, v4}, Lbi/a;-><init>(IIF)V

    iput-object v0, p0, LU/I;->c:Lbi/a;

    .line 201
    :cond_d
    :goto_d
    return-void

    .line 190
    :cond_e
    iget-object v0, p0, LU/I;->c:Lbi/a;

    iget-object v0, v0, Lbi/a;->a:Lbi/t;

    .line 191
    new-instance v1, Lbi/v;

    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-direct {v1, v2}, Lbi/v;-><init>(Lbi/d;)V

    invoke-virtual {v1, v0}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v1

    .line 192
    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-virtual {v2, v0}, Lbi/d;->e(Lbi/t;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 193
    iget v2, p0, LU/I;->d:F

    const v3, 0x3e4ccccd

    add-float/2addr v2, v3

    iput v2, p0, LU/I;->d:F

    .line 194
    iget v2, p0, LU/I;->d:F

    const/high16 v3, 0x3f80

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3b

    .line 195
    invoke-virtual {v1}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    .line 196
    iput v4, p0, LU/I;->d:F

    .line 198
    :cond_3b
    new-instance v1, Lbi/a;

    iget v2, p0, LU/I;->d:F

    invoke-direct {v1, v0, v2}, Lbi/a;-><init>(Lbi/t;F)V

    iput-object v1, p0, LU/I;->c:Lbi/a;

    goto :goto_d
.end method

.method private i()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 204
    iget-object v0, p0, LU/I;->c:Lbi/a;

    if-nez v0, :cond_e

    .line 205
    new-instance v0, Lbi/a;

    invoke-direct {v0, v1, v1, v4}, Lbi/a;-><init>(IIF)V

    iput-object v0, p0, LU/I;->c:Lbi/a;

    .line 227
    :goto_d
    return-void

    .line 208
    :cond_e
    iget-object v0, p0, LU/I;->c:Lbi/a;

    iget-object v1, v0, Lbi/a;->a:Lbi/t;

    .line 210
    iget-object v0, p0, LU/I;->a:Lbi/d;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->c()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 211
    iget-object v0, p0, LU/I;->a:Lbi/d;

    iget-object v2, p0, LU/I;->c:Lbi/a;

    const/high16 v3, 0x41a0

    invoke-static {v0, v2, v3}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    .line 218
    :goto_28
    iget-object v2, v0, Lbi/a;->a:Lbi/t;

    invoke-virtual {v2, v1}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4d

    .line 219
    new-instance v0, Lbi/v;

    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-direct {v0, v2}, Lbi/v;-><init>(Lbi/d;)V

    invoke-virtual {v0, v1}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object v0

    .line 220
    iget-object v2, p0, LU/I;->a:Lbi/d;

    invoke-virtual {v2, v1}, Lbi/d;->e(Lbi/t;)Z

    move-result v2

    if-nez v2, :cond_5b

    .line 221
    invoke-virtual {v0}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    .line 223
    :goto_47
    new-instance v1, Lbi/a;

    invoke-direct {v1, v0, v4}, Lbi/a;-><init>(Lbi/t;F)V

    move-object v0, v1

    .line 225
    :cond_4d
    iput-object v0, p0, LU/I;->c:Lbi/a;

    goto :goto_d

    .line 213
    :cond_50
    iget-object v0, p0, LU/I;->a:Lbi/d;

    iget-object v2, p0, LU/I;->c:Lbi/a;

    const/high16 v3, 0x4316

    invoke-static {v0, v2, v3}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    goto :goto_28

    :cond_5b
    move-object v0, v1

    goto :goto_47
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 176
    iget-object v0, p0, LU/I;->e:Landroid/os/Handler;

    iget-object v1, p0, LU/I;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 177
    iget-object v1, p0, LU/I;->f:Ljava/lang/Runnable;

    monitor-enter v1

    .line 178
    const/4 v0, 0x1

    :try_start_b
    iput-boolean v0, p0, LU/I;->g:Z

    .line 182
    monitor-exit v1

    .line 183
    return-void

    .line 182
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_f

    throw v0
.end method

.method public a(LU/y;)V
    .registers 3
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 171
    iget-object v0, p0, LU/I;->e:Landroid/os/Handler;

    iget-object v1, p0, LU/I;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 172
    return-void
.end method

.method public b(LU/y;)V
    .registers 3
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, LU/I;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 168
    return-void
.end method
