.class LU/V;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LU/U;


# direct methods
.method constructor <init>(LU/U;)V
    .registers 2
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, LU/V;->a:LU/U;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    .line 50
    new-instance v0, Landroid/location/Location;

    const-string v1, "speed_provider"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, LU/V;->a:LU/U;

    iget-object v2, p0, LU/V;->a:LU/U;

    invoke-static {v2}, LU/U;->a(LU/U;)F

    move-result v2

    invoke-static {v1, v0, v2}, LU/U;->a(LU/U;Landroid/location/Location;F)V

    .line 52
    iget-object v1, p0, LU/V;->a:LU/U;

    invoke-static {v1}, LU/U;->b(LU/U;)Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    .line 53
    new-instance v1, LU/R;

    invoke-direct {v1, v0}, LU/R;-><init>(Landroid/location/Location;)V

    .line 54
    sget-object v0, LU/S;->c:LU/S;

    invoke-virtual {v1, v0}, LU/R;->a(LU/S;)V

    .line 55
    iget-object v0, p0, LU/V;->a:LU/U;

    invoke-static {v0, v1}, LU/U;->a(LU/U;LU/R;)V

    .line 56
    iget-object v0, p0, LU/V;->a:LU/U;

    invoke-virtual {v0}, LU/U;->e()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 57
    iget-object v0, p0, LU/V;->a:LU/U;

    invoke-static {v0}, LU/U;->c(LU/U;)LX/i;

    move-result-object v0

    iget-object v1, p0, LU/V;->a:LU/U;

    iget-object v1, v1, LU/U;->a:Ljava/lang/Runnable;

    invoke-static {}, LU/U;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/i;->b(Ljava/lang/Runnable;J)Z

    .line 59
    :cond_47
    return-void
.end method
