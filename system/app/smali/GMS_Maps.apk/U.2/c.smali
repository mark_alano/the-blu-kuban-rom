.class public final LU/c;
.super LU/b;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field private final a:Landroid/location/LocationManager;

.field private final b:Lcom/google/googlenav/common/a;


# direct methods
.method public constructor <init>(LU/T;Landroid/content/Context;Lcom/google/googlenav/common/a;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    const-string v0, "gps"

    invoke-direct {p0, v0, p1}, LU/b;-><init>(Ljava/lang/String;LU/T;)V

    .line 34
    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/a;

    iput-object v0, p0, LU/c;->b:Lcom/google/googlenav/common/a;

    .line 35
    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    .line 36
    return-void
.end method

.method private static a()I
    .registers 1

    .prologue
    .line 93
    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->c:I

    return v0
.end method


# virtual methods
.method public b()V
    .registers 8

    .prologue
    .line 63
    invoke-virtual {p0}, LU/c;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 76
    :cond_6
    :goto_6
    return-void

    .line 67
    :cond_7
    iget-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_6

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 71
    invoke-static {}, LU/c;->a()I

    move-result v2

    .line 72
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 73
    iget-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    const-string v1, "gps"

    int-to-long v2, v2

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 75
    invoke-super {p0}, LU/b;->b()V

    goto :goto_6
.end method

.method public d()V
    .registers 2

    .prologue
    .line 80
    invoke-virtual {p0}, LU/c;->e()Z

    move-result v0

    if-nez v0, :cond_7

    .line 85
    :goto_6
    return-void

    .line 83
    :cond_7
    invoke-super {p0}, LU/b;->d()V

    .line 84
    iget-object v0, p0, LU/c;->a:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    goto :goto_6
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .registers 5
    .parameter

    .prologue
    .line 40
    new-instance v0, LU/R;

    invoke-direct {v0, p1}, LU/R;-><init>(Landroid/location/Location;)V

    .line 41
    sget-object v1, LU/S;->a:LU/S;

    invoke-virtual {v0, v1}, LU/R;->a(LU/S;)V

    .line 42
    iget-object v1, p0, LU/c;->b:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LU/R;->setTime(J)V

    .line 43
    invoke-super {p0, v0}, LU/b;->a(LU/R;)V

    .line 44
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 53
    invoke-super {p0, p1, p1}, LU/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 48
    invoke-super {p0, p1, p1}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-super {p0, p1, p1, p2}, LU/b;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 59
    return-void
.end method
