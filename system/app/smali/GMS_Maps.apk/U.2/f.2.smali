.class LU/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LU/F;


# static fields
.field private static a:F

.field private static b:F

.field private static c:F

.field private static d:F

.field private static e:F

.field private static f:F

.field private static g:F

.field private static h:F

.field private static i:F


# instance fields
.field private final j:Lbi/d;

.field private final k:Ljava/util/ArrayList;

.field private l:LU/h;


# direct methods
.method public constructor <init>(Lbi/d;)V
    .registers 3
    .parameter

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, LU/f;->l:LU/h;

    .line 118
    iput-object p1, p0, LU/f;->j:Lbi/d;

    .line 119
    invoke-direct {p0}, LU/f;->a()V

    .line 120
    return-void
.end method

.method private static a(F)F
    .registers 3
    .parameter

    .prologue
    .line 693
    sget v0, LU/f;->g:F

    mul-float/2addr v0, p0

    .line 694
    sget v1, LU/f;->h:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_c

    .line 695
    sget v0, LU/f;->h:F

    .line 699
    :cond_b
    :goto_b
    return v0

    .line 696
    :cond_c
    sget v1, LU/f;->i:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_b

    .line 697
    sget v0, LU/f;->i:F

    goto :goto_b
.end method

.method private static a(FJ)F
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 561
    long-to-float v0, p1

    const/high16 v1, 0x447a

    div-float/2addr v0, v1

    .line 562
    sget v1, LU/f;->c:F

    mul-float/2addr v0, v1

    .line 563
    invoke-static {v0, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 564
    sget v1, LU/f;->d:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_13

    sget v0, LU/f;->d:F

    :cond_13
    return v0
.end method

.method private a(LU/h;Ljava/util/ArrayList;)I
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 489
    .line 490
    invoke-virtual {p1}, LU/h;->d()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 491
    const/4 v0, 0x1

    .line 493
    :goto_8
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    move v1, v0

    :goto_e
    if-ge v2, v3, :cond_25

    .line 494
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    .line 495
    if-eq p1, v0, :cond_26

    invoke-virtual {v0}, LU/h;->d()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 496
    add-int/lit8 v0, v1, 0x1

    .line 493
    :goto_20
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_e

    .line 499
    :cond_25
    return v1

    :cond_26
    move v0, v1

    goto :goto_20

    :cond_28
    move v0, v1

    goto :goto_8
.end method

.method private a(LU/h;LU/R;)LU/h;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 271
    invoke-direct {p0, p1, p2}, LU/f;->b(LU/h;LU/R;)Ljava/util/ArrayList;

    move-result-object v4

    .line 274
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 275
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    .line 276
    invoke-virtual {p1}, LU/h;->h()Lbi/a;

    move-result-object v1

    iget v1, v1, Lbi/a;->b:F

    const/4 v6, 0x0

    cmpl-float v1, v1, v6

    if-eqz v1, :cond_2b

    iget-object v1, v0, LU/h;->b:Lbi/t;

    iget-object v6, p1, LU/h;->b:Lbi/t;

    invoke-virtual {v1, v6}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3d

    :cond_2b
    move v1, v3

    .line 279
    :goto_2c
    invoke-virtual {v0, v1}, LU/h;->a(Z)V

    .line 280
    invoke-direct {p0, v0, p1}, LU/f;->a(LU/h;LU/h;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 281
    invoke-virtual {p1}, LU/h;->c()LU/j;

    move-result-object v1

    invoke-virtual {v0, v1}, LU/h;->a(LU/j;)V

    goto :goto_a

    :cond_3d
    move v1, v2

    .line 276
    goto :goto_2c

    .line 283
    :cond_3f
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_a

    .line 286
    :cond_43
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_76

    .line 288
    invoke-direct {p0, v4, p1}, LU/f;->a(Ljava/util/ArrayList;LU/h;)LU/h;

    move-result-object v0

    .line 289
    invoke-virtual {v0, p1}, LU/h;->c(LU/h;)LU/i;

    .line 290
    invoke-virtual {v0}, LU/h;->e()LU/i;

    move-result-object v1

    sget-object v4, LU/i;->c:LU/i;

    if-ne v1, v4, :cond_66

    .line 291
    :goto_58
    if-eqz v3, :cond_68

    invoke-virtual {v0}, LU/h;->d()Z

    move-result v1

    if-eqz v1, :cond_68

    .line 292
    sget-object v1, LU/j;->b:LU/j;

    invoke-virtual {v0, v1}, LU/h;->a(LU/j;)V

    .line 298
    :cond_65
    :goto_65
    return-object v0

    :cond_66
    move v3, v2

    .line 290
    goto :goto_58

    .line 293
    :cond_68
    if-nez v3, :cond_65

    invoke-virtual {v0}, LU/h;->d()Z

    move-result v1

    if-nez v1, :cond_65

    .line 294
    sget-object v1, LU/j;->c:LU/j;

    invoke-virtual {v0, v1}, LU/h;->a(LU/j;)V

    goto :goto_65

    .line 298
    :cond_76
    const/4 v0, 0x0

    goto :goto_65
.end method

.method private a(Ljava/util/ArrayList;LU/h;)LU/h;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 578
    const/4 v1, 0x0

    .line 579
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    :goto_7
    if-ge v2, v3, :cond_1c

    .line 580
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    .line 581
    if-eqz v1, :cond_17

    invoke-direct {p0, v0, v1, p2}, LU/f;->a(LU/h;LU/h;LU/h;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 579
    :cond_17
    :goto_17
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_7

    .line 586
    :cond_1c
    if-eqz p2, :cond_2b

    invoke-virtual {v1, p2}, LU/h;->a(LU/h;)Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-virtual {v1, p2}, LU/h;->b(LU/h;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 590
    :goto_2a
    return-object p2

    :cond_2b
    move-object p2, v1

    goto :goto_2a

    :cond_2d
    move-object v0, v1

    goto :goto_17
.end method

.method private a(Lbi/a;Lbi/a;)Lbi/a;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 396
    iget-object v0, p2, Lbi/a;->a:Lbi/t;

    iget-object v1, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 397
    iget-object v0, p0, LU/f;->j:Lbi/d;

    sget v1, LU/f;->a:F

    neg-float v1, v1

    invoke-static {v0, p2, v1}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object p2

    .line 399
    :cond_13
    return-object p2
.end method

.method private a(Lbi/t;LU/R;II)Ljava/util/ArrayList;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 418
    iget-object v0, p0, LU/f;->j:Lbi/d;

    invoke-virtual {v0, p1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v9

    .line 419
    invoke-virtual {v9}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 420
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v10

    .line 423
    if-gez p4, :cond_14

    .line 424
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p4

    .line 426
    :cond_14
    invoke-virtual {v9, p3}, Lbi/h;->b(I)LaN/B;

    move-result-object v0

    move v3, p3

    move-object v2, v0

    .line 427
    :goto_1a
    if-ge v3, p4, :cond_9b

    .line 430
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v9, v0}, Lbi/h;->b(I)LaN/B;

    move-result-object v8

    .line 444
    invoke-static {p2}, LX/g;->a(Landroid/location/Location;)LaN/B;

    move-result-object v4

    .line 445
    invoke-virtual {v2, v8}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 446
    invoke-virtual {v9}, Lbi/h;->b()Lbi/q;

    move-result-object v0

    sget-object v1, Lbi/q;->e:Lbi/q;

    if-ne v0, v1, :cond_4c

    .line 447
    invoke-static {v4, v2}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v5

    .line 448
    invoke-static {v5, v9, v3}, LU/f;->a(FLbi/h;I)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 449
    new-instance v0, LU/h;

    iget-object v1, p0, LU/f;->j:Lbi/d;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, LU/h;-><init>(Lbi/d;Lbi/t;ILU/R;FFZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    :cond_4c
    :goto_4c
    add-int/lit8 v3, v3, 0x1

    move-object v2, v8

    goto :goto_1a

    .line 455
    :cond_50
    invoke-static {v4, v2, v8}, LX/g;->c(LaN/B;LaN/B;LaN/B;)F

    move-result v6

    .line 457
    invoke-static {v4, v2}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    .line 458
    invoke-static {v4, v8}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v1

    .line 459
    invoke-static {v4, v2, v8}, LX/g;->b(LaN/B;LaN/B;LaN/B;)F

    move-result v5

    .line 462
    const/4 v2, 0x0

    cmpl-float v2, v6, v2

    if-ltz v2, :cond_90

    const/high16 v2, 0x3f80

    cmpg-float v2, v6, v2

    if-gtz v2, :cond_90

    .line 470
    :goto_6b
    invoke-static {v5, v9, v3}, LU/f;->a(FLbi/h;I)Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-static {v6}, LU/f;->b(F)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 472
    const/4 v0, 0x0

    cmpg-float v0, v6, v0

    if-ltz v0, :cond_82

    const/high16 v0, 0x3f80

    cmpl-float v0, v6, v0

    if-lez v0, :cond_99

    :cond_82
    const/4 v7, 0x1

    .line 473
    :goto_83
    new-instance v0, LU/h;

    iget-object v1, p0, LU/f;->j:Lbi/d;

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, LU/h;-><init>(Lbi/d;Lbi/t;ILU/R;FFZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4c

    .line 464
    :cond_90
    const/4 v2, 0x0

    cmpg-float v2, v6, v2

    if-gez v2, :cond_97

    move v5, v0

    .line 465
    goto :goto_6b

    :cond_97
    move v5, v1

    .line 467
    goto :goto_6b

    .line 472
    :cond_99
    const/4 v7, 0x0

    goto :goto_83

    .line 477
    :cond_9b
    return-object v10
.end method

.method private a()V
    .registers 4

    .prologue
    .line 123
    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    .line 124
    iget v1, v0, LX/q;->e:F

    iget v2, v0, LX/q;->d:F

    add-float/2addr v1, v2

    sput v1, LU/f;->a:F

    .line 125
    iget v1, v0, LX/q;->A:F

    sput v1, LU/f;->b:F

    .line 126
    iget v1, v0, LX/q;->j:F

    sput v1, LU/f;->c:F

    .line 127
    iget v1, v0, LX/q;->k:F

    sput v1, LU/f;->d:F

    .line 128
    iget v1, v0, LX/q;->l:F

    sput v1, LU/f;->e:F

    .line 129
    iget v1, v0, LX/q;->m:F

    sput v1, LU/f;->f:F

    .line 130
    iget v1, v0, LX/q;->n:F

    sput v1, LU/f;->g:F

    .line 131
    iget v1, v0, LX/q;->o:F

    sput v1, LU/f;->h:F

    .line 132
    iget v0, v0, LX/q;->p:F

    sput v0, LU/f;->i:F

    .line 133
    return-void
.end method

.method private a(LU/h;)V
    .registers 4
    .parameter

    .prologue
    .line 514
    iget v0, p1, LU/h;->g:F

    sget v1, LU/f;->a:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_e

    .line 515
    sget-object v0, LU/j;->a:LU/j;

    invoke-virtual {p1, v0}, LU/h;->a(LU/j;)V

    .line 525
    :goto_d
    return-void

    .line 518
    :cond_e
    iget-object v0, p0, LU/f;->j:Lbi/d;

    iget-object v1, p1, LU/h;->b:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->c()Z

    move-result v0

    if-nez v0, :cond_22

    invoke-direct {p0, p1}, LU/f;->c(LU/h;)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 520
    :cond_22
    invoke-virtual {p1}, LU/h;->g()Lbi/a;

    move-result-object v0

    invoke-virtual {p1, v0}, LU/h;->a(Lbi/a;)V

    .line 521
    sget-object v0, LU/j;->c:LU/j;

    invoke-virtual {p1, v0}, LU/h;->a(LU/j;)V

    goto :goto_d

    .line 524
    :cond_2f
    sget-object v0, LU/j;->a:LU/j;

    invoke-virtual {p1, v0}, LU/h;->a(LU/j;)V

    goto :goto_d
.end method

.method private a(Ljava/util/ArrayList;LU/h;Lbi/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 212
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 213
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    .line 214
    if-eq v0, p2, :cond_4

    .line 215
    invoke-virtual {v0}, LU/h;->h()Lbi/a;

    move-result-object v0

    .line 216
    iget-object v2, p0, LU/f;->j:Lbi/d;

    invoke-static {v2, p3, v0}, LX/g;->a(Lbi/d;Lbi/a;Lbi/a;)F

    move-result v0

    .line 217
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v2, LU/f;->a:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_4

    .line 218
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 222
    :cond_2a
    return-void
.end method

.method private static a(FLbi/h;I)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 682
    invoke-virtual {p1, p2}, Lbi/h;->d(I)F

    move-result v0

    .line 683
    invoke-static {v0}, LU/f;->a(F)F

    move-result v0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private a(LU/h;LU/h;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 536
    const/4 v0, 0x1

    .line 537
    invoke-virtual {p1}, LU/h;->h()Lbi/a;

    move-result-object v1

    .line 538
    invoke-virtual {p2}, LU/h;->h()Lbi/a;

    move-result-object v2

    .line 540
    iget-object v1, v1, Lbi/a;->a:Lbi/t;

    iget-object v2, v2, Lbi/a;->a:Lbi/t;

    invoke-virtual {v1, v2}, Lbi/t;->a(Lbi/t;)I

    move-result v1

    if-lez v1, :cond_17

    .line 541
    invoke-direct {p0, p1}, LU/f;->b(LU/h;)Z

    move-result v0

    .line 543
    :cond_17
    return v0
.end method

.method private a(LU/h;LU/h;LU/h;)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 607
    invoke-virtual {p1}, LU/h;->c()LU/j;

    move-result-object v2

    invoke-virtual {p2}, LU/h;->c()LU/j;

    move-result-object v3

    invoke-virtual {v2, v3}, LU/j;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    .line 609
    if-lez v2, :cond_11

    .line 651
    :cond_10
    :goto_10
    return v0

    .line 611
    :cond_11
    if-gez v2, :cond_15

    move v0, v1

    .line 612
    goto :goto_10

    .line 615
    :cond_15
    invoke-virtual {p1}, LU/h;->h()Lbi/a;

    move-result-object v2

    .line 616
    invoke-virtual {p2}, LU/h;->h()Lbi/a;

    move-result-object v3

    .line 617
    if-eqz p3, :cond_3d

    .line 619
    invoke-virtual {p3}, LU/h;->h()Lbi/a;

    move-result-object v4

    .line 620
    iget-object v5, v2, Lbi/a;->a:Lbi/t;

    iget-object v6, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v5, v6}, Lbi/t;->a(Lbi/t;)I

    move-result v5

    .line 621
    iget-object v6, v3, Lbi/a;->a:Lbi/t;

    iget-object v4, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v6, v4}, Lbi/t;->a(Lbi/t;)I

    move-result v4

    .line 622
    if-ltz v5, :cond_37

    if-ltz v4, :cond_10

    .line 624
    :cond_37
    if-gez v5, :cond_3d

    if-ltz v4, :cond_3d

    move v0, v1

    .line 625
    goto :goto_10

    .line 629
    :cond_3d
    invoke-direct {p0, p1, p2}, LU/f;->b(LU/h;LU/h;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 631
    invoke-direct {p0, p2, p1}, LU/f;->b(LU/h;LU/h;)Z

    move-result v4

    if-nez v4, :cond_76

    .line 633
    invoke-direct {p0, p1}, LU/f;->c(LU/h;)Z

    move-result v4

    if-nez v4, :cond_55

    invoke-direct {p0, p2}, LU/f;->c(LU/h;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 638
    :cond_55
    if-eqz p3, :cond_63

    invoke-virtual {p1, p3}, LU/h;->b(LU/h;)Z

    move-result v4

    if-nez v4, :cond_63

    invoke-virtual {p2, p3}, LU/h;->b(LU/h;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 646
    :cond_63
    invoke-virtual {v2, v3}, Lbi/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_71

    .line 647
    invoke-virtual {p1, p2}, LU/h;->a(LU/h;)Z

    move-result v2

    if-eqz v2, :cond_10

    move v0, v1

    goto :goto_10

    .line 649
    :cond_71
    invoke-virtual {p1, p2}, LU/h;->a(LU/h;)Z

    move-result v0

    goto :goto_10

    :cond_76
    move v0, v1

    .line 651
    goto :goto_10
.end method

.method private b(Lbi/a;Lbi/a;)Lbi/a;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 404
    iget-object v0, p2, Lbi/a;->a:Lbi/t;

    iget-object v1, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v1}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 405
    iget-object v0, p0, LU/f;->j:Lbi/d;

    sget v1, LU/f;->a:F

    invoke-static {v0, p2, v1}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object p2

    .line 407
    :cond_12
    return-object p2
.end method

.method private b(LU/R;)Ljava/util/ArrayList;
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 230
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 232
    iget-object v0, p0, LU/f;->l:LU/h;

    if-eqz v0, :cond_14

    .line 233
    iget-object v0, p0, LU/f;->l:LU/h;

    invoke-direct {p0, v0, p1}, LU/f;->a(LU/h;LU/R;)LU/h;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_14

    .line 235
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    :cond_14
    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_1b
    if-ge v2, v4, :cond_37

    .line 240
    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    .line 241
    iget-object v5, p0, LU/f;->l:LU/h;

    if-ne v0, v5, :cond_2d

    .line 239
    :cond_29
    :goto_29
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1b

    .line 244
    :cond_2d
    invoke-direct {p0, v0, p1}, LU/f;->a(LU/h;LU/R;)LU/h;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_29

    .line 246
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_29

    .line 251
    :cond_37
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 252
    invoke-direct {p0, p1}, LU/f;->c(LU/R;)Ljava/util/ArrayList;

    move-result-object v2

    .line 253
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_45
    if-ge v1, v4, :cond_5f

    .line 254
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    .line 255
    invoke-direct {p0, v0}, LU/f;->a(LU/h;)V

    .line 256
    invoke-virtual {v0}, LU/h;->c()LU/j;

    move-result-object v5

    sget-object v6, LU/j;->a:LU/j;

    if-eq v5, v6, :cond_5b

    .line 257
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    :cond_5b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_45

    .line 261
    :cond_5f
    return-object v3
.end method

.method private b(LU/h;LU/R;)Ljava/util/ArrayList;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v1, -0x1

    .line 327
    invoke-static {p2}, LX/g;->a(Landroid/location/Location;)LaN/B;

    move-result-object v0

    .line 329
    iget-object v2, p1, LU/h;->d:LaN/B;

    invoke-static {v0, v2}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    invoke-virtual {p2}, LU/R;->getTime()J

    move-result-wide v2

    iget-wide v4, p1, LU/h;->e:J

    sub-long/2addr v2, v4

    invoke-static {v0, v2, v3}, LU/f;->a(FJ)F

    move-result v0

    .line 331
    iget v2, p1, LU/h;->h:F

    add-float/2addr v0, v2

    invoke-virtual {p2}, LU/R;->getAccuracy()F

    move-result v2

    add-float/2addr v0, v2

    .line 333
    invoke-virtual {p1}, LU/h;->g()Lbi/a;

    move-result-object v2

    .line 335
    iget-object v3, p0, LU/f;->j:Lbi/d;

    neg-float v4, v0

    invoke-static {v3, v2, v4}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v3

    .line 336
    invoke-direct {p0, v2, v3}, LU/f;->a(Lbi/a;Lbi/a;)Lbi/a;

    move-result-object v3

    .line 337
    iget-object v4, p0, LU/f;->j:Lbi/d;

    invoke-static {v4, v2, v0}, LX/g;->a(Lbi/d;Lbi/a;F)Lbi/a;

    move-result-object v0

    .line 338
    invoke-direct {p0, v2, v0}, LU/f;->b(Lbi/a;Lbi/a;)Lbi/a;

    move-result-object v4

    .line 341
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 342
    new-instance v0, Lbi/v;

    iget-object v2, p0, LU/f;->j:Lbi/d;

    invoke-direct {v0, v2}, Lbi/v;-><init>(Lbi/d;)V

    iget-object v2, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Lbi/v;->b(Lbi/t;)Lbi/v;

    move-result-object v6

    .line 343
    :cond_49
    invoke-virtual {v6}, Lbi/v;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f4

    .line 344
    invoke-virtual {v6}, Lbi/v;->a()Lbi/t;

    move-result-object v7

    .line 347
    invoke-virtual {p2}, LU/R;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_6f

    iget-object v0, p0, LU/f;->j:Lbi/d;

    invoke-virtual {v0, v7}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->c()Z

    move-result v0

    if-eqz v0, :cond_6f

    invoke-virtual {p2}, LU/R;->getSpeed()F

    move-result v0

    sget v2, LU/f;->b:F

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_49

    .line 353
    :cond_6f
    iget-object v0, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f7

    .line 354
    iget-object v0, p0, LU/f;->j:Lbi/d;

    iget-object v2, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v0, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    iget v2, v3, Lbi/a;->b:F

    invoke-virtual {v0, v2}, Lbi/h;->b(F)I

    move-result v0

    .line 359
    :goto_85
    iget-object v2, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v2}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f5

    .line 360
    iget-object v2, p0, LU/f;->j:Lbi/d;

    iget-object v8, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v2, v8}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    iget v8, v4, Lbi/a;->b:F

    invoke-virtual {v2, v8}, Lbi/h;->b(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 363
    :goto_9d
    invoke-direct {p0, v7, p2, v0, v2}, LU/f;->a(Lbi/t;LU/R;II)Ljava/util/ArrayList;

    move-result-object v2

    .line 367
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c2

    iget-object v0, v3, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c2

    .line 368
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    invoke-virtual {v0}, LU/h;->g()Lbi/a;

    move-result-object v0

    .line 369
    invoke-virtual {v3, v0}, Lbi/a;->a(Lbi/a;)I

    move-result v0

    if-lez v0, :cond_c2

    .line 370
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 374
    :cond_c2
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e9

    iget-object v0, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e9

    .line 375
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    .line 376
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LU/h;

    invoke-virtual {v0}, LU/h;->g()Lbi/a;

    move-result-object v0

    .line 377
    invoke-virtual {v4, v0}, Lbi/a;->a(Lbi/a;)I

    move-result v0

    if-gez v0, :cond_e9

    .line 378
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 381
    :cond_e9
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 382
    iget-object v0, v4, Lbi/a;->a:Lbi/t;

    invoke-virtual {v7, v0}, Lbi/t;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 386
    :cond_f4
    return-object v5

    :cond_f5
    move v2, v1

    goto :goto_9d

    :cond_f7
    move v0, v1

    goto :goto_85
.end method

.method private b(Ljava/util/ArrayList;LU/h;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 716
    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 717
    iget-object v0, p0, LU/f;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 718
    iput-object p2, p0, LU/f;->l:LU/h;

    .line 719
    return-void
.end method

.method private static b(F)Z
    .registers 3
    .parameter

    .prologue
    .line 707
    sget v0, LU/f;->e:F

    neg-float v0, v0

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_12

    const/high16 v0, 0x3f80

    sget v1, LU/f;->e:F

    add-float/2addr v0, v1

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private b(LU/h;)Z
    .registers 4
    .parameter

    .prologue
    .line 550
    iget v0, p1, LU/h;->g:F

    sget v1, LU/f;->a:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private b(LU/h;LU/h;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 667
    iget v0, p1, LU/h;->f:F

    iget v1, p2, LU/h;->f:F

    sget v2, LU/f;->f:F

    sub-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_d

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private c(LU/R;)Ljava/util/ArrayList;
    .registers 8
    .parameter

    .prologue
    const/4 v5, -0x1

    .line 306
    new-instance v0, Lbi/v;

    iget-object v1, p0, LU/f;->j:Lbi/d;

    invoke-direct {v0, v1}, Lbi/v;-><init>(Lbi/d;)V

    .line 307
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 308
    :cond_c
    :goto_c
    invoke-virtual {v0}, Lbi/v;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 309
    invoke-virtual {v0}, Lbi/v;->a()Lbi/t;

    move-result-object v2

    .line 312
    invoke-virtual {p1}, LU/R;->hasSpeed()Z

    move-result v3

    if-eqz v3, :cond_32

    iget-object v3, p0, LU/f;->j:Lbi/d;

    invoke-virtual {v3, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v3

    invoke-virtual {v3}, Lbi/h;->c()Z

    move-result v3

    if-eqz v3, :cond_32

    invoke-virtual {p1}, LU/R;->getSpeed()F

    move-result v3

    sget v4, LU/f;->b:F

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_c

    .line 316
    :cond_32
    invoke-direct {p0, v2, p1, v5, v5}, LU/f;->a(Lbi/t;LU/R;II)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_c

    .line 318
    :cond_3a
    return-object v1
.end method

.method private c(LU/h;)Z
    .registers 4
    .parameter

    .prologue
    .line 658
    iget-boolean v0, p1, LU/h;->i:Z

    if-eqz v0, :cond_e

    iget v0, p1, LU/h;->g:F

    sget v1, LU/f;->a:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public a(LU/R;)LU/G;
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const-wide/16 v5, 0x0

    const/4 v2, 0x0

    .line 137
    invoke-virtual {p1}, LU/R;->getLatitude()D

    move-result-wide v3

    cmpl-double v0, v3, v5

    if-eqz v0, :cond_31

    invoke-virtual {p1}, LU/R;->getLongitude()D

    move-result-wide v3

    cmpl-double v0, v3, v5

    if-eqz v0, :cond_31

    move v0, v1

    :goto_15
    const-string v3, "Location should have both latitude and longitude"

    invoke-static {v0, v3}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    .line 140
    invoke-direct {p0, p1}, LU/f;->b(LU/R;)Ljava/util/ArrayList;

    move-result-object v4

    .line 145
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 148
    sget-object v0, LU/H;->b:LU/H;

    move-object v1, v0

    move-object v0, v2

    .line 193
    :goto_28
    invoke-direct {p0, v4, v2}, LU/f;->b(Ljava/util/ArrayList;LU/h;)V

    .line 194
    new-instance v2, LU/G;

    invoke-direct {v2, v1, v0}, LU/G;-><init>(LU/H;Lbi/a;)V

    return-object v2

    .line 137
    :cond_31
    const/4 v0, 0x0

    goto :goto_15

    .line 150
    :cond_33
    iget-object v0, p0, LU/f;->l:LU/h;

    invoke-direct {p0, v4, v0}, LU/f;->a(Ljava/util/ArrayList;LU/h;)LU/h;

    move-result-object v3

    .line 151
    invoke-virtual {v3}, LU/h;->h()Lbi/a;

    move-result-object v0

    .line 152
    invoke-direct {p0, v4, v3, v0}, LU/f;->a(Ljava/util/ArrayList;LU/h;Lbi/a;)V

    .line 163
    sget-object v5, LU/g;->a:[I

    invoke-virtual {v3}, LU/h;->c()LU/j;

    move-result-object v6

    invoke-virtual {v6}, LU/j;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_6a

    .line 189
    sget-object v0, LU/H;->b:LU/H;

    move-object v1, v0

    move-object v0, v2

    goto :goto_28

    .line 165
    :pswitch_54
    sget-object v1, LU/H;->e:LU/H;

    move-object v2, v3

    .line 166
    goto :goto_28

    .line 171
    :pswitch_58
    invoke-direct {p0, v3, v4}, LU/f;->a(LU/h;Ljava/util/ArrayList;)I

    move-result v5

    .line 173
    if-ne v5, v1, :cond_67

    .line 174
    sget-object v1, LU/H;->e:LU/H;

    .line 175
    sget-object v2, LU/j;->d:LU/j;

    invoke-virtual {v3, v2}, LU/h;->a(LU/j;)V

    move-object v2, v3

    goto :goto_28

    .line 181
    :cond_67
    sget-object v1, LU/H;->c:LU/H;

    goto :goto_28

    .line 163
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_54
        :pswitch_58
    .end packed-switch
.end method
