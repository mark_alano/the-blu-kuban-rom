.class public LW/f;
.super LW/k;
.source "SourceFile"


# static fields
.field public static final a:LW/f;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 19
    new-instance v0, LW/f;

    invoke-direct {v0}, LW/f;-><init>()V

    sput-object v0, LW/f;->a:LW/f;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, LW/k;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LU/z;)V
    .registers 4
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, LW/f;->b:LW/h;

    sget-object v1, LW/n;->a:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    move-result-object v0

    invoke-virtual {v0, p1}, LW/k;->a(LU/z;)V

    .line 43
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 52
    invoke-virtual {p0}, LW/f;->m()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/h;->a()V

    .line 53
    sget-object v0, LU/B;->c:LU/B;

    .line 54
    iget-object v0, p0, LW/f;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_13

    .line 56
    iget-object v0, v0, LU/z;->f:LU/B;

    .line 58
    :cond_13
    invoke-virtual {p0}, LW/f;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    iget-object v1, p0, LW/f;->d:LW/g;

    invoke-virtual {v1}, LW/g;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->b(Z)V

    .line 59
    return-void
.end method

.method public b(LU/z;)V
    .registers 4
    .parameter

    .prologue
    .line 36
    invoke-virtual {p0}, LW/f;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    iget-object v1, p0, LW/f;->d:LW/g;

    invoke-virtual {v1}, LW/g;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->b(Z)V

    .line 37
    return-void
.end method

.method public c(LU/z;)V
    .registers 4
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, LW/f;->b:LW/h;

    sget-object v1, LW/n;->b:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    .line 48
    return-void
.end method

.method protected e()V
    .registers 3

    .prologue
    .line 25
    invoke-virtual {p0}, LW/f;->k()Lcom/google/android/maps/rideabout/view/j;

    .line 26
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LW/f;->a(Z)V

    .line 27
    iget-object v0, p0, LW/f;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    .line 28
    if-eqz v0, :cond_14

    .line 29
    iget-object v1, p0, LW/f;->c:LW/j;

    invoke-virtual {v1, v0}, LW/j;->c(LU/z;)V

    .line 31
    :cond_14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LW/f;->b(Z)V

    .line 32
    return-void
.end method
