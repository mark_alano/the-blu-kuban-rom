.class public Lx/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:LD/b;

.field private final b:LE/i;

.field private final c:Lx/b;

.field private final d:Lx/a;


# direct methods
.method public constructor <init>(ILx/a;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x1

    invoke-static {p1, v0}, LE/l;->b(II)LE/l;

    move-result-object v0

    iput-object v0, p0, Lx/c;->b:LE/i;

    .line 75
    new-instance v0, Lx/b;

    invoke-direct {v0}, Lx/b;-><init>()V

    iput-object v0, p0, Lx/c;->c:Lx/b;

    .line 76
    iput-object p2, p0, Lx/c;->d:Lx/a;

    .line 77
    return-void
.end method

.method private c()V
    .registers 2

    .prologue
    .line 157
    iget-object v0, p0, Lx/c;->a:LD/b;

    if-eqz v0, :cond_c

    .line 158
    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lx/c;->a:LD/b;

    .line 161
    :cond_c
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0}, LE/i;->b()I

    move-result v0

    return v0
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 83
    if-lez p2, :cond_16

    .line 84
    iget-object v0, p0, Lx/c;->d:Lx/a;

    iget-object v1, p0, Lx/c;->c:Lx/b;

    invoke-virtual {v0, p1, v1}, Lx/a;->a(ILx/b;)V

    .line 85
    iget-object v0, p0, Lx/c;->b:LE/i;

    iget-object v1, p0, Lx/c;->c:Lx/b;

    iget v1, v1, Lx/b;->a:I

    iget-object v2, p0, Lx/c;->c:Lx/b;

    iget v2, v2, Lx/b;->b:I

    invoke-virtual {v0, v1, v2, p2}, LE/i;->a(III)V

    .line 87
    :cond_16
    return-void
.end method

.method public a(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lx/c;->a:LD/b;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {v0}, LD/b;->a()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    if-eq v0, v1, :cond_13

    .line 103
    invoke-direct {p0}, Lx/c;->c()V

    .line 105
    :cond_13
    iget-object v0, p0, Lx/c;->d:Lx/a;

    invoke-virtual {v0, p1}, Lx/a;->b(LD/a;)V

    .line 106
    iget-object v0, p0, Lx/c;->a:LD/b;

    if-nez v0, :cond_29

    .line 107
    iget-object v0, p0, Lx/c;->d:Lx/a;

    invoke-virtual {v0, p1}, Lx/a;->a(LD/a;)LD/b;

    move-result-object v0

    iput-object v0, p0, Lx/c;->a:LD/b;

    .line 108
    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {v0}, LD/b;->f()V

    .line 110
    :cond_29
    iget-object v0, p0, Lx/c;->a:LD/b;

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 111
    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 112
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 142
    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0}, LE/i;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 120
    invoke-direct {p0}, Lx/c;->c()V

    .line 121
    return-void
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lx/c;->b:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 128
    invoke-direct {p0}, Lx/c;->c()V

    .line 129
    return-void
.end method
