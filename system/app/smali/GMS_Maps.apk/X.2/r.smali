.class public Lx/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[F

.field private static final b:[F

.field private static final c:[F


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0x14

    .line 53
    const/16 v0, 0xc

    new-array v0, v0, [F

    fill-array-data v0, :array_1a

    sput-object v0, Lx/r;->a:[F

    .line 60
    new-array v0, v1, [F

    fill-array-data v0, :array_36

    sput-object v0, Lx/r;->b:[F

    .line 67
    new-array v0, v1, [F

    fill-array-data v0, :array_62

    sput-object v0, Lx/r;->c:[F

    return-void

    .line 53
    :array_1a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 60
    :array_36
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    .line 67
    :array_62
    .array-data 0x4
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0xbft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public static a(LE/q;LE/e;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 86
    const v0, 0x3d80adfd

    .line 87
    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v4, v3

    .line 88
    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v5

    .line 89
    const/high16 v1, 0x3f80

    .line 97
    const/4 v0, 0x0

    move v3, v1

    move v1, v2

    :goto_13
    const/16 v6, 0x64

    if-ge v0, v6, :cond_2f

    .line 98
    add-float v6, v3, v2

    add-float v7, v1, v2

    invoke-interface {p0, v6, v7, v2}, LE/q;->a(FFF)V

    .line 99
    if-eqz p1, :cond_24

    .line 100
    int-to-short v6, v0

    invoke-interface {p1, v6}, LE/e;->d(I)V

    .line 105
    :cond_24
    neg-float v6, v1

    .line 109
    mul-float/2addr v6, v4

    add-float/2addr v6, v3

    .line 110
    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    .line 113
    mul-float v3, v6, v5

    .line 114
    mul-float/2addr v1, v5

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 116
    :cond_2f
    return-void
.end method

.method public static b(LE/q;LE/e;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 131
    const v1, 0x3d80adfd

    .line 132
    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->tan(D)D

    move-result-wide v3

    double-to-float v5, v3

    .line 133
    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v6

    .line 134
    const/high16 v4, 0x3f80

    .line 142
    invoke-interface {p0, v2, v2, v2}, LE/q;->a(FFF)V

    .line 143
    if-eqz p1, :cond_19

    .line 144
    invoke-interface {p1, v0}, LE/e;->d(I)V

    :cond_19
    move v1, v2

    move v3, v4

    .line 146
    :goto_1b
    const/16 v7, 0x64

    if-ge v0, v7, :cond_39

    .line 147
    add-float v7, v3, v2

    add-float v8, v1, v2

    invoke-interface {p0, v7, v8, v2}, LE/q;->a(FFF)V

    .line 148
    if-eqz p1, :cond_2e

    .line 149
    add-int/lit8 v7, v0, 0x1

    int-to-short v7, v7

    invoke-interface {p1, v7}, LE/e;->d(I)V

    .line 154
    :cond_2e
    neg-float v7, v1

    .line 158
    mul-float/2addr v7, v5

    add-float/2addr v7, v3

    .line 159
    mul-float/2addr v3, v5

    add-float/2addr v1, v3

    .line 162
    mul-float v3, v7, v6

    .line 163
    mul-float/2addr v1, v6

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 165
    :cond_39
    if-eqz p1, :cond_40

    .line 166
    const/4 v0, 0x1

    invoke-interface {p1, v0}, LE/e;->d(I)V

    .line 170
    :goto_3f
    return-void

    .line 168
    :cond_40
    add-float v0, v4, v2

    invoke-interface {p0, v0, v2, v2}, LE/q;->a(FFF)V

    goto :goto_3f
.end method
