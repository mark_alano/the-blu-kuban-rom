.class final LY/H;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:LY/n;


# direct methods
.method constructor <init>(LY/n;)V
    .registers 2
    .parameter

    .prologue
    .line 4464
    iput-object p1, p0, LY/H;->a:LY/n;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    .prologue
    .line 4493
    iget-object v0, p0, LY/H;->a:LY/n;

    invoke-virtual {v0}, LY/n;->clear()V

    .line 4494
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 4483
    iget-object v0, p0, LY/H;->a:LY/n;

    invoke-virtual {v0, p1}, LY/n;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 4478
    iget-object v0, p0, LY/H;->a:LY/n;

    invoke-virtual {v0}, LY/n;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 4468
    new-instance v0, LY/G;

    iget-object v1, p0, LY/H;->a:LY/n;

    invoke-direct {v0, v1}, LY/G;-><init>(LY/n;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 4488
    iget-object v0, p0, LY/H;->a:LY/n;

    invoke-virtual {v0, p1}, LY/n;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public size()I
    .registers 2

    .prologue
    .line 4473
    iget-object v0, p0, LY/H;->a:LY/n;

    invoke-virtual {v0}, LY/n;->size()I

    move-result v0

    return v0
.end method
