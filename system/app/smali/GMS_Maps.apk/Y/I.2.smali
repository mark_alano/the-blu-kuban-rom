.class LY/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LY/Z;


# instance fields
.field volatile a:LY/Z;

.field final b:Lae/o;

.field final c:Lcom/google/common/base/Y;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 3504
    invoke-static {}, LY/n;->o()LY/Z;

    move-result-object v0

    invoke-direct {p0, v0}, LY/I;-><init>(LY/Z;)V

    .line 3505
    return-void
.end method

.method public constructor <init>(LY/Z;)V
    .registers 3
    .parameter

    .prologue
    .line 3507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3500
    invoke-static {}, Lae/o;->b()Lae/o;

    move-result-object v0

    iput-object v0, p0, LY/I;->b:Lae/o;

    .line 3501
    new-instance v0, Lcom/google/common/base/Y;

    invoke-direct {v0}, Lcom/google/common/base/Y;-><init>()V

    iput-object v0, p0, LY/I;->c:Lcom/google/common/base/Y;

    .line 3508
    iput-object p1, p0, LY/I;->a:LY/Z;

    .line 3509
    return-void
.end method

.method private static a(Lae/o;Ljava/lang/Throwable;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3536
    :try_start_0
    invoke-virtual {p0, p1}, Lae/o;->a(Ljava/lang/Throwable;)Z
    :try_end_3
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_3} :catch_5

    move-result v0

    .line 3539
    :goto_4
    return v0

    .line 3537
    :catch_5
    move-exception v0

    .line 3539
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private b(Ljava/lang/Throwable;)Lae/i;
    .registers 3
    .parameter

    .prologue
    .line 3544
    invoke-static {}, Lae/o;->b()Lae/o;

    move-result-object v0

    .line 3545
    invoke-static {v0, p1}, LY/I;->a(Lae/o;Ljava/lang/Throwable;)Z

    .line 3546
    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 3523
    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->a()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;LY/L;)LY/Z;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3605
    return-object p0
.end method

.method public a(Ljava/lang/Object;LY/k;)Lae/i;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3564
    iget-object v0, p0, LY/I;->c:Lcom/google/common/base/Y;

    invoke-virtual {v0}, Lcom/google/common/base/Y;->a()Lcom/google/common/base/Y;

    .line 3565
    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    .line 3567
    if-nez v0, :cond_1f

    .line 3568
    :try_start_d
    invoke-virtual {p2, p1}, LY/k;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3569
    invoke-virtual {p0, v0}, LY/I;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v0, p0, LY/I;->b:Lae/o;

    .line 3576
    :cond_19
    :goto_19
    return-object v0

    .line 3569
    :cond_1a
    invoke-static {v0}, Lae/g;->a(Ljava/lang/Object;)Lae/i;

    move-result-object v0

    goto :goto_19

    .line 3571
    :cond_1f
    invoke-virtual {p2, p1, v0}, LY/k;->a(Ljava/lang/Object;Ljava/lang/Object;)Lae/i;

    move-result-object v0

    .line 3573
    if-nez v0, :cond_19

    const/4 v0, 0x0

    invoke-static {v0}, Lae/g;->a(Ljava/lang/Object;)Lae/i;
    :try_end_29
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_29} :catch_2b

    move-result-object v0

    goto :goto_19

    .line 3575
    :catch_2b
    move-exception v0

    .line 3576
    invoke-virtual {p0, v0}, LY/I;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_35

    iget-object v0, p0, LY/I;->b:Lae/o;

    goto :goto_19

    :cond_35
    invoke-direct {p0, v0}, LY/I;->b(Ljava/lang/Throwable;)Lae/i;

    move-result-object v0

    goto :goto_19
.end method

.method public a(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 3551
    if-eqz p1, :cond_6

    .line 3554
    invoke-virtual {p0, p1}, LY/I;->b(Ljava/lang/Object;)Z

    .line 3561
    :goto_5
    return-void

    .line 3557
    :cond_6
    invoke-static {}, LY/n;->o()LY/Z;

    move-result-object v0

    iput-object v0, p0, LY/I;->a:LY/Z;

    goto :goto_5
.end method

.method public a(Ljava/lang/Throwable;)Z
    .registers 3
    .parameter

    .prologue
    .line 3531
    iget-object v0, p0, LY/I;->b:Lae/o;

    invoke-static {v0, p1}, LY/I;->a(Lae/o;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b()LY/L;
    .registers 2

    .prologue
    .line 3600
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 3527
    iget-object v0, p0, LY/I;->b:Lae/o;

    invoke-virtual {v0, p1}, Lae/o;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 3513
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 3518
    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->d()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3586
    iget-object v0, p0, LY/I;->b:Lae/o;

    invoke-static {v0}, Lae/q;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .registers 3

    .prologue
    .line 3581
    iget-object v0, p0, LY/I;->c:Lcom/google/common/base/Y;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/common/base/Y;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()LY/Z;
    .registers 2

    .prologue
    .line 3595
    iget-object v0, p0, LY/I;->a:LY/Z;

    return-object v0
.end method

.method public get()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3591
    iget-object v0, p0, LY/I;->a:LY/Z;

    invoke-interface {v0}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
