.class LY/V;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LY/L;


# instance fields
.field final g:Ljava/lang/Object;

.field final h:I

.field final i:LY/L;

.field volatile j:LY/Z;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILY/L;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1138
    invoke-static {}, LY/n;->o()LY/Z;

    move-result-object v0

    iput-object v0, p0, LY/V;->j:LY/Z;

    .line 1060
    iput-object p1, p0, LY/V;->g:Ljava/lang/Object;

    .line 1061
    iput p2, p0, LY/V;->h:I

    .line 1062
    iput-object p3, p0, LY/V;->i:LY/L;

    .line 1063
    return-void
.end method


# virtual methods
.method public a()LY/Z;
    .registers 2

    .prologue
    .line 1142
    iget-object v0, p0, LY/V;->j:LY/Z;

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1079
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1089
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LY/Z;)V
    .registers 2
    .parameter

    .prologue
    .line 1147
    iput-object p1, p0, LY/V;->j:LY/Z;

    .line 1148
    return-void
.end method

.method public b()LY/L;
    .registers 2

    .prologue
    .line 1157
    iget-object v0, p0, LY/V;->i:LY/L;

    return-object v0
.end method

.method public b(J)V
    .registers 4
    .parameter

    .prologue
    .line 1111
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1099
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1152
    iget v0, p0, LY/V;->h:I

    return v0
.end method

.method public c(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1067
    iget-object v0, p0, LY/V;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public d(LY/L;)V
    .registers 3
    .parameter

    .prologue
    .line 1131
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 1074
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()LY/L;
    .registers 2

    .prologue
    .line 1084
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()LY/L;
    .registers 2

    .prologue
    .line 1094
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()J
    .registers 2

    .prologue
    .line 1106
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()LY/L;
    .registers 2

    .prologue
    .line 1116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public j()LY/L;
    .registers 2

    .prologue
    .line 1126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
