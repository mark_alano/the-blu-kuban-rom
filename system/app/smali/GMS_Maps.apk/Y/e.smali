.class public final LY/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/common/base/ab;

.field static final b:LY/m;

.field static final c:Lcom/google/common/base/ab;

.field static final d:Lcom/google/common/base/ae;

.field private static final u:Ljava/util/logging/Logger;


# instance fields
.field e:Z

.field f:I

.field g:I

.field h:J

.field i:J

.field j:LY/av;

.field k:LY/P;

.field l:LY/P;

.field m:J

.field n:J

.field o:J

.field p:Lcom/google/common/base/t;

.field q:Lcom/google/common/base/t;

.field r:LY/at;

.field s:Lcom/google/common/base/ae;

.field t:Lcom/google/common/base/ab;


# direct methods
.method static constructor <clinit>()V
    .registers 13

    .prologue
    const-wide/16 v1, 0x0

    .line 142
    new-instance v0, LY/f;

    invoke-direct {v0}, LY/f;-><init>()V

    invoke-static {v0}, Lcom/google/common/base/ac;->a(Ljava/lang/Object;)Lcom/google/common/base/ab;

    move-result-object v0

    sput-object v0, LY/e;->a:Lcom/google/common/base/ab;

    .line 164
    new-instance v0, LY/m;

    move-wide v3, v1

    move-wide v5, v1

    move-wide v7, v1

    move-wide v9, v1

    move-wide v11, v1

    invoke-direct/range {v0 .. v12}, LY/m;-><init>(JJJJJJ)V

    sput-object v0, LY/e;->b:LY/m;

    .line 166
    new-instance v0, LY/g;

    invoke-direct {v0}, LY/g;-><init>()V

    sput-object v0, LY/e;->c:Lcom/google/common/base/ab;

    .line 190
    new-instance v0, LY/h;

    invoke-direct {v0}, LY/h;-><init>()V

    sput-object v0, LY/e;->d:Lcom/google/common/base/ae;

    .line 197
    const-class v0, LY/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LY/e;->u:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    const-wide/16 v1, -0x1

    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, LY/e;->e:Z

    .line 203
    iput v3, p0, LY/e;->f:I

    .line 204
    iput v3, p0, LY/e;->g:I

    .line 205
    iput-wide v1, p0, LY/e;->h:J

    .line 206
    iput-wide v1, p0, LY/e;->i:J

    .line 212
    iput-wide v1, p0, LY/e;->m:J

    .line 213
    iput-wide v1, p0, LY/e;->n:J

    .line 214
    iput-wide v1, p0, LY/e;->o:J

    .line 222
    sget-object v0, LY/e;->c:Lcom/google/common/base/ab;

    iput-object v0, p0, LY/e;->t:Lcom/google/common/base/ab;

    .line 225
    return-void
.end method

.method public static a()LY/e;
    .registers 1

    .prologue
    .line 232
    new-instance v0, LY/e;

    invoke-direct {v0}, LY/e;-><init>()V

    return-object v0
.end method

.method private p()V
    .registers 5

    .prologue
    .line 761
    iget-wide v0, p0, LY/e;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_9
    const-string v1, "refreshAfterWrite requires a LoadingCache"

    invoke-static {v0, v1}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    .line 762
    return-void

    .line 761
    :cond_f
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private q()V
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    .line 765
    iget-object v2, p0, LY/e;->j:LY/av;

    if-nez v2, :cond_16

    .line 766
    iget-wide v2, p0, LY/e;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_14

    :goto_e
    const-string v1, "maximumWeight requires weigher"

    invoke-static {v0, v1}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    .line 776
    :cond_13
    :goto_13
    return-void

    :cond_14
    move v0, v1

    .line 766
    goto :goto_e

    .line 768
    :cond_16
    iget-boolean v2, p0, LY/e;->e:Z

    if-eqz v2, :cond_28

    .line 769
    iget-wide v2, p0, LY/e;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_26

    :goto_20
    const-string v1, "weigher requires maximumWeight"

    invoke-static {v0, v1}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    goto :goto_13

    :cond_26
    move v0, v1

    goto :goto_20

    .line 771
    :cond_28
    iget-wide v0, p0, LY/e;->i:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_13

    .line 772
    sget-object v0, LY/e;->u:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "ignoring weigher specified without maximumWeight"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_13
.end method


# virtual methods
.method public a(J)LY/e;
    .registers 12
    .parameter

    .prologue
    const-wide/16 v7, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 343
    iget-wide v3, p0, LY/e;->h:J

    cmp-long v0, v3, v7

    if-nez v0, :cond_48

    move v0, v1

    :goto_b
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v5, p0, LY/e;->h:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/common/base/J;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 345
    iget-wide v3, p0, LY/e;->i:J

    cmp-long v0, v3, v7

    if-nez v0, :cond_4a

    move v0, v1

    :goto_21
    const-string v3, "maximum weight was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v5, p0, LY/e;->i:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/common/base/J;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 347
    iget-object v0, p0, LY/e;->j:LY/av;

    if-nez v0, :cond_4c

    move v0, v1

    :goto_35
    const-string v3, "maximum size can not be combined with weigher"

    invoke-static {v0, v3}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    .line 348
    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-ltz v0, :cond_4e

    :goto_40
    const-string v0, "maximum size must not be negative"

    invoke-static {v1, v0}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    .line 349
    iput-wide p1, p0, LY/e;->h:J

    .line 350
    return-object p0

    :cond_48
    move v0, v2

    .line 343
    goto :goto_b

    :cond_4a
    move v0, v2

    .line 345
    goto :goto_21

    :cond_4c
    move v0, v2

    .line 347
    goto :goto_35

    :cond_4e
    move v1, v2

    .line 348
    goto :goto_40
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)LY/e;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 559
    iget-wide v3, p0, LY/e;->m:J

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-nez v0, :cond_38

    move v0, v1

    :goto_b
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v5, p0, LY/e;->m:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/common/base/J;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 561
    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-ltz v0, :cond_3a

    move v0, v1

    :goto_21
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/common/base/J;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 562
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LY/e;->m:J

    .line 563
    return-object p0

    :cond_38
    move v0, v2

    .line 559
    goto :goto_b

    :cond_3a
    move v0, v2

    .line 561
    goto :goto_21
.end method

.method a(Z)Lcom/google/common/base/ae;
    .registers 3
    .parameter

    .prologue
    .line 659
    iget-object v0, p0, LY/e;->s:Lcom/google/common/base/ae;

    if-eqz v0, :cond_7

    .line 660
    iget-object v0, p0, LY/e;->s:Lcom/google/common/base/ae;

    .line 662
    :goto_6
    return-object v0

    :cond_7
    if-eqz p1, :cond_e

    invoke-static {}, Lcom/google/common/base/ae;->b()Lcom/google/common/base/ae;

    move-result-object v0

    goto :goto_6

    :cond_e
    sget-object v0, LY/e;->d:Lcom/google/common/base/ae;

    goto :goto_6
.end method

.method b()Lcom/google/common/base/t;
    .registers 3

    .prologue
    .line 256
    iget-object v0, p0, LY/e;->p:Lcom/google/common/base/t;

    invoke-virtual {p0}, LY/e;->h()LY/P;

    move-result-object v1

    invoke-virtual {v1}, LY/P;->a()Lcom/google/common/base/t;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/base/t;

    return-object v0
.end method

.method c()Lcom/google/common/base/t;
    .registers 3

    .prologue
    .line 274
    iget-object v0, p0, LY/e;->q:Lcom/google/common/base/t;

    invoke-virtual {p0}, LY/e;->i()LY/P;

    move-result-object v1

    invoke-virtual {v1}, LY/P;->a()Lcom/google/common/base/t;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/base/t;

    return-object v0
.end method

.method d()I
    .registers 3

    .prologue
    .line 296
    iget v0, p0, LY/e;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    const/16 v0, 0x10

    :goto_7
    return v0

    :cond_8
    iget v0, p0, LY/e;->f:I

    goto :goto_7
.end method

.method e()I
    .registers 3

    .prologue
    .line 326
    iget v0, p0, LY/e;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x4

    :goto_6
    return v0

    :cond_7
    iget v0, p0, LY/e;->g:I

    goto :goto_6
.end method

.method f()J
    .registers 5

    .prologue
    const-wide/16 v0, 0x0

    .line 426
    iget-wide v2, p0, LY/e;->m:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_e

    iget-wide v2, p0, LY/e;->n:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_f

    .line 429
    :cond_e
    :goto_e
    return-wide v0

    :cond_f
    iget-object v0, p0, LY/e;->j:LY/av;

    if-nez v0, :cond_16

    iget-wide v0, p0, LY/e;->h:J

    goto :goto_e

    :cond_16
    iget-wide v0, p0, LY/e;->i:J

    goto :goto_e
.end method

.method g()LY/av;
    .registers 3

    .prologue
    .line 435
    iget-object v0, p0, LY/e;->j:LY/av;

    sget-object v1, LY/j;->a:LY/j;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/av;

    return-object v0
.end method

.method h()LY/P;
    .registers 3

    .prologue
    .line 472
    iget-object v0, p0, LY/e;->k:LY/P;

    sget-object v1, LY/P;->a:LY/P;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/P;

    return-object v0
.end method

.method i()LY/P;
    .registers 3

    .prologue
    .line 536
    iget-object v0, p0, LY/e;->l:LY/P;

    sget-object v1, LY/P;->a:LY/P;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/P;

    return-object v0
.end method

.method j()J
    .registers 5

    .prologue
    .line 567
    iget-wide v0, p0, LY/e;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, LY/e;->m:J

    goto :goto_a
.end method

.method k()J
    .registers 5

    .prologue
    .line 601
    iget-wide v0, p0, LY/e;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, LY/e;->n:J

    goto :goto_a
.end method

.method l()J
    .registers 5

    .prologue
    .line 639
    iget-wide v0, p0, LY/e;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, LY/e;->o:J

    goto :goto_a
.end method

.method m()LY/at;
    .registers 3

    .prologue
    .line 708
    iget-object v0, p0, LY/e;->r:LY/at;

    sget-object v1, LY/i;->a:LY/i;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/at;

    return-object v0
.end method

.method n()Lcom/google/common/base/ab;
    .registers 2

    .prologue
    .line 721
    iget-object v0, p0, LY/e;->t:Lcom/google/common/base/ab;

    return-object v0
.end method

.method public o()LY/d;
    .registers 2

    .prologue
    .line 755
    invoke-direct {p0}, LY/e;->q()V

    .line 756
    invoke-direct {p0}, LY/e;->p()V

    .line 757
    new-instance v0, LY/J;

    invoke-direct {v0, p0}, LY/J;-><init>(LY/e;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v3, -0x1

    const-wide/16 v5, -0x1

    .line 784
    invoke-static {p0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    move-result-object v0

    .line 785
    iget v1, p0, LY/e;->f:I

    if-eq v1, v3, :cond_12

    .line 786
    const-string v1, "initialCapacity"

    iget v2, p0, LY/e;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    .line 788
    :cond_12
    iget v1, p0, LY/e;->g:I

    if-eq v1, v3, :cond_1d

    .line 789
    const-string v1, "concurrencyLevel"

    iget v2, p0, LY/e;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;I)Lcom/google/common/base/G;

    .line 791
    :cond_1d
    iget-wide v1, p0, LY/e;->i:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_2e

    .line 792
    iget-object v1, p0, LY/e;->j:LY/av;

    if-nez v1, :cond_b4

    .line 793
    const-string v1, "maximumSize"

    iget-wide v2, p0, LY/e;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/G;->a(Ljava/lang/String;J)Lcom/google/common/base/G;

    .line 798
    :cond_2e
    :goto_2e
    iget-wide v1, p0, LY/e;->m:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_4e

    .line 799
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, LY/e;->m:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    .line 801
    :cond_4e
    iget-wide v1, p0, LY/e;->n:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_6e

    .line 802
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, LY/e;->n:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    .line 804
    :cond_6e
    iget-object v1, p0, LY/e;->k:LY/P;

    if-eqz v1, :cond_81

    .line 805
    const-string v1, "keyStrength"

    iget-object v2, p0, LY/e;->k:LY/P;

    invoke-virtual {v2}, LY/P;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    .line 807
    :cond_81
    iget-object v1, p0, LY/e;->l:LY/P;

    if-eqz v1, :cond_94

    .line 808
    const-string v1, "valueStrength"

    iget-object v2, p0, LY/e;->l:LY/P;

    invoke-virtual {v2}, LY/P;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/G;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/G;

    .line 810
    :cond_94
    iget-object v1, p0, LY/e;->p:Lcom/google/common/base/t;

    if-eqz v1, :cond_9d

    .line 811
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, Lcom/google/common/base/G;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    .line 813
    :cond_9d
    iget-object v1, p0, LY/e;->q:Lcom/google/common/base/t;

    if-eqz v1, :cond_a6

    .line 814
    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, Lcom/google/common/base/G;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    .line 816
    :cond_a6
    iget-object v1, p0, LY/e;->r:LY/at;

    if-eqz v1, :cond_af

    .line 817
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, Lcom/google/common/base/G;->a(Ljava/lang/Object;)Lcom/google/common/base/G;

    .line 819
    :cond_af
    invoke-virtual {v0}, Lcom/google/common/base/G;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 795
    :cond_b4
    const-string v1, "maximumWeight"

    iget-wide v2, p0, LY/e;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/G;->a(Ljava/lang/String;J)Lcom/google/common/base/G;

    goto/16 :goto_2e
.end method
