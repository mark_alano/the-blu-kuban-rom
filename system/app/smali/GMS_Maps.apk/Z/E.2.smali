.class public Lz/E;
.super Lz/D;
.source "SourceFile"

# interfaces
.implements Lz/h;


# instance fields
.field private i:[F


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 40
    const-class v0, Lz/I;

    invoke-direct {p0, v0}, Lz/D;-><init>(Ljava/lang/Class;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lz/E;->i:[F

    .line 41
    return-void
.end method

.method public constructor <init>(I[F)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 53
    invoke-static {p1}, Lz/E;->b(I)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lz/D;-><init>(Ljava/lang/Class;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lz/E;->i:[F

    .line 54
    new-array v0, v2, [F

    iput-object v0, p0, Lz/E;->i:[F

    .line 55
    iget-object v0, p0, Lz/E;->i:[F

    invoke-static {p2, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    return-void
.end method

.method private static b(I)Ljava/lang/Class;
    .registers 4
    .parameter

    .prologue
    .line 64
    packed-switch p0, :pswitch_data_26

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid blend mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :pswitch_1c
    const-class v0, Lz/H;

    .line 70
    :goto_1e
    return-object v0

    .line 68
    :pswitch_1f
    const-class v0, Lz/I;

    goto :goto_1e

    .line 70
    :pswitch_22
    const-class v0, Lz/G;

    goto :goto_1e

    .line 64
    nop

    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
    .end packed-switch
.end method


# virtual methods
.method public a(FFFF)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    iget-boolean v0, p0, Lz/E;->c:Z

    if-eqz v0, :cond_7

    .line 105
    invoke-static {}, Lz/k;->c()V

    .line 107
    :cond_7
    iget-object v0, p0, Lz/E;->i:[F

    if-nez v0, :cond_10

    .line 108
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lz/E;->i:[F

    .line 110
    :cond_10
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 111
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 112
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 113
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    .line 114
    return-void
.end method

.method public a(I)V
    .registers 6
    .parameter

    .prologue
    const/high16 v3, 0x437f

    .line 85
    iget-boolean v0, p0, Lz/E;->c:Z

    if-eqz v0, :cond_9

    .line 86
    invoke-static {}, Lz/k;->c()V

    .line 88
    :cond_9
    iget-object v0, p0, Lz/E;->i:[F

    if-nez v0, :cond_12

    .line 89
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lz/E;->i:[F

    .line 91
    :cond_12
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 92
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 93
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 94
    iget-object v0, p0, Lz/E;->i:[F

    const/4 v1, 0x3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 95
    return-void
.end method
