.class public Lz/F;
.super Lz/C;
.source "SourceFile"


# instance fields
.field protected c:I

.field protected d:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Lz/C;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method


# virtual methods
.method protected a(I)V
    .registers 4
    .parameter

    .prologue
    .line 177
    invoke-super {p0, p1}, Lz/C;->a(I)V

    .line 178
    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lz/F;->c:I

    .line 179
    const-string v0, "ShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget v0, p0, Lz/F;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1f

    .line 181
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sTexture0 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_1f
    iget v0, p0, Lz/F;->c:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 188
    const-string v0, "ShaderState"

    const-string v1, "glUniform"

    invoke-static {v0, v1}, Lz/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v0, "uTextureMatrix"

    invoke-virtual {p0, p1, v0}, Lz/F;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lz/F;->d:I

    .line 192
    return-void
.end method
