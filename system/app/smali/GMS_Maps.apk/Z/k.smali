.class public Lz/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/c;


# static fields
.field private static m:Z

.field private static final o:Ljava/util/List;

.field private static final p:Ljava/util/List;

.field private static r:Ljava/lang/String;


# instance fields
.field b:I

.field c:I

.field private d:[Lz/o;

.field private e:Lz/m;

.field private f:[Ljava/util/Set;

.field private g:I

.field private h:I

.field private i:Ljava/lang/Object;

.field private j:[Lz/z;

.field private k:Ljava/util/Set;

.field private l:Ljava/util/Set;

.field private final n:Lz/n;

.field private q:I

.field private s:Lz/x;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 130
    const/4 v0, 0x0

    sput-boolean v0, Lz/k;->m:Z

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lz/k;->o:Ljava/util/List;

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lz/k;->p:Ljava/util/List;

    .line 175
    const/4 v0, 0x0

    sput-object v0, Lz/k;->r:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 576
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_13

    .line 579
    invoke-static {}, Ljava/lang/Thread;->dumpStack()V

    .line 580
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/System;->exit(I)V

    .line 582
    new-instance v1, Lz/q;

    invoke-direct {v1, v0}, Lz/q;-><init>(I)V

    throw v1

    .line 584
    :cond_13
    return-void
.end method

.method static c()V
    .registers 2

    .prologue
    .line 544
    sget-boolean v0, Lz/k;->m:Z

    if-nez v0, :cond_c

    .line 545
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to update live data from outside a Behavior"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_c
    return-void
.end method


# virtual methods
.method a()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 321
    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_a

    .line 322
    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-interface {v0}, Lz/x;->a()V

    .line 326
    :cond_a
    invoke-virtual {p0}, Lz/k;->b()V

    .line 333
    iget-object v2, p0, Lz/k;->i:Ljava/lang/Object;

    monitor-enter v2

    .line 334
    :try_start_10
    iget v0, p0, Lz/k;->g:I

    iput v0, p0, Lz/k;->h:I

    .line 335
    iget v0, p0, Lz/k;->g:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x2

    iput v0, p0, Lz/k;->g:I

    .line 336
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v3, p0, Lz/k;->h:I

    aget-object v0, v0, v3

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 337
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_10 .. :try_end_26} :catchall_40

    .line 345
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v2, p0, Lz/k;->g:I

    aget-object v0, v0, v2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_30
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/b;

    .line 346
    invoke-interface {v0}, Lz/b;->a()V

    goto :goto_30

    .line 337
    :catchall_40
    move-exception v0

    :try_start_41
    monitor-exit v2
    :try_end_42
    .catchall {:try_start_41 .. :try_end_42} :catchall_40

    throw v0

    .line 348
    :cond_43
    const/4 v0, 0x1

    sput-boolean v0, Lz/k;->m:Z

    .line 350
    :try_start_46
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v2, p0, Lz/k;->g:I

    aget-object v0, v0, v2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_50
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_64

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/b;

    .line 351
    invoke-interface {v0, p0}, Lz/b;->b(Lz/c;)V
    :try_end_5f
    .catchall {:try_start_46 .. :try_end_5f} :catchall_60

    goto :goto_50

    .line 354
    :catchall_60
    move-exception v0

    sput-boolean v1, Lz/k;->m:Z

    throw v0

    :cond_64
    sput-boolean v1, Lz/k;->m:Z

    .line 361
    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_6f

    .line 362
    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-interface {v0}, Lz/x;->b()V

    .line 366
    :cond_6f
    iget-object v0, p0, Lz/k;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_75
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_85

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/A;

    .line 367
    invoke-virtual {v0}, Lz/A;->a()V

    goto :goto_75

    .line 369
    :cond_85
    iget-object v0, p0, Lz/k;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/e;

    .line 370
    invoke-virtual {v0}, Lz/e;->c()V

    goto :goto_8b

    .line 374
    :cond_9b
    iget-object v2, p0, Lz/k;->j:[Lz/z;

    array-length v3, v2

    move v0, v1

    :goto_9f
    if-ge v0, v3, :cond_ab

    aget-object v1, v2, v0

    .line 375
    if-eqz v1, :cond_a8

    .line 379
    invoke-virtual {v1, p0}, Lz/z;->a(Lz/k;)V

    .line 374
    :cond_a8
    add-int/lit8 v0, v0, 0x1

    goto :goto_9f

    .line 384
    :cond_ab
    iget-object v0, p0, Lz/k;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/e;

    .line 385
    invoke-virtual {v0}, Lz/e;->d()V

    goto :goto_b1

    .line 387
    :cond_c1
    iget-object v0, p0, Lz/k;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/A;

    .line 388
    invoke-virtual {v0}, Lz/A;->d()V

    goto :goto_c7

    .line 394
    :cond_d7
    iget v0, p0, Lz/k;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lz/k;->q:I

    .line 395
    return-void
.end method

.method a(Ljavax/microedition/khronos/opengles/GL10;Lz/N;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 516
    iget-object v0, p0, Lz/k;->n:Lz/n;

    invoke-virtual {v0}, Lz/n;->a()V

    .line 518
    iget-object v2, p0, Lz/k;->j:[Lz/z;

    array-length v3, v2

    move v0, v1

    :goto_a
    if-ge v0, v3, :cond_16

    aget-object v4, v2, v0

    .line 519
    if-eqz v4, :cond_13

    .line 520
    invoke-virtual {v4, p0}, Lz/z;->b(Lz/k;)V

    .line 518
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_16
    move v0, v1

    .line 524
    :goto_17
    sget v2, Lz/o;->a:I

    if-ge v0, v2, :cond_23

    .line 525
    iget-object v2, p0, Lz/k;->d:[Lz/o;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 524
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 528
    :cond_23
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 529
    const/16 v2, 0xd57

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 530
    aget v0, v0, v1

    iput v0, p0, Lz/k;->b:I

    .line 531
    const-wide/high16 v0, 0x4000

    iget v2, p0, Lz/k;->b:I

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lz/k;->c:I

    .line 533
    sget-object v0, Lz/k;->r:Ljava/lang/String;

    if-nez v0, :cond_49

    .line 534
    const/16 v0, 0x1f03

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lz/k;->r:Ljava/lang/String;

    .line 537
    :cond_49
    return-void
.end method

.method public a(Lz/b;Lz/O;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 606
    sget-object v0, Lz/c;->a:Lz/P;

    if-ne p2, v0, :cond_15

    .line 607
    iget-object v1, p0, Lz/k;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 608
    :try_start_7
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v2, p0, Lz/k;->h:I

    aget-object v0, v0, v2

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 609
    monitor-exit v1

    .line 613
    return-void

    .line 609
    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_12

    throw v0

    .line 611
    :cond_15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unimplemented WakeUpCondition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lz/i;)V
    .registers 5
    .parameter

    .prologue
    .line 479
    iget-object v0, p0, Lz/k;->e:Lz/m;

    new-instance v1, Lz/l;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lz/l;-><init>(Lz/i;Z)V

    invoke-static {v0, v1}, Lz/m;->a(Lz/m;Lz/l;)V

    .line 483
    return-void
.end method

.method b()V
    .registers 9

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 403
    .line 404
    :cond_3
    :goto_3
    iget-object v0, p0, Lz/k;->e:Lz/m;

    invoke-static {v0}, Lz/m;->a(Lz/m;)Lz/l;

    move-result-object v2

    if-eqz v2, :cond_110

    .line 406
    :try_start_b
    iget v0, v2, Lz/l;->a:I

    packed-switch v0, :pswitch_data_112

    goto :goto_3

    .line 408
    :pswitch_11
    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/j;->c:Lz/j;

    invoke-virtual {v0, p0, v3}, Lz/i;->a(Lz/k;Lz/j;)Z

    .line 409
    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    invoke-virtual {v0}, Lz/i;->a()B

    move-result v3

    move v0, v1

    .line 410
    :goto_23
    if-ge v0, v7, :cond_38

    .line 411
    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_35

    .line 412
    iget-object v4, p0, Lz/k;->j:[Lz/z;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v5

    invoke-virtual {v4, v5}, Lz/z;->a(Lz/i;)V

    .line 410
    :cond_35
    add-int/lit8 v0, v0, 0x1

    goto :goto_23

    .line 415
    :cond_38
    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/p;->a:Lz/p;

    invoke-virtual {v0, v3}, Lz/i;->a(Lz/p;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 417
    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v2

    invoke-interface {v0, v2}, Lz/x;->a(Lz/i;)V

    goto :goto_3

    .line 471
    :catch_52
    move-exception v0

    goto :goto_3

    .line 421
    :pswitch_54
    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/j;->c:Lz/j;

    invoke-virtual {v0, p0, v3}, Lz/i;->a(Lz/k;Lz/j;)Z

    .line 422
    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    invoke-virtual {v0}, Lz/i;->a()B

    move-result v3

    move v0, v1

    .line 423
    :goto_66
    if-ge v0, v7, :cond_7b

    .line 424
    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_78

    .line 425
    iget-object v4, p0, Lz/k;->j:[Lz/z;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v5

    invoke-virtual {v4, v5}, Lz/z;->b(Lz/i;)V

    .line 423
    :cond_78
    add-int/lit8 v0, v0, 0x1

    goto :goto_66

    .line 428
    :cond_7b
    iget-object v0, p0, Lz/k;->s:Lz/x;

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v0

    sget-object v3, Lz/p;->a:Lz/p;

    invoke-virtual {v0, v3}, Lz/i;->a(Lz/p;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 430
    iget-object v0, p0, Lz/k;->s:Lz/x;

    invoke-virtual {v2}, Lz/l;->a()Lz/i;

    move-result-object v2

    invoke-interface {v0, v2}, Lz/x;->b(Lz/i;)V

    goto/16 :goto_3

    .line 436
    :pswitch_96
    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v0

    sget-object v3, Lz/j;->c:Lz/j;

    invoke-virtual {v0, p0, v3}, Lz/e;->a(Lz/k;Lz/j;)Z

    .line 437
    iget-object v0, p0, Lz/k;->k:Ljava/util/Set;

    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v3

    invoke-virtual {v3}, Lz/e;->b()Lz/A;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 438
    iget-object v0, p0, Lz/k;->l:Ljava/util/Set;

    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 439
    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v0

    invoke-virtual {v0}, Lz/e;->e()B

    move-result v3

    move v0, v1

    .line 440
    :goto_be
    if-ge v0, v7, :cond_3

    .line 441
    shl-int v4, v6, v0

    and-int/2addr v4, v3

    if-eqz v4, :cond_d0

    .line 442
    iget-object v4, p0, Lz/k;->j:[Lz/z;

    aget-object v4, v4, v0

    invoke-virtual {v2}, Lz/l;->b()Lz/e;

    move-result-object v5

    invoke-virtual {v4, v5}, Lz/z;->a(Lz/e;)V

    .line 440
    :cond_d0
    add-int/lit8 v0, v0, 0x1

    goto :goto_be

    .line 449
    :pswitch_d3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Remove camera not implemented"

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451
    :pswitch_db
    const/4 v0, 0x1

    sput-boolean v0, Lz/k;->m:Z
    :try_end_de
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_de} :catch_52

    .line 453
    :try_start_de
    invoke-virtual {v2}, Lz/l;->c()Lz/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lz/b;->a(Lz/c;)V
    :try_end_e5
    .catchall {:try_start_de .. :try_end_e5} :catchall_ea

    .line 455
    const/4 v0, 0x0

    :try_start_e6
    sput-boolean v0, Lz/k;->m:Z

    goto/16 :goto_3

    :catchall_ea
    move-exception v0

    const/4 v2, 0x0

    sput-boolean v2, Lz/k;->m:Z

    throw v0

    .line 459
    :pswitch_ef
    const/4 v0, 0x1

    sput-boolean v0, Lz/k;->m:Z
    :try_end_f2
    .catch Ljava/lang/Exception; {:try_start_e6 .. :try_end_f2} :catch_52

    .line 462
    :try_start_f2
    iget-object v3, p0, Lz/k;->i:Ljava/lang/Object;

    monitor-enter v3
    :try_end_f5
    .catchall {:try_start_f2 .. :try_end_f5} :catchall_10b

    .line 463
    :try_start_f5
    iget-object v0, p0, Lz/k;->f:[Ljava/util/Set;

    iget v4, p0, Lz/k;->h:I

    aget-object v0, v0, v4

    invoke-virtual {v2}, Lz/l;->c()Lz/b;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 465
    monitor-exit v3
    :try_end_103
    .catchall {:try_start_f5 .. :try_end_103} :catchall_108

    .line 467
    const/4 v0, 0x0

    :try_start_104
    sput-boolean v0, Lz/k;->m:Z
    :try_end_106
    .catch Ljava/lang/Exception; {:try_start_104 .. :try_end_106} :catch_52

    goto/16 :goto_3

    .line 465
    :catchall_108
    move-exception v0

    :try_start_109
    monitor-exit v3
    :try_end_10a
    .catchall {:try_start_109 .. :try_end_10a} :catchall_108

    :try_start_10a
    throw v0
    :try_end_10b
    .catchall {:try_start_10a .. :try_end_10b} :catchall_10b

    .line 467
    :catchall_10b
    move-exception v0

    const/4 v2, 0x0

    :try_start_10d
    sput-boolean v2, Lz/k;->m:Z

    throw v0
    :try_end_110
    .catch Ljava/lang/Exception; {:try_start_10d .. :try_end_110} :catch_52

    .line 475
    :cond_110
    return-void

    .line 406
    nop

    :pswitch_data_112
    .packed-switch 0x0
        :pswitch_11
        :pswitch_54
        :pswitch_db
        :pswitch_ef
        :pswitch_96
        :pswitch_d3
    .end packed-switch
.end method

.method public b(Lz/i;)V
    .registers 5
    .parameter

    .prologue
    .line 492
    iget-object v0, p0, Lz/k;->e:Lz/m;

    new-instance v1, Lz/l;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lz/l;-><init>(Lz/i;Z)V

    invoke-static {v0, v1}, Lz/m;->a(Lz/m;Lz/l;)V

    .line 496
    return-void
.end method

.method public d()Lz/B;
    .registers 2

    .prologue
    .line 587
    iget-object v0, p0, Lz/k;->n:Lz/n;

    return-object v0
.end method

.method e()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 596
    const/4 v0, 0x0

    :goto_2
    sget v1, Lz/o;->a:I

    if-ge v0, v1, :cond_1a

    .line 597
    iget-object v1, p0, Lz/k;->d:[Lz/o;

    aget-object v1, v1, v0

    if-eqz v1, :cond_17

    .line 598
    iget-object v1, p0, Lz/k;->d:[Lz/o;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0, v2}, Lz/o;->a(Lz/k;Lz/o;)V

    .line 599
    iget-object v1, p0, Lz/k;->d:[Lz/o;

    aput-object v2, v1, v0

    .line 596
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 602
    :cond_1a
    return-void
.end method
