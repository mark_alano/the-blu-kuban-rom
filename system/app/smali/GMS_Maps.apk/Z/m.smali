.class public Lz/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lz/l;

.field private b:Lz/l;


# direct methods
.method private declared-synchronized a()Lz/l;
    .registers 3

    .prologue
    .line 865
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lz/m;->a:Lz/l;

    .line 866
    if-eqz v0, :cond_12

    .line 867
    invoke-static {v0}, Lz/l;->a(Lz/l;)Lz/l;

    move-result-object v1

    iput-object v1, p0, Lz/m;->a:Lz/l;

    .line 868
    iget-object v1, p0, Lz/m;->a:Lz/l;

    if-nez v1, :cond_12

    .line 869
    const/4 v1, 0x0

    iput-object v1, p0, Lz/m;->b:Lz/l;
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_14

    .line 872
    :cond_12
    monitor-exit p0

    return-object v0

    .line 865
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lz/m;)Lz/l;
    .registers 2
    .parameter

    .prologue
    .line 844
    invoke-direct {p0}, Lz/m;->a()Lz/l;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lz/l;)V
    .registers 3
    .parameter

    .prologue
    .line 879
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lz/m;->b:Lz/l;

    .line 880
    iput-object p1, p0, Lz/m;->b:Lz/l;

    .line 881
    if-nez v0, :cond_f

    .line 882
    iput-object p1, p0, Lz/m;->a:Lz/l;

    .line 886
    :goto_9
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lz/l;->a(Lz/l;Lz/l;)Lz/l;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_13

    .line 887
    monitor-exit p0

    return-void

    .line 884
    :cond_f
    :try_start_f
    invoke-static {v0, p1}, Lz/l;->a(Lz/l;Lz/l;)Lz/l;
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_13

    goto :goto_9

    .line 879
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lz/m;Lz/l;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 844
    invoke-direct {p0, p1}, Lz/m;->a(Lz/l;)V

    return-void
.end method
