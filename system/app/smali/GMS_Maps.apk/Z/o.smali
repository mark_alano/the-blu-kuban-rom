.class public abstract Lz/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I

.field public static final b:I


# instance fields
.field protected c:Z

.field protected d:I

.field protected e:Lz/k;

.field final f:Lz/p;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 55
    invoke-static {}, Lz/p;->values()[Lz/p;

    move-result-object v0

    array-length v0, v0

    sput v0, Lz/o;->a:I

    .line 61
    sget-object v0, Lz/p;->b:Lz/p;

    invoke-virtual {v0}, Lz/p;->a()I

    move-result v0

    sput v0, Lz/o;->b:I

    return-void
.end method

.method protected constructor <init>(Lz/p;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-boolean v0, p0, Lz/o;->c:Z

    .line 76
    iput v0, p0, Lz/o;->d:I

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lz/o;->e:Lz/k;

    .line 96
    iput-object p1, p0, Lz/o;->f:Lz/p;

    .line 102
    return-void
.end method


# virtual methods
.method a(Lz/i;)V
    .registers 3
    .parameter

    .prologue
    .line 170
    iget v0, p0, Lz/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lz/o;->d:I

    .line 171
    return-void
.end method

.method abstract a(Lz/k;Lz/o;)V
.end method

.method a(Lz/k;Lz/j;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 125
    iget-boolean v1, p2, Lz/j;->e:Z

    iget-boolean v2, p0, Lz/o;->c:Z

    if-ne v1, v2, :cond_c

    iget-boolean v1, p2, Lz/j;->f:Z

    if-nez v1, :cond_c

    .line 137
    :cond_b
    :goto_b
    return v0

    .line 129
    :cond_c
    iget-boolean v1, p2, Lz/j;->e:Z

    if-nez v1, :cond_18

    iget-boolean v1, p2, Lz/j;->f:Z

    if-nez v1, :cond_18

    .line 130
    iget v1, p0, Lz/o;->d:I

    if-nez v1, :cond_b

    .line 135
    :cond_18
    iget-boolean v0, p2, Lz/j;->e:Z

    iput-boolean v0, p0, Lz/o;->c:Z

    .line 136
    iget-boolean v0, p0, Lz/o;->c:Z

    if-eqz v0, :cond_24

    :goto_20
    iput-object p1, p0, Lz/o;->e:Lz/k;

    .line 137
    const/4 v0, 0x1

    goto :goto_b

    .line 136
    :cond_24
    const/4 p1, 0x0

    goto :goto_20
.end method

.method b(Lz/i;)V
    .registers 3
    .parameter

    .prologue
    .line 185
    iget v0, p0, Lz/o;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lz/o;->d:I

    .line 186
    return-void
.end method
