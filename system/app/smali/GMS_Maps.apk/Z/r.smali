.class public Lz/r;
.super Lz/i;
.source "SourceFile"


# static fields
.field private static final m:Lz/L;


# instance fields
.field final g:I

.field final h:I

.field final i:I

.field final j:Lo/T;

.field private k:[F

.field private final l:I

.field private n:F

.field private o:[F

.field private p:Z

.field private q:F

.field private final r:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 34
    new-instance v0, Lz/L;

    const/high16 v1, 0x3f80

    invoke-direct {v0, v2, v2, v1}, Lz/L;-><init>(FFF)V

    sput-object v0, Lz/r;->m:Lz/L;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/E;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lz/r;-><init>(Lcom/google/android/maps/driveabout/vector/E;Z)V

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/E;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Lz/i;-><init>()V

    .line 27
    new-array v0, v2, [F

    iput-object v0, p0, Lz/r;->k:[F

    .line 39
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lz/r;->j:Lo/T;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lz/r;->o:[F

    .line 86
    iput v1, p0, Lz/r;->i:I

    .line 87
    iput v1, p0, Lz/r;->h:I

    .line 88
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v0

    iput v0, p0, Lz/r;->g:I

    .line 89
    new-instance v0, Lz/K;

    invoke-direct {v0}, Lz/K;-><init>()V

    iput-object v0, p0, Lz/r;->a:Lz/K;

    .line 91
    sget-object v0, Lz/s;->a:[I

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/E;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_40

    .line 99
    const/16 v0, 0x10

    iput v0, p0, Lz/r;->l:I

    .line 102
    :goto_35
    iput-boolean p2, p0, Lz/r;->r:Z

    .line 103
    return-void

    .line 93
    :pswitch_38
    iput v2, p0, Lz/r;->l:I

    goto :goto_35

    .line 96
    :pswitch_3b
    const/16 v0, 0x8

    iput v0, p0, Lz/r;->l:I

    goto :goto_35

    .line 91
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_38
        :pswitch_3b
    .end packed-switch
.end method


# virtual methods
.method public a(F)V
    .registers 3
    .parameter

    .prologue
    .line 153
    iget-boolean v0, p0, Lz/r;->f:Z

    if-eqz v0, :cond_7

    .line 154
    invoke-static {}, Lz/k;->c()V

    .line 156
    :cond_7
    iput p1, p0, Lz/r;->q:F

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/r;->b:Z

    .line 158
    return-void
.end method

.method public a(Lo/T;F)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-virtual {p0, p1, p2, p2, p2}, Lz/r;->a(Lo/T;FFF)V

    .line 115
    return-void
.end method

.method public a(Lo/T;FFF)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 119
    iget-boolean v0, p0, Lz/r;->f:Z

    if-eqz v0, :cond_8

    .line 120
    invoke-static {}, Lz/k;->c()V

    .line 122
    :cond_8
    iget-object v0, p0, Lz/r;->j:Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    .line 123
    iget-object v0, p0, Lz/r;->o:[F

    const/4 v1, 0x0

    aput p2, v0, v1

    .line 124
    iget-object v0, p0, Lz/r;->o:[F

    aput p3, v0, v2

    .line 125
    iget-object v0, p0, Lz/r;->o:[F

    const/4 v1, 0x2

    aput p4, v0, v1

    .line 126
    iput-boolean v2, p0, Lz/r;->p:Z

    .line 127
    iput-boolean v2, p0, Lz/r;->b:Z

    .line 128
    return-void
.end method

.method public a(Lz/K;)V
    .registers 3
    .parameter

    .prologue
    .line 234
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lz/M;)V
    .registers 3
    .parameter

    .prologue
    .line 187
    iget v0, p0, Lz/r;->l:I

    invoke-super {p0, p1, v0}, Lz/i;->a(Lz/M;I)V

    .line 188
    return-void
.end method

.method public a(Lz/o;)V
    .registers 3
    .parameter

    .prologue
    .line 168
    iget v0, p0, Lz/r;->l:I

    invoke-super {p0, p1, v0}, Lz/i;->a(Lz/o;I)V

    .line 169
    return-void
.end method

.method public b(Lo/T;F)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 139
    iget-boolean v0, p0, Lz/r;->f:Z

    if-eqz v0, :cond_7

    .line 140
    invoke-static {}, Lz/k;->c()V

    .line 142
    :cond_7
    iget-object v0, p0, Lz/r;->j:Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    .line 143
    iput p2, p0, Lz/r;->n:F

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lz/r;->p:Z

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lz/r;->b:Z

    .line 146
    return-void
.end method
