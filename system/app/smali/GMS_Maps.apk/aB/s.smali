.class public LaB/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentMap;

.field private final b:Ljava/util/Map;

.field private final c:Lcom/google/googlenav/android/aa;

.field private d:I


# direct methods
.method public constructor <init>(ILcom/google/googlenav/android/aa;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaB/s;->b:Ljava/util/Map;

    .line 58
    iput p1, p0, LaB/s;->d:I

    .line 59
    iput-object p2, p0, LaB/s;->c:Lcom/google/googlenav/android/aa;

    .line 60
    invoke-static {}, LY/e;->a()LY/e;

    move-result-object v0

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, LY/e;->a(J)LY/e;

    move-result-object v0

    invoke-virtual {v0}, LY/e;->o()LY/d;

    move-result-object v0

    .line 63
    invoke-interface {v0}, LY/d;->a()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaB/s;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 64
    return-void
.end method

.method static synthetic a(LaB/s;)Ljava/util/concurrent/ConcurrentMap;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LaB/s;->a:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ui/bs;LaB/v;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 170
    invoke-virtual {p0, p1}, LaB/s;->b(Lcom/google/googlenav/ui/bs;)Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/aP;

    new-instance v3, LaB/t;

    invoke-direct {v3, p0, v0, p2}, LaB/t;-><init>(LaB/s;Ljava/lang/String;LaB/v;)V

    invoke-direct {v2, v0, p3, v3}, Lcom/google/googlenav/aP;-><init>(Ljava/lang/String;ZLcom/google/googlenav/aR;)V

    invoke-virtual {v1, v2}, Law/h;->c(Law/g;)V

    .line 210
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/bs;Ljava/util/List;LaB/v;LaB/p;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 132
    invoke-virtual {p0, p1}, LaB/s;->b(Lcom/google/googlenav/ui/bs;)Ljava/lang/String;

    move-result-object v1

    .line 133
    iget-object v0, p0, LaB/s;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 134
    if-eqz v0, :cond_12

    .line 136
    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_11
    :goto_11
    return-void

    .line 137
    :cond_12
    iget-object v0, p0, LaB/s;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_11

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 141
    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v2, p0, LaB/s;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-virtual {p3}, LaB/v;->a()V

    goto :goto_11
.end method

.method private a(Ljava/util/List;LaB/v;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 150
    move v1, v2

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_20

    .line 151
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/bs;

    .line 152
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1e

    const/4 v3, 0x1

    .line 154
    :goto_17
    invoke-direct {p0, v0, p2, v3}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/v;Z)V

    .line 150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_1e
    move v3, v2

    .line 152
    goto :goto_17

    .line 156
    :cond_20
    return-void
.end method

.method static synthetic b(LaB/s;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LaB/s;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(LaB/s;)Lcom/google/googlenav/android/aa;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LaB/s;->c:Lcom/google/googlenav/android/aa;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/bs;)Lam/f;
    .registers 4
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LaB/s;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p0, p1}, LaB/s;->b(Lcom/google/googlenav/ui/bs;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_f

    .line 69
    check-cast v0, Lam/f;

    .line 71
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public a()V
    .registers 2

    .prologue
    .line 223
    iget-object v0, p0, LaB/s;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 224
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/bs;LaB/p;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 165
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    .line 166
    return-void
.end method

.method public a(Ljava/lang/Iterable;LaB/p;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 117
    new-instance v1, LaB/v;

    invoke-direct {v1}, LaB/v;-><init>()V

    .line 118
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 121
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/bs;

    .line 122
    invoke-direct {p0, v0, v2, v1, p2}, LaB/s;->a(Lcom/google/googlenav/ui/bs;Ljava/util/List;LaB/v;LaB/p;)V

    goto :goto_e

    .line 126
    :cond_1e
    invoke-direct {p0, v2, v1}, LaB/s;->a(Ljava/util/List;LaB/v;)V

    .line 127
    return-void
.end method

.method public a(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-static {p1, p2}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {}, Lcom/google/common/base/Predicates;->a()Lcom/google/common/base/K;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/aT;->b(Ljava/lang/Iterable;Lcom/google/common/base/K;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    .line 90
    return-void
.end method

.method public b()I
    .registers 4

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    iget-object v1, p0, LaB/s;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    .line 230
    invoke-interface {v0}, Lam/f;->g()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_c

    .line 232
    :cond_1f
    return v1
.end method

.method protected b(Lcom/google/googlenav/ui/bs;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 214
    invoke-virtual {p1}, Lcom/google/googlenav/ui/bs;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/bs;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/aP;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 105
    invoke-static {p1, p2}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;)Ljava/lang/Iterable;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcom/google/common/collect/aT;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {}, Lcom/google/common/base/Predicates;->a()Lcom/google/common/base/K;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/aT;->b(Ljava/lang/Iterable;Lcom/google/common/base/K;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    .line 107
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 236
    iget v0, p0, LaB/s;->d:I

    return v0
.end method

.method public c(Lcom/google/googlenav/ui/bs;)Z
    .registers 4
    .parameter

    .prologue
    .line 218
    invoke-virtual {p0, p1}, LaB/s;->b(Lcom/google/googlenav/ui/bs;)Ljava/lang/String;

    move-result-object v0

    .line 219
    iget-object v1, p0, LaB/s;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
