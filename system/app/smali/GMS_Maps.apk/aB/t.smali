.class LaB/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/aR;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:LaB/v;

.field final synthetic c:LaB/s;


# direct methods
.method constructor <init>(LaB/s;Ljava/lang/String;LaB/v;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 173
    iput-object p1, p0, LaB/t;->c:LaB/s;

    iput-object p2, p0, LaB/t;->a:Ljava/lang/String;

    iput-object p3, p0, LaB/t;->b:LaB/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;[B)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 176
    if-eqz p2, :cond_57

    .line 177
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, p2

    invoke-interface {v0, p2, v1, v2}, Lam/h;->a([BII)Lam/f;

    move-result-object v0

    .line 179
    iget-object v1, p0, LaB/t;->c:LaB/s;

    invoke-static {v1}, LaB/s;->a(LaB/s;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget-object v2, p0, LaB/t;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    iget-object v0, p0, LaB/t;->c:LaB/s;

    invoke-static {v0}, LaB/s;->b(LaB/s;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, LaB/t;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 187
    iget-object v1, p0, LaB/t;->b:LaB/v;

    if-eqz v1, :cond_35

    iget-object v1, p0, LaB/t;->b:LaB/v;

    invoke-virtual {v1}, LaB/v;->b()Z

    move-result v1

    if-eqz v1, :cond_6b

    .line 188
    :cond_35
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_39
    :goto_39
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/p;

    .line 189
    if-eqz v0, :cond_39

    .line 190
    const/4 v2, 0x1

    .line 191
    iget-object v3, p0, LaB/t;->c:LaB/s;

    invoke-static {v3}, LaB/s;->c(LaB/s;)Lcom/google/googlenav/android/aa;

    move-result-object v3

    new-instance v4, LaB/u;

    invoke-direct {v4, p0, v0}, LaB/u;-><init>(LaB/t;LaB/p;)V

    invoke-virtual {v3, v4, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_39

    .line 202
    :cond_57
    iget-object v0, p0, LaB/t;->c:LaB/s;

    invoke-static {v0}, LaB/s;->b(LaB/s;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, LaB/t;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v0, p0, LaB/t;->b:LaB/v;

    if-eqz v0, :cond_6b

    .line 205
    iget-object v0, p0, LaB/t;->b:LaB/v;

    invoke-virtual {v0}, LaB/v;->b()Z

    .line 208
    :cond_6b
    return-void
.end method
