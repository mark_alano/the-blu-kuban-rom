.class public LaC/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lao/h;Lau/B;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 68
    if-eqz p1, :cond_5

    if-nez p0, :cond_6

    .line 76
    :cond_5
    :goto_5
    return v0

    .line 71
    :cond_6
    invoke-static {p0}, LaC/g;->a(Lao/h;)Lau/B;

    move-result-object v1

    .line 72
    if-eqz v1, :cond_5

    .line 76
    invoke-static {v1, p1}, Lcom/google/googlenav/ui/o;->a(Lau/B;Lau/B;)I

    move-result v0

    goto :goto_5
.end method

.method private static a(Lao/h;)Lau/B;
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-interface {p0}, Lao/h;->g()Z

    move-result v1

    if-nez v1, :cond_8

    .line 57
    :cond_7
    :goto_7
    return-object v0

    .line 53
    :cond_8
    invoke-interface {p0}, Lao/h;->m()Lao/s;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_7

    .line 55
    invoke-virtual {v1}, Lao/s;->a()Lau/B;

    move-result-object v0

    goto :goto_7
.end method

.method public static a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;Lau/B;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 109
    .line 111
    if-eqz p2, :cond_25

    .line 112
    invoke-static {}, Lao/l;->p()Lao/h;

    move-result-object v1

    .line 113
    invoke-static {}, LaC/h;->i()LaC/h;

    move-result-object v0

    .line 116
    :goto_b
    if-eqz p0, :cond_16

    .line 117
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/DistanceView;->setLocationProvider(Lao/h;)V

    .line 118
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/android/DistanceView;->setInitialVisibility(Lau/B;)V

    .line 119
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/view/android/DistanceView;->setDestination(Lau/B;)V

    .line 122
    :cond_16
    if-eqz p1, :cond_24

    .line 123
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/view/android/HeadingView;->setOrientationProvider(LaC/h;)V

    .line 124
    invoke-virtual {p1, v1}, Lcom/google/googlenav/ui/view/android/HeadingView;->setLocationProvider(Lao/h;)V

    .line 125
    invoke-virtual {p1, p2}, Lcom/google/googlenav/ui/view/android/HeadingView;->setInitialVisibility(Lau/B;)V

    .line 126
    invoke-virtual {p1, p2}, Lcom/google/googlenav/ui/view/android/HeadingView;->setDestination(Lau/B;)V

    .line 128
    :cond_24
    return-void

    :cond_25
    move-object v1, v0

    goto :goto_b
.end method
