.class public final enum LaC/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaC/j;

.field public static final enum b:LaC/j;

.field public static final enum c:LaC/j;

.field private static final synthetic e:[LaC/j;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    new-instance v0, LaC/j;

    const-string v1, "UPDATE_FREQUENCY_NONE"

    invoke-direct {v0, v1, v3, v3}, LaC/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaC/j;->a:LaC/j;

    .line 47
    new-instance v0, LaC/j;

    const-string v1, "UPDATE_FREQUENCY_SLOW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v4, v2}, LaC/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaC/j;->b:LaC/j;

    .line 50
    new-instance v0, LaC/j;

    const-string v1, "UPDATE_FREQUENCY_FAST"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v5, v2}, LaC/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaC/j;->c:LaC/j;

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [LaC/j;

    sget-object v1, LaC/j;->a:LaC/j;

    aput-object v1, v0, v3

    sget-object v1, LaC/j;->b:LaC/j;

    aput-object v1, v0, v4

    sget-object v1, LaC/j;->c:LaC/j;

    aput-object v1, v0, v5

    sput-object v0, LaC/j;->e:[LaC/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    if-nez p3, :cond_9

    const/4 v0, 0x0

    :goto_6
    iput v0, p0, LaC/j;->d:I

    .line 57
    return-void

    .line 56
    :cond_9
    const v0, 0xf4240

    div-int/2addr v0, p3

    goto :goto_6
.end method

.method public static valueOf(Ljava/lang/String;)LaC/j;
    .registers 2
    .parameter

    .prologue
    .line 39
    const-class v0, LaC/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaC/j;

    return-object v0
.end method

.method public static values()[LaC/j;
    .registers 1

    .prologue
    .line 39
    sget-object v0, LaC/j;->e:[LaC/j;

    invoke-virtual {v0}, [LaC/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaC/j;

    return-object v0
.end method
