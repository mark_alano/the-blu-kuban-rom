.class public LaG/a;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:LaG/h;

.field private final b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private d:Lcom/google/googlenav/a;

.field private e:LaG/n;

.field private f:LaG/c;


# direct methods
.method public constructor <init>(LaG/h;)V
    .registers 5
    .parameter

    .prologue
    .line 101
    invoke-direct {p0}, Law/a;-><init>()V

    .line 102
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ed;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 104
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    sget v2, Lcom/google/googlenav/ui/bi;->bF:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 107
    iput-object p1, p0, LaG/a;->a:LaG/h;

    .line 108
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 418
    sget-object v0, LaG/b;->a:[I

    invoke-virtual {p2}, LaG/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_20

    .line 430
    const/4 v0, 0x0

    :goto_c
    return v0

    .line 420
    :pswitch_d
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    goto :goto_c

    .line 423
    :pswitch_13
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    goto :goto_c

    .line 426
    :pswitch_19
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    goto :goto_c

    .line 418
    nop

    :pswitch_data_20
    .packed-switch 0x1
        :pswitch_d
        :pswitch_13
        :pswitch_19
    .end packed-switch
.end method

.method private a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;I)Ljava/util/List;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 343
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 344
    const/4 v0, 0x0

    :goto_5
    array-length v2, p1

    if-ge v0, v2, :cond_41

    .line 345
    aget-object v2, p1, v0

    .line 347
    const/4 v3, 0x2

    const/4 v4, -0x1

    invoke-static {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v3

    .line 349
    if-gez v3, :cond_15

    .line 344
    :cond_12
    :goto_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 354
    :cond_15
    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v4

    .line 356
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_12

    .line 362
    iget-object v6, p0, LaG/a;->a:LaG/h;

    invoke-virtual {v6, v4, v5}, LaG/h;->a(J)LaG/m;

    move-result-object v4

    .line 363
    if-eqz v4, :cond_12

    invoke-virtual {v4}, LaG/m;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 368
    invoke-direct {p0, v2, p2}, LaG/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;)I

    move-result v2

    .line 370
    new-instance v5, LaG/f;

    add-int v6, p3, v0

    invoke-direct {v5, v6, v3, v2, v4}, LaG/f;-><init>(IIILaG/m;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 372
    :cond_41
    return-object v1
.end method

.method private a(Lcom/google/googlenav/a;Z)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 131
    invoke-virtual {p1}, Lcom/google/googlenav/a;->c()Z

    move-result v0

    if-nez v0, :cond_a

    .line 170
    :cond_9
    :goto_9
    return-void

    .line 135
    :cond_a
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ed;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 138
    invoke-virtual {p1}, Lcom/google/googlenav/a;->b()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 139
    invoke-virtual {p1}, Lcom/google/googlenav/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 142
    invoke-virtual {p1}, Lcom/google/googlenav/a;->g()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 144
    :try_start_28
    invoke-virtual {p1}, Lcom/google/googlenav/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/az;->a(Ljava/lang/String;)J

    move-result-wide v1

    .line 145
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_34
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_34} :catch_65

    .line 151
    :cond_34
    :goto_34
    invoke-virtual {p1}, Lcom/google/googlenav/a;->f()Z

    move-result v1

    if-eqz v1, :cond_58

    .line 152
    invoke-virtual {p1}, Lcom/google/googlenav/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 154
    invoke-static {v1, v7}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 155
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4c

    .line 156
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 159
    :cond_4c
    const/16 v2, 0x18

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_58

    .line 160
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 164
    :cond_58
    iget-object v1, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 166
    if-eqz p2, :cond_9

    .line 167
    iget-object v1, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_9

    .line 146
    :catch_65
    move-exception v1

    goto :goto_34
.end method

.method private k()I
    .registers 4

    .prologue
    .line 298
    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_d

    .line 299
    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    .line 303
    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method private l()V
    .registers 9

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v5, 0x0

    .line 309
    invoke-direct {p0}, LaG/a;->m()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 310
    iget-object v1, p0, LaG/a;->a:LaG/h;

    invoke-virtual {v1, v0}, LaG/h;->a(Ljava/util/List;)V

    .line 313
    iget-object v1, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 315
    iget-object v1, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 318
    invoke-direct {p0}, LaG/a;->n()LaG/g;

    move-result-object v1

    .line 319
    invoke-static {v1}, LaG/d;->a(LaG/g;)LaG/d;

    move-result-object v4

    .line 320
    invoke-direct {p0, v2, v1, v5}, LaG/a;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v4, v2}, LaG/d;->a(Ljava/util/List;)V

    .line 322
    invoke-static {v1}, LaG/d;->a(LaG/g;)LaG/d;

    move-result-object v2

    .line 323
    invoke-direct {p0, v3, v1, v5}, LaG/a;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaG/g;I)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, LaG/d;->a(Ljava/util/List;)V

    .line 326
    iget-object v3, p0, LaG/a;->e:LaG/n;

    invoke-direct {p0}, LaG/a;->n()LaG/g;

    move-result-object v5

    invoke-virtual {v3, v5}, LaG/n;->a(LaG/g;)V

    .line 327
    iget-object v3, p0, LaG/a;->e:LaG/n;

    invoke-virtual {v3, v4}, LaG/n;->a(LaG/d;)V

    .line 328
    iget-object v3, p0, LaG/a;->e:LaG/n;

    invoke-virtual {v3, v2}, LaG/n;->b(LaG/d;)V

    .line 329
    iget-object v2, p0, LaG/a;->e:LaG/n;

    invoke-virtual {v2, v0}, LaG/n;->a(Lcom/google/common/collect/ImmutableList;)V

    .line 332
    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    .line 334
    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v4, 0x7

    invoke-static {v0, v4}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v4

    .line 336
    iget-object v0, p0, LaG/a;->e:LaG/n;

    div-long/2addr v2, v6

    div-long/2addr v4, v6

    invoke-virtual/range {v0 .. v5}, LaG/n;->a(LaG/g;JJ)V

    .line 338
    return-void
.end method

.method private m()Lcom/google/common/collect/ImmutableList;
    .registers 15

    .prologue
    .line 376
    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 378
    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v3

    .line 380
    iget-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v5

    .line 382
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-nez v0, :cond_1c

    .line 386
    :cond_1c
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/aw;

    move-result-object v7

    .line 387
    array-length v8, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_23
    if-ge v1, v8, :cond_45

    aget-object v9, v2, v1

    .line 388
    const/4 v0, 0x1

    invoke-static {v9, v0}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v10

    .line 390
    const-wide/16 v12, 0x0

    cmp-long v0, v5, v12

    if-eqz v0, :cond_43

    cmp-long v0, v5, v10

    if-nez v0, :cond_43

    const/4 v0, 0x1

    .line 391
    :goto_37
    new-instance v10, LaG/m;

    invoke-direct {v10, v9, v0, v3, v4}, LaG/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V

    .line 392
    invoke-virtual {v7, v10}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    .line 387
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_23

    .line 390
    :cond_43
    const/4 v0, 0x0

    goto :goto_37

    .line 394
    :cond_45
    invoke-virtual {v7}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private n()LaG/g;
    .registers 3

    .prologue
    .line 401
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_18

    .line 410
    sget-object v0, LaG/g;->b:LaG/g;

    :goto_d
    return-object v0

    .line 403
    :pswitch_e
    sget-object v0, LaG/g;->a:LaG/g;

    goto :goto_d

    .line 405
    :pswitch_11
    sget-object v0, LaG/g;->b:LaG/g;

    goto :goto_d

    .line 407
    :pswitch_14
    sget-object v0, LaG/g;->c:LaG/g;

    goto :goto_d

    .line 401
    nop

    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_11
        :pswitch_e
        :pswitch_14
    .end packed-switch
.end method


# virtual methods
.method public a(I)LaG/a;
    .registers 5
    .parameter

    .prologue
    .line 201
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 203
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 204
    return-object p0
.end method

.method public a(II)LaG/a;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 218
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 219
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 220
    return-object p0
.end method

.method public a(LaG/c;)LaG/a;
    .registers 2
    .parameter

    .prologue
    .line 263
    iput-object p1, p0, LaG/a;->f:LaG/c;

    .line 264
    return-object p0
.end method

.method public a(LaG/g;)LaG/a;
    .registers 5
    .parameter

    .prologue
    .line 244
    sget-object v0, LaG/b;->a:[I

    invoke-virtual {p1}, LaG/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1a

    .line 259
    :goto_b
    return-object p0

    .line 246
    :pswitch_c
    const/4 v0, 0x1

    .line 258
    :goto_d
    iget-object v1, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_b

    .line 249
    :pswitch_15
    const/4 v0, 0x0

    .line 250
    goto :goto_d

    .line 252
    :pswitch_17
    const/4 v0, 0x2

    .line 253
    goto :goto_d

    .line 244
    nop

    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_c
        :pswitch_15
        :pswitch_17
    .end packed-switch
.end method

.method public a(Ljava/util/List;)LaG/a;
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v0, 0x0

    :goto_8
    iput-object v0, p0, LaG/a;->d:Lcom/google/googlenav/a;

    .line 116
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v3, v2

    .line 117
    :goto_15
    if-ge v3, v4, :cond_31

    .line 119
    if-nez v3, :cond_2f

    const/4 v0, 0x1

    move v1, v0

    .line 120
    :goto_1b
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/a;

    invoke-direct {p0, v0, v1}, LaG/a;->a(Lcom/google/googlenav/a;Z)V

    .line 117
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_15

    .line 115
    :cond_28
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/a;

    goto :goto_8

    :cond_2f
    move v1, v2

    .line 119
    goto :goto_1b

    .line 122
    :cond_31
    return-object p0
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 274
    iget-object v0, p0, LaG/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 275
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 4
    .parameter

    .prologue
    .line 279
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ed;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, LaG/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 282
    invoke-direct {p0}, LaG/a;->k()I

    move-result v0

    .line 283
    if-nez v0, :cond_27

    .line 284
    new-instance v0, LaG/n;

    iget-object v1, p0, LaG/a;->d:Lcom/google/googlenav/a;

    invoke-direct {v0, v1}, LaG/n;-><init>(Lcom/google/googlenav/a;)V

    iput-object v0, p0, LaG/a;->e:LaG/n;

    .line 285
    invoke-direct {p0}, LaG/a;->l()V

    .line 286
    iget-object v0, p0, LaG/a;->f:LaG/c;

    if-eqz v0, :cond_25

    .line 287
    iget-object v0, p0, LaG/a;->f:LaG/c;

    iget-object v1, p0, LaG/a;->e:LaG/n;

    invoke-interface {v0, v1}, LaG/c;->a(LaG/n;)V

    .line 294
    :cond_25
    :goto_25
    const/4 v0, 0x1

    return v0

    .line 290
    :cond_27
    new-instance v0, LaG/n;

    iget-object v1, p0, LaG/a;->d:Lcom/google/googlenav/a;

    invoke-direct {v0, v1}, LaG/n;-><init>(Lcom/google/googlenav/a;)V

    iput-object v0, p0, LaG/a;->e:LaG/n;

    goto :goto_25
.end method

.method public b()I
    .registers 2

    .prologue
    .line 269
    const/16 v0, 0x7c

    return v0
.end method

.method public d_()V
    .registers 3

    .prologue
    .line 438
    .line 439
    monitor-enter p0

    .line 440
    :try_start_1
    iget-object v0, p0, LaG/a;->f:LaG/c;

    .line 441
    const/4 v1, 0x0

    iput-object v1, p0, LaG/a;->f:LaG/c;

    .line 442
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_15

    .line 443
    if-eqz v0, :cond_14

    .line 444
    invoke-direct {p0}, LaG/a;->k()I

    move-result v1

    if-nez v1, :cond_18

    .line 445
    iget-object v1, p0, LaG/a;->e:LaG/n;

    invoke-interface {v0, v1}, LaG/c;->b(LaG/n;)V

    .line 450
    :cond_14
    :goto_14
    return-void

    .line 442
    :catchall_15
    move-exception v0

    :try_start_16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    throw v0

    .line 447
    :cond_18
    invoke-interface {v0}, LaG/c;->a()V

    goto :goto_14
.end method

.method public u_()V
    .registers 3

    .prologue
    .line 456
    .line 457
    monitor-enter p0

    .line 458
    :try_start_1
    iget-object v0, p0, LaG/a;->f:LaG/c;

    .line 459
    const/4 v1, 0x0

    iput-object v1, p0, LaG/a;->f:LaG/c;

    .line 460
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_d

    .line 461
    if-eqz v0, :cond_c

    .line 462
    invoke-interface {v0}, LaG/c;->a()V

    .line 464
    :cond_c
    return-void

    .line 460
    :catchall_d
    move-exception v0

    :try_start_e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_e .. :try_end_f} :catchall_d

    throw v0
.end method
