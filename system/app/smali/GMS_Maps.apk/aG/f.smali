.class public LaG/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:LaG/m;


# direct methods
.method constructor <init>(IIILaG/m;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput p1, p0, LaG/f;->a:I

    .line 152
    iput p2, p0, LaG/f;->b:I

    .line 153
    iput p3, p0, LaG/f;->c:I

    .line 154
    iput-object p4, p0, LaG/f;->d:LaG/m;

    .line 155
    return-void
.end method

.method static synthetic b(LaG/f;)I
    .registers 2
    .parameter

    .prologue
    .line 126
    iget v0, p0, LaG/f;->a:I

    return v0
.end method


# virtual methods
.method public a(LaG/f;)I
    .registers 4
    .parameter

    .prologue
    .line 203
    iget v0, p0, LaG/f;->a:I

    iget v1, p1, LaG/f;->a:I

    invoke-static {v0, v1}, Lac/a;->a(II)I

    move-result v0

    return v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 162
    iget v0, p0, LaG/f;->a:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 166
    iget v0, p0, LaG/f;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 170
    iget v0, p0, LaG/f;->c:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 126
    check-cast p1, LaG/f;

    invoke-virtual {p0, p1}, LaG/f;->a(LaG/f;)I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 174
    iget v0, p0, LaG/f;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 178
    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Lam/f;
    .registers 2

    .prologue
    .line 186
    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->f()Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public h()LaG/m;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, LaG/f;->d:LaG/m;

    return-object v0
.end method

.method public i()J
    .registers 3

    .prologue
    .line 194
    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, LaG/f;->d:LaG/m;

    invoke-virtual {v0}, LaG/m;->g()Z

    move-result v0

    return v0
.end method
