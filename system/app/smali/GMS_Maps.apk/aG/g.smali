.class public final enum LaG/g;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaG/g;

.field public static final enum b:LaG/g;

.field public static final enum c:LaG/g;

.field private static final synthetic d:[LaG/g;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, LaG/g;

    const-string v1, "LAST_WEEK"

    invoke-direct {v0, v1, v2}, LaG/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaG/g;->a:LaG/g;

    .line 32
    new-instance v0, LaG/g;

    const-string v1, "THIS_WEEK"

    invoke-direct {v0, v1, v3}, LaG/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaG/g;->b:LaG/g;

    .line 33
    new-instance v0, LaG/g;

    const-string v1, "ALL_TIME"

    invoke-direct {v0, v1, v4}, LaG/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaG/g;->c:LaG/g;

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [LaG/g;

    sget-object v1, LaG/g;->a:LaG/g;

    aput-object v1, v0, v2

    sget-object v1, LaG/g;->b:LaG/g;

    aput-object v1, v0, v3

    sget-object v1, LaG/g;->c:LaG/g;

    aput-object v1, v0, v4

    sput-object v0, LaG/g;->d:[LaG/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaG/g;
    .registers 2
    .parameter

    .prologue
    .line 30
    const-class v0, LaG/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaG/g;

    return-object v0
.end method

.method public static values()[LaG/g;
    .registers 1

    .prologue
    .line 30
    sget-object v0, LaG/g;->d:[LaG/g;

    invoke-virtual {v0}, [LaG/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaG/g;

    return-object v0
.end method
