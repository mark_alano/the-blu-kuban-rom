.class public final LaG/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Z

.field private final c:J

.field private d:Lam/f;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ZJ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 53
    iput-boolean p2, p0, LaG/m;->b:Z

    .line 54
    iput-wide p3, p0, LaG/m;->c:J

    .line 55
    return-void
.end method

.method private static a(JLjava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 120
    const-wide/32 v0, 0x493e0

    cmp-long v0, p0, v0

    if-gez v0, :cond_1d

    .line 121
    const/16 v0, 0x210

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 125
    :goto_e
    const-string v1, "%s - %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 123
    :cond_1d
    invoke-static {p0, p1, v3, v3}, Lcom/google/googlenav/b;->a(JZZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_e
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 65
    iget-object v0, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized a(Lam/f;)V
    .registers 3
    .parameter

    .prologue
    .line 137
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaG/m;->d:Lam/f;

    if-nez v0, :cond_7

    .line 138
    iput-object p1, p0, LaG/m;->d:Lam/f;
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 140
    :cond_7
    monitor-exit p0

    return-void

    .line 137
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 73
    iget-object v0, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 81
    iget-object v0, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 9

    .prologue
    const-wide/16 v6, 0x0

    .line 89
    const-string v0, ""

    .line 90
    iget-object v1, p0, LaG/m;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 92
    if-eqz v1, :cond_3d

    .line 93
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    .line 95
    const/4 v4, 0x4

    invoke-static {v1, v4}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    .line 97
    iget-wide v4, p0, LaG/m;->c:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3d

    cmp-long v4, v2, v6

    if-lez v4, :cond_3d

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3d

    .line 101
    iget-wide v4, p0, LaG/m;->c:J

    sub-long v2, v4, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 104
    cmp-long v4, v2, v6

    if-ltz v4, :cond_3d

    const-wide/32 v4, 0x240c8400

    cmp-long v4, v2, v4

    if-gez v4, :cond_3d

    .line 105
    invoke-static {v2, v3, v1}, LaG/m;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    :cond_3d
    return-object v0
.end method

.method public declared-synchronized e()Z
    .registers 2

    .prologue
    .line 129
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaG/m;->d:Lam/f;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 151
    instance-of v1, p1, LaG/m;

    if-eqz v1, :cond_14

    .line 152
    invoke-virtual {p0}, LaG/m;->a()J

    move-result-wide v1

    check-cast p1, LaG/m;

    invoke-virtual {p1}, LaG/m;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_14

    const/4 v0, 0x1

    .line 154
    :cond_14
    return v0
.end method

.method public declared-synchronized f()Lam/f;
    .registers 2

    .prologue
    .line 133
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaG/m;->d:Lam/f;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaG/m;->d:Lam/f;
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_c

    :goto_7
    monitor-exit p0

    return-object v0

    :cond_9
    :try_start_9
    sget-object v0, LaG/h;->a:Lam/f;
    :try_end_b
    .catchall {:try_start_9 .. :try_end_b} :catchall_c

    goto :goto_7

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 146
    iget-boolean v0, p0, LaG/m;->b:Z

    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 159
    invoke-virtual {p0}, LaG/m;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lac/b;->a(J)I

    move-result v0

    return v0
.end method
