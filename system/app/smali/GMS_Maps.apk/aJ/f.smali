.class public abstract LaJ/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaJ/r;


# instance fields
.field private a:I

.field private b:LZ/d;

.field private volatile c:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v1, p0, LaJ/f;->a:I

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, LaJ/f;->b:LZ/d;

    .line 38
    iput-boolean v1, p0, LaJ/f;->c:Z

    return-void
.end method

.method public static a(II)Lau/B;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-static {p0, p1}, LaJ/f;->b(II)Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lau/B;

    invoke-direct {v0, p0, p1}, Lau/B;-><init>(II)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static b(II)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    const v0, 0xbebc200

    .line 184
    if-eq p0, v0, :cond_9

    if-eq p1, v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method protected static f(LaJ/s;)Z
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 196
    invoke-static {}, Lat/f;->j()Lat/f;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-static {}, Lat/f;->j()Lat/f;

    move-result-object v1

    invoke-virtual {v1}, Lat/f;->k()Z

    move-result v1

    if-nez v1, :cond_12

    .line 215
    :cond_11
    :goto_11
    return v0

    .line 209
    :cond_12
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 215
    const/4 v0, 0x0

    goto :goto_11
.end method


# virtual methods
.method public K_()Z
    .registers 2

    .prologue
    .line 160
    iget-boolean v0, p0, LaJ/f;->c:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method protected final declared-synchronized a(LaJ/z;)V
    .registers 5
    .parameter

    .prologue
    .line 94
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, LaJ/f;->i()Z

    .line 96
    new-instance v0, LaJ/g;

    invoke-static {}, Lcom/google/googlenav/bL;->a()LZ/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, LaJ/g;-><init>(LaJ/f;LZ/c;LaJ/z;)V

    iput-object v0, p0, LaJ/f;->b:LZ/d;

    .line 112
    iget-object v0, p0, LaJ/f;->b:LZ/d;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, v1, v2}, LZ/d;->a(J)V

    .line 113
    iget-object v0, p0, LaJ/f;->b:LZ/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LZ/d;->b(I)V

    .line 114
    iget-object v0, p0, LaJ/f;->b:LZ/d;

    invoke-virtual {v0}, LZ/d;->g()V
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    .line 115
    monitor-exit p0

    return-void

    .line 94
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(LaJ/z;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-static {}, LaJ/o;->a()LaJ/o;

    move-result-object v0

    .line 74
    invoke-virtual {p0}, LaJ/f;->c()I

    move-result v1

    invoke-virtual {v0, v1}, LaJ/o;->a(I)V

    .line 81
    invoke-virtual {p1}, LaJ/z;->b()LaJ/s;

    move-result-object v1

    invoke-virtual {v1}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LaJ/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lab/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 82
    invoke-virtual {p0}, LaJ/f;->c()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, LaJ/o;->a(LaJ/z;ZI)V

    .line 84
    :cond_24
    return-void
.end method

.method protected abstract a_(LaJ/s;)V
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 135
    const-string v0, "o"

    return-object v0
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 165
    iput p1, p0, LaJ/f;->a:I

    .line 166
    return-void
.end method

.method public b(LaJ/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method public c(LaJ/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method public d()[I
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 145
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    return-object v0
.end method

.method public final e(LaJ/s;)V
    .registers 3
    .parameter

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, LaJ/f;->c:Z

    .line 56
    invoke-virtual {p0, p1}, LaJ/f;->a_(LaJ/s;)V

    .line 57
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, LaJ/f;->c:Z

    .line 62
    return-void
.end method

.method public declared-synchronized i()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 122
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, LaJ/f;->b:LZ/d;

    if-eqz v1, :cond_12

    .line 123
    iget-object v1, p0, LaJ/f;->b:LZ/d;

    invoke-virtual {v1}, LZ/d;->c()I

    move-result v1

    if-eqz v1, :cond_f

    const/4 v0, 0x1

    .line 124
    :cond_f
    const/4 v1, 0x0

    iput-object v1, p0, LaJ/f;->b:LZ/d;
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_14

    .line 127
    :cond_12
    monitor-exit p0

    return v0

    .line 122
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()I
    .registers 2

    .prologue
    .line 170
    iget v0, p0, LaJ/f;->a:I

    return v0
.end method
