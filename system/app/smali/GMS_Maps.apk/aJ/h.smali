.class public LaJ/h;
.super LaJ/c;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:LaJ/w;

.field private final c:Lay/m;


# direct methods
.method public constructor <init>(Lay/m;LaJ/w;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LaJ/c;-><init>()V

    .line 36
    invoke-static {p2}, Lcom/google/common/base/P;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iput-object p1, p0, LaJ/h;->c:Lay/m;

    .line 38
    iput-object p2, p0, LaJ/h;->b:LaJ/w;

    .line 39
    iput p3, p0, LaJ/h;->a:I

    .line 40
    return-void
.end method

.method private static final a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 107
    const/16 v0, 0xb

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 102
    const-string v0, "m"

    return-object v0
.end method

.method public b(LaJ/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 92
    invoke-static {p1}, LaJ/h;->f(LaJ/s;)Z

    move-result v0

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 97
    iget v0, p0, LaJ/h;->a:I

    return v0
.end method

.method protected d(LaJ/s;)LaJ/z;
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x6

    .line 50
    new-instance v1, LaJ/z;

    invoke-direct {v1, p1}, LaJ/z;-><init>(LaJ/s;)V

    .line 52
    invoke-static {}, LaJ/o;->a()LaJ/o;

    .line 54
    iget-object v0, p0, LaJ/h;->c:Lay/m;

    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v2

    .line 55
    invoke-interface {v2}, Lay/t;->a()Ljava/util/List;

    move-result-object v0

    .line 56
    invoke-virtual {p1}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LaJ/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 57
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1f
    :goto_1f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/C;

    .line 58
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Lay/C;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1f

    .line 62
    invoke-virtual {v0}, Lay/C;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LaJ/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 63
    invoke-virtual {v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1f

    .line 67
    invoke-static {v2, v0}, LaJ/i;->b(Lay/t;Lay/C;)Ljava/lang/String;

    move-result-object v5

    .line 68
    invoke-static {v2, v0}, LaJ/i;->a(Lay/t;Lay/C;)Ljava/lang/String;

    move-result-object v6

    .line 69
    new-instance v7, LaJ/y;

    invoke-direct {v7}, LaJ/y;-><init>()V

    invoke-virtual {v7, v5}, LaJ/y;->a(Ljava/lang/String;)LaJ/y;

    move-result-object v5

    invoke-virtual {v0}, Lay/C;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LaJ/y;->b(Ljava/lang/String;)LaJ/y;

    move-result-object v5

    invoke-virtual {v5, v6}, LaJ/y;->c(Ljava/lang/String;)LaJ/y;

    move-result-object v5

    invoke-virtual {v5, v8}, LaJ/y;->a(I)LaJ/y;

    move-result-object v5

    iget v6, p0, LaJ/h;->a:I

    invoke-virtual {v5, v6}, LaJ/y;->b(I)LaJ/y;

    move-result-object v5

    invoke-virtual {p0}, LaJ/h;->j()I

    move-result v6

    invoke-virtual {v5, v6}, LaJ/y;->c(I)LaJ/y;

    move-result-object v5

    invoke-virtual {v0}, Lay/C;->i()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, LaJ/y;->a(J)LaJ/y;

    move-result-object v5

    invoke-virtual {v0}, Lay/C;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LaJ/y;->d(Ljava/lang/String;)LaJ/y;

    move-result-object v5

    invoke-virtual {v0}, Lay/C;->d()Lau/B;

    move-result-object v0

    invoke-virtual {v5, v0}, LaJ/y;->a(Lau/B;)LaJ/y;

    move-result-object v0

    invoke-virtual {p0, v8}, LaJ/h;->a(I)I

    move-result v5

    invoke-virtual {v0, v5}, LaJ/y;->d(I)LaJ/y;

    move-result-object v0

    const-string v5, "google.recentplace:"

    invoke-virtual {v0, v5}, LaJ/y;->e(Ljava/lang/String;)LaJ/y;

    move-result-object v0

    invoke-virtual {v0}, LaJ/y;->a()LaJ/w;

    move-result-object v0

    invoke-virtual {v1, v0}, LaJ/z;->b(LaJ/w;)V

    goto :goto_1f

    .line 84
    :cond_9f
    invoke-virtual {v1}, LaJ/z;->d()I

    move-result v0

    if-lez v0, :cond_b0

    invoke-virtual {p1}, LaJ/s;->h()Z

    move-result v0

    if-eqz v0, :cond_b0

    .line 85
    iget-object v0, p0, LaJ/h;->b:LaJ/w;

    invoke-virtual {v1, v0}, LaJ/z;->a(LaJ/w;)V

    .line 87
    :cond_b0
    return-object v1
.end method

.method public d()[I
    .registers 4

    .prologue
    .line 112
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xb

    aput v2, v0, v1

    return-object v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method
