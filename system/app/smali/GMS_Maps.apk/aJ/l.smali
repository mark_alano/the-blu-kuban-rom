.class LaJ/l;
.super Lad/a;
.source "SourceFile"


# instance fields
.field final synthetic a:LaJ/j;

.field private final b:LaU/i;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final d:Lau/B;

.field private final e:LaJ/s;

.field private final f:I

.field private g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(LaJ/j;LaJ/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lau/B;I)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 252
    iput-object p1, p0, LaJ/l;->a:LaJ/j;

    invoke-direct {p0}, Lad/a;-><init>()V

    .line 253
    iput-object p2, p0, LaJ/l;->e:LaJ/s;

    .line 254
    iput-object p3, p0, LaJ/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 255
    iput-object p4, p0, LaJ/l;->d:Lau/B;

    .line 256
    iput p5, p0, LaJ/l;->f:I

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "remoteSuggest ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 258
    new-instance v1, LaU/i;

    const-string v2, "rsd"

    const/16 v3, 0x16

    invoke-direct {v1, v0, v2, v3}, LaU/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, LaJ/l;->b:LaU/i;

    .line 261
    iget-object v0, p0, LaJ/l;->b:LaU/i;

    invoke-virtual {v0}, LaU/i;->a()V

    .line 262
    return-void
.end method

.method private a(I)I
    .registers 3
    .parameter

    .prologue
    .line 313
    iget-object v0, p0, LaJ/l;->a:LaJ/j;

    invoke-static {v0}, LaJ/j;->e(LaJ/j;)I

    move-result v0

    return v0
.end method

.method private k()LaJ/z;
    .registers 15

    .prologue
    const/4 v13, 0x7

    const/4 v12, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v11, 0x2

    .line 323
    new-instance v5, LaJ/z;

    iget-object v0, p0, LaJ/l;->e:LaJ/s;

    invoke-direct {v5, v0}, LaJ/z;-><init>(LaJ/s;)V

    .line 325
    iget-object v0, p0, LaJ/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-eqz v0, :cond_c6

    .line 326
    iget-object v0, p0, LaJ/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v11, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    move v0, v1

    move v2, v1

    .line 327
    :goto_1c
    invoke-virtual {v6, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-ge v0, v3, :cond_ab

    .line 328
    invoke-virtual {v6, v11, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 329
    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v8

    .line 330
    new-instance v3, LaJ/y;

    invoke-direct {v3}, LaJ/y;-><init>()V

    invoke-virtual {v7, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LaJ/y;->a(Ljava/lang/String;)LaJ/y;

    move-result-object v3

    invoke-virtual {v3, v8}, LaJ/y;->a(I)LaJ/y;

    move-result-object v3

    invoke-virtual {v3, v4}, LaJ/y;->b(I)LaJ/y;

    move-result-object v3

    invoke-direct {p0, v8}, LaJ/l;->a(I)I

    move-result v9

    invoke-virtual {v3, v9}, LaJ/y;->c(I)LaJ/y;

    move-result-object v3

    iget-object v9, p0, LaJ/l;->a:LaJ/j;

    invoke-virtual {v9, v8}, LaJ/j;->a(I)I

    move-result v9

    invoke-virtual {v3, v9}, LaJ/y;->d(I)LaJ/y;

    move-result-object v3

    invoke-virtual {v3, v7}, LaJ/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaJ/y;

    move-result-object v9

    .line 337
    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_66

    .line 338
    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 339
    invoke-virtual {v9, v3}, LaJ/y;->b(Ljava/lang/String;)LaJ/y;

    move-result-object v10

    invoke-virtual {v10, v3}, LaJ/y;->d(Ljava/lang/String;)LaJ/y;

    .line 341
    :cond_66
    const/16 v3, 0xf

    invoke-virtual {v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_77

    .line 342
    const/16 v3, 0xf

    invoke-virtual {v7, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, LaJ/y;->c(Ljava/lang/String;)LaJ/y;

    .line 344
    :cond_77
    invoke-virtual {v7, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v3

    if-eqz v3, :cond_88

    .line 345
    invoke-virtual {v7, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 346
    invoke-static {v3}, Lau/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v3

    invoke-virtual {v9, v3}, LaJ/y;->a(Lau/B;)LaJ/y;

    :cond_88
    move v3, v1

    .line 348
    :goto_89
    const/16 v10, 0xb

    invoke-virtual {v7, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v10

    if-ge v3, v10, :cond_9d

    .line 349
    const/16 v10, 0xb

    invoke-virtual {v7, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v10

    invoke-virtual {v9, v10}, LaJ/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaJ/y;

    .line 348
    add-int/lit8 v3, v3, 0x1

    goto :goto_89

    .line 351
    :cond_9d
    invoke-virtual {v9}, LaJ/y;->a()LaJ/w;

    move-result-object v3

    invoke-virtual {v5, v3}, LaJ/z;->a(LaJ/w;)V

    .line 353
    if-ne v8, v12, :cond_a7

    move v2, v4

    .line 327
    :cond_a7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1c

    .line 359
    :cond_ab
    if-eqz v2, :cond_c6

    .line 360
    iget-object v0, p0, LaJ/l;->a:LaJ/j;

    invoke-static {v0}, LaJ/j;->f(LaJ/j;)LaJ/w;

    move-result-object v0

    if-eqz v0, :cond_c6

    iget-object v0, p0, LaJ/l;->e:LaJ/s;

    invoke-virtual {v0}, LaJ/s;->h()Z

    move-result v0

    if-eqz v0, :cond_c6

    .line 361
    iget-object v0, p0, LaJ/l;->a:LaJ/j;

    invoke-static {v0}, LaJ/j;->f(LaJ/j;)LaJ/w;

    move-result-object v0

    invoke-virtual {v5, v0}, LaJ/z;->a(LaJ/w;)V

    .line 365
    :cond_c6
    return-object v5
.end method

.method private l()V
    .registers 2

    .prologue
    .line 375
    monitor-enter p0

    .line 376
    :goto_1
    :try_start_1
    iget-object v0, p0, LaJ/l;->a:LaJ/j;

    invoke-static {v0}, LaJ/j;->b(LaJ/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_24

    .line 377
    iget-object v0, p0, LaJ/l;->a:LaJ/j;

    invoke-static {v0}, LaJ/j;->b(LaJ/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJ/l;

    .line 378
    if-ne v0, p0, :cond_1d

    .line 379
    monitor-exit p0

    .line 384
    :goto_1c
    return-void

    .line 381
    :cond_1d
    invoke-virtual {v0}, LaJ/l;->Z()V

    goto :goto_1

    .line 383
    :catchall_21
    move-exception v0

    monitor-exit p0
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_21

    throw v0

    :cond_24
    :try_start_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_21

    goto :goto_1c
.end method


# virtual methods
.method public Z()V
    .registers 2

    .prologue
    .line 318
    invoke-super {p0}, Lad/a;->Z()V

    .line 319
    iget-object v0, p0, LaJ/l;->b:LaU/i;

    invoke-virtual {v0}, LaU/i;->c()V

    .line 320
    return-void
.end method

.method public a()I
    .registers 2

    .prologue
    .line 266
    const/16 v0, 0x4c

    return v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 284
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gP;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 287
    const/4 v1, 0x5

    iget-object v2, p0, LaJ/l;->a:LaJ/j;

    invoke-static {v2}, LaJ/j;->d(LaJ/j;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 289
    iget v1, p0, LaJ/l;->f:I

    if-eqz v1, :cond_1c

    .line 290
    const/4 v1, 0x4

    iget v2, p0, LaJ/l;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 292
    :cond_1c
    iget-object v1, p0, LaJ/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_26

    .line 293
    const/4 v1, 0x2

    iget-object v2, p0, LaJ/l;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 295
    :cond_26
    iget-object v1, p0, LaJ/l;->d:Lau/B;

    if-eqz v1, :cond_34

    .line 296
    const/4 v1, 0x3

    iget-object v2, p0, LaJ/l;->d:Lau/B;

    invoke-static {v2}, Lau/C;->c(Lau/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 299
    :cond_34
    iget-object v1, p0, LaJ/l;->e:LaJ/s;

    invoke-virtual {v1}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 300
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 301
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 302
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 303
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 304
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 3
    .parameter

    .prologue
    .line 442
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gP;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, LaJ/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 444
    const/4 v0, 0x1

    return v0
.end method

.method public i_()Z
    .registers 2

    .prologue
    .line 271
    const/4 v0, 0x1

    return v0
.end method

.method public j_()Z
    .registers 2

    .prologue
    .line 279
    const/4 v0, 0x0

    return v0
.end method

.method public l_()V
    .registers 4

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x1

    .line 388
    iget-object v0, p0, LaJ/l;->b:LaU/i;

    invoke-virtual {v0}, LaU/i;->b()V

    .line 391
    iget-object v0, p0, LaJ/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 392
    iget-object v0, p0, LaJ/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 393
    const/4 v1, -0x1

    invoke-static {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    .line 395
    invoke-static {}, Lat/f;->j()Lat/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lat/f;->a(I)V

    .line 400
    :cond_21
    iget-object v0, p0, LaJ/l;->a:LaJ/j;

    invoke-static {v0}, LaJ/j;->b(LaJ/j;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-virtual {p0}, LaJ/l;->A_()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 424
    :cond_33
    :goto_33
    return-void

    .line 405
    :cond_34
    iget-object v0, p0, LaJ/l;->g:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    .line 407
    iget-object v1, p0, LaJ/l;->e:LaJ/s;

    invoke-virtual {v1}, LaJ/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lab/b;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 410
    invoke-virtual {p0}, LaJ/l;->Z()V

    goto :goto_33

    .line 414
    :cond_4a
    invoke-direct {p0}, LaJ/l;->l()V

    .line 416
    invoke-direct {p0}, LaJ/l;->k()LaJ/z;

    move-result-object v0

    .line 417
    iget-object v1, p0, LaJ/l;->e:LaJ/s;

    invoke-static {v1}, LaJ/f;->f(LaJ/s;)Z

    move-result v1

    if-nez v1, :cond_60

    .line 420
    invoke-static {}, LaJ/o;->a()LaJ/o;

    move-result-object v1

    invoke-virtual {v1}, LaJ/o;->b()V

    .line 423
    :cond_60
    iget-object v1, p0, LaJ/l;->a:LaJ/j;

    invoke-virtual {v1, v0, v2}, LaJ/j;->a(LaJ/z;Z)V

    goto :goto_33
.end method

.method public m_()V
    .registers 5

    .prologue
    .line 428
    iget-object v0, p0, LaJ/l;->b:LaU/i;

    invoke-virtual {v0}, LaU/i;->c()V

    .line 429
    invoke-direct {p0}, LaJ/l;->l()V

    .line 432
    iget-object v0, p0, LaJ/l;->a:LaJ/j;

    invoke-virtual {v0}, LaJ/j;->i()Z

    .line 435
    const/4 v0, 0x1

    .line 436
    iget-object v1, p0, LaJ/l;->a:LaJ/j;

    new-instance v2, LaJ/z;

    iget-object v3, p0, LaJ/l;->e:LaJ/s;

    invoke-direct {v2, v3}, LaJ/z;-><init>(LaJ/s;)V

    invoke-virtual {v1, v2, v0}, LaJ/j;->a(LaJ/z;Z)V

    .line 437
    return-void
.end method
