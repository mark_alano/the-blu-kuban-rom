.class public LaL/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/net/wifi/WifiManager;

.field private final c:LaL/i;

.field private final d:I

.field private final e:LaL/j;

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;LaL/i;I)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean v0, p0, LaL/g;->f:Z

    .line 45
    iput v0, p0, LaL/g;->g:I

    .line 58
    iput-object p1, p0, LaL/g;->a:Landroid/content/Context;

    .line 59
    iput-object p2, p0, LaL/g;->b:Landroid/net/wifi/WifiManager;

    .line 60
    iput-object p3, p0, LaL/g;->c:LaL/i;

    .line 61
    iput p4, p0, LaL/g;->d:I

    .line 62
    new-instance v0, LaL/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaL/j;-><init>(LaL/g;LaL/h;)V

    iput-object v0, p0, LaL/g;->e:LaL/j;

    .line 63
    return-void
.end method

.method static synthetic a(LaL/g;)Landroid/net/wifi/WifiManager;
    .registers 2
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, LaL/g;->b:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic b(LaL/g;)LaL/i;
    .registers 2
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, LaL/g;->c:LaL/i;

    return-object v0
.end method

.method static synthetic c(LaL/g;)I
    .registers 3
    .parameter

    .prologue
    .line 19
    iget v0, p0, LaL/g;->g:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LaL/g;->g:I

    return v0
.end method

.method static synthetic d(LaL/g;)I
    .registers 2
    .parameter

    .prologue
    .line 19
    iget v0, p0, LaL/g;->g:I

    return v0
.end method

.method static synthetic e(LaL/g;)I
    .registers 2
    .parameter

    .prologue
    .line 19
    iget v0, p0, LaL/g;->d:I

    return v0
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    .line 69
    iget-boolean v0, p0, LaL/g;->f:Z

    if-eqz v0, :cond_5

    .line 79
    :goto_4
    return-void

    .line 74
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LaL/g;->f:Z

    .line 76
    iget-object v0, p0, LaL/g;->a:Landroid/content/Context;

    iget-object v1, p0, LaL/g;->e:LaL/j;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 78
    iget-object v0, p0, LaL/g;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    goto :goto_4
.end method

.method public b()V
    .registers 3

    .prologue
    .line 85
    iget-boolean v0, p0, LaL/g;->f:Z

    if-nez v0, :cond_5

    .line 93
    :goto_4
    return-void

    .line 90
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, LaL/g;->f:Z

    .line 92
    iget-object v0, p0, LaL/g;->a:Landroid/content/Context;

    iget-object v1, p0, LaL/g;->e:LaL/j;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_4
.end method
