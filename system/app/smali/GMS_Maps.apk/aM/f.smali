.class public abstract LaM/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:LaM/f;

.field private static final j:Landroid/net/Uri;


# instance fields
.field private final b:LaM/e;

.field private c:Ljava/util/Vector;

.field private final d:Ljava/lang/Object;

.field private volatile e:Z

.field private f:Z

.field private g:Ljava/util/Vector;

.field private h:Ljava/lang/String;

.field private i:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 165
    const-string v0, "https://www.google.com/accounts/TokenAuth"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LaM/f;->j:Landroid/net/Uri;

    return-void
.end method

.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    new-instance v0, LaM/e;

    invoke-direct {v0}, LaM/e;-><init>()V

    iput-object v0, p0, LaM/f;->b:LaM/e;

    .line 131
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    .line 134
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaM/f;->d:Ljava/lang/Object;

    .line 146
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaM/f;->g:Ljava/util/Vector;

    .line 159
    const-string v0, "https://www.google.com/accounts/IssueAuthToken?service=gaia&Session=false"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LaM/f;->i:Landroid/net/Uri;

    .line 169
    return-void
.end method

.method public static a(LaM/f;)V
    .registers 1
    .parameter

    .prologue
    .line 180
    sput-object p0, LaM/f;->a:LaM/f;

    .line 181
    return-void
.end method

.method public static j()LaM/f;
    .registers 1

    .prologue
    .line 187
    sget-object v0, LaM/f;->a:LaM/f;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 755
    iget-object v0, p0, LaM/f;->i:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "SID"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "LSID"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 760
    const/4 v2, 0x0

    .line 762
    :try_start_1b
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 763
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2e
    .catchall {:try_start_1b .. :try_end_2e} :catchall_5a

    .line 764
    :try_start_2e
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 767
    sget-object v2, LaM/f;->j:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "source"

    const-string v4, "android-browser"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "auth"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "continue"

    invoke-virtual {v0, v2, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_53
    .catchall {:try_start_2e .. :try_end_53} :catchall_62

    move-result-object v0

    .line 776
    if-eqz v1, :cond_59

    .line 777
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :cond_59
    return-object v0

    .line 776
    :catchall_5a
    move-exception v0

    move-object v1, v2

    :goto_5c
    if-eqz v1, :cond_61

    .line 777
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    :cond_61
    throw v0

    .line 776
    :catchall_62
    move-exception v0

    goto :goto_5c
.end method

.method public a()V
    .registers 1

    .prologue
    .line 323
    invoke-virtual {p0}, LaM/f;->w()V

    .line 324
    invoke-virtual {p0}, LaM/f;->n()V

    .line 325
    return-void
.end method

.method public abstract a(I)V
.end method

.method public final a(LaM/g;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 292
    iput-boolean v0, p0, LaM/f;->f:Z

    .line 293
    invoke-virtual {p0, p1, v0}, LaM/f;->a(LaM/g;Z)V

    .line 294
    return-void
.end method

.method protected abstract a(LaM/g;Z)V
.end method

.method public a(LaM/h;)V
    .registers 7
    .parameter

    .prologue
    .line 537
    iget-object v1, p0, LaM/f;->b:LaM/e;

    monitor-enter v1

    .line 538
    :try_start_3
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 541
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 542
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_f

    .line 543
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Already have an instance of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " present in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SystemEventListeners.  Cannot add "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 550
    :catchall_5c
    move-exception v0

    monitor-exit v1
    :try_end_5e
    .catchall {:try_start_3 .. :try_end_5e} :catchall_5c

    throw v0

    .line 549
    :cond_5f
    :try_start_5f
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 550
    monitor-exit v1
    :try_end_65
    .catchall {:try_start_5f .. :try_end_65} :catchall_5c

    .line 551
    return-void
.end method

.method public a(LaM/i;)V
    .registers 4
    .parameter

    .prologue
    .line 650
    iget-object v1, p0, LaM/f;->g:Ljava/util/Vector;

    monitor-enter v1

    .line 651
    :try_start_3
    iget-object v0, p0, LaM/f;->g:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 652
    monitor-exit v1

    .line 653
    return-void

    .line 652
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 686
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 687
    if-nez p1, :cond_16

    .line 688
    iput-object v2, p0, LaM/f;->h:Ljava/lang/String;

    .line 689
    const-string v1, "CurrentAccountName"

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 690
    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    .line 715
    :cond_15
    :goto_15
    return-void

    .line 697
    :cond_16
    iget-object v1, p0, LaM/f;->h:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 704
    iput-object p1, p0, LaM/f;->h:Ljava/lang/String;

    .line 706
    :try_start_20
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 707
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 708
    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 709
    const-string v2, "CurrentAccountName"

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 710
    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_39} :catch_3a

    goto :goto_15

    .line 711
    :catch_3a
    move-exception v0

    .line 712
    const-string v1, "LOGIN - setting current account name failed."

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_15
.end method

.method public final b(LaM/g;)V
    .registers 3
    .parameter

    .prologue
    .line 302
    const/4 v0, 0x0

    iput-boolean v0, p0, LaM/f;->f:Z

    .line 303
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LaM/f;->a(LaM/g;Z)V

    .line 304
    return-void
.end method

.method public b(LaM/h;)V
    .registers 4
    .parameter

    .prologue
    .line 558
    iget-object v1, p0, LaM/f;->b:LaM/e;

    monitor-enter v1

    .line 559
    :try_start_3
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 560
    monitor-exit v1

    .line 561
    return-void

    .line 560
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public b(LaM/i;)V
    .registers 4
    .parameter

    .prologue
    .line 659
    iget-object v1, p0, LaM/f;->g:Ljava/util/Vector;

    monitor-enter v1

    .line 660
    :try_start_3
    iget-object v0, p0, LaM/f;->g:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 661
    monitor-exit v1

    .line 662
    return-void

    .line 661
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 342
    iget-object v1, p0, LaM/f;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 343
    :try_start_3
    iput-boolean p1, p0, LaM/f;->e:Z

    .line 344
    monitor-exit v1

    .line 345
    return-void

    .line 344
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method protected abstract b()Z
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method public c(LaM/g;)V
    .registers 5
    .parameter

    .prologue
    .line 389
    iget-object v1, p0, LaM/f;->b:LaM/e;

    monitor-enter v1

    .line 390
    :try_start_3
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0, p1}, LaM/e;->d(LaM/g;)V

    .line 391
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 392
    invoke-interface {v0}, LaM/h;->M_()V

    goto :goto_e

    .line 394
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0

    :cond_21
    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_1e

    .line 395
    return-void
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method protected d(LaM/g;)V
    .registers 3
    .parameter

    .prologue
    .line 430
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0, p1}, LaM/e;->b(LaM/g;)V

    .line 431
    return-void
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public e(LaM/g;)V
    .registers 3
    .parameter

    .prologue
    .line 527
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0, p1}, LaM/e;->a(LaM/g;)V

    .line 528
    return-void
.end method

.method protected abstract f()V
.end method

.method public abstract g()V
.end method

.method public h()V
    .registers 1

    .prologue
    .line 675
    invoke-virtual {p0}, LaM/f;->u()V

    .line 676
    return-void
.end method

.method protected i()V
    .registers 1

    .prologue
    .line 176
    invoke-virtual {p0}, LaM/f;->n()V

    .line 177
    return-void
.end method

.method public final k()Z
    .registers 2

    .prologue
    .line 197
    invoke-virtual {p0}, LaM/f;->b()Z

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 211
    invoke-virtual {p0}, LaM/f;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final m()V
    .registers 1

    .prologue
    .line 261
    invoke-virtual {p0}, LaM/f;->f()V

    .line 263
    return-void
.end method

.method public n()V
    .registers 3

    .prologue
    .line 315
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {p0}, LaM/f;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Law/h;->b(Ljava/lang/String;)V

    .line 316
    return-void
.end method

.method public o()Z
    .registers 3

    .prologue
    .line 332
    iget-object v1, p0, LaM/f;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 333
    :try_start_3
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0}, LaM/e;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 334
    const/4 v0, 0x1

    monitor-exit v1

    .line 338
    :goto_d
    return v0

    .line 336
    :cond_e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_12

    .line 338
    iget-boolean v0, p0, LaM/f;->e:Z

    goto :goto_d

    .line 336
    :catchall_12
    move-exception v0

    :try_start_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public p()V
    .registers 4

    .prologue
    .line 353
    iget-object v1, p0, LaM/f;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 355
    :try_start_3
    iget-boolean v0, p0, LaM/f;->e:Z

    if-eqz v0, :cond_f

    .line 357
    const/4 v0, 0x1

    iput-boolean v0, p0, LaM/f;->f:Z

    .line 358
    const/4 v0, 0x0

    iput-boolean v0, p0, LaM/f;->e:Z

    .line 361
    monitor-exit v1

    .line 374
    :goto_e
    return-void

    .line 363
    :cond_f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_34

    .line 365
    invoke-virtual {p0}, LaM/f;->m()V

    .line 368
    iget-object v1, p0, LaM/f;->b:LaM/e;

    monitor-enter v1

    .line 369
    :try_start_16
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0}, LaM/e;->d()V

    .line 370
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 371
    invoke-interface {v0}, LaM/h;->M_()V

    goto :goto_21

    .line 373
    :catchall_31
    move-exception v0

    monitor-exit v1
    :try_end_33
    .catchall {:try_start_16 .. :try_end_33} :catchall_31

    throw v0

    .line 363
    :catchall_34
    move-exception v0

    :try_start_35
    monitor-exit v1
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    throw v0

    .line 373
    :cond_37
    :try_start_37
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_31

    goto :goto_e
.end method

.method public q()V
    .registers 4

    .prologue
    .line 401
    iget-object v1, p0, LaM/f;->b:LaM/e;

    monitor-enter v1

    .line 402
    :try_start_3
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0}, LaM/e;->e()V

    .line 403
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 404
    invoke-interface {v0}, LaM/h;->N_()V

    goto :goto_e

    .line 406
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0

    :cond_21
    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_1e

    .line 407
    return-void
.end method

.method public r()V
    .registers 4

    .prologue
    .line 417
    iget-object v1, p0, LaM/f;->b:LaM/e;

    monitor-enter v1

    .line 418
    :try_start_3
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0}, LaM/e;->f()V

    .line 419
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 420
    invoke-interface {v0}, LaM/h;->E_()V

    goto :goto_e

    .line 422
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0

    :cond_21
    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_1e

    .line 423
    return-void
.end method

.method public s()V
    .registers 5

    .prologue
    .line 450
    iget-object v1, p0, LaM/f;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 452
    :try_start_3
    iget-boolean v0, p0, LaM/f;->f:Z

    if-eqz v0, :cond_37

    .line 453
    const/4 v0, 0x0

    iput-boolean v0, p0, LaM/f;->f:Z

    .line 456
    invoke-virtual {p0}, LaM/f;->m()V

    .line 459
    iget-object v2, p0, LaM/f;->b:LaM/e;

    monitor-enter v2
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_2e

    .line 460
    :try_start_10
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0}, LaM/e;->c()V

    .line 461
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 462
    invoke-interface {v0}, LaM/h;->D_()V

    goto :goto_1b

    .line 464
    :catchall_2b
    move-exception v0

    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_10 .. :try_end_2d} :catchall_2b

    :try_start_2d
    throw v0

    .line 492
    :catchall_2e
    move-exception v0

    monitor-exit v1
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_2e

    throw v0

    .line 464
    :cond_31
    :try_start_31
    monitor-exit v2
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_2b

    .line 491
    :goto_32
    const/4 v0, 0x0

    :try_start_33
    iput-boolean v0, p0, LaM/f;->e:Z

    .line 492
    monitor-exit v1

    .line 493
    return-void

    .line 469
    :cond_37
    iget-object v2, p0, LaM/f;->b:LaM/e;

    monitor-enter v2
    :try_end_3a
    .catchall {:try_start_33 .. :try_end_3a} :catchall_2e

    .line 470
    :try_start_3a
    invoke-virtual {p0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_61

    .line 473
    invoke-virtual {p0}, LaM/f;->n()V

    .line 476
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0}, LaM/e;->a()V

    .line 477
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 478
    invoke-interface {v0}, LaM/h;->C_()V

    goto :goto_4e

    .line 487
    :catchall_5e
    move-exception v0

    monitor-exit v2
    :try_end_60
    .catchall {:try_start_3a .. :try_end_60} :catchall_5e

    :try_start_60
    throw v0
    :try_end_61
    .catchall {:try_start_60 .. :try_end_61} :catchall_2e

    .line 482
    :cond_61
    :try_start_61
    iget-object v0, p0, LaM/f;->b:LaM/e;

    invoke-virtual {v0}, LaM/e;->c()V

    .line 483
    iget-object v0, p0, LaM/f;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 484
    invoke-interface {v0}, LaM/h;->D_()V

    goto :goto_6c

    .line 487
    :cond_7c
    monitor-exit v2
    :try_end_7d
    .catchall {:try_start_61 .. :try_end_7d} :catchall_5e

    goto :goto_32
.end method

.method protected t()V
    .registers 3

    .prologue
    .line 496
    .line 497
    iget-object v1, p0, LaM/f;->g:Ljava/util/Vector;

    monitor-enter v1

    .line 501
    :try_start_3
    iget-object v0, p0, LaM/f;->g:Ljava/util/Vector;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 502
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_1e

    .line 503
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/i;

    .line 504
    invoke-interface {v0}, LaM/i;->o()V

    goto :goto_e

    .line 502
    :catchall_1e
    move-exception v0

    :try_start_1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    throw v0

    .line 506
    :cond_21
    return-void
.end method

.method protected u()V
    .registers 3

    .prologue
    .line 509
    .line 510
    iget-object v1, p0, LaM/f;->g:Ljava/util/Vector;

    monitor-enter v1

    .line 514
    :try_start_3
    iget-object v0, p0, LaM/f;->g:Ljava/util/Vector;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 515
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_1e

    .line 516
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/i;

    .line 517
    invoke-interface {v0}, LaM/i;->p()V

    goto :goto_e

    .line 515
    :catchall_1e
    move-exception v0

    :try_start_1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    throw v0

    .line 519
    :cond_21
    return-void
.end method

.method public v()Ljava/lang/String;
    .registers 3

    .prologue
    .line 721
    iget-object v0, p0, LaM/f;->h:Ljava/lang/String;

    if-nez v0, :cond_14

    .line 722
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "CurrentAccountName"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaM/f;->h:Ljava/lang/String;

    .line 726
    :cond_14
    iget-object v0, p0, LaM/f;->h:Ljava/lang/String;

    return-object v0
.end method

.method public w()V
    .registers 2

    .prologue
    .line 733
    const/4 v0, 0x0

    iput-object v0, p0, LaM/f;->h:Ljava/lang/String;

    .line 734
    invoke-virtual {p0}, LaM/f;->v()Ljava/lang/String;

    .line 735
    return-void
.end method
