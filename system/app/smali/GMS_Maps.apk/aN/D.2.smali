.class public LaN/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/o;
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/util/Hashtable;

.field b:LaN/K;

.field volatile c:Z

.field private final d:Z

.field private final e:Ljava/util/Vector;

.field private f:LaN/m;

.field private final g:Ljava/util/Vector;

.field private h:I

.field private i:I

.field private volatile j:Z

.field private k:LaN/G;

.field private l:I

.field private final m:Ljava/util/Hashtable;

.field private final n:Ljava/lang/Object;

.field private final o:Ljava/lang/Object;

.field private p:J

.field private final q:Ljava/util/List;

.field private r:I

.field private s:J


# direct methods
.method constructor <init>(IIIILjava/lang/String;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    .line 97
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/D;->g:Ljava/util/Vector;

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, LaN/D;->k:LaN/G;

    .line 128
    iput v2, p0, LaN/D;->l:I

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaN/D;->n:Ljava/lang/Object;

    .line 169
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaN/D;->o:Ljava/lang/Object;

    .line 175
    iput-boolean v4, p0, LaN/D;->c:Z

    .line 178
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaN/D;->q:Ljava/util/List;

    .line 195
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, LaN/D;->s:J

    .line 256
    const/16 v0, 0x1a

    iput v0, p0, LaN/D;->r:I

    .line 257
    if-ne p1, v3, :cond_76

    .line 258
    iput-boolean v4, p0, LaN/D;->d:Z

    .line 259
    const/16 v0, 0x61a8

    iput v0, p0, LaN/D;->h:I

    .line 260
    invoke-direct {p0}, LaN/D;->s()V

    .line 272
    :goto_42
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    .line 273
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    .line 274
    iput-boolean v2, p0, LaN/D;->j:Z

    .line 280
    new-instance v0, LaN/E;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    move-object v1, p0

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LaN/E;-><init>(LaN/D;Las/c;IILjava/lang/String;)V

    invoke-virtual {v0}, LaN/E;->g()V

    .line 288
    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    iput-wide v0, p0, LaN/D;->p:J

    .line 294
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, LaN/F;

    invoke-direct {v1, p0}, LaN/F;-><init>(LaN/D;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 301
    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    .line 302
    return-void

    .line 262
    :cond_76
    iput-boolean v2, p0, LaN/D;->d:Z

    .line 263
    iput p1, p0, LaN/D;->h:I

    .line 265
    if-ne p2, v3, :cond_80

    .line 266
    invoke-direct {p0}, LaN/D;->s()V

    goto :goto_42

    .line 268
    :cond_80
    iput p2, p0, LaN/D;->i:I

    goto :goto_42
.end method

.method private a([J[LaN/P;III)I
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 982
    aget-wide v2, p1, p5

    .line 983
    invoke-direct {p0, p1, p2, p5, p4}, LaN/D;->a([J[LaN/P;II)V

    move v1, p3

    .line 985
    :goto_6
    if-ge p3, p4, :cond_17

    .line 986
    aget-wide v4, p1, p3

    cmp-long v0, v4, v2

    if-ltz v0, :cond_25

    .line 987
    add-int/lit8 v0, v1, 0x1

    invoke-direct {p0, p1, p2, p3, v1}, LaN/D;->a([J[LaN/P;II)V

    .line 985
    :goto_13
    add-int/lit8 p3, p3, 0x1

    move v1, v0

    goto :goto_6

    .line 991
    :cond_17
    aget-wide v2, p1, p4

    aget-wide v4, p1, v1

    cmp-long v0, v2, v4

    if-lez v0, :cond_23

    .line 992
    invoke-direct {p0, p1, p2, p4, v1}, LaN/D;->a([J[LaN/P;II)V

    .line 995
    :goto_22
    return v1

    :cond_23
    move v1, p4

    goto :goto_22

    :cond_25
    move v0, v1

    goto :goto_13
.end method

.method static a(LaN/P;JJ)J
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 924
    sub-long v0, p1, p3

    return-wide v0
.end method

.method private a(LaN/P;I)Lam/f;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 556
    packed-switch p2, :pswitch_data_10

    .line 565
    invoke-direct {p0, p1}, LaN/D;->c(LaN/P;)Lam/f;

    move-result-object v0

    .line 569
    :goto_7
    return-object v0

    .line 558
    :pswitch_8
    const/4 v0, 0x0

    .line 559
    goto :goto_7

    .line 561
    :pswitch_a
    invoke-direct {p0, p1}, LaN/D;->b(LaN/P;)Lam/f;

    move-result-object v0

    goto :goto_7

    .line 556
    nop

    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_8
        :pswitch_a
    .end packed-switch
.end method

.method private a(LaN/P;LaN/P;LaN/I;)Lam/f;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x100

    const/4 v0, 0x0

    const/16 v3, 0x80

    .line 687
    .line 688
    invoke-virtual {p1}, LaN/P;->c()I

    move-result v1

    invoke-virtual {p2}, LaN/P;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    if-ne v1, v2, :cond_27

    move v1, v0

    .line 689
    :goto_12
    invoke-virtual {p1}, LaN/P;->d()I

    move-result v2

    invoke-virtual {p2}, LaN/P;->d()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    if-ne v2, v4, :cond_29

    move v2, v0

    :goto_1f
    move-object v0, p3

    move v4, v3

    move v6, v5

    .line 690
    invoke-virtual/range {v0 .. v6}, LaN/I;->a(IIIIII)Lam/f;

    move-result-object v0

    return-object v0

    :cond_27
    move v1, v3

    .line 688
    goto :goto_12

    :cond_29
    move v2, v3

    .line 689
    goto :goto_1f
.end method

.method private a(I)V
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 802
    iget-object v3, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v3

    .line 804
    const/4 v1, 0x1

    :try_start_5
    iput-boolean v1, p0, LaN/D;->j:Z

    .line 806
    invoke-virtual {p0}, LaN/D;->i()[LaN/P;

    move-result-object v4

    move v2, v0

    move v1, p1

    .line 808
    :goto_d
    array-length v0, v4

    if-ge v2, v0, :cond_3a

    iget v0, p0, LaN/D;->i:I

    if-le v1, v0, :cond_3a

    .line 809
    aget-object v5, v4, v2

    .line 811
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    .line 814
    invoke-virtual {v0}, LaN/I;->e()Z

    move-result v6

    if-nez v6, :cond_2a

    invoke-virtual {v0}, LaN/I;->v()Z

    move-result v6

    if-nez v6, :cond_47

    .line 815
    :cond_2a
    iget-object v6, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v6, v5}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    invoke-virtual {v0}, LaN/I;->j()I
    :try_end_32
    .catchall {:try_start_5 .. :try_end_32} :catchall_3f

    move-result v0

    sub-int v0, v1, v0

    .line 808
    :goto_35
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_d

    .line 827
    :cond_3a
    const/4 v0, 0x0

    :try_start_3b
    iput-boolean v0, p0, LaN/D;->j:Z

    .line 829
    monitor-exit v3

    .line 830
    return-void

    .line 827
    :catchall_3f
    move-exception v0

    const/4 v1, 0x0

    iput-boolean v1, p0, LaN/D;->j:Z

    throw v0

    .line 829
    :catchall_44
    move-exception v0

    monitor-exit v3
    :try_end_46
    .catchall {:try_start_3b .. :try_end_46} :catchall_44

    throw v0

    :cond_47
    move v0, v1

    goto :goto_35
.end method

.method private a(IILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 306
    if-lez p1, :cond_a

    .line 307
    new-instance v0, LaN/x;

    invoke-direct {v0, p0, p3, p1, p2}, LaN/x;-><init>(LaN/D;Ljava/lang/String;II)V

    iput-object v0, p0, LaN/D;->b:LaN/K;

    .line 313
    :goto_9
    return-void

    .line 311
    :cond_a
    new-instance v0, LaN/L;

    invoke-direct {v0}, LaN/L;-><init>()V

    iput-object v0, p0, LaN/D;->b:LaN/K;

    goto :goto_9
.end method

.method static synthetic a(LaN/D;)V
    .registers 1
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, LaN/D;->w()V

    return-void
.end method

.method static synthetic a(LaN/D;IILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, LaN/D;->a(IILjava/lang/String;)V

    return-void
.end method

.method private a(LaN/I;)V
    .registers 4
    .parameter

    .prologue
    .line 759
    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v0

    .line 760
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 761
    return-void
.end method

.method private a(LaN/I;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 696
    iget-object v0, p0, LaN/D;->k:LaN/G;

    if-nez v0, :cond_13

    .line 699
    new-instance v0, LaN/G;

    invoke-virtual {p1}, LaN/I;->c()LaN/P;

    move-result-object v1

    invoke-virtual {v1}, LaN/P;->b()B

    move-result v1

    invoke-direct {v0, p0, v1}, LaN/G;-><init>(LaN/D;B)V

    iput-object v0, p0, LaN/D;->k:LaN/G;

    .line 701
    :cond_13
    iget-object v0, p0, LaN/D;->k:LaN/G;

    invoke-virtual {v0, p1, p2}, LaN/G;->a(LaN/I;I)V

    .line 702
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LaN/I;->a(Z)V

    .line 703
    return-void
.end method

.method private a([J[LaN/P;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1014
    const/4 v0, 0x0

    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, LaN/D;->b([J[LaN/P;II)V

    .line 1015
    return-void
.end method

.method private a([J[LaN/P;II)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 961
    aget-object v0, p2, p4

    .line 962
    aget-object v1, p2, p3

    aput-object v1, p2, p4

    .line 963
    aput-object v0, p2, p3

    .line 965
    aget-wide v0, p1, p4

    .line 966
    aget-wide v2, p1, p3

    aput-wide v2, p1, p4

    .line 967
    aput-wide v0, p1, p3

    .line 968
    return-void
.end method

.method static synthetic b(LaN/D;)I
    .registers 2
    .parameter

    .prologue
    .line 46
    iget v0, p0, LaN/D;->r:I

    return v0
.end method

.method private b(LaN/P;)Lam/f;
    .registers 3
    .parameter

    .prologue
    .line 579
    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    return-object v0
.end method

.method private b([J[LaN/P;II)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1005
    if-le p4, p3, :cond_16

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p3

    .line 1007
    invoke-direct/range {v0 .. v5}, LaN/D;->a([J[LaN/P;III)I

    move-result v0

    .line 1008
    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, p1, p2, p3, v1}, LaN/D;->b([J[LaN/P;II)V

    .line 1009
    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, p2, v0, p4}, LaN/D;->b([J[LaN/P;II)V

    .line 1011
    :cond_16
    return-void
.end method

.method static synthetic c(LaN/D;)I
    .registers 3
    .parameter

    .prologue
    .line 46
    iget v0, p0, LaN/D;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LaN/D;->l:I

    return v0
.end method

.method private c(LaN/P;)Lam/f;
    .registers 4
    .parameter

    .prologue
    .line 590
    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    .line 591
    if-nez v0, :cond_15

    .line 592
    invoke-direct {p0, p1}, LaN/D;->d(LaN/P;)Lam/f;

    move-result-object v0

    .line 593
    if-eqz v0, :cond_15

    .line 594
    iget-object v1, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    :cond_15
    return-object v0
.end method

.method static synthetic d(LaN/D;)I
    .registers 3
    .parameter

    .prologue
    .line 46
    iget v0, p0, LaN/D;->l:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LaN/D;->l:I

    return v0
.end method

.method private d(LaN/P;)Lam/f;
    .registers 10
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 607
    invoke-static {}, LaN/D;->u()J

    move-result-wide v2

    .line 608
    iget-wide v4, p0, LaN/D;->s:J

    const-wide/16 v6, 0x2710

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_f

    .line 641
    :cond_e
    :goto_e
    return-object v0

    .line 613
    :cond_f
    :try_start_f
    invoke-virtual {p1}, LaN/P;->h()LaN/P;

    move-result-object v1

    .line 614
    if-eqz v1, :cond_e

    .line 615
    invoke-virtual {v1}, LaN/P;->e()LaN/Y;

    move-result-object v4

    invoke-virtual {p1}, LaN/P;->e()LaN/Y;

    move-result-object v5

    invoke-virtual {v4, v5}, LaN/Y;->a(LaN/Y;)I

    move-result v4

    .line 616
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v1, v5, v6, v7}, LaN/D;->a(LaN/P;IZZ)LaN/I;

    move-result-object v5

    .line 617
    const/4 v6, 0x2

    if-ne v4, v6, :cond_e

    .line 620
    invoke-direct {p0, p1, v1, v5}, LaN/D;->a(LaN/P;LaN/P;LaN/I;)Lam/f;
    :try_end_2e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_2e} :catch_30

    move-result-object v0

    goto :goto_e

    .line 623
    :catch_30
    move-exception v1

    .line 636
    invoke-direct {p0}, LaN/D;->t()V

    .line 637
    iput-wide v2, p0, LaN/D;->s:J

    .line 638
    const-string v2, "Map Service image scaling"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_e
.end method

.method private d(Z)V
    .registers 13
    .parameter

    .prologue
    .line 719
    if-eqz p1, :cond_5e

    const-wide/16 v0, 0x7d0

    move-wide v2, v0

    .line 720
    :goto_5
    iget-object v5, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v5

    .line 721
    const/4 v0, 0x1

    :try_start_9
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_6e

    .line 723
    :try_start_c
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v6

    .line 724
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v8

    :cond_1e
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_62

    .line 725
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    .line 726
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/I;

    .line 727
    invoke-virtual {v1}, LaN/I;->g()J

    move-result-wide v9

    .line 729
    add-long/2addr v9, v2

    cmp-long v4, v9, v6

    if-gez v4, :cond_1e

    .line 730
    invoke-virtual {v1}, LaN/I;->d()V

    .line 733
    iget-object v1, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v4, v1

    :goto_47
    if-ltz v4, :cond_1e

    .line 734
    iget-object v1, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/k;

    const/16 v9, 0x8

    invoke-static {v9, v0}, LaN/P;->a(BLaN/P;)LaN/P;

    move-result-object v9

    invoke-virtual {v1, v9}, LaN/k;->b(LaN/P;)V
    :try_end_5a
    .catchall {:try_start_c .. :try_end_5a} :catchall_68

    .line 733
    add-int/lit8 v1, v4, -0x1

    move v4, v1

    goto :goto_47

    .line 719
    :cond_5e
    const-wide/16 v0, 0xfa0

    move-wide v2, v0

    goto :goto_5

    .line 739
    :cond_62
    const/4 v0, 0x0

    :try_start_63
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V

    .line 741
    monitor-exit v5

    .line 742
    return-void

    .line 739
    :catchall_68
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    throw v0

    .line 741
    :catchall_6e
    move-exception v0

    monitor-exit v5
    :try_end_70
    .catchall {:try_start_63 .. :try_end_70} :catchall_6e

    throw v0
.end method

.method private e(LaN/P;)J
    .registers 4
    .parameter

    .prologue
    .line 930
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic e(LaN/D;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, LaN/D;->q:Ljava/util/List;

    return-object v0
.end method

.method private s()V
    .registers 2

    .prologue
    .line 334
    iget v0, p0, LaN/D;->h:I

    mul-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0x5

    iput v0, p0, LaN/D;->i:I

    .line 335
    return-void
.end method

.method private t()V
    .registers 4

    .prologue
    .line 645
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v1

    .line 646
    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, LaN/D;->j:Z

    .line 649
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    .line 650
    :goto_c
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 651
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    .line 652
    invoke-virtual {v0}, LaN/I;->a()V

    goto :goto_c

    .line 658
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_1c

    throw v0

    .line 654
    :cond_1f
    const/4 v0, 0x0

    :try_start_20
    iput-boolean v0, p0, LaN/D;->j:Z

    .line 657
    invoke-virtual {p0}, LaN/D;->b()V

    .line 658
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_20 .. :try_end_26} :catchall_1c

    .line 659
    return-void
.end method

.method private static u()J
    .registers 2

    .prologue
    .line 1278
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method private v()V
    .registers 3

    .prologue
    .line 1285
    iget-boolean v0, p0, LaN/D;->c:Z

    if-nez v0, :cond_19

    .line 1286
    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/D;->c:Z

    .line 1287
    iget-object v1, p0, LaN/D;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1288
    :try_start_a
    iget-object v0, p0, LaN/D;->o:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1289
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_a .. :try_end_10} :catchall_1a

    .line 1290
    iget-object v1, p0, LaN/D;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 1291
    :try_start_13
    iget-object v0, p0, LaN/D;->n:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1292
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_13 .. :try_end_19} :catchall_1d

    .line 1294
    :cond_19
    return-void

    .line 1289
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    throw v0

    .line 1292
    :catchall_1d
    move-exception v0

    :try_start_1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    throw v0
.end method

.method private w()V
    .registers 3

    .prologue
    .line 1300
    iget-boolean v0, p0, LaN/D;->c:Z

    if-eqz v0, :cond_15

    .line 1301
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/D;->c:Z

    .line 1302
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "MapService"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1303
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 1304
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1306
    :cond_15
    return-void
.end method


# virtual methods
.method a(LaN/P;IZIJ)LaN/I;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 439
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    .line 446
    iget-object v1, p0, LaN/D;->b:LaN/K;

    if-nez v1, :cond_10

    move p3, v4

    .line 450
    :cond_10
    const-wide/high16 v1, -0x8000

    cmp-long v1, p5, v1

    if-nez v1, :cond_be

    .line 451
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    .line 454
    :goto_22
    if-nez v0, :cond_93

    .line 456
    iget-boolean v0, p0, LaN/D;->j:Z

    if-nez v0, :cond_8d

    .line 457
    iget-object v4, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v4

    .line 458
    const/4 v0, 0x1

    :try_start_2c
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V
    :try_end_2f
    .catchall {:try_start_2c .. :try_end_2f} :catchall_79

    .line 462
    :try_start_2f
    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_67

    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0, p1}, LaN/K;->a(LaN/P;)LaN/I;

    move-result-object v0

    .line 464
    :goto_39
    if-nez v0, :cond_7c

    .line 469
    invoke-direct {p0, p1, p4}, LaN/D;->a(LaN/P;I)Lam/f;

    move-result-object v3

    .line 473
    if-eqz p3, :cond_69

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->n()Z

    move-result v0

    if-eqz v0, :cond_69

    .line 476
    new-instance v0, LaN/I;

    invoke-direct {v0, p1, v3}, LaN/I;-><init>(LaN/P;Lam/f;)V

    .line 478
    invoke-direct {p0, v0, p2}, LaN/D;->a(LaN/I;I)V

    .line 479
    invoke-direct {p0, v0}, LaN/D;->a(LaN/I;)V

    .line 481
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->c()V
    :try_end_5e
    .catchall {:try_start_2f .. :try_end_5e} :catchall_73

    .line 510
    :goto_5e
    const/4 v3, 0x0

    :try_start_5f
    invoke-virtual {p0, v3}, LaN/D;->b(Z)V

    .line 512
    monitor-exit v4
    :try_end_63
    .catchall {:try_start_5f .. :try_end_63} :catchall_79

    .line 533
    :goto_63
    invoke-virtual {v0, v1, v2}, LaN/I;->a(J)V

    .line 534
    return-object v0

    :cond_67
    move-object v0, v3

    .line 462
    goto :goto_39

    .line 484
    :cond_69
    :try_start_69
    new-instance v0, LaN/I;

    const/4 v5, 0x1

    invoke-direct {v0, p1, v3, v5}, LaN/I;-><init>(LaN/P;Lam/f;Z)V

    .line 491
    invoke-direct {p0, v0}, LaN/D;->a(LaN/I;)V
    :try_end_72
    .catchall {:try_start_69 .. :try_end_72} :catchall_73

    goto :goto_5e

    .line 510
    :catchall_73
    move-exception v0

    const/4 v1, 0x0

    :try_start_75
    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    throw v0

    .line 512
    :catchall_79
    move-exception v0

    monitor-exit v4
    :try_end_7b
    .catchall {:try_start_75 .. :try_end_7b} :catchall_79

    throw v0

    .line 498
    :cond_7c
    if-nez p3, :cond_81

    .line 499
    const-wide/16 v5, 0x4e20

    sub-long/2addr v1, v5

    .line 502
    :cond_81
    :try_start_81
    invoke-direct {p0, v0}, LaN/D;->a(LaN/I;)V

    .line 507
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->b()V
    :try_end_8c
    .catchall {:try_start_81 .. :try_end_8c} :catchall_73

    goto :goto_5e

    .line 517
    :cond_8d
    new-instance v0, LaN/I;

    invoke-direct {v0, p1, v3, v5}, LaN/I;-><init>(LaN/P;Lam/f;Z)V

    goto :goto_63

    .line 519
    :cond_93
    invoke-virtual {v0}, LaN/I;->e()Z

    move-result v3

    if-nez v3, :cond_b6

    invoke-virtual {v0}, LaN/I;->v()Z

    move-result v3

    if-nez v3, :cond_b6

    if-eqz p3, :cond_b6

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    invoke-virtual {v3}, Law/h;->n()Z

    move-result v3

    if-eqz v3, :cond_b6

    .line 524
    invoke-direct {p0, v0, p2}, LaN/D;->a(LaN/I;I)V

    .line 526
    invoke-static {v4}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->c()V

    goto :goto_63

    .line 529
    :cond_b6
    invoke-static {v4}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bE;->a()V

    goto :goto_63

    :cond_be
    move-wide v1, p5

    goto/16 :goto_22
.end method

.method public a(LaN/P;IZZ)LaN/I;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 381
    const-wide/high16 v5, -0x8000

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, LaN/D;->a(LaN/P;IZZJ)LaN/I;

    move-result-object v0

    return-object v0
.end method

.method a(LaN/P;IZZJ)LaN/I;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 410
    if-eqz p4, :cond_d

    const/4 v4, 0x2

    :goto_3
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, LaN/D;->a(LaN/P;IZIJ)LaN/I;

    move-result-object v0

    return-object v0

    :cond_d
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public a(LaN/P;)LaN/n;
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 542
    invoke-virtual {p0, p1, v0, v0, v0}, LaN/D;->a(LaN/P;IZZ)LaN/I;

    move-result-object v0

    invoke-virtual {v0}, LaN/I;->p()LaN/n;

    move-result-object v0

    return-object v0
.end method

.method a()Ljava/util/Hashtable;
    .registers 2

    .prologue
    .line 325
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    return-object v0
.end method

.method public a(LaN/P;ZJ)Ljava/util/Vector;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1064
    iget-object v0, p0, LaN/D;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 1065
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_e
    if-ltz v1, :cond_41

    .line 1066
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    .line 1067
    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v2

    if-eqz v2, :cond_39

    .line 1068
    const/16 v2, 0x8

    invoke-static {v2, p1}, LaN/P;->a(BLaN/P;)LaN/P;

    move-result-object v2

    .line 1069
    invoke-virtual {v0, v2, p2}, LaN/k;->a(LaN/P;Z)LaN/n;

    move-result-object v0

    .line 1073
    if-eqz v0, :cond_39

    .line 1074
    invoke-virtual {v0}, LaN/n;->h()Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 1075
    iget-object v2, p0, LaN/D;->g:Ljava/util/Vector;

    invoke-virtual {v0, p3, p4}, LaN/n;->b(J)Lam/f;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1065
    :cond_39
    :goto_39
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_e

    .line 1077
    :cond_3d
    invoke-virtual {v0, p3, p4}, LaN/n;->a(J)V

    goto :goto_39

    .line 1082
    :cond_41
    iget-object v0, p0, LaN/D;->g:Ljava/util/Vector;

    return-object v0
.end method

.method a(II)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1150
    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_51

    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0, p1, p2}, LaN/K;->a(II)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 1151
    iget-object v2, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v2

    .line 1155
    :try_start_f
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    .line 1156
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1157
    :cond_19
    :goto_19
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1158
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    .line 1159
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaN/I;

    .line 1160
    invoke-virtual {v1}, LaN/I;->e()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1161
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 1169
    :catchall_37
    move-exception v0

    monitor-exit v2
    :try_end_39
    .catchall {:try_start_f .. :try_end_39} :catchall_37

    throw v0

    .line 1166
    :cond_3a
    :try_start_3a
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    .line 1167
    iget-object v3, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v3, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3e

    .line 1169
    :cond_50
    monitor-exit v2
    :try_end_51
    .catchall {:try_start_3a .. :try_end_51} :catchall_37

    .line 1171
    :cond_51
    return-void
.end method

.method public a(LaN/k;)V
    .registers 3
    .parameter

    .prologue
    .line 1102
    if-eqz p1, :cond_12

    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 1103
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1104
    invoke-static {p1}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    .line 1106
    :cond_12
    return-void
.end method

.method public a(LaN/m;)V
    .registers 2
    .parameter

    .prologue
    .line 1097
    iput-object p1, p0, LaN/D;->f:LaN/m;

    .line 1098
    return-void
.end method

.method a(Lat/d;)V
    .registers 3
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, LaN/D;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317
    return-void
.end method

.method public a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 1189
    invoke-static {}, LaN/f;->f()V

    .line 1190
    invoke-direct {p0}, LaN/D;->t()V

    .line 1192
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v1

    .line 1194
    const/4 v0, 0x1

    :try_start_a
    invoke-direct {p0, v0}, LaN/D;->d(Z)V

    .line 1196
    iget-boolean v0, p0, LaN/D;->d:Z

    if-eqz v0, :cond_1d

    .line 1197
    const/16 v0, 0x61a8

    iput v0, p0, LaN/D;->h:I

    .line 1198
    invoke-direct {p0}, LaN/D;->s()V

    .line 1205
    :goto_18
    invoke-virtual {p0}, LaN/D;->d()V

    .line 1206
    monitor-exit v1

    .line 1207
    return-void

    .line 1200
    :cond_1d
    iget v0, p0, LaN/D;->h:I

    add-int/lit16 v0, v0, -0x1f40

    const/16 v2, 0x61a8

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LaN/D;->h:I

    .line 1202
    invoke-direct {p0}, LaN/D;->s()V

    goto :goto_18

    .line 1206
    :catchall_2d
    move-exception v0

    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_a .. :try_end_2f} :catchall_2d

    throw v0
.end method

.method b()V
    .registers 4

    .prologue
    .line 668
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v1

    .line 669
    :try_start_3
    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    .line 670
    :goto_9
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 671
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    invoke-interface {v0}, Lam/f;->d()V

    goto :goto_9

    .line 675
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0

    .line 674
    :cond_1c
    :try_start_1c
    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 675
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_1c .. :try_end_22} :catchall_19

    .line 676
    return-void
.end method

.method public b(LaN/k;)V
    .registers 3
    .parameter

    .prologue
    .line 1110
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1111
    invoke-static {p1}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    .line 1112
    return-void
.end method

.method b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 238
    iput-boolean p1, p0, LaN/D;->j:Z

    .line 239
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 749
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 750
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->f()V

    .line 749
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 752
    :cond_1a
    return-void
.end method

.method c(Z)V
    .registers 4
    .parameter

    .prologue
    .line 343
    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    .line 344
    invoke-direct {p0}, LaN/D;->v()V

    .line 345
    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_f

    .line 346
    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0, p1}, LaN/K;->a(Z)V

    .line 350
    :cond_f
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_18
    if-ltz v1, :cond_2c

    .line 351
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    .line 352
    invoke-virtual {v0}, LaN/k;->d()V

    .line 353
    invoke-static {v0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    .line 350
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_18

    .line 355
    :cond_2c
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 356
    return-void
.end method

.method d()V
    .registers 6

    .prologue
    .line 768
    invoke-virtual {p0}, LaN/D;->e()I

    move-result v0

    .line 769
    iget v1, p0, LaN/D;->h:I

    if-le v0, v1, :cond_3e

    .line 770
    iget-boolean v1, p0, LaN/D;->d:Z

    if-eqz v1, :cond_3f

    .line 772
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 775
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v1

    int-to-long v3, v0

    add-long/2addr v1, v3

    .line 777
    const-wide/32 v3, 0x9c40

    sub-long/2addr v1, v3

    long-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    .line 779
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    long-to-int v2, v2

    .line 781
    div-int/lit8 v2, v2, 0x3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 783
    const/16 v2, 0x61a8

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, LaN/D;->h:I

    .line 785
    invoke-direct {p0}, LaN/D;->s()V

    .line 788
    iget v1, p0, LaN/D;->h:I

    if-ge v0, v1, :cond_3f

    .line 794
    :cond_3e
    :goto_3e
    return-void

    .line 792
    :cond_3f
    invoke-direct {p0, v0}, LaN/D;->a(I)V

    goto :goto_3e
.end method

.method e()I
    .registers 5

    .prologue
    .line 835
    const/4 v0, 0x0

    .line 836
    iget-object v2, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v2

    .line 837
    :try_start_4
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v3

    move v1, v0

    :goto_b
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 838
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    .line 839
    invoke-virtual {v0}, LaN/I;->j()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 840
    goto :goto_b

    .line 841
    :cond_1e
    monitor-exit v2

    .line 842
    return v1

    .line 841
    :catchall_20
    move-exception v0

    monitor-exit v2
    :try_end_22
    .catchall {:try_start_4 .. :try_end_22} :catchall_20

    throw v0
.end method

.method public f()I
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 857
    .line 858
    iget-object v3, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v3

    .line 860
    const/4 v1, 0x1

    :try_start_5
    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    .line 861
    invoke-virtual {p0}, LaN/D;->h()I

    move-result v1

    .line 862
    const/16 v2, 0x30

    if-le v1, v2, :cond_36

    .line 866
    invoke-virtual {p0}, LaN/D;->i()[LaN/P;

    move-result-object v4

    move v2, v0

    .line 870
    :goto_15
    array-length v0, v4

    if-ge v2, v0, :cond_36

    const/16 v0, 0x18

    if-le v1, v0, :cond_36

    .line 871
    aget-object v0, v4, v2

    .line 872
    iget-object v5, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    .line 879
    invoke-virtual {v0}, LaN/I;->t()Z

    move-result v5

    if-eqz v5, :cond_45

    .line 880
    invoke-virtual {v0}, LaN/I;->k()V
    :try_end_2f
    .catchall {:try_start_5 .. :try_end_2f} :catchall_3c

    .line 881
    add-int/lit8 v0, v1, -0x1

    .line 870
    :goto_31
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_15

    .line 891
    :cond_36
    const/4 v0, 0x0

    :try_start_37
    invoke-virtual {p0, v0}, LaN/D;->b(Z)V

    .line 893
    monitor-exit v3

    .line 894
    return v1

    .line 891
    :catchall_3c
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LaN/D;->b(Z)V

    throw v0

    .line 893
    :catchall_42
    move-exception v0

    monitor-exit v3
    :try_end_44
    .catchall {:try_start_37 .. :try_end_44} :catchall_42

    throw v0

    :cond_45
    move v0, v1

    goto :goto_31
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 1333
    const/4 v3, 0x0

    .line 1334
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1336
    iget-object v5, p0, LaN/D;->a:Ljava/util/Hashtable;

    monitor-enter v5

    .line 1337
    :try_start_9
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 1339
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v7

    move v1, v2

    .line 1340
    :goto_14
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1341
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    .line 1342
    invoke-virtual {v0}, LaN/I;->x()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1343
    goto :goto_14

    .line 1344
    :cond_27
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v7, "images"

    invoke-direct {v0, v7, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1346
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "data"

    invoke-virtual {p0}, LaN/D;->e()I

    move-result v7

    invoke-direct {v0, v1, v7}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1347
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "mapCache"

    invoke-direct {v0, v1, v3, v6}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1351
    iget-object v0, p0, LaN/D;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v3

    move v1, v2

    .line 1352
    :goto_50
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_63

    .line 1353
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    .line 1354
    invoke-interface {v0}, Lam/f;->g()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1355
    goto :goto_50

    .line 1356
    :cond_63
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v3, "tempScaledImages"

    invoke-direct {v0, v3, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1358
    monitor-exit v5
    :try_end_6e
    .catchall {:try_start_9 .. :try_end_6e} :catchall_93

    .line 1360
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "layerServices"

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    move v1, v2

    move-object v3, v0

    .line 1361
    :goto_77
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_96

    .line 1362
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    .line 1363
    invoke-virtual {v0}, LaN/k;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v3

    .line 1361
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_77

    .line 1358
    :catchall_93
    move-exception v0

    :try_start_94
    monitor-exit v5
    :try_end_95
    .catchall {:try_start_94 .. :try_end_95} :catchall_93

    throw v0

    .line 1366
    :cond_96
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1368
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "MapService"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0
.end method

.method h()I
    .registers 4

    .prologue
    .line 903
    const/4 v0, 0x0

    .line 904
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    move v1, v0

    :cond_8
    :goto_8
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 905
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->t()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 906
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 909
    :cond_1e
    return v1
.end method

.method i()[LaN/P;
    .registers 10

    .prologue
    .line 939
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    .line 940
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    new-array v4, v0, [LaN/P;

    .line 941
    iget-object v0, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    new-array v5, v0, [J

    .line 942
    const/4 v0, 0x0

    .line 944
    iget-object v1, p0, LaN/D;->a:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v6

    move v1, v0

    .line 945
    :goto_24
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 946
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/P;

    aput-object v0, v4, v1

    .line 947
    aget-object v0, v4, v1

    aget-object v7, v4, v1

    invoke-direct {p0, v7}, LaN/D;->e(LaN/P;)J

    move-result-wide v7

    invoke-static {v0, v2, v3, v7, v8}, LaN/D;->a(LaN/P;JJ)J

    move-result-wide v7

    aput-wide v7, v5, v1

    .line 949
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_24

    .line 952
    :cond_44
    invoke-direct {p0, v5, v4}, LaN/D;->a([J[LaN/P;)V

    .line 954
    return-object v4
.end method

.method j()Z
    .registers 3

    .prologue
    .line 1021
    iget-object v0, p0, LaN/D;->k:LaN/G;

    if-eqz v0, :cond_12

    .line 1022
    iget-object v0, p0, LaN/D;->k:LaN/G;

    .line 1025
    const/4 v1, 0x0

    iput-object v1, p0, LaN/D;->k:LaN/G;

    .line 1026
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 1027
    const/4 v0, 0x1

    .line 1029
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public k()V
    .registers 4

    .prologue
    .line 1036
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_20

    .line 1037
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    .line 1038
    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1039
    invoke-virtual {v0}, LaN/k;->e()V

    .line 1036
    :cond_1c
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1042
    :cond_20
    return-void
.end method

.method public l()V
    .registers 3

    .prologue
    .line 1087
    iget-object v0, p0, LaN/D;->f:LaN/m;

    if-eqz v0, :cond_9

    .line 1088
    iget-object v0, p0, LaN/D;->f:LaN/m;

    invoke-interface {v0}, LaN/m;->c()V

    .line 1090
    :cond_9
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_12
    if-ltz v1, :cond_23

    .line 1091
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    invoke-virtual {v0}, LaN/k;->c()V

    .line 1090
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_12

    .line 1093
    :cond_23
    return-void
.end method

.method public m()Z
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1118
    move v1, v2

    :goto_2
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 1119
    iget-object v0, p0, LaN/D;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/k;

    .line 1120
    invoke-virtual {v0}, LaN/k;->h()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1121
    const/4 v2, 0x1

    .line 1124
    :cond_19
    return v2

    .line 1118
    :cond_1a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 1131
    iget v0, p0, LaN/D;->l:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method o()V
    .registers 3

    .prologue
    .line 1178
    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    iput-wide v0, p0, LaN/D;->p:J

    .line 1179
    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_f

    .line 1180
    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0}, LaN/K;->g()V

    .line 1182
    :cond_f
    iget-object v1, p0, LaN/D;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 1183
    :try_start_12
    iget-object v0, p0, LaN/D;->n:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1184
    monitor-exit v1

    .line 1185
    return-void

    .line 1184
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_12 .. :try_end_1b} :catchall_19

    throw v0
.end method

.method p()V
    .registers 1

    .prologue
    .line 1312
    invoke-direct {p0}, LaN/D;->v()V

    .line 1313
    return-void
.end method

.method q()V
    .registers 1

    .prologue
    .line 1319
    invoke-direct {p0}, LaN/D;->w()V

    .line 1320
    return-void
.end method

.method r()V
    .registers 2

    .prologue
    .line 1326
    iget-object v0, p0, LaN/D;->b:LaN/K;

    if-eqz v0, :cond_9

    .line 1327
    iget-object v0, p0, LaN/D;->b:LaN/K;

    invoke-interface {v0}, LaN/K;->h()V

    .line 1329
    :cond_9
    return-void
.end method

.method public run()V
    .registers 15

    .prologue
    const-wide/16 v12, 0xc29

    const-wide/16 v10, 0x835

    .line 1211
    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    add-long v2, v0, v10

    .line 1212
    invoke-static {}, LaN/D;->u()J

    move-result-wide v0

    add-long/2addr v0, v12

    .line 1214
    :cond_f
    :goto_f
    iget-boolean v4, p0, LaN/D;->c:Z

    if-nez v4, :cond_7a

    .line 1217
    :try_start_13
    iget-object v6, p0, LaN/D;->o:Ljava/lang/Object;

    monitor-enter v6
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_16} :catch_69
    .catch Ljava/lang/OutOfMemoryError; {:try_start_13 .. :try_end_16} :catch_75

    .line 1224
    :try_start_16
    invoke-static {}, LaN/D;->u()J

    move-result-wide v7

    .line 1225
    cmp-long v4, v2, v0

    if-gez v4, :cond_70

    move-wide v4, v2

    :goto_1f
    sub-long/2addr v4, v7

    .line 1227
    const-wide/16 v7, 0x0

    cmp-long v7, v4, v7

    if-lez v7, :cond_2b

    .line 1228
    iget-object v7, p0, LaN/D;->o:Ljava/lang/Object;

    invoke-virtual {v7, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_2b
    .catchall {:try_start_16 .. :try_end_2b} :catchall_72
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_2b} :catch_7d

    .line 1233
    :cond_2b
    :goto_2b
    :try_start_2b
    monitor-exit v6
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_72

    .line 1235
    :try_start_2c
    iget-boolean v4, p0, LaN/D;->c:Z

    if-nez v4, :cond_f

    .line 1236
    invoke-static {}, LaN/D;->u()J

    move-result-wide v4

    .line 1238
    cmp-long v6, v2, v4

    if-gez v6, :cond_3d

    .line 1239
    invoke-virtual {p0}, LaN/D;->d()V

    .line 1240
    add-long v2, v4, v10

    .line 1244
    :cond_3d
    cmp-long v6, v0, v4

    if-gez v6, :cond_47

    .line 1245
    const/4 v6, 0x0

    invoke-direct {p0, v6}, LaN/D;->d(Z)V

    .line 1246
    add-long v0, v4, v12

    .line 1254
    :cond_47
    iget-object v6, p0, LaN/D;->b:LaN/K;

    if-eqz v6, :cond_f

    iget-object v6, p0, LaN/D;->b:LaN/K;

    invoke-interface {v6}, LaN/K;->i()Z

    move-result v6

    if-nez v6, :cond_f

    iget-wide v6, p0, LaN/D;->p:J

    const-wide/16 v8, 0xfa0

    add-long/2addr v6, v8

    cmp-long v4, v6, v4

    if-gez v4, :cond_f

    .line 1257
    iget-object v5, p0, LaN/D;->n:Ljava/lang/Object;

    monitor-enter v5
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_5f} :catch_69
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2c .. :try_end_5f} :catch_75

    .line 1259
    :try_start_5f
    iget-object v4, p0, LaN/D;->n:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_64
    .catchall {:try_start_5f .. :try_end_64} :catchall_66
    .catch Ljava/lang/InterruptedException; {:try_start_5f .. :try_end_64} :catch_7b

    .line 1263
    :goto_64
    :try_start_64
    monitor-exit v5

    goto :goto_f

    :catchall_66
    move-exception v4

    monitor-exit v5
    :try_end_68
    .catchall {:try_start_64 .. :try_end_68} :catchall_66

    :try_start_68
    throw v4
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_68 .. :try_end_69} :catch_69
    .catch Ljava/lang/OutOfMemoryError; {:try_start_68 .. :try_end_69} :catch_75

    .line 1266
    :catch_69
    move-exception v4

    .line 1267
    const-string v5, "MapService BG"

    invoke-static {v5, v4}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_f

    :cond_70
    move-wide v4, v0

    .line 1225
    goto :goto_1f

    .line 1233
    :catchall_72
    move-exception v4

    :try_start_73
    monitor-exit v6
    :try_end_74
    .catchall {:try_start_73 .. :try_end_74} :catchall_72

    :try_start_74
    throw v4
    :try_end_75
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_75} :catch_69
    .catch Ljava/lang/OutOfMemoryError; {:try_start_74 .. :try_end_75} :catch_75

    .line 1268
    :catch_75
    move-exception v4

    .line 1270
    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_f

    .line 1275
    :cond_7a
    return-void

    .line 1260
    :catch_7b
    move-exception v4

    goto :goto_64

    .line 1230
    :catch_7d
    move-exception v4

    goto :goto_2b
.end method
