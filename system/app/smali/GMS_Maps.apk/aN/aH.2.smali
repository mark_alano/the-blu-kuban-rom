.class public LaN/aH;
.super LaN/aY;
.source "SourceFile"


# instance fields
.field protected final d:Lai/s;


# direct methods
.method public constructor <init>(LaN/i;Lai/s;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, LaN/aY;-><init>(LaN/i;)V

    .line 24
    iput-object p2, p0, LaN/aH;->d:Lai/s;

    .line 25
    return-void
.end method

.method private e(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aX()I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;)LaR/F;
    .registers 5
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, LaN/aH;->e(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 31
    new-instance v0, Lcom/google/googlenav/ui/aB;

    invoke-virtual {p0}, LaN/aH;->b()LaN/m;

    move-result-object v1

    iget-object v2, p0, LaN/aH;->d:Lai/s;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/googlenav/ui/aB;-><init>(Lcom/google/googlenav/ai;LaN/m;Lai/s;)V

    .line 36
    :goto_11
    return-object v0

    :cond_12
    invoke-super {p0, p1}, LaN/aY;->a(Lcom/google/googlenav/ai;)LaR/F;

    move-result-object v0

    goto :goto_11
.end method

.method protected a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/bi;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-direct {p0, p1}, LaN/aH;->e(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 74
    :goto_6
    return-void

    .line 72
    :cond_7
    invoke-super {p0, p1, p2}, LaN/aY;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/bi;)V

    goto :goto_6
.end method

.method protected a(Lcom/google/googlenav/ai;Ljava/util/Vector;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1}, LaN/aH;->e(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 64
    invoke-super {p0, p1, p2}, LaN/aY;->a(Lcom/google/googlenav/ai;Ljava/util/Vector;)V

    .line 66
    :cond_9
    return-void
.end method

.method protected b(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, LaN/aH;->e(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 54
    const/4 v0, 0x0

    .line 56
    :goto_7
    return-object v0

    :cond_8
    invoke-super {p0, p1}, LaN/aY;->b(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method
