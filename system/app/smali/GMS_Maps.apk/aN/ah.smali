.class public LaN/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/au;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private final b:Lcom/google/android/maps/driveabout/vector/dd;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lcom/google/android/maps/driveabout/vector/dd;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-boolean v0, p0, LaN/ah;->c:Z

    .line 21
    iput-boolean v0, p0, LaN/ah;->d:Z

    .line 24
    iput-object p1, p0, LaN/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 25
    iput-object p2, p0, LaN/ah;->b:Lcom/google/android/maps/driveabout/vector/dd;

    .line 26
    return-void
.end method

.method private declared-synchronized a()V
    .registers 4

    .prologue
    .line 93
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, LaN/ah;->c:Z

    if-eqz v0, :cond_f

    .line 94
    iget-object v0, p0, LaN/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/ah;->c:Z
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 97
    :cond_f
    monitor-exit p0

    return-void

    .line 93
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(LaN/i;)V
    .registers 2
    .parameter

    .prologue
    .line 39
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 32
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, LaN/ah;->c:Z

    iget-object v1, p0, LaN/ah;->b:Lcom/google/android/maps/driveabout/vector/dd;

    new-instance v2, Lo/u;

    invoke-direct {v2}, Lo/u;-><init>()V

    invoke-virtual {v2, p1}, Lo/u;->a(Ljava/lang/String;)Lo/u;

    move-result-object v2

    invoke-virtual {v2}, Lo/u;->a()Lo/t;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lo/ao;)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, LaN/ah;->c:Z
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1b

    .line 34
    monitor-exit p0

    return-void

    .line 32
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 62
    iput-boolean p1, p0, LaN/ah;->d:Z

    .line 63
    return-void
.end method

.method public b(LaN/i;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    iget-boolean v0, p0, LaN/ah;->d:Z

    if-eqz v0, :cond_5

    .line 55
    :cond_4
    :goto_4
    return-void

    .line 48
    :cond_5
    instance-of v0, p1, LaN/bj;

    if-eqz v0, :cond_4

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/ah;->a(Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, LaN/ah;->a()V

    goto :goto_4
.end method

.method public c(LaN/i;)V
    .registers 5
    .parameter

    .prologue
    .line 68
    instance-of v0, p1, LaN/bj;

    if-nez v0, :cond_5

    .line 89
    :cond_4
    :goto_4
    return-void

    .line 74
    :cond_5
    iget-object v0, p0, LaN/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->A()Lcom/google/android/maps/driveabout/vector/dd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dd;->m()Lcom/google/android/maps/driveabout/vector/di;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->a:Lcom/google/android/maps/driveabout/vector/di;

    if-ne v0, v1, :cond_4

    .line 78
    check-cast p1, LaN/bj;

    invoke-virtual {p1}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 80
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 81
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bT()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cf()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 83
    invoke-virtual {p0, v1}, LaN/ah;->a(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, LaN/ah;->a()V

    goto :goto_4
.end method
