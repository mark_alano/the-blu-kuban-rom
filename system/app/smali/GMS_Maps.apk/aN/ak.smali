.class public LaN/ak;
.super LaN/m;
.source "SourceFile"


# instance fields
.field private final B:Lcom/google/googlenav/layer/s;

.field private final C:Z

.field private final D:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lau/k;Lcom/google/googlenav/ai;ZBZ)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, LaN/m;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V

    .line 71
    invoke-virtual {p6}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 72
    check-cast p6, Lcom/google/googlenav/W;

    .line 73
    new-instance v0, Lcom/google/googlenav/layer/m;

    invoke-virtual {p6}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;)V

    .line 74
    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(LaN/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, LaN/ak;->B:Lcom/google/googlenav/layer/s;

    .line 78
    new-instance v1, Lcom/google/googlenav/T;

    invoke-direct {v1, v0, p5, p2, p3}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;Lau/o;Lau/p;Lau/u;)V

    .line 80
    invoke-virtual {v1, p6}, Lcom/google/googlenav/T;->a(Lcom/google/googlenav/W;)V

    .line 81
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/T;->a(I)V

    .line 83
    iput-object v1, p0, LaN/ak;->f:Lcom/google/googlenav/F;

    .line 91
    :goto_29
    iput-boolean p7, p0, LaN/ak;->C:Z

    .line 92
    iput-boolean p9, p0, LaN/ak;->D:Z

    .line 95
    invoke-virtual {p0, p8}, LaN/ak;->b(B)V

    .line 100
    invoke-virtual {p0}, LaN/ak;->af()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 101
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaN/ak;->e(Z)V

    .line 103
    :cond_3a
    return-void

    .line 87
    :cond_3b
    const/4 v0, 0x0

    iput-object v0, p0, LaN/ak;->B:Lcom/google/googlenav/layer/s;

    .line 88
    new-instance v0, Lcom/google/googlenav/bl;

    invoke-direct {v0, p6}, Lcom/google/googlenav/bl;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v0, p0, LaN/ak;->f:Lcom/google/googlenav/F;

    goto :goto_29
.end method


# virtual methods
.method protected O()Z
    .registers 2

    .prologue
    .line 236
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .registers 2

    .prologue
    .line 309
    invoke-super {p0}, LaN/m;->Z()V

    .line 310
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/v;->a(LaN/i;)V

    .line 311
    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 2
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, LaN/ak;->f:Lcom/google/googlenav/F;

    .line 108
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 255
    iget-boolean v0, p0, LaN/ak;->C:Z

    return v0
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method public aE()Z
    .registers 2

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 246
    const/4 v0, 0x0

    return v0
.end method

.method public aW()V
    .registers 4

    .prologue
    .line 165
    const/4 v1, 0x4

    iget-object v0, p0, LaN/ak;->f:Lcom/google/googlenav/F;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v1, v0}, LaN/ak;->a(ILcom/google/googlenav/ai;)V

    .line 167
    invoke-super {p0}, LaN/m;->aW()V

    .line 169
    iget-boolean v0, p0, LaN/ak;->C:Z

    if-eqz v0, :cond_2f

    invoke-virtual {p0}, LaN/ak;->aj()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 171
    iget-object v0, p0, LaN/ak;->d:Lau/u;

    invoke-virtual {v0}, Lau/u;->c()Lau/B;

    move-result-object v0

    iget-object v1, p0, LaN/ak;->d:Lau/u;

    invoke-virtual {v1}, Lau/u;->d()Lau/Y;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LaN/ak;->a(Lau/B;Lau/Y;)Lau/B;

    move-result-object v0

    .line 173
    iget-object v1, p0, LaN/ak;->d:Lau/u;

    invoke-virtual {v1, v0}, Lau/u;->b(Lau/B;)V

    .line 175
    :cond_2f
    return-void
.end method

.method protected am()V
    .registers 5

    .prologue
    .line 179
    invoke-virtual {p0}, LaN/ak;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 180
    if-eqz v0, :cond_25

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_25

    move-object v1, v0

    .line 181
    check-cast v1, Lcom/google/googlenav/W;

    .line 182
    const/16 v2, 0x43

    const-string v3, "o"

    invoke-virtual {v1}, Lcom/google/googlenav/W;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-object v1, p0, LaN/ak;->B:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Lad/g;

    .line 187
    :cond_25
    return-void
.end method

.method public ao()Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 274
    invoke-virtual {p0}, LaN/ak;->bu()Lcom/google/googlenav/ai;

    move-result-object v3

    .line 275
    if-nez v3, :cond_a

    move v0, v1

    .line 297
    :cond_9
    :goto_9
    return v0

    .line 281
    :cond_a
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->av()Z

    move-result v2

    if-nez v2, :cond_9

    .line 286
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->av()Lcom/google/googlenav/settings/e;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/settings/e;->c:Lcom/google/googlenav/settings/e;

    if-ne v2, v4, :cond_49

    move v2, v0

    .line 288
    :goto_1d
    if-eqz v2, :cond_35

    iget-object v2, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/v;->ad()Lcom/google/googlenav/aA;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/aA;->e()Z

    move-result v2

    if-eqz v2, :cond_35

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 293
    :cond_35
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->av()Lcom/google/googlenav/settings/e;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/settings/e;->d:Lcom/google/googlenav/settings/e;

    if-ne v2, v4, :cond_4b

    .line 294
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bq()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_9

    :cond_49
    move v2, v1

    .line 286
    goto :goto_1d

    :cond_4b
    move v0, v1

    .line 297
    goto :goto_9
.end method

.method protected aq()V
    .registers 1

    .prologue
    .line 192
    return-void
.end method

.method public au()Z
    .registers 2

    .prologue
    .line 224
    iget-boolean v0, p0, LaN/ak;->C:Z

    if-eqz v0, :cond_c

    invoke-super {p0}, LaN/m;->au()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public av()I
    .registers 2

    .prologue
    .line 241
    const/16 v0, 0xf

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public b()Lcom/google/googlenav/T;
    .registers 2

    .prologue
    .line 259
    iget-object v0, p0, LaN/ak;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/T;

    return-object v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 4
    .parameter

    .prologue
    .line 212
    iget-boolean v0, p0, LaN/ak;->C:Z

    if-eqz v0, :cond_1d

    move-object v0, p1

    .line 213
    check-cast v0, Lcom/google/googlenav/ai;

    .line 215
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bo()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bp()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->f()I

    move-result v0

    .line 217
    :goto_15
    rsub-int/lit8 v0, v0, 0x1

    .line 219
    :goto_17
    return v0

    .line 215
    :cond_18
    invoke-virtual {p0, p1}, LaN/ak;->f(Lcom/google/googlenav/E;)I

    move-result v0

    goto :goto_15

    .line 219
    :cond_1d
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public d(Lcom/google/googlenav/ui/u;)V
    .registers 2
    .parameter

    .prologue
    .line 270
    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/u;)V
    .registers 2
    .parameter

    .prologue
    .line 265
    return-void
.end method

.method protected f(Laa/a;)Z
    .registers 4
    .parameter

    .prologue
    .line 122
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_d

    .line 123
    invoke-virtual {p0}, LaN/ak;->h()V

    .line 124
    const/4 v0, 0x1

    .line 126
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public h()V
    .registers 2

    .prologue
    .line 131
    invoke-virtual {p0}, LaN/ak;->bz()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 159
    :goto_6
    return-void

    .line 137
    :cond_7
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 138
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 139
    invoke-virtual {p0, v0}, LaN/ak;->b(Lcom/google/googlenav/ui/wizard/A;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 140
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    goto :goto_6

    .line 142
    :cond_2d
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/v;->a(LaN/i;)V

    .line 143
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ag()V

    goto :goto_6

    .line 147
    :cond_38
    iget-boolean v0, p0, LaN/ak;->D:Z

    if-eqz v0, :cond_47

    .line 148
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/v;->a(LaN/i;)V

    .line 149
    iget-object v0, p0, LaN/ak;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->X()V

    goto :goto_6

    .line 151
    :cond_47
    invoke-virtual {p0}, LaN/ak;->ag()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 152
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/ak;->a(B)V

    goto :goto_6

    .line 155
    :cond_52
    invoke-virtual {p0}, LaN/ak;->n()V

    goto :goto_6
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, LaN/ak;->B:Lcom/google/googlenav/layer/s;

    if-eqz v0, :cond_e

    iget-object v0, p0, LaN/ak;->B:Lcom/google/googlenav/layer/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Lad/g;

    move-result-object v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected i()LaP/a;
    .registers 2

    .prologue
    .line 117
    new-instance v0, LaP/g;

    invoke-direct {v0, p0}, LaP/g;-><init>(LaN/i;)V

    return-object v0
.end method

.method protected l()V
    .registers 1

    .prologue
    .line 202
    return-void
.end method
