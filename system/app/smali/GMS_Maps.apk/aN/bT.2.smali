.class public LaN/bT;
.super LaN/m;
.source "SourceFile"

# interfaces
.implements Lau/m;


# static fields
.field public static final C:Lcom/google/googlenav/ab;

.field private static final O:Lcom/google/googlenav/ab;

.field private static P:Lcom/google/googlenav/ab;


# instance fields
.field B:Lcom/google/googlenav/ui/android/ah;

.field private D:Lae/y;

.field private E:Lcom/google/googlenav/ui/view/K;

.field private F:Lau/k;

.field private G:Z

.field private H:Landroid/widget/ArrayAdapter;

.field private final I:Landroid/widget/AdapterView$OnItemClickListener;

.field private final J:Landroid/os/Handler;

.field private K:Lcom/google/googlenav/layer/s;

.field private final L:Landroid/os/HandlerThread;

.field private M:Landroid/os/Handler;

.field private final N:Ljava/lang/Runnable;

.field private final Q:Ljava/util/ArrayList;

.field private R:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 1187
    new-instance v0, Lcom/google/googlenav/ab;

    const/4 v1, -0x2

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    sput-object v0, LaN/bT;->O:Lcom/google/googlenav/ab;

    .line 1190
    new-instance v0, Lcom/google/googlenav/ab;

    const/4 v1, -0x1

    const/16 v2, 0x5c7

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    sput-object v0, LaN/bT;->C:Lcom/google/googlenav/ab;

    .line 1213
    sget-object v0, LaN/bT;->O:Lcom/google/googlenav/ab;

    sput-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lau/k;Lcom/google/googlenav/F;Z)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 253
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LaN/m;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/F;)V

    .line 155
    new-instance v0, LaN/bU;

    invoke-direct {v0, p0}, LaN/bU;-><init>(LaN/bT;)V

    iput-object v0, p0, LaN/bT;->I:Landroid/widget/AdapterView$OnItemClickListener;

    .line 183
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LaN/bT;->J:Landroid/os/Handler;

    .line 189
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "VehicleRequestThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LaN/bT;->L:Landroid/os/HandlerThread;

    .line 204
    new-instance v0, LaN/bV;

    invoke-direct {v0, p0}, LaN/bV;-><init>(LaN/bT;)V

    iput-object v0, p0, LaN/bT;->N:Ljava/lang/Runnable;

    .line 1215
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    .line 254
    iput-object p5, p0, LaN/bT;->F:Lau/k;

    .line 255
    iput-boolean p7, p0, LaN/bT;->G:Z

    .line 256
    return-void
.end method

.method static synthetic a(LaN/bT;)Lae/y;
    .registers 2
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, LaN/bT;->bW()Lae/y;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;I)Landroid/view/View;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 424
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 426
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(IILjava/lang/Object;Lcom/google/googlenav/cq;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 722
    if-eqz p4, :cond_11

    .line 723
    iget-object v0, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->au()Lcom/google/googlenav/cd;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/googlenav/cd;->a(Lcom/google/googlenav/cq;)V

    .line 726
    :cond_11
    invoke-direct {p0}, LaN/bT;->bU()V

    .line 728
    invoke-virtual {p0, p2, p3}, LaN/bT;->a(ILjava/lang/Object;)V

    .line 729
    return-void
.end method

.method private a(ILjava/lang/Object;ILjava/lang/Object;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 711
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->f()I

    move-result v0

    if-ge p1, v0, :cond_c

    if-gez p1, :cond_d

    .line 718
    :cond_c
    :goto_c
    return-void

    .line 715
    :cond_d
    invoke-virtual {p0, p1}, LaN/bT;->b(I)V

    .line 716
    check-cast p2, Lcom/google/googlenav/cq;

    invoke-direct {p0, p1, p3, p4, p2}, LaN/bT;->a(IILjava/lang/Object;Lcom/google/googlenav/cq;)V

    goto :goto_c
.end method

.method private a(LaN/bF;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 756
    invoke-direct {p0}, LaN/bT;->bV()V

    .line 757
    new-instance v0, LaN/ce;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaN/ce;-><init>(LaN/bT;LaN/bU;)V

    .line 758
    iget-object v1, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/googlenav/ui/v;->a(LaN/bF;Lcom/google/googlenav/bY;Z)Lcom/google/googlenav/ca;

    move-result-object v2

    .line 760
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    const/16 v1, 0x4b8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/v;->a(Ljava/lang/String;Lad/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 762
    return-void
.end method

.method static synthetic a(LaN/bT;Lcom/google/googlenav/aa;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, LaN/bT;->b(Lcom/google/googlenav/aa;)V

    return-void
.end method

.method static synthetic a(LaN/bT;Lcom/google/googlenav/ab;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, LaN/bT;->b(Lcom/google/googlenav/ab;)V

    return-void
.end method

.method private a(Lae/y;II)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 874
    new-instance v0, Lcom/google/googlenav/aa;

    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bq;->d()Lcom/google/googlenav/ui/bq;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bq;->Q()Lcom/google/googlenav/ui/bp;

    move-result-object v5

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/aa;-><init>(Lcom/google/googlenav/Y;Lae/y;IILcom/google/googlenav/ui/bp;)V

    .line 877
    invoke-direct {p0, v0}, LaN/bT;->b(Lcom/google/googlenav/aa;)V

    .line 878
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 419
    const v0, 0x7f100463

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ah;

    iput-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    .line 420
    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    iget-object v1, p0, LaN/bT;->I:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0, v1}, LaN/bT;->a(Lcom/google/googlenav/ui/android/ah;Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 421
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/j;)V
    .registers 4
    .parameter

    .prologue
    .line 1266
    if-nez p0, :cond_3

    .line 1282
    :cond_2
    :goto_2
    return-void

    .line 1270
    :cond_3
    sget-object v0, LaN/bT;->O:Lcom/google/googlenav/ab;

    sget-object v1, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1274
    :try_start_d
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1275
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1276
    sget-object v2, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 1277
    sget-object v2, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1278
    const-string v1, "TRANSIT_VEHICLE_TYPE"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p0, v1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_32} :catch_33

    goto :goto_2

    .line 1279
    :catch_33
    move-exception v0

    goto :goto_2
.end method

.method static synthetic b(LaN/bT;)Lae/y;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LaN/bT;->D:Lae/y;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/aa;)V
    .registers 6
    .parameter

    .prologue
    .line 1428
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v0

    .line 1429
    sget-object v1, LaN/bT;->C:Lcom/google/googlenav/ab;

    sget-object v2, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 1430
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget-object v3, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v3}, Lcom/google/googlenav/ab;->a()I

    move-result v3

    aput v3, v1, v2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/aa;->a([I)V

    .line 1432
    :cond_1d
    invoke-virtual {v0, p1}, Lad/h;->c(Lad/g;)V

    .line 1433
    return-void
.end method

.method private b(Lcom/google/googlenav/ab;)V
    .registers 4
    .parameter

    .prologue
    .line 442
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, LaN/am;->e(Ljava/lang/String;)LaN/i;

    move-result-object v0

    check-cast v0, LaO/q;

    .line 444
    if-eqz v0, :cond_13

    .line 445
    invoke-virtual {v0, p1}, LaO/q;->a(Lcom/google/googlenav/ab;)V

    .line 447
    :cond_13
    return-void
.end method

.method public static bK()Lcom/google/googlenav/ab;
    .registers 2

    .prologue
    .line 1226
    sget-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    sget-object v1, LaN/bT;->O:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1227
    invoke-static {}, LaN/bT;->cb()Lcom/google/googlenav/ab;

    move-result-object v0

    sput-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    .line 1229
    :cond_10
    sget-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    return-object v0
.end method

.method public static bL()V
    .registers 1

    .prologue
    .line 1237
    sget-object v0, LaN/bT;->C:Lcom/google/googlenav/ab;

    sput-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    .line 1238
    return-void
.end method

.method private bO()V
    .registers 2

    .prologue
    .line 284
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 291
    :goto_a
    return-void

    .line 289
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/bT;->b(I)V

    .line 290
    invoke-virtual {p0}, LaN/bT;->an()Z

    goto :goto_a
.end method

.method private bP()V
    .registers 13

    .prologue
    const v10, 0x3f99999a

    const/4 v4, 0x0

    .line 299
    const/4 v0, 0x3

    iget-object v1, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 300
    if-gez v5, :cond_12

    .line 339
    :cond_11
    :goto_11
    return-void

    .line 303
    :cond_12
    iget-object v0, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v0}, Lau/u;->c()Lau/B;

    move-result-object v6

    .line 304
    invoke-virtual {v6}, Lau/B;->c()I

    move-result v7

    .line 305
    invoke-virtual {v6}, Lau/B;->e()I

    move-result v8

    move v3, v4

    move v2, v4

    move v1, v4

    .line 309
    :goto_23
    if-ge v3, v5, :cond_5d

    .line 310
    iget-object v0, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v3}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v9

    .line 311
    if-nez v9, :cond_37

    move v0, v2

    .line 309
    :goto_32
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_23

    .line 314
    :cond_37
    invoke-virtual {v9}, Lau/B;->c()I

    move-result v0

    sub-int v0, v7, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 315
    if-le v0, v1, :cond_8e

    .line 318
    :goto_43
    invoke-virtual {v9}, Lau/B;->e()I

    move-result v1

    sub-int v1, v8, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 319
    const v9, 0xaba9500

    if-le v1, v9, :cond_57

    .line 321
    const v9, 0x15752a00

    sub-int v1, v9, v1

    .line 323
    :cond_57
    if-le v1, v2, :cond_8b

    move v11, v1

    move v1, v0

    move v0, v11

    .line 324
    goto :goto_32

    .line 327
    :cond_5d
    if-nez v1, :cond_61

    if-eqz v2, :cond_11

    .line 331
    :cond_61
    mul-int/lit8 v0, v1, 0x2

    int-to-float v0, v0

    mul-float/2addr v0, v10

    float-to-int v0, v0

    .line 332
    mul-int/lit8 v1, v2, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v10

    float-to-int v1, v1

    .line 333
    iget-object v2, p0, LaN/bT;->d:Lau/u;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v3

    invoke-virtual {v2, v0, v1, v4, v3}, Lau/u;->a(IIII)Lau/Y;

    move-result-object v0

    .line 336
    iget-object v1, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v1}, Lau/u;->d()Lau/Y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lau/Y;->b(Lau/Y;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 337
    iget-object v1, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v1, v6, v0}, Lau/u;->d(Lau/B;Lau/Y;)V

    goto :goto_11

    :cond_8b
    move v1, v0

    move v0, v2

    goto :goto_32

    :cond_8e
    move v0, v1

    goto :goto_43
.end method

.method private bQ()V
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 363
    invoke-direct {p0}, LaN/bT;->bR()Z

    move-result v0

    if-nez v0, :cond_8

    .line 392
    :cond_7
    :goto_7
    return-void

    .line 367
    :cond_8
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 368
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    .line 369
    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->n()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0401d8

    invoke-direct {p0, v1, v2}, LaN/bT;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 371
    invoke-direct {p0, v1}, LaN/bT;->a(Landroid/view/View;)V

    .line 372
    invoke-direct {p0}, LaN/bT;->bS()Landroid/app/ActionBar$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/actionbar/a;->b(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_7

    .line 374
    :cond_2c
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_64

    .line 375
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    .line 376
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->o()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0401d9

    invoke-direct {p0, v0, v2}, LaN/bT;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    .line 378
    invoke-direct {p0, v2}, LaN/bT;->a(Landroid/view/View;)V

    .line 379
    const v0, 0x7f100009

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 380
    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 381
    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->c()Lcom/google/googlenav/actionbar/b;

    move-result-object v4

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    .line 383
    invoke-direct {p0}, LaN/bT;->bS()Landroid/app/ActionBar$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_7

    .line 384
    :cond_64
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 385
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->f()Landroid/view/View;

    move-result-object v0

    .line 386
    if-eqz v0, :cond_7

    .line 387
    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ah;

    iput-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    .line 388
    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    iget-object v1, p0, LaN/bT;->I:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0, v1}, LaN/bT;->a(Lcom/google/googlenav/ui/android/ah;Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 389
    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7
.end method

.method private bR()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 395
    const/4 v0, 0x0

    .line 396
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aq()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 398
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    .line 399
    const v2, 0x7f100463

    invoke-virtual {v0, v2}, Lcom/google/googlenav/actionbar/a;->a(I)Landroid/view/View;

    move-result-object v0

    .line 409
    :cond_17
    :goto_17
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3e

    :cond_1f
    const/4 v0, 0x1

    :goto_20
    move v1, v0

    :cond_21
    return v1

    .line 400
    :cond_22
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ap()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 401
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->f()Landroid/view/View;

    move-result-object v2

    .line 402
    if-eqz v2, :cond_17

    .line 403
    const v0, 0x7f100201

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_17

    :cond_3e
    move v0, v1

    .line 409
    goto :goto_20
.end method

.method private bS()Landroid/app/ActionBar$LayoutParams;
    .registers 4

    .prologue
    const/4 v2, -0x2

    .line 413
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    const/16 v1, 0x13

    invoke-direct {v0, v2, v2, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method private bT()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 454
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    .line 455
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 456
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->j()V

    .line 468
    :cond_12
    :goto_12
    return-void

    .line 457
    :cond_13
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 458
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->o()Landroid/content/Context;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 460
    const/high16 v2, 0x7f04

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 461
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 462
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->c()Lcom/google/googlenav/actionbar/b;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    .line 464
    invoke-direct {p0}, LaN/bT;->bS()Landroid/app/ActionBar$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_12

    .line 465
    :cond_44
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    if-eqz v0, :cond_12

    .line 466
    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_12
.end method

.method private bU()V
    .registers 3

    .prologue
    .line 732
    invoke-virtual {p0}, LaN/bT;->s()Lcom/google/googlenav/E;

    move-result-object v0

    .line 733
    if-eqz v0, :cond_16

    .line 734
    invoke-interface {v0}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v0

    .line 735
    if-eqz v0, :cond_16

    .line 736
    iget-object v1, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->R()V

    .line 737
    iget-object v1, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v1, v0}, Lau/u;->c(Lau/B;)V

    .line 740
    :cond_16
    return-void
.end method

.method private bV()V
    .registers 4

    .prologue
    .line 765
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {p0}, LaN/bT;->af()Z

    move-result v0

    if-eqz v0, :cond_18

    const/16 v0, 0x1f

    :goto_e
    iget-object v2, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v0, v2}, LaN/bT;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 769
    return-void

    .line 765
    :cond_18
    const/16 v0, 0x20

    goto :goto_e
.end method

.method private bW()Lae/y;
    .registers 3

    .prologue
    .line 854
    iget-object v0, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v0}, Lau/u;->c()Lau/B;

    move-result-object v0

    .line 855
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lae/y;->a(Lau/B;Lo/B;)Lae/y;

    move-result-object v0

    return-object v0
.end method

.method private bX()V
    .registers 4

    .prologue
    .line 862
    iget-object v0, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lae/y;

    move-result-object v0

    const/16 v1, 0x4e20

    const/16 v2, 0xa

    invoke-direct {p0, v0, v1, v2}, LaN/bT;->a(Lae/y;II)V

    .line 864
    return-void
.end method

.method private bY()V
    .registers 7

    .prologue
    .line 1043
    iget-object v0, p0, LaN/bT;->F:Lau/k;

    if-nez v0, :cond_5

    .line 1068
    :goto_4
    return-void

    .line 1048
    :cond_5
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 1050
    iget-object v0, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v0, p0}, Lau/k;->b(Lau/m;)V

    .line 1051
    iget-object v0, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v0}, Lau/k;->b()Z

    move-result v0

    if-nez v0, :cond_24

    .line 1052
    iget-object v0, p0, LaN/bT;->c:Lau/p;

    iget-object v1, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v0, v1}, Lau/p;->b(Lau/k;)V

    .line 1056
    :cond_24
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1057
    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 1058
    iget-object v1, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v1, p0}, Lau/k;->a(Lau/m;)V

    .line 1059
    iget-object v1, p0, LaN/bT;->c:Lau/p;

    iget-object v2, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v1, v2}, Lau/p;->a(Lau/k;)V

    .line 1060
    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(LaN/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, LaN/bT;->K:Lcom/google/googlenav/layer/s;

    .line 1061
    invoke-virtual {p0}, LaN/bT;->aP()V

    .line 1066
    :goto_48
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, LaN/bT;->F:Lau/k;

    iget-object v4, p0, LaN/bT;->c:Lau/p;

    iget-object v5, p0, LaN/bT;->d:Lau/u;

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;Lau/o;Lau/p;Lau/u;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    goto :goto_4

    .line 1063
    :cond_5b
    invoke-direct {p0}, LaN/bT;->cc()V

    goto :goto_48
.end method

.method private bZ()V
    .registers 3

    .prologue
    .line 1173
    iget-object v0, p0, LaN/bT;->M:Landroid/os/Handler;

    if-nez v0, :cond_5

    .line 1179
    :goto_4
    return-void

    .line 1177
    :cond_5
    iget-object v0, p0, LaN/bT;->M:Landroid/os/Handler;

    iget-object v1, p0, LaN/bT;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1178
    iget-object v0, p0, LaN/bT;->M:Landroid/os/Handler;

    iget-object v1, p0, LaN/bT;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4
.end method

.method static synthetic c(LaN/bT;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/ct;)V
    .registers 5
    .parameter

    .prologue
    .line 538
    invoke-virtual {p1}, Lcom/google/googlenav/ct;->m()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 539
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {p1}, Lcom/google/googlenav/ct;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/ct;->n()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/v;->a(Ljava/lang/String;I)V

    .line 545
    :goto_13
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaN/bT;->h(I)V

    .line 546
    const/4 v1, 0x2

    invoke-virtual {p0}, LaN/bT;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v1, v0}, LaN/bT;->a(ILcom/google/googlenav/ai;)V

    .line 550
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LaN/bT;->f(I)V

    .line 551
    return-void

    .line 541
    :cond_27
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {p1}, Lcom/google/googlenav/ct;->a()Lau/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->c(Lau/B;)V

    goto :goto_13
.end method

.method private ca()V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 1194
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v2

    .line 1196
    iget-object v1, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1197
    iget-object v1, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    sget-object v3, LaN/bT;->C:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1198
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_1a
    if-ge v1, v3, :cond_28

    .line 1199
    iget-object v4, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1198
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    .line 1201
    :cond_28
    iget-object v1, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_47

    .line 1202
    iget-object v1, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1203
    iget-object v1, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_37
    if-ge v0, v1, :cond_47

    .line 1204
    iget-object v2, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1203
    add-int/lit8 v0, v0, 0x1

    goto :goto_37

    .line 1207
    :cond_47
    return-void
.end method

.method private static cb()Lcom/google/googlenav/ab;
    .registers 4

    .prologue
    .line 1247
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 1248
    if-eqz v0, :cond_12

    const-string v1, "TRANSIT_VEHICLE_TYPE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_15

    .line 1249
    :cond_12
    sget-object v0, LaN/bT;->C:Lcom/google/googlenav/ab;

    .line 1259
    :cond_14
    :goto_14
    return-object v0

    .line 1253
    :cond_15
    :try_start_15
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const-string v3, "TRANSIT_VEHICLE_TYPE"

    invoke-interface {v0, v3}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1255
    new-instance v0, Lcom/google/googlenav/ab;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_32} :catch_3d

    .line 1259
    :goto_32
    sget-object v1, LaN/bT;->O:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    sget-object v0, LaN/bT;->C:Lcom/google/googlenav/ab;

    goto :goto_14

    .line 1256
    :catch_3d
    move-exception v0

    .line 1257
    sget-object v0, LaN/bT;->C:Lcom/google/googlenav/ab;

    goto :goto_32
.end method

.method private cc()V
    .registers 3

    .prologue
    .line 1563
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 1564
    invoke-static {p0}, LaN/am;->m(LaN/i;)Ljava/lang/String;

    move-result-object v1

    .line 1565
    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 1566
    return-void
.end method

.method static synthetic d(LaN/bT;)V
    .registers 1
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, LaN/bT;->ca()V

    return-void
.end method

.method static synthetic e(LaN/bT;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LaN/bT;->J:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(LaN/bT;)V
    .registers 1
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, LaN/bT;->bZ()V

    return-void
.end method

.method private j(I)V
    .registers 5
    .parameter

    .prologue
    .line 683
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-nez v0, :cond_32

    .line 684
    invoke-virtual {p0}, LaN/bT;->ae()Z

    move-result v0

    if-eqz v0, :cond_2f

    const/16 v0, 0x22

    :goto_12
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LaN/bT;->b(ILjava/lang/Object;)V

    .line 697
    :goto_19
    iget-object v0, p0, LaN/bT;->d:Lau/u;

    iget-object v1, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v1}, Lau/u;->c()Lau/B;

    move-result-object v1

    iget-object v2, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v2}, Lau/u;->d()Lau/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LaN/bT;->a(Lau/B;Lau/Y;)Lau/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lau/u;->b(Lau/B;)V

    .line 699
    return-void

    .line 684
    :cond_2f
    const/16 v0, 0x21

    goto :goto_12

    .line 691
    :cond_32
    invoke-virtual {p0}, LaN/bT;->an()Z

    goto :goto_19
.end method

.method private l(Lcom/google/googlenav/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 933
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, LaN/am;->e(Ljava/lang/String;)LaN/i;

    move-result-object v0

    check-cast v0, LaO/q;

    .line 935
    if-eqz v0, :cond_16

    .line 937
    if-nez p1, :cond_17

    const/4 v1, 0x0

    :goto_13
    invoke-virtual {v0, v1}, LaO/q;->b(Ljava/lang/String;)V

    .line 941
    :cond_16
    return-void

    .line 937
    :cond_17
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_24

    check-cast p1, Lcom/google/googlenav/W;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_13

    :cond_24
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bT()Ljava/lang/String;

    move-result-object v1

    goto :goto_13
.end method


# virtual methods
.method protected O()Z
    .registers 2

    .prologue
    .line 1072
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 1090
    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1343
    invoke-super {p0}, LaN/m;->X()Z

    .line 1345
    iget-boolean v1, p0, LaN/bT;->R:Z

    if-eqz v1, :cond_22

    .line 1346
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->n()V

    .line 1349
    iput-boolean v0, p0, LaN/bT;->R:Z

    .line 1352
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 1353
    invoke-virtual {p0}, LaN/bT;->Z()V

    .line 1355
    :cond_1e
    invoke-virtual {p0}, LaN/bT;->R()V

    .line 1356
    const/4 v0, 0x1

    .line 1358
    :cond_22
    return v0
.end method

.method public Z()V
    .registers 2

    .prologue
    .line 928
    invoke-super {p0}, LaN/m;->Z()V

    .line 929
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->d(LaN/i;)V

    .line 930
    return-void
.end method

.method public a()Landroid/widget/AdapterView$OnItemClickListener;
    .registers 2

    .prologue
    .line 450
    iget-object v0, p0, LaN/bT;->I:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method public a(Lae/y;)V
    .registers 2
    .parameter

    .prologue
    .line 811
    iput-object p1, p0, LaN/bT;->D:Lae/y;

    .line 812
    return-void
.end method

.method public a(Lae/y;Lae/y;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1328
    invoke-virtual {p0, p2}, LaN/bT;->a(Lae/y;)V

    .line 1330
    if-nez p3, :cond_9

    .line 1331
    invoke-static {p1}, Lcom/google/googlenav/ui/bD;->c(Lae/y;)Ljava/lang/String;

    move-result-object p3

    .line 1333
    :cond_9
    new-instance v2, Lcom/google/googlenav/aa;

    invoke-static {}, Lcom/google/googlenav/ui/bq;->d()Lcom/google/googlenav/ui/bq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bq;->Q()Lcom/google/googlenav/ui/bp;

    move-result-object v0

    invoke-direct {v2, p1, p3, v0}, Lcom/google/googlenav/aa;-><init>(Lae/y;Ljava/lang/String;Lcom/google/googlenav/ui/bp;)V

    .line 1335
    invoke-virtual {v2, p2}, Lcom/google/googlenav/aa;->a(Lae/y;)V

    .line 1336
    invoke-direct {p0, v2}, LaN/bT;->b(Lcom/google/googlenav/aa;)V

    .line 1337
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    const/16 v1, 0x4b9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/v;->a(Ljava/lang/String;Lad/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 1339
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 1299
    invoke-super {p0, p1}, LaN/m;->a(Landroid/content/res/Configuration;)V

    .line 1300
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    if-eqz v0, :cond_14

    .line 1301
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/aX;

    if-eqz v0, :cond_14

    .line 1302
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->h()V

    .line 1305
    :cond_14
    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    if-eqz v0, :cond_25

    .line 1308
    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ah;->b()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1309
    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ah;->a()V

    .line 1312
    :cond_25
    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 597
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    .line 598
    iput-object p1, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    .line 599
    check-cast p1, Lcom/google/googlenav/Y;

    .line 600
    invoke-virtual {p1, v0}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    .line 601
    invoke-virtual {p1}, Lcom/google/googlenav/Y;->g()Lae/y;

    move-result-object v0

    iput-object v0, p0, LaN/bT;->D:Lae/y;

    .line 603
    invoke-direct {p0}, LaN/bT;->ca()V

    .line 604
    return-void
.end method

.method public a(Lcom/google/googlenav/aa;)V
    .registers 5
    .parameter

    .prologue
    .line 995
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->l()Lcom/google/googlenav/Y;

    move-result-object v0

    .line 997
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->i()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 998
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    .line 999
    invoke-virtual {v1}, Lcom/google/googlenav/Y;->d()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(B)V

    .line 1000
    invoke-virtual {p0, v0}, LaN/bT;->b(Lcom/google/googlenav/F;)V

    .line 1002
    invoke-virtual {p0}, LaN/bT;->ae()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1003
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->l()V

    .line 1040
    :cond_25
    :goto_25
    return-void

    .line 1006
    :cond_26
    invoke-virtual {v0}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5b

    .line 1007
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5b

    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->b()Lae/y;

    move-result-object v1

    invoke-virtual {v1}, Lae/y;->f()Lau/B;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lae/y;

    move-result-object v2

    invoke-virtual {v2}, Lae/y;->f()Lau/B;

    move-result-object v2

    invoke-virtual {v1, v2}, Lau/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 1010
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(Ljava/lang/String;)V

    .line 1013
    :cond_5b
    invoke-virtual {p0, v0}, LaN/bT;->b(Lcom/google/googlenav/F;)V

    .line 1014
    invoke-direct {p0}, LaN/bT;->bY()V

    .line 1015
    invoke-direct {p0}, LaN/bT;->bP()V

    .line 1016
    invoke-direct {p0}, LaN/bT;->bO()V

    .line 1020
    iget-object v0, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_9c

    iget-object v0, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    if-eqz v0, :cond_9c

    .line 1021
    iget-object v0, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    sget-object v1, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1022
    if-ltz v0, :cond_7e

    .line 1023
    iget-object v1, p0, LaN/bT;->B:Lcom/google/googlenav/ui/android/ah;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/android/ah;->setSelection(I)V

    .line 1026
    :cond_7e
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 1027
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->f()Landroid/view/View;

    move-result-object v0

    .line 1028
    if-eqz v0, :cond_9c

    .line 1029
    const v1, 0x7f1001ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1030
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1034
    :cond_9c
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/aX;

    if-eqz v0, :cond_25

    .line 1035
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LaN/bT;->b(B)V

    .line 1036
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->l()V

    .line 1037
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->m()V

    goto/16 :goto_25
.end method

.method public a(Lcom/google/googlenav/ab;)V
    .registers 8
    .parameter

    .prologue
    .line 430
    sget-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 439
    :goto_8
    return-void

    .line 433
    :cond_9
    sput-object p1, LaN/bT;->P:Lcom/google/googlenav/ab;

    .line 434
    invoke-virtual {p0}, LaN/bT;->bH()V

    .line 435
    const/16 v0, 0x73

    const-string v1, "f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/ab;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method public a(Lcom/google/googlenav/ct;)V
    .registers 8
    .parameter

    .prologue
    .line 494
    invoke-virtual {p1}, Lcom/google/googlenav/ct;->au()Lcom/google/googlenav/cd;

    move-result-object v2

    .line 495
    if-nez v2, :cond_7

    .line 531
    :goto_6
    return-void

    .line 500
    :cond_7
    new-instance v0, Lcom/google/googlenav/layer/p;

    const-string v1, "LayerTransit"

    invoke-virtual {v2}, Lcom/google/googlenav/cd;->h()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-instance v5, LaN/bX;

    invoke-direct {v5, p0, p1}, LaN/bX;-><init>(LaN/bT;Lcom/google/googlenav/ct;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/layer/p;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/googlenav/layer/q;)V

    .line 530
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lad/h;->c(Lad/g;)V

    goto :goto_6
.end method

.method public a(Lcom/google/googlenav/ui/android/ah;Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1122
    iget-object v0, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_1c

    .line 1124
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 1125
    const v0, 0x1090009

    .line 1129
    :goto_11
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/google/googlenav/ui/bq;->e()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    .line 1132
    :cond_1c
    iget-object v0, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1133
    const/4 v0, 0x0

    iget-object v1, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_28
    if-ge v0, v1, :cond_3c

    .line 1134
    iget-object v2, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, LaN/bT;->Q:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1133
    add-int/lit8 v0, v0, 0x1

    goto :goto_28

    .line 1127
    :cond_38
    const v0, 0x7f0401e9

    goto :goto_11

    .line 1137
    :cond_3c
    new-instance v0, LaN/cb;

    invoke-direct {v0, p0}, LaN/cb;-><init>(LaN/bT;)V

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ah;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1158
    iget-object v0, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ah;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1159
    iget-object v0, p0, LaN/bT;->H:Landroid/widget/ArrayAdapter;

    sget-object v1, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 1160
    const/4 v1, -0x1

    if-eq v0, v1, :cond_57

    .line 1161
    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ah;->setSelection(I)V

    .line 1163
    :cond_57
    invoke-interface {p1, p2}, Lcom/google/googlenav/ui/android/ah;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1164
    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .registers 5
    .parameter

    .prologue
    .line 1547
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0}, LaN/am;->x()LaN/bT;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 1548
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1549
    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1551
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v0

    .line 1552
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1553
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    .line 1554
    invoke-virtual {v0}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1555
    invoke-virtual {v0}, Lcom/google/googlenav/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    goto :goto_2e

    .line 1558
    :cond_49
    invoke-direct {p0}, LaN/bT;->cc()V

    .line 1560
    :cond_4c
    return-void
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1470
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1471
    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1472
    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/m;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1474
    :cond_15
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 608
    sparse-switch p1, :sswitch_data_b8

    .line 672
    :cond_6
    invoke-super {p0, p1, p2, p3}, LaN/m;->a(IILjava/lang/Object;)Z

    move-result v1

    :goto_a
    return v1

    .line 610
    :sswitch_b
    invoke-virtual {p0}, LaN/bT;->f()V

    goto :goto_a

    .line 613
    :sswitch_f
    iget-object v0, p0, LaN/bT;->D:Lae/y;

    if-eqz v0, :cond_1f

    iget-object v0, p0, LaN/bT;->D:Lae/y;

    invoke-virtual {v0}, Lae/y;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 614
    :cond_1f
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lae/y;

    move-result-object v0

    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v4, v2}, LaN/bT;->a(Lae/y;Lae/y;Ljava/lang/String;)V

    goto :goto_a

    .line 617
    :cond_33
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    iget-object v3, p0, LaN/bT;->D:Lae/y;

    invoke-virtual {v3}, Lae/y;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->i(Z)Lcom/google/googlenav/bg;

    move-result-object v2

    new-instance v3, LaN/bZ;

    invoke-direct {v3, p0}, LaN/bZ;-><init>(LaN/bT;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/ba;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/bf;)V

    goto :goto_a

    .line 633
    :sswitch_59
    iget-object v2, p0, LaN/bT;->D:Lae/y;

    if-nez v2, :cond_5e

    move v0, v1

    .line 634
    :cond_5e
    check-cast p3, LaN/bF;

    invoke-direct {p0, p3, v0}, LaN/bT;->a(LaN/bF;Z)V

    goto :goto_a

    .line 637
    :sswitch_64
    invoke-direct {p0}, LaN/bT;->bX()V

    goto :goto_a

    .line 641
    :sswitch_68
    invoke-direct {p0}, LaN/bT;->bV()V

    .line 642
    invoke-virtual {p0, v4}, LaN/bT;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_a

    .line 646
    :sswitch_6f
    iget-object v2, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7e

    .line 647
    const/16 v0, 0x22

    .line 651
    :cond_7a
    :goto_7a
    invoke-direct {p0, p2, p3, v0, v4}, LaN/bT;->a(ILjava/lang/Object;ILjava/lang/Object;)V

    goto :goto_a

    .line 648
    :cond_7e
    iget-object v2, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    if-ne v2, v1, :cond_7a

    .line 649
    const/16 v0, 0x23

    goto :goto_7a

    .line 654
    :sswitch_89
    if-ltz p2, :cond_8e

    .line 655
    invoke-virtual {p0, p2}, LaN/bT;->b(I)V

    .line 657
    :cond_8e
    iget-object v0, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-direct {p0, v0}, LaN/bT;->j(I)V

    goto/16 :goto_a

    .line 660
    :sswitch_99
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 661
    const-string v0, "Street View"

    invoke-static {v0}, LQ/a;->b(Ljava/lang/String;)V

    .line 663
    :cond_a4
    invoke-virtual {p0}, LaN/bT;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 664
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v2

    if-nez v2, :cond_6

    .line 668
    check-cast v0, Lcom/google/googlenav/ct;

    invoke-direct {p0, v0}, LaN/bT;->c(Lcom/google/googlenav/ct;)V

    goto/16 :goto_a

    .line 608
    nop

    :sswitch_data_b8
    .sparse-switch
        0x1 -> :sswitch_89
        0xd5 -> :sswitch_b
        0x258 -> :sswitch_99
        0x25b -> :sswitch_68
        0x262 -> :sswitch_68
        0x264 -> :sswitch_f
        0xfa1 -> :sswitch_6f
        0xfa3 -> :sswitch_64
        0xfa4 -> :sswitch_59
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/J;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 1095
    iget-object v1, p0, LaN/bT;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_4b

    iget-object v1, p0, LaN/bT;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v1, :cond_4b

    .line 1097
    iget-object v1, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(I)V

    .line 1099
    const/16 v1, 0x73

    const-string v2, "m"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "a=b"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/Y;->c()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v3}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1104
    const/16 v1, 0x23

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LaN/bT;->a(ILjava/lang/Object;)V

    .line 1107
    :goto_4a
    return v0

    :cond_4b
    invoke-super {p0, p1}, LaN/m;->a(Lcom/google/googlenav/ui/view/J;)Z

    move-result v0

    goto :goto_4a
.end method

.method protected a(Ljava/io/DataInput;)Z
    .registers 8
    .parameter

    .prologue
    .line 1575
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dX;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1577
    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1578
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/layer/m;)V

    .line 1579
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v1

    .line 1580
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1581
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 1582
    const/4 v0, 0x0

    :goto_22
    if-ge v0, v2, :cond_37

    .line 1583
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v3

    .line 1584
    invoke-interface {p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 1585
    new-instance v5, Lcom/google/googlenav/ab;

    invoke-direct {v5, v3, v4}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1582
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 1587
    :cond_37
    const/4 v0, 0x1

    return v0
.end method

.method public aB()Z
    .registers 2

    .prologue
    .line 1539
    const/4 v0, 0x1

    return v0
.end method

.method public aG()I
    .registers 2

    .prologue
    .line 979
    const v0, 0x7f02021c

    return v0
.end method

.method public aH()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 984
    const/16 v0, 0x5c6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1077
    const/16 v0, 0x2f7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1592
    const/16 v0, 0x23a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 1533
    const/4 v0, 0x1

    return v0
.end method

.method protected aT()Z
    .registers 7

    .prologue
    .line 260
    invoke-static {}, LaN/bT;->bK()Lcom/google/googlenav/ab;

    move-result-object v0

    sput-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    .line 261
    sget-object v0, LaN/bT;->P:Lcom/google/googlenav/ab;

    invoke-direct {p0, v0}, LaN/bT;->b(Lcom/google/googlenav/ab;)V

    .line 263
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 266
    iget-object v1, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v1, p0}, Lau/k;->a(Lau/m;)V

    .line 267
    iget-object v1, p0, LaN/bT;->c:Lau/p;

    iget-object v2, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v1, v2}, Lau/p;->a(Lau/k;)V

    .line 268
    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(LaN/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, LaN/bT;->K:Lcom/google/googlenav/layer/s;

    .line 270
    :cond_2c
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, LaN/bT;->F:Lau/k;

    iget-object v4, p0, LaN/bT;->c:Lau/p;

    iget-object v5, p0, LaN/bT;->d:Lau/u;

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;Lau/o;Lau/p;Lau/u;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    .line 273
    iget-boolean v0, p0, LaN/bT;->G:Z

    if-eqz v0, :cond_45

    .line 274
    invoke-direct {p0}, LaN/bT;->bP()V

    .line 277
    :cond_45
    iget-object v0, p0, LaN/bT;->L:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 278
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LaN/bT;->L:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LaN/bT;->M:Landroid/os/Handler;

    .line 280
    invoke-super {p0}, LaN/m;->aT()Z

    move-result v0

    return v0
.end method

.method public aU()V
    .registers 3

    .prologue
    .line 343
    iget-object v0, p0, LaN/bT;->M:Landroid/os/Handler;

    if-eqz v0, :cond_12

    .line 344
    iget-object v0, p0, LaN/bT;->M:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_f

    .line 346
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 348
    :cond_f
    const/4 v0, 0x0

    iput-object v0, p0, LaN/bT;->M:Landroid/os/Handler;

    .line 351
    :cond_12
    invoke-direct {p0}, LaN/bT;->bT()V

    .line 352
    sget-object v0, LaN/bT;->C:Lcom/google/googlenav/ab;

    invoke-direct {p0, v0}, LaN/bT;->b(Lcom/google/googlenav/ab;)V

    .line 354
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 356
    iget-object v0, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v0, p0}, Lau/k;->b(Lau/m;)V

    .line 357
    iget-object v0, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v0}, Lau/k;->b()Z

    move-result v0

    if-nez v0, :cond_39

    .line 358
    iget-object v0, p0, LaN/bT;->c:Lau/p;

    iget-object v1, p0, LaN/bT;->F:Lau/k;

    invoke-virtual {v0, v1}, Lau/p;->b(Lau/k;)V

    .line 360
    :cond_39
    return-void
.end method

.method public aW()V
    .registers 2

    .prologue
    .line 949
    invoke-super {p0}, LaN/m;->aW()V

    .line 952
    invoke-direct {p0}, LaN/bT;->bQ()V

    .line 954
    iget-boolean v0, p0, LaN/bT;->G:Z

    if-eqz v0, :cond_10

    .line 955
    invoke-direct {p0}, LaN/bT;->bO()V

    .line 956
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/bT;->G:Z

    .line 958
    :cond_10
    return-void
.end method

.method protected am()V
    .registers 3

    .prologue
    .line 962
    invoke-virtual {p0}, LaN/bT;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 963
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-nez v1, :cond_25

    .line 964
    check-cast v0, Lcom/google/googlenav/ct;

    .line 965
    invoke-virtual {v0}, Lcom/google/googlenav/ct;->au()Lcom/google/googlenav/cd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cd;->f()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 967
    invoke-virtual {p0, v0}, LaN/bT;->b(Lcom/google/googlenav/ct;)V

    .line 969
    :cond_1b
    invoke-virtual {v0}, Lcom/google/googlenav/ct;->j()Z

    move-result v1

    if-nez v1, :cond_24

    .line 970
    invoke-virtual {p0, v0}, LaN/bT;->a(Lcom/google/googlenav/ct;)V

    .line 975
    :cond_24
    :goto_24
    return-void

    .line 973
    :cond_25
    iget-object v1, p0, LaN/bT;->K:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Lad/g;

    goto :goto_24
.end method

.method protected aq()V
    .registers 2

    .prologue
    .line 477
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/aX;-><init>(LaN/bT;)V

    iput-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    .line 478
    return-void
.end method

.method public synthetic ar()Lcom/google/googlenav/F;
    .registers 2

    .prologue
    .line 104
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    return-object v0
.end method

.method public av()I
    .registers 2

    .prologue
    .line 803
    const/16 v0, 0x17

    return v0
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .registers 2

    .prologue
    .line 1465
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 921
    invoke-super {p0, p1}, LaN/m;->b(I)V

    .line 922
    iget-object v0, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-direct {p0, v0}, LaN/bT;->l(Lcom/google/googlenav/ai;)V

    .line 923
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->d(LaN/i;)V

    .line 924
    return-void
.end method

.method b(Lcom/google/googlenav/ct;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1498
    invoke-virtual {p1}, Lcom/google/googlenav/ct;->au()Lcom/google/googlenav/cd;

    move-result-object v0

    .line 1499
    invoke-virtual {v0}, Lcom/google/googlenav/cd;->e()Z

    move-result v1

    .line 1500
    invoke-virtual {v0}, Lcom/google/googlenav/cd;->f()Z

    move-result v2

    .line 1502
    invoke-virtual {v0, v3}, Lcom/google/googlenav/cd;->a(Z)V

    .line 1503
    invoke-virtual {v0, v3}, Lcom/google/googlenav/cd;->b(Z)V

    .line 1504
    new-instance v3, Lcom/google/googlenav/cl;

    new-instance v4, LaN/cc;

    invoke-direct {v4, p0, v0, v1, v2}, LaN/cc;-><init>(LaN/bT;Lcom/google/googlenav/cd;ZZ)V

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/cl;-><init>(Lcom/google/googlenav/cd;Lcom/google/googlenav/cm;)V

    .line 1521
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v0

    .line 1522
    invoke-virtual {v0, v3}, Lad/h;->c(Lad/g;)V

    .line 1523
    return-void
.end method

.method public bF()Lcom/google/googlenav/ui/view/K;
    .registers 2

    .prologue
    .line 819
    iget-object v0, p0, LaN/bT;->E:Lcom/google/googlenav/ui/view/K;

    if-nez v0, :cond_b

    .line 820
    new-instance v0, LaN/ca;

    invoke-direct {v0, p0}, LaN/ca;-><init>(LaN/bT;)V

    iput-object v0, p0, LaN/bT;->E:Lcom/google/googlenav/ui/view/K;

    .line 834
    :cond_b
    iget-object v0, p0, LaN/bT;->E:Lcom/google/googlenav/ui/view/K;

    return-object v0
.end method

.method public bG()Z
    .registers 2

    .prologue
    .line 838
    iget-object v0, p0, LaN/bT;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->h()Z

    move-result v0

    return v0
.end method

.method public bH()V
    .registers 4

    .prologue
    .line 848
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    .line 849
    invoke-direct {p0}, LaN/bT;->bW()Lae/y;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->g()Lae/y;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, LaN/bT;->a(Lae/y;Lae/y;Ljava/lang/String;)V

    .line 851
    return-void
.end method

.method public bI()V
    .registers 2

    .prologue
    .line 944
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->h(LaN/i;)V

    .line 945
    return-void
.end method

.method public bJ()V
    .registers 4

    .prologue
    .line 1167
    const/16 v0, 0x73

    const-string v1, "f"

    const-string v2, "t=o"

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1170
    return-void
.end method

.method public bM()Lcom/google/googlenav/T;
    .registers 2

    .prologue
    .line 1285
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    return-object v0
.end method

.method public bN()Lcom/google/googlenav/Y;
    .registers 2

    .prologue
    .line 1316
    invoke-super {p0}, LaN/m;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/Y;

    return-object v0
.end method

.method public c()V
    .registers 2

    .prologue
    .line 1478
    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/bT;->R:Z

    .line 1479
    return-void
.end method

.method public d(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 482
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 483
    invoke-super {p0, p1}, LaN/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v0

    .line 485
    :goto_a
    return v0

    :cond_b
    check-cast p1, Lcom/google/googlenav/ct;

    invoke-virtual {p1}, Lcom/google/googlenav/ct;->k()Z

    move-result v0

    goto :goto_a
.end method

.method public e()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 587
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 588
    invoke-virtual {p0}, LaN/bT;->Z()V

    .line 589
    iget-object v0, p0, LaN/bT;->d:Lau/u;

    invoke-virtual {v0}, Lau/u;->c()Lau/B;

    move-result-object v0

    .line 590
    invoke-virtual {p0}, LaN/bT;->bN()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->b()Lae/y;

    move-result-object v1

    invoke-virtual {v1}, Lae/y;->f()Lau/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lau/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2e

    .line 591
    iget-object v1, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-static {v0, v2}, Lae/y;->a(Lau/B;Lo/B;)Lae/y;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/v;->a(Lae/y;Lae/y;)Lcom/google/googlenav/aa;

    .line 593
    :cond_2e
    return-void
.end method

.method public f()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 773
    const/16 v0, 0xc2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 776
    invoke-static {}, Lcom/google/googlenav/ui/wizard/dj;->e()I

    move-result v1

    xor-int/lit8 v1, v1, 0x4

    xor-int/lit8 v1, v1, 0x8

    .line 781
    const/16 v2, 0x116

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 784
    const/4 v3, 0x5

    .line 786
    const/16 v4, 0x5d3

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 789
    iget-object v5, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v5

    new-instance v6, Lcom/google/googlenav/ui/wizard/dH;

    invoke-direct {v6}, Lcom/google/googlenav/ui/wizard/dH;-><init>()V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/wizard/dH;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dH;->a(I)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dH;->b(Z)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    new-instance v1, LaN/cd;

    const/4 v6, 0x0

    invoke-direct {v1, p0, v6}, LaN/cd;-><init>(LaN/bT;LaN/bU;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dH;->a(Lcom/google/googlenav/ui/wizard/dA;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/dH;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/dH;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dH;->a(B)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dH;->c(Z)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dH;->d(Z)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/ui/wizard/dH;)V

    .line 799
    return-void
.end method

.method protected f(Laa/a;)Z
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x23

    const/4 v0, 0x1

    .line 558
    invoke-virtual {p1}, Laa/a;->a()I

    move-result v1

    if-ne v1, v4, :cond_25

    invoke-virtual {p1}, Laa/a;->c()I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_25

    .line 560
    invoke-virtual {p0}, LaN/bT;->ae()Z

    move-result v1

    if-nez v1, :cond_25

    .line 561
    const/16 v1, 0x73

    const-string v2, "m"

    const-string v3, "a=l"

    invoke-static {v1, v2, v3}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 564
    const/4 v1, 0x0

    invoke-virtual {p0, v4, v1}, LaN/bT;->c(ILjava/lang/Object;)V

    .line 576
    :goto_24
    return v0

    .line 570
    :cond_25
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_37

    invoke-virtual {p0}, LaN/bT;->aa()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 571
    invoke-virtual {p0}, LaN/bT;->h()V

    goto :goto_24

    .line 576
    :cond_37
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 882
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 883
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_62

    .line 910
    invoke-virtual {p0}, LaN/bT;->ag()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 911
    invoke-virtual {p0, v2}, LaN/bT;->a(B)V

    .line 915
    :goto_1c
    return-void

    .line 885
    :sswitch_1d
    invoke-virtual {p0, v2, v3}, LaN/bT;->c(ILjava/lang/Object;)V

    goto :goto_1c

    .line 888
    :sswitch_21
    invoke-virtual {p0, v2, v3}, LaN/bT;->b(ILjava/lang/Object;)V

    goto :goto_1c

    .line 891
    :sswitch_25
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    .line 892
    if-eqz v0, :cond_38

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_38

    .line 893
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/bT;->b(I)V

    .line 895
    :cond_38
    invoke-virtual {p0, v2, v3}, LaN/bT;->a(ILjava/lang/Object;)V

    goto :goto_1c

    .line 898
    :sswitch_3c
    invoke-virtual {p0}, LaN/bT;->ag()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 899
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {p0, v1, v3}, LaN/bT;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 901
    invoke-virtual {p0, v2}, LaN/bT;->a(B)V

    goto :goto_1c

    .line 903
    :cond_55
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->X()V

    goto :goto_1c

    .line 913
    :cond_5b
    iget-object v0, p0, LaN/bT;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->X()V

    goto :goto_1c

    .line 883
    nop

    :sswitch_data_62
    .sparse-switch
        0x11 -> :sswitch_3c
        0x21 -> :sswitch_25
        0x22 -> :sswitch_1d
        0x23 -> :sswitch_21
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1483
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1484
    iget-object v1, p0, LaN/bT;->K:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Lad/g;

    move-result-object v1

    if-eqz v1, :cond_10

    const/4 v0, 0x1

    .line 1491
    :cond_10
    :goto_10
    return v0

    .line 1486
    :cond_11
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->au()Lcom/google/googlenav/cd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cd;->f()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1489
    check-cast p1, Lcom/google/googlenav/ct;

    invoke-virtual {p0, p1}, LaN/bT;->b(Lcom/google/googlenav/ct;)V

    goto :goto_10
.end method

.method protected i()LaP/a;
    .registers 2

    .prologue
    .line 472
    new-instance v0, LaP/l;

    invoke-direct {v0, p0}, LaP/l;-><init>(LaN/i;)V

    return-object v0
.end method

.method protected l()V
    .registers 2

    .prologue
    .line 1082
    invoke-super {p0}, LaN/m;->l()V

    .line 1083
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    if-eqz v0, :cond_e

    .line 1084
    iget-object v0, p0, LaN/bT;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->m()V

    .line 1086
    :cond_e
    return-void
.end method

.method protected x()I
    .registers 2

    .prologue
    .line 1528
    const/4 v0, 0x0

    return v0
.end method
