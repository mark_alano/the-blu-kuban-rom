.class public LaN/bj;
.super LaN/m;
.source "SourceFile"

# interfaces
.implements Lau/m;
.implements Lay/B;
.implements Lcom/google/android/maps/driveabout/vector/i;
.implements Lcom/google/googlenav/aT;
.implements Lcom/google/googlenav/bb;


# instance fields
.field protected volatile B:Z

.field protected C:I

.field protected D:I

.field private E:Z

.field private F:J

.field private G:Lcom/google/googlenav/ui/view/d;

.field private H:J

.field private I:Lcom/google/googlenav/ui/android/ap;

.field private J:J

.field private K:Lcom/google/googlenav/ui/view/d;

.field private L:Lau/H;

.field private M:Z

.field private N:Lcom/google/googlenav/ui/view/K;

.field private O:Ljava/lang/String;

.field private P:Lcom/google/googlenav/layer/m;

.field private Q:Lau/k;

.field private R:Lcom/google/googlenav/layer/s;

.field private S:Z

.field private T:LP/b;

.field private U:LP/b;

.field private V:Z

.field private W:LaU/i;

.field private X:LaN/ah;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;I)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 301
    invoke-direct {p0, p1, p2, p3, p4}, LaN/m;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V

    .line 162
    iput-boolean v0, p0, LaN/bj;->B:Z

    .line 258
    iput-boolean v0, p0, LaN/bj;->S:Z

    .line 273
    iput-boolean v0, p0, LaN/bj;->V:Z

    .line 302
    invoke-direct {p0, p5, p6, p7}, LaN/bj;->a(Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 303
    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;Lau/k;I)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 312
    invoke-direct/range {p0 .. p5}, LaN/m;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/F;)V

    .line 162
    iput-boolean v0, p0, LaN/bj;->B:Z

    .line 258
    iput-boolean v0, p0, LaN/bj;->S:Z

    .line 273
    iput-boolean v0, p0, LaN/bj;->V:Z

    .line 313
    invoke-direct {p0, p6, p7, p8}, LaN/bj;->a(Lcom/google/googlenav/layer/m;Lau/k;I)V

    .line 314
    iget-object v0, p1, Lcom/google/googlenav/ui/v;->f:LaU/i;

    iput-object v0, p0, LaN/bj;->W:LaU/i;

    .line 315
    return-void
.end method

.method static synthetic a(LaN/bj;)LP/b;
    .registers 2
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, LaN/bj;->T:LP/b;

    return-object v0
.end method

.method private a(LaD/S;)V
    .registers 6
    .parameter

    .prologue
    .line 1575
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1577
    iget-object v1, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {p1}, LaD/S;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    const/16 v3, 0x5e8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v2

    const-string v3, "20"

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/bf;)V

    .line 1587
    return-void
.end method

.method static synthetic a(LaN/bj;Lcom/google/googlenav/aZ;ZZ)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3}, LaN/bj;->a(Lcom/google/googlenav/aZ;ZZ)V

    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;ZLjava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x2

    .line 618
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/c;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 620
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aM()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 621
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 622
    new-instance v1, LP/b;

    new-instance v2, LaN/bk;

    invoke-direct {v2, p0, p1, p2}, LaN/bk;-><init>(LaN/bj;Lcom/google/googlenav/aZ;Z)V

    invoke-direct {v1, v3, v0, v2}, LP/b;-><init>(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LP/c;)V

    iput-object v1, p0, LaN/bj;->T:LP/b;

    .line 638
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v0

    iget-object v1, p0, LaN/bj;->T:LP/b;

    invoke-virtual {v0, v1}, Lad/h;->c(Lad/g;)V

    .line 639
    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;ZZ)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 668
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 669
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    .line 672
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/aZ;)V

    .line 674
    invoke-virtual {p0, v1}, LaN/bj;->b(I)V

    .line 676
    invoke-virtual {p0}, LaN/bj;->R()V

    .line 678
    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 679
    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/aZ;I)V

    .line 741
    :cond_2a
    :goto_2a
    invoke-virtual {p0}, LaN/bj;->bF()V

    .line 744
    invoke-direct {p0}, LaN/bj;->bY()V

    .line 747
    iput-boolean v6, p0, LaN/bj;->B:Z

    .line 748
    return-void

    .line 682
    :cond_33
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->K()Z

    move-result v0

    if-eqz v0, :cond_79

    .line 683
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    const/16 v1, 0x5b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xa

    const/16 v5, 0x20

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->a(Ljava/lang/String;)V

    .line 689
    :cond_79
    if-eqz p2, :cond_9b

    .line 690
    invoke-static {p1}, LaN/bj;->d(Lcom/google/googlenav/F;)Z

    move-result v0

    if-eqz v0, :cond_88

    .line 692
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/bj;->b(I)V

    .line 697
    :cond_88
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_98

    invoke-virtual {p0}, LaN/bj;->bL()Z

    move-result v0

    if-nez v0, :cond_9b

    .line 698
    :cond_98
    invoke-virtual {p0}, LaN/bj;->an()Z

    .line 704
    :cond_9b
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 705
    invoke-direct {p0}, LaN/bj;->ck()V

    .line 711
    :cond_a4
    iget-object v0, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_ab

    .line 712
    invoke-direct {p0, p1}, LaN/bj;->i(Lcom/google/googlenav/aZ;)V

    .line 721
    :cond_ab
    if-eqz p2, :cond_bb

    .line 726
    iput-boolean v7, p0, LaN/bj;->M:Z

    .line 728
    invoke-virtual {p0, p3}, LaN/bj;->k(Z)Z

    move-result v0

    if-eqz v0, :cond_bb

    .line 730
    invoke-direct {p0}, LaN/bj;->bX()Lau/H;

    move-result-object v0

    iput-object v0, p0, LaN/bj;->L:Lau/H;

    .line 734
    :cond_bb
    invoke-virtual {p0, p1}, LaN/bj;->c(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 735
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LaN/bj;->c(ILjava/lang/Object;)V

    goto/16 :goto_2a
.end method

.method private a(Lcom/google/googlenav/be;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1541
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 1542
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v0

    .line 1544
    if-nez v0, :cond_f

    .line 1545
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1548
    :cond_f
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_60

    .line 1549
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1554
    :goto_18
    if-eqz p2, :cond_5f

    .line 1556
    iget-object v2, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Set;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0x5e8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v3, "20"

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/bf;)V

    .line 1567
    :cond_5f
    return-void

    .line 1551
    :cond_60
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_18
.end method

.method private a(Lcom/google/googlenav/layer/m;Lau/k;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 318
    iput-object p1, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    .line 319
    iput-object p2, p0, LaN/bj;->Q:Lau/k;

    .line 320
    iput p3, p0, LaN/bj;->C:I

    .line 322
    new-instance v0, Lcom/google/googlenav/layer/s;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/layer/s;-><init>(LaN/i;Lcom/google/googlenav/layer/m;)V

    iput-object v0, p0, LaN/bj;->R:Lcom/google/googlenav/layer/s;

    .line 324
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    if-eqz v0, :cond_1e

    .line 325
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    move-result-object v0

    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v0

    invoke-interface {v0, p0}, Lay/t;->a(Lay/B;)V

    .line 331
    :cond_1e
    iget-object v0, p0, LaN/bj;->t:LaP/a;

    check-cast v0, LaP/i;

    invoke-virtual {v0, p3}, LaP/i;->c(I)V

    .line 333
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/bj;->i(Z)V

    .line 334
    invoke-direct {p0}, LaN/bj;->bV()V

    .line 335
    return-void
.end method

.method private b(ILcom/google/googlenav/ai;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 3139
    const/4 v5, 0x0

    .line 3141
    new-instance v2, LaN/bt;

    invoke-direct {v2, p0, p1}, LaN/bt;-><init>(LaN/bj;I)V

    .line 3160
    iget-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_f

    .line 3161
    iget-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3163
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LaN/bj;->F:J

    .line 3164
    new-instance v0, Lcom/google/googlenav/ui/android/am;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/am;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;LaN/bj;Lcom/google/googlenav/ai;LT/f;)V

    iput-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    .line 3166
    return-void
.end method

.method static synthetic b(LaN/bj;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, LaN/bj;->bZ()V

    return-void
.end method

.method public static bU()J
    .registers 2

    .prologue
    .line 3179
    const-wide/16 v0, 0x1388

    return-wide v0
.end method

.method private bV()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 343
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    if-nez v0, :cond_6

    .line 374
    :cond_5
    :goto_5
    return-void

    .line 347
    :cond_6
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v1

    .line 348
    instance-of v0, v1, LaO/b;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 352
    check-cast v0, LaO/b;

    invoke-virtual {v0}, LaO/b;->ae()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->w()Lcom/google/android/maps/driveabout/vector/cz;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 360
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->w()Lcom/google/android/maps/driveabout/vector/cz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/cz;->o()Lcom/google/android/maps/driveabout/vector/dd;

    move-result-object v2

    .line 364
    if-eqz v2, :cond_5

    .line 369
    new-instance v3, LaN/ah;

    invoke-direct {v3, v0, v2}, LaN/ah;-><init>(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lcom/google/android/maps/driveabout/vector/dd;)V

    iput-object v3, p0, LaN/bj;->X:LaN/ah;

    .line 372
    iget-object v0, p0, LaN/bj;->X:LaN/ah;

    const/4 v2, 0x1

    new-array v2, v2, [I

    aput v4, v2, v4

    invoke-static {v2}, Lu/a;->a([I)Lu/a;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LaN/am;->a(LaN/au;Lu/a;)V

    goto :goto_5
.end method

.method private bW()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 759
    iget-object v1, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v1

    invoke-virtual {v1}, LaN/am;->J()LaN/i;

    move-result-object v1

    if-eq v1, p0, :cond_e

    .line 769
    :cond_d
    :goto_d
    return v0

    .line 765
    :cond_e
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->am()Z

    move-result v1

    if-nez v1, :cond_d

    .line 769
    const/4 v0, 0x1

    goto :goto_d
.end method

.method private bX()Lau/H;
    .registers 7

    .prologue
    .line 795
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 799
    iget-object v1, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->E()V

    .line 804
    iget-object v1, p0, LaN/bj;->d:Lau/u;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->S()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->T()I

    move-result v3

    invoke-virtual {p0}, LaN/bj;->bL()Z

    move-result v4

    invoke-virtual {p0, v4}, LaN/bj;->c(Z)I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lau/u;->a(IIII)Lau/Y;

    move-result-object v1

    .line 808
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->O()Lau/B;

    move-result-object v0

    .line 809
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aq()Z

    move-result v2

    if-eqz v2, :cond_4b

    .line 811
    invoke-virtual {p0}, LaN/bj;->bL()Z

    move-result v2

    invoke-virtual {p0, v2}, LaN/bj;->c(Z)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 812
    invoke-virtual {p0}, LaN/bj;->q()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    .line 813
    neg-int v2, v2

    neg-int v3, v3

    invoke-virtual {v0, v2, v3, v1}, Lau/B;->a(IILau/Y;)Lau/B;

    move-result-object v0

    .line 819
    :cond_4b
    invoke-virtual {p0, v0, v1}, LaN/bj;->a(Lau/B;Lau/Y;)Lau/B;

    move-result-object v0

    .line 822
    iget-object v2, p0, LaN/bj;->d:Lau/u;

    invoke-virtual {v2, v0, v1}, Lau/u;->d(Lau/B;Lau/Y;)V

    .line 824
    iget-object v2, p0, LaN/bj;->d:Lau/u;

    invoke-virtual {v2}, Lau/u;->f()Lau/H;

    move-result-object v2

    invoke-virtual {v2, v1}, Lau/H;->a(Lau/Y;)Lau/H;

    move-result-object v1

    invoke-virtual {v1, v0}, Lau/H;->a(Lau/B;)Lau/H;

    move-result-object v0

    return-object v0
.end method

.method private bY()V
    .registers 6

    .prologue
    .line 862
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 868
    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    :goto_9
    if-ge v0, v2, :cond_17

    .line 869
    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    const/16 v4, 0x10

    invoke-virtual {p0, v3, v4}, LaN/bj;->a(Lcom/google/googlenav/E;I)V

    .line 868
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 873
    :cond_17
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_29

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_29

    .line 874
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, LaN/bj;->f(I)V

    .line 878
    :cond_29
    return-void
.end method

.method private bZ()V
    .registers 8

    .prologue
    const/4 v6, 0x3

    .line 1449
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 1450
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    .line 1451
    if-nez v0, :cond_10

    .line 1452
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1457
    :cond_10
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1458
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_62

    .line 1459
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1465
    :goto_21
    iget-object v2, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0x5e8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v3, "20"

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/bf;)V

    .line 1474
    return-void

    .line 1461
    :cond_62
    new-instance v3, Lcom/google/googlenav/bc;

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct {v3, v6, v4, v5}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_21
.end method

.method static synthetic c(LaN/bj;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, LaN/bj;->cg()V

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x4

    const/4 v2, 0x0

    .line 1498
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v4

    .line 1499
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_74

    move v0, v1

    .line 1500
    :goto_11
    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v3

    .line 1502
    if-nez v3, :cond_1c

    .line 1503
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1508
    :cond_1c
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1509
    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_76

    if-nez v0, :cond_76

    .line 1510
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520
    :cond_2f
    :goto_2f
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v3, 0x5e8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const-string v3, "20"

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/bf;)V

    .line 1530
    return-void

    :cond_74
    move v0, v2

    .line 1499
    goto :goto_11

    .line 1511
    :cond_76
    invoke-static {p2}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 1512
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1513
    new-instance v6, Lcom/google/googlenav/bd;

    const-string v7, ""

    invoke-direct {v6, v2, v7, p2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1514
    new-instance v6, Lcom/google/googlenav/bc;

    const/16 v7, 0x12c

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-static {v7, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v8, v1, v0}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2f
.end method

.method private ca()V
    .registers 4

    .prologue
    .line 1590
    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1591
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, LaN/bj;->f(I)V

    .line 1598
    :goto_b
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/bj;->f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1599
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, LaN/bj;->a(ILcom/google/googlenav/ai;)V

    .line 1602
    invoke-direct {p0, v0}, LaN/bj;->p(Lcom/google/googlenav/ai;)Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1603
    iget-object v1, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LaN/am;->c(Lcom/google/googlenav/layer/m;Z)LaN/y;

    .line 1604
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/v;->a(LaN/i;)V

    .line 1605
    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v0

    if-nez v0, :cond_36

    invoke-virtual {p0}, LaN/bj;->af()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 1606
    :cond_36
    invoke-virtual {p0}, LaN/bj;->n()V

    .line 1608
    :cond_39
    return-void

    .line 1592
    :cond_3a
    invoke-virtual {p0}, LaN/bj;->af()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 1593
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, LaN/bj;->f(I)V

    goto :goto_b

    .line 1595
    :cond_46
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, LaN/bj;->f(I)V

    goto :goto_b
.end method

.method private cb()V
    .registers 2

    .prologue
    .line 1874
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/bj;->b(I)V

    .line 1875
    invoke-virtual {p0}, LaN/bj;->an()Z

    .line 1876
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/bj;->b(Z)V

    .line 1877
    return-void
.end method

.method private cc()Ljava/lang/String;
    .registers 5

    .prologue
    .line 1887
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1888
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "n="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1890
    invoke-virtual {p0}, LaN/bj;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    .line 1891
    if-eqz v1, :cond_56

    .line 1892
    invoke-static {v1}, Lcom/google/googlenav/bQ;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    .line 1893
    if-eqz v1, :cond_56

    .line 1895
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&gmmsmh=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1896
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "u="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1900
    :cond_56
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private cd()Z
    .registers 3

    .prologue
    .line 2005
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_a
    if-ltz v0, :cond_29

    .line 2006
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    if-eqz v1, :cond_26

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v1

    if-eqz v1, :cond_26

    .line 2007
    const/4 v0, 0x0

    .line 2010
    :goto_25
    return v0

    .line 2005
    :cond_26
    add-int/lit8 v0, v0, -0x1

    goto :goto_a

    .line 2010
    :cond_29
    const/4 v0, 0x1

    goto :goto_25
.end method

.method private ce()Z
    .registers 3

    .prologue
    .line 2024
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    .line 2025
    add-int/lit8 v0, v0, 0x1

    :goto_a
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    if-ge v0, v1, :cond_27

    .line 2026
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v1

    if-eqz v1, :cond_24

    .line 2027
    const/4 v0, 0x0

    .line 2030
    :goto_23
    return v0

    .line 2025
    :cond_24
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2030
    :cond_27
    const/4 v0, 0x1

    goto :goto_23
.end method

.method private cf()Lcom/google/googlenav/n;
    .registers 2

    .prologue
    .line 2253
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/n;

    return-object v0
.end method

.method private cg()V
    .registers 2

    .prologue
    .line 3197
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaN/bj;->l(Z)V

    .line 3198
    return-void
.end method

.method private ch()V
    .registers 4

    .prologue
    .line 3201
    iget-object v0, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    if-nez v0, :cond_5

    .line 3212
    :goto_4
    return-void

    .line 3205
    :cond_5
    iget-object v0, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ap;->c()V

    .line 3206
    const/4 v0, 0x2

    const-string v1, "pubalerts d"

    iget-object v2, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/ap;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3210
    const/4 v0, 0x0

    iput-object v0, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    .line 3211
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaN/bj;->H:J

    goto :goto_4
.end method

.method private ci()V
    .registers 7

    .prologue
    .line 3218
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aP()J

    move-result-wide v0

    .line 3219
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_25

    .line 3220
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    .line 3221
    iget-wide v4, p0, LaN/bj;->F:J

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_25

    .line 3222
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaN/bj;->l(Z)V

    .line 3225
    :cond_25
    return-void
.end method

.method private cj()V
    .registers 7

    .prologue
    .line 3231
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 3232
    iget-wide v2, p0, LaN/bj;->H:J

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_28

    .line 3234
    iget-object v0, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    if-eqz v0, :cond_28

    .line 3235
    const/4 v0, 0x2

    const-string v1, "pubalerts dt"

    iget-object v2, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/ap;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3239
    invoke-direct {p0}, LaN/bj;->ch()V

    .line 3242
    :cond_28
    return-void
.end method

.method private ck()V
    .registers 5

    .prologue
    .line 3245
    iget-object v0, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_9

    .line 3246
    iget-object v0, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3250
    :cond_9
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aq()I

    move-result v0

    if-nez v0, :cond_22

    .line 3251
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, LaN/am;->a(Lcom/google/googlenav/aZ;I)V

    .line 3303
    :goto_21
    return-void

    .line 3256
    :cond_22
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x3a98

    add-long/2addr v0, v2

    iput-wide v0, p0, LaN/bj;->J:J

    .line 3262
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 3263
    new-instance v1, LaN/bu;

    invoke-direct {v1, p0}, LaN/bu;-><init>(LaN/bj;)V

    .line 3275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xe8

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aZ;->ap:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    .line 3298
    :goto_67
    new-instance v2, Lcom/google/googlenav/ui/android/ax;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v3

    invoke-direct {v2, v3, v1, v0}, Lcom/google/googlenav/ui/android/ax;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/ba;)V

    iput-object v2, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    .line 3301
    const/16 v0, 0x38

    const-string v1, "v"

    invoke-static {v0, v1}, LaU/m;->a(ILjava/lang/String;)V

    goto :goto_21

    .line 3282
    :cond_7e
    new-instance v1, LaN/bm;

    invoke-direct {v1, p0}, LaN/bm;-><init>(LaN/bj;)V

    .line 3294
    const/16 v0, 0xe9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aZ;->ap:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    goto :goto_67
.end method

.method private cl()V
    .registers 3

    .prologue
    .line 3306
    iget-object v0, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_10

    .line 3307
    iget-object v0, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3308
    const/4 v0, 0x0

    iput-object v0, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    .line 3309
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaN/bj;->J:J

    .line 3311
    :cond_10
    return-void
.end method

.method private cm()V
    .registers 5

    .prologue
    .line 3317
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, LaN/bj;->J:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_28

    .line 3318
    invoke-direct {p0}, LaN/bj;->cl()V

    .line 3322
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->r()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 3323
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->h(LaN/i;)V

    .line 3326
    :cond_28
    return-void
.end method

.method static synthetic d(LaN/bj;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, LaN/bj;->ca()V

    return-void
.end method

.method public static d(Lcom/google/googlenav/F;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1060
    move v1, v2

    :goto_2
    invoke-interface {p0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 1061
    invoke-interface {p0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1062
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1063
    const/4 v2, 0x1

    .line 1066
    :cond_19
    return v2

    .line 1060
    :cond_1a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method static synthetic e(LaN/bj;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, LaN/bj;->cl()V

    return-void
.end method

.method private i(Lcom/google/googlenav/aZ;)V
    .registers 3
    .parameter

    .prologue
    .line 2880
    invoke-direct {p0}, LaN/bj;->bW()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2894
    :goto_6
    return-void

    .line 2886
    :cond_7
    invoke-virtual {p0, p1}, LaN/bj;->f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 2887
    if-eqz v0, :cond_11

    .line 2888
    invoke-direct {p0, v0}, LaN/bj;->o(Lcom/google/googlenav/ai;)V

    goto :goto_6

    .line 2889
    :cond_11
    invoke-virtual {p0, p1}, LaN/bj;->g(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2890
    invoke-direct {p0, p1}, LaN/bj;->k(Lcom/google/googlenav/aZ;)V

    goto :goto_6

    .line 2892
    :cond_1b
    invoke-direct {p0, p1}, LaN/bj;->j(Lcom/google/googlenav/aZ;)Z

    goto :goto_6
.end method

.method private j(Lcom/google/googlenav/aZ;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 3058
    .line 3061
    invoke-virtual {p1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    check-cast v3, Lcom/google/googlenav/ai;

    .line 3062
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_10

    .line 3127
    :cond_f
    :goto_f
    return v0

    .line 3067
    :cond_10
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->a()Lau/B;

    move-result-object v1

    if-eqz v1, :cond_51

    invoke-virtual {p0, v3, v0}, LaN/bj;->c(Lcom/google/googlenav/ai;Z)LT/f;

    move-result-object v9

    .line 3069
    :goto_1a
    new-instance v2, LaN/bs;

    invoke-direct {v2, p0}, LaN/bs;-><init>(LaN/bj;)V

    .line 3115
    iget-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_28

    .line 3116
    iget-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3118
    :cond_28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LaN/bj;->F:J

    .line 3120
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aH()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 3121
    new-instance v0, Lcom/google/googlenav/ui/android/an;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    invoke-virtual {p0, v3}, LaN/bj;->j(Lcom/google/googlenav/ai;)Z

    move-result v4

    invoke-virtual {p0, v3}, LaN/bj;->k(Lcom/google/googlenav/ai;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/an;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ai;ZZ)V

    iput-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    .line 3127
    :goto_4f
    const/4 v0, 0x1

    goto :goto_f

    .line 3067
    :cond_51
    const/4 v9, 0x0

    goto :goto_1a

    .line 3124
    :cond_53
    new-instance v4, Lcom/google/googlenav/ui/android/am;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v5

    move-object v6, v2

    move-object v7, p0

    move-object v8, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/googlenav/ui/android/am;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;LaN/bj;Lcom/google/googlenav/ai;LT/f;)V

    iput-object v4, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    goto :goto_4f
.end method

.method private k(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 3169
    invoke-virtual {p0, p1}, LaN/bj;->h(Lcom/google/googlenav/aZ;)I

    move-result v1

    .line 3170
    const/4 v0, -0x1

    if-ne v1, v0, :cond_8

    .line 3176
    :goto_7
    return-void

    .line 3174
    :cond_8
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 3175
    invoke-direct {p0, v1, v0}, LaN/bj;->b(ILcom/google/googlenav/ai;)V

    goto :goto_7
.end method

.method private l(I)Lcom/google/googlenav/ai;
    .registers 3
    .parameter

    .prologue
    .line 1049
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private l(Z)V
    .registers 4
    .parameter

    .prologue
    .line 3188
    if-nez p1, :cond_c

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aQ()Z

    move-result v0

    if-nez v0, :cond_1c

    :cond_c
    iget-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1c

    .line 3190
    iget-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3191
    const/4 v0, 0x0

    iput-object v0, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    .line 3192
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaN/bj;->F:J

    .line 3194
    :cond_1c
    return-void
.end method

.method private m(I)I
    .registers 5
    .parameter

    .prologue
    .line 2736
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2737
    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_1d

    .line 2738
    invoke-direct {p0, v0}, LaN/bj;->l(I)Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 2739
    if-nez p1, :cond_18

    .line 2746
    :goto_17
    return v0

    .line 2742
    :cond_18
    add-int/lit8 p1, p1, -0x1

    .line 2737
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2746
    :cond_1d
    const/4 v0, -0x1

    goto :goto_17
.end method

.method private m(Lcom/google/googlenav/ai;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1847
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ah()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1869
    :goto_8
    :pswitch_8
    return v0

    .line 1852
    :cond_9
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->c()B

    move-result v2

    packed-switch v2, :pswitch_data_14

    :pswitch_10
    move v0, v1

    .line 1869
    goto :goto_8

    :pswitch_12
    move v0, v1

    .line 1854
    goto :goto_8

    .line 1852
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_12
        :pswitch_8
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_10
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method private n(Lcom/google/googlenav/ai;)I
    .registers 5
    .parameter

    .prologue
    .line 2707
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2708
    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_15

    .line 2709
    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    if-ne v2, p1, :cond_12

    .line 2713
    :goto_11
    return v0

    .line 2708
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2713
    :cond_15
    const/4 v0, -0x1

    goto :goto_11
.end method

.method private o(Lcom/google/googlenav/ai;)V
    .registers 5
    .parameter

    .prologue
    .line 2947
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, LaN/bj;->f(I)V

    .line 2948
    new-instance v0, LaN/br;

    invoke-direct {v0, p0}, LaN/br;-><init>(LaN/bj;)V

    .line 2961
    iget-object v1, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_13

    .line 2962
    iget-object v1, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 2964
    :cond_13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, LaN/bj;->F:J

    .line 2965
    new-instance v1, Lcom/google/googlenav/ui/android/az;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v2

    invoke-direct {v1, v2, v0, p0, p1}, Lcom/google/googlenav/ui/android/az;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;LaN/bj;Lcom/google/googlenav/ai;)V

    iput-object v1, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    .line 2967
    return-void
.end method

.method private p(Lcom/google/googlenav/ai;)Lcom/google/googlenav/layer/m;
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2970
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dX;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2972
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bM()Ljava/lang/String;

    move-result-object v1

    .line 2973
    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2974
    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2975
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bL()Ljava/lang/String;

    move-result-object v1

    .line 2976
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "msid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2977
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2978
    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2980
    const/16 v1, 0x11

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ao()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    .line 2982
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dX;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2984
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2985
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2986
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2987
    const/4 v2, 0x4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2988
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2989
    const/16 v2, 0x17

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2991
    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method


# virtual methods
.method public M()Z
    .registers 2

    .prologue
    .line 1985
    invoke-direct {p0}, LaN/bj;->cd()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, LaN/bj;->ah()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public N()Z
    .registers 2

    .prologue
    .line 1992
    const/4 v0, 0x1

    return v0
.end method

.method protected O()Z
    .registers 2

    .prologue
    .line 1957
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 1962
    invoke-virtual {p0}, LaN/bj;->bL()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-super {p0}, LaN/m;->P()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected T()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1967
    const/16 v0, 0x4e9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .registers 2

    .prologue
    .line 1972
    const/16 v0, 0xf

    return v0
.end method

.method protected X()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1074
    invoke-super {p0}, LaN/m;->X()Z

    .line 1078
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->h()[Lcom/google/googlenav/W;

    move-result-object v0

    if-eqz v0, :cond_30

    move v0, v1

    .line 1080
    :goto_10
    iget-boolean v3, p0, LaN/bj;->S:Z

    if-eqz v3, :cond_32

    if-nez v0, :cond_32

    .line 1081
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->a()V

    .line 1084
    iput-boolean v2, p0, LaN/bj;->S:Z

    .line 1087
    invoke-virtual {p0}, LaN/bj;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_2c

    .line 1088
    invoke-virtual {p0}, LaN/bj;->Z()V

    .line 1090
    :cond_2c
    invoke-virtual {p0}, LaN/bj;->R()V

    .line 1093
    :goto_2f
    return v1

    :cond_30
    move v0, v2

    .line 1078
    goto :goto_10

    :cond_32
    move v1, v2

    .line 1093
    goto :goto_2f
.end method

.method public Y()V
    .registers 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1237
    iget-boolean v2, p0, LaN/bj;->M:Z

    if-eqz v2, :cond_9

    .line 1238
    iput-boolean v0, p0, LaN/bj;->M:Z

    .line 1269
    :goto_8
    return-void

    .line 1242
    :cond_9
    iget-object v2, p0, LaN/bj;->L:Lau/H;

    if-eqz v2, :cond_40

    iget-object v2, p0, LaN/bj;->L:Lau/H;

    invoke-virtual {v2}, Lau/H;->a()Lau/B;

    move-result-object v2

    iget-object v3, p0, LaN/bj;->d:Lau/u;

    invoke-virtual {v3}, Lau/u;->c()Lau/B;

    move-result-object v3

    iget-object v4, p0, LaN/bj;->L:Lau/H;

    invoke-virtual {v4}, Lau/H;->b()Lau/Y;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lau/B;->a(Lau/B;Lau/Y;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gez v2, :cond_40

    iget-object v2, p0, LaN/bj;->L:Lau/H;

    invoke-virtual {v2}, Lau/H;->b()Lau/Y;

    move-result-object v2

    invoke-virtual {v2}, Lau/Y;->a()I

    move-result v2

    iget-object v3, p0, LaN/bj;->d:Lau/u;

    invoke-virtual {v3}, Lau/u;->d()Lau/Y;

    move-result-object v3

    invoke-virtual {v3}, Lau/Y;->a()I

    move-result v3

    if-ge v2, v3, :cond_40

    move v0, v1

    .line 1250
    :cond_40
    const/4 v2, 0x0

    iput-object v2, p0, LaN/bj;->L:Lau/H;

    .line 1255
    invoke-virtual {p0}, LaN/bj;->b()Lcom/google/googlenav/layer/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v2

    if-eqz v2, :cond_50

    .line 1256
    invoke-virtual {p0, v1}, LaN/bj;->e(Z)V

    .line 1259
    :cond_50
    if-eqz v0, :cond_56

    .line 1262
    invoke-virtual {p0, v1}, LaN/bj;->b(Z)V

    goto :goto_8

    .line 1264
    :cond_56
    invoke-direct {p0}, LaN/bj;->ci()V

    .line 1265
    invoke-direct {p0}, LaN/bj;->cm()V

    .line 1266
    invoke-direct {p0}, LaN/bj;->cj()V

    .line 1267
    invoke-super {p0}, LaN/m;->Y()V

    goto :goto_8
.end method

.method public a()V
    .registers 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 3342
    iget-boolean v0, p0, LaN/bj;->V:Z

    if-nez v0, :cond_3e

    iget-object v0, p0, LaN/bj;->W:LaU/i;

    if-eqz v0, :cond_3e

    .line 3343
    iget-object v0, p0, LaN/bj;->W:LaU/i;

    invoke-virtual {v0}, LaU/i;->b()V

    .line 3344
    iget-object v0, p0, LaN/bj;->W:LaU/i;

    invoke-virtual {v0}, LaU/i;->g()I

    move-result v0

    .line 3345
    if-lez v0, :cond_3c

    .line 3346
    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "s=a"

    aput-object v3, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-static {v1}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3350
    const-string v1, "stat"

    invoke-static {v5, v1, v0}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3353
    :cond_3c
    iput-boolean v4, p0, LaN/bj;->V:Z

    .line 3355
    :cond_3e
    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 410
    iput-object p1, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    .line 412
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->az()Z

    move-result v0

    if-nez v0, :cond_10

    .line 413
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaN/bj;->b(B)V

    .line 415
    :cond_10
    return-void
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 2516
    invoke-super {p0, p1, p2, p3}, LaN/m;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    .line 2519
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2520
    if-eqz v1, :cond_fd

    .line 2521
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 2522
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v0

    invoke-virtual {v0}, Lau/H;->a()Lau/B;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->S()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->T()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v4

    invoke-virtual {v4}, Lau/H;->b()Lau/Y;

    move-result-object v4

    invoke-virtual {v4}, Lau/Y;->a()I

    move-result v4

    invoke-static {v0, v2, v3, v4}, Lau/C;->a(Lau/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/R;

    .line 2527
    :cond_33
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 2528
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->d(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2530
    :cond_40
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4d

    .line 2531
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->e(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2533
    :cond_4d
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5a

    .line 2534
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->f(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2537
    :cond_5a
    const/4 v0, 0x4

    if-eq p3, v0, :cond_61

    const/16 v0, 0xf

    if-ne p3, v0, :cond_76

    .line 2540
    :cond_61
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aa()Lcom/google/googlenav/layer/a;

    move-result-object v0

    .line 2541
    if-eqz v0, :cond_127

    .line 2542
    invoke-virtual {v0}, Lcom/google/googlenav/layer/a;->a()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2543
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(I)Lcom/google/googlenav/R;

    .line 2550
    :cond_76
    :goto_76
    const/16 v0, 0x19

    if-eq p3, v0, :cond_7e

    const/16 v0, 0x18

    if-ne p3, v0, :cond_97

    .line 2552
    :cond_7e
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_97

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_97

    .line 2553
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/Integer;)Lcom/google/googlenav/R;

    .line 2558
    :cond_97
    const/16 v0, 0x1f

    if-ne p3, v0, :cond_fd

    if-eqz p2, :cond_fd

    move-object v0, p2

    .line 2560
    check-cast v0, Lcom/google/googlenav/ai;

    .line 2561
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->Y()I

    move-result v1

    sub-int v1, v4, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->N()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v2}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2567
    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2571
    :cond_fd
    if-eqz p2, :cond_10e

    invoke-interface {p2}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_10e

    .line 2572
    check-cast p2, Lcom/google/googlenav/ai;

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->bO()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/util/List;)Lcom/google/googlenav/R;

    .line 2577
    :cond_10e
    invoke-virtual {p0}, LaN/bj;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_126

    .line 2578
    invoke-virtual {p0}, LaN/bj;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2579
    invoke-virtual {p1, v6}, Lcom/google/googlenav/R;->b(Z)Lcom/google/googlenav/R;

    .line 2581
    :cond_126
    return-void

    .line 2546
    :cond_127
    invoke-virtual {p1, v6}, Lcom/google/googlenav/R;->a(Z)Lcom/google/googlenav/R;

    goto/16 :goto_76
.end method

.method public a(Lcom/google/googlenav/aS;)V
    .registers 5
    .parameter

    .prologue
    .line 2778
    invoke-virtual {p1}, Lcom/google/googlenav/aS;->i()I

    move-result v0

    if-nez v0, :cond_a

    .line 2779
    const/4 v0, 0x0

    iput-object v0, p0, LaN/bj;->O:Ljava/lang/String;

    .line 2789
    :cond_9
    :goto_9
    return-void

    .line 2782
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aS;->a(I)Lcom/google/googlenav/aU;

    move-result-object v0

    .line 2783
    invoke-virtual {v0}, Lcom/google/googlenav/aU;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaN/bj;->O:Ljava/lang/String;

    .line 2785
    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    if-eqz v0, :cond_9

    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2786
    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    iget-object v1, p0, LaN/bj;->O:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aX;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Ljava/lang/String;)V

    goto :goto_9
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2239
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3021
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_9e

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aC()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9e

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aC()Ljava/lang/String;

    move-result-object v0

    .line 3023
    :goto_10
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aI()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 3024
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aJ()LP/d;

    move-result-object v1

    invoke-virtual {v1}, LP/d;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3028
    :cond_39
    sget-object v1, Lcom/google/googlenav/ui/aZ;->ai:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3030
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()Lau/B;

    move-result-object v0

    if-eqz v0, :cond_a4

    .line 3031
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->aj:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3043
    :goto_68
    sget-object v0, Lag/j;->a:Lag/j;

    invoke-virtual {v0}, Lag/j;->d()Z

    move-result v0

    if-nez v0, :cond_9d

    .line 3044
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->aj:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3046
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->aj:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3049
    :cond_9d
    return-void

    .line 3021
    :cond_9e
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_10

    .line 3033
    :cond_a4
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_cf

    .line 3035
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->ah:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_68

    .line 3039
    :cond_cf
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->ah:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_68
.end method

.method protected a(Lcom/google/googlenav/ui/ba;)V
    .registers 5
    .parameter

    .prologue
    .line 423
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/ba;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lcom/google/common/collect/cx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Ljava/util/List;)V

    .line 424
    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 1012
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1013
    if-eqz v0, :cond_9

    .line 1014
    invoke-virtual {v0, p1}, Lcom/google/googlenav/aZ;->b(Ljava/io/DataOutput;)V

    .line 1016
    :cond_9
    return-void
.end method

.method public a(Ljava/lang/String;Lau/H;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 2826
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->c(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->d(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    .line 2840
    invoke-virtual {p0}, LaN/bj;->bT()Z

    move-result v0

    if-eqz v0, :cond_60

    .line 2841
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->p()Lcom/google/googlenav/ui/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ap;->q()Lao/h;

    move-result-object v0

    .line 2844
    invoke-interface {v0}, Lao/h;->m()Lao/s;

    move-result-object v0

    .line 2845
    if-eqz v0, :cond_6a

    invoke-virtual {v0}, Lao/s;->a()Lau/B;

    move-result-object v0

    .line 2846
    :goto_53
    iget-object v2, p0, LaN/bj;->d:Lau/u;

    invoke-virtual {v2}, Lau/u;->f()Lau/H;

    move-result-object v2

    invoke-virtual {v2, v0}, Lau/H;->a(Lau/B;)Lau/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Lau/H;)Lcom/google/googlenav/bg;

    .line 2848
    :cond_60
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/bf;)V

    .line 2849
    return-void

    .line 2845
    :cond_6a
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->O()Lau/B;

    move-result-object v0

    goto :goto_53
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 389
    iget-object v0, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 390
    iget-object v0, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/m;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 392
    :cond_11
    return-void
.end method

.method public a(Ljava/util/Vector;Lau/B;I)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1120
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v3

    .line 1121
    const/4 v0, 0x0

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    :goto_9
    if-ge v0, v1, :cond_29

    .line 1122
    invoke-virtual {v3, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    .line 1125
    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v4

    if-nez v4, :cond_18

    .line 1121
    :cond_15
    :goto_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1128
    :cond_18
    invoke-virtual {p0, v2, p2}, LaN/bj;->a(Lcom/google/googlenav/E;Lau/B;)J

    move-result-wide v4

    .line 1129
    int-to-long v6, p3

    cmp-long v6, v4, v6

    if-gez v6, :cond_15

    .line 1130
    invoke-static {p0, v2, v0, v4, v5}, LaN/ai;->a(LaN/i;Lcom/google/googlenav/E;IJ)LaN/ai;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_15

    .line 1135
    :cond_29
    invoke-virtual {p0}, LaN/bj;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_34

    .line 1165
    :cond_33
    return-void

    .line 1141
    :cond_34
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    .line 1142
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v5

    .line 1143
    invoke-virtual {v5, p2}, Lcom/google/googlenav/T;->a(Lau/B;)Ljava/util/Enumeration;

    move-result-object v6

    .line 1144
    if-eqz v6, :cond_33

    .line 1147
    :cond_42
    :goto_42
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1148
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    .line 1149
    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/aZ;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_42

    .line 1152
    invoke-virtual {p0, v0, p2}, LaN/bj;->a(Lcom/google/googlenav/E;Lau/B;)J

    move-result-wide v1

    .line 1153
    int-to-long v7, p3

    cmp-long v7, v1, v7

    if-gez v7, :cond_42

    .line 1155
    iget-boolean v7, p0, LaN/bj;->o:Z

    if-eqz v7, :cond_69

    .line 1156
    sget v7, LaN/am;->j:I

    int-to-long v7, v7

    add-long/2addr v1, v7

    .line 1158
    :cond_69
    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v7, v4

    .line 1160
    invoke-static {p0, v0, v7, v1, v2}, LaN/ai;->a(LaN/i;Lcom/google/googlenav/E;IJ)LaN/ai;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_42
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2292
    const/4 v0, 0x0

    invoke-super {p0, v0}, LaN/m;->a(Z)V

    .line 2293
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aF()V

    .line 2294
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 2295
    iget-object v0, p0, LaN/bj;->y:Lai/s;

    invoke-virtual {v0}, Lai/s;->a()V

    .line 2296
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x7

    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v5, 0x1

    .line 1273
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 1274
    sparse-switch p1, :sswitch_data_172

    .line 1442
    :cond_c
    :goto_c
    invoke-super {p0, p1, p2, p3}, LaN/m;->a(IILjava/lang/Object;)Z

    move-result v5

    :cond_10
    :goto_10
    return v5

    .line 1287
    :sswitch_11
    iput-boolean v9, p0, LaN/bj;->B:Z

    .line 1288
    iput p2, p0, LaN/bj;->D:I

    .line 1289
    const-string v0, "d"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, LaN/bj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    invoke-virtual {p0, p2}, LaN/bj;->j(I)V

    goto :goto_10

    .line 1295
    :sswitch_20
    iget-object v1, p0, LaN/bj;->U:LP/b;

    invoke-virtual {v1}, LP/b;->i()[Lcom/google/googlenav/ai;

    move-result-object v1

    .line 1296
    if-eqz v1, :cond_10

    array-length v2, v1

    if-ge p2, v2, :cond_10

    .line 1303
    iget-object v2, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1304
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/ai;BZ)V

    goto :goto_10

    .line 1309
    :sswitch_41
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p2}, Lcom/google/googlenav/F;->a(I)V

    .line 1310
    iput p2, p0, LaN/bj;->D:I

    .line 1313
    invoke-virtual {p0}, LaN/bj;->an()Z

    .line 1314
    invoke-virtual {p0, v5}, LaN/bj;->b(Z)V

    goto :goto_10

    .line 1318
    :sswitch_4f
    invoke-virtual {p0}, LaN/bj;->bN()V

    goto :goto_10

    .line 1322
    :sswitch_53
    invoke-direct {p0}, LaN/bj;->ca()V

    .line 1323
    const/16 v0, 0x54

    const-string v1, "ac"

    const-string v2, "sl"

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    .line 1329
    :sswitch_60
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "20"

    invoke-virtual {v0, v1, v2, v6, v9}, Lcom/google/googlenav/ui/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_10

    .line 1334
    :sswitch_70
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    const-string v2, "b"

    invoke-static {v0, v1, p2, p0, v2}, Lcom/google/googlenav/ui/wizard/hw;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/aZ;ILcom/google/googlenav/bb;Ljava/lang/String;)V

    goto :goto_10

    .line 1339
    :sswitch_78
    const/16 v0, 0x59

    const-string v2, "m"

    const-string v3, "1"

    invoke-static {v0, v2, v3}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1342
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v9}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    goto :goto_10

    .line 1346
    :sswitch_8b
    const/16 v0, 0x59

    const-string v2, "m"

    const-string v3, "2"

    invoke-static {v0, v2, v3}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1349
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v5}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    goto/16 :goto_10

    .line 1353
    :sswitch_9f
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v9}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_10

    .line 1356
    :sswitch_aa
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v4}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_10

    .line 1359
    :sswitch_b5
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_10

    .line 1367
    :sswitch_c1
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v0, v4, :cond_cf

    .line 1368
    invoke-super {p0, p1, p2, p3}, LaN/m;->a(IILjava/lang/Object;)Z

    move-result v5

    goto/16 :goto_10

    .line 1370
    :cond_cf
    if-eqz p3, :cond_de

    .line 1371
    check-cast p3, [Ljava/lang/String;

    check-cast p3, [Ljava/lang/String;

    .line 1372
    aget-object v0, p3, v9

    aget-object v1, p3, v5

    invoke-direct {p0, v0, v1}, LaN/bj;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 1374
    :cond_de
    invoke-direct {p0, v6, v6}, LaN/bj;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 1378
    :sswitch_e3
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v8

    .line 1379
    const-string v0, "OpenNowNotification"

    invoke-interface {v8, v0}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_127

    .line 1380
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    const/16 v1, 0x35d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x35c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x357

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x6a

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-instance v7, LaN/bp;

    invoke-direct {v7, p0}, LaN/bp;-><init>(LaN/bj;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jt;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bn;)V

    .line 1402
    const-string v0, "OpenNowNotification"

    new-array v1, v5, [B

    aput-byte v5, v1, v9

    invoke-interface {v8, v0, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 1404
    invoke-interface {v8}, Lcom/google/googlenav/common/io/j;->a()V

    goto/16 :goto_10

    .line 1406
    :cond_127
    invoke-direct {p0}, LaN/bj;->bZ()V

    goto/16 :goto_10

    .line 1411
    :sswitch_12c
    if-eqz p3, :cond_10

    .line 1412
    check-cast p3, Lcom/google/googlenav/be;

    .line 1413
    invoke-direct {p0, p3, v5}, LaN/bj;->a(Lcom/google/googlenav/be;Z)V

    goto/16 :goto_10

    .line 1418
    :sswitch_135
    if-eqz p3, :cond_10

    .line 1419
    check-cast p3, Lcom/google/googlenav/be;

    .line 1420
    invoke-direct {p0, p3, v9}, LaN/bj;->a(Lcom/google/googlenav/be;Z)V

    goto/16 :goto_10

    .line 1425
    :sswitch_13e
    if-eqz p3, :cond_c

    move-object v0, p3

    .line 1426
    check-cast v0, LaD/S;

    .line 1427
    invoke-direct {p0, v0}, LaN/bj;->a(LaD/S;)V

    goto/16 :goto_c

    .line 1431
    :sswitch_148
    iget-object v1, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {p0}, LaN/bj;->af()Z

    move-result v2

    if-eqz v2, :cond_160

    :goto_154
    invoke-virtual {p0, v0, v6}, LaN/bj;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1434
    invoke-virtual {p0}, LaN/bj;->W()V

    goto/16 :goto_10

    .line 1431
    :cond_160
    const/16 v0, 0x8

    goto :goto_154

    .line 1437
    :sswitch_163
    invoke-super {p0, p1, p2, p3}, LaN/m;->a(IILjava/lang/Object;)Z

    move-result v5

    goto/16 :goto_10

    .line 1439
    :sswitch_169
    invoke-virtual {p0}, LaN/bj;->bE()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->f()V

    goto/16 :goto_10

    .line 1274
    :sswitch_data_172
    .sparse-switch
        0x2 -> :sswitch_41
        0x2bc -> :sswitch_11
        0x2c4 -> :sswitch_53
        0x2c5 -> :sswitch_4f
        0x2c6 -> :sswitch_70
        0x2c7 -> :sswitch_78
        0x2c8 -> :sswitch_9f
        0x2ca -> :sswitch_aa
        0x2cd -> :sswitch_e3
        0x2d0 -> :sswitch_8b
        0x2d1 -> :sswitch_c1
        0x2d3 -> :sswitch_b5
        0x2d4 -> :sswitch_20
        0x2d5 -> :sswitch_135
        0x2d6 -> :sswitch_12c
        0x2d7 -> :sswitch_13e
        0x2ef -> :sswitch_60
        0x3f9 -> :sswitch_148
        0x6a4 -> :sswitch_163
        0x713 -> :sswitch_c1
        0x76e -> :sswitch_169
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/J;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2482
    iget-object v0, p0, LaN/bj;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_2c

    iget-object v0, p0, LaN/bj;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_2c

    .line 2484
    sget-object v0, LaN/bj;->z:Lcom/google/googlenav/ui/view/android/aD;

    if-eqz v0, :cond_14

    .line 2485
    sget-object v0, LaN/bj;->z:Lcom/google/googlenav/ui/view/android/aD;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aD;->dismiss()V

    .line 2486
    sput-object v1, LaN/bj;->z:Lcom/google/googlenav/ui/view/android/aD;

    .line 2490
    :cond_14
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v1}, LaN/bj;->a(ILjava/lang/Object;)V

    .line 2491
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const-string v1, "s"

    const-string v2, "c"

    invoke-virtual {p0}, LaN/bj;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, LaN/bj;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2494
    const/4 v0, 0x1

    .line 2496
    :goto_2b
    return v0

    :cond_2c
    invoke-super {p0, p1}, LaN/m;->a(Lcom/google/googlenav/ui/view/J;)Z

    move-result v0

    goto :goto_2b
.end method

.method protected a(Ljava/io/DataInput;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 1026
    invoke-static {p1}, Lcom/google/googlenav/aZ;->b(Ljava/io/DataInput;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1027
    invoke-virtual {v0, v7}, Lcom/google/googlenav/aZ;->b(Z)V

    .line 1028
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v1

    iput-object v1, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    .line 1029
    new-instance v1, Lcom/google/googlenav/n;

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    iget-object v4, p0, LaN/bj;->Q:Lau/k;

    iget-object v5, p0, LaN/bj;->c:Lau/p;

    iget-object v6, p0, LaN/bj;->d:Lau/u;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;Lau/o;Lau/p;Lau/u;)V

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    iput-object v1, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    .line 1031
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1035
    iget-object v1, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ab()Lcom/google/googlenav/android/Y;

    move-result-object v1

    new-instance v2, LaN/bo;

    invoke-direct {v2, p0, v0}, LaN/bo;-><init>(LaN/bj;Lcom/google/googlenav/aZ;)V

    invoke-virtual {v1, v2, v7}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V

    .line 1044
    :cond_36
    return v7
.end method

.method public aB()Z
    .registers 2

    .prologue
    .line 2364
    const/4 v0, 0x1

    return v0
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 2369
    invoke-virtual {p0}, LaN/bj;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_20

    :cond_1e
    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public aD()Z
    .registers 2

    .prologue
    .line 2377
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    return v0
.end method

.method public aF()I
    .registers 3

    .prologue
    .line 572
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    .line 574
    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v0

    if-nez v0, :cond_16

    .line 575
    const v0, 0x7f110014

    .line 582
    :goto_15
    return v0

    .line 576
    :cond_16
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 579
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2d

    const v0, 0x7f110013

    goto :goto_15

    :cond_2d
    const v0, 0x7f110015

    goto :goto_15

    .line 582
    :cond_31
    invoke-super {p0}, LaN/m;->aF()I

    move-result v0

    goto :goto_15
.end method

.method public aJ()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 588
    const/16 v0, 0x4eb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .registers 4

    .prologue
    .line 2333
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2335
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2336
    invoke-virtual {p0, v1}, LaN/bj;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    .line 2344
    :cond_e
    :goto_e
    return-object v0

    .line 2339
    :cond_f
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_22

    .line 2340
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 2343
    :cond_22
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    .line 2344
    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {p0, v1}, LaN/bj;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    goto :goto_e
.end method

.method public aL()LT/f;
    .registers 3

    .prologue
    .line 2349
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2350
    iget-object v0, p0, LaN/bj;->a:Lcom/google/googlenav/ui/bq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bq;->p()LT/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bq;->ad:C

    invoke-interface {v0, v1}, LT/g;->e(C)LT/f;

    move-result-object v0

    .line 2352
    :goto_16
    return-object v0

    :cond_17
    iget-object v0, p0, LaN/bj;->a:Lcom/google/googlenav/ui/bq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bq;->p()LT/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bq;->ae:C

    invoke-interface {v0, v1}, LT/g;->e(C)LT/f;

    move-result-object v0

    goto :goto_16
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 2387
    const/4 v0, 0x1

    return v0
.end method

.method protected aN()Z
    .registers 2

    .prologue
    .line 2392
    const/4 v0, 0x1

    return v0
.end method

.method public aT()Z
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 2405
    iget v0, p0, LaN/bj;->C:I

    invoke-static {v0, v2}, LaN/am;->a(IZ)V

    .line 2406
    invoke-static {p0}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/h;)V

    .line 2411
    iget-object v0, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 2412
    invoke-virtual {p0}, LaN/bj;->f()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 2413
    iget-object v0, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, p0}, Lau/k;->a(Lau/m;)V

    .line 2415
    :cond_1c
    iget-object v0, p0, LaN/bj;->c:Lau/p;

    iget-object v1, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, v1}, Lau/p;->a(Lau/k;)V

    .line 2418
    :cond_23
    return v2
.end method

.method public aU()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2450
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    if-eqz v0, :cond_13

    .line 2451
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    move-result-object v0

    invoke-interface {v0}, Lay/m;->d()Lay/t;

    move-result-object v0

    invoke-interface {v0, p0}, Lay/t;->b(Lay/B;)V

    .line 2453
    :cond_13
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaN/bj;->l(Z)V

    .line 2454
    invoke-direct {p0}, LaN/bj;->cl()V

    .line 2455
    invoke-direct {p0}, LaN/bj;->ch()V

    .line 2457
    invoke-static {p0}, Lcom/google/googlenav/common/j;->c(Lcom/google/googlenav/common/h;)V

    .line 2458
    iget v0, p0, LaN/bj;->C:I

    invoke-static {v0, v1}, LaN/am;->a(IZ)V

    .line 2463
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/aZ;->a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/bj;->e(Lcom/google/googlenav/aZ;)V

    .line 2464
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 2466
    iget-object v0, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, p0}, Lau/k;->b(Lau/m;)V

    .line 2467
    iget-object v0, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0}, Lau/k;->b()Z

    move-result v0

    if-nez v0, :cond_4b

    .line 2468
    iget-object v0, p0, LaN/bj;->c:Lau/p;

    iget-object v1, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, v1}, Lau/p;->b(Lau/k;)V

    .line 2472
    :cond_4b
    iget-object v0, p0, LaN/bj;->X:LaN/ah;

    if-eqz v0, :cond_5e

    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    if-eqz v0, :cond_5e

    .line 2473
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    iget-object v1, p0, LaN/bj;->X:LaN/ah;

    invoke-virtual {v0, v1}, LaN/am;->a(LaN/au;)V

    .line 2476
    :cond_5e
    iput-object v2, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    .line 2477
    invoke-super {p0}, LaN/m;->aU()V

    .line 2478
    return-void
.end method

.method public aW()V
    .registers 3

    .prologue
    .line 2423
    invoke-super {p0}, LaN/m;->aW()V

    .line 2425
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()Z

    move-result v0

    if-nez v0, :cond_17

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 2427
    :cond_17
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2428
    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_38

    invoke-static {v0}, Lab/b;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_38

    .line 2429
    sget-object v1, Lcom/google/googlenav/ui/aZ;->bG:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/bj;->a(Lcom/google/googlenav/ui/ba;)V

    .line 2432
    :cond_38
    return-void
.end method

.method public aX()V
    .registers 3

    .prologue
    .line 2436
    invoke-super {p0}, LaN/m;->aX()V

    .line 2437
    invoke-direct {p0}, LaN/bj;->cg()V

    .line 2438
    invoke-direct {p0}, LaN/bj;->cl()V

    .line 2439
    invoke-direct {p0}, LaN/bj;->ch()V

    .line 2441
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 2443
    :cond_20
    const/16 v0, 0x4e8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->bF:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/bj;->a(Lcom/google/googlenav/ui/ba;)V

    .line 2446
    :cond_2f
    return-void
.end method

.method protected am()V
    .registers 9

    .prologue
    .line 519
    invoke-virtual {p0}, LaN/bj;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 520
    if-eqz v0, :cond_5d

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_5d

    move-object v1, v0

    .line 521
    check-cast v1, Lcom/google/googlenav/W;

    .line 522
    const/16 v2, 0x43

    const-string v3, "o"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "l="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "i="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v4}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 527
    iget-object v1, p0, LaN/bj;->R:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Lad/g;

    .line 531
    :goto_5c
    return-void

    .line 529
    :cond_5d
    invoke-super {p0}, LaN/m;->am()V

    goto :goto_5c
.end method

.method public ao()Z
    .registers 3

    .prologue
    .line 535
    invoke-virtual {p0}, LaN/bj;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_1c

    invoke-virtual {p0}, LaN/bj;->aj()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_1c

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->q()Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method protected aq()V
    .registers 2

    .prologue
    .line 546
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaN/bj;->d(Z)V

    .line 548
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bT;-><init>(LaN/bj;)V

    iput-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    .line 556
    invoke-virtual {p0}, LaN/bj;->bF()V

    .line 557
    return-void
.end method

.method public au()Z
    .registers 2

    .prologue
    .line 2094
    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2095
    const/4 v0, 0x0

    .line 2097
    :goto_7
    return v0

    :cond_8
    invoke-super {p0}, LaN/m;->au()Z

    move-result v0

    goto :goto_7
.end method

.method public av()I
    .registers 2

    .prologue
    .line 2382
    const/4 v0, 0x0

    return v0
.end method

.method public ay()Z
    .registers 2

    .prologue
    .line 2359
    iget-boolean v0, p0, LaN/bj;->p:Z

    if-nez v0, :cond_e

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public b(Lau/B;)I
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v1, -0x1

    .line 1178
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 1179
    if-ltz v0, :cond_17

    iget-object v2, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v6}, LaN/bj;->a(Lcom/google/googlenav/E;Lau/B;LT/e;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1214
    :cond_16
    :goto_16
    return v0

    .line 1185
    :cond_17
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v2

    .line 1186
    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    :goto_20
    if-ge v0, v3, :cond_2f

    .line 1187
    invoke-virtual {v2, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-virtual {p0, v4, p1, v6}, LaN/bj;->a(Lcom/google/googlenav/E;Lau/B;LT/e;)Z

    move-result v4

    if-nez v4, :cond_16

    .line 1186
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 1193
    :cond_2f
    invoke-virtual {p0}, LaN/bj;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_3b

    move v0, v1

    .line 1194
    goto :goto_16

    .line 1198
    :cond_3b
    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    .line 1199
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v3

    .line 1200
    invoke-virtual {v3, p1}, Lcom/google/googlenav/T;->a(Lau/B;)Ljava/util/Enumeration;

    move-result-object v4

    .line 1201
    if-nez v4, :cond_4b

    move v0, v1

    .line 1202
    goto :goto_16

    .line 1204
    :cond_4b
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 1205
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    .line 1206
    invoke-virtual {p0, v0, p1, v6}, LaN/bj;->a(Lcom/google/googlenav/E;Lau/B;LT/e;)Z

    move-result v5

    if-eqz v5, :cond_4b

    .line 1207
    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v2

    goto :goto_16

    :cond_67
    move v0, v1

    .line 1214
    goto :goto_16
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2035
    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    packed-switch v1, :pswitch_data_24

    .line 2048
    :cond_8
    :goto_8
    return v0

    .line 2037
    :pswitch_9
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aH()Z

    move-result v1

    if-nez v1, :cond_8

    .line 2043
    iget-object v0, p0, LaN/bj;->a:Lcom/google/googlenav/ui/bq;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bq;->d(B)I

    move-result v0

    neg-int v0, v0

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x8

    goto :goto_8

    .line 2035
    nop

    :pswitch_data_24
    .packed-switch 0xc
        :pswitch_9
    .end packed-switch
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .registers 2

    .prologue
    .line 384
    iget-object v0, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    return-object v0
.end method

.method protected b(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2106
    invoke-super {p0, p1, p2}, LaN/m;->b(ILjava/lang/Object;)V

    .line 2112
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->n()Lcom/google/googlenav/ui/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/av;->d()V

    .line 2113
    return-void
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .registers 3
    .parameter

    .prologue
    .line 2243
    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/bj;->B:Z

    .line 2244
    return-void
.end method

.method public b(Lcom/google/googlenav/aZ;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 602
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_21

    .line 603
    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 604
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bT()Ljava/lang/String;

    move-result-object v0

    .line 605
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 606
    if-eqz v0, :cond_21

    sget-boolean v1, Lcom/google/googlenav/aZ;->a:Z

    if-eqz v1, :cond_21

    .line 607
    invoke-direct {p0, p1, p2, v0}, LaN/bj;->a(Lcom/google/googlenav/aZ;ZLjava/lang/String;)V

    .line 613
    :cond_21
    invoke-direct {p0, p1, p2, v2}, LaN/bj;->a(Lcom/google/googlenav/aZ;ZZ)V

    .line 614
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 2593
    invoke-virtual {p0, v0}, LaN/bj;->e(Z)V

    .line 2594
    invoke-virtual {p0, v0}, LaN/bj;->d(Z)V

    .line 2595
    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 1919
    const/4 v0, 0x5

    const-string v1, "v"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "v"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1923
    return-void
.end method

.method protected b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1223
    invoke-direct {p0}, LaN/bj;->ci()V

    .line 1224
    invoke-direct {p0}, LaN/bj;->cm()V

    .line 1225
    invoke-direct {p0}, LaN/bj;->cj()V

    .line 1226
    invoke-super {p0, p1}, LaN/m;->b(Z)V

    .line 1227
    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2693
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2702
    :cond_7
    :goto_7
    return v0

    .line 2697
    :cond_8
    invoke-virtual {p0, p1}, LaN/bj;->l(Lcom/google/googlenav/ai;)I

    move-result v1

    .line 2698
    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 2702
    const/4 v0, 0x1

    goto :goto_7
.end method

.method protected bC()I
    .registers 2

    .prologue
    .line 2187
    iget v0, p0, LaN/bj;->D:I

    return v0
.end method

.method protected bF()V
    .registers 4

    .prologue
    .line 834
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ak()LZ/c;

    move-result-object v0

    .line 835
    if-eqz v0, :cond_10

    .line 836
    new-instance v1, LaN/bn;

    invoke-direct {v1, p0, v0}, LaN/bn;-><init>(LaN/bj;LZ/c;)V

    invoke-virtual {v1}, LaN/bn;->g()V

    .line 845
    :cond_10
    invoke-virtual {p0}, LaN/bj;->bT()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aL()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3d

    .line 849
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v0

    invoke-virtual {v0}, Lau/H;->a()Lau/B;

    move-result-object v0

    .line 850
    new-instance v1, Lcom/google/googlenav/aS;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aS;-><init>(Lau/B;I)V

    .line 851
    invoke-virtual {v1, p0}, Lcom/google/googlenav/aS;->a(Lcom/google/googlenav/aT;)V

    .line 852
    invoke-static {}, Lad/h;->a()Lad/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Lad/h;->c(Lad/g;)V

    .line 856
    :goto_3c
    return-void

    .line 854
    :cond_3d
    const/4 v0, 0x0

    iput-object v0, p0, LaN/bj;->O:Ljava/lang/String;

    goto :goto_3c
.end method

.method protected bG()Lcom/google/googlenav/aZ;
    .registers 3

    .prologue
    .line 2216
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/google/googlenav/aZ;->a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public bH()LaN/ah;
    .registers 2

    .prologue
    .line 377
    iget-object v0, p0, LaN/bj;->X:LaN/ah;

    return-object v0
.end method

.method public bI()V
    .registers 5

    .prologue
    .line 885
    const/4 v0, 0x0

    .line 886
    iget-object v1, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    instance-of v1, v1, Lai/p;

    if-eqz v1, :cond_b

    .line 887
    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lai/p;

    .line 889
    :cond_b
    iget-object v1, p0, LaN/bj;->y:Lai/s;

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    sget-object v3, LaN/bj;->u:Lcom/google/common/base/D;

    invoke-virtual {v1, v2, v3, v0}, Lai/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/D;Lai/p;)V

    .line 891
    return-void
.end method

.method public bJ()V
    .registers 7

    .prologue
    .line 959
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 960
    new-instance v2, LaN/bv;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, LaN/bv;-><init>(LaN/bk;)V

    .line 961
    iget-object v3, p0, LaN/bj;->y:Lai/s;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lai/p;

    invoke-virtual {v3, v4, v2, v0}, Lai/s;->b(Ljava/lang/Iterable;Lcom/google/common/base/D;Lai/p;)V

    .line 964
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "p="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, LaN/bv;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "s="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, LaN/bv;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, LaN/bv;->c()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 972
    const/16 v1, 0x5e

    const-string v2, "r"

    invoke-static {v1, v2, v0}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 974
    return-void
.end method

.method protected bK()V
    .registers 4

    .prologue
    .line 1881
    const/4 v0, 0x5

    const-string v1, "0"

    invoke-direct {p0}, LaN/bj;->cc()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1884
    return-void
.end method

.method protected bL()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1980
    iget-object v1, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    if-eqz v1, :cond_e

    iget-object v1, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-le v1, v0, :cond_e

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public bM()V
    .registers 3

    .prologue
    .line 2191
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LaN/bj;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2192
    return-void
.end method

.method protected bN()V
    .registers 5

    .prologue
    .line 2199
    iget-boolean v0, p0, LaN/bj;->B:Z

    if-eqz v0, :cond_5

    .line 2209
    :goto_4
    return-void

    .line 2205
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/bj;->B:Z

    .line 2207
    invoke-virtual {p0}, LaN/bj;->bG()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 2208
    iget-object v1, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/ui/v;->a(Lcom/google/googlenav/aZ;IZ)V

    goto :goto_4
.end method

.method public bO()Z
    .registers 3

    .prologue
    .line 2220
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_22

    const/16 v0, 0x12c

    .line 2222
    :goto_c
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->am()I

    move-result v1

    if-lez v1, :cond_25

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    if-ge v1, v0, :cond_25

    const/4 v0, 0x1

    :goto_21
    return v0

    .line 2220
    :cond_22
    const/16 v0, 0x64

    goto :goto_c

    .line 2222
    :cond_25
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public bP()Lcom/google/googlenav/aZ;
    .registers 2

    .prologue
    .line 2264
    invoke-direct {p0}, LaN/bj;->cf()Lcom/google/googlenav/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/n;->a()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aZ;

    return-object v0
.end method

.method public bQ()Lcom/google/googlenav/T;
    .registers 2

    .prologue
    .line 2287
    invoke-direct {p0}, LaN/bj;->cf()Lcom/google/googlenav/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/n;->b()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/T;

    return-object v0
.end method

.method public bR()I
    .registers 2

    .prologue
    .line 2584
    iget v0, p0, LaN/bj;->C:I

    return v0
.end method

.method public bS()Lcom/google/googlenav/ui/view/K;
    .registers 2

    .prologue
    .line 2754
    iget-object v0, p0, LaN/bj;->N:Lcom/google/googlenav/ui/view/K;

    if-nez v0, :cond_b

    .line 2755
    new-instance v0, LaN/bq;

    invoke-direct {v0, p0}, LaN/bq;-><init>(LaN/bj;)V

    iput-object v0, p0, LaN/bj;->N:Lcom/google/googlenav/ui/view/K;

    .line 2767
    :cond_b
    iget-object v0, p0, LaN/bj;->N:Lcom/google/googlenav/ui/view/K;

    return-object v0
.end method

.method public bT()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2808
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v3

    .line 2809
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aB()Lau/B;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->U()Lau/H;

    move-result-object v4

    invoke-virtual {v4}, Lau/H;->a()Lau/B;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/aX;->a(Lau/B;Lau/B;)Z

    move-result v0

    if-eqz v0, :cond_36

    move v0, v1

    .line 2812
    :goto_1f
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_38

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aL()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_35

    if-eqz v0, :cond_38

    :cond_35
    :goto_35
    return v1

    :cond_36
    move v0, v2

    .line 2809
    goto :goto_1f

    :cond_38
    move v1, v2

    .line 2812
    goto :goto_35
.end method

.method public bg()Z
    .registers 3

    .prologue
    .line 2599
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public bh()Z
    .registers 2

    .prologue
    .line 2607
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->m()Z

    move-result v0

    return v0
.end method

.method public bi()Z
    .registers 2

    .prologue
    .line 2617
    const/4 v0, 0x1

    return v0
.end method

.method public bl()Z
    .registers 3

    .prologue
    .line 2629
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public bm()Z
    .registers 2

    .prologue
    .line 2635
    const/4 v0, 0x0

    return v0
.end method

.method protected bn()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1612
    const-string v0, "p"

    return-object v0
.end method

.method protected bo()V
    .registers 3

    .prologue
    .line 1648
    invoke-virtual {p0}, LaN/bj;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1649
    if-eqz v0, :cond_17

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ae()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 1650
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    .line 1651
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/ai;)V

    .line 1653
    :cond_17
    return-void
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 4
    .parameter

    .prologue
    .line 2054
    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v0

    sparse-switch v0, :sswitch_data_60

    move-object v0, p1

    .line 2081
    check-cast v0, Lcom/google/googlenav/ai;

    .line 2083
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bo()Z

    move-result v1

    if-eqz v1, :cond_5a

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bp()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->f()I

    move-result v0

    .line 2085
    :goto_18
    rsub-int/lit8 v0, v0, 0x1

    :goto_1a
    return v0

    .line 2057
    :sswitch_1b
    const/4 v0, 0x0

    goto :goto_1a

    .line 2059
    :sswitch_1d
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aH()Z

    move-result v0

    if-nez v0, :cond_33

    .line 2061
    iget-object v0, p0, LaN/bj;->a:Lcom/google/googlenav/ui/bq;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bq;->c(B)I

    move-result v0

    neg-int v0, v0

    goto :goto_1a

    .line 2067
    :cond_33
    invoke-virtual {p0, p1}, LaN/bj;->f(Lcom/google/googlenav/E;)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 2072
    :sswitch_3a
    invoke-virtual {p0, p1}, LaN/bj;->f(Lcom/google/googlenav/E;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1a

    :sswitch_43
    move-object v0, p1

    .line 2077
    check-cast v0, Lcom/google/googlenav/ai;

    iget-object v1, p0, LaN/bj;->t:LaP/a;

    check-cast v1, LaP/i;

    invoke-virtual {v1}, LaP/i;->b()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    .line 2079
    invoke-virtual {p0, p1}, LaN/bj;->f(Lcom/google/googlenav/E;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 2083
    :cond_5a
    invoke-virtual {p0, p1}, LaN/bj;->f(Lcom/google/googlenav/E;)I

    move-result v0

    goto :goto_18

    .line 2054
    nop

    :sswitch_data_60
    .sparse-switch
        0x0 -> :sswitch_1b
        0xc -> :sswitch_1d
        0xf -> :sswitch_3a
        0x10 -> :sswitch_43
        0x11 -> :sswitch_43
        0x12 -> :sswitch_43
        0x13 -> :sswitch_43
    .end sparse-switch
.end method

.method public c()V
    .registers 2

    .prologue
    .line 396
    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/bj;->S:Z

    .line 397
    return-void
.end method

.method public c(Lcom/google/googlenav/F;)V
    .registers 5
    .parameter

    .prologue
    .line 437
    invoke-virtual {p0, p1}, LaN/bj;->a(Lcom/google/googlenav/F;)V

    .line 438
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LaN/bj;->b(B)V

    .line 439
    invoke-virtual {p0}, LaN/bj;->R()V

    .line 441
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 442
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->W()I

    move-result v1

    .line 445
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/T;->g()V

    .line 448
    const/4 v2, 0x0

    iput-boolean v2, p0, LaN/bj;->B:Z

    .line 451
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v0

    iput-object v0, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    .line 453
    iget-object v0, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, p0}, Lau/k;->b(Lau/m;)V

    .line 454
    iget-object v0, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0}, Lau/k;->b()Z

    move-result v0

    if-nez v0, :cond_36

    .line 455
    iget-object v0, p0, LaN/bj;->c:Lau/p;

    iget-object v2, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, v2}, Lau/p;->b(Lau/k;)V

    .line 463
    :cond_36
    iget-object v0, p0, LaN/bj;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 464
    invoke-virtual {p0}, LaN/bj;->f()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 465
    iget-object v0, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, p0}, Lau/k;->a(Lau/m;)V

    .line 467
    :cond_49
    iget-object v0, p0, LaN/bj;->c:Lau/p;

    iget-object v2, p0, LaN/bj;->Q:Lau/k;

    invoke-virtual {v0, v2}, Lau/p;->a(Lau/k;)V

    .line 471
    :cond_50
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/bj;->b(I)V

    .line 473
    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v0

    if-eqz v0, :cond_80

    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/bT;

    if-eqz v0, :cond_80

    .line 477
    const/16 v0, 0xa

    if-eq v1, v0, :cond_72

    .line 478
    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->l()V

    .line 480
    :cond_72
    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->m()V

    .line 481
    iget-object v0, p0, LaN/bj;->r:Lcom/google/googlenav/ui/view/android/aY;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->h()V

    .line 485
    :cond_80
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()Z

    move-result v0

    if-nez v0, :cond_94

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_a9

    .line 487
    :cond_94
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->bG:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/ba;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aZ;)Lcom/google/googlenav/ui/ba;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/bj;->a(Lcom/google/googlenav/ui/ba;)V

    .line 492
    :cond_a9
    invoke-direct {p0}, LaN/bj;->bX()Lau/H;

    .line 495
    invoke-direct {p0}, LaN/bj;->bY()V

    .line 496
    return-void
.end method

.method protected c(Laa/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1714
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_19

    invoke-virtual {p0}, LaN/bj;->aa()Z

    move-result v1

    if-eqz v1, :cond_19

    iget-boolean v1, p0, LaN/bj;->E:Z

    if-eqz v1, :cond_19

    .line 1716
    iput-boolean v0, p0, LaN/bj;->E:Z

    .line 1717
    invoke-virtual {p0}, LaN/bj;->h()V

    .line 1718
    const/4 v0, 0x1

    .line 1721
    :cond_19
    return v0
.end method

.method protected c(Lcom/google/googlenav/aZ;)Z
    .registers 4
    .parameter

    .prologue
    .line 660
    const-string v0, "19"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    const-string v0, "20"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-virtual {p0}, LaN/bj;->bL()Z

    move-result v0

    if-eqz v0, :cond_30

    :cond_28
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_30

    const/4 v0, 0x1

    :goto_2f
    return v0

    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method public d(Lcom/google/googlenav/aZ;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 1933
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1934
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->Q()Ljava/lang/String;

    move-result-object v0

    .line 1947
    :goto_e
    return-object v0

    .line 1935
    :cond_f
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1940
    const/16 v0, 0x203

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 1941
    :cond_20
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_37

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 1945
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 1947
    :cond_37
    const/16 v0, 0x46d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_e
.end method

.method public d()[Lcom/google/googlenav/ui/aL;
    .registers 2

    .prologue
    .line 2304
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ai()[Lcom/google/googlenav/ui/aK;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 2588
    invoke-virtual {p0}, LaN/bj;->bR()I

    move-result v0

    return v0
.end method

.method protected e(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 2276
    new-instance v0, Lcom/google/googlenav/n;

    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    iput-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    .line 2277
    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/u;)V
    .registers 3
    .parameter

    .prologue
    .line 1104
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LaN/bj;->a(Lcom/google/googlenav/ui/u;Lcom/google/googlenav/F;)V

    .line 1105
    return-void
.end method

.method public e(Laa/a;)Z
    .registers 10
    .parameter

    .prologue
    const/16 v7, 0x36

    const/16 v6, 0x34

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 1735
    invoke-virtual {p1}, Laa/a;->e()C

    move-result v5

    .line 1738
    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1799
    :cond_11
    :goto_11
    return v1

    .line 1743
    :cond_12
    if-eq v5, v7, :cond_16

    if-ne v5, v6, :cond_11

    .line 1744
    :cond_16
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    .line 1749
    invoke-virtual {p0}, LaN/bj;->ah()Z

    move-result v4

    if-eqz v4, :cond_58

    if-eq v0, v3, :cond_58

    .line 1782
    :cond_26
    if-ltz v0, :cond_32

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ne v0, v3, :cond_33

    :cond_32
    move v0, v1

    .line 1786
    :cond_33
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1789
    if-ltz v0, :cond_11

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ge v0, v3, :cond_11

    .line 1792
    invoke-virtual {p0, v0}, LaN/bj;->b(I)V

    .line 1793
    invoke-virtual {p0}, LaN/bj;->an()Z

    .line 1794
    invoke-virtual {p0, v2}, LaN/bj;->b(Z)V

    move v1, v2

    .line 1795
    goto :goto_11

    .line 1754
    :cond_58
    invoke-virtual {p0}, LaN/bj;->ah()Z

    move-result v4

    if-nez v4, :cond_9b

    .line 1760
    if-ne v5, v6, :cond_9d

    invoke-direct {p0}, LaN/bj;->cd()Z

    move-result v4

    if-eqz v4, :cond_9d

    .line 1761
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    move v4, v0

    .line 1774
    :goto_6f
    if-ne v5, v6, :cond_a7

    move v0, v3

    :goto_72
    add-int/2addr v0, v4

    .line 1776
    if-ltz v0, :cond_26

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    if-ge v0, v4, :cond_26

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v4

    if-eqz v4, :cond_9b

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/E;->c()B

    move-result v4

    if-nez v4, :cond_26

    :cond_9b
    move v4, v0

    goto :goto_6f

    .line 1762
    :cond_9d
    if-ne v5, v7, :cond_9b

    invoke-direct {p0}, LaN/bj;->ce()Z

    move-result v4

    if-eqz v4, :cond_9b

    move v4, v3

    .line 1763
    goto :goto_6f

    :cond_a7
    move v0, v2

    .line 1774
    goto :goto_72
.end method

.method public f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;
    .registers 5
    .parameter

    .prologue
    .line 2937
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_1d

    .line 2938
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 2939
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 2943
    :goto_18
    return-object v0

    .line 2937
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2943
    :cond_1d
    const/4 v0, 0x0

    goto :goto_18
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 2400
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->at()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected f(Laa/a;)Z
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/16 v2, 0x23

    const/4 v0, 0x1

    .line 1662
    invoke-virtual {p1}, Laa/a;->e()C

    move-result v1

    if-ne v1, v2, :cond_4b

    .line 1663
    invoke-virtual {p0}, LaN/bj;->ae()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1664
    const-string v1, "m"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LaN/bj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1667
    invoke-direct {p0}, LaN/bj;->cb()V

    .line 1668
    invoke-virtual {p0, v4, v3}, LaN/bj;->b(ILjava/lang/Object;)V

    .line 1702
    :cond_21
    :goto_21
    return v0

    .line 1670
    :cond_22
    invoke-virtual {p0}, LaN/bj;->af()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1671
    const-string v1, "l"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LaN/bj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1673
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v3}, LaN/bj;->b(ILjava/lang/Object;)V

    goto :goto_21

    .line 1675
    :cond_36
    invoke-virtual {p0}, LaN/bj;->bL()Z

    move-result v1

    if-eqz v1, :cond_4b

    .line 1676
    const-string v1, "l"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LaN/bj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1679
    const/16 v1, 0x9

    invoke-virtual {p0, v1, v3}, LaN/bj;->c(ILjava/lang/Object;)V

    goto :goto_21

    .line 1684
    :cond_4b
    invoke-virtual {p0, p1}, LaN/bj;->g(Laa/a;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 1688
    invoke-virtual {p0, p1}, LaN/bj;->d(Laa/a;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 1694
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v1

    if-ne v1, v4, :cond_66

    invoke-virtual {p0}, LaN/bj;->aa()Z

    move-result v1

    if-eqz v1, :cond_66

    .line 1698
    iput-boolean v0, p0, LaN/bj;->E:Z

    goto :goto_21

    .line 1702
    :cond_66
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public g(I)Z
    .registers 3
    .parameter

    .prologue
    .line 2502
    packed-switch p1, :pswitch_data_16

    .line 2509
    invoke-super {p0, p1}, LaN/m;->g(I)Z

    move-result v0

    :goto_7
    return v0

    .line 2506
    :pswitch_8
    invoke-virtual {p0}, LaN/bj;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->k()Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    goto :goto_7

    :cond_14
    const/4 v0, 0x0

    goto :goto_7

    .line 2502
    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method protected final g(Laa/a;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1811
    invoke-virtual {p0}, LaN/bj;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1814
    if-nez v0, :cond_d

    move v0, v1

    .line 1836
    :goto_c
    return v0

    .line 1818
    :cond_d
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_20

    invoke-virtual {p0}, LaN/bj;->af()Z

    move-result v3

    if-eqz v3, :cond_20

    .line 1819
    invoke-virtual {p0, v1, v5}, LaN/bj;->b(ILjava/lang/Object;)V

    move v0, v2

    .line 1820
    goto :goto_c

    .line 1823
    :cond_20
    invoke-direct {p0, v0}, LaN/bj;->m(Lcom/google/googlenav/ai;)Z

    move-result v3

    if-nez v3, :cond_28

    move v0, v1

    .line 1825
    goto :goto_c

    .line 1828
    :cond_28
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_4d

    invoke-virtual {p0}, LaN/bj;->ag()Z

    move-result v3

    if-eqz v3, :cond_4d

    .line 1830
    const/16 v1, 0x9

    invoke-virtual {p0, v1, v5}, LaN/bj;->a(ILjava/lang/Object;)V

    .line 1831
    invoke-static {v0}, Lcom/google/googlenav/bQ;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    .line 1832
    iget-object v1, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    const-string v3, "s"

    const-string v4, "k"

    invoke-virtual {p0, v1, v3, v4, v0}, LaN/bj;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1834
    goto :goto_c

    :cond_4d
    move v0, v1

    .line 1836
    goto :goto_c
.end method

.method public g(Lcom/google/googlenav/aZ;)Z
    .registers 4
    .parameter

    .prologue
    .line 2995
    invoke-virtual {p0, p1}, LaN/bj;->h(Lcom/google/googlenav/aZ;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public h(Lcom/google/googlenav/aZ;)I
    .registers 4
    .parameter

    .prologue
    .line 3006
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 3007
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 3008
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aH()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 3012
    :goto_14
    return v1

    .line 3006
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3012
    :cond_19
    const/4 v1, -0x1

    goto :goto_14
.end method

.method public h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2117
    invoke-virtual {p0}, LaN/bj;->bz()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2183
    :cond_8
    :goto_8
    return-void

    .line 2121
    :cond_9
    iput-boolean v2, p0, LaN/bj;->B:Z

    .line 2123
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 2124
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_b6

    .line 2182
    invoke-virtual {p0, v0}, LaN/bj;->b(Lcom/google/googlenav/ui/wizard/A;)Z

    goto :goto_8

    .line 2126
    :sswitch_20
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaN/bj;->i(Z)V

    .line 2127
    invoke-virtual {p0, v2, v3}, LaN/bj;->c(ILjava/lang/Object;)V

    .line 2128
    invoke-virtual {p0, v2}, LaN/bj;->i(Z)V

    goto :goto_8

    .line 2132
    :sswitch_2b
    invoke-virtual {p0, v2, v3}, LaN/bj;->b(ILjava/lang/Object;)V

    goto :goto_8

    .line 2136
    :sswitch_2f
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    .line 2137
    if-eqz v0, :cond_42

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_42

    .line 2138
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/bj;->b(I)V

    .line 2140
    :cond_42
    invoke-virtual {p0, v2, v3}, LaN/bj;->a(ILjava/lang/Object;)V

    goto :goto_8

    .line 2145
    :sswitch_46
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->h(LaN/i;)V

    .line 2148
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0}, LaN/am;->J()LaN/i;

    move-result-object v0

    .line 2149
    if-eqz v0, :cond_8

    .line 2150
    invoke-virtual {v0}, LaN/i;->aR()V

    goto :goto_8

    .line 2155
    :sswitch_5f
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->h(LaN/i;)V

    .line 2156
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->i(Ljava/lang/String;)V

    goto :goto_8

    .line 2160
    :sswitch_70
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->h(LaN/i;)V

    .line 2161
    invoke-virtual {p0, v2, v3}, LaN/bj;->b(ILjava/lang/Object;)V

    .line 2162
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->Y()V

    goto :goto_8

    .line 2166
    :sswitch_82
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->b(I)V

    .line 2167
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->h(LaN/i;)V

    goto/16 :goto_8

    .line 2171
    :sswitch_93
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->h(LaN/i;)V

    .line 2172
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0}, LaN/am;->J()LaN/i;

    move-result-object v0

    check-cast v0, LaN/bx;

    .line 2173
    if-eqz v0, :cond_8

    .line 2174
    invoke-virtual {v0}, LaN/bx;->bF()V

    goto/16 :goto_8

    .line 2178
    :sswitch_af
    iget-object v0, p0, LaN/bj;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->y()V

    goto/16 :goto_8

    .line 2124
    :sswitch_data_b6
    .sparse-switch
        0x0 -> :sswitch_46
        0x6 -> :sswitch_5f
        0x7 -> :sswitch_2f
        0x8 -> :sswitch_20
        0x9 -> :sswitch_2b
        0xf -> :sswitch_70
        0x10 -> :sswitch_82
        0x11 -> :sswitch_46
        0x1a -> :sswitch_93
        0x1c -> :sswitch_af
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 507
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, LaN/bj;->R:Lcom/google/googlenav/layer/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Lad/g;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method protected i()LaP/a;
    .registers 2

    .prologue
    .line 1707
    new-instance v0, LaP/i;

    invoke-direct {v0, p0}, LaP/i;-><init>(LaN/i;)V

    return-object v0
.end method

.method protected j(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1617
    const-string v0, "m"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LaN/bj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1618
    iget-object v0, p0, LaN/bj;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 1619
    invoke-virtual {p0, p1}, LaN/bj;->b(I)V

    .line 1625
    const/4 v1, -0x1

    if-eq v0, v1, :cond_25

    invoke-virtual {p0}, LaN/bj;->af()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 1626
    const/4 v1, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LaN/bj;->a(ILjava/lang/Object;)V

    .line 1639
    :cond_24
    :goto_24
    return-void

    .line 1628
    :cond_25
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v2}, LaN/bj;->a(ILjava/lang/Object;)V

    .line 1635
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, LaN/bj;->g:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_24

    .line 1636
    invoke-virtual {p0, v2}, LaN/bj;->a(Ljava/lang/Object;)V

    goto :goto_24
.end method

.method public k(I)V
    .registers 4
    .parameter

    .prologue
    .line 2314
    iget v0, p0, LaN/bj;->C:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, LaN/am;->a(IZ)V

    .line 2315
    iput p1, p0, LaN/bj;->C:I

    .line 2316
    iget v0, p0, LaN/bj;->C:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, LaN/am;->a(IZ)V

    .line 2320
    iget-object v0, p0, LaN/bj;->t:LaP/a;

    check-cast v0, LaP/i;

    invoke-virtual {v0, p1}, LaP/i;->c(I)V

    .line 2321
    return-void
.end method

.method protected k(Z)Z
    .registers 3
    .parameter

    .prologue
    .line 652
    if-nez p1, :cond_4

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public l(Lcom/google/googlenav/ai;)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2721
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2722
    invoke-direct {p0, p1}, LaN/bj;->n(Lcom/google/googlenav/ai;)I

    move-result v2

    .line 2723
    if-eq v2, v0, :cond_15

    .line 2725
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    .line 2726
    sub-int v0, v2, v0

    invoke-direct {p0, v0}, LaN/bj;->m(I)I

    move-result v0

    .line 2728
    :cond_15
    return v0
.end method

.method protected m()V
    .registers 1

    .prologue
    .line 2772
    invoke-super {p0}, LaN/m;->m()V

    .line 2773
    invoke-virtual {p0}, LaN/bj;->bK()V

    .line 2774
    return-void
.end method

.method public q()I
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 993
    invoke-super {p0}, LaN/m;->q()I

    move-result v0

    .line 994
    iget-object v1, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_20

    .line 995
    iget-object v1, p0, LaN/bj;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    .line 1000
    :cond_12
    :goto_12
    iget-object v1, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    if-eqz v1, :cond_1f

    .line 1001
    iget-object v1, p0, LaN/bj;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/ap;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    .line 1003
    :cond_1f
    return v0

    .line 996
    :cond_20
    iget-object v1, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_12

    .line 997
    iget-object v1, p0, LaN/bj;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    goto :goto_12
.end method

.method protected t()Ljava/lang/String;
    .registers 2

    .prologue
    .line 3359
    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bQ;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[SearchLayer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaN/bj;->bP()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected v()V
    .registers 3

    .prologue
    .line 567
    invoke-virtual {p0}, LaN/bj;->bQ()Lcom/google/googlenav/T;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/T;->b(Lau/B;)V

    .line 568
    return-void
.end method
