.class public LaN/bx;
.super LaN/m;
.source "SourceFile"

# interfaces
.implements LaN/k;
.implements Lat/h;


# instance fields
.field protected B:Ljava/lang/String;

.field private C:Lcom/google/googlenav/ui/wizard/dA;

.field private D:I


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3, p4}, LaN/m;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;)V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, LaN/bx;->C:Lcom/google/googlenav/ui/wizard/dA;

    .line 67
    iput v2, p0, LaN/bx;->D:I

    .line 96
    new-instance v0, Lcom/google/googlenav/bz;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    move-result-object v1

    invoke-interface {v1}, Lay/m;->d()Lay/t;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/googlenav/bz;-><init>(Lay/t;LaN/k;)V

    iput-object v0, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    .line 101
    invoke-virtual {p0, v2}, LaN/bx;->i(Z)V

    .line 102
    return-void
.end method

.method private bJ()V
    .registers 4

    .prologue
    .line 524
    const/4 v0, 0x1

    .line 525
    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ab()Lcom/google/googlenav/android/Y;

    move-result-object v1

    new-instance v2, LaN/bB;

    invoke-direct {v2, p0}, LaN/bB;-><init>(LaN/bx;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V

    .line 535
    return-void
.end method


# virtual methods
.method public D_()V
    .registers 1

    .prologue
    .line 546
    invoke-direct {p0}, LaN/bx;->bJ()V

    .line 547
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 551
    invoke-direct {p0}, LaN/bx;->bJ()V

    .line 552
    return-void
.end method

.method public F()I
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 589
    invoke-virtual {p0}, LaN/bx;->az()Z

    move-result v1

    if-eqz v1, :cond_b

    iget v1, p0, LaN/bx;->D:I

    if-gez v1, :cond_c

    .line 593
    :cond_b
    :goto_b
    return v0

    .line 592
    :cond_c
    invoke-virtual {p0, v0}, LaN/bx;->i(Z)V

    .line 593
    iget v0, p0, LaN/bx;->D:I

    goto :goto_b
.end method

.method public F_()V
    .registers 1

    .prologue
    .line 557
    return-void
.end method

.method public L_()V
    .registers 1

    .prologue
    .line 541
    invoke-direct {p0}, LaN/bx;->bJ()V

    .line 542
    return-void
.end method

.method public M_()V
    .registers 1

    .prologue
    .line 562
    return-void
.end method

.method protected O()Z
    .registers 2

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 340
    invoke-super {p0}, LaN/m;->X()Z

    move-result v1

    .line 345
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bz;->a(Z)Z

    move-result v2

    .line 347
    if-nez v1, :cond_11

    if-eqz v2, :cond_12

    :cond_11
    const/4 v0, 0x1

    :cond_12
    return v0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 183
    iput-object p1, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    .line 184
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LaN/bx;->b(I)V

    .line 185
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/u;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 491
    invoke-virtual {p0, p2}, LaN/bx;->b(Ljava/lang/String;)I

    move-result v0

    .line 492
    invoke-virtual {p0}, LaN/bx;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    .line 493
    if-eqz v1, :cond_28

    .line 494
    invoke-interface {v1}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v1

    .line 495
    iget-object v2, p0, LaN/bx;->c:Lau/p;

    iget-object v3, p0, LaN/bx;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, Lau/p;->a(Lau/B;Landroid/graphics/Point;)V

    .line 496
    invoke-virtual {p0, v0}, LaN/bx;->c(I)Lcom/google/googlenav/e;

    move-result-object v0

    iget-object v1, p0, LaN/bx;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, LaN/bx;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v0, p1, v1, v2}, Lcom/google/googlenav/e;->a(Lcom/google/googlenav/e;Lcom/google/googlenav/ui/u;II)V

    .line 499
    :cond_28
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-virtual {p0}, LaN/bx;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bz;

    .line 302
    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    .line 303
    const/4 v2, -0x1

    if-eq v0, v2, :cond_30

    .line 304
    invoke-virtual {p0, v0}, LaN/bx;->b(I)V

    .line 305
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0, p0}, LaN/am;->e(LaN/i;)V

    .line 306
    invoke-virtual {p0}, LaN/bx;->bG()V

    .line 312
    iput v1, p0, LaN/bx;->D:I

    .line 314
    if-eqz p2, :cond_31

    .line 315
    const/16 v0, 0xe

    .line 317
    :goto_23
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LaN/bx;->a(ILjava/lang/Object;)V

    .line 320
    invoke-virtual {p0}, LaN/bx;->bI()Lay/m;

    move-result-object v0

    const-string v1, "s"

    invoke-interface {v0, v1, p1}, Lay/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_30
    return-void

    :cond_31
    move v0, v1

    goto :goto_23
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 211
    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    .line 212
    sparse-switch p1, :sswitch_data_32

    .line 225
    invoke-super {p0, p1, p2, p3}, LaN/m;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_d
    return v0

    .line 214
    :sswitch_e
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->am()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 215
    invoke-virtual {p0}, LaN/bx;->an()Z

    .line 216
    invoke-virtual {p0, v0}, LaN/bx;->b(Z)V

    goto :goto_d

    .line 218
    :cond_1f
    iget-object v1, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, LaN/bx;->a(Ljava/lang/Object;)V

    goto :goto_d

    .line 222
    :sswitch_2d
    invoke-virtual {p0}, LaN/bx;->W()V

    goto :goto_d

    .line 212
    nop

    :sswitch_data_32
    .sparse-switch
        0x1 -> :sswitch_e
        0x3f9 -> :sswitch_2d
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/J;)Z
    .registers 4
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, LaN/bx;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_10

    iget-object v0, p0, LaN/bx;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_10

    .line 231
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LaN/bx;->a(ILjava/lang/Object;)V

    .line 235
    const/4 v0, 0x1

    :goto_f
    return v0

    .line 233
    :cond_10
    invoke-super {p0, p1}, LaN/m;->a(Lcom/google/googlenav/ui/view/J;)Z

    move-result v0

    goto :goto_f
.end method

.method public aE()Z
    .registers 2

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public aU()V
    .registers 2

    .prologue
    .line 106
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bz;->a()V

    .line 107
    invoke-super {p0}, LaN/m;->aU()V

    .line 108
    return-void
.end method

.method public aX()V
    .registers 2

    .prologue
    .line 113
    invoke-virtual {p0}, LaN/bx;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 114
    if-eqz v0, :cond_e

    .line 115
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaN/bx;->B:Ljava/lang/String;

    .line 117
    :cond_e
    invoke-super {p0}, LaN/m;->aX()V

    .line 118
    return-void
.end method

.method protected am()V
    .registers 4

    .prologue
    .line 354
    const/4 v0, 0x0

    .line 355
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bz;->a(Ljava/lang/Runnable;)V

    .line 357
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    move-result-object v0

    const-string v1, "v"

    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/by;->B()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lay/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    return-void
.end method

.method protected aq()V
    .registers 5

    .prologue
    .line 425
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    const-string v2, "stars"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/ui/v;Ljava/lang/String;Z)V

    .line 427
    return-void
.end method

.method public av()I
    .registers 2

    .prologue
    .line 459
    const/4 v0, 0x2

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 442
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/String;)I
    .registers 3
    .parameter

    .prologue
    .line 487
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected bC()I
    .registers 3

    .prologue
    .line 503
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v0

    iget-object v1, p0, LaN/bx;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    .line 504
    if-gez v0, :cond_d

    .line 506
    const/4 v0, -0x2

    .line 508
    :cond_d
    return v0
.end method

.method protected bF()V
    .registers 2

    .prologue
    .line 335
    invoke-virtual {p0}, LaN/bx;->bC()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/bx;->j(I)V

    .line 336
    return-void
.end method

.method public bG()V
    .registers 3

    .prologue
    .line 476
    invoke-virtual {p0}, LaN/bx;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 477
    if-eqz v0, :cond_18

    .line 478
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()Lau/B;

    move-result-object v0

    .line 479
    if-eqz v0, :cond_18

    .line 480
    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->R()V

    .line 481
    iget-object v1, p0, LaN/bx;->d:Lau/u;

    invoke-virtual {v1, v0}, Lau/u;->c(Lau/B;)V

    .line 484
    :cond_18
    return-void
.end method

.method public bH()Lcom/google/googlenav/bz;
    .registers 2

    .prologue
    .line 512
    invoke-virtual {p0}, LaN/bx;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bz;

    return-object v0
.end method

.method public bI()Lay/m;
    .registers 2

    .prologue
    .line 516
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    move-result-object v0

    return-object v0
.end method

.method public bj()Z
    .registers 2

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public bk()Z
    .registers 2

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method protected bv()Z
    .registers 2

    .prologue
    .line 454
    const/4 v0, 0x1

    return v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 448
    invoke-virtual {p0, p1}, LaN/bx;->f(Lcom/google/googlenav/E;)I

    move-result v0

    .line 449
    if-eqz v0, :cond_b

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x2

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public c(LaN/i;)V
    .registers 5
    .parameter

    .prologue
    .line 151
    const/4 v0, 0x1

    .line 152
    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ab()Lcom/google/googlenav/android/Y;

    move-result-object v1

    new-instance v2, LaN/by;

    invoke-direct {v2, p0}, LaN/by;-><init>(LaN/bx;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V

    .line 161
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 570
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    .line 571
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1d

    .line 572
    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v1

    invoke-virtual {v1, p0}, LaN/am;->e(LaN/i;)V

    .line 573
    invoke-virtual {p0, v0}, LaN/bx;->b(I)V

    .line 574
    invoke-virtual {p0}, LaN/bx;->an()Z

    .line 575
    invoke-virtual {p0}, LaN/bx;->bG()V

    .line 579
    :cond_1d
    return-void
.end method

.method public e(ILjava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 435
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    const-string v2, "stars"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jt;->a(Lcom/google/googlenav/ui/v;Ljava/lang/String;Z)V

    .line 437
    return-void
.end method

.method protected f(Laa/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 194
    invoke-virtual {p0}, LaN/bx;->ah()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 195
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1b

    invoke-virtual {p0}, LaN/bx;->ag()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 196
    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LaN/bx;->a(ILjava/lang/Object;)V

    .line 206
    :goto_1a
    return v0

    .line 201
    :cond_1b
    invoke-virtual {p1}, Laa/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_33

    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 203
    invoke-virtual {p0}, LaN/bx;->h()V

    goto :goto_1a

    .line 206
    :cond_33
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public g(I)Z
    .registers 3
    .parameter

    .prologue
    .line 464
    packed-switch p1, :pswitch_data_a

    .line 471
    invoke-super {p0, p1}, LaN/m;->g(I)Z

    move-result v0

    :goto_7
    return v0

    .line 468
    :pswitch_8
    const/4 v0, 0x0

    goto :goto_7

    .line 464
    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 245
    invoke-virtual {p0}, LaN/bx;->bz()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 288
    :cond_8
    :goto_8
    return-void

    .line 250
    :cond_9
    iput-object v3, p0, LaN/bx;->C:Lcom/google/googlenav/ui/wizard/dA;

    .line 252
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 253
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_8c

    .line 286
    invoke-virtual {p0}, LaN/bx;->n()V

    goto :goto_8

    .line 255
    :sswitch_20
    iget-object v0, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LaN/bx;->b(ILjava/lang/Object;)V

    goto :goto_8

    .line 259
    :sswitch_2e
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 260
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/bx;->j(I)V

    goto :goto_8

    .line 264
    :sswitch_4a
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaN/bx;->i(Z)V

    .line 265
    invoke-virtual {p0}, LaN/bx;->l()V

    goto :goto_8

    .line 268
    :sswitch_52
    invoke-virtual {p0}, LaN/bx;->n()V

    .line 269
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->j()V

    goto :goto_8

    .line 272
    :sswitch_5f
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->av()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->L()Lcom/google/googlenav/ui/wizard/jA;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jA;->y()Lcom/google/googlenav/ui/wizard/dj;

    move-result-object v0

    const/16 v1, 0x32c

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/dj;->a(IILjava/lang/Object;)Z

    .line 274
    invoke-virtual {p0}, LaN/bx;->n()V

    goto :goto_8

    .line 277
    :sswitch_76
    invoke-virtual {p0}, LaN/bx;->Z()V

    .line 278
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->i(Ljava/lang/String;)V

    goto :goto_8

    .line 281
    :sswitch_81
    invoke-virtual {p0}, LaN/bx;->Z()V

    .line 283
    iget-object v0, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->y()V

    goto/16 :goto_8

    .line 253
    nop

    :sswitch_data_8c
    .sparse-switch
        0x6 -> :sswitch_76
        0x7 -> :sswitch_2e
        0x9 -> :sswitch_20
        0xe -> :sswitch_4a
        0x10 -> :sswitch_52
        0x16 -> :sswitch_5f
        0x1c -> :sswitch_81
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 364
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v1

    .line 365
    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/google/googlenav/by;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 393
    :cond_19
    :goto_19
    return v0

    .line 369
    :cond_1a
    invoke-virtual {v1}, Lcom/google/googlenav/by;->i()Z

    move-result v1

    if-nez v1, :cond_19

    .line 375
    invoke-virtual {p0}, LaN/bx;->bH()Lcom/google/googlenav/bz;

    move-result-object v0

    new-instance v1, LaN/bz;

    invoke-direct {v1, p0}, LaN/bz;-><init>(LaN/bx;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bz;->a(Ljava/lang/Runnable;)V

    .line 393
    const/4 v0, 0x1

    goto :goto_19
.end method

.method protected i()LaP/a;
    .registers 2

    .prologue
    .line 240
    new-instance v0, LaP/j;

    invoke-direct {v0, p0}, LaP/j;-><init>(LaN/i;)V

    return-object v0
.end method

.method protected j(I)V
    .registers 4
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge p1, v0, :cond_18

    .line 327
    iget-object v0, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->a(I)V

    .line 328
    invoke-virtual {p0}, LaN/bx;->an()Z

    .line 329
    invoke-virtual {p0}, LaN/bx;->bG()V

    .line 330
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LaN/bx;->a(ILjava/lang/Object;)V

    .line 332
    :cond_18
    return-void
.end method

.method protected m()V
    .registers 4

    .prologue
    .line 398
    iget-object v0, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 401
    if-nez v0, :cond_b

    .line 410
    :goto_a
    return-void

    .line 405
    :cond_b
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    .line 406
    iget-object v2, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    move-result-object v2

    invoke-interface {v2, v1}, Lay/m;->a(Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, LaN/bx;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/v;->am()Lay/m;

    move-result-object v1

    const-string v2, "o"

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lay/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    invoke-super {p0}, LaN/m;->m()V

    goto :goto_a
.end method

.method protected n()V
    .registers 2

    .prologue
    .line 414
    iget-object v0, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-ltz v0, :cond_c

    .line 415
    invoke-super {p0}, LaN/m;->n()V

    .line 419
    :goto_b
    return-void

    .line 417
    :cond_c
    invoke-virtual {p0}, LaN/bx;->Z()V

    goto :goto_b
.end method

.method public s()Lcom/google/googlenav/E;
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, LaN/bx;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method
