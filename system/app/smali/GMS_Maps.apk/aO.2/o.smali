.class public LaO/o;
.super LaN/bD;
.source "SourceFile"


# instance fields
.field private final C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private D:Lcom/google/android/maps/driveabout/vector/dA;

.field private E:Lcom/google/android/maps/driveabout/vector/df;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct/range {p0 .. p6}, LaN/bD;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    .line 33
    new-instance v0, LaO/p;

    invoke-direct {v0, p0}, LaO/p;-><init>(LaO/o;)V

    iput-object v0, p0, LaO/o;->E:Lcom/google/android/maps/driveabout/vector/df;

    .line 54
    iput-object p7, p0, LaO/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 55
    return-void
.end method

.method static synthetic a(LaO/o;)Lau/u;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, LaO/o;->d:Lau/u;

    return-object v0
.end method

.method static synthetic b(LaO/o;)Lcom/google/googlenav/ui/v;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, LaO/o;->b:Lcom/google/googlenav/ui/v;

    return-object v0
.end method


# virtual methods
.method protected bJ()V
    .registers 3

    .prologue
    .line 64
    iget-object v0, p0, LaO/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_5

    .line 77
    :goto_4
    return-void

    .line 73
    :cond_5
    iget-object v0, p0, LaO/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->g:Lcom/google/android/maps/driveabout/vector/di;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/dd;->a(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/di;)Lcom/google/android/maps/driveabout/vector/dA;

    move-result-object v0

    iput-object v0, p0, LaO/o;->D:Lcom/google/android/maps/driveabout/vector/dA;

    .line 75
    iget-object v0, p0, LaO/o;->D:Lcom/google/android/maps/driveabout/vector/dA;

    iget-object v1, p0, LaO/o;->E:Lcom/google/android/maps/driveabout/vector/df;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/dA;->a(Lcom/google/android/maps/driveabout/vector/df;)V

    .line 76
    iget-object v0, p0, LaO/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, LaO/o;->D:Lcom/google/android/maps/driveabout/vector/dA;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aD;)V

    goto :goto_4
.end method

.method protected bK()V
    .registers 3

    .prologue
    .line 90
    iget-object v0, p0, LaO/o;->D:Lcom/google/android/maps/driveabout/vector/dA;

    if-eqz v0, :cond_b

    .line 91
    iget-object v0, p0, LaO/o;->D:Lcom/google/android/maps/driveabout/vector/dA;

    iget-object v1, p0, LaO/o;->E:Lcom/google/android/maps/driveabout/vector/df;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/dA;->b(Lcom/google/android/maps/driveabout/vector/df;)V

    .line 93
    :cond_b
    iget-object v0, p0, LaO/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_16

    .line 95
    iget-object v0, p0, LaO/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, LaO/o;->D:Lcom/google/android/maps/driveabout/vector/dA;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 97
    :cond_16
    return-void
.end method
