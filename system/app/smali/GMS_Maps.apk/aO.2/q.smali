.class public LaO/q;
.super LaN/y;
.source "SourceFile"


# instance fields
.field private final C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private D:Lcom/google/android/maps/driveabout/vector/dd;

.field private E:Lo/ax;

.field private F:Lo/m;

.field private final G:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct/range {p0 .. p6}, LaN/y;-><init>(Lcom/google/googlenav/ui/v;Lau/p;Lau/u;Lcom/google/googlenav/ui/ac;Lcom/google/googlenav/layer/m;Lau/k;)V

    .line 43
    iput-object p7, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 44
    new-instance v0, Lo/ay;

    invoke-direct {v0}, Lo/ay;-><init>()V

    invoke-virtual {v0}, Lo/ay;->b()Lo/ax;

    move-result-object v0

    iput-object v0, p0, LaO/q;->E:Lo/ax;

    .line 45
    iput-boolean p8, p0, LaO/q;->G:Z

    .line 46
    return-void
.end method

.method private bJ()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 106
    iget-object v0, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_14

    .line 107
    iget-object v0, p0, LaO/q;->D:Lcom/google/android/maps/driveabout/vector/dd;

    iget-object v1, p0, LaO/q;->E:Lo/ax;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/dd;->a(Lo/ao;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 108
    iget-object v0, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    .line 111
    :cond_14
    return-void
.end method

.method private bK()Z
    .registers 2

    .prologue
    .line 119
    iget-object v0, p0, LaO/q;->F:Lo/m;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ab;)V
    .registers 4
    .parameter

    .prologue
    .line 100
    new-instance v0, Lo/ay;

    invoke-direct {v0}, Lo/ay;-><init>()V

    iget-object v1, p0, LaO/q;->E:Lo/ax;

    invoke-virtual {v0, v1}, Lo/ay;->a(Lo/ax;)Lo/ay;

    move-result-object v0

    invoke-virtual {v0}, Lo/ay;->a()Lo/ay;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ab;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lo/ay;->a(I)Lo/ay;

    move-result-object v0

    invoke-virtual {v0}, Lo/ay;->b()Lo/ax;

    move-result-object v0

    iput-object v0, p0, LaO/q;->E:Lo/ax;

    .line 102
    invoke-direct {p0}, LaO/q;->bJ()V

    .line 103
    return-void
.end method

.method public a(Lo/m;)V
    .registers 4
    .parameter

    .prologue
    .line 74
    new-instance v0, Lo/ay;

    invoke-direct {v0}, Lo/ay;-><init>()V

    iget-object v1, p0, LaO/q;->E:Lo/ax;

    invoke-virtual {v0, v1}, Lo/ay;->a(Lo/ax;)Lo/ay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lo/ay;->a(Lo/m;)Lo/ay;

    move-result-object v0

    invoke-virtual {v0}, Lo/ay;->b()Lo/ax;

    move-result-object v0

    iput-object v0, p0, LaO/q;->E:Lo/ax;

    .line 76
    invoke-direct {p0}, LaO/q;->bJ()V

    .line 77
    iput-object p1, p0, LaO/q;->F:Lo/m;

    .line 78
    return-void
.end method

.method public aE()Z
    .registers 2

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public aT()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 51
    invoke-virtual {p0}, LaO/q;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->q()Lo/m;

    move-result-object v0

    iput-object v0, p0, LaO/q;->F:Lo/m;

    .line 52
    iget-object v0, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_31

    .line 53
    iget-object v0, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/dd;->a(Landroid/content/res/Resources;)Lcom/google/android/maps/driveabout/vector/dd;

    move-result-object v0

    iput-object v0, p0, LaO/q;->D:Lcom/google/android/maps/driveabout/vector/dd;

    .line 54
    invoke-virtual {p0}, LaO/q;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->q()Lo/m;

    move-result-object v0

    invoke-virtual {p0, v0}, LaO/q;->a(Lo/m;)V

    .line 55
    iget-object v0, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v2, p0, LaO/q;->D:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 57
    :cond_31
    invoke-direct {p0}, LaO/q;->bK()Z

    move-result v0

    if-nez v0, :cond_3c

    move v0, v1

    :goto_38
    invoke-virtual {p0, v0}, LaO/q;->k(Z)V

    .line 58
    return v1

    .line 57
    :cond_3c
    const/4 v0, 0x0

    goto :goto_38
.end method

.method public aU()V
    .registers 3

    .prologue
    .line 64
    iget-object v0, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_b

    .line 65
    iget-object v0, p0, LaO/q;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, LaO/q;->D:Lcom/google/android/maps/driveabout/vector/dd;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/aD;)V

    .line 67
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LaO/q;->k(Z)V

    .line 68
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 85
    if-nez p1, :cond_8

    .line 86
    const/4 v0, 0x0

    check-cast v0, Lo/m;

    invoke-virtual {p0, v0}, LaO/q;->a(Lo/m;)V

    .line 89
    :cond_8
    :try_start_8
    invoke-static {p1}, Lo/m;->a(Ljava/lang/String;)Lo/m;

    move-result-object v0

    .line 90
    invoke-virtual {p0, v0}, LaO/q;->a(Lo/m;)V
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_f} :catch_10

    .line 94
    :goto_f
    return-void

    .line 91
    :catch_10
    move-exception v0

    goto :goto_f
.end method

.method public bI()Z
    .registers 2

    .prologue
    .line 115
    invoke-direct {p0}, LaO/q;->bK()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected k(Z)V
    .registers 4
    .parameter

    .prologue
    .line 129
    invoke-super {p0, p1}, LaN/y;->k(Z)V

    .line 130
    iget-object v0, p0, LaO/q;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {p0}, LaO/q;->bG()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/v;->g(Z)V

    .line 131
    iget-object v0, p0, LaO/q;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->al()LaN/am;

    move-result-object v0

    invoke-virtual {v0}, LaN/am;->x()LaN/bT;

    move-result-object v0

    .line 132
    if-eqz p1, :cond_24

    if-nez v0, :cond_24

    .line 134
    iget-boolean v0, p0, LaO/q;->G:Z

    if-nez v0, :cond_23

    .line 135
    iget-object v0, p0, LaO/q;->b:Lcom/google/googlenav/ui/v;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/v;->ay()V

    .line 140
    :cond_23
    :goto_23
    return-void

    .line 137
    :cond_24
    if-nez p1, :cond_23

    if-eqz v0, :cond_23

    .line 138
    invoke-virtual {v0}, LaN/bT;->bI()V

    goto :goto_23
.end method
