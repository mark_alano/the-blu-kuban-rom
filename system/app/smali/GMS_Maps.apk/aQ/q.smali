.class public final enum LaQ/q;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaQ/q;

.field public static final enum b:LaQ/q;

.field public static final enum c:LaQ/q;

.field public static final enum d:LaQ/q;

.field public static final enum e:LaQ/q;

.field private static final synthetic f:[LaQ/q;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, LaQ/q;

    const-string v1, "FIRST_WALK"

    invoke-direct {v0, v1, v2}, LaQ/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaQ/q;->a:LaQ/q;

    .line 48
    new-instance v0, LaQ/q;

    const-string v1, "BOARD"

    invoke-direct {v0, v1, v3}, LaQ/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaQ/q;->b:LaQ/q;

    .line 49
    new-instance v0, LaQ/q;

    const-string v1, "STAY"

    invoke-direct {v0, v1, v4}, LaQ/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaQ/q;->c:LaQ/q;

    .line 50
    new-instance v0, LaQ/q;

    const-string v1, "GET_OFF"

    invoke-direct {v0, v1, v5}, LaQ/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaQ/q;->d:LaQ/q;

    .line 51
    new-instance v0, LaQ/q;

    const-string v1, "DESTINATION"

    invoke-direct {v0, v1, v6}, LaQ/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaQ/q;->e:LaQ/q;

    .line 46
    const/4 v0, 0x5

    new-array v0, v0, [LaQ/q;

    sget-object v1, LaQ/q;->a:LaQ/q;

    aput-object v1, v0, v2

    sget-object v1, LaQ/q;->b:LaQ/q;

    aput-object v1, v0, v3

    sget-object v1, LaQ/q;->c:LaQ/q;

    aput-object v1, v0, v4

    sget-object v1, LaQ/q;->d:LaQ/q;

    aput-object v1, v0, v5

    sget-object v1, LaQ/q;->e:LaQ/q;

    aput-object v1, v0, v6

    sput-object v0, LaQ/q;->f:[LaQ/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaQ/q;
    .registers 2
    .parameter

    .prologue
    .line 46
    const-class v0, LaQ/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaQ/q;

    return-object v0
.end method

.method public static values()[LaQ/q;
    .registers 1

    .prologue
    .line 46
    sget-object v0, LaQ/q;->f:[LaQ/q;

    invoke-virtual {v0}, [LaQ/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaQ/q;

    return-object v0
.end method
