.class public LaQ/r;
.super LaQ/s;
.source "SourceFile"


# instance fields
.field private final e:Lae/u;

.field private final f:Lae/u;


# direct methods
.method public constructor <init>(Lae/t;Lae/u;Lae/u;ILjava/util/List;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 714
    sget-object v1, LaQ/q;->c:LaQ/q;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LaQ/s;-><init>(LaQ/q;Lae/w;Lae/t;ILjava/util/List;)V

    .line 715
    iput-object p2, p0, LaQ/r;->e:Lae/u;

    .line 716
    iput-object p3, p0, LaQ/r;->f:Lae/u;

    .line 717
    invoke-virtual {p2}, Lae/u;->e()Lae/v;

    move-result-object v0

    iput-object v0, p0, LaQ/r;->d:Lae/v;

    .line 718
    invoke-virtual {p0}, LaQ/r;->C()V

    .line 719
    return-void
.end method


# virtual methods
.method protected C()V
    .registers 2

    .prologue
    .line 796
    invoke-virtual {p0}, LaQ/r;->s()V

    .line 797
    invoke-virtual {p0}, LaQ/r;->t()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 798
    invoke-virtual {p0}, LaQ/r;->y()F

    move-result v0

    invoke-virtual {p0, v0}, LaQ/r;->a(F)V

    .line 800
    :cond_10
    return-void
.end method

.method public E()Z
    .registers 2

    .prologue
    .line 803
    iget-object v0, p0, LaQ/r;->d:Lae/v;

    iget-boolean v0, v0, Lae/v;->a:Z

    return v0
.end method

.method public F()Z
    .registers 2

    .prologue
    .line 813
    iget-object v0, p0, LaQ/r;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->R()Z

    move-result v0

    return v0
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 808
    iget-object v0, p0, LaQ/r;->d:Lae/v;

    iget-object v0, v0, Lae/v;->i:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 723
    iget-object v0, p0, LaQ/r;->e:Lae/u;

    invoke-virtual {v0}, Lae/u;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()I
    .registers 4

    .prologue
    const/16 v0, 0xf

    .line 760
    iget-object v1, p0, LaQ/r;->e:Lae/u;

    invoke-virtual {v1}, Lae/u;->b()Ljava/util/Date;

    move-result-object v1

    .line 761
    iget-object v2, p0, LaQ/r;->e:Lae/u;

    invoke-virtual {v2}, Lae/u;->c()Ljava/util/Date;

    move-result-object v2

    .line 762
    if-eqz v1, :cond_17

    if-eqz v2, :cond_17

    .line 763
    invoke-static {v1, v2}, LaQ/h;->a(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    .line 769
    :cond_16
    :goto_16
    return v0

    .line 765
    :cond_17
    invoke-virtual {p0}, LaQ/r;->q()I

    move-result v1

    .line 766
    if-gt v1, v0, :cond_16

    .line 769
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public n()Ljava/util/Date;
    .registers 3

    .prologue
    .line 776
    iget-object v0, p0, LaQ/r;->e:Lae/u;

    invoke-virtual {v0}, Lae/u;->b()Ljava/util/Date;

    move-result-object v0

    .line 777
    iget-object v1, p0, LaQ/r;->e:Lae/u;

    invoke-virtual {v1}, Lae/u;->c()Ljava/util/Date;

    move-result-object v1

    .line 778
    if-eqz v0, :cond_f

    .line 781
    :goto_e
    return-object v0

    :cond_f
    move-object v0, v1

    goto :goto_e
.end method

.method public o()Ljava/util/Date;
    .registers 2

    .prologue
    .line 787
    iget-object v0, p0, LaQ/r;->f:Lae/u;

    if-eqz v0, :cond_b

    .line 788
    iget-object v0, p0, LaQ/r;->f:Lae/u;

    invoke-virtual {v0}, Lae/u;->b()Ljava/util/Date;

    move-result-object v0

    .line 790
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, LaQ/r;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaQ/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    goto :goto_a
.end method

.method public u()Lau/B;
    .registers 2

    .prologue
    .line 742
    iget-object v0, p0, LaQ/r;->e:Lae/u;

    invoke-virtual {v0}, Lae/u;->d()Lau/B;

    move-result-object v0

    return-object v0
.end method

.method public v()Lau/B;
    .registers 2

    .prologue
    .line 747
    iget-object v0, p0, LaQ/r;->f:Lae/u;

    if-eqz v0, :cond_b

    .line 748
    iget-object v0, p0, LaQ/r;->f:Lae/u;

    invoke-virtual {v0}, Lae/u;->d()Lau/B;

    move-result-object v0

    .line 750
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, LaQ/r;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->k()Lau/B;

    move-result-object v0

    goto :goto_a
.end method

.method public w()Ljava/lang/String;
    .registers 2

    .prologue
    .line 728
    iget-object v0, p0, LaQ/r;->f:Lae/u;

    if-eqz v0, :cond_b

    .line 729
    iget-object v0, p0, LaQ/r;->f:Lae/u;

    invoke-virtual {v0}, Lae/u;->a()Ljava/lang/String;

    move-result-object v0

    .line 731
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, LaQ/r;->a:Lae/t;

    invoke-virtual {v0}, Lae/t;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method
