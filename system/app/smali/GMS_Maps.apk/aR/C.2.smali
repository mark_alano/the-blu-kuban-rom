.class LaR/c;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:LaR/b;


# direct methods
.method constructor <init>(LaR/b;Las/c;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 142
    iput-object p1, p0, LaR/c;->a:LaR/b;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    .line 145
    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->a(LaR/b;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1

    .line 148
    :try_start_7
    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->a(LaR/b;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    .line 149
    monitor-exit v1

    .line 175
    :goto_14
    return-void

    .line 151
    :cond_15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_2a

    .line 154
    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->b(LaR/b;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 155
    :try_start_1d
    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->c(LaR/b;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 156
    monitor-exit v1

    goto :goto_14

    .line 162
    :catchall_27
    move-exception v0

    monitor-exit v1
    :try_end_29
    .catchall {:try_start_1d .. :try_end_29} :catchall_27

    throw v0

    .line 151
    :catchall_2a
    move-exception v0

    :try_start_2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    throw v0

    .line 158
    :cond_2d
    :try_start_2d
    iget-object v0, p0, LaR/c;->a:LaR/b;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LaR/b;->a(LaR/b;Z)Z

    .line 160
    iget-object v0, p0, LaR/c;->a:LaR/b;

    invoke-static {v0}, LaR/b;->d(LaR/b;)Z

    move-result v0

    .line 161
    iget-object v2, p0, LaR/c;->a:LaR/b;

    const/4 v3, 0x0

    invoke-static {v2, v3}, LaR/b;->b(LaR/b;Z)Z

    .line 162
    monitor-exit v1
    :try_end_40
    .catchall {:try_start_2d .. :try_end_40} :catchall_27

    .line 164
    const-string v1, "BackgroundPlaceDetailsFetcher.doCheck"

    invoke-static {v1}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, LaR/c;->a:LaR/b;

    invoke-static {v1}, LaR/b;->e(LaR/b;)I

    move-result v1

    .line 170
    if-eqz v0, :cond_65

    .line 171
    const-string v0, "BPDF1"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_65
    const-string v0, "BackgroundPlaceDetailsFetcher.doCheck"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    goto :goto_14
.end method
