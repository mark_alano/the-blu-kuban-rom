.class public LaR/G;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(LaR/D;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 47
    invoke-virtual {p0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 48
    const/16 v0, 0x2e2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    :goto_c
    return-object v0

    :cond_d
    const/16 v0, 0x2e3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_c
.end method

.method public static a(LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-static {p0}, LaR/G;->a(LaR/D;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, LaR/G;->a(Ljava/lang/String;LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    if-eqz p0, :cond_2e

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LaR/D;->i()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 26
    :goto_2d
    return-object v0

    :cond_2e
    invoke-virtual {p1}, LaR/D;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_2d
.end method

.method static b(LaR/D;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 59
    invoke-static {p0}, LaR/G;->c(LaR/D;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-static {p0}, LaR/G;->b(LaR/D;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, LaR/G;->a(Ljava/lang/String;LaR/D;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(LaR/D;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-virtual {p0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 31
    const/16 v0, 0x2e5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 42
    :goto_c
    return-object v0

    .line 33
    :cond_d
    invoke-virtual {p0}, LaR/D;->l()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 34
    const/16 v0, 0x2e4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    .line 36
    :cond_1a
    invoke-virtual {p0}, LaR/D;->m()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 37
    const/16 v0, 0x3ef

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    .line 39
    :cond_27
    invoke-virtual {p0}, LaR/D;->o()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 40
    const/16 v0, 0x6f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    .line 42
    :cond_34
    const/4 v0, 0x0

    goto :goto_c
.end method
