.class public LaR/h;
.super LaR/t;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Lax/b;


# direct methods
.method private constructor <init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, LaR/t;-><init>(Ljava/lang/String;J)V

    .line 43
    invoke-virtual {p0, p4, p5}, LaR/h;->b(J)V

    .line 44
    iput-object p8, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 45
    iput-object p6, p0, LaR/h;->d:Ljava/lang/String;

    .line 46
    iput-object p7, p0, LaR/h;->e:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/h;
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    const/4 v0, 0x4

    const-wide/16 v2, -0x1

    invoke-static {p0, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v2

    .line 55
    const/16 v0, 0xc

    const-wide/16 v4, 0x0

    invoke-static {p0, v0, v4, v5}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v4

    .line 58
    const/16 v0, 0xe

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    .line 60
    array-length v0, v8

    const/4 v7, 0x2

    if-ge v0, v7, :cond_26

    .line 62
    new-instance v0, LaR/h;

    move-object v7, v6

    invoke-direct/range {v0 .. v8}, LaR/h;-><init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 68
    :goto_25
    return-object v0

    .line 65
    :cond_26
    const/4 v0, 0x0

    aget-object v0, v8, v0

    invoke-static {v0}, LaR/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v6

    .line 66
    array-length v0, v8

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v8, v0

    invoke-static {v0}, LaR/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v7

    .line 68
    new-instance v0, LaR/h;

    invoke-direct/range {v0 .. v8}, LaR/h;-><init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_25
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 112
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_10

    .line 113
    const/16 v0, 0x3a4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 115
    :goto_f
    return-object v0

    :cond_10
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_f
.end method

.method private f()Lax/b;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 123
    invoke-virtual {p0}, LaR/h;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 124
    new-instance v1, Lbm/b;

    const-string v2, "dirflg"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbm/b;-><init>(Ljava/lang/String;)V

    .line 126
    invoke-virtual {v1}, Lbm/b;->a()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 127
    new-instance v0, Lax/x;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v3}, Lax/x;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    .line 135
    :goto_21
    return-object v0

    .line 128
    :cond_22
    invoke-virtual {v1}, Lbm/b;->d()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 129
    new-instance v0, Lax/w;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lax/w;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    goto :goto_21

    .line 131
    :cond_38
    invoke-virtual {v1}, Lbm/b;->c()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 132
    new-instance v0, Lax/i;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v3}, Lax/i;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    goto :goto_21

    .line 135
    :cond_46
    new-instance v0, Lax/s;

    iget-object v1, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v1, v3}, Lax/s;-><init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    goto :goto_21
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 89
    invoke-super {p0}, LaR/t;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 90
    const/4 v0, 0x0

    :goto_5
    iget-object v2, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v2, v2

    if-ge v0, v2, :cond_16

    .line 91
    const/16 v2, 0xe

    iget-object v3, p0, LaR/h;->f:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 93
    :cond_16
    return-object v1
.end method

.method public b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LaR/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaR/h;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, LaR/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 101
    iget-object v0, p0, LaR/h;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lax/b;
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, LaR/h;->g:Lax/b;

    if-nez v0, :cond_a

    .line 141
    invoke-direct {p0}, LaR/h;->f()Lax/b;

    move-result-object v0

    iput-object v0, p0, LaR/h;->g:Lax/b;

    .line 143
    :cond_a
    iget-object v0, p0, LaR/h;->g:Lax/b;

    return-object v0
.end method
