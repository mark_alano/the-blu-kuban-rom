.class public LaR/I;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field private b:LR/h;

.field private c:Ljava/util/Map;

.field private d:Ljava/util/Map;

.field private e:J

.field private f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private g:Lcom/google/googlenav/common/io/j;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/j;)V
    .registers 4
    .parameter

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, LaR/I;->a:Z

    .line 124
    iput-object p1, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    .line 125
    new-instance v0, LR/h;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, LaR/I;->b:LR/h;

    .line 126
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LaR/I;->c:Ljava/util/Map;

    .line 127
    if-eqz p1, :cond_1c

    .line 128
    invoke-direct {p0}, LaR/I;->b()V

    .line 130
    :cond_1c
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 430
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    iget-object v0, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 432
    const/4 v0, 0x0

    :goto_d
    if-ge v0, v1, :cond_25

    .line 433
    iget-object v2, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 434
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 435
    iget-object v1, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    .line 439
    :cond_25
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    invoke-interface {v0, p2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 440
    invoke-direct {p0}, LaR/I;->c()V

    .line 441
    return-void

    .line 432
    :cond_2e
    add-int/lit8 v0, v0, 0x1

    goto :goto_d
.end method

.method static a([B)V
    .registers 7
    .parameter

    .prologue
    const/4 v5, 0x4

    .line 521
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 522
    array-length v1, p0

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, p0, v5, v1}, Ljava/util/zip/CRC32;->update([BII)V

    .line 523
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v1

    .line 524
    const/4 v0, 0x0

    :goto_11
    if-ge v0, v5, :cond_21

    .line 525
    const/16 v3, 0x18

    shr-long v3, v1, v3

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, p0, v0

    .line 526
    const/16 v3, 0x8

    shl-long/2addr v1, v3

    .line 524
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 528
    :cond_21
    return-void
.end method

.method private b(J)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 317
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "star_details_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .registers 10

    .prologue
    .line 248
    const-string v0, "PlaceDetailsCache.loadIndexFromPersistentStore"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 249
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LaR/I;->d:Ljava/util/Map;

    .line 250
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaR/I;->e:J

    .line 251
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbM/a;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 253
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    const-string v1, "star_details_index"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 254
    if-eqz v0, :cond_73

    .line 257
    :try_start_22
    invoke-static {v0}, LaR/I;->b([B)V

    .line 260
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 261
    const-wide/16 v2, 0x4

    invoke-virtual {v1, v2, v3}, Ljava/io/ByteArrayInputStream;->skip(J)J

    .line 264
    iget-object v0, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 265
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 268
    iget-object v0, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 269
    const/4 v0, 0x0

    :goto_3f
    if-ge v0, v1, :cond_73

    .line 270
    iget-object v2, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 271
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    .line 272
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 273
    iget-object v5, p0, LaR/I;->d:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    iget-wide v5, p0, LaR/I;->e:J

    const-wide/16 v7, 0x1

    add-long v2, v3, v7

    invoke-static {v5, v6, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, LaR/I;->e:J
    :try_end_67
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_67} :catch_6a

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_3f

    .line 276
    :catch_6a
    move-exception v0

    .line 279
    const-string v1, "PlaceDetailsCache"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 280
    invoke-direct {p0}, LaR/I;->d()V

    .line 283
    :cond_73
    const-string v0, "PlaceDetailsCache.loadIndexFromPersistentStore"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 284
    return-void
.end method

.method private b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 367
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 369
    if-nez v0, :cond_2b

    .line 370
    :goto_c
    if-eqz v2, :cond_14

    .line 371
    iget-wide v3, p0, LaR/I;->e:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 373
    :cond_14
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, LaR/I;->b(J)Ljava/lang/String;

    move-result-object v3

    .line 377
    :try_start_1c
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 378
    :goto_21
    const/4 v5, 0x4

    if-ge v1, v5, :cond_2d

    .line 379
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 378
    add-int/lit8 v1, v1, 0x1

    goto :goto_21

    :cond_2b
    move v2, v1

    .line 369
    goto :goto_c

    .line 383
    :cond_2d
    invoke-virtual {p2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 384
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 385
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 388
    invoke-static {v1}, LaR/I;->a([B)V

    .line 391
    iget-object v4, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    invoke-interface {v4, v1, v3}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    move-result v1

    .line 392
    if-gez v1, :cond_65

    .line 393
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Persistent store write failed "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5b
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_5b} :catch_5b

    .line 409
    :catch_5b
    move-exception v0

    .line 412
    const-string v1, "PlaceDetailsCache"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 413
    invoke-direct {p0, p1, v3}, LaR/I;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_64
    :goto_64
    return-void

    .line 400
    :cond_65
    if-eqz v2, :cond_64

    .line 401
    :try_start_67
    iget-object v1, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    iget-wide v1, p0, LaR/I;->e:J

    const-wide/16 v4, 0x1

    add-long/2addr v1, v4

    iput-wide v1, p0, LaR/I;->e:J

    .line 403
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbM/a;->h:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 404
    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 405
    const/4 v0, 0x2

    invoke-virtual {v1, v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 406
    iget-object v0, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 407
    invoke-direct {p0}, LaR/I;->c()V
    :try_end_8f
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_8f} :catch_5b

    goto :goto_64
.end method

.method static b([B)V
    .registers 8
    .parameter

    .prologue
    const/4 v6, 0x4

    .line 537
    array-length v0, p0

    if-ge v0, v6, :cond_c

    .line 538
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Missing checksum"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 540
    :cond_c
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 541
    array-length v1, p0

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, p0, v6, v1}, Ljava/util/zip/CRC32;->update([BII)V

    .line 542
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v1

    .line 543
    const/4 v0, 0x0

    :goto_1c
    if-ge v0, v6, :cond_36

    .line 544
    aget-byte v3, p0, v0

    const/16 v4, 0x18

    shr-long v4, v1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    if-eq v3, v4, :cond_30

    .line 545
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Checksum mismatch"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_30
    const/16 v3, 0x8

    shl-long/2addr v1, v3

    .line 543
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 549
    :cond_36
    return-void
.end method

.method private c()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 289
    :try_start_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 290
    :goto_6
    const/4 v2, 0x4

    if-ge v0, v2, :cond_10

    .line 291
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 295
    :cond_10
    iget-object v0, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 296
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 297
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 300
    invoke-static {v0}, LaR/I;->a([B)V

    .line 303
    iget-object v1, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    const-string v2, "star_details_index"

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    move-result v0

    .line 305
    if-gez v0, :cond_4b

    .line 306
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Persistent store write failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_42} :catch_42

    .line 308
    :catch_42
    move-exception v0

    .line 311
    const-string v1, "PlaceDetailsCache"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 312
    invoke-direct {p0}, LaR/I;->d()V

    .line 314
    :cond_4b
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 419
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 420
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaR/I;->e:J

    .line 421
    iget-object v0, p0, LaR/I;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clear()V

    .line 422
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    const-string v1, "star_details_"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    .line 423
    return-void
.end method

.method private e(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 323
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 324
    if-nez v0, :cond_d

    move-object v0, v1

    .line 360
    :cond_c
    :goto_c
    return-object v0

    .line 327
    :cond_d
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, LaR/I;->b(J)Ljava/lang/String;

    move-result-object v2

    .line 331
    :try_start_15
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    invoke-interface {v0, v2}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 332
    if-nez v0, :cond_30

    .line 333
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Block not found"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_25} :catch_25

    .line 355
    :catch_25
    move-exception v0

    .line 358
    const-string v3, "PlaceDetailsCache"

    invoke-static {v3, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 359
    invoke-direct {p0, p1, v2}, LaR/I;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 360
    goto :goto_c

    .line 337
    :cond_30
    :try_start_30
    invoke-static {v0}, LaR/I;->b([B)V

    .line 340
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 341
    const-wide/16 v4, 0x4

    invoke-virtual {v3, v4, v5}, Ljava/io/ByteArrayInputStream;->skip(J)J

    .line 344
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, LbM/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 345
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 346
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V

    .line 350
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 351
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 352
    new-instance v0, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "URL mismatch: ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] != ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7e
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_7e} :catch_25
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 137
    monitor-enter p0

    :try_start_2
    iget-object v0, p0, LaR/I;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_a
    .catchall {:try_start_2 .. :try_end_a} :catchall_2c

    .line 138
    if-eqz v0, :cond_e

    .line 160
    :cond_c
    :goto_c
    monitor-exit p0

    return-object v0

    .line 143
    :cond_e
    :try_start_e
    iget-object v0, p0, LaR/I;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 144
    if-nez v0, :cond_c

    .line 149
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    if-nez v0, :cond_1e

    move-object v0, v1

    .line 150
    goto :goto_c

    .line 152
    :cond_1e
    invoke-direct {p0, p1}, LaR/I;->e(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 153
    if-nez v0, :cond_26

    move-object v0, v1

    .line 154
    goto :goto_c

    .line 158
    :cond_26
    iget-object v1, p0, LaR/I;->b:LR/h;

    invoke-virtual {v1, p1, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2b
    .catchall {:try_start_e .. :try_end_2b} :catchall_2c

    goto :goto_c

    .line 137
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(J)V
    .registers 9
    .parameter

    .prologue
    .line 448
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    .line 449
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 450
    :cond_11
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 451
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LaR/I;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 452
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v3

    if-nez v3, :cond_2f

    .line 453
    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/I;->a:Z
    :try_end_2b
    .catchall {:try_start_1 .. :try_end_2b} :catchall_2c

    goto :goto_11

    .line 448
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454
    :cond_2f
    const/4 v3, 0x5

    :try_start_30
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    cmp-long v0, v3, p1

    if-gez v0, :cond_11

    .line 455
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_11

    .line 458
    :cond_3c
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    .line 459
    const/16 v2, 0x7b

    const-string v3, "p"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "b="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "|"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "a"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_72
    .catchall {:try_start_30 .. :try_end_72} :catchall_2c

    .line 463
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(JJ)V
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 472
    monitor-enter p0

    :try_start_1
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1, p3, p4}, Ljava/util/Random;-><init>(J)V

    .line 473
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 474
    :cond_10
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 475
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LaR/I;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 476
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v3

    if-nez v3, :cond_10

    .line 477
    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    const-wide v5, 0x95586c00L

    rem-long/2addr v3, v5

    .line 478
    const/4 v5, 0x5

    sub-long v3, p1, v3

    invoke-virtual {v0, v5, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 479
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v0}, LaR/I;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_43
    .catchall {:try_start_1 .. :try_end_43} :catchall_44

    goto :goto_10

    .line 472
    :catchall_44
    move-exception v0

    monitor-exit p0

    throw v0

    .line 482
    :cond_47
    const/4 v0, 0x0

    :try_start_48
    iput-boolean v0, p0, LaR/I;->a:Z

    .line 483
    const/16 v0, 0x7b

    const-string v1, "p"

    const-string v2, "d"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_53
    .catchall {:try_start_48 .. :try_end_53} :catchall_44

    .line 486
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 209
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaR/I;->b:LR/h;

    invoke-virtual {v0, p1, p2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 212
    const/4 v0, 0x4

    invoke-static {p2, v0}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 213
    iget-object v0, p0, LaR/I;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_21

    .line 225
    :cond_12
    :goto_12
    monitor-exit p0

    return-void

    .line 219
    :cond_14
    :try_start_14
    iget-object v0, p0, LaR/I;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    if-eqz v0, :cond_12

    .line 223
    invoke-direct {p0, p1, p2}, LaR/I;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_20
    .catchall {:try_start_14 .. :try_end_20} :catchall_21

    goto :goto_12

    .line 209
    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Z
    .registers 10

    .prologue
    const-wide/16 v7, -0x1

    .line 494
    monitor-enter p0

    const/4 v0, 0x0

    .line 495
    :try_start_4
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    .line 496
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v3

    const-string v4, "star_details_last_purge_ms"

    const-wide/16 v5, -0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v3

    .line 499
    cmp-long v5, v3, v7

    if-eqz v5, :cond_2d

    const-wide/32 v5, 0x5265c00

    sub-long v5, v1, v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_50

    .line 500
    :cond_2d
    const-wide v3, 0x95586c00L

    sub-long v3, v1, v3

    invoke-virtual {p0, v3, v4}, LaR/I;->a(J)V

    .line 501
    const/4 v0, 0x1

    .line 502
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v3

    const-string v4, "star_details_last_purge_ms"

    invoke-virtual {v3, v4, v1, v2}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    .line 504
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/io/j;->a()V

    .line 506
    :cond_50
    iget-boolean v3, p0, LaR/I;->a:Z

    if-eqz v3, :cond_69

    .line 509
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LJ/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 510
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    int-to-long v3, v3

    xor-long/2addr v3, v1

    invoke-virtual {p0, v1, v2, v3, v4}, LaR/I;->a(JJ)V
    :try_end_69
    .catchall {:try_start_4 .. :try_end_69} :catchall_6b

    .line 512
    :cond_69
    monitor-exit p0

    return v0

    .line 494
    :catchall_6b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 169
    monitor-enter p0

    :try_start_2
    iget-object v0, p0, LaR/I;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_a
    .catchall {:try_start_2 .. :try_end_a} :catchall_27

    .line 170
    if-eqz v0, :cond_e

    .line 180
    :cond_c
    :goto_c
    monitor-exit p0

    return v1

    .line 175
    :cond_e
    :try_start_e
    iget-object v0, p0, LaR/I;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 180
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    if-eqz v0, :cond_25

    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_1f
    .catchall {:try_start_e .. :try_end_1f} :catchall_27

    move-result v0

    if-eqz v0, :cond_25

    move v0, v1

    :goto_23
    move v1, v0

    goto :goto_c

    :cond_25
    const/4 v0, 0x0

    goto :goto_23

    .line 169
    :catchall_27
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Ljava/lang/String;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    monitor-enter p0

    :try_start_3
    iget-object v0, p0, LaR/I;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 190
    if-eqz v0, :cond_19

    .line 191
    const/4 v3, 0x4

    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_33

    move-result v0

    if-nez v0, :cond_17

    move v0, v1

    .line 203
    :goto_15
    monitor-exit p0

    return v0

    :cond_17
    move v0, v2

    .line 191
    goto :goto_15

    .line 196
    :cond_19
    :try_start_19
    iget-object v0, p0, LaR/I;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    move v0, v2

    .line 197
    goto :goto_15

    .line 203
    :cond_23
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    if-eqz v0, :cond_31

    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_2c
    .catchall {:try_start_19 .. :try_end_2c} :catchall_33

    move-result v0

    if-eqz v0, :cond_31

    :goto_2f
    move v0, v1

    goto :goto_15

    :cond_31
    move v1, v2

    goto :goto_2f

    .line 189
    :catchall_33
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 233
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaR/I;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-object v0, p0, LaR/I;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v0, p0, LaR/I;->g:Lcom/google/googlenav/common/io/j;

    if-eqz v0, :cond_24

    .line 240
    iget-object v0, p0, LaR/I;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 241
    if-eqz v0, :cond_24

    .line 242
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LaR/I;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LaR/I;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_26

    .line 245
    :cond_24
    monitor-exit p0

    return-void

    .line 233
    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0
.end method
