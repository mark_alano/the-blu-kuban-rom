.class public LaR/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/C;
.implements LaR/n;
.implements Lcom/google/googlenav/g;


# instance fields
.field private a:LaR/o;

.field private final b:I

.field private final c:LaR/R;

.field private d:Law/h;

.field private final e:Ljava/util/List;

.field private f:Lcom/google/googlenav/ui/wizard/jv;

.field private g:LaR/u;

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaR/R;Law/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;LaR/I;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaR/q;->e:Ljava/util/List;

    .line 67
    iput-boolean v1, p0, LaR/q;->h:Z

    .line 70
    iput-boolean v1, p0, LaR/q;->i:Z

    .line 77
    iput-boolean v1, p0, LaR/q;->j:Z

    .line 86
    iput-object p1, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    .line 87
    iput-object p2, p0, LaR/q;->c:LaR/R;

    .line 88
    iput-object p3, p0, LaR/q;->d:Law/h;

    .line 89
    iput p4, p0, LaR/q;->b:I

    .line 91
    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-direct {p0, p6, p5}, LaR/q;->a(LaR/I;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 93
    return-void
.end method

.method private a(LaR/I;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 107
    iget v0, p0, LaR/q;->b:I

    iget-object v1, p0, LaR/q;->c:LaR/R;

    iget v2, p0, LaR/q;->b:I

    invoke-interface {v1, v2}, LaR/R;->a(I)LaR/T;

    move-result-object v1

    invoke-static {v0, v1, p1}, LaR/v;->a(ILaR/T;LaR/I;)LaR/v;

    move-result-object v0

    iput-object v0, p0, LaR/q;->g:LaR/u;

    .line 109
    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, p0}, LaR/u;->a(LaR/C;)V

    .line 110
    return-void
.end method

.method static synthetic a(LaR/q;)V
    .registers 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, LaR/q;->g()V

    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 326
    iput-boolean v1, p0, LaR/q;->i:Z

    .line 327
    iget-boolean v0, p0, LaR/q;->h:Z

    if-eqz v0, :cond_e

    .line 328
    iput-boolean v1, p0, LaR/q;->h:Z

    .line 329
    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    .line 331
    :cond_e
    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 332
    invoke-interface {v0}, LaM/h;->C_()V

    goto :goto_14

    .line 334
    :cond_24
    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/q;->j:Z

    .line 335
    return-void
.end method


# virtual methods
.method public B_()V
    .registers 4

    .prologue
    .line 293
    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_18

    .line 294
    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/q;->h:Z

    .line 295
    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x57f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1b1

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V

    .line 298
    :cond_18
    return-void
.end method

.method public C_()V
    .registers 4

    .prologue
    .line 307
    iget-boolean v0, p0, LaR/q;->i:Z

    if-eqz v0, :cond_5

    .line 323
    :goto_4
    return-void

    .line 311
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, LaR/q;->i:Z

    .line 312
    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_24

    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 313
    const/4 v0, 0x0

    .line 314
    iget-object v1, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, LaR/s;

    invoke-direct {v2, p0}, LaR/s;-><init>(LaR/q;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_4

    .line 321
    :cond_24
    invoke-direct {p0}, LaR/q;->g()V

    goto :goto_4
.end method

.method public D_()V
    .registers 3

    .prologue
    .line 339
    iget-boolean v0, p0, LaR/q;->h:Z

    if-eqz v0, :cond_c

    .line 340
    const/4 v0, 0x0

    iput-boolean v0, p0, LaR/q;->h:Z

    .line 341
    iget-object v0, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->e()V

    .line 343
    :cond_c
    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 344
    invoke-interface {v0}, LaM/h;->D_()V

    goto :goto_12

    .line 346
    :cond_22
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 376
    return-void
.end method

.method public M_()V
    .registers 3

    .prologue
    .line 362
    iget-object v0, p0, LaR/q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaM/h;

    .line 363
    invoke-interface {v0}, LaM/h;->M_()V

    goto :goto_6

    .line 367
    :cond_16
    iget-boolean v0, p0, LaR/q;->j:Z

    if-eqz v0, :cond_24

    .line 368
    invoke-virtual {p0}, LaR/q;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->d()V

    .line 369
    const/4 v0, 0x0

    iput-boolean v0, p0, LaR/q;->j:Z

    .line 371
    :cond_24
    return-void
.end method

.method public N_()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 382
    invoke-virtual {p0}, LaR/q;->e()LaR/u;

    move-result-object v0

    .line 383
    instance-of v1, v0, LaR/M;

    if-nez v1, :cond_a

    .line 400
    :goto_9
    return-void

    .line 390
    :cond_a
    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    .line 391
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/t;

    .line 392
    check-cast v0, LaR/D;

    .line 393
    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 394
    add-int/lit8 v0, v1, 0x1

    :goto_29
    move v1, v0

    .line 396
    goto :goto_13

    .line 397
    :cond_2b
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "v="

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    const/16 v1, 0x9

    const-string v2, "s"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    :cond_54
    move v0, v1

    goto :goto_29
.end method

.method public a(LaR/D;LaR/H;Lbf/am;Ljava/lang/String;)LaR/D;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 246
    invoke-virtual {p0}, LaR/q;->e()LaR/u;

    move-result-object v0

    check-cast v0, LaR/M;

    .line 247
    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaR/M;->a(Ljava/lang/String;)LaR/t;

    move-result-object v1

    check-cast v1, LaR/D;

    .line 248
    if-eqz v1, :cond_29

    invoke-virtual {v1}, LaR/D;->g()Z

    move-result v2

    if-eqz v2, :cond_29

    .line 249
    invoke-virtual {v1, v3}, LaR/D;->a(Z)V

    .line 250
    invoke-virtual {v0, v1}, LaR/M;->a(LaR/t;)Z

    .line 251
    const-string v0, "d"

    invoke-virtual {v1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2, p4}, LaR/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :goto_28
    return-object v1

    .line 254
    :cond_29
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LaR/D;->a(Z)V

    .line 255
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, LaR/D;->a(J)V

    .line 256
    invoke-virtual {v0, p1}, LaR/M;->a(LaR/t;)Z

    .line 257
    if-eqz p2, :cond_44

    .line 258
    invoke-virtual {v0, p2, v3}, LaR/M;->a(LaR/H;Z)V

    .line 260
    :cond_44
    if-eqz p3, :cond_56

    .line 262
    invoke-virtual {p3}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    if-nez v0, :cond_4f

    .line 263
    invoke-virtual {p3, v3}, Lbf/am;->a(Z)Lbf/by;

    .line 265
    :cond_4f
    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lbf/am;->f(Ljava/lang/String;)V

    .line 267
    :cond_56
    const-string v0, "c"

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p4}, LaR/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    goto :goto_28
.end method

.method public a(LaR/O;)V
    .registers 4
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, LaR/q;->c:LaR/R;

    iget v1, p0, LaR/q;->b:I

    invoke-interface {v0, v1, p1}, LaR/R;->a(ILaR/O;)V

    .line 126
    return-void
.end method

.method public a(LaR/o;)V
    .registers 2
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, LaR/q;->a:LaR/o;

    .line 98
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 275
    iget-object v0, p0, LaR/q;->c:LaR/R;

    iget v1, p0, LaR/q;->b:I

    invoke-interface {v0, v1, p1, p2, p3}, LaR/R;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    .line 276
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 2
    .parameter

    .prologue
    .line 404
    iput-object p1, p0, LaR/q;->f:Lcom/google/googlenav/ui/wizard/jv;

    .line 405
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 130
    new-instance v0, Lcom/google/googlenav/f;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, LaR/q;->d:Law/h;

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 132
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 136
    new-instance v0, LaR/r;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, LaR/r;-><init>(LaR/q;Las/c;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, LaR/r;->g()V

    .line 142
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 280
    const-string v0, "p"

    invoke-virtual {p0, p1, p2, v0}, LaR/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 285
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "a="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 287
    const/16 v1, 0x9

    const-string v2, "f"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 205
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 240
    :cond_8
    :goto_8
    return-void

    .line 209
    :cond_9
    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, p1}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_8

    instance-of v1, v0, LaR/D;

    if-eqz v1, :cond_8

    .line 215
    check-cast v0, LaR/D;

    .line 217
    iget-object v1, p0, LaR/q;->g:LaR/u;

    invoke-interface {v1, p1}, LaR/u;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 220
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/fC;->p:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 221
    invoke-virtual {v1, v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 222
    const/16 v2, 0x90

    invoke-virtual {v1, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 223
    const/4 v2, 0x2

    invoke-virtual {v0}, LaR/D;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 224
    invoke-static {v1}, LaR/H;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/H;

    move-result-object v2

    .line 227
    invoke-virtual {v2, v4}, LaR/H;->a(I)V

    .line 229
    :try_start_3d
    invoke-virtual {v0}, LaR/D;->d()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_53

    .line 230
    const/4 v3, 0x3

    invoke-virtual {v0}, LaR/D;->d()LaN/B;

    move-result-object v0

    invoke-static {v0}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 234
    :cond_53
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, LaR/H;->a(Z)V

    .line 235
    iget-object v0, p0, LaR/q;->g:LaR/u;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, LaR/u;->a(LaR/H;Z)V
    :try_end_5d
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_5d} :catch_5e

    goto :goto_8

    .line 236
    :catch_5e
    move-exception v0

    goto :goto_8
.end method

.method public b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x90

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 146
    if-nez p2, :cond_8

    move v0, v1

    .line 200
    :goto_7
    return v0

    .line 151
    :cond_8
    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_10

    move v0, v1

    .line 152
    goto :goto_7

    .line 155
    :cond_10
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 158
    invoke-virtual {p2, v3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 161
    :cond_19
    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 164
    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, v3}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    if-nez v0, :cond_27

    move v0, v2

    .line 165
    goto :goto_7

    .line 172
    :cond_27
    iget-object v0, p0, LaR/q;->g:LaR/u;

    invoke-interface {v0, v3}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v4

    .line 174
    if-nez v4, :cond_5a

    .line 175
    new-instance v0, LaR/H;

    invoke-direct {v0}, LaR/H;-><init>()V

    .line 177
    invoke-virtual {v0, v3}, LaR/H;->a(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v0, v2}, LaR/H;->a(I)V

    .line 188
    :goto_3a
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v5, LbM/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 189
    invoke-virtual {v4, v2, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 190
    invoke-virtual {v0, v4}, LaR/H;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 191
    invoke-virtual {v0, v1}, LaR/H;->a(Z)V

    .line 194
    iget-object v1, p0, LaR/q;->g:LaR/u;

    invoke-interface {v1, v0, v2}, LaR/u;->a(LaR/H;Z)V

    .line 196
    iget-object v0, p0, LaR/q;->a:LaR/o;

    if-eqz v0, :cond_58

    .line 197
    iget-object v0, p0, LaR/q;->a:LaR/o;

    invoke-interface {v0, v3}, LaR/o;->a(Ljava/lang/String;)V

    :cond_58
    move v0, v2

    .line 200
    goto :goto_7

    .line 184
    :cond_5a
    new-instance v0, LaR/H;

    invoke-virtual {v4}, LaR/H;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v0, v4}, LaR/H;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_3a
.end method

.method public e()LaR/u;
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, LaR/q;->g:LaR/u;

    return-object v0
.end method

.method public t()Z
    .registers 2

    .prologue
    .line 302
    const/4 v0, 0x0

    return v0
.end method
