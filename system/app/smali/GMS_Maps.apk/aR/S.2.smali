.class public LaR/S;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/R;


# instance fields
.field private a:LaR/Y;

.field private b:LaR/ac;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 4
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, LaR/Y;

    invoke-direct {v0, p0, p1}, LaR/Y;-><init>(LaR/aa;Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, LaR/S;->a:LaR/Y;

    .line 35
    new-instance v0, LaR/ac;

    iget-object v1, p0, LaR/S;->a:LaR/Y;

    invoke-direct {v0, v1}, LaR/ac;-><init>(LaR/X;)V

    iput-object v0, p0, LaR/S;->b:LaR/ac;

    .line 36
    invoke-virtual {p0}, LaR/S;->i()V

    .line 37
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, LaR/S;->b:LaR/ac;

    if-eqz v0, :cond_c

    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->i()Z

    move-result v0

    if-nez v0, :cond_d

    .line 113
    :cond_c
    :goto_c
    return-void

    .line 102
    :cond_d
    :try_start_d
    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 103
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 104
    if-eqz v1, :cond_c

    .line 108
    invoke-virtual {p0, v0, p2, p3}, LaR/S;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_1b} :catch_1c

    goto :goto_c

    .line 109
    :catch_1c
    move-exception v0

    goto :goto_c
.end method


# virtual methods
.method public C_()V
    .registers 2

    .prologue
    .line 177
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->g()V

    .line 178
    return-void
.end method

.method public D_()V
    .registers 1

    .prologue
    .line 184
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 196
    return-void
.end method

.method public M_()V
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->h()V

    .line 190
    return-void
.end method

.method public N_()V
    .registers 1

    .prologue
    .line 202
    return-void
.end method

.method public a(I)LaR/T;
    .registers 3
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->d(I)LaR/T;

    move-result-object v0

    return-object v0
.end method

.method public a(ILaR/O;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->e(I)V

    .line 59
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, LaR/S;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V

    .line 61
    return-void
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1, p2, p3, p4}, LaR/ac;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    .line 148
    return-void
.end method

.method public a(LaR/ab;)V
    .registers 3
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1}, LaR/ac;->a(LaR/ab;)V

    .line 73
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1, p2, p3}, LaR/ac;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V

    .line 140
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 141
    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 3
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0, p1}, LaR/Y;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 54
    return-void
.end method

.method public a(ZLaR/O;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->b()V

    .line 86
    if-eqz p1, :cond_c

    .line 87
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->c()V

    .line 89
    :cond_c
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p2}, LaR/S;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaR/O;)V

    .line 90
    return-void
.end method

.method public b(LaR/ab;)V
    .registers 3
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0, p1}, LaR/ac;->b(LaR/ab;)V

    .line 78
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->d()V

    .line 160
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->e()V

    .line 166
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, LaR/S;->b:LaR/ac;

    invoke-virtual {v0}, LaR/ac;->h()V

    .line 154
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 171
    iget-object v0, p0, LaR/S;->a:LaR/Y;

    invoke-virtual {v0}, LaR/Y;->f()V

    .line 172
    return-void
.end method
