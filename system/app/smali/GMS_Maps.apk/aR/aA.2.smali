.class LaR/aA;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:LaR/ao;

.field private final b:LaR/aD;

.field private final c:LaT/h;

.field private final d:LaR/aC;


# direct methods
.method public constructor <init>(LaR/ao;LaR/aD;LaT/h;LaR/aC;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, LaR/aA;->a:LaR/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p2, p0, LaR/aA;->b:LaR/aD;

    .line 84
    iput-object p3, p0, LaR/aA;->c:LaT/h;

    .line 85
    iput-object p4, p0, LaR/aA;->d:LaR/aC;

    .line 86
    return-void
.end method

.method private a()LaR/aB;
    .registers 4

    .prologue
    .line 89
    sget-object v0, LaR/aB;->a:LaR/aB;

    .line 92
    iget-object v1, p0, LaR/aA;->b:LaR/aD;

    iget-object v1, v1, LaR/aD;->b:LaT/c;

    invoke-virtual {v1}, LaT/c;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, LaR/aA;->b:LaR/aD;

    iget-object v1, v1, LaR/aD;->b:LaT/c;

    invoke-virtual {v1}, LaT/c;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1c

    .line 94
    sget-object v0, LaR/aB;->b:LaR/aB;

    .line 97
    :cond_1c
    iget-object v1, p0, LaR/aA;->c:LaT/h;

    sget-object v2, LaT/e;->c:LaT/e;

    invoke-virtual {v1, v2}, LaT/h;->a(LaT/e;)Z

    move-result v1

    if-eqz v1, :cond_40

    iget-object v1, p0, LaR/aA;->b:LaR/aD;

    iget-object v1, v1, LaR/aD;->b:LaT/c;

    invoke-virtual {v1}, LaT/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "incorrect"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_40

    iget-object v1, p0, LaR/aA;->c:LaT/h;

    invoke-virtual {v1}, LaT/h;->j()Z

    move-result v1

    if-eqz v1, :cond_40

    .line 100
    sget-object v0, LaR/aB;->c:LaR/aB;

    .line 103
    :cond_40
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 108
    iget-object v0, p0, LaR/aA;->b:LaR/aD;

    iget-object v0, v0, LaR/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 110
    iget-object v0, p0, LaR/aA;->c:LaT/h;

    iget-object v1, p0, LaR/aA;->b:LaR/aD;

    iget-object v1, v1, LaR/aD;->b:LaT/c;

    iget-object v2, p0, LaR/aA;->a:LaR/ao;

    invoke-static {v2}, LaR/ao;->a(LaR/ao;)Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaT/a;->a(LaT/h;LaT/c;Lcom/google/googlenav/ai;)V

    .line 113
    invoke-static {}, LaT/j;->a()LaT/j;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 114
    iget-object v0, p0, LaR/aA;->a:LaR/ao;

    invoke-static {v0}, LaR/ao;->a(LaR/ao;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, LaT/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-static {}, LaT/j;->a()LaT/j;

    move-result-object v1

    iget-object v2, p0, LaR/aA;->c:LaT/h;

    invoke-virtual {v2}, LaT/h;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LaR/aA;->c:LaT/h;

    invoke-virtual {v3}, LaT/h;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LaR/aA;->b:LaR/aD;

    iget-object v4, v4, LaR/aD;->b:LaT/c;

    invoke-virtual {v4}, LaT/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, LaT/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 122
    :cond_42
    invoke-direct {p0}, LaR/aA;->a()LaR/aB;

    move-result-object v0

    .line 125
    iget-object v1, p0, LaR/aA;->b:LaR/aD;

    iget-object v1, v1, LaR/aD;->b:LaT/c;

    invoke-virtual {v1}, LaT/c;->e()Z

    move-result v1

    if-eqz v1, :cond_68

    .line 126
    iget-object v1, p0, LaR/aA;->a:LaR/ao;

    invoke-static {v1}, LaR/ao;->b(LaR/ao;)Lcom/google/googlenav/bh;

    move-result-object v1

    iget-object v2, p0, LaR/aA;->a:LaR/ao;

    invoke-static {v2}, LaR/ao;->a(LaR/ao;)Lcom/google/googlenav/ai;

    move-result-object v2

    iget-object v3, p0, LaR/aA;->b:LaR/aD;

    iget-object v3, v3, LaR/aD;->b:LaT/c;

    invoke-virtual {v3}, LaT/c;->f()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    .line 130
    :cond_68
    iget-object v1, p0, LaR/aA;->a:LaR/ao;

    invoke-static {v1}, LaR/ao;->a(LaR/ao;)Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->cc()LaT/o;

    move-result-object v1

    invoke-virtual {v1}, LaT/o;->a()V

    .line 133
    iget-object v1, p0, LaR/aA;->a:LaR/ao;

    invoke-static {v1}, LaR/ao;->c(LaR/ao;)LaN/aP;

    move-result-object v1

    invoke-virtual {v1, v5}, LaN/aP;->d(Z)V

    .line 135
    sget-object v1, LaR/az;->a:[I

    invoke-virtual {v0}, LaR/aB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_c4

    .line 146
    new-instance v0, Lcom/google/googlenav/ui/android/c;

    iget-object v1, p0, LaR/aA;->d:LaR/aC;

    iget-object v1, v1, LaR/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/c;-><init>(Landroid/content/Context;)V

    .line 148
    iget-object v1, p0, LaR/aA;->c:LaT/h;

    invoke-virtual {v1}, LaT/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/c;->b(Ljava/lang/String;)V

    .line 151
    :goto_9f
    return-void

    .line 137
    :pswitch_a0
    iget-object v0, p0, LaR/aA;->a:LaR/ao;

    iget-object v1, p0, LaR/aA;->d:LaR/aC;

    iget-object v1, v1, LaR/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LaR/aA;->c:LaT/h;

    invoke-static {v0, v1, v2}, LaR/ao;->a(LaR/ao;Landroid/content/Context;LaT/h;)V

    goto :goto_9f

    .line 140
    :pswitch_b0
    iget-object v0, p0, LaR/aA;->a:LaR/ao;

    iget-object v1, p0, LaR/aA;->d:LaR/aC;

    iget-object v1, v1, LaR/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LaR/aA;->c:LaT/h;

    iget-object v3, p0, LaR/aA;->b:LaR/aD;

    iget-object v3, v3, LaR/aD;->b:LaT/c;

    invoke-static {v0, v1, v2, v3}, LaR/ao;->a(LaR/ao;Landroid/content/Context;LaT/h;LaT/c;)V

    goto :goto_9f

    .line 135
    :pswitch_data_c4
    .packed-switch 0x1
        :pswitch_a0
        :pswitch_b0
    .end packed-switch
.end method
