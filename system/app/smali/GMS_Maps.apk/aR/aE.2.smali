.class public LaR/aE;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/H;


# static fields
.field private static final b:Lcom/google/googlenav/ui/aZ;


# instance fields
.field protected final a:LaR/aP;

.field private final c:I

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Lcom/google/googlenav/ui/af;

.field private final g:LaN/aP;

.field private final h:Z

.field private final i:Z

.field private final j:Lcom/google/googlenav/ai;

.field private final k:I

.field private l:Lcom/google/googlenav/ui/g;

.field private m:LaR/aM;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/googlenav/ui/aZ;->M:Lcom/google/googlenav/ui/aZ;

    sput-object v0, LaR/aE;->b:Lcom/google/googlenav/ui/aZ;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/af;LaN/aP;IZ)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 541
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, LaR/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/af;LaN/aP;IZZ)V

    .line 543
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/af;LaN/aP;IZLcom/google/googlenav/ai;ILcom/google/googlenav/ui/g;Z)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564
    if-eqz p6, :cond_22

    const/4 v0, 0x1

    .line 565
    :goto_6
    new-instance v1, LaR/aP;

    invoke-direct {v1, v0, p1, p2}, LaR/aP;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v1, p0, LaR/aE;->a:LaR/aP;

    .line 566
    iput-object p3, p0, LaR/aE;->d:Ljava/util/List;

    .line 567
    iput-object p4, p0, LaR/aE;->e:Ljava/util/List;

    .line 568
    iput-object p5, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    .line 569
    iput-object p6, p0, LaR/aE;->g:LaN/aP;

    .line 570
    iput p7, p0, LaR/aE;->c:I

    .line 571
    iput-boolean p8, p0, LaR/aE;->h:Z

    .line 572
    iput-object p9, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    .line 573
    iput p10, p0, LaR/aE;->k:I

    .line 574
    iput-object p11, p0, LaR/aE;->l:Lcom/google/googlenav/ui/g;

    .line 575
    iput-boolean p12, p0, LaR/aE;->i:Z

    .line 576
    return-void

    .line 564
    :cond_22
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/af;LaN/aP;IZZ)V
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 554
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, LaR/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/af;LaN/aP;IZLcom/google/googlenav/ai;ILcom/google/googlenav/ui/g;Z)V

    .line 556
    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Landroid/util/Pair;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 171
    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v3

    .line 173
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-static {v2}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-static {v3}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 176
    :cond_16
    const/4 v0, 0x0

    .line 202
    :goto_17
    return-object v0

    .line 182
    :cond_18
    const/16 v0, 0x28

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 185
    if-lez v0, :cond_6a

    .line 186
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    :goto_27
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_40

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_68

    .line 192
    :cond_40
    :try_start_40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 193
    const/16 v1, 0x2b4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Lab/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_5f
    .catch Ljava/lang/RuntimeException; {:try_start_40 .. :try_end_5f} :catch_65

    move-result-object v0

    .line 202
    :goto_60
    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_17

    .line 195
    :catch_65
    move-exception v0

    move-object v0, v2

    goto :goto_60

    :cond_68
    move v0, v1

    goto :goto_27

    :cond_6a
    move-object v0, v2

    goto :goto_60
.end method

.method static synthetic a(LaR/aE;)V
    .registers 1
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, LaR/aE;->e()V

    return-void
.end method

.method static synthetic a(LaR/aE;LaR/aM;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1}, LaR/aE;->c(LaR/aM;)V

    return-void
.end method

.method static synthetic a(LaR/aE;Lcom/google/googlenav/ui/ag;Landroid/widget/ImageView;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, LaR/aE;->a(Lcom/google/googlenav/ui/ag;Landroid/widget/ImageView;)V

    return-void
.end method

.method private a(LaR/aL;)V
    .registers 6
    .parameter

    .prologue
    .line 1082
    new-instance v1, LaR/aN;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, LaR/aN;-><init>(LaR/aF;)V

    .line 1083
    const v0, 0x7f040146

    iget-object v2, p1, LaR/aL;->j:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/bq;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, LaR/aN;->a:Landroid/view/ViewGroup;

    .line 1085
    iget-object v0, p1, LaR/aL;->j:Landroid/view/ViewGroup;

    iget-object v2, v1, LaR/aN;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1086
    iget-object v0, v1, LaR/aN;->a:Landroid/view/ViewGroup;

    const v2, 0x7f1002ab

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LaR/aN;->b:Landroid/widget/ImageView;

    .line 1088
    iget-object v0, p1, LaR/aL;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1089
    return-void
.end method

.method private a(LaR/aM;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1024
    invoke-direct {p0, p1, p2}, LaR/aE;->b(LaR/aM;I)LaR/aL;

    move-result-object v0

    .line 1026
    iget-object v1, p1, LaR/aM;->d:Landroid/view/ViewGroup;

    iget-object v2, v0, LaR/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1027
    iget-object v1, p1, LaR/aM;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1028
    iget-object v0, p1, LaR/aM;->i:Ljava/util/List;

    invoke-direct {p0, p2}, LaR/aE;->a(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1029
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 791
    if-eqz p2, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 792
    return-void

    .line 791
    :cond_7
    const/16 v0, 0x8

    goto :goto_3
.end method

.method private a(Lcom/google/googlenav/ai;Landroid/widget/ImageView;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1194
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ax()Lcom/google/googlenav/ap;

    move-result-object v0

    .line 1195
    invoke-static {v0}, LaN/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bA;

    move-result-object v1

    .line 1196
    if-nez v1, :cond_b

    .line 1216
    :goto_a
    return-void

    .line 1199
    :cond_b
    iget-object v0, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    check-cast v0, Lcom/google/googlenav/ui/bz;

    .line 1200
    invoke-virtual {v0}, Lcom/google/googlenav/ui/bz;->b()Lai/s;

    move-result-object v2

    .line 1203
    invoke-virtual {v2, v1}, Lai/s;->c(Lcom/google/googlenav/ui/bA;)Z

    move-result v3

    if-nez v3, :cond_22

    .line 1204
    new-instance v0, LaR/aK;

    invoke-direct {v0, p0, v2, v1, p2}, LaR/aK;-><init>(LaR/aE;Lai/s;Lcom/google/googlenav/ui/bA;Landroid/widget/ImageView;)V

    .line 1211
    invoke-virtual {v2, v1, v0}, Lai/s;->a(Lcom/google/googlenav/ui/bA;Lai/p;)V

    goto :goto_a

    .line 1213
    :cond_22
    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bz;->a(Lcom/google/googlenav/ui/ag;)LT/f;

    move-result-object v0

    check-cast v0, LU/f;

    .line 1214
    invoke-static {p2, v0}, LaR/G;->a(Landroid/widget/ImageView;LU/f;)V

    goto :goto_a
.end method

.method private a(Lcom/google/googlenav/ui/ag;Landroid/widget/ImageView;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1154
    iget-object v0, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    if-nez v0, :cond_5

    .line 1161
    :cond_4
    :goto_4
    return-void

    .line 1157
    :cond_5
    iget-object v0, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/af;->a(Lcom/google/googlenav/ui/ag;)LT/f;

    move-result-object v0

    .line 1158
    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    .line 1159
    check-cast v0, LU/f;

    invoke-virtual {v0}, LU/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_4
.end method

.method private a(Lcom/google/googlenav/ui/g;LaR/aL;LaR/aQ;Ljava/util/List;[Lcom/google/googlenav/aw;Z)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 712
    iget-object v0, p2, LaR/aL;->b:Landroid/view/View;

    if-eqz v0, :cond_22

    .line 713
    invoke-static {p3}, LaR/aE;->a(LaR/aQ;)Z

    move-result v0

    if-eqz v0, :cond_c5

    if-nez p6, :cond_c5

    move v0, v2

    .line 714
    :goto_f
    iget v1, p0, LaR/aE;->k:I

    if-lez v1, :cond_c8

    iget-object v1, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v1, :cond_c8

    move v1, v2

    .line 715
    :goto_18
    if-nez v0, :cond_1c

    if-eqz v1, :cond_cb

    :cond_1c
    move v0, v2

    .line 716
    :goto_1d
    iget-object v1, p2, LaR/aL;->b:Landroid/view/View;

    invoke-direct {p0, v1, v0}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 720
    :cond_22
    iget-boolean v0, p0, LaR/aE;->h:Z

    if-nez v0, :cond_26

    .line 726
    :cond_26
    iget-boolean v0, p0, LaR/aE;->h:Z

    if-eqz v0, :cond_42

    .line 727
    invoke-static {}, Lcom/google/googlenav/bm;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, LaR/aQ;->b:Ljava/lang/String;

    .line 728
    invoke-static {}, Lcom/google/googlenav/bm;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, LaR/aQ;->c:Ljava/lang/String;

    .line 730
    iget-object v0, p3, LaR/aQ;->c:Ljava/lang/String;

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 732
    const-string v0, ""

    iput-object v0, p3, LaR/aQ;->b:Ljava/lang/String;

    .line 736
    :cond_42
    iget-object v0, p2, LaR/aL;->d:Landroid/widget/ImageView;

    invoke-direct {p0, p2, p3}, LaR/aE;->a(LaR/aL;LaR/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 737
    iget-object v0, p2, LaR/aL;->c:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, LaR/aE;->b(LaR/aL;LaR/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 738
    iget-object v0, p2, LaR/aL;->e:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, LaR/aE;->c(LaR/aL;LaR/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 743
    iget-object v0, p2, LaR/aL;->f:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2, p3}, LaR/aE;->a(Lcom/google/googlenav/ui/g;LaR/aL;LaR/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 746
    iget-object v0, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_96

    .line 747
    iget-object v0, p2, LaR/aL;->k:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v2}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 748
    iget-object v0, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    iget-object v1, p2, LaR/aL;->l:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Lcom/google/googlenav/ai;Landroid/widget/ImageView;)V

    .line 749
    iget-object v0, p2, LaR/aL;->m:Landroid/widget/TextView;

    iget-object v1, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LaR/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 750
    iget-object v0, p2, LaR/aL;->n:Landroid/widget/TextView;

    iget-object v1, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LaR/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 751
    iget-object v0, p2, LaR/aL;->k:Landroid/view/ViewGroup;

    new-instance v1, LaR/aG;

    invoke-direct {v1, p0, p1}, LaR/aG;-><init>(LaR/aE;Lcom/google/googlenav/ui/g;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 759
    :cond_96
    invoke-static {p3}, LaR/aE;->a(LaR/aQ;)Z

    move-result v0

    if-eqz v0, :cond_ce

    .line 760
    iget-object v0, p2, LaR/aL;->g:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, LaR/aE;->d(LaR/aL;LaR/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 766
    :goto_a5
    if-eqz p4, :cond_126

    .line 767
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p2, LaR/aL;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_e1

    .line 770
    iget-object v0, p2, LaR/aL;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_b9
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_e1

    .line 771
    invoke-direct {p0, p2}, LaR/aE;->a(LaR/aL;)V

    .line 770
    add-int/lit8 v0, v0, 0x1

    goto :goto_b9

    :cond_c5
    move v0, v3

    .line 713
    goto/16 :goto_f

    :cond_c8
    move v1, v3

    .line 714
    goto/16 :goto_18

    :cond_cb
    move v0, v3

    .line 715
    goto/16 :goto_1d

    .line 762
    :cond_ce
    iget-object v0, p2, LaR/aL;->h:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, LaR/aE;->e(LaR/aL;LaR/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 763
    iget-object v0, p2, LaR/aL;->i:Landroid/widget/TextView;

    invoke-direct {p0, p2, p3}, LaR/aE;->f(LaR/aL;LaR/aQ;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    goto :goto_a5

    :cond_e1
    move v4, v3

    .line 774
    :goto_e2
    iget-object v0, p2, LaR/aL;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_11b

    .line 775
    iget-object v0, p2, LaR/aL;->p:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/aN;

    .line 776
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lt v4, v1, :cond_101

    .line 777
    iget-object v0, v0, LaR/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 774
    :goto_fd
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_e2

    .line 779
    :cond_101
    iget-object v1, v0, LaR/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v2}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 780
    invoke-interface {p4, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/bA;

    invoke-direct {p0, v0, v1}, LaR/aE;->a(LaR/aN;Lcom/google/googlenav/ui/bA;)Z

    move-result v1

    .line 781
    iget-object v5, v0, LaR/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, v5, v1}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 782
    iget-object v0, v0, LaR/aN;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, p1, v0, p5, v4}, LaR/aE;->a(Lcom/google/googlenav/ui/g;Landroid/view/ViewGroup;[Lcom/google/googlenav/aw;I)V

    goto :goto_fd

    .line 786
    :cond_11b
    iget-object v0, p2, LaR/aL;->j:Landroid/view/ViewGroup;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_127

    :goto_123
    invoke-direct {p0, v0, v2}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 788
    :cond_126
    return-void

    :cond_127
    move v2, v3

    .line 786
    goto :goto_123
.end method

.method private a(Lcom/google/googlenav/ui/g;Landroid/view/ViewGroup;[Lcom/google/googlenav/aw;I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1184
    new-instance v0, LaR/aJ;

    invoke-direct {v0, p0, p1, p4, p3}, LaR/aJ;-><init>(LaR/aE;Lcom/google/googlenav/ui/g;I[Lcom/google/googlenav/aw;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1191
    return-void
.end method

.method private a(I)Z
    .registers 3
    .parameter

    .prologue
    .line 1122
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    invoke-virtual {v0}, LaR/aP;->a()I

    move-result v0

    if-le v0, p1, :cond_1a

    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    iget-object v0, v0, LaR/aP;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/aQ;

    invoke-static {v0}, LaR/aE;->a(LaR/aQ;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private a(LaR/aL;LaR/aQ;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 800
    iget-object v0, p2, LaR/aQ;->b:Ljava/lang/String;

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 801
    invoke-static {}, Lcom/google/googlenav/ui/bq;->d()Lcom/google/googlenav/ui/bq;

    move-result-object v1

    invoke-static {p2}, LaR/aE;->a(LaR/aQ;)Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x2

    :goto_14
    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/ui/bq;->a(ZI)LT/f;

    move-result-object v0

    .line 804
    iget-object v1, p1, LaR/aL;->d:Landroid/widget/ImageView;

    check-cast v0, LU/f;

    invoke-virtual {v0}, LU/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 821
    :goto_23
    return v3

    .line 801
    :cond_24
    const/4 v0, 0x0

    goto :goto_14

    .line 806
    :cond_26
    new-instance v1, Lcom/google/googlenav/ui/bA;

    iget-object v0, p2, LaR/aQ;->b:Ljava/lang/String;

    sget v2, Lcom/google/googlenav/ui/bq;->bx:I

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/ui/bA;-><init>(Ljava/lang/String;I)V

    .line 809
    iget-object v0, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    check-cast v0, Lcom/google/googlenav/ui/bz;

    .line 810
    invoke-virtual {v0}, Lcom/google/googlenav/ui/bz;->b()Lai/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lai/s;->c(Lcom/google/googlenav/ui/bA;)Z

    move-result v2

    if-nez v2, :cond_4a

    .line 811
    invoke-virtual {v0}, Lcom/google/googlenav/ui/bz;->b()Lai/s;

    move-result-object v0

    new-instance v2, LaR/aH;

    invoke-direct {v2, p0, v1, p1}, LaR/aH;-><init>(LaR/aE;Lcom/google/googlenav/ui/bA;LaR/aL;)V

    invoke-virtual {v0, v1, v2}, Lai/s;->a(Lcom/google/googlenav/ui/bA;Lai/p;)V

    goto :goto_23

    .line 818
    :cond_4a
    iget-object v0, p1, LaR/aL;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v0}, LaR/aE;->a(Lcom/google/googlenav/ui/ag;Landroid/widget/ImageView;)V

    goto :goto_23
.end method

.method private a(LaR/aM;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 650
    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    iget-object v1, v1, LaR/aP;->b:Ljava/lang/String;

    if-eqz v1, :cond_f

    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    invoke-virtual {v1}, LaR/aP;->a()I

    move-result v1

    if-nez v1, :cond_10

    .line 656
    :cond_f
    :goto_f
    return v0

    .line 654
    :cond_10
    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    iget-object v1, v1, LaR/aP;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 655
    iget-object v2, p1, LaR/aM;->b:Landroid/widget/TextView;

    sget-object v3, Lcom/google/googlenav/ui/aZ;->bY:Lcom/google/googlenav/ui/aZ;

    invoke-static {v2, v1, v3}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)V

    .line 656
    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    iget-object v1, v1, LaR/aP;->b:Ljava/lang/String;

    invoke-static {v1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    const/4 v0, 0x1

    goto :goto_f
.end method

.method private a(LaR/aN;Lcom/google/googlenav/ui/bA;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x30

    const/4 v0, 0x1

    .line 961
    iget-object v1, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    if-nez v1, :cond_9

    .line 962
    const/4 v0, 0x0

    .line 975
    :goto_8
    return v0

    .line 966
    :cond_9
    iget-object v1, p1, LaR/aN;->b:Landroid/widget/ImageView;

    .line 967
    iget-object v2, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    invoke-interface {v2, p2}, Lcom/google/googlenav/ui/af;->a(Lcom/google/googlenav/ui/ag;)LT/f;

    move-result-object v2

    invoke-interface {v2}, LT/f;->a()I

    move-result v2

    .line 968
    iget-object v3, p0, LaR/aE;->f:Lcom/google/googlenav/ui/af;

    invoke-interface {v3, p2}, Lcom/google/googlenav/ui/af;->a(Lcom/google/googlenav/ui/ag;)LT/f;

    move-result-object v3

    invoke-interface {v3}, LT/f;->b()I

    move-result v3

    .line 969
    mul-int/lit8 v2, v2, 0x30

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    div-int/2addr v2, v3

    .line 970
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setMaxWidth(I)V

    .line 971
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 972
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 974
    iget-object v1, p1, LaR/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, p2, v1}, LaR/aE;->a(Lcom/google/googlenav/ui/ag;Landroid/widget/ImageView;)V

    goto :goto_8
.end method

.method private static a(LaR/aQ;)Z
    .registers 2
    .parameter

    .prologue
    .line 1130
    iget-object v0, p0, LaR/aQ;->e:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 950
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 951
    invoke-static {p1, p2}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 953
    :cond_9
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private a(Lcom/google/googlenav/ui/g;LaR/aL;LaR/aQ;)Z
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 871
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 873
    iget-object v0, p3, LaR/aQ;->f:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_65

    move v0, v1

    .line 874
    :goto_10
    iget-object v3, p0, LaR/aE;->g:LaN/aP;

    if-eqz v3, :cond_67

    move v3, v1

    .line 875
    :goto_15
    iget-object v4, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v4, :cond_69

    move v4, v1

    .line 876
    :goto_1a
    iget-object v5, p3, LaR/aQ;->h:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    iget-object v7, p3, LaR/aQ;->g:Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-le v5, v7, :cond_6b

    move v5, v1

    .line 879
    :goto_29
    iget-boolean v7, p0, LaR/aE;->h:Z

    if-nez v7, :cond_33

    if-nez v4, :cond_6d

    if-nez v3, :cond_6d

    if-eqz v5, :cond_6d

    .line 880
    :cond_33
    iget-object v4, p3, LaR/aQ;->h:Ljava/lang/CharSequence;

    .line 886
    :goto_35
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_70

    move v5, v1

    .line 888
    :goto_3c
    if-eqz v0, :cond_43

    .line 889
    iget-object v7, p3, LaR/aQ;->f:Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 892
    :cond_43
    iget-object v7, p3, LaR/aQ;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 893
    new-instance v8, LaR/aI;

    invoke-direct {v8, p0, p1, p2, v7}, LaR/aI;-><init>(LaR/aE;Lcom/google/googlenav/ui/g;LaR/aL;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 915
    if-eqz v5, :cond_4f

    .line 916
    invoke-virtual {v6, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 920
    :cond_4f
    if-nez v3, :cond_55

    iget-object v3, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v3, :cond_5a

    .line 922
    :cond_55
    iget-object v3, p2, LaR/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 925
    :cond_5a
    iget-object v3, p2, LaR/aL;->f:Landroid/widget/TextView;

    invoke-static {v3, v6}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 926
    if-nez v0, :cond_63

    if-eqz v5, :cond_64

    :cond_63
    move v2, v1

    :cond_64
    return v2

    :cond_65
    move v0, v2

    .line 873
    goto :goto_10

    :cond_67
    move v3, v2

    .line 874
    goto :goto_15

    :cond_69
    move v4, v2

    .line 875
    goto :goto_1a

    :cond_6b
    move v5, v2

    .line 876
    goto :goto_29

    .line 884
    :cond_6d
    iget-object v4, p3, LaR/aQ;->g:Ljava/lang/CharSequence;

    goto :goto_35

    :cond_70
    move v5, v2

    .line 886
    goto :goto_3c
.end method

.method private a(Lcom/google/googlenav/ui/g;LaR/aM;)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 664
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    iget-object v0, v0, LaR/aP;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_d

    .line 700
    :goto_c
    return v4

    .line 667
    :cond_d
    iget-object v0, p2, LaR/aM;->e:Landroid/widget/TextView;

    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    iget-object v1, v1, LaR/aP;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 668
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    iget-object v0, v0, LaR/aP;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p2, LaR/aM;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_3c

    .line 671
    iget-object v0, p2, LaR/aM;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_2c
    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    iget-object v1, v1, LaR/aP;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3c

    .line 673
    invoke-direct {p0, p2}, LaR/aE;->b(LaR/aM;)V

    .line 672
    add-int/lit8 v0, v0, 0x1

    goto :goto_2c

    :cond_3c
    move v3, v4

    .line 676
    :goto_3d
    iget-object v0, p2, LaR/aM;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_96

    .line 677
    iget-object v0, p2, LaR/aM;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/aO;

    .line 679
    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    iget-object v1, v1, LaR/aP;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v3, v1, :cond_60

    .line 680
    iget-object v0, v0, LaR/aO;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v4}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 676
    :goto_5c
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3d

    .line 682
    :cond_60
    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    iget-object v1, v1, LaR/aP;->e:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 683
    iget-object v2, v0, LaR/aO;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v2, v5}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 684
    iget-object v2, p0, LaR/aE;->a:LaR/aP;

    iget-object v2, v2, LaR/aP;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v3, v2, :cond_80

    .line 685
    iget-object v2, v0, LaR/aO;->b:Landroid/view/View;

    invoke-direct {p0, v2, v5}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 687
    :cond_80
    iget-object v6, v0, LaR/aO;->c:Landroid/widget/TextView;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    sget-object v7, LaR/aE;->b:Lcom/google/googlenav/ui/aZ;

    invoke-static {v6, v2, v7}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)V

    .line 690
    iget-object v0, v0, LaR/aO;->a:Landroid/view/ViewGroup;

    .line 691
    new-instance v2, LaR/aF;

    invoke-direct {v2, p0, p1, v1}, LaR/aF;-><init>(LaR/aE;Lcom/google/googlenav/ui/g;Landroid/util/Pair;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5c

    :cond_96
    move v4, v5

    .line 700
    goto/16 :goto_c
.end method

.method static synthetic b(LaR/aE;)I
    .registers 2
    .parameter

    .prologue
    .line 67
    iget v0, p0, LaR/aE;->k:I

    return v0
.end method

.method private b(LaR/aM;I)LaR/aL;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1033
    new-instance v2, LaR/aL;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, LaR/aL;-><init>(LaR/aF;)V

    .line 1035
    invoke-direct {p0, p2}, LaR/aE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_be

    const v0, 0x7f040156

    :goto_10
    iget-object v3, p1, LaR/aM;->d:Landroid/view/ViewGroup;

    invoke-static {v0, v3, v1}, Lcom/google/googlenav/ui/bq;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    .line 1039
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, LaR/aL;->b:Landroid/view/View;

    .line 1041
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->c:Landroid/widget/TextView;

    .line 1043
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, LaR/aL;->d:Landroid/widget/ImageView;

    .line 1045
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034d

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->e:Landroid/widget/TextView;

    .line 1048
    iget-object v0, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_84

    .line 1049
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10037f

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, LaR/aL;->k:Landroid/view/ViewGroup;

    .line 1051
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100380

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, LaR/aL;->l:Landroid/widget/ImageView;

    .line 1053
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100381

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->m:Landroid/widget/TextView;

    .line 1055
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100382

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->n:Landroid/widget/TextView;

    .line 1059
    :cond_84
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f10034e

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->f:Landroid/widget/TextView;

    .line 1061
    invoke-direct {p0, p2}, LaR/aE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 1062
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100383

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->g:Landroid/widget/TextView;

    .line 1070
    :goto_a4
    iput p2, v2, LaR/aL;->o:I

    .line 1072
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100352

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, LaR/aL;->j:Landroid/view/ViewGroup;

    move v0, v1

    .line 1074
    :goto_b4
    const/16 v1, 0xa

    if-ge v0, v1, :cond_de

    .line 1075
    invoke-direct {p0, v2}, LaR/aE;->a(LaR/aL;)V

    .line 1074
    add-int/lit8 v0, v0, 0x1

    goto :goto_b4

    .line 1035
    :cond_be
    const v0, 0x7f040145

    goto/16 :goto_10

    .line 1065
    :cond_c3
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100350

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->h:Landroid/widget/TextView;

    .line 1067
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100351

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aL;->i:Landroid/widget/TextView;

    goto :goto_a4

    .line 1078
    :cond_de
    return-object v2
.end method

.method private b(LaR/aM;)V
    .registers 6
    .parameter

    .prologue
    .line 1092
    new-instance v1, LaR/aO;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, LaR/aO;-><init>(LaR/aF;)V

    .line 1093
    const v0, 0x7f040151

    iget-object v2, p1, LaR/aM;->g:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/ui/bq;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, LaR/aO;->a:Landroid/view/ViewGroup;

    .line 1095
    iget-object v0, p1, LaR/aM;->g:Landroid/view/ViewGroup;

    iget-object v2, v1, LaR/aO;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1097
    iget-object v0, v1, LaR/aO;->a:Landroid/view/ViewGroup;

    const v2, 0x7f10036e

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, LaR/aO;->b:Landroid/view/View;

    .line 1100
    iget-object v0, v1, LaR/aO;->a:Landroid/view/ViewGroup;

    const v2, 0x7f10036d

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LaR/aO;->c:Landroid/widget/TextView;

    .line 1103
    iget-object v0, p1, LaR/aM;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1104
    return-void
.end method

.method private b(LaR/aL;LaR/aQ;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 829
    iget-object v0, p2, LaR/aQ;->c:Ljava/lang/String;

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    const/4 v0, 0x1

    move v1, v0

    .line 830
    :goto_a
    if-eqz v1, :cond_1f

    .line 832
    iget-boolean v0, p2, LaR/aQ;->k:Z

    if-eqz v0, :cond_23

    sget-object v0, Lcom/google/googlenav/ui/aZ;->bg:Lcom/google/googlenav/ui/aZ;

    .line 834
    :goto_12
    iget-object v2, p1, LaR/aL;->c:Landroid/widget/TextView;

    iget-object v3, p2, LaR/aQ;->c:Ljava/lang/String;

    invoke-static {p2}, LaR/aE;->a(LaR/aQ;)Z

    move-result v4

    if-eqz v4, :cond_26

    :goto_1c
    invoke-static {v2, v3, v0}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)V

    .line 837
    :cond_1f
    return v1

    .line 829
    :cond_20
    const/4 v0, 0x0

    move v1, v0

    goto :goto_a

    .line 832
    :cond_23
    sget-object v0, Lcom/google/googlenav/ui/aZ;->bf:Lcom/google/googlenav/ui/aZ;

    goto :goto_12

    .line 834
    :cond_26
    sget-object v0, Lcom/google/googlenav/ui/aZ;->aT:Lcom/google/googlenav/ui/aZ;

    goto :goto_1c
.end method

.method static synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 2
    .parameter

    .prologue
    .line 67
    invoke-static {p0}, LaR/aE;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(LaR/aE;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LaR/aE;->j:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private c(LaR/aM;)V
    .registers 2
    .parameter

    .prologue
    .line 1168
    iput-object p1, p0, LaR/aE;->m:LaR/aM;

    .line 1169
    return-void
.end method

.method private c(LaR/aL;LaR/aQ;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 845
    iget-object v0, p2, LaR/aQ;->d:Ljava/lang/String;

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    move v1, v0

    .line 846
    :goto_a
    if-eqz v1, :cond_1b

    .line 847
    iget-object v2, p1, LaR/aL;->e:Landroid/widget/TextView;

    iget-object v3, p2, LaR/aQ;->d:Ljava/lang/String;

    invoke-static {p2}, LaR/aE;->a(LaR/aQ;)Z

    move-result v0

    if-eqz v0, :cond_1f

    sget-object v0, Lcom/google/googlenav/ui/aZ;->ba:Lcom/google/googlenav/ui/aZ;

    :goto_18
    invoke-static {v2, v3, v0}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)V

    .line 850
    :cond_1b
    return v1

    .line 845
    :cond_1c
    const/4 v0, 0x0

    move v1, v0

    goto :goto_a

    .line 847
    :cond_1f
    sget-object v0, Lcom/google/googlenav/ui/aZ;->aY:Lcom/google/googlenav/ui/aZ;

    goto :goto_18
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 2
    .parameter

    .prologue
    .line 1126
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method static synthetic d(LaR/aE;)Z
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-boolean v0, p0, LaR/aE;->h:Z

    return v0
.end method

.method private d(LaR/aL;LaR/aQ;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 855
    iget-object v0, p2, LaR/aQ;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    const/4 v0, 0x1

    .line 856
    :goto_d
    if-eqz v0, :cond_16

    .line 857
    iget-object v1, p1, LaR/aL;->g:Landroid/widget/TextView;

    iget-object v2, p2, LaR/aQ;->e:Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bq;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 859
    :cond_16
    return v0

    .line 855
    :cond_17
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private e()V
    .registers 2

    .prologue
    .line 1176
    const/4 v0, 0x0

    iput-object v0, p0, LaR/aE;->m:LaR/aM;

    .line 1177
    return-void
.end method

.method private e(LaR/aL;LaR/aQ;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 934
    iget-object v0, p1, LaR/aL;->h:Landroid/widget/TextView;

    iget-object v1, p2, LaR/aQ;->i:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private f(LaR/aL;LaR/aQ;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 942
    iget-object v0, p1, LaR/aL;->i:Landroid/widget/TextView;

    iget-object v1, p2, LaR/aQ;->j:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, LaR/aE;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 1108
    iget v0, p0, LaR/aE;->c:I

    return v0
.end method

.method public a(Landroid/view/View;)LaR/bE;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 980
    iget-boolean v0, p0, LaR/aE;->i:Z

    if-eqz v0, :cond_8

    .line 981
    invoke-static {p1}, LaN/aR;->a(Landroid/view/View;)V

    .line 984
    :cond_8
    iget-object v0, p0, LaR/aE;->g:LaN/aP;

    if-eqz v0, :cond_5e

    const/4 v0, 0x1

    .line 985
    :goto_d
    if-nez v0, :cond_1e

    .line 986
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 989
    :cond_1e
    new-instance v2, LaR/aM;

    invoke-direct {v2}, LaR/aM;-><init>()V

    .line 992
    const v0, 0x7f10025f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, LaR/aM;->a:Landroid/view/ViewGroup;

    .line 993
    iget-object v0, v2, LaR/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100260

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aM;->b:Landroid/widget/TextView;

    .line 995
    iget-object v0, v2, LaR/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100236

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, LaR/aM;->c:Landroid/view/View;

    .line 997
    iget-object v0, v2, LaR/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100261

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, LaR/aM;->d:Landroid/view/ViewGroup;

    move v0, v1

    .line 1000
    :goto_54
    const/16 v3, 0x8

    if-ge v0, v3, :cond_60

    .line 1001
    invoke-direct {p0, v2, v0}, LaR/aE;->a(LaR/aM;I)V

    .line 1000
    add-int/lit8 v0, v0, 0x1

    goto :goto_54

    :cond_5e
    move v0, v1

    .line 984
    goto :goto_d

    .line 1004
    :cond_60
    iget-object v0, v2, LaR/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100262

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, LaR/aM;->e:Landroid/widget/TextView;

    .line 1006
    iget-object v0, v2, LaR/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100237

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, LaR/aM;->f:Landroid/view/View;

    .line 1008
    iget-object v0, v2, LaR/aM;->a:Landroid/view/ViewGroup;

    const v3, 0x7f100263

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, LaR/aM;->g:Landroid/view/ViewGroup;

    move v0, v1

    .line 1011
    :goto_86
    const/4 v1, 0x5

    if-ge v0, v1, :cond_8f

    .line 1012
    invoke-direct {p0, v2}, LaR/aE;->b(LaR/aM;)V

    .line 1011
    add-int/lit8 v0, v0, 0x1

    goto :goto_86

    .line 1015
    :cond_8f
    return-object v2
.end method

.method public a(Lcom/google/googlenav/ui/g;LaR/bE;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 580
    check-cast p2, LaR/aM;

    .line 582
    if-eqz p1, :cond_9

    .line 583
    iput-object p1, p0, LaR/aE;->l:Lcom/google/googlenav/ui/g;

    .line 587
    :cond_9
    invoke-virtual {p2, p0}, LaR/aM;->a(LaR/aE;)V

    .line 590
    invoke-direct {p0, p2}, LaR/aE;->a(LaR/aM;)Z

    move-result v0

    .line 591
    iget-object v1, p2, LaR/aM;->b:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 592
    iget-object v1, p2, LaR/aM;->c:Landroid/view/View;

    if-eqz v1, :cond_1e

    .line 593
    iget-object v1, p2, LaR/aM;->c:Landroid/view/View;

    invoke-direct {p0, v1, v0}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 597
    :cond_1e
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    invoke-virtual {v0}, LaR/aP;->a()I

    move-result v0

    iget-object v1, p2, LaR/aM;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_40

    .line 600
    iget-object v0, p2, LaR/aM;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_32
    iget-object v1, p0, LaR/aE;->a:LaR/aP;

    invoke-virtual {v1}, LaR/aP;->a()I

    move-result v1

    if-ge v0, v1, :cond_40

    .line 601
    invoke-direct {p0, p2, v0}, LaR/aE;->a(LaR/aM;I)V

    .line 600
    add-int/lit8 v0, v0, 0x1

    goto :goto_32

    :cond_40
    move v1, v7

    .line 607
    :goto_41
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    iget-object v0, v0, LaR/aP;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_97

    .line 608
    iget-object v0, p2, LaR/aM;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 609
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    iget-object v0, v0, LaR/aP;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/aQ;

    invoke-static {v0}, LaR/aE;->a(LaR/aQ;)Z

    move-result v0

    .line 610
    if-eq v0, v2, :cond_93

    .line 611
    invoke-direct {p0, p2, v1}, LaR/aE;->b(LaR/aM;I)LaR/aL;

    move-result-object v2

    .line 612
    iget-object v0, p2, LaR/aM;->h:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 613
    iget-object v3, p2, LaR/aM;->i:Ljava/util/List;

    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    iget-object v0, v0, LaR/aP;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/aQ;

    invoke-static {v0}, LaR/aE;->a(LaR/aQ;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 614
    iget-object v0, p2, LaR/aM;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 615
    iget-object v0, p2, LaR/aM;->d:Landroid/view/ViewGroup;

    iget-object v2, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 607
    :cond_93
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_41

    :cond_97
    move v8, v7

    .line 620
    :goto_98
    iget-object v0, p2, LaR/aM;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_102

    .line 621
    iget-object v0, p2, LaR/aM;->h:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaR/aL;

    .line 622
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    invoke-virtual {v0}, LaR/aP;->a()I

    move-result v0

    if-ge v8, v0, :cond_fc

    .line 623
    iget-object v0, p0, LaR/aE;->d:Ljava/util/List;

    if-eqz v0, :cond_bc

    iget-object v0, p0, LaR/aE;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e6

    :cond_bc
    move-object v4, v9

    .line 625
    :goto_bd
    iget-object v0, p0, LaR/aE;->e:Ljava/util/List;

    if-eqz v0, :cond_c9

    iget-object v0, p0, LaR/aE;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f0

    :cond_c9
    move-object v5, v9

    .line 627
    :goto_ca
    iget-object v0, p0, LaR/aE;->a:LaR/aP;

    iget-object v0, v0, LaR/aP;->c:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaR/aQ;

    .line 628
    iget-object v1, p0, LaR/aE;->l:Lcom/google/googlenav/ui/g;

    if-nez v8, :cond_fa

    move v6, v10

    :goto_d9
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaR/aE;->a(Lcom/google/googlenav/ui/g;LaR/aL;LaR/aQ;Ljava/util/List;[Lcom/google/googlenav/aw;Z)V

    .line 630
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v10}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 620
    :goto_e2
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_98

    .line 623
    :cond_e6
    iget-object v0, p0, LaR/aE;->d:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v4, v0

    goto :goto_bd

    .line 625
    :cond_f0
    iget-object v0, p0, LaR/aE;->e:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/aw;

    move-object v5, v0

    goto :goto_ca

    :cond_fa
    move v6, v7

    .line 628
    goto :goto_d9

    .line 632
    :cond_fc
    iget-object v0, v2, LaR/aL;->a:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v7}, LaR/aE;->a(Landroid/view/View;Z)V

    goto :goto_e2

    .line 637
    :cond_102
    iget-object v0, p0, LaR/aE;->l:Lcom/google/googlenav/ui/g;

    invoke-direct {p0, v0, p2}, LaR/aE;->a(Lcom/google/googlenav/ui/g;LaR/aM;)Z

    move-result v0

    .line 639
    iget-object v1, p2, LaR/aM;->e:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 640
    iget-object v1, p2, LaR/aM;->f:Landroid/view/View;

    if-eqz v1, :cond_116

    .line 641
    iget-object v1, p2, LaR/aM;->f:Landroid/view/View;

    invoke-direct {p0, v1, v0}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 643
    :cond_116
    iget-object v1, p2, LaR/aM;->g:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, LaR/aE;->a(Landroid/view/View;Z)V

    .line 644
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 1113
    const v0, 0x7f0400c7

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 1118
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 1141
    iget-object v0, p0, LaR/aE;->d:Ljava/util/List;

    if-eqz v0, :cond_4f

    iget-object v0, p0, LaR/aE;->m:LaR/aM;

    if-eqz v0, :cond_4f

    move v3, v4

    .line 1142
    :goto_a
    iget-object v0, p0, LaR/aE;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_46

    .line 1143
    iget-object v0, p0, LaR/aE;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move v5, v4

    .line 1144
    :goto_1b
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_42

    .line 1145
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/ag;

    iget-object v2, p0, LaR/aE;->m:LaR/aM;

    iget-object v2, v2, LaR/aM;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaR/aL;

    iget-object v2, v2, LaR/aL;->p:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaR/aN;

    iget-object v2, v2, LaR/aN;->b:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2}, LaR/aE;->a(Lcom/google/googlenav/ui/ag;Landroid/widget/ImageView;)V

    .line 1144
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1b

    .line 1142
    :cond_42
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    .line 1149
    :cond_46
    iget-object v0, p0, LaR/aE;->l:Lcom/google/googlenav/ui/g;

    const/16 v1, 0x18

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/ui/g;->a(IILjava/lang/Object;)Z

    .line 1151
    :cond_4f
    return-void
.end method
