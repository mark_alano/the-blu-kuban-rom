.class LaR/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/bE;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

.field private n:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LaR/ah;)V
    .registers 2
    .parameter

    .prologue
    .line 516
    invoke-direct {p0}, LaR/ak;-><init>()V

    return-void
.end method

.method static synthetic a(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->c:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(LaR/ak;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->d:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic a(LaR/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->e:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(LaR/ak;Lcom/google/googlenav/ui/android/MultipleTextLineLayout;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->m:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object p1
.end method

.method static synthetic b(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->g:Landroid/view/View;

    return-object p1
.end method

.method static synthetic b(LaR/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->f:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic b(LaR/ak;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->n:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object v0
.end method

.method static synthetic b(LaR/ak;Lcom/google/googlenav/ui/android/MultipleTextLineLayout;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->n:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object p1
.end method

.method static synthetic c(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->o:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->h:Landroid/view/View;

    return-object p1
.end method

.method static synthetic c(LaR/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->k:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic d(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->p:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->i:Landroid/view/View;

    return-object p1
.end method

.method static synthetic d(LaR/ak;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->l:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic e(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->q:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->j:Landroid/view/View;

    return-object p1
.end method

.method static synthetic f(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->o:Landroid/view/View;

    return-object p1
.end method

.method static synthetic g(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->p:Landroid/view/View;

    return-object p1
.end method

.method static synthetic h(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->q:Landroid/view/View;

    return-object p1
.end method

.method static synthetic i(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic i(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->b:Landroid/view/View;

    return-object p1
.end method

.method static synthetic j(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(LaR/ak;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 516
    iput-object p1, p0, LaR/ak;->a:Landroid/view/View;

    return-object p1
.end method

.method static synthetic k(LaR/ak;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(LaR/ak;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic m(LaR/ak;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic n(LaR/ak;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic o(LaR/ak;)Lcom/google/googlenav/ui/android/MultipleTextLineLayout;
    .registers 2
    .parameter

    .prologue
    .line 516
    iget-object v0, p0, LaR/ak;->m:Lcom/google/googlenav/ui/android/MultipleTextLineLayout;

    return-object v0
.end method
