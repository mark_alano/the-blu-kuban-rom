.class public LaR/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/H;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field private c:Lcom/google/googlenav/ai;

.field private d:LaN/aP;

.field private e:LaN/m;

.field private f:I

.field private final g:Lcom/google/googlenav/bh;

.field private final h:Z

.field private i:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;LaN/aP;LaN/m;ILcom/google/googlenav/bh;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaR/ao;->a:Ljava/util/List;

    .line 192
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaR/ao;->b:Ljava/util/List;

    .line 197
    iput-object p1, p0, LaR/ao;->c:Lcom/google/googlenav/ai;

    .line 198
    iput-object p2, p0, LaR/ao;->d:LaN/aP;

    .line 199
    iput-object p3, p0, LaR/ao;->e:LaN/m;

    .line 200
    iput p4, p0, LaR/ao;->f:I

    .line 201
    iput-object p5, p0, LaR/ao;->g:Lcom/google/googlenav/bh;

    .line 202
    iput-boolean p6, p0, LaR/ao;->h:Z

    .line 203
    return-void
.end method

.method static synthetic a(LaR/ao;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, LaR/ao;->c:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic a(LaR/ao;Landroid/content/Context;LaT/h;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, LaR/ao;->a(Landroid/content/Context;LaT/h;)V

    return-void
.end method

.method static synthetic a(LaR/ao;Landroid/content/Context;LaT/h;LaT/c;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, LaR/ao;->a(Landroid/content/Context;LaT/h;LaT/c;)V

    return-void
.end method

.method private a(Landroid/content/Context;LaT/h;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 348
    invoke-virtual {p2}, LaT/h;->k()Ljava/lang/String;

    move-result-object v0

    .line 350
    new-instance v1, LaR/aq;

    invoke-direct {v1, p0, p2, v0}, LaR/aq;-><init>(LaR/ao;LaT/h;Ljava/lang/String;)V

    .line 383
    new-instance v0, LaR/at;

    invoke-direct {v0, p0, p2}, LaR/at;-><init>(LaR/ao;LaT/h;)V

    .line 391
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 392
    invoke-virtual {p2}, LaT/h;->l()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_20

    .line 393
    invoke-virtual {p2}, LaT/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 395
    :cond_20
    invoke-virtual {p2}, LaT/h;->m()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2d

    .line 396
    invoke-virtual {p2}, LaT/h;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 398
    :cond_2d
    const/16 v3, 0x68

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, LaR/au;

    invoke-direct {v4, p0, p2}, LaR/au;-><init>(LaR/ao;LaT/h;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 410
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v3

    if-eqz v3, :cond_59

    .line 411
    iget-object v3, p0, LaR/ao;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    iget-object v1, p0, LaR/ao;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_59
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, LaR/ao;->i:Landroid/app/AlertDialog;

    .line 420
    return-void
.end method

.method private a(Landroid/content/Context;LaT/h;LaT/c;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 469
    invoke-virtual {p3}, LaT/c;->d()Ljava/util/List;

    move-result-object v0

    new-instance v1, LaR/aw;

    invoke-direct {v1, p0}, LaR/aw;-><init>(LaR/ao;)V

    invoke-static {v0, v1}, Lcom/google/common/collect/cx;->a(Ljava/util/List;Lcom/google/common/base/D;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p3}, LaT/c;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 477
    new-instance v1, LaR/ax;

    invoke-direct {v1, p0, p3, p2, p1}, LaR/ax;-><init>(LaR/ao;LaT/c;LaT/h;Landroid/content/Context;)V

    .line 496
    new-instance v2, LaR/ay;

    invoke-direct {v2, p0, p2, p1}, LaR/ay;-><init>(LaR/ao;LaT/h;Landroid/content/Context;)V

    .line 506
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 507
    invoke-virtual {p3}, LaT/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 511
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 512
    iget-object v0, p0, LaR/ao;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 515
    :cond_46
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, LaR/ao;->i:Landroid/app/AlertDialog;

    .line 522
    return-void
.end method

.method static synthetic b(LaR/ao;)Lcom/google/googlenav/bh;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, LaR/ao;->g:Lcom/google/googlenav/bh;

    return-object v0
.end method

.method static synthetic b(LaR/ao;Landroid/content/Context;LaT/h;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, LaR/ao;->b(Landroid/content/Context;LaT/h;)V

    return-void
.end method

.method private b(Landroid/content/Context;LaT/h;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 430
    iget-object v0, p0, LaR/ao;->e:LaN/m;

    if-eqz v0, :cond_4b

    .line 431
    const/16 v0, 0x6a

    const-string v1, "el"

    const-string v2, ""

    invoke-static {v0, v1, v2}, LaU/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 434
    iget-object v1, p0, LaR/ao;->e:LaN/m;

    iget-object v2, p0, LaR/ao;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->a()Lau/B;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/dH;

    invoke-direct {v3}, Lcom/google/googlenav/ui/wizard/dH;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dH;->a(I)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v3

    const/16 v4, 0x44c

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dH;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v3

    invoke-virtual {p2}, LaT/h;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/wizard/dH;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/ui/wizard/dH;->b(I)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/ui/wizard/dH;->a(B)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/wizard/dH;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    new-instance v3, LaR/av;

    invoke-direct {v3, p0, p2}, LaR/av;-><init>(LaR/ao;LaT/h;)V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dH;->a(Lcom/google/googlenav/ui/wizard/dA;)Lcom/google/googlenav/ui/wizard/dH;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LaN/m;->a(Lau/B;Lcom/google/googlenav/ui/wizard/dH;)V

    .line 464
    :cond_4b
    return-void
.end method

.method static synthetic c(LaR/ao;)LaN/aP;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, LaR/ao;->d:LaN/aP;

    return-object v0
.end method

.method static synthetic d(LaR/ao;)LaN/m;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, LaR/ao;->e:LaN/m;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 544
    iget v0, p0, LaR/ao;->f:I

    return v0
.end method

.method public a(Landroid/view/View;)LaR/bE;
    .registers 4
    .parameter

    .prologue
    .line 211
    iget-boolean v0, p0, LaR/ao;->h:Z

    if-eqz v0, :cond_7

    .line 212
    invoke-static {p1}, LaN/aR;->a(Landroid/view/View;)V

    .line 214
    :cond_7
    new-instance v1, LaR/aC;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, LaR/aC;-><init>(LaR/ap;)V

    .line 216
    const v0, 0x7f100288

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, LaR/aC;->a:Landroid/view/ViewGroup;

    .line 218
    const v0, 0x7f10028a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, LaR/aC;->b:Landroid/view/ViewGroup;

    .line 220
    const v0, 0x7f10028b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, LaR/aC;->c:Landroid/view/ViewGroup;

    .line 221
    const v0, 0x7f10028e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LaR/aC;->e:Landroid/widget/TextView;

    .line 222
    const v0, 0x7f10028c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LaR/aC;->d:Landroid/widget/ImageView;

    .line 223
    const v0, 0x7f10028f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, LaR/aC;->f:Landroid/view/ViewGroup;

    .line 224
    const v0, 0x7f100293

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, LaR/aC;->g:Landroid/view/View;

    .line 226
    iget-object v0, p0, LaR/ao;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cc()LaT/o;

    move-result-object v0

    invoke-virtual {v0}, LaT/o;->k()LaT/h;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_7e

    invoke-virtual {v0}, LaT/h;->i()Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 228
    const v0, 0x7f100291

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v1, LaR/aC;->f:Landroid/view/ViewGroup;

    .line 229
    const v0, 0x7f100290

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, LaR/aC;->g:Landroid/view/View;

    .line 232
    :cond_7e
    const v0, 0x7f100292

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, LaR/aC;->h:Landroid/view/View;

    .line 233
    return-object v1
.end method

.method public a(Landroid/view/View;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 206
    if-eqz p2, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 207
    return-void

    .line 206
    :cond_7
    const/16 v0, 0x8

    goto :goto_3
.end method

.method public a(Lcom/google/googlenav/ui/g;LaR/bE;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 238
    check-cast p2, LaR/aC;

    .line 243
    iget-object v0, p0, LaR/ao;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cc()LaT/o;

    move-result-object v0

    invoke-virtual {v0}, LaT/o;->k()LaT/h;

    move-result-object v4

    .line 246
    if-eqz v4, :cond_15e

    .line 249
    iget-object v0, p2, LaR/aC;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 250
    iget-object v0, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 251
    iget-object v0, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 256
    invoke-virtual {v4}, LaT/h;->i()Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 257
    iget-object v0, p2, LaR/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100291

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    .line 259
    iget-object v0, p2, LaR/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100290

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p2, LaR/aC;->g:Landroid/view/View;

    :goto_3d
    move v2, v3

    .line 268
    :goto_3e
    invoke-virtual {v4}, LaT/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_af

    .line 269
    new-instance v5, LaR/aD;

    invoke-virtual {v4}, LaT/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/c;

    invoke-direct {v5, v0}, LaR/aD;-><init>(LaT/c;)V

    .line 271
    invoke-virtual {v4}, LaT/h;->i()Z

    move-result v0

    if-nez v0, :cond_93

    .line 272
    const v0, 0x7f040153

    iget-object v1, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    invoke-static {v0, v1, v3}, Lcom/google/googlenav/ui/bq;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v5, LaR/aD;->a:Landroid/widget/TextView;

    .line 274
    iget-object v0, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    iget-object v1, v5, LaR/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 282
    :goto_71
    iget-object v0, p2, LaR/aC;->i:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3e

    .line 262
    :cond_7a
    iget-object v0, p2, LaR/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f10028f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    .line 264
    iget-object v0, p2, LaR/aC;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100293

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p2, LaR/aC;->g:Landroid/view/View;

    goto :goto_3d

    .line 276
    :cond_93
    const v0, 0x7f040154

    iget-object v1, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    invoke-static {v0, v1, v3}, Lcom/google/googlenav/ui/bq;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 278
    const v1, 0x7f100371

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v5, LaR/aD;->a:Landroid/widget/TextView;

    .line 279
    iget-object v1, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_71

    .line 285
    :cond_af
    iget-object v0, p2, LaR/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v6}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 287
    iget-object v0, p2, LaR/aC;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v6}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 288
    iget-object v0, p2, LaR/aC;->b:Landroid/view/ViewGroup;

    new-instance v1, LaR/ap;

    invoke-direct {v1, p0, p1}, LaR/ap;-><init>(LaR/ao;Lcom/google/googlenav/ui/g;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    iget-object v0, p2, LaR/aC;->e:Landroid/widget/TextView;

    invoke-virtual {v4}, LaT/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v0, p2, LaR/aC;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v6}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 299
    iget-object v0, p2, LaR/aC;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v6}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 300
    iget-object v1, p2, LaR/aC;->d:Landroid/widget/ImageView;

    iget-object v0, p0, LaR/ao;->d:LaN/aP;

    invoke-virtual {v0}, LaN/aP;->d()Z

    move-result v0

    if-eqz v0, :cond_148

    const v0, 0x7f020470

    :goto_e3
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 303
    iget-object v0, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    iget-object v1, p0, LaR/ao;->d:LaN/aP;

    invoke-virtual {v1}, LaN/aP;->d()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 304
    iget-object v0, p2, LaR/aC;->g:Landroid/view/View;

    if-eqz v0, :cond_100

    .line 305
    iget-object v0, p2, LaR/aC;->g:Landroid/view/View;

    iget-object v1, p0, LaR/ao;->d:LaN/aP;

    invoke-virtual {v1}, LaN/aP;->d()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 307
    :cond_100
    iget-object v0, p2, LaR/aC;->h:Landroid/view/View;

    invoke-virtual {p0, v0, v6}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 309
    iget-object v0, p2, LaR/aC;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_10b
    :goto_10b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/aD;

    .line 310
    iget-object v2, v0, LaR/aD;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v6}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 311
    invoke-virtual {v4}, LaT/h;->i()Z

    move-result v2

    if-eqz v2, :cond_14c

    .line 312
    iget-object v2, v0, LaR/aD;->a:Landroid/widget/TextView;

    iget-object v3, v0, LaR/aD;->b:LaT/c;

    invoke-virtual {v3}, LaT/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    :goto_12d
    iget-object v2, v0, LaR/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 320
    new-instance v2, LaR/aA;

    invoke-direct {v2, p0, v0, v4, p2}, LaR/aA;-><init>(LaR/ao;LaR/aD;LaT/h;LaR/aC;)V

    .line 323
    iget-object v0, v0, LaR/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_10b

    .line 327
    iget-object v0, p0, LaR/ao;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10b

    .line 300
    :cond_148
    const v0, 0x7f020471

    goto :goto_e3

    .line 314
    :cond_14c
    iget-object v2, v0, LaR/aD;->b:LaT/c;

    invoke-virtual {v2}, LaT/c;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aZ;->q:Lcom/google/googlenav/ui/aZ;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bq;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 316
    iget-object v3, v0, LaR/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_12d

    .line 331
    :cond_15e
    iget-object v0, p2, LaR/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 332
    iget-object v0, p2, LaR/aC;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 333
    iget-object v0, p2, LaR/aC;->f:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, v3}, LaR/ao;->a(Landroid/view/View;Z)V

    .line 335
    :cond_16d
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 549
    const v0, 0x7f0400e3

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 554
    const/4 v0, 0x0

    return v0
.end method
