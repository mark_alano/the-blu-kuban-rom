.class public LaR/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/H;


# static fields
.field static final a:Ljava/lang/CharSequence;

.field static final b:Ljava/lang/CharSequence;

.field private static final j:Lcom/google/googlenav/ui/view/a;

.field private static final k:Lcom/google/googlenav/ui/view/a;


# instance fields
.field final c:I

.field final d:Lcom/google/googlenav/ui/bz;

.field final e:Lcom/google/googlenav/ai;

.field final f:Z

.field final g:Z

.field h:LaR/bp;

.field final i:Z


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/16 v3, 0x258

    const/4 v2, -0x1

    .line 59
    const/16 v0, 0x588

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->aU:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bq;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    sput-object v0, LaR/bm;->a:Ljava/lang/CharSequence;

    .line 61
    const/16 v0, 0x1e1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aZ;->aU:Lcom/google/googlenav/ui/aZ;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bq;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aZ;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    sput-object v0, LaR/bm;->b:Ljava/lang/CharSequence;

    .line 63
    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/4 v1, 0x0

    invoke-direct {v0, v3, v2, v1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sput-object v0, LaR/bm;->j:Lcom/google/googlenav/ui/view/a;

    .line 65
    new-instance v0, Lcom/google/googlenav/ui/view/a;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v3, v2, v1}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sput-object v0, LaR/bm;->k:Lcom/google/googlenav/ui/view/a;

    return-void
.end method

.method public constructor <init>(ILcom/google/googlenav/ui/bz;Lcom/google/googlenav/ai;ZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-virtual {p3}, Lcom/google/googlenav/ai;->br()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    const/4 v5, 0x1

    :goto_7
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LaR/bm;-><init>(ILcom/google/googlenav/ui/bz;Lcom/google/googlenav/ai;ZZZ)V

    .line 82
    return-void

    .line 79
    :cond_11
    const/4 v5, 0x0

    goto :goto_7
.end method

.method constructor <init>(ILcom/google/googlenav/ui/bz;Lcom/google/googlenav/ai;ZZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput p1, p0, LaR/bm;->c:I

    .line 89
    iput-object p2, p0, LaR/bm;->d:Lcom/google/googlenav/ui/bz;

    .line 90
    iput-object p3, p0, LaR/bm;->e:Lcom/google/googlenav/ai;

    .line 95
    iput-boolean p4, p0, LaR/bm;->f:Z

    .line 96
    iput-boolean p5, p0, LaR/bm;->g:Z

    .line 98
    iput-boolean p6, p0, LaR/bm;->i:Z

    .line 99
    return-void
.end method

.method static synthetic a(LaR/bm;)V
    .registers 1
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, LaR/bm;->d()V

    return-void
.end method

.method static synthetic a(LaR/bm;LaR/bp;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, LaR/bm;->a(LaR/bp;)V

    return-void
.end method

.method static synthetic a(LaR/bm;Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, LaR/bm;->a(Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;)V

    return-void
.end method

.method private a(LaR/bp;)V
    .registers 2
    .parameter

    .prologue
    .line 184
    iput-object p1, p0, LaR/bm;->h:LaR/bp;

    .line 185
    return-void
.end method

.method private a(Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, LaR/bm;->d:Lcom/google/googlenav/ui/bz;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/bz;->a(Lcom/google/googlenav/ui/ag;)LT/f;

    move-result-object v0

    check-cast v0, LU/f;

    .line 174
    iget-object v1, p0, LaR/bm;->d:Lcom/google/googlenav/ui/bz;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bz;->a()LT/f;

    move-result-object v1

    if-eq v0, v1, :cond_13

    .line 175
    invoke-static {p1, v0}, LaR/G;->a(Landroid/widget/ImageView;LU/f;)V

    .line 177
    :cond_13
    return-void
.end method

.method private a(Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;Lcom/google/googlenav/ui/g;Lcom/google/googlenav/ui/view/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, LaR/bm;->d:Lcom/google/googlenav/ui/bz;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bz;->b()Lai/s;

    move-result-object v0

    new-instance v1, LaR/bn;

    invoke-direct {v1, p0, p1, p2}, LaR/bn;-><init>(LaR/bm;Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;)V

    invoke-virtual {v0, p2, v1}, Lai/s;->a(Lcom/google/googlenav/ui/bA;Lai/p;)V

    .line 114
    invoke-direct {p0, p1, p2}, LaR/bm;->a(Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;)V

    .line 115
    new-instance v0, LaR/bo;

    invoke-direct {v0, p0, p3, p4}, LaR/bo;-><init>(LaR/bm;Lcom/google/googlenav/ui/g;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-void
.end method

.method private d()V
    .registers 2

    .prologue
    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, LaR/bm;->h:LaR/bp;

    .line 193
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 210
    iget v0, p0, LaR/bm;->c:I

    return v0
.end method

.method public a(Landroid/view/View;)LaR/bE;
    .registers 4
    .parameter

    .prologue
    .line 197
    iget-boolean v0, p0, LaR/bm;->i:Z

    if-eqz v0, :cond_7

    .line 198
    invoke-static {p1}, LaN/aR;->a(Landroid/view/View;)V

    .line 200
    :cond_7
    new-instance v1, LaR/bp;

    invoke-direct {v1}, LaR/bp;-><init>()V

    .line 201
    const v0, 0x7f100306

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LaR/bp;->a:Landroid/widget/TextView;

    .line 202
    const v0, 0x7f100305

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LaR/bp;->b:Landroid/widget/ImageView;

    .line 203
    const v0, 0x7f100307

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LaR/bp;->c:Landroid/widget/TextView;

    .line 204
    const v0, 0x7f100094

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LaR/bp;->d:Landroid/widget/ImageView;

    .line 205
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/g;LaR/bE;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v7, 0x8

    .line 127
    check-cast p2, LaR/bp;

    iput-object p2, p0, LaR/bm;->h:LaR/bp;

    .line 130
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    invoke-virtual {v0, p0}, LaR/bp;->a(LaR/bm;)V

    .line 132
    iget-object v0, p0, LaR/bm;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bW()Lcom/google/googlenav/ui/bA;

    move-result-object v4

    .line 133
    iget-object v0, p0, LaR/bm;->e:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bX()Lcom/google/googlenav/ui/bA;

    move-result-object v5

    .line 135
    if-eqz v4, :cond_59

    move v3, v1

    .line 136
    :goto_1c
    if-eqz v5, :cond_5b

    move v0, v1

    .line 138
    :goto_1f
    iget-boolean v6, p0, LaR/bm;->f:Z

    if-eqz v6, :cond_5d

    if-eqz v3, :cond_5d

    move v3, v1

    .line 139
    :goto_26
    iget-boolean v6, p0, LaR/bm;->g:Z

    if-eqz v6, :cond_5f

    if-eqz v0, :cond_5f

    .line 140
    :goto_2c
    if-nez v3, :cond_30

    if-eqz v1, :cond_7f

    .line 141
    :cond_30
    if-eqz v1, :cond_61

    .line 142
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->a:Landroid/widget/TextView;

    sget-object v1, LaR/bm;->b:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, LaR/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->b:Landroid/widget/ImageView;

    sget-object v1, LaR/bm;->k:Lcom/google/googlenav/ui/view/a;

    invoke-direct {p0, v0, v5, p1, v1}, LaR/bm;->a(Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;Lcom/google/googlenav/ui/g;Lcom/google/googlenav/ui/view/a;)V

    .line 150
    :goto_44
    if-eqz v3, :cond_70

    .line 151
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->c:Landroid/widget/TextView;

    sget-object v1, LaR/bm;->a:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, LaR/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->d:Landroid/widget/ImageView;

    sget-object v1, LaR/bm;->j:Lcom/google/googlenav/ui/view/a;

    invoke-direct {p0, v0, v4, p1, v1}, LaR/bm;->a(Landroid/widget/ImageView;Lcom/google/googlenav/ui/bA;Lcom/google/googlenav/ui/g;Lcom/google/googlenav/ui/view/a;)V

    .line 164
    :goto_58
    return-void

    :cond_59
    move v3, v2

    .line 135
    goto :goto_1c

    :cond_5b
    move v0, v2

    .line 136
    goto :goto_1f

    :cond_5d
    move v3, v2

    .line 138
    goto :goto_26

    :cond_5f
    move v1, v2

    .line 139
    goto :goto_2c

    .line 146
    :cond_61
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_44

    .line 155
    :cond_70
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_58

    .line 159
    :cond_7f
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 161
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v0, p0, LaR/bm;->h:LaR/bp;

    iget-object v0, v0, LaR/bp;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_58
.end method

.method public b()I
    .registers 2

    .prologue
    .line 215
    const v0, 0x7f04012c

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method
