.class public LaR/H;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private d:Z

.field private e:J


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, ""

    iput-object v0, p0, LaR/H;->a:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, LaR/H;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, LaR/H;->b:I

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, LaR/H;->d:Z

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaR/H;->e:J

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaR/H;->a:Ljava/lang/String;

    .line 42
    const/4 v0, 0x2

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, LaR/H;->b:I

    .line 43
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 44
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, LaR/H;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 46
    :cond_21
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    :goto_2b
    iput-boolean v0, p0, LaR/H;->d:Z

    .line 48
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 49
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, LaR/H;->e:J

    .line 51
    :cond_39
    return-void

    .line 46
    :cond_3a
    const/4 v0, 0x0

    goto :goto_2b
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/H;
    .registers 4
    .parameter

    .prologue
    .line 58
    new-instance v0, LaR/H;

    invoke-direct {v0}, LaR/H;-><init>()V

    .line 59
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaR/H;->a(Ljava/lang/String;)V

    .line 60
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbM/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 61
    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 62
    invoke-virtual {v0, v1}, LaR/H;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 63
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, LaR/H;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 83
    iput p1, p0, LaR/H;->b:I

    .line 84
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, LaR/H;->a:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 91
    iput-boolean p1, p0, LaR/H;->d:Z

    .line 92
    return-void
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, LaR/H;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, LaR/H;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 80
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 87
    iget-boolean v0, p0, LaR/H;->d:Z

    return v0
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6

    .prologue
    .line 103
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbM/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 104
    const/4 v1, 0x1

    iget-object v2, p0, LaR/H;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 105
    iget v1, p0, LaR/H;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_18

    .line 106
    const/4 v1, 0x2

    iget v2, p0, LaR/H;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 108
    :cond_18
    iget-object v1, p0, LaR/H;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_22

    .line 109
    const/4 v1, 0x3

    iget-object v2, p0, LaR/H;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 111
    :cond_22
    const/4 v1, 0x4

    iget-boolean v2, p0, LaR/H;->d:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 112
    iget-wide v1, p0, LaR/H;->e:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_36

    .line 113
    const/4 v1, 0x5

    iget-wide v2, p0, LaR/H;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 115
    :cond_36
    return-object v0
.end method
