.class public abstract LaR/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/W;
.implements LaR/u;


# static fields
.field private static final f:Ljava/util/Comparator;

.field private static final g:Ljava/util/Comparator;


# instance fields
.field protected final a:I

.field protected final b:LaR/T;

.field private final c:Ljava/util/List;

.field private d:LaR/I;

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 57
    new-instance v0, LaR/w;

    invoke-direct {v0}, LaR/w;-><init>()V

    sput-object v0, LaR/v;->f:Ljava/util/Comparator;

    .line 76
    new-instance v0, LaR/x;

    invoke-direct {v0}, LaR/x;-><init>()V

    sput-object v0, LaR/v;->g:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(LaR/T;LaR/I;IZLaR/U;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-object p1, p0, LaR/v;->b:LaR/T;

    .line 154
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LaR/v;->c:Ljava/util/List;

    .line 155
    invoke-interface {p1, p0}, LaR/T;->a(LaR/W;)V

    .line 156
    invoke-interface {p1, p3, p5}, LaR/T;->a(ILaR/U;)V

    .line 157
    iput-object p2, p0, LaR/v;->d:LaR/I;

    .line 158
    iput p3, p0, LaR/v;->a:I

    .line 159
    iput-boolean p4, p0, LaR/v;->e:Z

    .line 160
    return-void
.end method

.method public static a(ILaR/T;LaR/I;)LaR/v;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 93
    packed-switch p0, :pswitch_data_4a

    .line 146
    :pswitch_3
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 95
    :pswitch_5
    new-instance v0, LaR/M;

    invoke-direct {v0, p1, p2}, LaR/M;-><init>(LaR/T;LaR/I;)V

    goto :goto_4

    .line 97
    :pswitch_b
    new-instance v0, LaR/k;

    new-instance v1, LaR/y;

    invoke-direct {v1}, LaR/y;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_4

    .line 108
    :pswitch_16
    new-instance v0, LaR/k;

    new-instance v1, LaR/z;

    invoke-direct {v1}, LaR/z;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_4

    .line 121
    :pswitch_21
    new-instance v0, LaR/k;

    new-instance v1, LaR/A;

    invoke-direct {v1}, LaR/A;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_4

    .line 131
    :pswitch_2c
    new-instance v0, LaR/k;

    new-instance v1, LaR/B;

    invoke-direct {v1}, LaR/B;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, LaR/k;-><init>(LaR/T;LaR/I;ILaR/U;)V

    goto :goto_4

    .line 140
    :pswitch_37
    new-instance v0, LaR/E;

    invoke-direct {v0, p1, p2}, LaR/E;-><init>(LaR/T;LaR/I;)V

    goto :goto_4

    .line 142
    :pswitch_3d
    new-instance v0, LaR/i;

    invoke-direct {v0, p1, p2}, LaR/i;-><init>(LaR/T;LaR/I;)V

    goto :goto_4

    .line 144
    :pswitch_43
    new-instance v0, LaR/J;

    invoke-direct {v0, p1, p2}, LaR/J;-><init>(LaR/T;LaR/I;)V

    goto :goto_4

    .line 93
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_b
        :pswitch_16
        :pswitch_21
        :pswitch_3d
        :pswitch_2c
        :pswitch_37
        :pswitch_43
    .end packed-switch
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x2

    .line 216
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hi;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 217
    invoke-virtual {v0, v4, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 218
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 221
    iget-object v2, p0, LaR/v;->b:LaR/T;

    iget v3, p0, LaR/v;->a:I

    invoke-interface {v2, v3, v1}, LaR/T;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 222
    if-eqz v1, :cond_23

    .line 226
    :try_start_19
    invoke-static {v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_1c} :catch_30

    move-result-object v1

    .line 230
    invoke-virtual {p0, v1, p1}, LaR/v;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 231
    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 235
    :cond_23
    iget-boolean v1, p0, LaR/v;->e:Z

    if-eqz v1, :cond_33

    .line 236
    iget-object v1, p0, LaR/v;->b:LaR/T;

    iget v2, p0, LaR/v;->a:I

    invoke-interface {v1, v2, v0}, LaR/T;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    .line 238
    :goto_2f
    return v0

    .line 227
    :catch_30
    move-exception v0

    .line 228
    const/4 v0, 0x0

    goto :goto_2f

    .line 238
    :cond_33
    iget-object v1, p0, LaR/v;->b:LaR/T;

    iget v2, p0, LaR/v;->a:I

    invoke-interface {v1, v2, v0}, LaR/T;->b(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    goto :goto_2f
.end method

.method private f(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, LaR/v;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/C;

    .line 356
    invoke-interface {v0, p1}, LaR/C;->b(Ljava/lang/String;)V

    goto :goto_6

    .line 358
    :cond_16
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LaR/t;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 176
    :cond_7
    :goto_7
    return-object v0

    .line 172
    :cond_8
    iget-object v1, p0, LaR/v;->b:LaR/T;

    iget v2, p0, LaR/v;->a:I

    invoke-interface {v1, v2, p1}, LaR/T;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 173
    if-eqz v1, :cond_7

    .line 174
    invoke-virtual {p0, v1}, LaR/v;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/t;

    move-result-object v0

    goto :goto_7
.end method

.method public a()Ljava/util/List;
    .registers 4

    .prologue
    .line 207
    iget-object v0, p0, LaR/v;->b:LaR/T;

    iget v1, p0, LaR/v;->a:I

    invoke-interface {v0, v1}, LaR/T;->b(I)Ljava/util/List;

    move-result-object v0

    .line 208
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 209
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 210
    invoke-virtual {p0, v0}, LaR/v;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/t;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 212
    :cond_28
    return-object v1
.end method

.method public a(LaR/C;)V
    .registers 3
    .parameter

    .prologue
    .line 322
    iget-object v0, p0, LaR/v;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    return-void
.end method

.method public a(LaR/H;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 275
    invoke-virtual {p1}, LaR/H;->a()Ljava/lang/String;

    move-result-object v0

    .line 276
    invoke-virtual {p0, v0, p1, p2}, LaR/v;->a(Ljava/lang/String;LaR/H;Z)V

    .line 277
    invoke-direct {p0, v0}, LaR/v;->f(Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method protected abstract a(LaR/t;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method a(Ljava/lang/String;LaR/H;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 288
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 309
    :goto_6
    return-void

    .line 296
    :cond_7
    :try_start_7
    invoke-virtual {p2}, LaR/H;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_e} :catch_33

    move-result-object v0

    .line 302
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 303
    const/4 v1, 0x4

    invoke-virtual {p2}, LaR/H;->c()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 304
    if-eqz p3, :cond_2d

    .line 305
    const/4 v1, 0x5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 308
    :cond_2d
    iget-object v1, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v1, p1, v0}, LaR/I;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_6

    .line 297
    :catch_33
    move-exception v0

    goto :goto_6
.end method

.method public a(LaR/t;)Z
    .registers 5
    .parameter

    .prologue
    .line 250
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hi;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 251
    invoke-virtual {p0, p1, v0}, LaR/v;->a(LaR/t;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 252
    const/4 v1, 0x2

    invoke-virtual {p1}, LaR/t;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 253
    invoke-direct {p0, v0}, LaR/v;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public a_(ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 333
    iget v0, p0, LaR/v;->a:I

    if-eq p1, v0, :cond_5

    .line 341
    :cond_4
    :goto_4
    return-void

    .line 336
    :cond_5
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 340
    invoke-direct {p0, p2}, LaR/v;->f(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public b(Ljava/lang/String;)LaR/H;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 188
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 192
    :cond_7
    :goto_7
    return-object v0

    .line 191
    :cond_8
    iget-object v1, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v1, p1}, LaR/I;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_7

    new-instance v0, LaR/H;

    invoke-direct {v0, v1}, LaR/H;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_7
.end method

.method protected abstract b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/t;
.end method

.method public b()Ljava/util/List;
    .registers 2

    .prologue
    .line 378
    invoke-virtual {p0}, LaR/v;->c()Ljava/util/List;

    move-result-object v0

    .line 379
    return-object v0
.end method

.method public b(LaR/C;)V
    .registers 3
    .parameter

    .prologue
    .line 328
    iget-object v0, p0, LaR/v;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 329
    return-void
.end method

.method public c()Ljava/util/List;
    .registers 6

    .prologue
    .line 362
    invoke-virtual {p0}, LaR/v;->a()Ljava/util/List;

    move-result-object v0

    .line 364
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LaR/t;

    .line 365
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 366
    sget-object v2, LaR/v;->g:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 368
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 369
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1c
    if-ge v0, v3, :cond_2a

    aget-object v4, v1, v0

    .line 370
    invoke-virtual {v4}, LaR/t;->h()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 373
    :cond_2a
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 197
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v0, p1}, LaR/I;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public d()V
    .registers 3

    .prologue
    .line 384
    iget-object v0, p0, LaR/v;->b:LaR/T;

    iget v1, p0, LaR/v;->a:I

    invoke-interface {v0, v1}, LaR/T;->c(I)V

    .line 385
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaR/v;->f(Ljava/lang/String;)V

    .line 386
    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 202
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, LaR/v;->d:LaR/I;

    invoke-virtual {v0, p1}, LaR/I;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public e(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 313
    iget-object v0, p0, LaR/v;->b:LaR/T;

    iget v1, p0, LaR/v;->a:I

    invoke-interface {v0, v1, p1}, LaR/T;->b(ILjava/lang/String;)V

    .line 314
    invoke-direct {p0, p1}, LaR/v;->f(Ljava/lang/String;)V

    .line 315
    return-void
.end method
