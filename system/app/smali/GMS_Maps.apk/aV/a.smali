.class public LaV/a;
.super LaV/h;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final N:F

.field private static final O:F

.field private static final P:F

.field private static final Q:F

.field private static final a:F


# instance fields
.field private final A:[F

.field private final B:[F

.field private final C:[F

.field private D:I

.field private E:I

.field private F:I

.field private G:LaV/e;

.field private final H:Lcom/google/googlenav/common/a;

.field private I:J

.field private J:F

.field private final K:Ljava/util/WeakHashMap;

.field private final L:Ljava/util/Map;

.field private M:LaV/d;

.field private R:Landroid/view/WindowManager;

.field private b:Z

.field private c:LaV/j;

.field private d:Landroid/content/Context;

.field private e:Landroid/hardware/SensorManager;

.field private final f:LaV/f;

.field private g:I

.field private h:Z

.field private i:F

.field private j:F

.field private k:Landroid/hardware/Sensor;

.field private l:Landroid/hardware/Sensor;

.field private m:Landroid/hardware/Sensor;

.field private n:Landroid/hardware/Sensor;

.field private final o:[F

.field private final p:[F

.field private final q:[F

.field private final r:[F

.field private final s:[F

.field private final t:[F

.field private final u:[F

.field private final v:[F

.field private final w:[F

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/high16 v2, 0x4000

    const v1, 0x3c8efa35

    .line 71
    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    div-float/2addr v0, v2

    sput v0, LaV/a;->a:F

    .line 412
    const v0, 0x3d0efa35

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->N:F

    .line 416
    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->O:F

    .line 420
    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->P:F

    .line 424
    const v0, 0x3c0efa35

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, LaV/a;->Q:F

    return-void
.end method

.method public constructor <init>(LaV/e;Lcom/google/googlenav/common/a;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/16 v6, 0x9

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x3

    .line 455
    invoke-direct {p0}, LaV/h;-><init>()V

    .line 90
    iput-boolean v4, p0, LaV/a;->b:Z

    .line 97
    sget-object v0, LaV/j;->a:LaV/j;

    iput-object v0, p0, LaV/a;->c:LaV/j;

    .line 127
    new-instance v0, LaV/f;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LaV/f;-><init>(Z)V

    iput-object v0, p0, LaV/a;->f:LaV/f;

    .line 135
    iput v3, p0, LaV/a;->g:I

    .line 144
    iput-boolean v4, p0, LaV/a;->h:Z

    .line 151
    const/high16 v0, -0x4080

    iput v0, p0, LaV/a;->i:F

    .line 156
    const/high16 v0, 0x7fc0

    iput v0, p0, LaV/a;->j:F

    .line 207
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->o:[F

    .line 217
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->p:[F

    .line 226
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->q:[F

    .line 243
    new-array v0, v5, [F

    iput-object v0, p0, LaV/a;->r:[F

    .line 249
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->s:[F

    .line 255
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->t:[F

    .line 263
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->u:[F

    .line 271
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->v:[F

    .line 278
    new-array v0, v5, [F

    iput-object v0, p0, LaV/a;->w:[F

    .line 285
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, LaV/a;->x:J

    .line 304
    new-array v0, v6, [F

    iput-object v0, p0, LaV/a;->A:[F

    .line 305
    new-array v0, v6, [F

    iput-object v0, p0, LaV/a;->B:[F

    .line 311
    new-array v0, v2, [F

    iput-object v0, p0, LaV/a;->C:[F

    .line 319
    iput v3, p0, LaV/a;->D:I

    .line 327
    iput v3, p0, LaV/a;->E:I

    .line 333
    iput v3, p0, LaV/a;->F:I

    .line 359
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    .line 384
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LaV/a;->L:Ljava/util/Map;

    .line 391
    new-instance v0, LaV/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LaV/d;-><init>(LaV/b;)V

    iput-object v0, p0, LaV/a;->M:LaV/d;

    .line 456
    iput-object p1, p0, LaV/a;->G:LaV/e;

    .line 457
    iput-object p2, p0, LaV/a;->H:Lcom/google/googlenav/common/a;

    .line 458
    return-void
.end method

.method public static a([F)F
    .registers 5
    .parameter

    .prologue
    const/high16 v3, 0x3f80

    .line 851
    const/4 v1, 0x0

    .line 852
    const/4 v0, 0x0

    :goto_4
    const/4 v2, 0x3

    if-ge v0, v2, :cond_e

    .line 853
    aget v2, p0, v0

    .line 854
    mul-float/2addr v2, v2

    add-float/2addr v1, v2

    .line 852
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 859
    :cond_e
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 862
    sub-float v0, v3, v0

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method protected static a([F[F)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 956
    const/4 v1, 0x0

    .line 957
    const/4 v0, 0x0

    :goto_2
    array-length v2, p0

    if-ge v0, v2, :cond_e

    .line 958
    aget v2, p0, v0

    aget v3, p1, v0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 957
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 960
    :cond_e
    return v1
.end method

.method private static a(Landroid/hardware/Sensor;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 722
    const-string v0, "sensor of %s \"%s\" v%d by %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    invoke-static {v3}, LaV/a;->b(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getVersion()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(I[I)[Landroid/hardware/Sensor;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 650
    .line 652
    array-length v0, p2

    new-array v4, v0, [Landroid/hardware/Sensor;

    move v0, v1

    move v2, v1

    .line 653
    :goto_7
    array-length v5, p2

    if-ge v0, v5, :cond_31

    .line 654
    aget v5, p2, v0

    .line 655
    invoke-static {v5}, LaV/a;->b(I)Ljava/lang/String;

    .line 658
    invoke-virtual {p0, v5}, LaV/a;->a(I)Landroid/hardware/Sensor;

    move-result-object v5

    .line 659
    aput-object v5, v4, v0

    .line 660
    if-nez v5, :cond_22

    .line 680
    :cond_17
    :goto_17
    if-eqz v2, :cond_1e

    if-nez v1, :cond_1e

    .line 681
    invoke-virtual {p0}, LaV/a;->e()V

    .line 685
    :cond_1e
    if-eqz v1, :cond_2f

    move-object v0, v4

    .line 688
    :goto_21
    return-object v0

    .line 667
    :cond_22
    invoke-static {v5}, LaV/a;->a(Landroid/hardware/Sensor;)Ljava/lang/String;

    .line 668
    invoke-virtual {p0, v5, p1}, LaV/a;->a(Landroid/hardware/Sensor;I)Z

    move-result v5

    .line 669
    if-eqz v5, :cond_17

    .line 653
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_7

    .line 688
    :cond_2f
    const/4 v0, 0x0

    goto :goto_21

    :cond_31
    move v1, v3

    goto :goto_17
.end method

.method private static b(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 707
    sparse-switch p0, :sswitch_data_24

    .line 717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_16
    return-object v0

    .line 709
    :sswitch_17
    const-string v0, "TYPE_ROTATION_VECTOR"

    goto :goto_16

    .line 711
    :sswitch_1a
    const-string v0, "TYPE_MAGNETIC_FIELD"

    goto :goto_16

    .line 713
    :sswitch_1d
    const-string v0, "TYPE_ACCELEROMETER"

    goto :goto_16

    .line 715
    :sswitch_20
    const-string v0, "TYPE_ORIENTATION"

    goto :goto_16

    .line 707
    nop

    :sswitch_data_24
    .sparse-switch
        0x1 -> :sswitch_1d
        0x2 -> :sswitch_1a
        0x3 -> :sswitch_20
        0xb -> :sswitch_17
    .end sparse-switch
.end method

.method public static b([F[F)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x3

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 971
    move v1, v0

    move v2, v3

    .line 972
    :goto_5
    if-ge v1, v6, :cond_10

    .line 973
    aget v4, p0, v1

    aget v5, p0, v1

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    .line 972
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 975
    :cond_10
    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v1

    .line 976
    cmpl-float v2, v1, v3

    if-nez v2, :cond_1f

    .line 977
    :goto_18
    if-ge v0, v6, :cond_29

    .line 978
    aput v3, p1, v0

    .line 977
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 981
    :cond_1f
    :goto_1f
    if-ge v0, v6, :cond_29

    .line 982
    aget v2, p0, v0

    div-float/2addr v2, v1

    aput v2, p1, v0

    .line 981
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 985
    :cond_29
    return-void
.end method

.method private k()Landroid/hardware/SensorManager;
    .registers 3

    .prologue
    .line 763
    iget-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_10

    .line 764
    iget-object v0, p0, LaV/a;->d:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    .line 766
    :cond_10
    iget-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method private l()I
    .registers 3

    .prologue
    .line 1197
    iget v0, p0, LaV/a;->F:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_10

    iget-object v0, p0, LaV/a;->R:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    :goto_f
    return v0

    :cond_10
    iget v0, p0, LaV/a;->F:I

    goto :goto_f
.end method

.method private declared-synchronized m()V
    .registers 9

    .prologue
    .line 1281
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaV/a;->G:LaV/e;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_3d

    if-nez v0, :cond_7

    .line 1303
    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    .line 1286
    :cond_7
    :try_start_7
    iget-object v0, p0, LaV/a;->H:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    .line 1287
    iget-wide v0, p0, LaV/a;->I:J

    sub-long v0, v4, v0

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 1290
    iget-object v0, p0, LaV/a;->G:LaV/e;

    invoke-interface {v0}, LaV/e;->a()Landroid/location/Location;

    move-result-object v3

    .line 1291
    if-eqz v3, :cond_5

    .line 1295
    iput-wide v4, p0, LaV/a;->I:J

    .line 1296
    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    double-to-float v2, v6

    invoke-virtual {v3}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    double-to-float v3, v6

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    .line 1301
    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    iput v0, p0, LaV/a;->J:F
    :try_end_3c
    .catchall {:try_start_7 .. :try_end_3c} :catchall_3d

    goto :goto_5

    .line 1281
    :catchall_3d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized n()V
    .registers 6

    .prologue
    .line 1333
    monitor-enter p0

    :try_start_1
    iget-object v2, p0, LaV/a;->L:Ljava/util/Map;

    monitor-enter v2
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_40

    .line 1334
    :try_start_4
    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_e
    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1335
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaV/i;

    .line 1336
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaV/c;

    .line 1337
    if-eqz v1, :cond_e

    if-eqz v0, :cond_e

    .line 1340
    sget-object v4, LaV/b;->a:[I

    invoke-virtual {v0}, LaV/c;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_52

    goto :goto_e

    .line 1342
    :pswitch_36
    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_e

    .line 1350
    :catchall_3d
    move-exception v0

    monitor-exit v2
    :try_end_3f
    .catchall {:try_start_4 .. :try_end_3f} :catchall_3d

    :try_start_3f
    throw v0
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_40

    .line 1333
    :catchall_40
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1345
    :pswitch_43
    :try_start_43
    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_e

    .line 1349
    :cond_49
    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1350
    monitor-exit v2
    :try_end_4f
    .catchall {:try_start_43 .. :try_end_4f} :catchall_3d

    .line 1351
    monitor-exit p0

    return-void

    .line 1340
    nop

    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_36
        :pswitch_43
    .end packed-switch
.end method


# virtual methods
.method protected a(I)Landroid/hardware/Sensor;
    .registers 3
    .parameter

    .prologue
    .line 703
    invoke-direct {p0}, LaV/a;->k()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    return-object v0
.end method

.method public a(LaV/i;)V
    .registers 4
    .parameter

    .prologue
    .line 1317
    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    sget-object v1, LaV/c;->a:LaV/c;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1318
    return-void
.end method

.method public declared-synchronized a(LaV/j;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x3

    .line 495
    monitor-enter p0

    :try_start_3
    invoke-static {}, Lcom/google/googlenav/android/a;->e()Z
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_44

    move-result v2

    if-eqz v2, :cond_b

    .line 609
    :cond_9
    :goto_9
    monitor-exit p0

    return-void

    .line 500
    :cond_b
    :try_start_b
    iget-boolean v2, p0, LaV/a;->b:Z

    if-eqz v2, :cond_9

    .line 505
    iget-object v2, p0, LaV/a;->c:LaV/j;

    if-eq v2, p1, :cond_9

    .line 508
    iput-object p1, p0, LaV/a;->c:LaV/j;

    .line 511
    invoke-static {}, Lcom/google/googlenav/android/a;->a()Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 516
    iget v1, p1, LaV/j;->d:I

    if-nez v1, :cond_47

    .line 540
    :cond_1f
    :goto_1f
    invoke-virtual {p0}, LaV/a;->e()V

    .line 556
    sget-boolean v1, Lcom/google/googlenav/android/E;->d:Z

    if-nez v1, :cond_67

    .line 559
    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v1

    if-eqz v1, :cond_50

    sget-boolean v1, Lcom/google/googlenav/android/E;->h:Z

    if-nez v1, :cond_50

    .line 561
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0xb

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, LaV/a;->a(I[I)[Landroid/hardware/Sensor;

    move-result-object v1

    .line 562
    if-eqz v1, :cond_50

    .line 563
    const/4 v0, 0x0

    aget-object v0, v1, v0

    iput-object v0, p0, LaV/a;->n:Landroid/hardware/Sensor;
    :try_end_43
    .catchall {:try_start_b .. :try_end_43} :catchall_44

    goto :goto_9

    .line 495
    :catchall_44
    move-exception v0

    monitor-exit p0

    throw v0

    .line 516
    :cond_47
    :try_start_47
    iget v0, p1, LaV/j;->d:I

    goto :goto_1f

    .line 536
    :cond_4a
    sget-object v2, LaV/j;->c:LaV/j;

    if-ne p1, v2, :cond_1f

    move v0, v1

    goto :goto_1f

    .line 578
    :cond_50
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_7a

    invoke-direct {p0, v0, v1}, LaV/a;->a(I[I)[Landroid/hardware/Sensor;

    move-result-object v1

    .line 580
    if-eqz v1, :cond_67

    .line 581
    const/4 v0, 0x0

    aget-object v0, v1, v0

    iput-object v0, p0, LaV/a;->k:Landroid/hardware/Sensor;

    .line 582
    const/4 v0, 0x1

    aget-object v0, v1, v0

    iput-object v0, p0, LaV/a;->l:Landroid/hardware/Sensor;

    goto :goto_9

    .line 588
    :cond_67
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x3

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, LaV/a;->a(I[I)[Landroid/hardware/Sensor;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_9

    .line 590
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, LaV/a;->m:Landroid/hardware/Sensor;
    :try_end_79
    .catchall {:try_start_47 .. :try_end_79} :catchall_44

    goto :goto_9

    .line 578
    :array_7a
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public a(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 1171
    iget-object v0, p0, LaV/a;->d:Landroid/content/Context;

    if-eq v0, p1, :cond_13

    .line 1172
    iput-object p1, p0, LaV/a;->d:Landroid/content/Context;

    .line 1173
    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->e:Landroid/hardware/SensorManager;

    .line 1174
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, LaV/a;->R:Landroid/view/WindowManager;

    .line 1190
    :cond_13
    return-void
.end method

.method public declared-synchronized a()Z
    .registers 3

    .prologue
    .line 462
    monitor-enter p0

    :try_start_1
    iget v0, p0, LaV/a;->g:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_b

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_7
    monitor-exit p0

    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_7

    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JZZ)Z
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 887
    monitor-enter p0

    if-eqz p3, :cond_7

    .line 947
    :cond_5
    :goto_5
    monitor-exit p0

    return v0

    .line 891
    :cond_7
    if-nez p4, :cond_1a

    .line 894
    :try_start_9
    iget-wide v2, p0, LaV/a;->y:J

    iget-wide v4, p0, LaV/a;->z:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 896
    const-wide/16 v4, 0x1388

    cmp-long v2, v2, v4

    if-lez v2, :cond_1a

    move v0, v1

    .line 897
    goto :goto_5

    .line 903
    :cond_1a
    iget-object v2, p0, LaV/a;->c:LaV/j;

    sget-object v3, LaV/j;->c:LaV/j;

    if-ne v2, v3, :cond_47

    const-wide/high16 v2, 0x4049

    .line 905
    :goto_22
    iget-wide v4, p0, LaV/a;->x:J

    const-wide/high16 v6, -0x8000

    cmp-long v4, v4, v6

    if-eqz v4, :cond_33

    iget-wide v4, p0, LaV/a;->x:J

    sub-long v4, p1, v4

    long-to-double v4, v4

    cmpl-double v2, v4, v2

    if-lez v2, :cond_4a

    .line 907
    :cond_33
    if-nez p4, :cond_5

    .line 909
    iget-object v1, p0, LaV/a;->o:[F

    iget-object v2, p0, LaV/a;->s:[F

    invoke-static {v1, v2}, LaV/a;->b([F[F)V

    .line 910
    iget-object v1, p0, LaV/a;->p:[F

    iget-object v2, p0, LaV/a;->t:[F

    invoke-static {v1, v2}, LaV/a;->b([F[F)V
    :try_end_43
    .catchall {:try_start_9 .. :try_end_43} :catchall_44

    goto :goto_5

    .line 887
    :catchall_44
    move-exception v0

    monitor-exit p0

    throw v0

    .line 903
    :cond_47
    const-wide/high16 v2, 0x4059

    goto :goto_22

    .line 915
    :cond_4a
    if-eqz p4, :cond_6c

    .line 920
    :try_start_4c
    iget-object v2, p0, LaV/a;->c:LaV/j;

    sget-object v3, LaV/j;->c:LaV/j;

    if-ne v2, v3, :cond_68

    sget v2, LaV/a;->Q:F

    float-to-double v2, v2

    .line 923
    :goto_55
    iget-object v4, p0, LaV/a;->r:[F

    iget-object v5, p0, LaV/a;->w:[F

    invoke-static {v4, v5}, LaV/a;->a([F[F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v4, v2

    if-ltz v2, :cond_5

    :cond_66
    move v0, v1

    .line 947
    goto :goto_5

    .line 920
    :cond_68
    sget v2, LaV/a;->P:F

    float-to-double v2, v2

    goto :goto_55

    .line 934
    :cond_6c
    iget-object v2, p0, LaV/a;->c:LaV/j;

    sget-object v3, LaV/j;->c:LaV/j;

    if-ne v2, v3, :cond_9f

    sget v2, LaV/a;->O:F

    float-to-double v2, v2

    .line 936
    :goto_75
    iget-object v4, p0, LaV/a;->o:[F

    iget-object v5, p0, LaV/a;->s:[F

    invoke-static {v4, v5}, LaV/a;->b([F[F)V

    .line 937
    iget-object v4, p0, LaV/a;->p:[F

    iget-object v5, p0, LaV/a;->t:[F

    invoke-static {v4, v5}, LaV/a;->b([F[F)V

    .line 938
    iget-object v4, p0, LaV/a;->s:[F

    iget-object v5, p0, LaV/a;->u:[F

    invoke-static {v4, v5}, LaV/a;->a([F[F)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v4, v4, v2

    if-ltz v4, :cond_5

    iget-object v4, p0, LaV/a;->t:[F

    iget-object v5, p0, LaV/a;->v:[F

    invoke-static {v4, v5}, LaV/a;->a([F[F)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v4, v2

    if-gez v2, :cond_66

    goto/16 :goto_5

    .line 934
    :cond_9f
    sget v2, LaV/a;->N:F
    :try_end_a1
    .catchall {:try_start_4c .. :try_end_a1} :catchall_44

    float-to-double v2, v2

    goto :goto_75
.end method

.method protected a(Landroid/hardware/Sensor;I)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 698
    invoke-direct {p0}, LaV/a;->k()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized b()I
    .registers 2

    .prologue
    .line 467
    monitor-enter p0

    :try_start_1
    iget v0, p0, LaV/a;->g:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(JZZ)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v7, 0x42b4

    const v6, 0x42652ee0

    const/4 v0, 0x1

    .line 1000
    monitor-enter p0

    if-eqz p3, :cond_66

    .line 1003
    :try_start_9
    iget-object v0, p0, LaV/a;->q:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1007
    invoke-direct {p0}, LaV/a;->l()I

    move-result v1

    packed-switch v1, :pswitch_data_fe

    .line 1113
    :goto_15
    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(F)F

    move-result v0

    .line 1114
    invoke-direct {p0}, LaV/a;->m()V

    .line 1115
    iget v1, p0, LaV/a;->J:F

    add-float/2addr v0, v1

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(F)F

    move-result v0

    .line 1116
    iget-object v1, p0, LaV/a;->f:LaV/f;

    invoke-virtual {v1, p1, p2, v0}, LaV/f;->a(JF)F

    move-result v0

    iput v0, p0, LaV/a;->i:F

    .line 1119
    invoke-direct {p0}, LaV/a;->n()V

    .line 1120
    iget-object v0, p0, LaV/a;->M:LaV/d;

    iget v3, p0, LaV/a;->i:F

    iget v4, p0, LaV/a;->j:F

    iget-object v5, p0, LaV/a;->c:LaV/j;

    move-wide v1, p1

    invoke-static/range {v0 .. v5}, LaV/d;->a(LaV/d;JFFLaV/j;)Z

    move-result v0

    if-eqz v0, :cond_e0

    .line 1121
    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_47
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaV/i;

    .line 1122
    iget v2, p0, LaV/a;->i:F

    iget v3, p0, LaV/a;->j:F

    invoke-interface {v0, v2, v3}, LaV/i;->a(FF)V
    :try_end_5a
    .catchall {:try_start_9 .. :try_end_5a} :catchall_5b

    goto :goto_47

    .line 1000
    :catchall_5b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1009
    :pswitch_5e
    add-float/2addr v0, v7

    .line 1010
    goto :goto_15

    .line 1012
    :pswitch_60
    sub-float/2addr v0, v7

    .line 1013
    goto :goto_15

    .line 1015
    :pswitch_62
    const/high16 v1, 0x4334

    add-float/2addr v0, v1

    .line 1016
    goto :goto_15

    .line 1021
    :cond_66
    :try_start_66
    iput-wide p1, p0, LaV/a;->x:J

    .line 1023
    if-eqz p4, :cond_bf

    .line 1026
    iget-object v1, p0, LaV/a;->r:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->w:[F

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1030
    iget-object v1, p0, LaV/a;->A:[F

    iget-object v2, p0, LaV/a;->r:[F

    invoke-static {v1, v2}, Landroid/hardware/SensorManager;->getRotationMatrixFromVector([F[F)V

    .line 1052
    :cond_7b
    invoke-direct {p0}, LaV/a;->l()I

    move-result v1

    packed-switch v1, :pswitch_data_108

    .line 1067
    const/4 v1, 0x2

    move v8, v1

    move v1, v0

    move v0, v8

    .line 1069
    :goto_86
    iget-object v2, p0, LaV/a;->A:[F

    iget-object v3, p0, LaV/a;->B:[F

    invoke-static {v2, v1, v0, v3}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    move-result v0

    .line 1071
    if-nez v0, :cond_90

    .line 1086
    :cond_90
    iget-object v0, p0, LaV/a;->B:[F

    const/4 v1, 0x7

    aget v0, v0, v1

    sget v1, LaV/a;->a:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_ee

    .line 1087
    iget-object v0, p0, LaV/a;->B:[F

    const/4 v1, 0x1

    const/4 v2, 0x3

    iget-object v3, p0, LaV/a;->A:[F

    invoke-static {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    move-result v0

    .line 1089
    if-nez v0, :cond_a7

    .line 1096
    :cond_a7
    iget-object v0, p0, LaV/a;->A:[F

    iget-object v1, p0, LaV/a;->C:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    .line 1098
    iget-object v0, p0, LaV/a;->C:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    mul-float/2addr v0, v6

    sub-float/2addr v0, v7

    iput v0, p0, LaV/a;->j:F

    .line 1109
    :goto_b7
    iget-object v0, p0, LaV/a;->C:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    mul-float/2addr v0, v6

    goto/16 :goto_15

    .line 1034
    :cond_bf
    iget-object v1, p0, LaV/a;->s:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->u:[F

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1035
    iget-object v1, p0, LaV/a;->t:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->v:[F

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1039
    iget-object v1, p0, LaV/a;->A:[F

    const/4 v2, 0x0

    iget-object v3, p0, LaV/a;->p:[F

    iget-object v4, p0, LaV/a;->o:[F

    invoke-static {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z
    :try_end_dd
    .catchall {:try_start_66 .. :try_end_dd} :catchall_5b

    move-result v1

    .line 1041
    if-nez v1, :cond_7b

    .line 1125
    :cond_e0
    monitor-exit p0

    return-void

    .line 1054
    :pswitch_e2
    const/4 v1, 0x2

    .line 1055
    const/16 v0, 0x81

    .line 1056
    goto :goto_86

    .line 1058
    :pswitch_e6
    const/16 v1, 0x82

    .line 1060
    goto :goto_86

    .line 1062
    :pswitch_e9
    const/16 v1, 0x81

    .line 1063
    const/16 v0, 0x82

    .line 1064
    goto :goto_86

    .line 1102
    :cond_ee
    :try_start_ee
    iget-object v0, p0, LaV/a;->B:[F

    iget-object v1, p0, LaV/a;->C:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    .line 1104
    iget-object v0, p0, LaV/a;->C:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    mul-float/2addr v0, v6

    iput v0, p0, LaV/a;->j:F
    :try_end_fd
    .catchall {:try_start_ee .. :try_end_fd} :catchall_5b

    goto :goto_b7

    .line 1007
    :pswitch_data_fe
    .packed-switch 0x1
        :pswitch_5e
        :pswitch_62
        :pswitch_60
    .end packed-switch

    .line 1052
    :pswitch_data_108
    .packed-switch 0x1
        :pswitch_e2
        :pswitch_e9
        :pswitch_e6
    .end packed-switch
.end method

.method public b(LaV/i;)V
    .registers 4
    .parameter

    .prologue
    .line 1322
    iget-object v0, p0, LaV/a;->L:Ljava/util/Map;

    sget-object v1, LaV/c;->b:LaV/c;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1323
    return-void
.end method

.method public declared-synchronized c()Z
    .registers 3

    .prologue
    .line 472
    monitor-enter p0

    :try_start_1
    iget v0, p0, LaV/a;->i:F
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_d

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_b

    const/4 v0, 0x1

    :goto_9
    monitor-exit p0

    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_9

    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()F
    .registers 2

    .prologue
    .line 477
    monitor-enter p0

    :try_start_1
    iget v0, p0, LaV/a;->i:F
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected e()V
    .registers 2

    .prologue
    .line 693
    invoke-direct {p0}, LaV/a;->k()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 694
    return-void
.end method

.method protected declared-synchronized f()V
    .registers 3

    .prologue
    .line 734
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LaV/a;->b:Z

    .line 738
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaV/a;->y:J

    .line 739
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaV/a;->z:J

    .line 742
    sget-object v0, LaV/j;->b:LaV/j;

    invoke-virtual {p0, v0}, LaV/a;->a(LaV/j;)V
    :try_end_11
    .catchall {:try_start_2 .. :try_end_11} :catchall_13

    .line 743
    monitor-exit p0

    return-void

    .line 734
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized g()V
    .registers 2

    .prologue
    .line 747
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, LaV/a;->e()V

    .line 749
    const/4 v0, 0x0

    iput-boolean v0, p0, LaV/a;->b:Z

    .line 750
    sget-object v0, LaV/j;->a:LaV/j;

    iput-object v0, p0, LaV/a;->c:LaV/j;

    .line 751
    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->k:Landroid/hardware/Sensor;

    .line 752
    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->l:Landroid/hardware/Sensor;

    .line 753
    const/4 v0, 0x0

    iput-object v0, p0, LaV/a;->m:Landroid/hardware/Sensor;
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    .line 754
    monitor-exit p0

    return-void

    .line 747
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1136
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaV/a;->l:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_35

    .line 1137
    iput p2, p0, LaV/a;->E:I

    .line 1145
    :cond_7
    :goto_7
    iget-object v0, p0, LaV/a;->m:Landroid/hardware/Sensor;

    if-eq p1, v0, :cond_f

    iget-object v0, p0, LaV/a;->n:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_3c

    .line 1159
    :cond_f
    :goto_f
    iget v0, p0, LaV/a;->g:I

    if-eq p2, v0, :cond_45

    .line 1160
    iput p2, p0, LaV/a;->g:I

    .line 1161
    invoke-direct {p0}, LaV/a;->n()V

    .line 1162
    iget-object v0, p0, LaV/a;->K:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_22
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaV/i;

    .line 1163
    invoke-interface {v0, p2}, LaV/i;->a(I)V
    :try_end_31
    .catchall {:try_start_1 .. :try_end_31} :catchall_32

    goto :goto_22

    .line 1136
    :catchall_32
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1138
    :cond_35
    :try_start_35
    iget-object v0, p0, LaV/a;->k:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_7

    .line 1139
    iput p2, p0, LaV/a;->D:I

    goto :goto_7

    .line 1145
    :cond_3c
    iget v0, p0, LaV/a;->E:I

    iget v1, p0, LaV/a;->D:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_43
    .catchall {:try_start_35 .. :try_end_43} :catchall_32

    move-result p2

    goto :goto_f

    .line 1166
    :cond_45
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 12
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v9, 0x3

    const/4 v1, 0x0

    .line 782
    monitor-enter p0

    :try_start_4
    iget-object v2, p0, LaV/a;->H:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    .line 783
    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v5, p0, LaV/a;->m:Landroid/hardware/Sensor;

    if-ne v2, v5, :cond_34

    move v2, v0

    .line 784
    :goto_11
    iget-object v5, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, LaV/a;->n:Landroid/hardware/Sensor;

    if-ne v5, v6, :cond_36

    .line 785
    :goto_17
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v5, p0, LaV/a;->k:Landroid/hardware/Sensor;

    if-ne v1, v5, :cond_38

    .line 786
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->o:[F

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 787
    iput-wide v3, p0, LaV/a;->y:J

    .line 815
    :cond_29
    :goto_29
    invoke-virtual {p0, v3, v4, v2, v0}, LaV/a;->a(JZZ)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 816
    invoke-virtual {p0, v3, v4, v2, v0}, LaV/a;->b(JZZ)V
    :try_end_32
    .catchall {:try_start_4 .. :try_end_32} :catchall_4b

    .line 841
    :cond_32
    monitor-exit p0

    return-void

    :cond_34
    move v2, v1

    .line 783
    goto :goto_11

    :cond_36
    move v0, v1

    .line 784
    goto :goto_17

    .line 788
    :cond_38
    :try_start_38
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v5, p0, LaV/a;->l:Landroid/hardware/Sensor;

    if-ne v1, v5, :cond_4e

    .line 789
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->p:[F

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 790
    iput-wide v3, p0, LaV/a;->z:J
    :try_end_4a
    .catchall {:try_start_38 .. :try_end_4a} :catchall_4b

    goto :goto_29

    .line 782
    :catchall_4b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 791
    :cond_4e
    if-eqz v2, :cond_5b

    .line 798
    :try_start_50
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->q:[F

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_29

    .line 799
    :cond_5b
    if-eqz v0, :cond_29

    .line 807
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    iget-object v6, p0, LaV/a;->r:[F

    const/4 v7, 0x0

    iget-object v8, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v8, v8

    invoke-static {v1, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 808
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v1, v1

    if-ne v1, v9, :cond_29

    .line 809
    iget-object v1, p0, LaV/a;->r:[F

    const/4 v5, 0x3

    iget-object v6, p0, LaV/a;->r:[F

    invoke-static {v6}, LaV/a;->a([F)F

    move-result v6

    aput v6, v1, v5
    :try_end_79
    .catchall {:try_start_50 .. :try_end_79} :catchall_4b

    goto :goto_29
.end method
