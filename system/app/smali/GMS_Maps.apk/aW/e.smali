.class public Law/e;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 47
    invoke-direct {p0}, Law/a;-><init>()V

    .line 48
    iput-object p1, p0, Law/e;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 49
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_15

    .line 50
    invoke-direct {p0}, Law/e;->k()Ljava/lang/String;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_15

    .line 52
    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 55
    :cond_15
    return-void
.end method

.method private k()Ljava/lang/String;
    .registers 3

    .prologue
    .line 105
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "Cohort"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Law/e;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 82
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 86
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ab;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 88
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 89
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    iget-object v2, p0, Law/e;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 92
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v2

    const-string v3, "Cohort"

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_24
    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 98
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v0}, Law/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 101
    :cond_35
    return v4
.end method

.method public b()I
    .registers 2

    .prologue
    .line 59
    const/16 v0, 0x3e

    return v0
.end method

.method public b_()Z
    .registers 2

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public s_()Z
    .registers 2

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public t_()Z
    .registers 2

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method
