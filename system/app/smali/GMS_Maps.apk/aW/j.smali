.class public Law/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:I

.field private q:Law/h;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 388
    const-class v0, Law/h;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Law/j;->a:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393
    const/4 v0, 0x0

    iput-boolean v0, p0, Law/j;->f:Z

    .line 406
    const/4 v0, -0x1

    iput v0, p0, Law/j;->p:I

    return-void
.end method


# virtual methods
.method public a()Law/h;
    .registers 6

    .prologue
    .line 510
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 511
    :cond_e
    iget-object v0, p0, Law/j;->b:Ljava/lang/String;

    iget-object v1, p0, Law/j;->c:Ljava/lang/String;

    iget-object v2, p0, Law/j;->d:Ljava/lang/String;

    iget-object v3, p0, Law/j;->e:Ljava/lang/String;

    iget-boolean v4, p0, Law/j;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Law/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Law/h;

    move-result-object v0

    iput-object v0, p0, Law/j;->q:Law/h;

    .line 513
    iget-object v0, p0, Law/j;->q:Law/h;

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0, v1}, Law/h;->a(Law/h;Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Law/j;->q:Law/h;

    const-string v1, "SYSTEM"

    invoke-virtual {v0, v1}, Law/h;->c(Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Law/h;->d(Ljava/lang/String;)V

    .line 516
    iget-object v0, p0, Law/j;->q:Law/h;

    iget v1, p0, Law/j;->g:I

    invoke-static {v0, v1}, Law/h;->a(Law/h;I)V

    .line 518
    iget-object v0, p0, Law/j;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_49

    .line 519
    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->a(Law/h;Z)V

    .line 521
    :cond_49
    iget-object v0, p0, Law/j;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_58

    .line 522
    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->b(Law/h;Z)V

    .line 524
    :cond_58
    iget-object v0, p0, Law/j;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_67

    .line 525
    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->c(Law/h;Z)V

    .line 527
    :cond_67
    iget-object v0, p0, Law/j;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_76

    .line 528
    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v1, p0, Law/j;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/h;->d(Law/h;Z)V

    .line 532
    :cond_76
    iget-object v0, p0, Law/j;->q:Law/h;

    iget-object v0, v0, Law/h;->f:Law/o;

    .line 533
    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_87

    .line 534
    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/o;->a(Law/o;Z)V

    .line 536
    :cond_87
    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    if-eqz v1, :cond_90

    .line 537
    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Law/o;->a(Law/o;Ljava/lang/String;)V

    .line 539
    :cond_90
    iget v1, p0, Law/j;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_9a

    .line 540
    iget v1, p0, Law/j;->p:I

    invoke-virtual {v0, v1}, Law/o;->c(I)V

    .line 542
    :cond_9a
    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_a7

    .line 543
    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Law/o;->a(Z)V

    .line 546
    :cond_a7
    iget-object v0, p0, Law/j;->q:Law/h;

    return-object v0
.end method

.method public a(I)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 451
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 452
    :cond_e
    iput p1, p0, Law/j;->g:I

    .line 453
    return-object p0
.end method

.method public a(Ljava/lang/String;)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 415
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 416
    :cond_e
    iput-object p1, p0, Law/j;->b:Ljava/lang/String;

    .line 417
    return-object p0
.end method

.method public a(Z)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 439
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 440
    :cond_e
    iput-boolean p1, p0, Law/j;->f:Z

    .line 441
    return-object p0
.end method

.method public b(I)Law/j;
    .registers 2
    .parameter

    .prologue
    .line 495
    iput p1, p0, Law/j;->p:I

    .line 496
    return-object p0
.end method

.method public b(Ljava/lang/String;)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 421
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 422
    :cond_e
    iput-object p1, p0, Law/j;->c:Ljava/lang/String;

    .line 423
    return-object p0
.end method

.method public b(Z)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 457
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 458
    :cond_e
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->j:Ljava/lang/Boolean;

    .line 459
    return-object p0
.end method

.method public b()Law/p;
    .registers 4

    .prologue
    .line 555
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 557
    :cond_e
    iget-object v0, p0, Law/j;->q:Law/h;

    invoke-static {v0}, Law/h;->a(Law/h;)Law/o;

    move-result-object v0

    .line 558
    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    .line 559
    iget-object v1, p0, Law/j;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Law/o;->a(Law/o;Z)V

    .line 561
    :cond_21
    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    if-eqz v1, :cond_2a

    .line 562
    iget-object v1, p0, Law/j;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Law/o;->a(Law/o;Ljava/lang/String;)V

    .line 564
    :cond_2a
    iget v1, p0, Law/j;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_42

    .line 565
    iget v1, p0, Law/j;->p:I

    invoke-virtual {v0, v1}, Law/o;->c(I)V

    .line 569
    :goto_34
    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_41

    .line 570
    iget-object v1, p0, Law/j;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Law/o;->a(Z)V

    .line 573
    :cond_41
    return-object v0

    .line 567
    :cond_42
    invoke-static {v0}, Law/o;->a(Law/o;)V

    goto :goto_34
.end method

.method public c(Ljava/lang/String;)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 427
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 428
    :cond_e
    iput-object p1, p0, Law/j;->d:Ljava/lang/String;

    .line 429
    return-object p0
.end method

.method public c(Z)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 463
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 464
    :cond_e
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->k:Ljava/lang/Boolean;

    .line 465
    return-object p0
.end method

.method public d(Ljava/lang/String;)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 433
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 434
    :cond_e
    iput-object p1, p0, Law/j;->e:Ljava/lang/String;

    .line 435
    return-object p0
.end method

.method public d(Z)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 469
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 470
    :cond_e
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->m:Ljava/lang/Boolean;

    .line 471
    return-object p0
.end method

.method public e(Ljava/lang/String;)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 445
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 446
    :cond_e
    iput-object p1, p0, Law/j;->h:Ljava/lang/String;

    .line 447
    return-object p0
.end method

.method public e(Z)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 475
    sget-boolean v0, Law/j;->a:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Law/j;->q:Law/h;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 476
    :cond_e
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->l:Ljava/lang/Boolean;

    .line 477
    return-object p0
.end method

.method public f(Ljava/lang/String;)Law/j;
    .registers 2
    .parameter

    .prologue
    .line 490
    iput-object p1, p0, Law/j;->i:Ljava/lang/String;

    .line 491
    return-object p0
.end method

.method public f(Z)Law/j;
    .registers 3
    .parameter

    .prologue
    .line 485
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Law/j;->n:Ljava/lang/Boolean;

    .line 486
    return-object p0
.end method
