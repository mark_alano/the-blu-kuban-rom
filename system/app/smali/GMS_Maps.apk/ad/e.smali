.class public abstract LaD/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:LaD/n;

.field private b:Z


# direct methods
.method public constructor <init>(LaD/n;)V
    .registers 2
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, LaD/e;->a:LaD/n;

    .line 42
    return-void
.end method

.method protected static a(FF)F
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 154
    cmpl-float v0, p1, p0

    if-ltz v0, :cond_15

    .line 164
    sub-float v0, p1, p0

    .line 165
    const-wide v1, 0x401921fb54442d18L

    float-to-double v3, p0

    add-double/2addr v1, v3

    float-to-double v3, p1

    sub-double/2addr v1, v3

    double-to-float v1, v1

    .line 166
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 170
    :goto_14
    return v0

    :cond_15
    invoke-static {p1, p0}, LaD/e;->a(FF)F

    move-result v0

    neg-float v0, v0

    goto :goto_14
.end method


# virtual methods
.method protected abstract a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)LaD/f;
.end method

.method public a(JLjava/util/LinkedList;ZLjava/util/List;Ljava/lang/StringBuilder;)LaD/f;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-virtual {p0}, LaD/e;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 89
    sget-object v0, LaD/f;->a:LaD/f;

    .line 112
    :goto_e
    return-object v0

    .line 93
    :cond_f
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/e;

    .line 94
    invoke-virtual {v0}, LaD/e;->b()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 100
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_e

    .line 105
    :cond_28
    invoke-virtual {p0}, LaD/e;->d()Z

    move-result v0

    if-eq p4, v0, :cond_31

    .line 109
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_e

    :cond_31
    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    .line 112
    invoke-virtual/range {v0 .. v5}, LaD/e;->a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)LaD/f;

    move-result-object v0

    goto :goto_e
.end method

.method protected a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 175
    const/16 v0, 0x63

    invoke-static {v0, p1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 176
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 45
    iget-boolean v0, p0, LaD/e;->b:Z

    return v0
.end method

.method public a(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 120
    iget-boolean v0, p0, LaD/e;->b:Z

    if-eqz v0, :cond_25

    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gesture already active: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_25
    invoke-virtual {p0, p1}, LaD/e;->b(LaD/k;)Z

    move-result v0

    iput-boolean v0, p0, LaD/e;->b:Z

    .line 124
    iget-boolean v0, p0, LaD/e;->b:Z

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract b(LaD/k;)Z
.end method

.method public c(LaD/k;)V
    .registers 5
    .parameter

    .prologue
    .line 132
    iget-boolean v0, p0, LaD/e;->b:Z

    if-nez v0, :cond_25

    .line 133
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gesture already inactive: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_25
    const/4 v0, 0x0

    iput-boolean v0, p0, LaD/e;->b:Z

    .line 136
    invoke-virtual {p0, p1}, LaD/e;->d(LaD/k;)V

    .line 137
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract d(LaD/k;)V
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public e(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 144
    iget-boolean v0, p0, LaD/e;->b:Z

    if-nez v0, :cond_25

    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gesture is not active: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_25
    invoke-virtual {p0, p1}, LaD/e;->f(LaD/k;)Z

    move-result v0

    return v0
.end method

.method protected abstract f(LaD/k;)Z
.end method
