.class public LaD/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaD/n;


# instance fields
.field protected a:LaD/m;

.field private b:LaD/k;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, LaD/g;->a:LaD/m;

    .line 56
    return-void
.end method

.method public a(Landroid/content/Context;LaD/m;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p2, p0, LaD/g;->a:LaD/m;

    .line 41
    new-instance v0, LaD/k;

    invoke-direct {v0, p1, p0}, LaD/k;-><init>(Landroid/content/Context;LaD/n;)V

    iput-object v0, p0, LaD/g;->b:LaD/k;

    .line 42
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 205
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 206
    iget-object v0, p0, LaD/g;->b:LaD/k;

    invoke-virtual {v0, p1}, LaD/k;->a(Z)V

    .line 207
    return-void
.end method

.method public a(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/d;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/d;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/u;)Z

    move-result v0

    return v0
.end method

.method public a(LaD/k;Z)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 64
    if-eqz p2, :cond_4

    .line 65
    const/4 v0, 0x1

    .line 68
    :goto_3
    return v0

    :cond_4
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/c;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/q;)Z

    move-result v0

    goto :goto_3
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, LaD/g;->b:LaD/k;

    invoke-virtual {v0, p1}, LaD/k;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public b(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 103
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/d;

    invoke-direct {v1, v2, p1}, LaD/d;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/u;)Z

    move-result v0

    .line 105
    if-eqz v0, :cond_10

    .line 107
    iput-boolean v2, p0, LaD/g;->c:Z

    .line 109
    :cond_10
    return v0
.end method

.method public b(LaD/k;Z)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 76
    if-eqz p2, :cond_4

    .line 80
    :goto_3
    return v0

    :cond_4
    iget-object v1, p0, LaD/g;->a:LaD/m;

    new-instance v2, LaD/c;

    invoke-direct {v2, v0, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v1, v2}, LaD/m;->a(LaD/q;)Z

    move-result v0

    goto :goto_3
.end method

.method public c(LaD/k;)V
    .registers 5
    .parameter

    .prologue
    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, LaD/g;->c:Z

    .line 115
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/d;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/d;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/u;)Z

    .line 117
    return-void
.end method

.method public c(LaD/k;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 86
    if-eqz p2, :cond_e

    .line 87
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/c;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/q;)Z

    .line 93
    :goto_d
    return-void

    .line 90
    :cond_e
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/c;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/c;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/q;)Z

    goto :goto_d
.end method

.method public d(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/b;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/b;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public e(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/b;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, LaD/b;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public f(LaD/k;)V
    .registers 5
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/b;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/b;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    .line 135
    return-void
.end method

.method public g(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/s;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, LaD/s;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public h(LaD/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/s;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, LaD/s;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    move-result v0

    return v0
.end method

.method public i(LaD/k;)V
    .registers 5
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, LaD/g;->a:LaD/m;

    new-instance v1, LaD/s;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LaD/s;-><init>(ILaD/k;)V

    invoke-interface {v0, v1}, LaD/m;->a(LaD/o;)Z

    .line 153
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 192
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 197
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1, p2, p3, p4}, LaD/m;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onLongPress(Landroid/view/MotionEvent;)V

    .line 178
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    iget-boolean v0, p0, LaD/g;->c:Z

    if-nez v0, :cond_e

    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1, p2, p3, p4}, LaD/m;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onShowPress(Landroid/view/MotionEvent;)V

    .line 163
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, LaD/g;->a:LaD/m;

    invoke-interface {v0, p1}, LaD/m;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
