.class public LaD/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:F

.field private B:F

.field private C:J

.field private D:F

.field private E:F

.field private F:F

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:LaD/h;

.field private a:Landroid/content/Context;

.field private b:Landroid/view/MotionEvent;

.field private c:Landroid/view/MotionEvent;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:LaD/e;

.field private final g:LaD/e;

.field private final h:LaD/e;

.field private final i:LaD/e;

.field private final j:Ljava/util/LinkedList;

.field private k:J

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;LaD/n;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaD/k;->d:Ljava/util/List;

    .line 280
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaD/k;->e:Ljava/util/List;

    .line 287
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    .line 331
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 332
    iput-object p1, p0, LaD/k;->a:Landroid/content/Context;

    .line 333
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LaD/k;->D:F

    .line 338
    iget-object v0, p0, LaD/k;->d:Ljava/util/List;

    new-instance v1, LaD/v;

    invoke-direct {v1, p2}, LaD/v;-><init>(LaD/n;)V

    iput-object v1, p0, LaD/k;->g:LaD/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 342
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->B()Z

    move-result v0

    if-eqz v0, :cond_77

    .line 343
    iget-object v0, p0, LaD/k;->d:Ljava/util/List;

    new-instance v1, LaD/p;

    invoke-direct {v1, p2}, LaD/p;-><init>(LaD/n;)V

    iput-object v1, p0, LaD/k;->h:LaD/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    const-string v0, "MD"

    const-string v1, "T"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :goto_4c
    iget-object v0, p0, LaD/k;->d:Ljava/util/List;

    new-instance v1, LaD/r;

    invoke-direct {v1, p2}, LaD/r;-><init>(LaD/n;)V

    iput-object v1, p0, LaD/k;->f:LaD/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    iget-object v0, p0, LaD/k;->d:Ljava/util/List;

    new-instance v1, LaD/x;

    invoke-direct {v1, p2}, LaD/x;-><init>(LaD/n;)V

    iput-object v1, p0, LaD/k;->i:LaD/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    new-instance v0, LaD/h;

    invoke-direct {v0, p1, p2}, LaD/h;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LaD/k;->K:LaD/h;

    .line 352
    iget-object v0, p0, LaD/k;->K:LaD/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaD/h;->a(Z)V

    .line 353
    iget-object v0, p0, LaD/k;->K:LaD/h;

    invoke-virtual {v0, p2}, LaD/h;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 354
    return-void

    .line 346
    :cond_77
    iget-object v0, p0, LaD/k;->d:Ljava/util/List;

    new-instance v1, LaD/t;

    invoke-direct {v1, p2}, LaD/t;-><init>(LaD/n;)V

    iput-object v1, p0, LaD/k;->h:LaD/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    const-string v0, "MD"

    const-string v1, "F"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4c
.end method

.method private static a(Landroid/view/MotionEvent;I)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 688
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    .line 689
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 594
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 598
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 599
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, LaD/k;->k:J

    .line 603
    :cond_13
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    new-instance v2, LaD/a;

    invoke-direct {v2, p1}, LaD/a;-><init>(Landroid/view/MotionEvent;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 606
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x14

    if-le v0, v2, :cond_32

    .line 607
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    invoke-virtual {v0}, LaD/j;->e()V

    .line 611
    :cond_32
    :goto_32
    invoke-direct {p0}, LaD/k;->k()Z

    move-result v0

    if-eqz v0, :cond_4d

    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_4d

    .line 612
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    invoke-virtual {v0}, LaD/j;->e()V

    goto :goto_32

    .line 617
    :cond_4d
    sparse-switch v1, :sswitch_data_9e

    :goto_50
    move v4, v7

    .line 633
    :goto_51
    iget-boolean v0, p0, LaD/k;->J:Z

    if-eqz v0, :cond_84

    .line 635
    iget-object v0, p0, LaD/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5b
    :goto_5b
    :pswitch_5b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/e;

    .line 638
    invoke-virtual {v0}, LaD/e;->a()Z

    move-result v1

    if-nez v1, :cond_5b

    .line 649
    sget-object v9, LaD/l;->a:[I

    iget-wide v1, p0, LaD/k;->k:J

    iget-object v3, p0, LaD/k;->j:Ljava/util/LinkedList;

    iget-object v5, p0, LaD/k;->e:Ljava/util/List;

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, LaD/e;->a(JLjava/util/LinkedList;ZLjava/util/List;Ljava/lang/StringBuilder;)LaD/f;

    move-result-object v1

    invoke-virtual {v1}, LaD/f;->ordinal()I

    move-result v1

    aget v1, v9, v1

    packed-switch v1, :pswitch_data_b0

    goto :goto_5b

    .line 672
    :cond_84
    :pswitch_84
    if-eqz v4, :cond_8b

    .line 673
    invoke-direct {p0}, LaD/k;->j()V

    .line 674
    iput-boolean v7, p0, LaD/k;->J:Z

    .line 676
    :cond_8b
    return-void

    .line 624
    :sswitch_8c
    const/4 v4, 0x1

    .line 625
    goto :goto_51

    .line 628
    :sswitch_8e
    iput-boolean v7, p0, LaD/k;->J:Z

    goto :goto_50

    .line 661
    :pswitch_91
    invoke-virtual {v0, p0}, LaD/e;->a(LaD/k;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 662
    iget-object v1, p0, LaD/k;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5b

    .line 617
    nop

    :sswitch_data_9e
    .sparse-switch
        0x1 -> :sswitch_8c
        0x3 -> :sswitch_8e
        0x6 -> :sswitch_8c
        0x106 -> :sswitch_8c
    .end sparse-switch

    .line 649
    :pswitch_data_b0
    .packed-switch 0x1
        :pswitch_5b
        :pswitch_84
        :pswitch_91
    .end packed-switch
.end method

.method private a(LaD/e;)Z
    .registers 3
    .parameter

    .prologue
    .line 569
    if-eqz p1, :cond_a

    invoke-virtual {p1}, LaD/e;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private a(Ljava/lang/StringBuilder;)Z
    .registers 5
    .parameter

    .prologue
    .line 573
    const/4 v0, 0x0

    .line 574
    iget-object v1, p0, LaD/k;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/e;

    .line 580
    invoke-virtual {v0, p0}, LaD/e;->e(LaD/k;)Z

    move-result v0

    .line 581
    or-int/2addr v0, v1

    move v1, v0

    .line 582
    goto :goto_8

    .line 583
    :cond_1b
    return v1
.end method

.method private static b(Landroid/view/MotionEvent;I)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 696
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    .line 697
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .registers 13
    .parameter

    .prologue
    const/high16 v1, -0x4080

    const/high16 v10, 0x3f00

    const/4 v9, 0x0

    .line 701
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, LaD/k;->c:Landroid/view/MotionEvent;

    .line 703
    iput v1, p0, LaD/k;->t:F

    .line 704
    iput v1, p0, LaD/k;->u:F

    .line 705
    iput v1, p0, LaD/k;->x:F

    .line 706
    const/4 v0, 0x0

    iput v0, p0, LaD/k;->y:F

    .line 707
    iput-boolean v9, p0, LaD/k;->H:Z

    .line 708
    iput-boolean v9, p0, LaD/k;->I:Z

    .line 710
    iget-object v0, p0, LaD/k;->b:Landroid/view/MotionEvent;

    .line 712
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 713
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 714
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 715
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 716
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    .line 717
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 718
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    .line 719
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    .line 721
    sub-float/2addr v3, v1

    .line 722
    sub-float/2addr v4, v2

    .line 723
    sub-float/2addr v7, v5

    .line 724
    sub-float/2addr v8, v6

    .line 725
    iput v3, p0, LaD/k;->p:F

    .line 726
    iput v4, p0, LaD/k;->q:F

    .line 727
    iput v7, p0, LaD/k;->r:F

    .line 728
    iput v8, p0, LaD/k;->s:F

    .line 729
    iput v2, p0, LaD/k;->v:F

    .line 730
    iput v6, p0, LaD/k;->w:F

    .line 732
    mul-float/2addr v7, v10

    add-float/2addr v5, v7

    iput v5, p0, LaD/k;->l:F

    .line 733
    mul-float v5, v8, v10

    add-float/2addr v5, v6

    iput v5, p0, LaD/k;->m:F

    .line 734
    mul-float/2addr v3, v10

    add-float/2addr v1, v3

    iput v1, p0, LaD/k;->n:F

    .line 735
    mul-float v1, v4, v10

    add-float/2addr v1, v2

    iput v1, p0, LaD/k;->o:F

    .line 736
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iput-wide v1, p0, LaD/k;->C:J

    .line 737
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, LaD/k;->A:F

    .line 738
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v0

    add-float/2addr v0, v1

    iput v0, p0, LaD/k;->B:F

    .line 739
    return-void
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 565
    iget-object v0, p0, LaD/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private j()V
    .registers 3

    .prologue
    .line 587
    iget-object v0, p0, LaD/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/e;

    .line 589
    invoke-virtual {v0, p0}, LaD/e;->c(LaD/k;)V

    goto :goto_6

    .line 591
    :cond_16
    return-void
.end method

.method private k()Z
    .registers 6

    .prologue
    .line 679
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    invoke-virtual {v0}, LaD/j;->a()J

    move-result-wide v1

    .line 680
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    invoke-virtual {v0}, LaD/j;->a()J

    move-result-wide v3

    .line 681
    sub-long v0, v3, v1

    const-wide/16 v2, 0xfa

    cmp-long v0, v0, v2

    if-ltz v0, :cond_22

    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method private l()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 742
    iput-object v1, p0, LaD/k;->b:Landroid/view/MotionEvent;

    .line 743
    iput-object v1, p0, LaD/k;->c:Landroid/view/MotionEvent;

    .line 744
    iput-boolean v0, p0, LaD/k;->G:Z

    .line 745
    iput-boolean v0, p0, LaD/k;->J:Z

    .line 746
    iget-object v0, p0, LaD/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 747
    invoke-direct {p0}, LaD/k;->m()V

    .line 748
    iget-object v0, p0, LaD/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_18
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/e;

    .line 749
    invoke-virtual {v0}, LaD/e;->a()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 750
    invoke-virtual {v0, p0}, LaD/e;->c(LaD/k;)V

    goto :goto_18

    .line 753
    :cond_2e
    return-void
.end method

.method private m()V
    .registers 3

    .prologue
    .line 756
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    .line 757
    invoke-virtual {v0}, LaD/j;->e()V

    goto :goto_6

    .line 759
    :cond_16
    iget-object v0, p0, LaD/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 760
    return-void
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 774
    iget v0, p0, LaD/k;->l:F

    return v0
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 938
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 939
    iget-object v0, p0, LaD/k;->K:LaD/h;

    invoke-virtual {v0, p1}, LaD/h;->a(Z)V

    .line 940
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 13
    .parameter

    .prologue
    const/high16 v10, -0x4080

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 357
    const/4 v1, 0x0

    .line 420
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 422
    const v4, 0xff00

    and-int/2addr v4, v3

    shr-int/lit8 v4, v4, 0x8

    .line 424
    iget-boolean v5, p0, LaD/k;->J:Z

    if-nez v5, :cond_17d

    .line 425
    const/4 v1, 0x5

    if-eq v3, v1, :cond_1c

    const/16 v1, 0x105

    if-eq v3, v1, :cond_1c

    if-nez v3, :cond_cf

    .line 429
    :cond_1c
    iget-object v1, p0, LaD/k;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 430
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p0, LaD/k;->D:F

    sub-float/2addr v3, v4

    iput v3, p0, LaD/k;->E:F

    .line 431
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v3, p0, LaD/k;->D:F

    sub-float/2addr v1, v3

    iput v1, p0, LaD/k;->F:F

    .line 434
    invoke-direct {p0}, LaD/k;->l()V

    .line 436
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, LaD/k;->b:Landroid/view/MotionEvent;

    .line 437
    const-wide/16 v3, 0x0

    iput-wide v3, p0, LaD/k;->C:J

    .line 439
    invoke-direct {p0, p1}, LaD/k;->b(Landroid/view/MotionEvent;)V

    .line 444
    iget v1, p0, LaD/k;->D:F

    .line 445
    iget v4, p0, LaD/k;->E:F

    .line 446
    iget v5, p0, LaD/k;->F:F

    .line 447
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 448
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    .line 449
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, LaD/k;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 450
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, LaD/k;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    .line 452
    cmpg-float v9, v3, v1

    if-ltz v9, :cond_78

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_78

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_78

    cmpl-float v3, v6, v5

    if-lez v3, :cond_9a

    :cond_78
    move v3, v2

    .line 453
    :goto_79
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_89

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_89

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_89

    cmpl-float v1, v8, v5

    if-lez v1, :cond_9c

    :cond_89
    move v1, v2

    .line 455
    :goto_8a
    if-eqz v3, :cond_9e

    if-eqz v1, :cond_9e

    .line 456
    iput v10, p0, LaD/k;->l:F

    .line 457
    iput v10, p0, LaD/k;->m:F

    .line 458
    iput-boolean v2, p0, LaD/k;->G:Z

    .line 557
    :cond_94
    :goto_94
    iget-object v0, p0, LaD/k;->K:LaD/h;

    invoke-virtual {v0, p1}, LaD/h;->a(Landroid/view/MotionEvent;)Z

    .line 561
    return v2

    :cond_9a
    move v3, v0

    .line 452
    goto :goto_79

    :cond_9c
    move v1, v0

    .line 453
    goto :goto_8a

    .line 459
    :cond_9e
    if-eqz v3, :cond_bb

    .line 460
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, LaD/k;->l:F

    .line 461
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LaD/k;->m:F

    .line 462
    iput-boolean v2, p0, LaD/k;->G:Z

    goto :goto_94

    .line 463
    :cond_bb
    if-eqz v1, :cond_cc

    .line 464
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, LaD/k;->l:F

    .line 465
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LaD/k;->m:F

    .line 466
    iput-boolean v2, p0, LaD/k;->G:Z

    goto :goto_94

    .line 468
    :cond_cc
    iput-boolean v2, p0, LaD/k;->J:Z

    goto :goto_94

    .line 470
    :cond_cf
    const/4 v1, 0x2

    if-ne v3, v1, :cond_15a

    iget-boolean v1, p0, LaD/k;->G:Z

    if-eqz v1, :cond_15a

    .line 472
    iget v1, p0, LaD/k;->D:F

    .line 473
    iget v4, p0, LaD/k;->E:F

    .line 474
    iget v5, p0, LaD/k;->F:F

    .line 475
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 476
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    .line 477
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, LaD/k;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 478
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, LaD/k;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    .line 480
    cmpg-float v9, v3, v1

    if-ltz v9, :cond_108

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_108

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_108

    cmpl-float v3, v6, v5

    if-lez v3, :cond_124

    :cond_108
    move v3, v2

    .line 481
    :goto_109
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_119

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_119

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_119

    cmpl-float v1, v8, v5

    if-lez v1, :cond_126

    :cond_119
    move v1, v2

    .line 483
    :goto_11a
    if-eqz v3, :cond_128

    if-eqz v1, :cond_128

    .line 484
    iput v10, p0, LaD/k;->l:F

    .line 485
    iput v10, p0, LaD/k;->m:F

    goto/16 :goto_94

    :cond_124
    move v3, v0

    .line 480
    goto :goto_109

    :cond_126
    move v1, v0

    .line 481
    goto :goto_11a

    .line 486
    :cond_128
    if-eqz v3, :cond_144

    .line 487
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, LaD/k;->l:F

    .line 488
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LaD/k;->m:F

    goto/16 :goto_94

    .line 489
    :cond_144
    if-eqz v1, :cond_154

    .line 490
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, LaD/k;->l:F

    .line 491
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LaD/k;->m:F

    goto/16 :goto_94

    .line 493
    :cond_154
    iput-boolean v0, p0, LaD/k;->G:Z

    .line 494
    iput-boolean v2, p0, LaD/k;->J:Z

    goto/16 :goto_94

    .line 496
    :cond_15a
    const/4 v1, 0x6

    if-eq v3, v1, :cond_163

    const/16 v1, 0x106

    if-eq v3, v1, :cond_163

    if-ne v3, v2, :cond_94

    :cond_163
    iget-boolean v1, p0, LaD/k;->G:Z

    if-eqz v1, :cond_94

    .line 501
    if-nez v4, :cond_16f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 502
    :cond_16f
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, LaD/k;->l:F

    .line 503
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LaD/k;->m:F

    goto/16 :goto_94

    .line 505
    :cond_17d
    invoke-direct {p0}, LaD/k;->i()Z

    move-result v5

    if-nez v5, :cond_18c

    .line 508
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-direct {p0, v0, v1}, LaD/k;->a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V

    goto/16 :goto_94

    .line 513
    :cond_18c
    sparse-switch v3, :sswitch_data_1e2

    goto/16 :goto_94

    .line 518
    :sswitch_191
    invoke-direct {p0, p1}, LaD/k;->b(Landroid/view/MotionEvent;)V

    .line 521
    if-nez v4, :cond_19c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 522
    :cond_19c
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, LaD/k;->l:F

    .line 523
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, LaD/k;->m:F

    .line 525
    iget-boolean v0, p0, LaD/k;->G:Z

    if-nez v0, :cond_1af

    .line 526
    invoke-direct {p0}, LaD/k;->j()V

    .line 529
    :cond_1af
    invoke-direct {p0}, LaD/k;->l()V

    goto/16 :goto_94

    .line 533
    :sswitch_1b4
    iget-boolean v0, p0, LaD/k;->G:Z

    if-nez v0, :cond_1bb

    .line 534
    invoke-direct {p0}, LaD/k;->j()V

    .line 537
    :cond_1bb
    invoke-direct {p0}, LaD/k;->l()V

    goto/16 :goto_94

    .line 541
    :sswitch_1c0
    invoke-direct {p0, p1}, LaD/k;->b(Landroid/view/MotionEvent;)V

    .line 544
    iget-object v0, p0, LaD/k;->c:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v1}, LaD/k;->a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V

    .line 549
    iget v0, p0, LaD/k;->A:F

    iget v3, p0, LaD/k;->B:F

    div-float/2addr v0, v3

    const v3, 0x3f2b851f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_94

    .line 550
    invoke-direct {p0, v1}, LaD/k;->a(Ljava/lang/StringBuilder;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 551
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, LaD/k;->b:Landroid/view/MotionEvent;

    goto/16 :goto_94

    .line 513
    :sswitch_data_1e2
    .sparse-switch
        0x1 -> :sswitch_191
        0x2 -> :sswitch_1c0
        0x3 -> :sswitch_1b4
        0x6 -> :sswitch_191
        0x106 -> :sswitch_191
    .end sparse-switch
.end method

.method public b()F
    .registers 2

    .prologue
    .line 789
    iget v0, p0, LaD/k;->m:F

    return v0
.end method

.method public c()F
    .registers 2

    .prologue
    .line 802
    iget v0, p0, LaD/k;->n:F

    return v0
.end method

.method public d()F
    .registers 3

    .prologue
    .line 825
    iget v0, p0, LaD/k;->t:F

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-nez v0, :cond_17

    .line 826
    iget v0, p0, LaD/k;->r:F

    .line 827
    iget v1, p0, LaD/k;->s:F

    .line 828
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LaD/k;->t:F

    .line 830
    :cond_17
    iget v0, p0, LaD/k;->t:F

    return v0
.end method

.method public e()F
    .registers 3

    .prologue
    .line 840
    iget v0, p0, LaD/k;->u:F

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-nez v0, :cond_17

    .line 841
    iget v0, p0, LaD/k;->p:F

    .line 842
    iget v1, p0, LaD/k;->q:F

    .line 843
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LaD/k;->u:F

    .line 845
    :cond_17
    iget v0, p0, LaD/k;->u:F

    return v0
.end method

.method public f()F
    .registers 4

    .prologue
    const/high16 v0, 0x3f80

    .line 856
    iget-object v1, p0, LaD/k;->f:LaD/e;

    invoke-direct {p0, v1}, LaD/k;->a(LaD/e;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 865
    :cond_a
    :goto_a
    return v0

    .line 859
    :cond_b
    iget-object v1, p0, LaD/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, LaD/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_a

    .line 862
    iget v0, p0, LaD/k;->x:F

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2c

    .line 863
    invoke-virtual {p0}, LaD/k;->d()F

    move-result v0

    invoke-virtual {p0}, LaD/k;->e()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, LaD/k;->x:F

    .line 865
    :cond_2c
    iget v0, p0, LaD/k;->x:F

    goto :goto_a
.end method

.method public g()F
    .registers 3

    .prologue
    .line 879
    iget-object v0, p0, LaD/k;->g:LaD/e;

    invoke-direct {p0, v0}, LaD/k;->a(LaD/e;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 880
    const/4 v0, 0x0

    .line 888
    :goto_9
    return v0

    .line 882
    :cond_a
    iget-boolean v0, p0, LaD/k;->H:Z

    if-nez v0, :cond_1b

    .line 885
    iget v0, p0, LaD/k;->w:F

    iget v1, p0, LaD/k;->v:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x3e80

    mul-float/2addr v0, v1

    iput v0, p0, LaD/k;->y:F

    .line 886
    const/4 v0, 0x1

    iput-boolean v0, p0, LaD/k;->H:Z

    .line 888
    :cond_1b
    iget v0, p0, LaD/k;->y:F

    goto :goto_9
.end method

.method public h()F
    .registers 7

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 896
    iget-object v1, p0, LaD/k;->h:LaD/e;

    invoke-direct {p0, v1}, LaD/k;->a(LaD/e;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 914
    :cond_a
    :goto_a
    return v0

    .line 899
    :cond_b
    iget-object v1, p0, LaD/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, LaD/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_a

    .line 902
    iget-boolean v0, p0, LaD/k;->I:Z

    if-nez v0, :cond_7e

    .line 903
    iget-object v0, p0, LaD/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget-object v1, p0, LaD/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v2, p0, LaD/k;->c:Landroid/view/MotionEvent;

    iget-object v3, p0, LaD/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget-object v3, p0, LaD/k;->c:Landroid/view/MotionEvent;

    iget-object v4, p0, LaD/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, LaD/j;->a(FFFF)F

    move-result v0

    .line 907
    iget-object v1, p0, LaD/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iget-object v2, p0, LaD/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget-object v3, p0, LaD/k;->b:Landroid/view/MotionEvent;

    iget-object v4, p0, LaD/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, LaD/k;->b:Landroid/view/MotionEvent;

    iget-object v5, p0, LaD/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-static {v1, v2, v3, v4}, LaD/j;->a(FFFF)F

    move-result v1

    .line 911
    invoke-static {v1, v0}, LaD/e;->a(FF)F

    move-result v0

    iput v0, p0, LaD/k;->z:F

    .line 912
    const/4 v0, 0x1

    iput-boolean v0, p0, LaD/k;->I:Z

    .line 914
    :cond_7e
    iget v0, p0, LaD/k;->z:F

    goto :goto_a
.end method
