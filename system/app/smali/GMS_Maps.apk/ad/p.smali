.class public LaD/p;
.super LaD/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(LaD/n;)V
    .registers 2
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, LaD/e;-><init>(LaD/n;)V

    .line 66
    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)LaD/f;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/high16 v5, 0x3f00

    .line 75
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/e;

    .line 76
    invoke-virtual {v0}, LaD/e;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 80
    sget-object v0, LaD/f;->a:LaD/f;

    .line 166
    :goto_1b
    return-object v0

    .line 86
    :cond_1c
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_26

    .line 90
    sget-object v0, LaD/f;->b:LaD/f;

    goto :goto_1b

    .line 94
    :cond_26
    const/4 v1, 0x0

    .line 95
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    .line 96
    invoke-virtual {v0}, LaD/j;->b()I

    move-result v3

    if-le v3, v4, :cond_2b

    move-object v1, v0

    .line 101
    :cond_3e
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    .line 102
    if-nez v1, :cond_49

    .line 106
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_1b

    .line 108
    :cond_49
    invoke-virtual {v0}, LaD/j;->b()I

    move-result v2

    if-gt v2, v4, :cond_52

    .line 112
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_1b

    .line 116
    :cond_52
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_72

    const v2, 0x3db2b8c2

    .line 120
    :goto_5b
    invoke-virtual {v1}, LaD/j;->f()F

    move-result v3

    .line 121
    invoke-virtual {v0}, LaD/j;->f()F

    move-result v4

    .line 122
    invoke-static {v3, v4}, LaD/p;->a(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 123
    cmpg-float v2, v3, v2

    if-gez v2, :cond_76

    .line 128
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_1b

    .line 116
    :cond_72
    const v2, 0x3e32b8c2

    goto :goto_5b

    .line 132
    :cond_76
    invoke-virtual {v0}, LaD/j;->c()F

    move-result v2

    invoke-virtual {v0}, LaD/j;->d()F

    move-result v4

    add-float/2addr v2, v4

    mul-float/2addr v2, v5

    .line 133
    invoke-virtual {v0}, LaD/j;->g()F

    move-result v0

    div-float/2addr v0, v2

    .line 134
    const/high16 v4, 0x3f40

    cmpg-float v4, v0, v4

    if-gez v4, :cond_8e

    .line 139
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_1b

    .line 144
    :cond_8e
    invoke-virtual {v1}, LaD/j;->g()F

    move-result v1

    div-float/2addr v1, v2

    .line 145
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 146
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_b2

    .line 147
    div-float v0, v3, v0

    .line 148
    cmpg-float v1, v0, v5

    if-gez v1, :cond_a7

    .line 152
    sget-object v0, LaD/f;->a:LaD/f;

    goto/16 :goto_1b

    .line 153
    :cond_a7
    const v1, 0x3f666666

    cmpg-float v0, v0, v1

    if-gez v0, :cond_b2

    .line 157
    sget-object v0, LaD/f;->b:LaD/f;

    goto/16 :goto_1b

    .line 166
    :cond_b2
    sget-object v0, LaD/f;->c:LaD/f;

    goto/16 :goto_1b
.end method

.method protected b(LaD/k;)Z
    .registers 3
    .parameter

    .prologue
    .line 171
    const-string v0, "r"

    invoke-virtual {p0, v0}, LaD/p;->a(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, LaD/p;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->e(LaD/k;)Z

    move-result v0

    return v0
.end method

.method protected d(LaD/k;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, LaD/p;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->f(LaD/k;)V

    .line 178
    return-void
.end method

.method protected f(LaD/k;)Z
    .registers 3
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, LaD/p;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->d(LaD/k;)Z

    move-result v0

    return v0
.end method
