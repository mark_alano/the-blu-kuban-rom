.class public LaD/v;
.super LaD/w;
.source "SourceFile"


# direct methods
.method public constructor <init>(LaD/n;)V
    .registers 2
    .parameter

    .prologue
    .line 16
    invoke-direct {p0, p1}, LaD/w;-><init>(LaD/n;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected a(F)F
    .registers 6
    .parameter

    .prologue
    .line 31
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ff921fb54442d18L

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method protected a(LaD/j;I)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-virtual {p1, p2}, LaD/j;->a(I)F

    move-result v0

    return v0
.end method

.method protected b(LaD/j;I)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-virtual {p1, p2}, LaD/j;->b(I)F

    move-result v0

    return v0
.end method

.method protected b(LaD/k;)Z
    .registers 3
    .parameter

    .prologue
    .line 36
    const-string v0, "t"

    invoke-virtual {p0, v0}, LaD/v;->a(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, LaD/v;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->b(LaD/k;)Z

    move-result v0

    return v0
.end method

.method protected d(LaD/k;)V
    .registers 3
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LaD/v;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->c(LaD/k;)V

    .line 43
    return-void
.end method

.method protected f(LaD/k;)Z
    .registers 3
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, LaD/v;->a:LaD/n;

    invoke-interface {v0, p1}, LaD/n;->a(LaD/k;)Z

    move-result v0

    return v0
.end method
