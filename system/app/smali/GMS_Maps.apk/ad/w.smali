.class public abstract LaD/w;
.super LaD/e;
.source "SourceFile"


# instance fields
.field protected b:F

.field protected c:F

.field protected d:F

.field protected e:F


# direct methods
.method public constructor <init>(LaD/n;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, LaD/e;-><init>(LaD/n;)V

    .line 52
    const v0, 0x3f490fdb

    iput v0, p0, LaD/w;->b:F

    .line 55
    const/high16 v0, 0x3e80

    iput v0, p0, LaD/w;->c:F

    .line 58
    const/high16 v0, 0x3e00

    iput v0, p0, LaD/w;->d:F

    .line 61
    const/high16 v0, 0x3f80

    iput v0, p0, LaD/w;->e:F

    .line 65
    return-void
.end method


# virtual methods
.method protected abstract a(F)F
.end method

.method protected abstract a(LaD/j;I)F
.end method

.method public a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)LaD/f;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 92
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_a

    .line 96
    sget-object v0, LaD/f;->b:LaD/f;

    .line 195
    :goto_9
    return-object v0

    .line 99
    :cond_a
    const/4 v5, 0x0

    .line 100
    const/4 v4, 0x0

    .line 101
    const/4 v3, 0x0

    .line 102
    const/4 v2, 0x0

    .line 105
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    .line 106
    invoke-virtual {v0}, LaD/j;->b()I

    move-result v1

    const/4 v6, 0x2

    if-eq v1, v6, :cond_1e

    .line 110
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_9

    .line 116
    :cond_1e
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v8

    .line 117
    const/4 v1, 0x0

    move v6, v4

    move v7, v5

    move v4, v2

    move v5, v3

    move-object v2, v0

    move-object v3, v1

    .line 119
    :goto_2d
    invoke-interface {v8}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_43

    .line 120
    invoke-interface {v8}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaD/j;

    .line 121
    invoke-virtual {v1}, LaD/j;->b()I

    move-result v9

    invoke-virtual {v0}, LaD/j;->b()I

    move-result v10

    if-eq v9, v10, :cond_51

    .line 162
    :cond_43
    add-float v1, v7, v6

    add-float v3, v5, v4

    iget v4, p0, LaD/w;->e:F

    mul-float/2addr v3, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_d5

    .line 166
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_9

    .line 126
    :cond_51
    invoke-virtual {v1}, LaD/j;->f()F

    move-result v2

    .line 127
    invoke-virtual {p0, v2}, LaD/w;->a(F)F

    move-result v2

    .line 130
    iget v9, p0, LaD/w;->b:F

    cmpl-float v2, v2, v9

    if-ltz v2, :cond_62

    .line 135
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_9

    .line 140
    :cond_62
    invoke-virtual {v1}, LaD/j;->g()F

    move-result v2

    invoke-virtual {v1}, LaD/j;->c()F

    move-result v9

    div-float/2addr v2, v9

    .line 141
    iget v9, p0, LaD/w;->c:F

    cmpg-float v2, v2, v9

    if-gez v2, :cond_74

    .line 146
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_9

    .line 150
    :cond_74
    if-eqz v3, :cond_126

    .line 151
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LaD/w;->a(LaD/j;I)F

    move-result v2

    const/4 v9, 0x0

    invoke-virtual {p0, v3, v9}, LaD/w;->a(LaD/j;I)F

    move-result v9

    sub-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v7, v2

    .line 152
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LaD/w;->b(LaD/j;I)F

    move-result v2

    const/4 v9, 0x0

    invoke-virtual {p0, v3, v9}, LaD/w;->b(LaD/j;I)F

    move-result v9

    sub-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v5, v2

    .line 153
    invoke-virtual {v1}, LaD/j;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, LaD/w;->a(LaD/j;I)F

    move-result v2

    invoke-virtual {v3}, LaD/j;->b()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p0, v3, v9}, LaD/w;->a(LaD/j;I)F

    move-result v9

    sub-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v6, v2

    .line 155
    invoke-virtual {v1}, LaD/j;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, LaD/w;->b(LaD/j;I)F

    move-result v2

    invoke-virtual {v3}, LaD/j;->b()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p0, v3, v9}, LaD/w;->b(LaD/j;I)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    :goto_cd
    move v6, v4

    move v7, v5

    move v4, v2

    move v5, v3

    move-object v2, v1

    move-object v3, v1

    .line 159
    goto/16 :goto_2d

    .line 170
    :cond_d5
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LaD/w;->b(LaD/j;I)F

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LaD/w;->b(LaD/j;I)F

    move-result v3

    sub-float/2addr v1, v3

    .line 171
    invoke-virtual {v0}, LaD/j;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v0, v3}, LaD/w;->b(LaD/j;I)F

    move-result v3

    invoke-virtual {v2}, LaD/j;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v2, v4}, LaD/w;->b(LaD/j;I)F

    move-result v2

    sub-float v2, v3, v2

    .line 175
    mul-float v3, v1, v2

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_101

    .line 179
    sget-object v0, LaD/f;->a:LaD/f;

    goto/16 :goto_9

    .line 183
    :cond_101
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v0}, LaD/j;->d()F

    move-result v3

    div-float/2addr v1, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {v0}, LaD/j;->d()F

    move-result v0

    div-float v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 185
    iget v1, p0, LaD/w;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_122

    .line 189
    sget-object v0, LaD/f;->b:LaD/f;

    goto/16 :goto_9

    .line 195
    :cond_122
    sget-object v0, LaD/f;->c:LaD/f;

    goto/16 :goto_9

    :cond_126
    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    goto :goto_cd
.end method

.method protected abstract b(LaD/j;I)F
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method
