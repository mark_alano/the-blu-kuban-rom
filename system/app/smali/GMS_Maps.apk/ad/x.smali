.class public LaD/x;
.super LaD/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(LaD/n;)V
    .registers 2
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, LaD/e;-><init>(LaD/n;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)LaD/f;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 50
    .line 51
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ae

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    .line 52
    invoke-virtual {v0}, LaD/j;->b()I

    move-result v3

    if-ne v3, v5, :cond_8

    move-object v1, v0

    .line 58
    :goto_1b
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 59
    :cond_23
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_ac

    .line 60
    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaD/j;

    .line 61
    invoke-virtual {v0}, LaD/j;->b()I

    move-result v4

    if-ne v4, v5, :cond_23

    .line 66
    :goto_35
    if-eqz v1, :cond_39

    if-nez v0, :cond_3c

    .line 70
    :cond_39
    sget-object v0, LaD/f;->a:LaD/f;

    .line 103
    :goto_3b
    return-object v0

    .line 74
    :cond_3c
    invoke-virtual {v0}, LaD/j;->a()J

    move-result-wide v2

    sub-long/2addr v2, p1

    .line 75
    const-wide/16 v4, 0x12c

    cmp-long v2, v2, v4

    if-lez v2, :cond_4a

    .line 79
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_3b

    .line 85
    :cond_4a
    invoke-virtual {v0, v6}, LaD/j;->a(I)F

    move-result v2

    invoke-virtual {v1, v6}, LaD/j;->a(I)F

    move-result v3

    sub-float/2addr v2, v3

    .line 86
    invoke-virtual {v0, v6}, LaD/j;->b(I)F

    move-result v3

    invoke-virtual {v1, v6}, LaD/j;->b(I)F

    move-result v4

    sub-float/2addr v3, v4

    .line 87
    invoke-virtual {v0, v7}, LaD/j;->a(I)F

    move-result v4

    invoke-virtual {v1, v7}, LaD/j;->a(I)F

    move-result v5

    sub-float/2addr v4, v5

    .line 88
    invoke-virtual {v0, v7}, LaD/j;->b(I)F

    move-result v5

    invoke-virtual {v1, v7}, LaD/j;->b(I)F

    move-result v1

    sub-float v1, v5, v1

    .line 89
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {v0}, LaD/j;->c()F

    move-result v5

    div-float/2addr v2, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v0}, LaD/j;->d()F

    move-result v5

    div-float/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v0}, LaD/j;->c()F

    move-result v4

    div-float/2addr v3, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v0}, LaD/j;->d()F

    move-result v0

    div-float v0, v1, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 92
    const/high16 v1, 0x3e00

    cmpl-float v0, v0, v1

    if-lez v0, :cond_a9

    .line 96
    sget-object v0, LaD/f;->a:LaD/f;

    goto :goto_3b

    .line 103
    :cond_a9
    sget-object v0, LaD/f;->c:LaD/f;

    goto :goto_3b

    :cond_ac
    move-object v0, v2

    goto :goto_35

    :cond_ae
    move-object v1, v2

    goto/16 :goto_1b
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method protected b(LaD/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, LaD/x;->a:LaD/n;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, LaD/n;->b(LaD/k;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method protected d(LaD/k;)V
    .registers 4
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, LaD/x;->a:LaD/n;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, LaD/n;->c(LaD/k;Z)V

    .line 114
    return-void
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method protected f(LaD/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, LaD/x;->a:LaD/n;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, LaD/n;->a(LaD/k;Z)Z

    move-result v0

    return v0
.end method
