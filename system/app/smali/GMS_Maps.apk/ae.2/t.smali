.class public Lae/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lae/v;

.field protected b:Lae/v;

.field private final c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final d:[Lae/n;

.field private final e:[Lae/u;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Z

.field private j:I

.field private k:Lau/B;

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 207
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-direct {p0, v0}, Lae/t;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 208
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x15

    const/4 v2, -0x1

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput v2, p0, Lae/t;->j:I

    .line 211
    iput v2, p0, Lae/t;->j:I

    .line 212
    iput-object p1, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 214
    invoke-direct {p0}, Lae/t;->S()[Lae/n;

    move-result-object v0

    iput-object v0, p0, Lae/t;->d:[Lae/n;

    .line 215
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v0, v0, [Lae/u;

    iput-object v0, p0, Lae/t;->e:[Lae/u;

    .line 216
    invoke-direct {p0}, Lae/t;->T()V

    .line 217
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x13

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 219
    invoke-static {v0}, Lae/t;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v1

    iput v1, p0, Lae/t;->f:I

    .line 220
    invoke-static {v0}, Lae/t;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v1

    iput v1, p0, Lae/t;->g:I

    .line 221
    const/4 v1, 0x7

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lae/t;->h:I

    .line 223
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 224
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lae/t;->i:Z

    .line 228
    :goto_4c
    return-void

    .line 226
    :cond_4d
    const/4 v0, 0x0

    iput-boolean v0, p0, Lae/t;->i:Z

    goto :goto_4c
.end method

.method private S()[Lae/n;
    .registers 12

    .prologue
    const/16 v2, 0x13

    const/4 v10, 0x5

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 491
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 492
    new-array v0, v1, [Lae/n;

    .line 509
    :goto_f
    return-object v0

    .line 495
    :cond_10
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 496
    invoke-virtual {v4, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    .line 497
    new-array v2, v5, [Lae/n;

    move v3, v1

    .line 498
    :goto_1d
    if-ge v3, v5, :cond_3d

    .line 499
    invoke-virtual {v4, v10, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 502
    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 503
    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 506
    :goto_2d
    new-instance v7, Lae/n;

    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v0, v6}, Lae/n;-><init>(ILjava/lang/String;)V

    aput-object v7, v2, v3

    .line 498
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1d

    :cond_3d
    move-object v0, v2

    .line 509
    goto :goto_f

    :cond_3f
    move v0, v1

    goto :goto_2d
.end method

.method private T()V
    .registers 15

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 564
    invoke-virtual {p0}, Lae/t;->E()I

    move-result v0

    if-ne v0, v10, :cond_11

    .line 565
    invoke-static {p0}, Lae/v;->a(Lae/t;)Lae/v;

    move-result-object v0

    iput-object v0, p0, Lae/t;->a:Lae/v;

    .line 567
    :cond_11
    iget-object v6, p0, Lae/t;->a:Lae/v;

    .line 568
    const/4 v0, 0x0

    move v7, v0

    :goto_15
    iget-object v0, p0, Lae/t;->e:[Lae/u;

    array-length v0, v0

    if-ge v7, v0, :cond_82

    .line 569
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 571
    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_89

    .line 572
    invoke-virtual {v0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 573
    invoke-static {v1}, Lcom/google/googlenav/ui/bl;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v3

    .line 576
    :goto_30
    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_87

    .line 577
    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 578
    invoke-static {v1}, Lcom/google/googlenav/ui/bl;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v4

    .line 581
    :goto_3e
    invoke-virtual {v0, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_85

    .line 582
    invoke-virtual {v0, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v5

    .line 584
    :goto_4c
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 585
    invoke-virtual {v0, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 586
    const/4 v9, 0x6

    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_78

    .line 587
    const/4 v9, 0x6

    invoke-virtual {v0, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget v6, v6, Lae/v;->b:I

    invoke-static {v0, v6}, Lae/v;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lae/v;

    move-result-object v6

    .line 589
    iget-object v9, p0, Lae/t;->e:[Lae/u;

    new-instance v0, Lae/u;

    invoke-direct/range {v0 .. v6}, Lae/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Lau/B;Lae/v;)V

    aput-object v0, v9, v7

    .line 590
    invoke-static {v6}, Lae/v;->a(Lae/v;)Lae/v;

    move-result-object v6

    .line 568
    :goto_74
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_15

    .line 592
    :cond_78
    iget-object v9, p0, Lae/t;->e:[Lae/u;

    new-instance v0, Lae/u;

    invoke-direct/range {v0 .. v6}, Lae/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Lau/B;Lae/v;)V

    aput-object v0, v9, v7

    goto :goto_74

    .line 596
    :cond_82
    iput-object v6, p0, Lae/t;->b:Lae/v;

    .line 597
    return-void

    :cond_85
    move-object v5, v8

    goto :goto_4c

    :cond_87
    move-object v4, v8

    goto :goto_3e

    :cond_89
    move-object v3, v8

    goto :goto_30
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 242
    .line 243
    if-eqz p0, :cond_16

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 245
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 246
    if-ltz v0, :cond_14

    const/16 v2, 0x12

    if-lt v0, v2, :cond_15

    :cond_14
    move v0, v1

    .line 250
    :cond_15
    :goto_15
    return v0

    :cond_16
    move v0, v1

    goto :goto_15
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 257
    .line 258
    if-eqz p0, :cond_15

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 260
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 261
    if-ltz v0, :cond_13

    const/4 v2, 0x3

    if-lt v0, v2, :cond_14

    :cond_13
    move v0, v1

    .line 265
    :cond_14
    :goto_14
    return v0

    :cond_15
    move v0, v1

    goto :goto_14
.end method

.method private e(I)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 513
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lae/t;->d:[Lae/n;

    array-length v1, v1

    if-ge v0, v1, :cond_18

    .line 514
    iget-object v1, p0, Lae/t;->d:[Lae/n;

    aget-object v1, v1, v0

    .line 515
    invoke-virtual {v1}, Lae/n;->a()I

    move-result v2

    if-ne v2, p1, :cond_15

    .line 516
    invoke-virtual {v1}, Lae/n;->b()Ljava/lang/String;

    move-result-object v0

    .line 519
    :goto_14
    return-object v0

    .line 513
    :cond_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 519
    :cond_18
    const/4 v0, 0x0

    goto :goto_14
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .registers 3

    .prologue
    .line 401
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1c

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public B()I
    .registers 3

    .prologue
    .line 405
    iget v0, p0, Lae/t;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_e

    .line 406
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lae/t;->j:I

    .line 408
    :cond_e
    iget v0, p0, Lae/t;->j:I

    return v0
.end method

.method public C()Z
    .registers 3

    .prologue
    .line 417
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    return v0
.end method

.method public D()Ljava/lang/String;
    .registers 4

    .prologue
    .line 429
    iget-object v0, p0, Lae/t;->l:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 430
    iget-object v0, p0, Lae/t;->l:Ljava/lang/String;

    .line 442
    :goto_6
    return-object v0

    .line 436
    :cond_7
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 437
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lae/t;->e(I)Ljava/lang/String;

    move-result-object v1

    .line 438
    if-eqz v1, :cond_33

    .line 439
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 441
    :cond_33
    iput-object v0, p0, Lae/t;->l:Ljava/lang/String;

    .line 442
    iget-object v0, p0, Lae/t;->l:Ljava/lang/String;

    goto :goto_6
.end method

.method public E()I
    .registers 3

    .prologue
    .line 446
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public F()I
    .registers 3

    .prologue
    .line 454
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public G()Ljava/lang/String;
    .registers 3

    .prologue
    .line 476
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()[Lae/n;
    .registers 2

    .prologue
    .line 523
    iget-object v0, p0, Lae/t;->d:[Lae/n;

    return-object v0
.end method

.method public I()I
    .registers 4

    .prologue
    .line 527
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getNumBytesUsed()I

    move-result v0

    add-int/lit8 v1, v0, 0x14

    .line 528
    const/4 v0, 0x0

    :goto_9
    iget-object v2, p0, Lae/t;->d:[Lae/n;

    array-length v2, v2

    if-ge v0, v2, :cond_1a

    .line 529
    iget-object v2, p0, Lae/t;->d:[Lae/n;

    aget-object v2, v2, v0

    .line 530
    invoke-virtual {v2}, Lae/n;->d()I

    move-result v2

    add-int/2addr v1, v2

    .line 528
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 532
    :cond_1a
    return v1
.end method

.method public J()Z
    .registers 3

    .prologue
    .line 536
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public K()I
    .registers 3

    .prologue
    .line 540
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public L()Z
    .registers 3

    .prologue
    .line 544
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public M()I
    .registers 3

    .prologue
    .line 548
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public N()Z
    .registers 2

    .prologue
    .line 552
    iget-boolean v0, p0, Lae/t;->m:Z

    return v0
.end method

.method public O()V
    .registers 2

    .prologue
    .line 556
    const/4 v0, 0x1

    iput-boolean v0, p0, Lae/t;->m:Z

    .line 557
    return-void
.end method

.method public P()V
    .registers 2

    .prologue
    .line 560
    const/4 v0, 0x0

    iput-boolean v0, p0, Lae/t;->m:Z

    .line 561
    return-void
.end method

.method public Q()[Lae/u;
    .registers 2

    .prologue
    .line 600
    iget-object v0, p0, Lae/t;->e:[Lae/u;

    return-object v0
.end method

.method public R()Z
    .registers 3

    .prologue
    .line 672
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x17

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    return v0
.end method

.method public a()Lae/v;
    .registers 2

    .prologue
    .line 231
    iget-object v0, p0, Lae/t;->a:Lae/v;

    return-object v0
.end method

.method public a(Lae/b;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 459
    iget-boolean v0, p0, Lae/t;->i:Z

    if-nez v0, :cond_7

    .line 460
    const-string v0, ""

    .line 468
    :goto_6
    return-object v0

    .line 462
    :cond_7
    invoke-virtual {p0}, Lae/t;->F()I

    move-result v0

    invoke-virtual {p1, v0}, Lae/b;->j(I)Lae/d;

    move-result-object v0

    .line 463
    if-eqz v0, :cond_25

    .line 466
    invoke-virtual {v0}, Lae/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_20

    invoke-virtual {v0}, Lae/d;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_20
    invoke-virtual {v0}, Lae/d;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 468
    :cond_25
    const-string v0, ""

    goto :goto_6
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 373
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 374
    return-void
.end method

.method public a(J)V
    .registers 5
    .parameter

    .prologue
    .line 393
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 394
    return-void
.end method

.method public a(Lau/B;)V
    .registers 2
    .parameter

    .prologue
    .line 293
    iput-object p1, p0, Lae/t;->k:Lau/B;

    .line 294
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 341
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 342
    return-void
.end method

.method public b()Lae/v;
    .registers 2

    .prologue
    .line 235
    iget-object v0, p0, Lae/t;->b:Lae/v;

    return-object v0
.end method

.method public b(I)V
    .registers 4
    .parameter

    .prologue
    .line 385
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 386
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 269
    iget v0, p0, Lae/t;->f:I

    return v0
.end method

.method public c(I)V
    .registers 4
    .parameter

    .prologue
    .line 412
    iput p1, p0, Lae/t;->j:I

    .line 413
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 414
    return-void
.end method

.method public d()I
    .registers 2

    .prologue
    .line 273
    iget v0, p0, Lae/t;->g:I

    return v0
.end method

.method public d(I)V
    .registers 4
    .parameter

    .prologue
    .line 450
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 451
    return-void
.end method

.method public e()I
    .registers 2

    .prologue
    .line 277
    iget v0, p0, Lae/t;->h:I

    return v0
.end method

.method public f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 281
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public g()Lau/B;
    .registers 2

    .prologue
    .line 289
    iget-object v0, p0, Lae/t;->k:Lau/B;

    return-object v0
.end method

.method public h()Lo/B;
    .registers 3

    .prologue
    .line 300
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x19

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_f

    .line 302
    invoke-static {v0}, Lo/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/B;

    move-result-object v0

    .line 305
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public i()Ljava/lang/String;
    .registers 3

    .prologue
    .line 309
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    .line 313
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public k()Lau/B;
    .registers 3

    .prologue
    .line 317
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .registers 3

    .prologue
    .line 321
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .registers 3

    .prologue
    .line 325
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6d

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .registers 3

    .prologue
    .line 329
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x6e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .registers 3

    .prologue
    .line 333
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1d

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .registers 3

    .prologue
    .line 337
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public q()Lau/B;
    .registers 3

    .prologue
    .line 345
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v0

    return-object v0
.end method

.method public r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    .line 349
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public s()Z
    .registers 3

    .prologue
    .line 357
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public t()I
    .registers 3

    .prologue
    .line 361
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method public u()Ljava/lang/String;
    .registers 3

    .prologue
    .line 365
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v()I
    .registers 3

    .prologue
    .line 369
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public w()Z
    .registers 3

    .prologue
    .line 377
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    return v0
.end method

.method public x()I
    .registers 3

    .prologue
    .line 381
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    return v0
.end method

.method public y()J
    .registers 3

    .prologue
    .line 389
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()Ljava/lang/String;
    .registers 3

    .prologue
    .line 397
    iget-object v0, p0, Lae/t;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
