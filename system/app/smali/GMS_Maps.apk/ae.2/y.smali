.class public Lae/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Z

.field private static final b:Lae/y;


# instance fields
.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Lau/B;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/googlenav/ai;

.field private final j:Lo/B;

.field private final k:Ljava/lang/Integer;

.field private final l:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-static {v0, v0}, Lae/y;->b(Lau/B;Lo/B;)Lae/y;

    move-result-object v0

    sput-object v0, Lae/y;->b:Lae/y;

    .line 72
    const/4 v0, 0x1

    sput-boolean v0, Lae/y;->a:Z

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lae/y;->c:I

    .line 78
    iput-object p2, p0, Lae/y;->d:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lae/y;->e:Lau/B;

    .line 80
    iput-object p4, p0, Lae/y;->f:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lae/y;->g:Ljava/lang/String;

    .line 82
    iput-object p6, p0, Lae/y;->h:Ljava/lang/String;

    .line 83
    iput-object p7, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    .line 87
    if-nez p8, :cond_29

    if-eqz p7, :cond_29

    .line 88
    invoke-virtual {p7}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_29

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_29

    .line 90
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/B;

    move-object p8, v0

    .line 93
    :cond_29
    iput-object p8, p0, Lae/y;->j:Lo/B;

    .line 94
    iput-object p9, p0, Lae/y;->k:Ljava/lang/Integer;

    .line 95
    iput-object p10, p0, Lae/y;->l:Ljava/lang/Integer;

    .line 96
    return-void
.end method

.method public static a()Lae/y;
    .registers 1

    .prologue
    .line 169
    sget-object v0, Lae/y;->b:Lae/y;

    return-object v0
.end method

.method private static a(ILcom/google/googlenav/ai;)Lae/y;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 242
    new-instance v0, Lae/y;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()Lau/B;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bq()Ljava/lang/String;

    move-result-object v6

    move v1, p0

    move-object v7, p1

    move-object v9, v8

    move-object v10, v8

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private static a(ILcom/google/googlenav/ai;Ljava/lang/Integer;Ljava/lang/Integer;)Lae/y;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 249
    new-instance v0, Lae/y;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()Lau/B;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ak()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bq()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    move v1, p0

    move-object v7, p1

    move-object v9, p2

    move-object v10, p3

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lae/y;Ljava/lang/String;Ljava/lang/String;)Lae/y;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 269
    new-instance v0, Lae/y;

    iget v1, p0, Lae/y;->c:I

    iget-object v2, p0, Lae/y;->d:Ljava/lang/String;

    iget-object v3, p0, Lae/y;->e:Lau/B;

    iget-object v4, p0, Lae/y;->f:Ljava/lang/String;

    iget-object v7, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lae/y;->l()Lo/B;

    move-result-object v8

    move-object v5, p1

    move-object v6, p2

    move-object v10, v9

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lae/y;Lo/B;)Lae/y;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 278
    new-instance v0, Lae/y;

    iget v1, p0, Lae/y;->c:I

    iget-object v2, p0, Lae/y;->d:Ljava/lang/String;

    iget-object v3, p0, Lae/y;->e:Lau/B;

    iget-object v4, p0, Lae/y;->f:Ljava/lang/String;

    iget-object v5, p0, Lae/y;->g:Ljava/lang/String;

    iget-object v6, p0, Lae/y;->h:Ljava/lang/String;

    iget-object v7, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    move-object v8, p1

    move-object v10, v9

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lau/B;Ljava/lang/String;Ljava/lang/String;Lo/B;)Lae/y;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 127
    new-instance v0, Lae/y;

    const/4 v1, 0x1

    move-object v2, p2

    move-object v3, p0

    move-object v5, p1

    move-object v6, v4

    move-object v7, v4

    move-object v8, p3

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lau/B;Ljava/lang/String;Lo/B;)Lae/y;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 118
    new-instance v0, Lae/y;

    const/4 v1, 0x1

    move-object v3, p0

    move-object v4, v2

    move-object v5, p1

    move-object v6, v2

    move-object v7, v2

    move-object v8, p2

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lau/B;Lo/B;)Lae/y;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lae/y;->a(Lau/B;Ljava/lang/String;Lo/B;)Lae/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;)Lae/y;
    .registers 2
    .parameter

    .prologue
    .line 184
    const/4 v0, 0x7

    invoke-static {v0, p0}, Lae/y;->a(ILcom/google/googlenav/ai;)Lae/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;II)Lae/y;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 237
    const/4 v0, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, p0, v1, v2}, Lae/y;->a(ILcom/google/googlenav/ai;Ljava/lang/Integer;Ljava/lang/Integer;)Lae/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lae/y;
    .registers 2
    .parameter

    .prologue
    .line 318
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lae/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lae/y;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lae/y;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x7

    const/4 v1, 0x3

    const/4 v5, 0x0

    .line 203
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 204
    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 205
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v3

    .line 207
    const/16 v0, 0xf

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 211
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 212
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v7

    .line 215
    :goto_2b
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 216
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lo/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/B;

    move-result-object v8

    .line 218
    :goto_39
    new-instance v0, Lae/y;

    move-object v9, v5

    move-object v10, v5

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_41
    move-object v8, v5

    goto :goto_39

    :cond_43
    move-object v7, v5

    goto :goto_2b
.end method

.method public static a(Lcom/google/googlenav/friend/aI;)Lae/y;
    .registers 12
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 197
    new-instance v0, Lae/y;

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->d()Lau/B;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/googlenav/friend/aI;->e()Lo/B;

    move-result-object v8

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lae/y;
    .registers 12
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 99
    new-instance v0, Lae/y;

    const/4 v1, 0x0

    move-object v2, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    move-object v10, v3

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lau/B;)Lae/y;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 259
    new-instance v0, Lae/y;

    const/4 v1, 0x5

    move-object v2, p0

    move-object v3, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/B;)Lae/y;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 174
    new-instance v0, Lae/y;

    const/4 v1, 0x3

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v8, p5

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Lae/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 558
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 560
    if-nez p0, :cond_a

    .line 584
    :goto_9
    return-object v0

    .line 564
    :cond_a
    invoke-virtual {p0}, Lae/y;->f()Lau/B;

    move-result-object v1

    if-eqz v1, :cond_1c

    .line 565
    const/4 v1, 0x2

    invoke-virtual {p0}, Lae/y;->f()Lau/B;

    move-result-object v2

    invoke-static {v2}, Lau/C;->d(Lau/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 570
    :cond_1c
    invoke-virtual {p0}, Lae/y;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2e

    .line 571
    const/4 v1, 0x1

    invoke-virtual {p0}, Lae/y;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 574
    :cond_2e
    invoke-virtual {p0}, Lae/y;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_40

    .line 575
    const/4 v1, 0x3

    invoke-virtual {p0}, Lae/y;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 578
    :cond_40
    invoke-virtual {p0}, Lae/y;->l()Lo/B;

    move-result-object v1

    if-eqz v1, :cond_53

    .line 579
    const/16 v1, 0x8

    invoke-virtual {p0}, Lae/y;->l()Lo/B;

    move-result-object v2

    invoke-virtual {v2}, Lo/B;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 582
    :cond_53
    const/16 v1, 0x9

    iget v2, p0, Lae/y;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_9
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 476
    if-nez p0, :cond_6

    if-nez p1, :cond_6

    .line 477
    const/4 v0, 0x1

    .line 484
    :goto_5
    return v0

    .line 480
    :cond_6
    if-nez p0, :cond_a

    .line 481
    const/4 v0, 0x0

    goto :goto_5

    .line 484
    :cond_a
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public static b(Lau/B;Lo/B;)Lae/y;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 138
    new-instance v0, Lae/y;

    const/4 v1, 0x2

    move-object v3, p0

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, p1

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static b(Lcom/google/googlenav/ai;)Lae/y;
    .registers 2
    .parameter

    .prologue
    .line 228
    const/4 v0, 0x3

    invoke-static {v0, p0}, Lae/y;->a(ILcom/google/googlenav/ai;)Lae/y;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lae/y;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x3

    const/4 v6, 0x0

    .line 290
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 293
    if-eqz p0, :cond_4f

    .line 294
    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 295
    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 296
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 297
    new-instance v7, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/google/googlenav/ai;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 300
    :goto_25
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 301
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 303
    invoke-virtual {p1, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v3

    .line 306
    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 307
    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lo/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/B;

    move-result-object v8

    .line 310
    :goto_43
    new-instance v0, Lae/y;

    move-object v9, v6

    move-object v10, v6

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_4b
    move-object v8, v6

    goto :goto_43

    :cond_4d
    move-object v7, v6

    goto :goto_25

    :cond_4f
    move-object v7, v6

    move-object v5, v6

    goto :goto_25
.end method

.method public static c(Lcom/google/googlenav/ai;)Lae/y;
    .registers 2
    .parameter

    .prologue
    .line 255
    const/4 v0, 0x4

    invoke-static {v0, p0}, Lae/y;->a(ILcom/google/googlenav/ai;)Lae/y;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lau/B;)Lae/y;
    .registers 13
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 322
    new-instance v0, Lae/y;

    iget v1, p0, Lae/y;->c:I

    iget-object v2, p0, Lae/y;->d:Ljava/lang/String;

    iget-object v4, p0, Lae/y;->f:Ljava/lang/String;

    iget-object v5, p0, Lae/y;->g:Ljava/lang/String;

    iget-object v6, p0, Lae/y;->h:Ljava/lang/String;

    iget-object v8, p0, Lae/y;->j:Lo/B;

    move-object v3, p1

    move-object v9, v7

    move-object v10, v7

    invoke-direct/range {v0 .. v10}, Lae/y;-><init>(ILjava/lang/String;Lau/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ai;Lo/B;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 447
    if-ne p0, p1, :cond_5

    .line 468
    :cond_4
    :goto_4
    return v0

    .line 451
    :cond_5
    instance-of v2, p1, Lae/y;

    if-nez v2, :cond_b

    move v0, v1

    .line 452
    goto :goto_4

    .line 455
    :cond_b
    check-cast p1, Lae/y;

    .line 459
    iget-object v2, p0, Lae/y;->j:Lo/B;

    iget-object v3, p1, Lae/y;->j:Lo/B;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    move v0, v1

    .line 460
    goto :goto_4

    .line 464
    :cond_19
    iget-object v2, p0, Lae/y;->e:Lau/B;

    if-eqz v2, :cond_31

    iget-object v2, p1, Lae/y;->e:Lau/B;

    if-eqz v2, :cond_31

    .line 465
    iget-object v2, p0, Lae/y;->e:Lau/B;

    iget-object v3, p1, Lae/y;->e:Lau/B;

    invoke-virtual {v2, v3}, Lau/B;->a(Lau/B;)J

    move-result-wide v2

    const-wide/16 v4, 0x19

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    move v0, v1

    goto :goto_4

    .line 468
    :cond_31
    iget-object v0, p0, Lae/y;->d:Ljava/lang/String;

    iget-object v1, p1, Lae/y;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 331
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 333
    const/4 v1, 0x1

    iget v2, p0, Lae/y;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 334
    iget-object v1, p0, Lae/y;->g:Ljava/lang/String;

    invoke-static {v1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 335
    const/4 v1, 0x2

    iget-object v2, p0, Lae/y;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 337
    :cond_1b
    iget-object v1, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    if-eqz v1, :cond_31

    iget-object v1, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bK()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 338
    const/4 v1, 0x3

    iget-object v2, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 340
    :cond_31
    return-object v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 351
    invoke-virtual {p0}, Lae/y;->p()Z

    move-result v0

    if-nez v0, :cond_1a

    invoke-virtual {p0}, Lae/y;->q()Z

    move-result v0

    if-nez v0, :cond_1a

    invoke-virtual {p0}, Lae/y;->u()Z

    move-result v0

    if-nez v0, :cond_1a

    invoke-virtual {p0}, Lae/y;->r()Z

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    .line 360
    :goto_19
    return v0

    .line 351
    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public d()I
    .registers 2

    .prologue
    .line 364
    iget v0, p0, Lae/y;->c:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 368
    iget-object v0, p0, Lae/y;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 489
    if-ne p0, p1, :cond_5

    .line 499
    :cond_4
    :goto_4
    return v0

    .line 493
    :cond_5
    instance-of v2, p1, Lae/y;

    if-nez v2, :cond_b

    move v0, v1

    .line 494
    goto :goto_4

    .line 497
    :cond_b
    check-cast p1, Lae/y;

    .line 499
    iget v2, p0, Lae/y;->c:I

    iget v3, p1, Lae/y;->c:I

    if-ne v2, v3, :cond_59

    iget-object v2, p0, Lae/y;->d:Ljava/lang/String;

    iget-object v3, p1, Lae/y;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_59

    iget-object v2, p0, Lae/y;->e:Lau/B;

    iget-object v3, p1, Lae/y;->e:Lau/B;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_59

    iget-object v2, p0, Lae/y;->f:Ljava/lang/String;

    iget-object v3, p1, Lae/y;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_59

    iget-object v2, p0, Lae/y;->g:Ljava/lang/String;

    iget-object v3, p1, Lae/y;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_59

    iget-object v2, p0, Lae/y;->h:Ljava/lang/String;

    iget-object v3, p1, Lae/y;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_59

    iget-object v2, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    iget-object v3, p1, Lae/y;->i:Lcom/google/googlenav/ai;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_59

    iget-object v2, p0, Lae/y;->j:Lo/B;

    iget-object v3, p1, Lae/y;->j:Lo/B;

    invoke-static {v2, v3}, Lae/y;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_59
    move v0, v1

    goto :goto_4
.end method

.method public f()Lau/B;
    .registers 2

    .prologue
    .line 372
    iget-object v0, p0, Lae/y;->e:Lau/B;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 376
    iget-object v0, p0, Lae/y;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 380
    iget-object v0, p0, Lae/y;->g:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 511
    .line 512
    iget v0, p0, Lae/y;->c:I

    add-int/lit8 v0, v0, 0x1f

    .line 513
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lae/y;->d:Ljava/lang/String;

    if-nez v0, :cond_3d

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 514
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lae/y;->e:Lau/B;

    if-nez v0, :cond_44

    move v0, v1

    :goto_14
    add-int/2addr v0, v2

    .line 515
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lae/y;->f:Ljava/lang/String;

    if-nez v0, :cond_4b

    move v0, v1

    :goto_1c
    add-int/2addr v0, v2

    .line 516
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lae/y;->g:Ljava/lang/String;

    if-nez v0, :cond_52

    move v0, v1

    :goto_24
    add-int/2addr v0, v2

    .line 517
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lae/y;->h:Ljava/lang/String;

    if-nez v0, :cond_59

    move v0, v1

    :goto_2c
    add-int/2addr v0, v2

    .line 519
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    if-nez v0, :cond_60

    move v0, v1

    :goto_34
    add-int/2addr v0, v2

    .line 520
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lae/y;->j:Lo/B;

    if-nez v2, :cond_67

    :goto_3b
    add-int/2addr v0, v1

    .line 521
    return v0

    .line 513
    :cond_3d
    iget-object v0, p0, Lae/y;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_c

    .line 514
    :cond_44
    iget-object v0, p0, Lae/y;->e:Lau/B;

    invoke-virtual {v0}, Lau/B;->hashCode()I

    move-result v0

    goto :goto_14

    .line 515
    :cond_4b
    iget-object v0, p0, Lae/y;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1c

    .line 516
    :cond_52
    iget-object v0, p0, Lae/y;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_24

    .line 517
    :cond_59
    iget-object v0, p0, Lae/y;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2c

    .line 519
    :cond_60
    iget-object v0, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_34

    .line 520
    :cond_67
    iget-object v1, p0, Lae/y;->j:Lo/B;

    invoke-virtual {v1}, Lo/B;->hashCode()I

    move-result v1

    goto :goto_3b
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 384
    iget-object v0, p0, Lae/y;->h:Ljava/lang/String;

    return-object v0
.end method

.method public j()Lcom/google/googlenav/ai;
    .registers 2

    .prologue
    .line 388
    iget-object v0, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 392
    invoke-virtual {p0}, Lae/y;->j()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bQ;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Lo/B;
    .registers 2

    .prologue
    .line 396
    iget-object v0, p0, Lae/y;->j:Lo/B;

    return-object v0
.end method

.method public m()Ljava/lang/Integer;
    .registers 2

    .prologue
    .line 400
    iget-object v0, p0, Lae/y;->k:Ljava/lang/Integer;

    return-object v0
.end method

.method public n()Ljava/lang/Integer;
    .registers 2

    .prologue
    .line 404
    iget-object v0, p0, Lae/y;->l:Ljava/lang/Integer;

    return-object v0
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 408
    iget v0, p0, Lae/y;->c:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public p()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 412
    iget v1, p0, Lae/y;->c:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public q()Z
    .registers 3

    .prologue
    .line 416
    iget v0, p0, Lae/y;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public r()Z
    .registers 3

    .prologue
    .line 424
    iget v0, p0, Lae/y;->c:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public s()Z
    .registers 3

    .prologue
    .line 428
    iget v0, p0, Lae/y;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public t()Z
    .registers 3

    .prologue
    .line 432
    iget v0, p0, Lae/y;->c:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 526
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 527
    const-string v1, "Waypoint {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "source="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lae/y;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    iget-object v1, p0, Lae/y;->d:Ljava/lang/String;

    if-eqz v1, :cond_44

    .line 530
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", query=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lae/y;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 532
    :cond_44
    iget-object v1, p0, Lae/y;->e:Lau/B;

    if-eqz v1, :cond_60

    .line 533
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", coordinates="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lae/y;->e:Lau/B;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 535
    :cond_60
    iget-object v1, p0, Lae/y;->f:Ljava/lang/String;

    if-eqz v1, :cond_82

    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", fingerprint=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lae/y;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    :cond_82
    iget-object v1, p0, Lae/y;->g:Ljava/lang/String;

    if-eqz v1, :cond_a4

    .line 539
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", name=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lae/y;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    :cond_a4
    iget-object v1, p0, Lae/y;->h:Ljava/lang/String;

    if-eqz v1, :cond_c6

    .line 542
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", streetViewPanoramaId=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lae/y;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    :cond_c6
    iget-object v1, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    if-eqz v1, :cond_e8

    .line 545
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", placemark=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lae/y;->i:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    :cond_e8
    iget-object v1, p0, Lae/y;->j:Lo/B;

    if-eqz v1, :cond_10a

    .line 548
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", level=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lae/y;->j:Lo/B;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    :cond_10a
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .registers 3

    .prologue
    .line 436
    iget v0, p0, Lae/y;->c:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public v()Ljava/lang/String;
    .registers 4

    .prologue
    .line 591
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 592
    invoke-virtual {p0}, Lae/y;->j()Lcom/google/googlenav/ai;

    move-result-object v1

    .line 593
    if-eqz v1, :cond_27

    .line 594
    invoke-virtual {v1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    .line 595
    if-eqz v2, :cond_1f

    .line 596
    invoke-virtual {v1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 603
    :goto_1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 598
    :cond_1f
    invoke-virtual {p0}, Lae/y;->f()Lau/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1a

    .line 601
    :cond_27
    invoke-virtual {p0}, Lae/y;->f()Lau/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1a
.end method
