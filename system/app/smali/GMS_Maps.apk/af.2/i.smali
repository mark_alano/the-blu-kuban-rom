.class public final Laf/i;
.super LbH/k;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:J

.field private c:Laf/d;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 411
    invoke-direct {p0}, LbH/k;-><init>()V

    .line 563
    invoke-static {}, Laf/d;->a()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/i;->c:Laf/d;

    .line 624
    const-string v0, ""

    iput-object v0, p0, Laf/i;->d:Ljava/lang/Object;

    .line 698
    const-string v0, ""

    iput-object v0, p0, Laf/i;->e:Ljava/lang/Object;

    .line 772
    const-string v0, ""

    iput-object v0, p0, Laf/i;->f:Ljava/lang/Object;

    .line 412
    invoke-direct {p0}, Laf/i;->h()V

    .line 413
    return-void
.end method

.method static synthetic g()Laf/i;
    .registers 1

    .prologue
    .line 406
    invoke-static {}, Laf/i;->i()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .registers 1

    .prologue
    .line 416
    return-void
.end method

.method private static i()Laf/i;
    .registers 1

    .prologue
    .line 418
    new-instance v0, Laf/i;

    invoke-direct {v0}, Laf/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Laf/i;
    .registers 3

    .prologue
    .line 437
    invoke-static {}, Laf/i;->i()Laf/i;

    move-result-object v0

    invoke-virtual {p0}, Laf/i;->c()Laf/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Laf/i;->a(Laf/g;)Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Laf/i;
    .registers 4
    .parameter

    .prologue
    .line 547
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laf/i;->a:I

    .line 548
    iput-wide p1, p0, Laf/i;->b:J

    .line 550
    return-object p0
.end method

.method public a(Laf/d;)Laf/i;
    .registers 4
    .parameter

    .prologue
    .line 602
    iget v0, p0, Laf/i;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_26

    iget-object v0, p0, Laf/i;->c:Laf/d;

    invoke-static {}, Laf/d;->a()Laf/d;

    move-result-object v1

    if-eq v0, v1, :cond_26

    .line 604
    iget-object v0, p0, Laf/i;->c:Laf/d;

    invoke-static {v0}, Laf/d;->a(Laf/d;)Laf/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Laf/f;->a(Laf/d;)Laf/f;

    move-result-object v0

    invoke-virtual {v0}, Laf/f;->c()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/i;->c:Laf/d;

    .line 610
    :goto_1f
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/i;->a:I

    .line 611
    return-object p0

    .line 607
    :cond_26
    iput-object p1, p0, Laf/i;->c:Laf/d;

    goto :goto_1f
.end method

.method public a(Laf/f;)Laf/i;
    .registers 3
    .parameter

    .prologue
    .line 593
    invoke-virtual {p1}, Laf/f;->b()Laf/d;

    move-result-object v0

    iput-object v0, p0, Laf/i;->c:Laf/d;

    .line 595
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/i;->a:I

    .line 596
    return-object p0
.end method

.method public a(Laf/g;)Laf/i;
    .registers 4
    .parameter

    .prologue
    .line 481
    invoke-static {}, Laf/g;->a()Laf/g;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 503
    :cond_6
    :goto_6
    return-object p0

    .line 482
    :cond_7
    invoke-virtual {p1}, Laf/g;->b()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 483
    invoke-virtual {p1}, Laf/g;->c()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Laf/i;->a(J)Laf/i;

    .line 485
    :cond_14
    invoke-virtual {p1}, Laf/g;->d()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 486
    invoke-virtual {p1}, Laf/g;->e()Laf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Laf/i;->a(Laf/d;)Laf/i;

    .line 488
    :cond_21
    invoke-virtual {p1}, Laf/g;->f()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 489
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laf/i;->a:I

    .line 490
    invoke-static {p1}, Laf/g;->b(Laf/g;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/i;->d:Ljava/lang/Object;

    .line 493
    :cond_33
    invoke-virtual {p1}, Laf/g;->i()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 494
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laf/i;->a:I

    .line 495
    invoke-static {p1}, Laf/g;->c(Laf/g;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/i;->e:Ljava/lang/Object;

    .line 498
    :cond_45
    invoke-virtual {p1}, Laf/g;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 499
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laf/i;->a:I

    .line 500
    invoke-static {p1}, Laf/g;->d(Laf/g;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/i;->f:Ljava/lang/Object;

    goto :goto_6
.end method

.method public a(LbH/f;LbH/i;)Laf/i;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 514
    const/4 v2, 0x0

    .line 516
    :try_start_1
    sget-object v0, Laf/g;->a:LbH/r;

    invoke-interface {v0, p1, p2}, LbH/r;->b(LbH/f;LbH/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/g;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch LbH/l; {:try_start_1 .. :try_end_9} :catch_f

    .line 521
    if-eqz v0, :cond_e

    .line 522
    invoke-virtual {p0, v0}, Laf/i;->a(Laf/g;)Laf/i;

    .line 525
    :cond_e
    return-object p0

    .line 517
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 518
    :try_start_11
    invoke-virtual {v1}, LbH/l;->a()LbH/p;

    move-result-object v0

    check-cast v0, Laf/g;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 519
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 521
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 522
    invoke-virtual {p0, v1}, Laf/i;->a(Laf/g;)Laf/i;

    :cond_21
    throw v0

    .line 521
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method public a(Ljava/lang/String;)Laf/i;
    .registers 3
    .parameter

    .prologue
    .line 666
    if-nez p1, :cond_8

    .line 667
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 669
    :cond_8
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laf/i;->a:I

    .line 670
    iput-object p1, p0, Laf/i;->d:Ljava/lang/Object;

    .line 672
    return-object p0
.end method

.method public b()Laf/g;
    .registers 3

    .prologue
    .line 445
    invoke-virtual {p0}, Laf/i;->c()Laf/g;

    move-result-object v0

    .line 446
    invoke-virtual {v0}, Laf/g;->n()Z

    move-result v1

    if-nez v1, :cond_f

    .line 447
    invoke-static {v0}, Laf/i;->a(LbH/p;)LbH/x;

    move-result-object v0

    throw v0

    .line 449
    :cond_f
    return-object v0
.end method

.method public b(Ljava/lang/String;)Laf/i;
    .registers 3
    .parameter

    .prologue
    .line 740
    if-nez p1, :cond_8

    .line 741
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 743
    :cond_8
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laf/i;->a:I

    .line 744
    iput-object p1, p0, Laf/i;->e:Ljava/lang/Object;

    .line 746
    return-object p0
.end method

.method public synthetic b(LbH/f;LbH/i;)LbH/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 406
    invoke-virtual {p0, p1, p2}, Laf/i;->a(LbH/f;LbH/i;)Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public c()Laf/g;
    .registers 7

    .prologue
    const/4 v0, 0x1

    .line 453
    new-instance v2, Laf/g;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Laf/g;-><init>(LbH/k;Laf/h;)V

    .line 454
    iget v3, p0, Laf/i;->a:I

    .line 455
    const/4 v1, 0x0

    .line 456
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_49

    .line 459
    :goto_e
    iget-wide v4, p0, Laf/i;->b:J

    invoke-static {v2, v4, v5}, Laf/g;->a(Laf/g;J)J

    .line 460
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1a

    .line 461
    or-int/lit8 v0, v0, 0x2

    .line 463
    :cond_1a
    iget-object v1, p0, Laf/i;->c:Laf/d;

    invoke-static {v2, v1}, Laf/g;->a(Laf/g;Laf/d;)Laf/d;

    .line 464
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_26

    .line 465
    or-int/lit8 v0, v0, 0x4

    .line 467
    :cond_26
    iget-object v1, p0, Laf/i;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/g;->a(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_33

    .line 469
    or-int/lit8 v0, v0, 0x8

    .line 471
    :cond_33
    iget-object v1, p0, Laf/i;->e:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/g;->b(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_40

    .line 473
    or-int/lit8 v0, v0, 0x10

    .line 475
    :cond_40
    iget-object v1, p0, Laf/i;->f:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/g;->c(Laf/g;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    invoke-static {v2, v0}, Laf/g;->a(Laf/g;I)I

    .line 477
    return-object v2

    :cond_49
    move v0, v1

    goto :goto_e
.end method

.method public c(Ljava/lang/String;)Laf/i;
    .registers 3
    .parameter

    .prologue
    .line 814
    if-nez p1, :cond_8

    .line 815
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 817
    :cond_8
    iget v0, p0, Laf/i;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laf/i;->a:I

    .line 818
    iput-object p1, p0, Laf/i;->f:Ljava/lang/Object;

    .line 820
    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 406
    invoke-virtual {p0}, Laf/i;->a()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()LbH/k;
    .registers 2

    .prologue
    .line 406
    invoke-virtual {p0}, Laf/i;->a()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()LbH/b;
    .registers 2

    .prologue
    .line 406
    invoke-virtual {p0}, Laf/i;->a()Laf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LbH/p;
    .registers 2

    .prologue
    .line 406
    invoke-virtual {p0}, Laf/i;->b()Laf/g;

    move-result-object v0

    return-object v0
.end method
