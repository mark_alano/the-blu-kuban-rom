.class public LaH/E;
.super Law/a;
.source "SourceFile"


# static fields
.field private static a:LaH/E;

.field private static j:Ljava/lang/Boolean;


# instance fields
.field private b:I

.field private c:LaN/B;

.field private d:LaN/B;

.field private e:I

.field private f:I

.field private g:[J

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Law/a;-><init>()V

    .line 82
    const/4 v0, 0x6

    new-array v0, v0, [J

    iput-object v0, p0, LaH/E;->g:[J

    .line 84
    iput v1, p0, LaH/E;->h:I

    .line 85
    iput v1, p0, LaH/E;->i:I

    .line 99
    invoke-virtual {p0}, LaH/E;->m()V

    .line 100
    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/j;)Lcom/google/googlenav/common/io/m;
    .registers 2
    .parameter

    .prologue
    .line 135
    instance-of v0, p0, Lcom/google/googlenav/common/io/n;

    if-eqz v0, :cond_d

    .line 136
    check-cast p0, Lcom/google/googlenav/common/io/n;

    const-string v0, "savedLocationShiftCoefficients_lock"

    invoke-interface {p0, v0}, Lcom/google/googlenav/common/io/n;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/m;

    move-result-object v0

    .line 138
    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private static a(Lcom/google/googlenav/common/io/m;)V
    .registers 1
    .parameter

    .prologue
    .line 142
    if-eqz p0, :cond_5

    .line 143
    invoke-interface {p0}, Lcom/google/googlenav/common/io/m;->a()Z

    .line 145
    :cond_5
    return-void
.end method

.method private a(LaN/B;I)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const-wide/32 v4, 0x15752a00

    const/4 v0, 0x0

    .line 267
    iget-object v1, p0, LaH/E;->c:LaN/B;

    if-nez v1, :cond_9

    .line 279
    :cond_8
    :goto_8
    return v0

    .line 270
    :cond_9
    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    iget-object v2, p0, LaH/E;->c:LaN/B;

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, p2, :cond_8

    .line 274
    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    iget-object v2, p0, LaH/E;->c:LaN/B;

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    sub-int/2addr v1, v2

    .line 275
    :goto_25
    if-gez v1, :cond_2b

    .line 276
    int-to-long v1, v1

    add-long/2addr v1, v4

    long-to-int v1, v1

    goto :goto_25

    .line 278
    :cond_2b
    int-to-long v2, v1

    sub-long v2, v4, v2

    long-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 279
    if-ge v1, p2, :cond_8

    const/4 v0, 0x1

    goto :goto_8
.end method

.method public static e(LaN/B;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 374
    sget-object v2, LaH/E;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 375
    sget-object v0, LaH/E;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 394
    :cond_c
    :goto_c
    return v0

    .line 378
    :cond_d
    if-eqz p0, :cond_c

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v2

    const v3, 0x2dc6c0

    if-lt v2, v3, :cond_c

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v2

    const v3, 0x337f980

    if-gt v2, v3, :cond_c

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v2

    const v3, 0x44aa200

    if-lt v2, v3, :cond_c

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v2

    const v3, 0x81b3200

    if-gt v2, v3, :cond_c

    .line 389
    invoke-static {}, LaI/b;->c()I

    move-result v2

    .line 390
    const/4 v3, -0x1

    if-eq v2, v3, :cond_44

    .line 391
    const/16 v3, 0x1cc

    if-eq v2, v3, :cond_42

    const/16 v3, 0x460

    if-ne v2, v3, :cond_c

    :cond_42
    move v0, v1

    goto :goto_c

    :cond_44
    move v0, v1

    .line 394
    goto :goto_c
.end method

.method public static k()LaH/E;
    .registers 1

    .prologue
    .line 108
    sget-object v0, LaH/E;->a:LaH/E;

    if-nez v0, :cond_7

    .line 109
    invoke-static {}, LaH/E;->p()V

    .line 111
    :cond_7
    sget-object v0, LaH/E;->a:LaH/E;

    return-object v0
.end method

.method private static declared-synchronized p()V
    .registers 2

    .prologue
    .line 103
    const-class v1, LaH/E;

    monitor-enter v1

    :try_start_3
    new-instance v0, LaH/E;

    invoke-direct {v0}, LaH/E;-><init>()V

    sput-object v0, LaH/E;->a:LaH/E;
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_c

    .line 104
    monitor-exit v1

    return-void

    .line 103
    :catchall_c
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 5
    .parameter

    .prologue
    .line 221
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eo;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 223
    const/4 v1, 0x2

    iget-object v2, p0, LaH/E;->d:LaN/B;

    invoke-virtual {v2}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 225
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 228
    return-void
.end method

.method public a(LaN/B;)Z
    .registers 3
    .parameter

    .prologue
    .line 288
    iget v0, p0, LaH/E;->e:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, v0}, LaH/E;->a(LaN/B;I)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 232
    if-nez p1, :cond_5

    .line 259
    :cond_4
    :goto_4
    return v1

    .line 235
    :cond_5
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/eo;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 237
    if-eqz v3, :cond_4

    .line 240
    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, LaH/E;->b:I

    .line 241
    iget v2, p0, LaH/E;->b:I

    if-nez v2, :cond_4

    move v2, v1

    .line 244
    :goto_18
    const/4 v4, 0x6

    if-ge v2, v4, :cond_27

    .line 245
    iget-object v4, p0, LaH/E;->g:[J

    const/4 v5, 0x2

    invoke-virtual {v3, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v5

    aput-wide v5, v4, v2

    .line 244
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 248
    :cond_27
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, LaH/E;->f:I

    .line 250
    const/4 v2, 0x5

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, LaH/E;->e:I

    .line 252
    const/4 v2, 0x3

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, LaN/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v2

    iput-object v2, p0, LaH/E;->c:LaN/B;

    .line 254
    iget v2, p0, LaH/E;->b:I

    if-nez v2, :cond_47

    .line 255
    invoke-virtual {p0}, LaH/E;->o()V

    .line 259
    :cond_47
    iget v2, p0, LaH/E;->b:I

    if-nez v2, :cond_4d

    :goto_4b
    move v1, v0

    goto :goto_4

    :cond_4d
    move v0, v1

    goto :goto_4b
.end method

.method public b()I
    .registers 2

    .prologue
    .line 211
    const/16 v0, 0x35

    return v0
.end method

.method public b(LaN/B;)Z
    .registers 3
    .parameter

    .prologue
    .line 297
    iget v0, p0, LaH/E;->f:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, v0}, LaH/E;->a(LaN/B;I)Z

    move-result v0

    return v0
.end method

.method public c(LaN/B;)V
    .registers 3
    .parameter

    .prologue
    .line 305
    if-nez p1, :cond_3

    .line 319
    :cond_2
    :goto_2
    return-void

    .line 309
    :cond_3
    iget-object v0, p0, LaH/E;->c:LaN/B;

    invoke-virtual {p1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LaH/E;->d:LaN/B;

    invoke-virtual {p1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 311
    iput-object p1, p0, LaH/E;->d:LaN/B;

    .line 312
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_2

    .line 316
    invoke-virtual {v0, p0}, Law/h;->c(Law/g;)V

    goto :goto_2
.end method

.method public d(LaN/B;)LaN/B;
    .registers 12
    .parameter

    .prologue
    const-wide/32 v8, 0xf4240

    .line 342
    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    iget-object v2, p0, LaH/E;->g:[J

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iget-object v2, p0, LaH/E;->g:[J

    const/4 v3, 0x2

    aget-wide v2, v2, v3

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    div-long/2addr v0, v8

    .line 344
    iget-object v2, p0, LaH/E;->g:[J

    const/4 v3, 0x3

    aget-wide v2, v2, v3

    iget-object v4, p0, LaH/E;->g:[J

    const/4 v5, 0x4

    aget-wide v4, v4, v5

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v4, p0, LaH/E;->g:[J

    const/4 v5, 0x5

    aget-wide v4, v4, v5

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    div-long/2addr v2, v8

    .line 346
    invoke-virtual {p0, p1}, LaH/E;->a(LaN/B;)Z

    move-result v4

    if-eqz v4, :cond_48

    .line 347
    invoke-virtual {p0, p1}, LaH/E;->c(LaN/B;)V

    .line 349
    :cond_48
    long-to-int v4, v0

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, LaH/E;->h:I

    .line 350
    long-to-int v4, v2

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, LaH/E;->i:I

    .line 351
    new-instance v4, LaN/B;

    long-to-int v0, v0

    long-to-int v1, v2

    invoke-direct {v4, v0, v1}, LaN/B;-><init>(II)V

    return-object v4
.end method

.method public l()V
    .registers 9

    .prologue
    const-wide/32 v6, 0xf4240

    const/4 v5, 0x1

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 119
    iget-object v0, p0, LaH/E;->g:[J

    aput-wide v3, v0, v2

    .line 120
    iget-object v0, p0, LaH/E;->g:[J

    aput-wide v6, v0, v5

    .line 121
    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x2

    aput-wide v3, v0, v1

    .line 122
    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x3

    aput-wide v3, v0, v1

    .line 123
    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x4

    aput-wide v3, v0, v1

    .line 124
    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x5

    aput-wide v6, v0, v1

    .line 125
    iput v2, p0, LaH/E;->e:I

    .line 126
    iput v2, p0, LaH/E;->f:I

    .line 127
    iput v2, p0, LaH/E;->h:I

    .line 128
    iput v2, p0, LaH/E;->i:I

    .line 129
    iput v5, p0, LaH/E;->b:I

    .line 130
    return-void
.end method

.method public declared-synchronized m()V
    .registers 5

    .prologue
    .line 152
    monitor-enter p0

    :try_start_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    .line 153
    invoke-static {v1}, LaH/E;->a(Lcom/google/googlenav/common/io/j;)Lcom/google/googlenav/common/io/m;

    move-result-object v2

    .line 154
    const-string v0, "savedLocationShiftCoefficients"

    invoke-static {v1, v0}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_2c

    move-result-object v0

    .line 157
    :try_start_13
    invoke-virtual {p0, v0}, LaH/E;->a(Ljava/io/DataInput;)Z
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_2f
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_16} :catch_21

    move-result v0

    .line 164
    :try_start_17
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V

    .line 166
    :goto_1a
    if-nez v0, :cond_1f

    .line 167
    invoke-virtual {p0}, LaH/E;->l()V
    :try_end_1f
    .catchall {:try_start_17 .. :try_end_1f} :catchall_2c

    .line 169
    :cond_1f
    monitor-exit p0

    return-void

    .line 158
    :catch_21
    move-exception v0

    .line 159
    const/4 v0, 0x0

    .line 162
    :try_start_23
    const-string v3, "savedLocationShiftCoefficients"

    invoke-interface {v1, v3}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_28
    .catchall {:try_start_23 .. :try_end_28} :catchall_2f

    .line 164
    :try_start_28
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V
    :try_end_2b
    .catchall {:try_start_28 .. :try_end_2b} :catchall_2c

    goto :goto_1a

    .line 152
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164
    :catchall_2f
    move-exception v0

    :try_start_30
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V

    throw v0
    :try_end_34
    .catchall {:try_start_30 .. :try_end_34} :catchall_2c
.end method

.method public n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6

    .prologue
    .line 175
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eo;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 177
    const/4 v0, 0x1

    iget v2, p0, LaH/E;->b:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 178
    const/4 v0, 0x0

    :goto_e
    const/4 v2, 0x6

    if-ge v0, v2, :cond_1c

    .line 179
    const/4 v2, 0x2

    iget-object v3, p0, LaH/E;->g:[J

    aget-wide v3, v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 181
    :cond_1c
    const/4 v0, 0x3

    iget-object v2, p0, LaH/E;->c:LaN/B;

    invoke-virtual {v2}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 183
    const/4 v0, 0x4

    iget v2, p0, LaH/E;->f:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 185
    const/4 v0, 0x5

    iget v2, p0, LaH/E;->e:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 187
    return-object v1
.end method

.method public declared-synchronized o()V
    .registers 6

    .prologue
    .line 192
    monitor-enter p0

    :try_start_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    .line 193
    invoke-static {v1}, LaH/E;->a(Lcom/google/googlenav/common/io/j;)Lcom/google/googlenav/common/io/m;

    move-result-object v2

    .line 194
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 195
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_38

    .line 197
    :try_start_17
    invoke-virtual {p0}, LaH/E;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 198
    check-cast v0, Ljava/io/DataOutputStream;

    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 199
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v3, "savedLocationShiftCoefficients"

    invoke-interface {v1, v0, v3}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_29
    .catchall {:try_start_17 .. :try_end_29} :catchall_3b
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_29} :catch_2e

    .line 205
    :try_start_29
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_38

    .line 207
    :goto_2c
    monitor-exit p0

    return-void

    .line 200
    :catch_2e
    move-exception v0

    .line 203
    :try_start_2f
    const-string v0, "savedLocationShiftCoefficients"

    invoke-interface {v1, v0}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_34
    .catchall {:try_start_2f .. :try_end_34} :catchall_3b

    .line 205
    :try_start_34
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    goto :goto_2c

    .line 192
    :catchall_38
    move-exception v0

    monitor-exit p0

    throw v0

    .line 205
    :catchall_3b
    move-exception v0

    :try_start_3c
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V

    throw v0
    :try_end_40
    .catchall {:try_start_3c .. :try_end_40} :catchall_38
.end method

.method public t_()Z
    .registers 2

    .prologue
    .line 216
    const/4 v0, 0x0

    return v0
.end method
