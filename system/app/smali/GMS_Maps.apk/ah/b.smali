.class public abstract LaH/b;
.super LaH/c;
.source "SourceFile"


# instance fields
.field private final a:LaH/C;

.field protected final b:Landroid/location/LocationManager;


# direct methods
.method protected constructor <init>(Z)V
    .registers 4
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1}, LaH/c;-><init>(Z)V

    .line 45
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/location/LocationManager;

    move-result-object v0

    iput-object v0, p0, LaH/b;->b:Landroid/location/LocationManager;

    .line 50
    sget-boolean v0, Lcom/google/googlenav/android/E;->i:Z

    if-eqz v0, :cond_1b

    new-instance v0, LaH/C;

    iget-object v1, p0, LaH/b;->b:Landroid/location/LocationManager;

    invoke-direct {v0, v1}, LaH/C;-><init>(Landroid/location/LocationManager;)V

    :goto_18
    iput-object v0, p0, LaH/b;->a:LaH/C;

    .line 52
    return-void

    .line 50
    :cond_1b
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, LaH/b;->a:LaH/C;

    if-eqz v0, :cond_b

    .line 87
    iget-object v0, p0, LaH/b;->a:LaH/C;

    invoke-virtual {v0, p1}, LaH/C;->a(Ljava/lang/String;)Z

    move-result v0

    .line 90
    :goto_a
    return v0

    .line 89
    :cond_b
    iget-object v0, p0, LaH/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_23

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, LaH/b;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    const/4 v0, 0x1

    goto :goto_a

    :cond_23
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected static b(Landroid/location/Location;)Lo/D;
    .registers 6
    .parameter

    .prologue
    const/high16 v4, -0x8000

    const/4 v0, 0x0

    .line 124
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->t()Z

    move-result v1

    if-nez v1, :cond_e

    .line 152
    :cond_d
    :goto_d
    return-object v0

    .line 128
    :cond_e
    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 129
    if-eqz v1, :cond_d

    .line 133
    const-string v2, "levelId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 134
    const-string v3, "levelNumberE3"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 137
    if-eqz v2, :cond_d

    .line 138
    invoke-static {v2}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v2

    .line 139
    if-eqz v2, :cond_d

    .line 140
    if-ne v1, v4, :cond_47

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing level number for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144
    const-string v3, "LOCATION"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 146
    :cond_47
    new-instance v0, Lo/D;

    invoke-direct {v0, v2, v1}, Lo/D;-><init>(Lo/r;I)V

    goto :goto_d
.end method


# virtual methods
.method public declared-synchronized f()V
    .registers 2

    .prologue
    .line 56
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, LaH/c;->f()V

    .line 58
    iget-object v0, p0, LaH/b;->a:LaH/C;

    if-eqz v0, :cond_d

    .line 59
    iget-object v0, p0, LaH/b;->a:LaH/C;

    invoke-virtual {v0}, LaH/C;->a()V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 61
    :cond_d
    monitor-exit p0

    return-void

    .line 56
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .registers 2

    .prologue
    .line 65
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaH/b;->a:LaH/C;

    if-eqz v0, :cond_a

    .line 66
    iget-object v0, p0, LaH/b;->a:LaH/C;

    invoke-virtual {v0}, LaH/C;->b()V

    .line 69
    :cond_a
    invoke-super {p0}, LaH/c;->g()V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 70
    monitor-exit p0

    return-void

    .line 65
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 79
    invoke-virtual {p0}, LaH/b;->k()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, LaH/b;->j()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 98
    :try_start_0
    const-string v0, "network"

    invoke-direct {p0, v0}, LaH/b;->a(Ljava/lang/String;)Z
    :try_end_5
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_5} :catch_7

    move-result v0

    .line 103
    :goto_6
    return v0

    .line 99
    :catch_7
    move-exception v0

    .line 103
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 110
    :try_start_0
    const-string v0, "gps"

    invoke-direct {p0, v0}, LaH/b;->a(Ljava/lang/String;)Z
    :try_end_5
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_5} :catch_7

    move-result v0

    .line 115
    :goto_6
    return v0

    .line 111
    :catch_7
    move-exception v0

    .line 115
    const/4 v0, 0x0

    goto :goto_6
.end method
