.class public abstract LaH/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/s;
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/util/concurrent/CopyOnWriteArrayList;

.field protected c:LaH/h;

.field protected d:Z

.field protected e:Z

.field protected volatile f:Z

.field protected final g:Ljava/lang/Object;

.field protected h:J


# direct methods
.method protected constructor <init>(Z)V
    .registers 4
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/c;->d:Z

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/c;->f:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaH/c;->g:Ljava/lang/Object;

    .line 59
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, LaH/c;->h:J

    .line 71
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 75
    iput-boolean p1, p0, LaH/c;->e:Z

    .line 78
    invoke-direct {p0}, LaH/c;->u()V

    .line 79
    return-void
.end method

.method private a(LaH/h;)LaH/h;
    .registers 5
    .parameter

    .prologue
    .line 324
    if-eqz p1, :cond_3c

    .line 325
    invoke-virtual {p1}, LaH/h;->d()Z

    move-result v0

    if-nez v0, :cond_21

    .line 327
    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LaH/j;->a(J)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object p1

    .line 332
    :cond_21
    invoke-virtual {p1}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3c

    .line 334
    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {p0}, LaH/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object p1

    .line 341
    :cond_3c
    iget-object v0, p0, LaH/c;->c:LaH/h;

    .line 342
    if-eqz p1, :cond_42

    .line 343
    iput-object p1, p0, LaH/c;->c:LaH/h;

    .line 345
    :cond_42
    const/4 v1, 0x1

    iput-boolean v1, p0, LaH/c;->e:Z

    .line 347
    return-object v0
.end method

.method private u()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 123
    iput-object v7, p0, LaH/c;->c:LaH/h;

    .line 125
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    invoke-direct {p0}, LaH/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_63

    .line 130
    :try_start_15
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v1

    .line 132
    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    sub-long/2addr v3, v1

    const-wide/32 v5, 0xafc80

    cmp-long v3, v3, v5

    if-gez v3, :cond_54

    .line 133
    invoke-interface {v0}, Ljava/io/DataInput;->readInt()I

    move-result v3

    .line 134
    invoke-static {v0}, LaN/B;->a(Ljava/io/DataInput;)LaN/B;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_54

    .line 136
    new-instance v4, LaH/j;

    invoke-direct {v4}, LaH/j;-><init>()V

    invoke-virtual {p0}, LaH/c;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    move-result-object v4

    invoke-virtual {v4, v0}, LaH/j;->a(LaN/B;)LaH/j;

    move-result-object v0

    int-to-float v3, v3

    invoke-virtual {v0, v3}, LaH/j;->a(F)LaH/j;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, LaH/j;->a(J)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/c;->b(LaH/h;)V
    :try_end_54
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_54} :catch_64

    .line 148
    :cond_54
    :goto_54
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-direct {p0}, LaH/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 151
    :cond_63
    return-void

    .line 144
    :catch_64
    move-exception v0

    goto :goto_54
.end method

.method private v()Ljava/lang/String;
    .registers 3

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LastLocation_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .registers 5

    .prologue
    .line 161
    invoke-virtual {p0}, LaH/c;->q()Z

    move-result v0

    if-nez v0, :cond_7

    .line 176
    :goto_6
    return-void

    .line 165
    :cond_7
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 166
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 168
    :try_start_11
    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Ljava/io/DataOutput;->writeLong(J)V

    .line 169
    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 170
    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v2, v1}, LaN/B;->a(LaN/B;Ljava/io/DataOutput;)V

    .line 171
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-direct {p0}, LaH/c;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_40} :catch_41

    goto :goto_6

    .line 173
    :catch_41
    move-exception v0

    goto :goto_6
.end method

.method private x()V
    .registers 5

    .prologue
    .line 180
    iget-boolean v0, p0, LaH/c;->e:Z

    if-eqz v0, :cond_21

    iget-object v0, p0, LaH/c;->c:LaH/h;

    if-eqz v0, :cond_21

    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xafc80

    cmp-long v0, v0, v2

    if-lez v0, :cond_21

    .line 183
    invoke-virtual {p0}, LaH/c;->m()V

    .line 185
    :cond_21
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public a(I)V
    .registers 5
    .parameter

    .prologue
    .line 352
    iget-object v1, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    .line 353
    :try_start_3
    iget-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/y;

    .line 354
    invoke-interface {v0, p1, p0}, LaH/y;->a(ILaH/s;)V

    goto :goto_9

    .line 356
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0

    :cond_1c
    :try_start_1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_19

    .line 357
    return-void
.end method

.method public a(LaH/y;)V
    .registers 3
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    return-void
.end method

.method protected abstract b()V
.end method

.method protected final b(LaH/h;)V
    .registers 6
    .parameter

    .prologue
    .line 313
    invoke-direct {p0, p1}, LaH/c;->a(LaH/h;)LaH/h;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_27

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    move-object v1, v0

    .line 315
    :goto_b
    iget-object v2, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v2

    .line 316
    :try_start_e
    iget-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/y;

    .line 317
    invoke-interface {v0, v1, p0}, LaH/y;->a(LaN/B;LaH/s;)V

    goto :goto_14

    .line 319
    :catchall_24
    move-exception v0

    monitor-exit v2
    :try_end_26
    .catchall {:try_start_e .. :try_end_26} :catchall_24

    throw v0

    .line 314
    :cond_27
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_b

    .line 319
    :cond_2a
    :try_start_2a
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_2a .. :try_end_2b} :catchall_24

    .line 320
    return-void
.end method

.method protected abstract c()V
.end method

.method public d()LaH/h;
    .registers 2

    .prologue
    .line 446
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 103
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized f()V
    .registers 3

    .prologue
    .line 266
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, LaH/c;->f:Z

    if-eqz v0, :cond_12

    .line 268
    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/c;->f:Z

    .line 270
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "BaseLocationProvider"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_14

    .line 272
    :cond_12
    monitor-exit p0

    return-void

    .line 266
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .registers 3

    .prologue
    .line 251
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, LaH/c;->f:Z

    if-nez v0, :cond_17

    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/c;->f:Z

    .line 255
    iget-object v1, p0, LaH/c;->g:Ljava/lang/Object;

    monitor-enter v1
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_1c

    .line 256
    :try_start_b
    iget-object v0, p0, LaH/c;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 257
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_19

    .line 259
    :try_start_11
    invoke-virtual {p0}, LaH/c;->b()V

    .line 260
    invoke-direct {p0}, LaH/c;->w()V
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_1c

    .line 262
    :cond_17
    monitor-exit p0

    return-void

    .line 257
    :catchall_19
    move-exception v0

    :try_start_1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_19

    :try_start_1b
    throw v0
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1c

    .line 251
    :catchall_1c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 234
    const/4 v0, 0x0

    return v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 239
    const/4 v0, 0x0

    return v0
.end method

.method protected l()I
    .registers 2

    .prologue
    .line 189
    invoke-direct {p0}, LaH/c;->x()V

    .line 190
    iget-boolean v0, p0, LaH/c;->e:Z

    if-eqz v0, :cond_1b

    iget-object v0, p0, LaH/c;->c:LaH/h;

    if-eqz v0, :cond_1b

    iget-object v0, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v0}, LaH/h;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v0}, LaH/h;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    :goto_1a
    return v0

    :cond_1b
    const v0, 0x1869f

    goto :goto_1a
.end method

.method public m()V
    .registers 2

    .prologue
    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, LaH/c;->c:LaH/h;

    .line 204
    return-void
.end method

.method public n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 209
    const/4 v0, 0x0

    return-object v0
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 214
    iget-boolean v0, p0, LaH/c;->e:Z

    return v0
.end method

.method public p()Z
    .registers 2

    .prologue
    .line 229
    invoke-virtual {p0}, LaH/c;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, LaH/c;->i()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public q()Z
    .registers 5

    .prologue
    .line 244
    iget-object v0, p0, LaH/c;->c:LaH/h;

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xafc80

    cmp-long v0, v0, v2

    if-gez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public r()LaH/h;
    .registers 2

    .prologue
    .line 361
    iget-object v0, p0, LaH/c;->c:LaH/h;

    return-object v0
.end method

.method public run()V
    .registers 2

    .prologue
    .line 283
    :try_start_0
    invoke-virtual {p0}, LaH/c;->c()V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_3} :catch_4

    .line 287
    :goto_3
    return-void

    .line 284
    :catch_4
    move-exception v0

    .line 285
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LaH/c;->a(I)V

    goto :goto_3
.end method

.method public s()Z
    .registers 2

    .prologue
    .line 372
    iget-boolean v0, p0, LaH/c;->e:Z

    return v0
.end method

.method public t()Lcom/google/googlenav/common/a;
    .registers 2

    .prologue
    .line 441
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 452
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LaH/c;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", location: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
