.class public LaH/d;
.super Law/a;
.source "SourceFile"


# instance fields
.field private final a:LaN/B;

.field private final b:I

.field private final c:I

.field private final d:LaI/b;

.field private final e:LaL/d;

.field private final f:LaH/m;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Z

.field private final j:J

.field private final k:LaH/f;

.field private final l:Ljava/lang/String;

.field private final m:LaN/B;

.field private final n:Z

.field private final o:Ljava/lang/String;


# direct methods
.method private constructor <init>(LaN/B;IILaI/b;LaL/d;LaH/m;Ljava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    invoke-direct {p0}, Law/a;-><init>()V

    .line 160
    iput-object p1, p0, LaH/d;->a:LaN/B;

    .line 161
    iput p2, p0, LaH/d;->b:I

    .line 162
    iput p3, p0, LaH/d;->c:I

    .line 163
    iput-object p4, p0, LaH/d;->d:LaI/b;

    .line 164
    iput-object p5, p0, LaH/d;->e:LaL/d;

    .line 165
    iput-object p6, p0, LaH/d;->f:LaH/m;

    .line 166
    iput-object p7, p0, LaH/d;->g:Ljava/lang/String;

    .line 167
    iput p8, p0, LaH/d;->h:I

    .line 168
    iput-boolean p9, p0, LaH/d;->i:Z

    .line 169
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    iput-wide v1, p0, LaH/d;->j:J

    .line 170
    iput-object p10, p0, LaH/d;->k:LaH/f;

    .line 171
    iput-object p11, p0, LaH/d;->l:Ljava/lang/String;

    .line 172
    iput-object p12, p0, LaH/d;->m:LaN/B;

    .line 173
    move/from16 v0, p13

    iput-boolean v0, p0, LaH/d;->n:Z

    .line 174
    move-object/from16 v0, p14

    iput-object v0, p0, LaH/d;->o:Ljava/lang/String;

    .line 175
    return-void
.end method

.method synthetic constructor <init>(LaN/B;IILaI/b;LaL/d;LaH/m;Ljava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;LaH/e;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct/range {p0 .. p14}, LaH/d;-><init>(LaN/B;IILaI/b;LaL/d;LaH/m;Ljava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaH/f;
    .registers 4
    .parameter

    .prologue
    .line 132
    new-instance v0, LaH/f;

    invoke-static {}, LaL/d;->d()[LaL/d;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, LaH/f;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LaL/d;LaH/e;)V

    return-object v0
.end method

.method public static a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 17
    .parameter
    .parameter

    .prologue
    .line 740
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v6

    .line 741
    if-eqz v6, :cond_31

    invoke-interface {v6}, LaH/m;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 742
    :goto_a
    invoke-static {v0}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaH/f;

    move-result-object v2

    .line 744
    const/4 v5, 0x0

    .line 746
    invoke-static {}, LaI/b;->d()LaI/b;

    move-result-object v0

    invoke-static {}, LaL/d;->c()LaL/d;

    move-result-object v1

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v7

    const/4 v9, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move v10, p0

    move/from16 v11, p1

    invoke-static/range {v0 .. v14}, LaH/d;->a(LaI/b;LaL/d;LaH/f;IILjava/lang/String;LaH/m;JIIILaN/B;ZLjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0

    .line 741
    :cond_31
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static a(LaH/m;JII)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    .line 353
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lby/Q;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 354
    if-eqz p0, :cond_4b

    .line 355
    invoke-interface {p0}, LaH/m;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 356
    invoke-interface {p0}, LaH/m;->s()LaH/h;

    move-result-object v2

    .line 357
    if-eqz v2, :cond_4b

    .line 358
    invoke-virtual {v2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_27

    .line 359
    const/4 v3, 0x3

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 361
    :cond_27
    const/4 v3, 0x6

    invoke-virtual {v1, v3, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 362
    invoke-virtual {v2}, Landroid/location/Location;->hasSpeed()Z

    move-result v3

    if-eqz v3, :cond_3b

    .line 363
    const/16 v3, 0xc

    invoke-virtual {v2}, Landroid/location/Location;->getSpeed()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 365
    :cond_3b
    invoke-virtual {v2}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_4b

    .line 366
    const/16 v3, 0xd

    invoke-virtual {v2}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 370
    :cond_4b
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lby/O;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 371
    invoke-virtual {v2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 372
    invoke-virtual {v2, v5, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 373
    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 374
    if-eqz p0, :cond_6a

    invoke-interface {p0}, LaH/m;->p()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_6a

    const/4 v0, 0x0

    .line 377
    :cond_6a
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 379
    return-object v1
.end method

.method private static a(LaI/b;LaL/d;LaH/f;IILjava/lang/String;LaH/m;JIIILaN/B;ZLjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 238
    new-instance v5, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lby/aF;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 249
    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lby/Q;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 251
    const/4 v7, 0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/googlenav/common/Config;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 252
    invoke-static {}, LaH/g;->a()Ljava/lang/String;

    move-result-object v7

    .line 253
    invoke-static {v7}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_28

    .line 254
    const/4 v8, 0x3

    invoke-virtual {v6, v8, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 263
    :cond_28
    invoke-virtual {p0}, LaI/b;->f()Z

    move-result v7

    if-eqz v7, :cond_36

    .line 264
    const/4 v7, 0x6

    invoke-virtual {p0}, LaI/b;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 268
    :cond_36
    invoke-virtual {p1}, LaL/d;->f()Z

    move-result v7

    if-eqz v7, :cond_44

    .line 269
    const/4 v7, 0x7

    invoke-virtual {p1}, LaL/d;->i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 272
    :cond_44
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 281
    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lby/Q;->j:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 286
    const/4 v7, 0x3

    invoke-virtual {v6, v7, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 288
    const/4 v7, 0x2

    if-ne p3, v7, :cond_62

    .line 289
    const/4 v7, 0x4

    invoke-virtual {v6, v7, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 290
    const/4 v7, 0x5

    if-eqz p5, :cond_c2

    :goto_5d
    move-object/from16 v0, p5

    invoke-virtual {v6, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 293
    :cond_62
    if-lez p9, :cond_6a

    .line 295
    const/4 v7, 0x6

    move/from16 v0, p9

    invoke-virtual {v6, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 297
    :cond_6a
    const/4 v7, 0x2

    invoke-virtual {v5, v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v6, p3

    move-object/from16 v7, p6

    move-object/from16 v8, p12

    move/from16 v9, p13

    move-object/from16 v10, p14

    .line 299
    invoke-static/range {v5 .. v10}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaH/m;LaN/B;ZLjava/lang/String;)V

    .line 306
    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v7, Lby/w;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 309
    invoke-virtual {p0}, LaI/b;->f()Z

    move-result v7

    if-eqz v7, :cond_8f

    .line 310
    const/4 v7, 0x1

    invoke-virtual {p0}, LaI/b;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 314
    :cond_8f
    invoke-virtual {p1}, LaL/d;->f()Z

    move-result v7

    if-eqz v7, :cond_9d

    .line 315
    const/4 v7, 0x2

    invoke-virtual {p1}, LaL/d;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 319
    :cond_9d
    move-object/from16 v0, p6

    move-wide/from16 v1, p7

    move/from16 v3, p10

    move/from16 v4, p11

    invoke-static {v0, v1, v2, v3, v4}, LaH/d;->a(LaH/m;JII)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 320
    const/4 v8, 0x3

    invoke-virtual {v6, v8, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 321
    const/4 v7, 0x4

    invoke-virtual {v5, v7, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 323
    if-eqz p2, :cond_c1

    .line 324
    invoke-virtual {p2}, LaH/f;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    invoke-static {v5, v6}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 325
    invoke-virtual {p2}, LaH/f;->b()[LaL/d;

    move-result-object v6

    invoke-static {v5, v6}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LaL/d;)V

    .line 328
    :cond_c1
    return-object v5

    .line 290
    :cond_c2
    const-string p5, ""

    goto :goto_5d
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x1

    .line 499
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v1, v2

    :goto_7
    if-ge v1, v3, :cond_21

    .line 500
    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 501
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 502
    if-eqz v0, :cond_1d

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    cmp-long v4, p1, v4

    if-nez v4, :cond_1d

    .line 507
    :goto_1c
    return-object v0

    .line 499
    :cond_1d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 507
    :cond_21
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method private static a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V
    .registers 27
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 704
    if-nez p5, :cond_3

    .line 729
    :goto_2
    return-void

    .line 708
    :cond_3
    new-instance v1, LaH/e;

    move-object v2, p1

    move-object/from16 v3, p5

    move/from16 v4, p6

    move v5, p0

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p7

    move-object/from16 v9, p2

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v1 .. v13}, LaH/e;-><init>(LaH/m;LaN/B;IILjava/lang/String;IZLaH/f;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    .line 727
    new-instance v2, Las/d;

    move-object/from16 v0, p8

    invoke-direct {v2, v0, v1}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    .line 728
    invoke-virtual {v2}, Las/d;->g()V

    goto :goto_2
.end method

.method public static a(LaH/m;LaN/B;ZLjava/lang/String;Las/c;)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 637
    if-eqz p0, :cond_25

    .line 638
    invoke-interface {p0}, LaH/m;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaH/f;

    move-result-object v2

    .line 639
    invoke-interface {p0}, LaH/m;->s()LaH/h;

    move-result-object v1

    .line 640
    const/16 v0, 0x80

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz v1, :cond_26

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v5

    :goto_18
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object/from16 v8, p4

    move-object v10, p1

    move v11, p2

    move-object/from16 v12, p3

    invoke-static/range {v0 .. v12}, LaH/d;->a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    .line 644
    :cond_25
    return-void

    .line 640
    :cond_26
    const/4 v5, 0x0

    goto :goto_18
.end method

.method public static a(LaN/B;ILas/c;)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 572
    const/4 v0, 0x7

    move-object v2, v1

    move-object v3, v1

    move-object v5, p0

    move v6, p1

    move v7, v4

    move-object v8, p2

    move-object v9, v1

    move-object v10, v1

    move v11, v4

    move-object v12, v1

    invoke-static/range {v0 .. v12}, LaH/d;->a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    .line 574
    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILaH/m;LaN/B;ZLjava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 442
    const/16 v0, 0x80

    if-eq p1, v0, :cond_5

    .line 458
    :cond_4
    :goto_4
    return-void

    .line 447
    :cond_5
    if-eqz p2, :cond_4

    .line 451
    invoke-interface {p2}, LaH/m;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 452
    if-eqz v0, :cond_4

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 456
    invoke-static {v0, p3, p4, p5}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ZLjava/lang/String;)V

    .line 457
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_4
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ZLjava/lang/String;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x7

    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 467
    invoke-virtual {p0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(II)[B

    move-result-object v2

    .line 468
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lby/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 470
    :try_start_f
    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 471
    if-eqz p1, :cond_2f

    .line 472
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v4, Lby/O;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 473
    const/4 v4, 0x1

    invoke-virtual {p1}, LaN/B;->d()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 474
    const/4 v4, 0x2

    invoke-virtual {p1}, LaN/B;->f()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 475
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 477
    :cond_2f
    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_39

    .line 478
    const/4 v2, 0x3

    invoke-virtual {v3, v2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 480
    :cond_39
    const/4 v2, 0x4

    if-eqz p2, :cond_51

    :goto_3c
    invoke-virtual {v3, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 484
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 485
    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 486
    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(II[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_50} :catch_53

    .line 490
    :goto_50
    return-void

    :cond_51
    move v0, v1

    .line 480
    goto :goto_3c

    .line 487
    :catch_53
    move-exception v0

    goto :goto_50
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 387
    if-eqz p1, :cond_11

    .line 388
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lby/w;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 390
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 391
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 393
    :cond_11
    return-void
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LaL/d;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    .line 403
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_35

    .line 404
    aget-object v1, p1, v0

    .line 405
    invoke-virtual {v1}, LaL/d;->f()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 406
    invoke-virtual {v1}, LaL/d;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 407
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    .line 410
    invoke-static {p0, v2, v3}, LaH/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 411
    if-eqz v2, :cond_26

    .line 415
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 416
    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 403
    :cond_23
    :goto_23
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 422
    :cond_26
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lby/w;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 424
    invoke-virtual {v2, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 426
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_23

    .line 430
    :cond_35
    return-void
.end method

.method public static a(Ljava/lang/String;ILaN/B;ILas/c;)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 588
    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v3, p0

    move v4, p1

    move-object v5, p2

    move/from16 v6, p3

    move-object/from16 v8, p4

    invoke-static/range {v0 .. v12}, LaH/d;->a(ILaH/m;LaH/f;Ljava/lang/String;ILaN/B;IZLas/c;Ljava/lang/String;LaN/B;ZLjava/lang/String;)V

    .line 591
    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 20
    .parameter

    .prologue
    .line 180
    new-instance v17, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/el;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 182
    move-object/from16 v0, p0

    iget v2, v0, LaH/d;->c:I

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8e

    .line 183
    const/4 v2, 0x1

    const/4 v3, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 192
    :goto_18
    move-object/from16 v0, p0

    iget-object v2, v0, LaH/d;->d:LaI/b;

    move-object/from16 v0, p0

    iget-object v3, v0, LaH/d;->e:LaL/d;

    move-object/from16 v0, p0

    iget-object v4, v0, LaH/d;->k:LaH/f;

    move-object/from16 v0, p0

    iget v5, v0, LaH/d;->c:I

    move-object/from16 v0, p0

    iget v6, v0, LaH/d;->h:I

    move-object/from16 v0, p0

    iget-object v7, v0, LaH/d;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LaH/d;->f:LaH/m;

    move-object/from16 v0, p0

    iget-wide v9, v0, LaH/d;->j:J

    move-object/from16 v0, p0

    iget v11, v0, LaH/d;->b:I

    move-object/from16 v0, p0

    iget-object v12, v0, LaH/d;->a:LaN/B;

    invoke-virtual {v12}, LaN/B;->c()I

    move-result v12

    mul-int/lit8 v12, v12, 0xa

    move-object/from16 v0, p0

    iget-object v13, v0, LaH/d;->a:LaN/B;

    invoke-virtual {v13}, LaN/B;->e()I

    move-result v13

    mul-int/lit8 v13, v13, 0xa

    move-object/from16 v0, p0

    iget-object v14, v0, LaH/d;->m:LaN/B;

    move-object/from16 v0, p0

    iget-boolean v15, v0, LaH/d;->n:Z

    move-object/from16 v0, p0

    iget-object v0, v0, LaH/d;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v2 .. v16}, LaH/d;->a(LaI/b;LaL/d;LaH/f;IILjava/lang/String;LaH/m;JIIILaN/B;ZLjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 197
    move-object/from16 v0, p0

    iget v3, v0, LaH/d;->c:I

    const/16 v4, 0x81

    if-ne v3, v4, :cond_7e

    .line 198
    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v4, 0x12

    move-object/from16 v0, p0

    iget-object v5, v0, LaH/d;->l:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 202
    :cond_7e
    const/4 v3, 0x2

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 204
    check-cast p1, Ljava/io/OutputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 205
    return-void

    .line 185
    :cond_8e
    move-object/from16 v0, p0

    iget v2, v0, LaH/d;->c:I

    const/16 v3, 0x81

    if-ne v2, v3, :cond_9f

    .line 186
    const/4 v2, 0x1

    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_18

    .line 189
    :cond_9f
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto/16 :goto_18
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 528
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/el;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 530
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 531
    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 533
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 534
    packed-switch v1, :pswitch_data_34

    .line 549
    :cond_1a
    :goto_1a
    return v2

    .line 536
    :pswitch_1b
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 537
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 538
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 539
    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaH/g;->a(Ljava/lang/String;)V

    goto :goto_1a

    .line 534
    nop

    :pswitch_data_34
    .packed-switch 0x0
        :pswitch_1b
    .end packed-switch
.end method

.method public b()I
    .registers 2

    .prologue
    .line 522
    const/16 v0, 0x29

    return v0
.end method

.method public s_()Z
    .registers 2

    .prologue
    .line 512
    iget-boolean v0, p0, LaH/d;->i:Z

    return v0
.end method

.method public t_()Z
    .registers 2

    .prologue
    .line 517
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 554
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GlsLocationReport[mapPoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->a:LaN/B;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", zoomLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaH/d;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaH/d;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cellTowerInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->d:LaI/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectedWifi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->e:LaL/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->f:LaH/m;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchTerm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LaH/d;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", immediate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LaH/d;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, LaH/d;->j:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", locationInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->k:LaH/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaH/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
