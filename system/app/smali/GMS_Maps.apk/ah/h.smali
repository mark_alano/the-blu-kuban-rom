.class public final LaH/h;
.super Landroid/location/Location;
.source "SourceFile"


# instance fields
.field private a:LaN/B;

.field private b:Lo/D;

.field private c:Z

.field private d:J

.field private e:LaH/l;

.field private f:LaH/k;


# direct methods
.method private constructor <init>(LaH/j;)V
    .registers 4
    .parameter

    .prologue
    .line 216
    invoke-static {p1}, LaH/j;->a(LaH/j;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-static {p1}, LaH/j;->b(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 218
    invoke-static {p1}, LaH/j;->c(LaH/j;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setAccuracy(F)V

    .line 220
    :cond_14
    invoke-static {p1}, LaH/j;->d(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 221
    invoke-static {p1}, LaH/j;->e(LaH/j;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setAltitude(D)V

    .line 223
    :cond_21
    invoke-static {p1}, LaH/j;->f(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 224
    invoke-static {p1}, LaH/j;->g(LaH/j;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setBearing(F)V

    .line 226
    :cond_2e
    invoke-static {p1}, LaH/j;->h(LaH/j;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 227
    invoke-static {p1}, LaH/j;->i(LaH/j;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 228
    invoke-static {p1}, LaH/j;->j(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 229
    invoke-static {p1}, LaH/j;->k(LaH/j;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setSpeed(F)V

    .line 231
    :cond_49
    invoke-static {p1}, LaH/j;->l(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 232
    invoke-static {p1}, LaH/j;->m(LaH/j;)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setTime(J)V

    .line 234
    :cond_56
    invoke-static {p1}, LaH/j;->l(LaH/j;)Z

    move-result v0

    iput-boolean v0, p0, LaH/h;->c:Z

    .line 235
    invoke-static {p1}, LaH/j;->n(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_88

    invoke-static {p1}, LaH/j;->o(LaH/j;)J

    move-result-wide v0

    :goto_66
    iput-wide v0, p0, LaH/h;->d:J

    .line 237
    invoke-static {p1}, LaH/j;->p(LaH/j;)Landroid/os/Bundle;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    .line 238
    invoke-static {p1}, LaH/j;->q(LaH/j;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaH/h;->a:LaN/B;

    .line 239
    invoke-static {p1}, LaH/j;->r(LaH/j;)Lo/D;

    move-result-object v0

    iput-object v0, p0, LaH/h;->b:Lo/D;

    .line 240
    invoke-static {p1}, LaH/j;->s(LaH/j;)LaH/l;

    move-result-object v0

    iput-object v0, p0, LaH/h;->e:LaH/l;

    .line 241
    invoke-static {p1}, LaH/j;->t(LaH/j;)LaH/k;

    move-result-object v0

    iput-object v0, p0, LaH/h;->f:LaH/k;

    .line 242
    return-void

    .line 235
    :cond_88
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto :goto_66
.end method

.method synthetic constructor <init>(LaH/j;LaH/i;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, LaH/h;-><init>(LaH/j;)V

    return-void
.end method

.method public static a(Landroid/location/Location;)I
    .registers 3
    .parameter

    .prologue
    const v0, 0x1869f

    .line 252
    if-nez p0, :cond_6

    .line 259
    :cond_5
    :goto_5
    return v0

    .line 255
    :cond_6
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 259
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    goto :goto_5
.end method

.method public static a(LaH/h;)LaN/B;
    .registers 2
    .parameter

    .prologue
    .line 284
    if-nez p0, :cond_4

    .line 285
    const/4 v0, 0x0

    .line 287
    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {p0}, LaH/h;->a()LaN/B;

    move-result-object v0

    goto :goto_3
.end method

.method private a(ZDZD)Z
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 553
    if-eqz p1, :cond_f

    .line 554
    if-nez p4, :cond_7

    .line 562
    :cond_6
    :goto_6
    return v1

    .line 557
    :cond_7
    cmpl-double v2, p2, p5

    if-nez v2, :cond_d

    :goto_b
    move v1, v0

    goto :goto_6

    :cond_d
    move v0, v1

    goto :goto_b

    .line 559
    :cond_f
    if-nez p4, :cond_6

    move v1, v0

    .line 562
    goto :goto_6
.end method

.method private a(ZJZJ)Z
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 538
    if-eqz p1, :cond_f

    .line 539
    if-nez p4, :cond_7

    .line 547
    :cond_6
    :goto_6
    return v1

    .line 542
    :cond_7
    cmp-long v2, p2, p5

    if-nez v2, :cond_d

    :goto_b
    move v1, v0

    goto :goto_6

    :cond_d
    move v0, v1

    goto :goto_b

    .line 544
    :cond_f
    if-nez p4, :cond_6

    move v1, v0

    .line 547
    goto :goto_6
.end method

.method public static b(Landroid/location/Location;)I
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 270
    if-nez p0, :cond_4

    .line 276
    :cond_3
    :goto_3
    return v0

    .line 273
    :cond_4
    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 276
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-int v0, v0

    goto :goto_3
.end method

.method static synthetic b(LaH/h;)J
    .registers 3
    .parameter

    .prologue
    .line 33
    iget-wide v0, p0, LaH/h;->d:J

    return-wide v0
.end method

.method public static c(Landroid/location/Location;)I
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 298
    if-nez p0, :cond_4

    .line 304
    :cond_3
    :goto_3
    return v0

    .line 301
    :cond_4
    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 304
    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-int v0, v0

    goto :goto_3
.end method

.method static synthetic c(LaH/h;)LaH/l;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, LaH/h;->e:LaH/l;

    return-object v0
.end method

.method static synthetic d(LaH/h;)LaH/k;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, LaH/h;->f:LaH/k;

    return-object v0
.end method

.method public static d(Landroid/location/Location;)Lo/D;
    .registers 2
    .parameter

    .prologue
    .line 312
    if-eqz p0, :cond_6

    instance-of v0, p0, LaH/h;

    if-nez v0, :cond_8

    .line 313
    :cond_6
    const/4 v0, 0x0

    .line 315
    :goto_7
    return-object v0

    :cond_8
    check-cast p0, LaH/h;

    invoke-virtual {p0}, LaH/h;->b()Lo/D;

    move-result-object v0

    goto :goto_7
.end method


# virtual methods
.method public a(Lo/T;)F
    .registers 11
    .parameter

    .prologue
    .line 677
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 678
    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lo/T;->b()D

    move-result-wide v4

    invoke-virtual {p1}, Lo/T;->d()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, LaH/h;->distanceBetween(DDDD[F)V

    .line 681
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public a(Lo/u;)F
    .registers 13
    .parameter

    .prologue
    const-wide v9, 0x3eb0c6f7a0b5ed8dL

    .line 653
    const/4 v0, 0x2

    new-array v8, v0, [F

    .line 654
    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lo/u;->a()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v9

    invoke-virtual {p1}, Lo/u;->b()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v9

    invoke-static/range {v0 .. v8}, LaH/h;->distanceBetween(DDDD[F)V

    .line 657
    const/4 v0, 0x1

    aget v0, v8, v0

    return v0
.end method

.method public a()LaN/B;
    .registers 2

    .prologue
    .line 323
    iget-object v0, p0, LaH/h;->a:LaN/B;

    return-object v0
.end method

.method public b(Lo/u;)F
    .registers 13
    .parameter

    .prologue
    const-wide v9, 0x3eb0c6f7a0b5ed8dL

    .line 669
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 670
    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lo/u;->a()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v9

    invoke-virtual {p1}, Lo/u;->b()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v9

    invoke-static/range {v0 .. v8}, LaH/h;->distanceBetween(DDDD[F)V

    .line 673
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public b()Lo/D;
    .registers 2

    .prologue
    .line 330
    iget-object v0, p0, LaH/h;->b:Lo/D;

    return-object v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 337
    iget-object v0, p0, LaH/h;->b:Lo/D;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 344
    iget-boolean v0, p0, LaH/h;->c:Z

    return v0
.end method

.method public e()J
    .registers 3

    .prologue
    .line 425
    iget-wide v0, p0, LaH/h;->d:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 363
    instance-of v0, p1, LaH/h;

    if-nez v0, :cond_8

    move v0, v7

    .line 418
    :goto_7
    return v0

    .line 366
    :cond_8
    check-cast p1, LaH/h;

    .line 368
    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    move v0, v7

    .line 369
    goto :goto_7

    .line 372
    :cond_1a
    invoke-virtual {p1}, LaH/h;->b()Lo/D;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->b()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    move v0, v7

    .line 373
    goto :goto_7

    .line 376
    :cond_2a
    invoke-virtual {p1}, LaH/h;->hasAccuracy()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getAccuracy()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, LaH/h;->hasAccuracy()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getAccuracy()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_45

    move v0, v7

    .line 378
    goto :goto_7

    .line 380
    :cond_45
    invoke-virtual {p1}, LaH/h;->hasAltitude()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getAltitude()D

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->hasAltitude()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getAltitude()D

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_5e

    move v0, v7

    .line 382
    goto :goto_7

    .line 384
    :cond_5e
    invoke-virtual {p1}, LaH/h;->hasBearing()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getBearing()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, LaH/h;->hasBearing()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getBearing()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_79

    move v0, v7

    .line 386
    goto :goto_7

    .line 388
    :cond_79
    invoke-virtual {p1}, LaH/h;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8a

    move v0, v7

    .line 389
    goto/16 :goto_7

    .line 391
    :cond_8a
    invoke-virtual {p1}, LaH/h;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_9e

    move v0, v7

    .line 392
    goto/16 :goto_7

    .line 394
    :cond_9e
    invoke-virtual {p1}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_b2

    move v0, v7

    .line 395
    goto/16 :goto_7

    .line 397
    :cond_b2
    invoke-virtual {p1}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c3

    move v0, v7

    .line 398
    goto/16 :goto_7

    .line 400
    :cond_c3
    invoke-virtual {p1}, LaH/h;->hasSpeed()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getSpeed()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, LaH/h;->hasSpeed()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getSpeed()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_df

    move v0, v7

    .line 402
    goto/16 :goto_7

    .line 404
    :cond_df
    invoke-virtual {p1}, LaH/h;->d()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getTime()J

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->d()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getTime()J

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZJZJ)Z

    move-result v0

    if-nez v0, :cond_f9

    move v0, v7

    .line 406
    goto/16 :goto_7

    .line 409
    :cond_f9
    invoke-virtual {p1}, LaH/h;->e()J

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->e()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_108

    move v0, v7

    .line 410
    goto/16 :goto_7

    .line 412
    :cond_108
    iget-object v0, p1, LaH/h;->e:LaH/l;

    iget-object v1, p0, LaH/h;->e:LaH/l;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_115

    move v0, v7

    .line 413
    goto/16 :goto_7

    .line 415
    :cond_115
    iget-object v0, p1, LaH/h;->f:LaH/k;

    iget-object v1, p0, LaH/h;->f:LaH/k;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_122

    move v0, v7

    .line 416
    goto/16 :goto_7

    :cond_122
    move v0, v8

    .line 418
    goto/16 :goto_7
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 432
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 447
    iget-object v0, p0, LaH/h;->f:LaH/k;

    if-eqz v0, :cond_c

    iget-object v0, p0, LaH/h;->f:LaH/k;

    iget-boolean v0, v0, LaH/k;->a:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 454
    iget-object v0, p0, LaH/h;->f:LaH/k;

    if-eqz v0, :cond_12

    iget-object v0, p0, LaH/h;->f:LaH/k;

    iget-boolean v0, v0, LaH/k;->a:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, LaH/h;->f:LaH/k;

    iget-boolean v0, v0, LaH/k;->b:Z

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 349
    .line 351
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LaH/h;->a:LaN/B;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaH/h;->b:Lo/D;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, LaH/h;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LaH/h;->e:LaH/l;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LaH/h;->f:LaH/k;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 353
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getAccuracy()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 354
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getTime()J

    move-result-wide v1

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 355
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getBearing()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 356
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getAltitude()D

    move-result-wide v1

    double-to-int v1, v1

    add-int/2addr v0, v1

    .line 357
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getSpeed()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 358
    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 476
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_c

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-boolean v0, v0, LaH/l;->a:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public j()I
    .registers 2

    .prologue
    .line 483
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget v0, v0, LaH/l;->f:I

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 491
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_c

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-boolean v0, v0, LaH/l;->g:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public l()Lo/af;
    .registers 2

    .prologue
    .line 498
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-object v0, v0, LaH/l;->b:Lo/af;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public m()Lo/T;
    .registers 2

    .prologue
    .line 506
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-object v0, v0, LaH/l;->c:Lo/T;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public n()LO/H;
    .registers 2

    .prologue
    .line 513
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-object v0, v0, LaH/l;->d:LO/H;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public o()Z
    .registers 5

    .prologue
    .line 526
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_10

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-wide v0, v0, LaH/l;->e:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public p()D
    .registers 3

    .prologue
    .line 533
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_9

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-wide v0, v0, LaH/l;->e:D

    :goto_8
    return-wide v0

    :cond_9
    const-wide/high16 v0, -0x4010

    goto :goto_8
.end method

.method public q()Lo/u;
    .registers 9

    .prologue
    const-wide v6, 0x412e848000000000L

    const-wide/high16 v4, 0x3fe0

    .line 643
    new-instance v0, Lo/u;

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v6

    add-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lo/u;-><init>(II)V

    return-object v0
.end method

.method public r()LaN/B;
    .registers 9

    .prologue
    const-wide v6, 0x412e848000000000L

    const-wide/high16 v4, 0x3fe0

    .line 648
    new-instance v0, LaN/B;

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v6

    add-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method

.method public setAccuracy(F)V
    .registers 3
    .parameter

    .prologue
    .line 599
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setAltitude(D)V
    .registers 4
    .parameter

    .prologue
    .line 604
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setBearing(F)V
    .registers 3
    .parameter

    .prologue
    .line 609
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 614
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLatitude(D)V
    .registers 4
    .parameter

    .prologue
    .line 619
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLongitude(D)V
    .registers 4
    .parameter

    .prologue
    .line 624
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setProvider(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 629
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setSpeed(F)V
    .registers 3
    .parameter

    .prologue
    .line 634
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setTime(J)V
    .registers 4
    .parameter

    .prologue
    .line 639
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 568
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 569
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    .line 570
    const-string v0, "GmmLocation["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "source = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", point = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, LaH/h;->a:LaN/B;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", accuracy = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, LaH/h;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_158

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaH/h;->getAccuracy()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", speed = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_15c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaH/h;->getSpeed()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m/s"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_74
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", bearing = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, LaH/h;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_160

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaH/h;->getBearing()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " degrees"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_9b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", time = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p0}, LaH/h;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", relativetime = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p0}, LaH/h;->e()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", level = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LaH/h;->b:Lo/D;

    if-eqz v0, :cond_164

    iget-object v0, p0, LaH/h;->b:Lo/D;

    :goto_d9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 579
    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_115

    .line 580
    const-string v0, ", RouteSnappingInfo["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", onRoad = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->e:LaH/l;

    iget-boolean v2, v2, LaH/l;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", onRteCon = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->e:LaH/l;

    iget-wide v2, v2, LaH/l;->e:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isProjected = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->e:LaH/l;

    iget-boolean v2, v2, LaH/l;->g:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    :cond_115
    iget-object v0, p0, LaH/h;->f:LaH/k;

    if-eqz v0, :cond_14e

    .line 587
    const-string v0, ", RouteSnappingInfo["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isGps = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->f:LaH/k;

    iget-boolean v2, v2, LaH/k;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isGpsAccurate = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->f:LaH/k;

    iget-boolean v2, v2, LaH/k;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", numSatInFix = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->f:LaH/k;

    iget v2, v2, LaH/k;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    :cond_14e
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 570
    :cond_158
    const-string v0, "n/a"

    goto/16 :goto_4d

    :cond_15c
    const-string v0, "n/a"

    goto/16 :goto_74

    :cond_160
    const-string v0, "n/a"

    goto/16 :goto_9b

    :cond_164
    const-string v0, "n/a"

    goto/16 :goto_d9
.end method
