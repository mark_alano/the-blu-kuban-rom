.class public LaH/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/m;
.implements LaH/y;
.implements Ljava/lang/Runnable;


# static fields
.field private static volatile k:LaH/o;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field c:Z

.field d:Z

.field private final e:LaH/q;

.field private volatile f:Ljava/util/Map;

.field private g:D

.field private h:LaH/h;

.field private i:LaH/h;

.field private j:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, LaH/r;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LaH/r;-><init>(LaH/p;)V

    iput-object v0, p0, LaH/o;->e:LaH/q;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaH/o;->a:Ljava/util/List;

    .line 117
    invoke-static {}, Lcom/google/common/collect/Maps;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LaH/o;->f:Ljava/util/Map;

    .line 121
    const-wide/high16 v0, -0x3c20

    iput-wide v0, p0, LaH/o;->g:D

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaH/o;->b:Ljava/util/List;

    .line 141
    iput-boolean v2, p0, LaH/o;->c:Z

    .line 145
    iput-boolean v2, p0, LaH/o;->d:Z

    .line 161
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    iput-boolean v0, p0, LaH/o;->j:Z

    .line 200
    if-nez p1, :cond_31

    .line 217
    :goto_30
    return-void

    .line 208
    :cond_31
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_40

    .line 210
    invoke-static {p2}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaH/o;->a:Ljava/util/List;

    .line 212
    invoke-direct {p0}, LaH/o;->v()V

    .line 215
    :cond_40
    invoke-direct {p0}, LaH/o;->t()V

    .line 216
    invoke-direct {p0}, LaH/o;->u()V

    goto :goto_30
.end method

.method private static a(LaN/B;)LaN/B;
    .registers 2
    .parameter

    .prologue
    .line 734
    invoke-static {p0}, LaH/E;->e(LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 735
    invoke-static {}, LaH/E;->k()LaH/E;

    move-result-object v0

    invoke-virtual {v0, p0}, LaH/E;->d(LaN/B;)LaN/B;

    move-result-object p0

    .line 737
    :cond_e
    return-object p0
.end method

.method private a(LaH/h;)V
    .registers 5
    .parameter

    .prologue
    .line 663
    if-eqz p1, :cond_53

    .line 664
    iput-object p1, p0, LaH/o;->h:LaH/h;

    .line 665
    monitor-enter p0

    .line 666
    :try_start_5
    iget-object v0, p0, LaH/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/n;

    .line 667
    iget-object v2, p0, LaH/o;->h:LaH/h;

    invoke-interface {v0, v2}, LaH/n;->a(LaH/h;)LaH/h;

    move-result-object v0

    iput-object v0, p0, LaH/o;->h:LaH/h;

    goto :goto_b

    .line 669
    :catchall_20
    move-exception v0

    monitor-exit p0
    :try_end_22
    .catchall {:try_start_5 .. :try_end_22} :catchall_20

    throw v0

    :cond_23
    :try_start_23
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_20

    .line 670
    iget-object v0, p0, LaH/o;->h:LaH/h;

    iput-object v0, p0, LaH/o;->i:LaH/h;

    .line 671
    iget-object v0, p0, LaH/o;->h:LaH/h;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-static {v0}, LaH/o;->a(LaN/B;)LaN/B;

    move-result-object v0

    .line 672
    iget-object v1, p0, LaH/o;->h:LaH/h;

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_53

    .line 673
    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    iget-object v2, p0, LaH/o;->h:LaH/h;

    invoke-virtual {v1, v2}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v1

    invoke-virtual {v1, v0}, LaH/j;->a(LaN/B;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    iput-object v0, p0, LaH/o;->i:LaH/h;

    .line 679
    :cond_53
    return-void
.end method

.method public static a(LaH/o;)V
    .registers 1
    .parameter

    .prologue
    .line 175
    sput-object p0, LaH/o;->k:LaH/o;

    .line 176
    return-void
.end method

.method private a(LaH/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 648
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    .line 649
    if-eq v0, p1, :cond_8

    .line 650
    const/4 v0, 0x0

    .line 655
    :goto_7
    return v0

    .line 654
    :cond_8
    invoke-interface {v0}, LaH/s;->r()LaH/h;

    move-result-object v0

    invoke-direct {p0, v0}, LaH/o;->a(LaH/h;)V

    .line 655
    const/4 v0, 0x1

    goto :goto_7
.end method

.method protected static a(LaH/s;LaH/s;)Z
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 596
    if-nez p0, :cond_6

    move v0, v1

    .line 633
    :cond_5
    :goto_5
    return v0

    .line 601
    :cond_6
    if-nez p1, :cond_16

    .line 605
    invoke-interface {p0}, LaH/s;->o()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {p0}, LaH/s;->i()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_14
    move v0, v1

    goto :goto_5

    .line 609
    :cond_16
    invoke-interface {p0}, LaH/s;->r()LaH/h;

    move-result-object v2

    .line 610
    invoke-interface {p1}, LaH/s;->r()LaH/h;

    move-result-object v3

    .line 611
    if-nez v2, :cond_22

    move v0, v1

    .line 612
    goto :goto_5

    .line 614
    :cond_22
    if-eqz v3, :cond_5

    .line 620
    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v4

    invoke-virtual {v3}, LaH/h;->getTime()J

    move-result-wide v6

    const-wide/16 v8, 0x2af8

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_5

    .line 627
    invoke-virtual {v2}, LaH/h;->hasAccuracy()Z

    move-result v4

    if-nez v4, :cond_3b

    move v0, v1

    .line 628
    goto :goto_5

    .line 630
    :cond_3b
    invoke-virtual {v3}, LaH/h;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 633
    invoke-virtual {v2}, LaH/h;->getAccuracy()F

    move-result v2

    invoke-virtual {v3}, LaH/h;->getAccuracy()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_5

    move v0, v1

    goto :goto_5
.end method

.method static synthetic b(LaH/o;)V
    .registers 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, LaH/o;->u()V

    return-void
.end method

.method public static m()LaH/m;
    .registers 1

    .prologue
    .line 171
    sget-object v0, LaH/o;->k:LaH/o;

    return-object v0
.end method

.method public static n()V
    .registers 1

    .prologue
    .line 180
    const/4 v0, 0x0

    sput-object v0, LaH/o;->k:LaH/o;

    .line 181
    return-void
.end method

.method private t()V
    .registers 4

    .prologue
    .line 220
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LaH/p;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, LaH/p;-><init>(LaH/o;Landroid/os/Handler;)V

    invoke-static {v0, v1}, LaH/x;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 232
    return-void
.end method

.method private declared-synchronized u()V
    .registers 3

    .prologue
    .line 235
    monitor-enter p0

    :try_start_1
    iget-boolean v1, p0, LaH/o;->j:Z

    .line 236
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->E()Landroid/content/Context;

    move-result-object v0

    .line 237
    invoke-static {v0}, LaH/x;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    :goto_12
    iput-boolean v0, p0, LaH/o;->j:Z

    .line 238
    iget-boolean v0, p0, LaH/o;->j:Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_31

    if-ne v0, v1, :cond_1c

    .line 250
    :cond_18
    :goto_18
    monitor-exit p0

    return-void

    .line 237
    :cond_1a
    const/4 v0, 0x0

    goto :goto_12

    .line 242
    :cond_1c
    :try_start_1c
    invoke-direct {p0}, LaH/o;->y()V

    .line 244
    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_18

    iget-boolean v0, p0, LaH/o;->d:Z

    if-eqz v0, :cond_18

    .line 245
    invoke-direct {p0}, LaH/o;->x()V

    .line 248
    iget-object v0, p0, LaH/o;->e:LaH/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p0}, LaH/q;->a(LaN/B;LaH/o;)V
    :try_end_30
    .catchall {:try_start_1c .. :try_end_30} :catchall_31

    goto :goto_18

    .line 235
    :catchall_31
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private v()V
    .registers 3

    .prologue
    .line 268
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 269
    invoke-interface {v0, p0}, LaH/s;->a(LaH/y;)V

    goto :goto_6

    .line 271
    :cond_16
    return-void
.end method

.method private w()V
    .registers 8

    .prologue
    .line 370
    iget-object v0, p0, LaH/o;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 371
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 372
    new-instance v2, LaH/B;

    invoke-interface {v0}, LaH/s;->o()Z

    move-result v3

    invoke-interface {v0}, LaH/s;->p()Z

    move-result v4

    invoke-interface {v0}, LaH/s;->r()LaH/h;

    move-result-object v5

    invoke-interface {v0}, LaH/s;->d()LaH/h;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, LaH/B;-><init>(ZZLaH/h;LaH/h;)V

    .line 377
    iget-object v3, p0, LaH/o;->f:Ljava/util/Map;

    invoke-interface {v0}, LaH/s;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 379
    :cond_36
    invoke-virtual {p0}, LaH/o;->o()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    long-to-double v0, v0

    iput-wide v0, p0, LaH/o;->g:D

    .line 380
    return-void
.end method

.method private x()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 440
    iput-object v0, p0, LaH/o;->h:LaH/h;

    .line 441
    iput-object v0, p0, LaH/o;->i:LaH/h;

    .line 442
    return-void
.end method

.method private y()V
    .registers 3

    .prologue
    .line 468
    iget-boolean v0, p0, LaH/o;->c:Z

    if-eqz v0, :cond_21

    iget-boolean v0, p0, LaH/o;->j:Z

    if-eqz v0, :cond_21

    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/o;->d:Z

    .line 470
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 471
    invoke-interface {v0}, LaH/s;->f()V

    goto :goto_11

    .line 474
    :cond_21
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_27
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 475
    invoke-interface {v0}, LaH/s;->g()V

    goto :goto_27

    .line 478
    :cond_37
    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_7

    move-object v0, v1

    .line 285
    :goto_6
    return-object v0

    .line 280
    :cond_7
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 281
    invoke-interface {v0}, LaH/s;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 282
    invoke-interface {v0}, LaH/s;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_6

    :cond_24
    move-object v0, v1

    .line 285
    goto :goto_6
.end method

.method public a(ILaH/s;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 692
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_9

    if-eq v0, p2, :cond_9

    .line 700
    :goto_8
    return-void

    .line 699
    :cond_9
    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1, p0}, LaH/q;->a(ILaH/o;)V

    goto :goto_8
.end method

.method public a(LaH/A;)V
    .registers 3
    .parameter

    .prologue
    .line 535
    iget-boolean v0, p0, LaH/o;->d:Z

    if-eqz v0, :cond_4

    .line 539
    :cond_4
    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1}, LaH/q;->a(LaH/A;)V

    .line 540
    return-void
.end method

.method public a(LaN/B;LaH/s;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 638
    invoke-direct {p0, p2}, LaH/o;->a(LaH/s;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 639
    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1, p0}, LaH/q;->a(LaN/B;LaH/o;)V

    .line 641
    :cond_b
    return-void
.end method

.method public b()LaH/h;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 513
    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_7

    move-object v0, v1

    .line 530
    :cond_6
    :goto_6
    return-object v0

    .line 517
    :cond_7
    invoke-virtual {p0}, LaH/o;->r()LaH/h;

    move-result-object v0

    .line 518
    if-nez v0, :cond_6

    .line 524
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 525
    invoke-interface {v0}, LaH/s;->d()LaH/h;

    move-result-object v0

    .line 526
    if-eqz v0, :cond_13

    goto :goto_6

    :cond_26
    move-object v0, v1

    .line 530
    goto :goto_6
.end method

.method public b(LaH/A;)V
    .registers 3
    .parameter

    .prologue
    .line 544
    iget-object v0, p0, LaH/o;->e:LaH/q;

    invoke-interface {v0, p1}, LaH/q;->b(LaH/A;)V

    .line 545
    return-void
.end method

.method public declared-synchronized c()Ljava/util/Map;
    .registers 5

    .prologue
    .line 359
    monitor-enter p0

    :try_start_1
    iget-wide v0, p0, LaH/o;->g:D

    const-wide v2, 0x409f400000000000L

    add-double/2addr v0, v2

    invoke-virtual {p0}, LaH/o;->o()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    long-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_19

    .line 361
    invoke-direct {p0}, LaH/o;->w()V

    .line 363
    :cond_19
    iget-object v0, p0, LaH/o;->f:Ljava/util/Map;
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_1d

    monitor-exit p0

    return-object v0

    .line 359
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 324
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_b

    invoke-interface {v0}, LaH/s;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public e()Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 384
    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_7

    .line 385
    const/4 v2, 0x1

    .line 400
    :goto_6
    return v2

    .line 389
    :cond_7
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 390
    invoke-interface {v0}, LaH/s;->h()Z

    move-result v4

    or-int/2addr v1, v4

    .line 391
    invoke-interface {v0}, LaH/s;->p()Z

    move-result v0

    if-eqz v0, :cond_e

    goto :goto_6

    :cond_26
    move v2, v1

    .line 400
    goto :goto_6
.end method

.method public f()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 405
    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_7

    move v0, v1

    .line 419
    :goto_6
    return v0

    .line 409
    :cond_7
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 410
    invoke-interface {v0}, LaH/s;->h()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v0}, LaH/s;->k()Z

    move-result v3

    if-eqz v3, :cond_2b

    invoke-interface {v0}, LaH/s;->j()Z

    move-result v0

    if-nez v0, :cond_d

    :cond_2b
    move v0, v1

    .line 412
    goto :goto_6

    .line 419
    :cond_2d
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 425
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_e

    invoke-interface {v0}, LaH/s;->q()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 354
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public i()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 710
    invoke-virtual {p0}, LaH/o;->s()LaH/h;

    move-result-object v1

    .line 711
    invoke-virtual {p0}, LaH/o;->g()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-static {v1}, LaH/h;->c(Landroid/location/Location;)I

    move-result v1

    if-lt v1, v0, :cond_12

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public j()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 549
    iget-boolean v0, p0, LaH/o;->c:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_b

    :cond_9
    move v0, v1

    .line 568
    :goto_a
    return v0

    .line 552
    :cond_b
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    .line 553
    if-nez v0, :cond_13

    move v0, v1

    .line 554
    goto :goto_a

    .line 556
    :cond_13
    invoke-interface {v0}, LaH/s;->q()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 557
    invoke-interface {v0}, LaH/s;->s()Z

    move-result v0

    goto :goto_a

    .line 563
    :cond_1e
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 564
    invoke-interface {v0}, LaH/s;->s()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 565
    const/4 v0, 0x1

    goto :goto_a

    :cond_38
    move v0, v1

    .line 568
    goto :goto_a
.end method

.method public declared-synchronized k()V
    .registers 2

    .prologue
    .line 446
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LaH/o;->c:Z

    .line 447
    invoke-direct {p0}, LaH/o;->y()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    .line 448
    monitor-exit p0

    return-void

    .line 446
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized l()V
    .registers 2

    .prologue
    .line 459
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, LaH/o;->c:Z

    .line 460
    invoke-direct {p0}, LaH/o;->y()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    .line 461
    monitor-exit p0

    return-void

    .line 459
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()Lcom/google/googlenav/common/a;
    .registers 2

    .prologue
    .line 290
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .registers 2

    .prologue
    .line 318
    invoke-virtual {p0}, LaH/o;->q()LaH/s;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_b

    invoke-interface {v0}, LaH/s;->a()Ljava/lang/String;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const-string v0, "none"

    goto :goto_a
.end method

.method protected q()LaH/s;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 577
    iget-boolean v0, p0, LaH/o;->j:Z

    if-nez v0, :cond_6

    .line 587
    :cond_5
    return-object v1

    .line 582
    :cond_6
    iget-object v0, p0, LaH/o;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/s;

    .line 583
    invoke-static {v0, v1}, LaH/o;->a(LaH/s;LaH/s;)Z

    move-result v3

    if-eqz v3, :cond_20

    :goto_1e
    move-object v1, v0

    .line 584
    goto :goto_c

    :cond_20
    move-object v0, v1

    goto :goto_1e
.end method

.method public r()LaH/h;
    .registers 2

    .prologue
    .line 717
    iget-object v0, p0, LaH/o;->h:LaH/h;

    return-object v0
.end method

.method public declared-synchronized run()V
    .registers 2

    .prologue
    .line 687
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, LaH/o;->y()V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_6

    .line 688
    monitor-exit p0

    return-void

    .line 687
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public s()LaH/h;
    .registers 2

    .prologue
    .line 722
    iget-object v0, p0, LaH/o;->i:LaH/h;

    return-object v0
.end method
