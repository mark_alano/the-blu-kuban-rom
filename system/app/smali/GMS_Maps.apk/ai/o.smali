.class public Lai/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/util/n;


# instance fields
.field private final a:Lcom/google/googlenav/android/Y;

.field private final b:LZ/c;

.field private c:Lai/a;

.field private d:Lai/s;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/Y;LZ/c;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lai/o;->a:Lcom/google/googlenav/android/Y;

    .line 38
    iput-object p2, p0, Lai/o;->b:LZ/c;

    .line 39
    return-void
.end method


# virtual methods
.method public a()Lai/a;
    .registers 5

    .prologue
    .line 48
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_28

    :cond_14
    iget-object v0, p0, Lai/o;->c:Lai/a;

    if-nez v0, :cond_28

    .line 52
    new-instance v0, Lai/r;

    invoke-direct {v0}, Lai/r;-><init>()V

    .line 54
    new-instance v1, Lai/a;

    iget-object v2, p0, Lai/o;->a:Lcom/google/googlenav/android/Y;

    iget-object v3, p0, Lai/o;->b:LZ/c;

    invoke-direct {v1, v2, v3, v0}, Lai/a;-><init>(Lcom/google/googlenav/android/Y;LZ/c;Lai/q;)V

    iput-object v1, p0, Lai/o;->c:Lai/a;

    .line 57
    :cond_28
    iget-object v0, p0, Lai/o;->c:Lai/a;

    return-object v0
.end method

.method public b()Lai/s;
    .registers 4

    .prologue
    .line 66
    iget-object v0, p0, Lai/o;->d:Lai/s;

    if-nez v0, :cond_f

    .line 67
    new-instance v0, Lai/s;

    const/16 v1, 0x64

    iget-object v2, p0, Lai/o;->a:Lcom/google/googlenav/android/Y;

    invoke-direct {v0, v1, v2}, Lai/s;-><init>(ILcom/google/googlenav/android/Y;)V

    iput-object v0, p0, Lai/o;->d:Lai/s;

    .line 69
    :cond_f
    iget-object v0, p0, Lai/o;->d:Lai/s;

    return-object v0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .registers 5

    .prologue
    .line 77
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lai/o;->c:Lai/a;

    if-eqz v1, :cond_18

    .line 79
    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v2, "friend photos"

    iget-object v3, p0, Lai/o;->c:Lai/a;

    invoke-virtual {v3}, Lai/a;->a()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_18
    iget-object v1, p0, Lai/o;->d:Lai/s;

    if-eqz v1, :cond_2c

    .line 84
    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v2, "media photos"

    iget-object v3, p0, Lai/o;->d:Lai/s;

    invoke-virtual {v3}, Lai/s;->b()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_2c
    const/4 v1, 0x0

    .line 89
    new-instance v2, Lcom/google/googlenav/common/util/l;

    const-string v3, "PhotoManager"

    invoke-direct {v2, v3, v1, v0}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v2
.end method
