.class public Lak/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lak/n;


# instance fields
.field protected a:Lak/m;

.field private b:Lak/k;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lak/g;->a:Lak/m;

    .line 56
    return-void
.end method

.method public a(Landroid/content/Context;Lak/m;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p2, p0, Lak/g;->a:Lak/m;

    .line 41
    new-instance v0, Lak/k;

    invoke-direct {v0, p1, p0}, Lak/k;-><init>(Landroid/content/Context;Lak/n;)V

    iput-object v0, p0, Lak/g;->b:Lak/k;

    .line 42
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 205
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 206
    iget-object v0, p0, Lak/g;->b:Lak/k;

    invoke-virtual {v0, p1}, Lak/k;->a(Z)V

    .line 207
    return-void
.end method

.method public a(Lak/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/d;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lak/d;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/u;)Z

    move-result v0

    return v0
.end method

.method public a(Lak/k;Z)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 64
    if-eqz p2, :cond_4

    .line 65
    const/4 v0, 0x1

    .line 68
    :goto_3
    return v0

    :cond_4
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/c;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lak/c;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/q;)Z

    move-result v0

    goto :goto_3
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lak/g;->b:Lak/k;

    invoke-virtual {v0, p1}, Lak/k;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public b(Lak/k;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 103
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/d;

    invoke-direct {v1, v2, p1}, Lak/d;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/u;)Z

    move-result v0

    .line 105
    if-eqz v0, :cond_10

    .line 107
    iput-boolean v2, p0, Lak/g;->c:Z

    .line 109
    :cond_10
    return v0
.end method

.method public b(Lak/k;Z)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 76
    if-eqz p2, :cond_4

    .line 80
    :goto_3
    return v0

    :cond_4
    iget-object v1, p0, Lak/g;->a:Lak/m;

    new-instance v2, Lak/c;

    invoke-direct {v2, v0, p1}, Lak/c;-><init>(ILak/k;)V

    invoke-interface {v1, v2}, Lak/m;->a(Lak/q;)Z

    move-result v0

    goto :goto_3
.end method

.method public c(Lak/k;)V
    .registers 5
    .parameter

    .prologue
    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lak/g;->c:Z

    .line 115
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/d;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lak/d;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/u;)Z

    .line 117
    return-void
.end method

.method public c(Lak/k;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 86
    if-eqz p2, :cond_e

    .line 87
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/c;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p1}, Lak/c;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/q;)Z

    .line 93
    :goto_d
    return-void

    .line 90
    :cond_e
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/c;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lak/c;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/q;)Z

    goto :goto_d
.end method

.method public d(Lak/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/b;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lak/b;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/o;)Z

    move-result v0

    return v0
.end method

.method public e(Lak/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/b;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lak/b;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/o;)Z

    move-result v0

    return v0
.end method

.method public f(Lak/k;)V
    .registers 5
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/b;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lak/b;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/o;)Z

    .line 135
    return-void
.end method

.method public g(Lak/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/s;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lak/s;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/o;)Z

    move-result v0

    return v0
.end method

.method public h(Lak/k;)Z
    .registers 5
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/s;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lak/s;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/o;)Z

    move-result v0

    return v0
.end method

.method public i(Lak/k;)V
    .registers 5
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lak/g;->a:Lak/m;

    new-instance v1, Lak/s;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lak/s;-><init>(ILak/k;)V

    invoke-interface {v0, v1}, Lak/m;->a(Lak/o;)Z

    .line 153
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 192
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1}, Lak/m;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 197
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1}, Lak/m;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1}, Lak/m;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lak/m;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1}, Lak/m;->onLongPress(Landroid/view/MotionEvent;)V

    .line 178
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    iget-boolean v0, p0, Lak/g;->c:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1, p2, p3, p4}, Lak/m;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1}, Lak/m;->onShowPress(Landroid/view/MotionEvent;)V

    .line 163
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1}, Lak/m;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lak/g;->a:Lak/m;

    invoke-interface {v0, p1}, Lak/m;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
