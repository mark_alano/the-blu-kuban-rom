.class Lak/i;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lak/h;


# direct methods
.method constructor <init>(Lak/h;)V
    .registers 2
    .parameter

    .prologue
    .line 110
    iput-object p1, p0, Lak/i;->a:Lak/h;

    .line 111
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 112
    return-void
.end method

.method constructor <init>(Lak/h;Landroid/os/Handler;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lak/i;->a:Lak/h;

    .line 115
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 116
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter

    .prologue
    .line 120
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_54

    .line 137
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :pswitch_1e
    iget-object v0, p0, Lak/i;->a:Lak/h;

    invoke-static {v0}, Lak/h;->b(Lak/h;)Landroid/view/GestureDetector$OnGestureListener;

    move-result-object v0

    iget-object v1, p0, Lak/i;->a:Lak/h;

    invoke-static {v1}, Lak/h;->a(Lak/h;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    .line 139
    :cond_2d
    :goto_2d
    return-void

    .line 126
    :pswitch_2e
    iget-object v0, p0, Lak/i;->a:Lak/h;

    invoke-static {v0}, Lak/h;->c(Lak/h;)V

    goto :goto_2d

    .line 131
    :pswitch_34
    iget-object v0, p0, Lak/i;->a:Lak/h;

    invoke-static {v0}, Lak/h;->d(Lak/h;)Landroid/view/GestureDetector$OnDoubleTapListener;

    move-result-object v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lak/i;->a:Lak/h;

    invoke-static {v0}, Lak/h;->e(Lak/h;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 132
    iget-object v0, p0, Lak/i;->a:Lak/h;

    invoke-static {v0}, Lak/h;->d(Lak/h;)Landroid/view/GestureDetector$OnDoubleTapListener;

    move-result-object v0

    iget-object v1, p0, Lak/i;->a:Lak/h;

    invoke-static {v1}, Lak/h;->a(Lak/h;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_2d

    .line 120
    :pswitch_data_54
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_2e
        :pswitch_34
    .end packed-switch
.end method
