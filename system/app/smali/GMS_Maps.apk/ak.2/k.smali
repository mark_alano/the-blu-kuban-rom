.class public Lak/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:F

.field private B:F

.field private C:J

.field private D:F

.field private E:F

.field private F:F

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Lak/h;

.field private a:Landroid/content/Context;

.field private b:Landroid/view/MotionEvent;

.field private c:Landroid/view/MotionEvent;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Lak/e;

.field private final g:Lak/e;

.field private final h:Lak/e;

.field private final i:Lak/e;

.field private final j:Ljava/util/LinkedList;

.field private k:J

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lak/n;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lak/k;->d:Ljava/util/List;

    .line 273
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lak/k;->e:Ljava/util/List;

    .line 280
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    .line 324
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 325
    iput-object p1, p0, Lak/k;->a:Landroid/content/Context;

    .line 326
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lak/k;->D:F

    .line 331
    iget-object v0, p0, Lak/k;->d:Ljava/util/List;

    new-instance v1, Lak/v;

    invoke-direct {v1, p2}, Lak/v;-><init>(Lak/n;)V

    iput-object v1, p0, Lak/k;->g:Lak/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->A()Z

    move-result v0

    if-eqz v0, :cond_77

    .line 336
    iget-object v0, p0, Lak/k;->d:Ljava/util/List;

    new-instance v1, Lak/p;

    invoke-direct {v1, p2}, Lak/p;-><init>(Lak/n;)V

    iput-object v1, p0, Lak/k;->h:Lak/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    const-string v0, "MD"

    const-string v1, "T"

    invoke-static {v0, v1}, LaU/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :goto_4c
    iget-object v0, p0, Lak/k;->d:Ljava/util/List;

    new-instance v1, Lak/r;

    invoke-direct {v1, p2}, Lak/r;-><init>(Lak/n;)V

    iput-object v1, p0, Lak/k;->f:Lak/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    iget-object v0, p0, Lak/k;->d:Ljava/util/List;

    new-instance v1, Lak/x;

    invoke-direct {v1, p2}, Lak/x;-><init>(Lak/n;)V

    iput-object v1, p0, Lak/k;->i:Lak/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    new-instance v0, Lak/h;

    invoke-direct {v0, p1, p2}, Lak/h;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lak/k;->K:Lak/h;

    .line 345
    iget-object v0, p0, Lak/k;->K:Lak/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lak/h;->a(Z)V

    .line 346
    iget-object v0, p0, Lak/k;->K:Lak/h;

    invoke-virtual {v0, p2}, Lak/h;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 347
    return-void

    .line 339
    :cond_77
    iget-object v0, p0, Lak/k;->d:Ljava/util/List;

    new-instance v1, Lak/t;

    invoke-direct {v1, p2}, Lak/t;-><init>(Lak/n;)V

    iput-object v1, p0, Lak/k;->h:Lak/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    const-string v0, "MD"

    const-string v1, "F"

    invoke-static {v0, v1}, LaU/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4c
.end method

.method private static a(Landroid/view/MotionEvent;I)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 601
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    .line 602
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private a(Lak/e;)Z
    .registers 3
    .parameter

    .prologue
    .line 496
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lak/e;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private static b(Landroid/view/MotionEvent;I)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 609
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    .line 610
    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .registers 11
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 517
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 521
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 522
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lak/k;->k:J

    .line 526
    :cond_13
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    new-instance v2, Lak/a;

    invoke-direct {v2, p1}, Lak/a;-><init>(Landroid/view/MotionEvent;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 529
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x14

    if-le v0, v2, :cond_32

    .line 530
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    invoke-virtual {v0}, Lak/j;->e()V

    .line 534
    :cond_32
    :goto_32
    invoke-direct {p0}, Lak/k;->l()Z

    move-result v0

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_4d

    .line 535
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    invoke-virtual {v0}, Lak/j;->e()V

    goto :goto_32

    .line 540
    :cond_4d
    sparse-switch v1, :sswitch_data_9c

    :goto_50
    move v4, v6

    .line 556
    :goto_51
    iget-boolean v0, p0, Lak/k;->J:Z

    if-eqz v0, :cond_83

    .line 557
    iget-object v0, p0, Lak/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5b
    :goto_5b
    :pswitch_5b
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_83

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/e;

    .line 560
    invoke-virtual {v0}, Lak/e;->a()Z

    move-result v1

    if-nez v1, :cond_5b

    .line 564
    sget-object v8, Lak/l;->a:[I

    iget-wide v1, p0, Lak/k;->k:J

    iget-object v3, p0, Lak/k;->j:Ljava/util/LinkedList;

    iget-object v5, p0, Lak/k;->e:Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, Lak/e;->a(JLjava/util/LinkedList;ZLjava/util/List;)Lak/f;

    move-result-object v1

    invoke-virtual {v1}, Lak/f;->ordinal()I

    move-result v1

    aget v1, v8, v1

    packed-switch v1, :pswitch_data_ae

    goto :goto_5b

    .line 585
    :cond_83
    :pswitch_83
    if-eqz v4, :cond_8a

    .line 586
    invoke-direct {p0}, Lak/k;->k()V

    .line 587
    iput-boolean v6, p0, Lak/k;->J:Z

    .line 589
    :cond_8a
    return-void

    .line 547
    :sswitch_8b
    const/4 v4, 0x1

    .line 548
    goto :goto_51

    .line 551
    :sswitch_8d
    iput-boolean v6, p0, Lak/k;->J:Z

    goto :goto_50

    .line 576
    :pswitch_90
    invoke-virtual {v0, p0}, Lak/e;->a(Lak/k;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 577
    iget-object v1, p0, Lak/k;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5b

    .line 540
    :sswitch_data_9c
    .sparse-switch
        0x1 -> :sswitch_8b
        0x3 -> :sswitch_8d
        0x6 -> :sswitch_8b
        0x106 -> :sswitch_8b
    .end sparse-switch

    .line 564
    :pswitch_data_ae
    .packed-switch 0x1
        :pswitch_5b
        :pswitch_83
        :pswitch_90
    .end packed-switch
.end method

.method private c(Landroid/view/MotionEvent;)V
    .registers 13
    .parameter

    .prologue
    const/high16 v1, -0x4080

    const/high16 v10, 0x3f00

    const/4 v9, 0x0

    .line 614
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lak/k;->c:Landroid/view/MotionEvent;

    .line 616
    iput v1, p0, Lak/k;->t:F

    .line 617
    iput v1, p0, Lak/k;->u:F

    .line 618
    iput v1, p0, Lak/k;->x:F

    .line 619
    const/4 v0, 0x0

    iput v0, p0, Lak/k;->y:F

    .line 620
    iput-boolean v9, p0, Lak/k;->H:Z

    .line 621
    iput-boolean v9, p0, Lak/k;->I:Z

    .line 623
    iget-object v0, p0, Lak/k;->b:Landroid/view/MotionEvent;

    .line 625
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 626
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 627
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 628
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 629
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    .line 630
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 631
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    .line 632
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    .line 634
    sub-float/2addr v3, v1

    .line 635
    sub-float/2addr v4, v2

    .line 636
    sub-float/2addr v7, v5

    .line 637
    sub-float/2addr v8, v6

    .line 638
    iput v3, p0, Lak/k;->p:F

    .line 639
    iput v4, p0, Lak/k;->q:F

    .line 640
    iput v7, p0, Lak/k;->r:F

    .line 641
    iput v8, p0, Lak/k;->s:F

    .line 642
    iput v2, p0, Lak/k;->v:F

    .line 643
    iput v6, p0, Lak/k;->w:F

    .line 645
    mul-float/2addr v7, v10

    add-float/2addr v5, v7

    iput v5, p0, Lak/k;->l:F

    .line 646
    mul-float v5, v8, v10

    add-float/2addr v5, v6

    iput v5, p0, Lak/k;->m:F

    .line 647
    mul-float/2addr v3, v10

    add-float/2addr v1, v3

    iput v1, p0, Lak/k;->n:F

    .line 648
    mul-float v1, v4, v10

    add-float/2addr v1, v2

    iput v1, p0, Lak/k;->o:F

    .line 649
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lak/k;->C:J

    .line 650
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lak/k;->A:F

    .line 651
    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v0

    add-float/2addr v0, v1

    iput v0, p0, Lak/k;->B:F

    .line 652
    return-void
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 492
    iget-object v0, p0, Lak/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private j()Z
    .registers 4

    .prologue
    .line 500
    const/4 v0, 0x0

    .line 501
    iget-object v1, p0, Lak/k;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/e;

    .line 503
    invoke-virtual {v0, p0}, Lak/e;->e(Lak/k;)Z

    move-result v0

    .line 504
    or-int/2addr v0, v1

    move v1, v0

    .line 505
    goto :goto_8

    .line 506
    :cond_1b
    return v1
.end method

.method private k()V
    .registers 3

    .prologue
    .line 510
    iget-object v0, p0, Lak/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/e;

    .line 512
    invoke-virtual {v0, p0}, Lak/e;->c(Lak/k;)V

    goto :goto_6

    .line 514
    :cond_16
    return-void
.end method

.method private l()Z
    .registers 6

    .prologue
    .line 592
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    invoke-virtual {v0}, Lak/j;->a()J

    move-result-wide v1

    .line 593
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    invoke-virtual {v0}, Lak/j;->a()J

    move-result-wide v3

    .line 594
    sub-long v0, v3, v1

    const-wide/16 v2, 0xfa

    cmp-long v0, v0, v2

    if-ltz v0, :cond_22

    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method private m()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 655
    iput-object v1, p0, Lak/k;->b:Landroid/view/MotionEvent;

    .line 656
    iput-object v1, p0, Lak/k;->c:Landroid/view/MotionEvent;

    .line 657
    iput-boolean v0, p0, Lak/k;->G:Z

    .line 658
    iput-boolean v0, p0, Lak/k;->J:Z

    .line 659
    iget-object v0, p0, Lak/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 660
    invoke-direct {p0}, Lak/k;->n()V

    .line 661
    iget-object v0, p0, Lak/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_18
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/e;

    .line 662
    invoke-virtual {v0}, Lak/e;->a()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 663
    invoke-virtual {v0, p0}, Lak/e;->c(Lak/k;)V

    goto :goto_18

    .line 666
    :cond_2e
    return-void
.end method

.method private n()V
    .registers 3

    .prologue
    .line 669
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    .line 670
    invoke-virtual {v0}, Lak/j;->e()V

    goto :goto_6

    .line 672
    :cond_16
    iget-object v0, p0, Lak/k;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 673
    return-void
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 687
    iget v0, p0, Lak/k;->l:F

    return v0
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 851
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 852
    iget-object v0, p0, Lak/k;->K:Lak/h;

    invoke-virtual {v0, p1}, Lak/h;->a(Z)V

    .line 853
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 13
    .parameter

    .prologue
    const/high16 v10, -0x4080

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 350
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 352
    const v3, 0xff00

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x8

    .line 354
    iget-boolean v4, p0, Lak/k;->J:Z

    if-nez v4, :cond_17c

    .line 355
    const/4 v4, 0x5

    if-eq v1, v4, :cond_1b

    const/16 v4, 0x105

    if-eq v1, v4, :cond_1b

    if-nez v1, :cond_ce

    .line 359
    :cond_1b
    iget-object v1, p0, Lak/k;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 360
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p0, Lak/k;->D:F

    sub-float/2addr v3, v4

    iput v3, p0, Lak/k;->E:F

    .line 361
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v3, p0, Lak/k;->D:F

    sub-float/2addr v1, v3

    iput v1, p0, Lak/k;->F:F

    .line 364
    invoke-direct {p0}, Lak/k;->m()V

    .line 366
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lak/k;->b:Landroid/view/MotionEvent;

    .line 367
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lak/k;->C:J

    .line 369
    invoke-direct {p0, p1}, Lak/k;->c(Landroid/view/MotionEvent;)V

    .line 374
    iget v1, p0, Lak/k;->D:F

    .line 375
    iget v4, p0, Lak/k;->E:F

    .line 376
    iget v5, p0, Lak/k;->F:F

    .line 377
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 378
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    .line 379
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Lak/k;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 380
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, Lak/k;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    .line 382
    cmpg-float v9, v3, v1

    if-ltz v9, :cond_77

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_77

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_77

    cmpl-float v3, v6, v5

    if-lez v3, :cond_99

    :cond_77
    move v3, v2

    .line 383
    :goto_78
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_88

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_88

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_88

    cmpl-float v1, v8, v5

    if-lez v1, :cond_9b

    :cond_88
    move v1, v2

    .line 385
    :goto_89
    if-eqz v3, :cond_9d

    if-eqz v1, :cond_9d

    .line 386
    iput v10, p0, Lak/k;->l:F

    .line 387
    iput v10, p0, Lak/k;->m:F

    .line 388
    iput-boolean v2, p0, Lak/k;->G:Z

    .line 487
    :cond_93
    :goto_93
    iget-object v0, p0, Lak/k;->K:Lak/h;

    invoke-virtual {v0, p1}, Lak/h;->a(Landroid/view/MotionEvent;)Z

    .line 488
    return v2

    :cond_99
    move v3, v0

    .line 382
    goto :goto_78

    :cond_9b
    move v1, v0

    .line 383
    goto :goto_89

    .line 389
    :cond_9d
    if-eqz v3, :cond_ba

    .line 390
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lak/k;->l:F

    .line 391
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lak/k;->m:F

    .line 392
    iput-boolean v2, p0, Lak/k;->G:Z

    goto :goto_93

    .line 393
    :cond_ba
    if-eqz v1, :cond_cb

    .line 394
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lak/k;->l:F

    .line 395
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lak/k;->m:F

    .line 396
    iput-boolean v2, p0, Lak/k;->G:Z

    goto :goto_93

    .line 398
    :cond_cb
    iput-boolean v2, p0, Lak/k;->J:Z

    goto :goto_93

    .line 400
    :cond_ce
    const/4 v4, 0x2

    if-ne v1, v4, :cond_159

    iget-boolean v4, p0, Lak/k;->G:Z

    if-eqz v4, :cond_159

    .line 402
    iget v1, p0, Lak/k;->D:F

    .line 403
    iget v4, p0, Lak/k;->E:F

    .line 404
    iget v5, p0, Lak/k;->F:F

    .line 405
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 406
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    .line 407
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Lak/k;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 408
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, Lak/k;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    .line 410
    cmpg-float v9, v3, v1

    if-ltz v9, :cond_107

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_107

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_107

    cmpl-float v3, v6, v5

    if-lez v3, :cond_123

    :cond_107
    move v3, v2

    .line 411
    :goto_108
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_118

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_118

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_118

    cmpl-float v1, v8, v5

    if-lez v1, :cond_125

    :cond_118
    move v1, v2

    .line 413
    :goto_119
    if-eqz v3, :cond_127

    if-eqz v1, :cond_127

    .line 414
    iput v10, p0, Lak/k;->l:F

    .line 415
    iput v10, p0, Lak/k;->m:F

    goto/16 :goto_93

    :cond_123
    move v3, v0

    .line 410
    goto :goto_108

    :cond_125
    move v1, v0

    .line 411
    goto :goto_119

    .line 416
    :cond_127
    if-eqz v3, :cond_143

    .line 417
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lak/k;->l:F

    .line 418
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lak/k;->m:F

    goto/16 :goto_93

    .line 419
    :cond_143
    if-eqz v1, :cond_153

    .line 420
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lak/k;->l:F

    .line 421
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lak/k;->m:F

    goto/16 :goto_93

    .line 423
    :cond_153
    iput-boolean v0, p0, Lak/k;->G:Z

    .line 424
    iput-boolean v2, p0, Lak/k;->J:Z

    goto/16 :goto_93

    .line 426
    :cond_159
    const/4 v4, 0x6

    if-eq v1, v4, :cond_162

    const/16 v4, 0x106

    if-eq v1, v4, :cond_162

    if-ne v1, v2, :cond_93

    :cond_162
    iget-boolean v1, p0, Lak/k;->G:Z

    if-eqz v1, :cond_93

    .line 431
    if-nez v3, :cond_16e

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 432
    :cond_16e
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lak/k;->l:F

    .line 433
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lak/k;->m:F

    goto/16 :goto_93

    .line 435
    :cond_17c
    invoke-direct {p0}, Lak/k;->i()Z

    move-result v4

    if-nez v4, :cond_18b

    .line 438
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lak/k;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_93

    .line 443
    :cond_18b
    sparse-switch v1, :sswitch_data_1e2

    goto/16 :goto_93

    .line 448
    :sswitch_190
    invoke-direct {p0, p1}, Lak/k;->c(Landroid/view/MotionEvent;)V

    .line 451
    if-nez v3, :cond_19b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 452
    :cond_19b
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lak/k;->l:F

    .line 453
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lak/k;->m:F

    .line 455
    iget-boolean v0, p0, Lak/k;->G:Z

    if-nez v0, :cond_1ae

    .line 456
    invoke-direct {p0}, Lak/k;->k()V

    .line 459
    :cond_1ae
    invoke-direct {p0}, Lak/k;->m()V

    goto/16 :goto_93

    .line 463
    :sswitch_1b3
    iget-boolean v0, p0, Lak/k;->G:Z

    if-nez v0, :cond_1ba

    .line 464
    invoke-direct {p0}, Lak/k;->k()V

    .line 467
    :cond_1ba
    invoke-direct {p0}, Lak/k;->m()V

    goto/16 :goto_93

    .line 471
    :sswitch_1bf
    invoke-direct {p0, p1}, Lak/k;->c(Landroid/view/MotionEvent;)V

    .line 474
    iget-object v0, p0, Lak/k;->c:Landroid/view/MotionEvent;

    invoke-direct {p0, v0}, Lak/k;->b(Landroid/view/MotionEvent;)V

    .line 479
    iget v0, p0, Lak/k;->A:F

    iget v1, p0, Lak/k;->B:F

    div-float/2addr v0, v1

    const v1, 0x3f2b851f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_93

    .line 480
    invoke-direct {p0}, Lak/k;->j()Z

    move-result v0

    if-eqz v0, :cond_93

    .line 481
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lak/k;->b:Landroid/view/MotionEvent;

    goto/16 :goto_93

    .line 443
    nop

    :sswitch_data_1e2
    .sparse-switch
        0x1 -> :sswitch_190
        0x2 -> :sswitch_1bf
        0x3 -> :sswitch_1b3
        0x6 -> :sswitch_190
        0x106 -> :sswitch_190
    .end sparse-switch
.end method

.method public b()F
    .registers 2

    .prologue
    .line 702
    iget v0, p0, Lak/k;->m:F

    return v0
.end method

.method public c()F
    .registers 2

    .prologue
    .line 715
    iget v0, p0, Lak/k;->n:F

    return v0
.end method

.method public d()F
    .registers 3

    .prologue
    .line 738
    iget v0, p0, Lak/k;->t:F

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-nez v0, :cond_17

    .line 739
    iget v0, p0, Lak/k;->r:F

    .line 740
    iget v1, p0, Lak/k;->s:F

    .line 741
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lak/k;->t:F

    .line 743
    :cond_17
    iget v0, p0, Lak/k;->t:F

    return v0
.end method

.method public e()F
    .registers 3

    .prologue
    .line 753
    iget v0, p0, Lak/k;->u:F

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-nez v0, :cond_17

    .line 754
    iget v0, p0, Lak/k;->p:F

    .line 755
    iget v1, p0, Lak/k;->q:F

    .line 756
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lak/k;->u:F

    .line 758
    :cond_17
    iget v0, p0, Lak/k;->u:F

    return v0
.end method

.method public f()F
    .registers 4

    .prologue
    const/high16 v0, 0x3f80

    .line 769
    iget-object v1, p0, Lak/k;->f:Lak/e;

    invoke-direct {p0, v1}, Lak/k;->a(Lak/e;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 778
    :cond_a
    :goto_a
    return v0

    .line 772
    :cond_b
    iget-object v1, p0, Lak/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, Lak/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_a

    .line 775
    iget v0, p0, Lak/k;->x:F

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2c

    .line 776
    invoke-virtual {p0}, Lak/k;->d()F

    move-result v0

    invoke-virtual {p0}, Lak/k;->e()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lak/k;->x:F

    .line 778
    :cond_2c
    iget v0, p0, Lak/k;->x:F

    goto :goto_a
.end method

.method public g()F
    .registers 3

    .prologue
    .line 792
    iget-object v0, p0, Lak/k;->g:Lak/e;

    invoke-direct {p0, v0}, Lak/k;->a(Lak/e;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 793
    const/4 v0, 0x0

    .line 801
    :goto_9
    return v0

    .line 795
    :cond_a
    iget-boolean v0, p0, Lak/k;->H:Z

    if-nez v0, :cond_1b

    .line 798
    iget v0, p0, Lak/k;->w:F

    iget v1, p0, Lak/k;->v:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x3e80

    mul-float/2addr v0, v1

    iput v0, p0, Lak/k;->y:F

    .line 799
    const/4 v0, 0x1

    iput-boolean v0, p0, Lak/k;->H:Z

    .line 801
    :cond_1b
    iget v0, p0, Lak/k;->y:F

    goto :goto_9
.end method

.method public h()F
    .registers 7

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 809
    iget-object v1, p0, Lak/k;->h:Lak/e;

    invoke-direct {p0, v1}, Lak/k;->a(Lak/e;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 827
    :cond_a
    :goto_a
    return v0

    .line 812
    :cond_b
    iget-object v1, p0, Lak/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, Lak/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_a

    .line 815
    iget-boolean v0, p0, Lak/k;->I:Z

    if-nez v0, :cond_7e

    .line 816
    iget-object v0, p0, Lak/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget-object v1, p0, Lak/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v2, p0, Lak/k;->c:Landroid/view/MotionEvent;

    iget-object v3, p0, Lak/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget-object v3, p0, Lak/k;->c:Landroid/view/MotionEvent;

    iget-object v4, p0, Lak/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lak/j;->a(FFFF)F

    move-result v0

    .line 820
    iget-object v1, p0, Lak/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iget-object v2, p0, Lak/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget-object v3, p0, Lak/k;->b:Landroid/view/MotionEvent;

    iget-object v4, p0, Lak/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lak/k;->b:Landroid/view/MotionEvent;

    iget-object v5, p0, Lak/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lak/j;->a(FFFF)F

    move-result v1

    .line 824
    invoke-static {v1, v0}, Lak/e;->a(FF)F

    move-result v0

    iput v0, p0, Lak/k;->z:F

    .line 825
    const/4 v0, 0x1

    iput-boolean v0, p0, Lak/k;->I:Z

    .line 827
    :cond_7e
    iget v0, p0, Lak/k;->z:F

    goto :goto_a
.end method
