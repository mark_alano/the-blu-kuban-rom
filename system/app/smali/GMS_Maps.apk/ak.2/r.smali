.class public Lak/r;
.super Lak/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lak/n;)V
    .registers 2
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lak/e;-><init>(Lak/n;)V

    .line 63
    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;)Lak/f;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 78
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_a

    .line 79
    sget-object v0, Lak/f;->b:Lak/f;

    .line 113
    :goto_9
    return-object v0

    .line 84
    :cond_a
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    .line 85
    invoke-virtual {v0}, Lak/j;->f()F

    move-result v3

    .line 87
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v4

    move-object v2, v0

    .line 88
    :goto_1d
    invoke-interface {v4}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 89
    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lak/j;

    .line 90
    invoke-virtual {v1}, Lak/j;->b()I

    move-result v5

    invoke-virtual {v0}, Lak/j;->b()I

    move-result v6

    if-eq v5, v6, :cond_5e

    .line 101
    :cond_33
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_76

    const v1, 0x3dcccccd

    .line 105
    :goto_3c
    invoke-virtual {v2}, Lak/j;->g()F

    move-result v2

    .line 106
    invoke-virtual {v0}, Lak/j;->g()F

    move-result v3

    .line 107
    invoke-virtual {v0}, Lak/j;->c()F

    move-result v4

    invoke-virtual {v0}, Lak/j;->d()F

    move-result v0

    add-float/2addr v0, v4

    const/high16 v4, 0x3f00

    mul-float/2addr v0, v4

    .line 108
    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float v0, v2, v0

    .line 109
    cmpg-float v0, v0, v1

    if-gez v0, :cond_7a

    .line 110
    sget-object v0, Lak/f;->b:Lak/f;

    goto :goto_9

    .line 93
    :cond_5e
    invoke-virtual {v1}, Lak/j;->f()F

    move-result v2

    invoke-static {v3, v2}, Lak/r;->a(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v5, 0x3e32b8c2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_74

    .line 95
    sget-object v0, Lak/f;->a:Lak/f;

    goto :goto_9

    :cond_74
    move-object v2, v1

    .line 98
    goto :goto_1d

    .line 101
    :cond_76
    const v1, 0x3e4ccccd

    goto :goto_3c

    .line 113
    :cond_7a
    sget-object v0, Lak/f;->c:Lak/f;

    goto :goto_9
.end method

.method protected b(Lak/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, Lak/r;->a:Lak/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lak/n;->b(Lak/k;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method protected d(Lak/k;)V
    .registers 4
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lak/r;->a:Lak/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lak/n;->c(Lak/k;Z)V

    .line 124
    return-void
.end method

.method protected f(Lak/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lak/r;->a:Lak/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lak/n;->a(Lak/k;Z)Z

    move-result v0

    return v0
.end method
