.class public final enum Lan/g;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lan/g;

.field public static final enum b:Lan/g;

.field private static final synthetic c:[Lan/g;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lan/g;

    const-string v1, "AUTO_SCALE_ENABLED"

    invoke-direct {v0, v1, v2}, Lan/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lan/g;->a:Lan/g;

    .line 24
    new-instance v0, Lan/g;

    const-string v1, "AUTO_SCALE_DISABLED"

    invoke-direct {v0, v1, v3}, Lan/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lan/g;->b:Lan/g;

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [Lan/g;

    sget-object v1, Lan/g;->a:Lan/g;

    aput-object v1, v0, v2

    sget-object v1, Lan/g;->b:Lan/g;

    aput-object v1, v0, v3

    sput-object v0, Lan/g;->c:[Lan/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lan/g;
    .registers 2
    .parameter

    .prologue
    .line 22
    const-class v0, Lan/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lan/g;

    return-object v0
.end method

.method public static values()[Lan/g;
    .registers 1

    .prologue
    .line 22
    sget-object v0, Lan/g;->c:[Lan/g;

    invoke-virtual {v0}, [Lan/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lan/g;

    return-object v0
.end method
