.class public Lao/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lao/h;
.implements Lao/q;
.implements Ljava/lang/Runnable;


# static fields
.field private static volatile k:Lao/l;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field c:Z

.field d:Z

.field private e:Lao/n;

.field private volatile f:Ljava/util/Map;

.field private g:D

.field private h:Lao/s;

.field private i:Lao/s;

.field private j:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v2, Lao/o;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lao/o;-><init>(Lao/m;)V

    iput-object v2, p0, Lao/l;->e:Lao/n;

    .line 108
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lao/l;->a:Ljava/util/List;

    .line 114
    invoke-static {}, Lcom/google/common/collect/Maps;->b()Ljava/util/LinkedHashMap;

    move-result-object v2

    iput-object v2, p0, Lao/l;->f:Ljava/util/Map;

    .line 118
    const-wide/high16 v2, -0x3c20

    iput-wide v2, p0, Lao/l;->g:D

    .line 130
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lao/l;->b:Ljava/util/List;

    .line 133
    iput-boolean v0, p0, Lao/l;->c:Z

    .line 137
    iput-boolean v1, p0, Lao/l;->d:Z

    .line 153
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-nez v2, :cond_34

    :goto_2f
    iput-boolean v0, p0, Lao/l;->j:Z

    .line 192
    if-nez p1, :cond_36

    .line 209
    :goto_33
    return-void

    :cond_34
    move v0, v1

    .line 153
    goto :goto_2f

    .line 200
    :cond_36
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_45

    .line 202
    invoke-static {p2}, Lcom/google/common/collect/cx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lao/l;->a:Ljava/util/List;

    .line 204
    invoke-direct {p0}, Lao/l;->v()V

    .line 207
    :cond_45
    invoke-direct {p0}, Lao/l;->t()V

    .line 208
    invoke-direct {p0}, Lao/l;->u()V

    goto :goto_33
.end method

.method private static a(Lau/B;)Lau/B;
    .registers 2
    .parameter

    .prologue
    .line 730
    invoke-static {p0}, Lao/A;->e(Lau/B;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 731
    invoke-static {}, Lao/A;->k()Lao/A;

    move-result-object v0

    invoke-virtual {v0, p0}, Lao/A;->d(Lau/B;)Lau/B;

    move-result-object p0

    .line 733
    :cond_e
    return-object p0
.end method

.method public static a(Lao/l;)V
    .registers 1
    .parameter

    .prologue
    .line 167
    sput-object p0, Lao/l;->k:Lao/l;

    .line 168
    return-void
.end method

.method private a(Lao/s;)V
    .registers 5
    .parameter

    .prologue
    .line 648
    if-eqz p1, :cond_53

    .line 649
    iput-object p1, p0, Lao/l;->h:Lao/s;

    .line 650
    monitor-enter p0

    .line 651
    :try_start_5
    iget-object v0, p0, Lao/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/i;

    .line 652
    iget-object v2, p0, Lao/l;->h:Lao/s;

    invoke-interface {v0, v2}, Lao/i;->a(Lao/s;)Lao/s;

    move-result-object v0

    iput-object v0, p0, Lao/l;->h:Lao/s;

    goto :goto_b

    .line 654
    :catchall_20
    move-exception v0

    monitor-exit p0
    :try_end_22
    .catchall {:try_start_5 .. :try_end_22} :catchall_20

    throw v0

    :cond_23
    :try_start_23
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_20

    .line 655
    iget-object v0, p0, Lao/l;->h:Lao/s;

    iput-object v0, p0, Lao/l;->i:Lao/s;

    .line 656
    iget-object v0, p0, Lao/l;->h:Lao/s;

    invoke-virtual {v0}, Lao/s;->a()Lau/B;

    move-result-object v0

    invoke-static {v0}, Lao/l;->a(Lau/B;)Lau/B;

    move-result-object v0

    .line 657
    iget-object v1, p0, Lao/l;->h:Lao/s;

    invoke-virtual {v1}, Lao/s;->a()Lau/B;

    move-result-object v1

    invoke-virtual {v1, v0}, Lau/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_53

    .line 658
    new-instance v1, Lao/u;

    invoke-direct {v1}, Lao/u;-><init>()V

    iget-object v2, p0, Lao/l;->h:Lao/s;

    invoke-virtual {v1, v2}, Lao/u;->a(Lao/s;)Lao/u;

    move-result-object v1

    invoke-virtual {v1, v0}, Lao/u;->a(Lau/B;)Lao/u;

    move-result-object v0

    invoke-virtual {v0}, Lao/u;->b()Lao/s;

    move-result-object v0

    iput-object v0, p0, Lao/l;->i:Lao/s;

    .line 664
    :cond_53
    return-void
.end method

.method private a(Lao/j;)Z
    .registers 3
    .parameter

    .prologue
    .line 633
    invoke-virtual {p0}, Lao/l;->s()Lao/j;

    move-result-object v0

    .line 634
    if-eq v0, p1, :cond_8

    .line 635
    const/4 v0, 0x0

    .line 640
    :goto_7
    return v0

    .line 639
    :cond_8
    invoke-interface {v0}, Lao/j;->r()Lao/s;

    move-result-object v0

    invoke-direct {p0, v0}, Lao/l;->a(Lao/s;)V

    .line 640
    const/4 v0, 0x1

    goto :goto_7
.end method

.method protected static a(Lao/j;Lao/j;)Z
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 581
    if-nez p0, :cond_6

    move v0, v1

    .line 618
    :cond_5
    :goto_5
    return v0

    .line 586
    :cond_6
    if-nez p1, :cond_16

    .line 590
    invoke-interface {p0}, Lao/j;->m()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {p0}, Lao/j;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_14
    move v0, v1

    goto :goto_5

    .line 594
    :cond_16
    invoke-interface {p0}, Lao/j;->r()Lao/s;

    move-result-object v2

    .line 595
    invoke-interface {p1}, Lao/j;->r()Lao/s;

    move-result-object v3

    .line 596
    if-nez v2, :cond_22

    move v0, v1

    .line 597
    goto :goto_5

    .line 599
    :cond_22
    if-eqz v3, :cond_5

    .line 605
    invoke-virtual {v2}, Lao/s;->getTime()J

    move-result-wide v4

    invoke-virtual {v3}, Lao/s;->getTime()J

    move-result-wide v6

    const-wide/16 v8, 0x2af8

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_5

    .line 612
    invoke-virtual {v2}, Lao/s;->hasAccuracy()Z

    move-result v4

    if-nez v4, :cond_3b

    move v0, v1

    .line 613
    goto :goto_5

    .line 615
    :cond_3b
    invoke-virtual {v3}, Lao/s;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 618
    invoke-virtual {v2}, Lao/s;->getAccuracy()F

    move-result v2

    invoke-virtual {v3}, Lao/s;->getAccuracy()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_5

    move v0, v1

    goto :goto_5
.end method

.method static synthetic b(Lao/l;)V
    .registers 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lao/l;->u()V

    return-void
.end method

.method public static p()Lao/h;
    .registers 1

    .prologue
    .line 163
    sget-object v0, Lao/l;->k:Lao/l;

    return-object v0
.end method

.method public static q()V
    .registers 1

    .prologue
    .line 172
    const/4 v0, 0x0

    sput-object v0, Lao/l;->k:Lao/l;

    .line 173
    return-void
.end method

.method private t()V
    .registers 4

    .prologue
    .line 212
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->D()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lao/m;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lao/m;-><init>(Lao/l;Landroid/os/Handler;)V

    invoke-static {v0, v1}, Lao/p;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 224
    return-void
.end method

.method private declared-synchronized u()V
    .registers 3

    .prologue
    .line 227
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lao/l;->j:Z

    .line 228
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->D()Landroid/content/Context;

    move-result-object v1

    .line 229
    invoke-static {v1}, Lao/p;->c(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lao/l;->j:Z

    .line 230
    iget-boolean v1, p0, Lao/l;->j:Z
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_2c

    if-ne v1, v0, :cond_17

    .line 243
    :cond_15
    :goto_15
    monitor-exit p0

    return-void

    .line 234
    :cond_17
    :try_start_17
    iget-boolean v0, p0, Lao/l;->j:Z

    if-eqz v0, :cond_2f

    iget-boolean v0, p0, Lao/l;->d:Z

    if-eqz v0, :cond_2f

    .line 235
    invoke-direct {p0}, Lao/l;->x()V

    .line 236
    invoke-virtual {p0}, Lao/l;->l()V

    .line 239
    iget-object v0, p0, Lao/l;->e:Lao/n;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p0}, Lao/n;->a(Lau/B;Lao/l;)V
    :try_end_2b
    .catchall {:try_start_17 .. :try_end_2b} :catchall_2c

    goto :goto_15

    .line 227
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 240
    :cond_2f
    :try_start_2f
    iget-boolean v0, p0, Lao/l;->j:Z

    if-nez v0, :cond_15

    iget-boolean v0, p0, Lao/l;->c:Z

    if-eqz v0, :cond_15

    .line 241
    invoke-virtual {p0}, Lao/l;->k()V
    :try_end_3a
    .catchall {:try_start_2f .. :try_end_3a} :catchall_2c

    goto :goto_15
.end method

.method private v()V
    .registers 3

    .prologue
    .line 261
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 262
    invoke-interface {v0, p0}, Lao/j;->a(Lao/q;)V

    goto :goto_6

    .line 264
    :cond_16
    return-void
.end method

.method private w()V
    .registers 8

    .prologue
    .line 363
    iget-object v0, p0, Lao/l;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 364
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 365
    new-instance v2, Lao/z;

    invoke-interface {v0}, Lao/j;->m()Z

    move-result v3

    invoke-interface {v0}, Lao/j;->n()Z

    move-result v4

    invoke-interface {v0}, Lao/j;->r()Lao/s;

    move-result-object v5

    invoke-interface {v0}, Lao/j;->d()Lao/s;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lao/z;-><init>(ZZLao/s;Lao/s;)V

    .line 370
    iget-object v3, p0, Lao/l;->f:Ljava/util/Map;

    invoke-interface {v0}, Lao/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 372
    :cond_36
    invoke-virtual {p0}, Lao/l;->r()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    long-to-double v0, v0

    iput-wide v0, p0, Lao/l;->g:D

    .line 373
    return-void
.end method

.method private x()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 433
    iput-object v0, p0, Lao/l;->h:Lao/s;

    .line 434
    iput-object v0, p0, Lao/l;->i:Lao/s;

    .line 435
    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 268
    iget-boolean v0, p0, Lao/l;->j:Z

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 278
    :goto_6
    return-object v0

    .line 273
    :cond_7
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 274
    invoke-interface {v0}, Lao/j;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 275
    invoke-interface {v0}, Lao/j;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    goto :goto_6

    :cond_24
    move-object v0, v1

    .line 278
    goto :goto_6
.end method

.method public a(ILao/j;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 688
    invoke-virtual {p0}, Lao/l;->s()Lao/j;

    move-result-object v0

    .line 692
    if-eqz v0, :cond_9

    if-eq v0, p2, :cond_9

    .line 696
    :goto_8
    return-void

    .line 695
    :cond_9
    iget-object v0, p0, Lao/l;->e:Lao/n;

    invoke-interface {v0, p1, p0}, Lao/n;->a(ILao/l;)V

    goto :goto_8
.end method

.method public a(Lao/y;)V
    .registers 3
    .parameter

    .prologue
    .line 520
    iget-boolean v0, p0, Lao/l;->d:Z

    if-eqz v0, :cond_4

    .line 524
    :cond_4
    iget-object v0, p0, Lao/l;->e:Lao/n;

    invoke-interface {v0, p1}, Lao/n;->a(Lao/y;)V

    .line 525
    return-void
.end method

.method public a(Lau/B;Lao/j;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 623
    invoke-direct {p0, p2}, Lao/l;->a(Lao/j;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 624
    iget-object v0, p0, Lao/l;->e:Lao/n;

    invoke-interface {v0, p1, p0}, Lao/n;->a(Lau/B;Lao/l;)V

    .line 626
    :cond_b
    return-void
.end method

.method public b()Lao/s;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 498
    iget-boolean v0, p0, Lao/l;->j:Z

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 515
    :cond_6
    :goto_6
    return-object v0

    .line 502
    :cond_7
    invoke-virtual {p0}, Lao/l;->n()Lao/s;

    move-result-object v0

    .line 503
    if-nez v0, :cond_6

    .line 509
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 510
    invoke-interface {v0}, Lao/j;->d()Lao/s;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_13

    goto :goto_6

    :cond_26
    move-object v0, v1

    .line 515
    goto :goto_6
.end method

.method public b(Lao/y;)V
    .registers 3
    .parameter

    .prologue
    .line 529
    iget-object v0, p0, Lao/l;->e:Lao/n;

    invoke-interface {v0, p1}, Lao/n;->b(Lao/y;)V

    .line 530
    return-void
.end method

.method public declared-synchronized c()Ljava/util/Map;
    .registers 5

    .prologue
    .line 352
    monitor-enter p0

    :try_start_1
    iget-wide v0, p0, Lao/l;->g:D

    const-wide v2, 0x409f400000000000L

    add-double/2addr v0, v2

    invoke-virtual {p0}, Lao/l;->r()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    long-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_19

    .line 354
    invoke-direct {p0}, Lao/l;->w()V

    .line 356
    :cond_19
    iget-object v0, p0, Lao/l;->f:Ljava/util/Map;
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_1d

    monitor-exit p0

    return-object v0

    .line 352
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 317
    invoke-virtual {p0}, Lao/l;->s()Lao/j;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_b

    invoke-interface {v0}, Lao/j;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public e()Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 377
    iget-boolean v0, p0, Lao/l;->j:Z

    if-eqz v0, :cond_7

    .line 378
    const/4 v2, 0x1

    .line 393
    :goto_6
    return v2

    .line 382
    :cond_7
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 383
    invoke-interface {v0}, Lao/j;->f()Z

    move-result v4

    or-int/2addr v1, v4

    .line 384
    invoke-interface {v0}, Lao/j;->n()Z

    move-result v0

    if-eqz v0, :cond_e

    goto :goto_6

    :cond_26
    move v2, v1

    .line 393
    goto :goto_6
.end method

.method public f()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 398
    iget-boolean v0, p0, Lao/l;->j:Z

    if-eqz v0, :cond_7

    move v0, v1

    .line 412
    :goto_6
    return v0

    .line 402
    :cond_7
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 403
    invoke-interface {v0}, Lao/j;->f()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v0}, Lao/j;->i()Z

    move-result v3

    if-eqz v3, :cond_2b

    invoke-interface {v0}, Lao/j;->h()Z

    move-result v0

    if-nez v0, :cond_d

    :cond_2b
    move v0, v1

    .line 405
    goto :goto_6

    .line 412
    :cond_2d
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 418
    invoke-virtual {p0}, Lao/l;->s()Lao/j;

    move-result-object v0

    .line 419
    if-eqz v0, :cond_e

    invoke-interface {v0}, Lao/j;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 347
    invoke-virtual {p0}, Lao/l;->s()Lao/j;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public i()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 706
    invoke-virtual {p0}, Lao/l;->m()Lao/s;

    move-result-object v1

    .line 707
    invoke-virtual {p0}, Lao/l;->g()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-static {v1}, Lao/s;->c(Landroid/location/Location;)I

    move-result v1

    if-lt v1, v0, :cond_12

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public j()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 534
    iget-boolean v0, p0, Lao/l;->c:Z

    if-eqz v0, :cond_7

    move v0, v1

    .line 553
    :goto_6
    return v0

    .line 537
    :cond_7
    invoke-virtual {p0}, Lao/l;->s()Lao/j;

    move-result-object v0

    .line 538
    if-nez v0, :cond_f

    move v0, v1

    .line 539
    goto :goto_6

    .line 541
    :cond_f
    invoke-interface {v0}, Lao/j;->o()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 542
    invoke-interface {v0}, Lao/j;->s()Z

    move-result v0

    goto :goto_6

    .line 548
    :cond_1a
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 549
    invoke-interface {v0}, Lao/j;->s()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 550
    const/4 v0, 0x1

    goto :goto_6

    :cond_34
    move v0, v1

    .line 553
    goto :goto_6
.end method

.method public declared-synchronized k()V
    .registers 3

    .prologue
    .line 439
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lao/l;->j:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_23

    if-eqz v0, :cond_7

    .line 448
    :cond_5
    monitor-exit p0

    return-void

    .line 443
    :cond_7
    const/4 v0, 0x0

    :try_start_8
    iput-boolean v0, p0, Lao/l;->c:Z

    .line 444
    const/4 v0, 0x1

    iput-boolean v0, p0, Lao/l;->d:Z

    .line 445
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 446
    invoke-interface {v0}, Lao/j;->q()V
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_23

    goto :goto_13

    .line 439
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized l()V
    .registers 3

    .prologue
    .line 459
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lao/l;->c:Z

    .line 460
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 461
    invoke-interface {v0}, Lao/j;->p()V
    :try_end_19
    .catchall {:try_start_2 .. :try_end_19} :catchall_1a

    goto :goto_a

    .line 459
    :catchall_1a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 463
    :cond_1d
    monitor-exit p0

    return-void
.end method

.method public m()Lao/s;
    .registers 2

    .prologue
    .line 718
    iget-object v0, p0, Lao/l;->i:Lao/s;

    return-object v0
.end method

.method public n()Lao/s;
    .registers 2

    .prologue
    .line 713
    iget-object v0, p0, Lao/l;->h:Lao/s;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 311
    invoke-virtual {p0}, Lao/l;->s()Lao/j;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_b

    invoke-interface {v0}, Lao/j;->a()Ljava/lang/String;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const-string v0, "none"

    goto :goto_a
.end method

.method public r()Lcom/google/googlenav/common/a;
    .registers 2

    .prologue
    .line 283
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .registers 3

    .prologue
    .line 672
    iget-boolean v0, p0, Lao/l;->j:Z

    if-eqz v0, :cond_5

    .line 684
    :goto_4
    return-void

    .line 676
    :cond_5
    monitor-enter p0

    .line 677
    :try_start_6
    iget-boolean v0, p0, Lao/l;->c:Z

    if-eqz v0, :cond_f

    .line 678
    monitor-exit p0

    goto :goto_4

    .line 683
    :catchall_c
    move-exception v0

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_6 .. :try_end_e} :catchall_c

    throw v0

    .line 680
    :cond_f
    :try_start_f
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 681
    invoke-interface {v0}, Lao/j;->q()V

    goto :goto_15

    .line 683
    :cond_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_f .. :try_end_26} :catchall_c

    goto :goto_4
.end method

.method protected s()Lao/j;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 562
    iget-boolean v0, p0, Lao/l;->j:Z

    if-eqz v0, :cond_6

    .line 572
    :cond_5
    return-object v1

    .line 567
    :cond_6
    iget-object v0, p0, Lao/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lao/j;

    .line 568
    invoke-static {v0, v1}, Lao/l;->a(Lao/j;Lao/j;)Z

    move-result v3

    if-eqz v3, :cond_20

    :goto_1e
    move-object v1, v0

    .line 569
    goto :goto_c

    :cond_20
    move-object v0, v1

    goto :goto_1e
.end method
