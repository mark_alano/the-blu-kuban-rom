.class public final Lao/s;
.super Landroid/location/Location;
.source "SourceFile"


# instance fields
.field private a:Lau/B;

.field private b:Lo/B;

.field private c:Z


# direct methods
.method private constructor <init>(Lao/u;)V
    .registers 4
    .parameter

    .prologue
    .line 90
    invoke-static {p1}, Lao/u;->a(Lao/u;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 91
    invoke-static {p1}, Lao/u;->b(Lao/u;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 92
    invoke-static {p1}, Lao/u;->c(Lao/u;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setAccuracy(F)V

    .line 94
    :cond_14
    invoke-static {p1}, Lao/u;->d(Lao/u;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 95
    invoke-static {p1}, Lao/u;->e(Lao/u;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setAltitude(D)V

    .line 97
    :cond_21
    invoke-static {p1}, Lao/u;->f(Lao/u;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 98
    invoke-static {p1}, Lao/u;->g(Lao/u;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setBearing(F)V

    .line 100
    :cond_2e
    invoke-static {p1}, Lao/u;->h(Lao/u;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 101
    invoke-static {p1}, Lao/u;->i(Lao/u;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 102
    invoke-static {p1}, Lao/u;->j(Lao/u;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 103
    invoke-static {p1}, Lao/u;->k(Lao/u;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setSpeed(F)V

    .line 105
    :cond_49
    invoke-static {p1}, Lao/u;->l(Lao/u;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 106
    invoke-static {p1}, Lao/u;->m(Lao/u;)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setTime(J)V

    .line 108
    :cond_56
    invoke-static {p1}, Lao/u;->l(Lao/u;)Z

    move-result v0

    iput-boolean v0, p0, Lao/s;->c:Z

    .line 109
    invoke-static {p1}, Lao/u;->n(Lao/u;)Landroid/os/Bundle;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    .line 110
    invoke-static {p1}, Lao/u;->o(Lao/u;)Lau/B;

    move-result-object v0

    iput-object v0, p0, Lao/s;->a:Lau/B;

    .line 111
    invoke-static {p1}, Lao/u;->p(Lao/u;)Lo/B;

    move-result-object v0

    iput-object v0, p0, Lao/s;->b:Lo/B;

    .line 112
    return-void
.end method

.method synthetic constructor <init>(Lao/u;Lao/t;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lao/s;-><init>(Lao/u;)V

    return-void
.end method

.method public static a(Landroid/location/Location;)I
    .registers 3
    .parameter

    .prologue
    const v0, 0x1869f

    .line 122
    if-nez p0, :cond_6

    .line 129
    :cond_5
    :goto_5
    return v0

    .line 125
    :cond_6
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 129
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    goto :goto_5
.end method

.method public static a(Lao/s;)Lau/B;
    .registers 2
    .parameter

    .prologue
    .line 154
    if-nez p0, :cond_4

    .line 155
    const/4 v0, 0x0

    .line 157
    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {p0}, Lao/s;->a()Lau/B;

    move-result-object v0

    goto :goto_3
.end method

.method private a(ZDZD)Z
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 298
    if-eqz p1, :cond_f

    .line 299
    if-nez p4, :cond_7

    .line 307
    :cond_6
    :goto_6
    return v1

    .line 302
    :cond_7
    cmpl-double v2, p2, p5

    if-nez v2, :cond_d

    :goto_b
    move v1, v0

    goto :goto_6

    :cond_d
    move v0, v1

    goto :goto_b

    .line 304
    :cond_f
    if-nez p4, :cond_6

    move v1, v0

    .line 307
    goto :goto_6
.end method

.method private a(ZJZJ)Z
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 283
    if-eqz p1, :cond_f

    .line 284
    if-nez p4, :cond_7

    .line 292
    :cond_6
    :goto_6
    return v1

    .line 287
    :cond_7
    cmp-long v2, p2, p5

    if-nez v2, :cond_d

    :goto_b
    move v1, v0

    goto :goto_6

    :cond_d
    move v0, v1

    goto :goto_b

    .line 289
    :cond_f
    if-nez p4, :cond_6

    move v1, v0

    .line 292
    goto :goto_6
.end method

.method public static b(Landroid/location/Location;)I
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 140
    if-nez p0, :cond_4

    .line 146
    :cond_3
    :goto_3
    return v0

    .line 143
    :cond_4
    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 146
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-int v0, v0

    goto :goto_3
.end method

.method public static c(Landroid/location/Location;)I
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 168
    if-nez p0, :cond_4

    .line 174
    :cond_3
    :goto_3
    return v0

    .line 171
    :cond_4
    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 174
    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-int v0, v0

    goto :goto_3
.end method

.method public static d(Landroid/location/Location;)Lo/B;
    .registers 2
    .parameter

    .prologue
    .line 182
    if-eqz p0, :cond_6

    instance-of v0, p0, Lao/s;

    if-nez v0, :cond_8

    .line 183
    :cond_6
    const/4 v0, 0x0

    .line 185
    :goto_7
    return-object v0

    :cond_8
    check-cast p0, Lao/s;

    invoke-virtual {p0}, Lao/s;->b()Lo/B;

    move-result-object v0

    goto :goto_7
.end method


# virtual methods
.method public a()Lau/B;
    .registers 2

    .prologue
    .line 193
    iget-object v0, p0, Lao/s;->a:Lau/B;

    return-object v0
.end method

.method public b()Lo/B;
    .registers 2

    .prologue
    .line 200
    iget-object v0, p0, Lao/s;->b:Lo/B;

    return-object v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Lao/s;->b:Lo/B;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 214
    iget-boolean v0, p0, Lao/s;->c:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 232
    instance-of v0, p1, Lao/s;

    if-nez v0, :cond_8

    move v0, v7

    .line 278
    :goto_7
    return v0

    .line 235
    :cond_8
    check-cast p1, Lao/s;

    .line 237
    invoke-virtual {p1}, Lao/s;->a()Lau/B;

    move-result-object v0

    invoke-virtual {p0}, Lao/s;->a()Lau/B;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    move v0, v7

    .line 238
    goto :goto_7

    .line 241
    :cond_1a
    invoke-virtual {p1}, Lao/s;->b()Lo/B;

    move-result-object v0

    invoke-virtual {p0}, Lao/s;->b()Lo/B;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    move v0, v7

    .line 242
    goto :goto_7

    .line 245
    :cond_2a
    invoke-virtual {p1}, Lao/s;->hasAccuracy()Z

    move-result v1

    invoke-virtual {p1}, Lao/s;->getAccuracy()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, Lao/s;->hasAccuracy()Z

    move-result v4

    invoke-virtual {p0}, Lao/s;->getAccuracy()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lao/s;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_45

    move v0, v7

    .line 247
    goto :goto_7

    .line 249
    :cond_45
    invoke-virtual {p1}, Lao/s;->hasAltitude()Z

    move-result v1

    invoke-virtual {p1}, Lao/s;->getAltitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lao/s;->hasAltitude()Z

    move-result v4

    invoke-virtual {p0}, Lao/s;->getAltitude()D

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lao/s;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_5e

    move v0, v7

    .line 251
    goto :goto_7

    .line 253
    :cond_5e
    invoke-virtual {p1}, Lao/s;->hasBearing()Z

    move-result v1

    invoke-virtual {p1}, Lao/s;->getBearing()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, Lao/s;->hasBearing()Z

    move-result v4

    invoke-virtual {p0}, Lao/s;->getBearing()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lao/s;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_79

    move v0, v7

    .line 255
    goto :goto_7

    .line 257
    :cond_79
    invoke-virtual {p1}, Lao/s;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lao/s;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8a

    move v0, v7

    .line 258
    goto/16 :goto_7

    .line 260
    :cond_8a
    invoke-virtual {p1}, Lao/s;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lao/s;->getLatitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, Lao/s;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_9e

    move v0, v7

    .line 261
    goto/16 :goto_7

    .line 263
    :cond_9e
    invoke-virtual {p1}, Lao/s;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lao/s;->getLongitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, Lao/s;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_b2

    move v0, v7

    .line 264
    goto/16 :goto_7

    .line 266
    :cond_b2
    invoke-virtual {p1}, Lao/s;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lao/s;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c3

    move v0, v7

    .line 267
    goto/16 :goto_7

    .line 269
    :cond_c3
    invoke-virtual {p1}, Lao/s;->hasSpeed()Z

    move-result v1

    invoke-virtual {p1}, Lao/s;->getSpeed()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, Lao/s;->hasSpeed()Z

    move-result v4

    invoke-virtual {p0}, Lao/s;->getSpeed()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lao/s;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_df

    move v0, v7

    .line 271
    goto/16 :goto_7

    .line 273
    :cond_df
    invoke-virtual {p1}, Lao/s;->d()Z

    move-result v1

    invoke-virtual {p1}, Lao/s;->getTime()J

    move-result-wide v2

    invoke-virtual {p0}, Lao/s;->d()Z

    move-result v4

    invoke-virtual {p0}, Lao/s;->getTime()J

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lao/s;->a(ZJZJ)Z

    move-result v0

    if-nez v0, :cond_f9

    move v0, v7

    .line 275
    goto/16 :goto_7

    :cond_f9
    move v0, v8

    .line 278
    goto/16 :goto_7
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 219
    .line 221
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lao/s;->a:Lau/B;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lao/s;->b:Lo/B;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lao/s;->getProvider()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lao/s;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/K;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 222
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lao/s;->getAccuracy()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 223
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lao/s;->getTime()J

    move-result-wide v1

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 224
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lao/s;->getBearing()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 225
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lao/s;->getAltitude()D

    move-result-wide v1

    double-to-int v1, v1

    add-int/2addr v0, v1

    .line 226
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lao/s;->getSpeed()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 227
    return v0
.end method

.method public setAccuracy(F)V
    .registers 3
    .parameter

    .prologue
    .line 329
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setAltitude(D)V
    .registers 4
    .parameter

    .prologue
    .line 334
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setBearing(F)V
    .registers 3
    .parameter

    .prologue
    .line 339
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 344
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLatitude(D)V
    .registers 4
    .parameter

    .prologue
    .line 349
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLongitude(D)V
    .registers 4
    .parameter

    .prologue
    .line 354
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setProvider(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setSpeed(F)V
    .registers 3
    .parameter

    .prologue
    .line 364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setTime(J)V
    .registers 4
    .parameter

    .prologue
    .line 369
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 313
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 314
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    .line 315
    const-string v0, "LocationFix["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "source = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lao/s;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", point = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lao/s;->a:Lau/B;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", accuracy = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lao/s;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_d0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lao/s;->getAccuracy()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", speed = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lao/s;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_d4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lao/s;->getSpeed()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m/s"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_74
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", bearing = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lao/s;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_d7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lao/s;->getBearing()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " degrees"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_9b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", time = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p0}, Lao/s;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", level = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lao/s;->b:Lo/B;

    if-eqz v0, :cond_da

    iget-object v0, p0, Lao/s;->b:Lo/B;

    :goto_c2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 315
    :cond_d0
    const-string v0, "n/a"

    goto/16 :goto_4d

    :cond_d4
    const-string v0, "n/a"

    goto :goto_74

    :cond_d7
    const-string v0, "n/a"

    goto :goto_9b

    :cond_da
    const-string v0, "n/a"

    goto :goto_c2
.end method
