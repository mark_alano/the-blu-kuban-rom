.class public Lao/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:D

.field private c:F

.field private d:Landroid/os/Bundle;

.field private e:D

.field private f:D

.field private g:Ljava/lang/String;

.field private h:F

.field private i:J

.field private j:Lo/B;

.field private k:Lau/B;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput-boolean v0, p0, Lao/u;->l:Z

    .line 387
    iput-boolean v0, p0, Lao/u;->m:Z

    .line 388
    iput-boolean v0, p0, Lao/u;->n:Z

    .line 389
    iput-boolean v0, p0, Lao/u;->o:Z

    .line 390
    iput-boolean v0, p0, Lao/u;->p:Z

    return-void
.end method

.method static synthetic a(Lao/u;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-object v0, p0, Lao/u;->g:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/location/Location;)V
    .registers 6
    .parameter

    .prologue
    .line 488
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 489
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p0, v0}, Lao/u;->a(F)Lao/u;

    .line 491
    :cond_d
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 492
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lao/u;->a(D)Lao/u;

    .line 494
    :cond_1a
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 495
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p0, v0}, Lao/u;->b(F)Lao/u;

    .line 497
    :cond_27
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lao/u;->a(DD)Lao/u;

    .line 498
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lao/u;->a(Ljava/lang/String;)Lao/u;

    .line 499
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 500
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-virtual {p0, v0}, Lao/u;->c(F)Lao/u;

    .line 502
    :cond_46
    return-void
.end method

.method static synthetic b(Lao/u;)Z
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-boolean v0, p0, Lao/u;->l:Z

    return v0
.end method

.method static synthetic c(Lao/u;)F
    .registers 2
    .parameter

    .prologue
    .line 373
    iget v0, p0, Lao/u;->a:F

    return v0
.end method

.method static synthetic d(Lao/u;)Z
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-boolean v0, p0, Lao/u;->m:Z

    return v0
.end method

.method static synthetic e(Lao/u;)D
    .registers 3
    .parameter

    .prologue
    .line 373
    iget-wide v0, p0, Lao/u;->b:D

    return-wide v0
.end method

.method static synthetic f(Lao/u;)Z
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-boolean v0, p0, Lao/u;->n:Z

    return v0
.end method

.method static synthetic g(Lao/u;)F
    .registers 2
    .parameter

    .prologue
    .line 373
    iget v0, p0, Lao/u;->c:F

    return v0
.end method

.method static synthetic h(Lao/u;)D
    .registers 3
    .parameter

    .prologue
    .line 373
    iget-wide v0, p0, Lao/u;->e:D

    return-wide v0
.end method

.method static synthetic i(Lao/u;)D
    .registers 3
    .parameter

    .prologue
    .line 373
    iget-wide v0, p0, Lao/u;->f:D

    return-wide v0
.end method

.method static synthetic j(Lao/u;)Z
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-boolean v0, p0, Lao/u;->o:Z

    return v0
.end method

.method static synthetic k(Lao/u;)F
    .registers 2
    .parameter

    .prologue
    .line 373
    iget v0, p0, Lao/u;->h:F

    return v0
.end method

.method static synthetic l(Lao/u;)Z
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-boolean v0, p0, Lao/u;->p:Z

    return v0
.end method

.method static synthetic m(Lao/u;)J
    .registers 3
    .parameter

    .prologue
    .line 373
    iget-wide v0, p0, Lao/u;->i:J

    return-wide v0
.end method

.method static synthetic n(Lao/u;)Landroid/os/Bundle;
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-object v0, p0, Lao/u;->d:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic o(Lao/u;)Lau/B;
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-object v0, p0, Lao/u;->k:Lau/B;

    return-object v0
.end method

.method static synthetic p(Lao/u;)Lo/B;
    .registers 2
    .parameter

    .prologue
    .line 373
    iget-object v0, p0, Lao/u;->j:Lo/B;

    return-object v0
.end method


# virtual methods
.method public a()Lao/u;
    .registers 2

    .prologue
    .line 510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lao/u;->p:Z

    .line 511
    return-object p0
.end method

.method public a(D)Lao/u;
    .registers 4
    .parameter

    .prologue
    .line 399
    iput-wide p1, p0, Lao/u;->b:D

    .line 400
    const/4 v0, 0x1

    iput-boolean v0, p0, Lao/u;->m:Z

    .line 401
    return-object p0
.end method

.method public a(DD)Lao/u;
    .registers 10
    .parameter
    .parameter

    .prologue
    const-wide v3, 0x412e848000000000L

    .line 420
    iput-wide p1, p0, Lao/u;->e:D

    .line 421
    iput-wide p3, p0, Lao/u;->f:D

    .line 422
    new-instance v0, Lau/B;

    mul-double v1, p1, v3

    double-to-int v1, v1

    mul-double v2, p3, v3

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lau/B;-><init>(II)V

    iput-object v0, p0, Lao/u;->k:Lau/B;

    .line 423
    return-object p0
.end method

.method public a(F)Lao/u;
    .registers 3
    .parameter

    .prologue
    .line 393
    iput p1, p0, Lao/u;->a:F

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lao/u;->l:Z

    .line 395
    return-object p0
.end method

.method public a(J)Lao/u;
    .registers 4
    .parameter

    .prologue
    .line 438
    iput-wide p1, p0, Lao/u;->i:J

    .line 439
    const/4 v0, 0x1

    iput-boolean v0, p0, Lao/u;->p:Z

    .line 440
    return-object p0
.end method

.method public a(Landroid/location/Location;)Lao/u;
    .registers 4
    .parameter

    .prologue
    .line 464
    if-nez p1, :cond_3

    .line 471
    :goto_2
    return-object p0

    .line 467
    :cond_3
    invoke-direct {p0, p1}, Lao/u;->b(Landroid/location/Location;)V

    .line 468
    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lao/u;->a(Landroid/os/Bundle;)Lao/u;

    .line 469
    invoke-static {p1}, Lao/b;->b(Landroid/location/Location;)Lo/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lao/u;->a(Lo/B;)Lao/u;

    .line 470
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lao/u;->a(J)Lao/u;

    goto :goto_2
.end method

.method public a(Landroid/os/Bundle;)Lao/u;
    .registers 2
    .parameter

    .prologue
    .line 411
    iput-object p1, p0, Lao/u;->d:Landroid/os/Bundle;

    .line 412
    return-object p0
.end method

.method public a(Lao/s;)Lao/u;
    .registers 4
    .parameter

    .prologue
    .line 475
    if-nez p1, :cond_3

    .line 484
    :cond_2
    :goto_2
    return-object p0

    .line 478
    :cond_3
    invoke-direct {p0, p1}, Lao/u;->b(Landroid/location/Location;)V

    .line 479
    invoke-virtual {p1}, Lao/s;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lao/u;->a(Landroid/os/Bundle;)Lao/u;

    .line 480
    invoke-virtual {p1}, Lao/s;->b()Lo/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lao/u;->a(Lo/B;)Lao/u;

    .line 481
    invoke-virtual {p1}, Lao/s;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 482
    invoke-virtual {p1}, Lao/s;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lao/u;->a(J)Lao/u;

    goto :goto_2
.end method

.method public a(Lau/B;)Lao/u;
    .registers 6
    .parameter

    .prologue
    const-wide v2, 0x412e848000000000L

    .line 457
    iput-object p1, p0, Lao/u;->k:Lau/B;

    .line 458
    invoke-virtual {p1}, Lau/B;->c()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lao/u;->e:D

    .line 459
    invoke-virtual {p1}, Lau/B;->e()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lao/u;->f:D

    .line 460
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lao/u;
    .registers 2
    .parameter

    .prologue
    .line 427
    iput-object p1, p0, Lao/u;->g:Ljava/lang/String;

    .line 428
    return-object p0
.end method

.method public a(Lo/B;)Lao/u;
    .registers 3
    .parameter

    .prologue
    .line 444
    iput-object p1, p0, Lao/u;->j:Lo/B;

    .line 445
    if-eqz p1, :cond_b

    .line 450
    invoke-static {p1}, Lar/a;->a(Lo/B;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lao/u;->a(Landroid/os/Bundle;)Lao/u;

    .line 452
    :cond_b
    return-object p0
.end method

.method public b()Lao/s;
    .registers 3

    .prologue
    .line 519
    iget-object v0, p0, Lao/u;->k:Lau/B;

    if-nez v0, :cond_c

    .line 520
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 522
    :cond_c
    new-instance v0, Lao/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lao/s;-><init>(Lao/u;Lao/t;)V

    return-object v0
.end method

.method public b(F)Lao/u;
    .registers 3
    .parameter

    .prologue
    .line 405
    iput p1, p0, Lao/u;->c:F

    .line 406
    const/4 v0, 0x1

    iput-boolean v0, p0, Lao/u;->n:Z

    .line 407
    return-object p0
.end method

.method public c(F)Lao/u;
    .registers 3
    .parameter

    .prologue
    .line 432
    iput p1, p0, Lao/u;->h:F

    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lao/u;->o:Z

    .line 434
    return-object p0
.end method
