.class LaT/c;
.super Las/b;
.source "SourceFile"


# instance fields
.field final synthetic a:LaT/a;


# direct methods
.method constructor <init>(LaT/a;Las/c;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 333
    iput-object p1, p0, LaT/c;->a:LaT/a;

    invoke-direct {p0, p2}, Las/b;-><init>(Las/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 8

    .prologue
    .line 337
    iget-object v2, p0, LaT/c;->a:LaT/a;

    monitor-enter v2

    .line 338
    :try_start_3
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbM/r;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 339
    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->b(LaT/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/f;

    .line 340
    const/4 v4, 0x1

    invoke-static {v0}, LaT/f;->a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_14

    .line 361
    :catchall_29
    move-exception v0

    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_29

    throw v0

    .line 343
    :cond_2c
    :try_start_2c
    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;

    .line 345
    array-length v4, v0

    const/4 v1, 0x0

    :goto_46
    if-ge v1, v4, :cond_55

    aget-object v5, v0, v1

    .line 346
    const/4 v6, 0x1

    invoke-static {v5}, LaT/f;->a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v3, v6, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 345
    add-int/lit8 v1, v1, 0x1

    goto :goto_46

    .line 349
    :cond_55
    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->a(LaT/a;)LaT/f;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 350
    const/4 v0, 0x1

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->a(LaT/a;)LaT/f;

    move-result-object v1

    invoke-static {v1}, LaT/f;->a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 353
    :cond_6b
    const/4 v0, 0x2

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->d(LaT/a;)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 354
    iget-object v0, p0, LaT/c;->a:LaT/a;

    invoke-static {v0}, LaT/a;->e(LaT/a;)Lo/aq;

    move-result-object v0

    if-eqz v0, :cond_8b

    .line 355
    const/4 v0, 0x3

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->e(LaT/a;)Lo/aq;

    move-result-object v1

    invoke-virtual {v1}, Lo/aq;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 358
    :cond_8b
    const/4 v0, 0x4

    iget-object v1, p0, LaT/c;->a:LaT/a;

    invoke-static {v1}, LaT/a;->f(LaT/a;)J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 361
    monitor-exit v2
    :try_end_96
    .catchall {:try_start_2c .. :try_end_96} :catchall_29

    .line 362
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 364
    :try_start_9e
    invoke-virtual {v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    sget-object v2, LaT/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_a7
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_a7} :catch_a8

    .line 368
    :goto_a7
    return-void

    .line 365
    :catch_a8
    move-exception v0

    goto :goto_a7
.end method
