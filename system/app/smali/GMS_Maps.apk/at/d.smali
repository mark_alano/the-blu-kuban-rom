.class LaT/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaT/a;


# direct methods
.method constructor <init>(LaT/a;)V
    .registers 2
    .parameter

    .prologue
    .line 377
    iput-object p1, p0, LaT/d;->a:LaT/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 15

    .prologue
    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v13, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 380
    const-wide/16 v0, 0x0

    .line 381
    sget-object v3, LA/c;->a:LA/c;

    invoke-static {v3}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_48

    .line 382
    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    .line 383
    invoke-interface {v0}, Lr/z;->f()J

    move-result-wide v0

    .line 394
    :goto_19
    :try_start_19
    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v6, 0x4

    invoke-static {v3, v6}, LaT/a;->a(LaT/a;I)I

    .line 396
    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbM/r;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 397
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v3

    .line 398
    sget-object v7, LaT/a;->a:Ljava/lang/String;

    invoke-interface {v3, v7}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v3

    .line 399
    if-eqz v3, :cond_39

    array-length v7, v3
    :try_end_37
    .catchall {:try_start_19 .. :try_end_37} :catchall_16b

    if-nez v7, :cond_4e

    .line 458
    :cond_39
    iget-object v2, p0, LaT/d;->a:LaT/a;

    invoke-static {v2, v0, v1}, LaT/a;->a(LaT/a;J)J

    .line 459
    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 465
    :cond_47
    :goto_47
    return-void

    .line 386
    :cond_48
    const-string v3, "Tile store should be set if not unit tests"

    invoke-static {v4, v3}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    goto :goto_19

    .line 403
    :cond_4e
    :try_start_4e
    invoke-virtual {v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_51
    .catchall {:try_start_4e .. :try_end_51} :catchall_16b
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_10e

    .line 408
    :try_start_51
    iget-object v7, p0, LaT/d;->a:LaT/a;

    monitor-enter v7
    :try_end_54
    .catchall {:try_start_51 .. :try_end_54} :catchall_16b

    .line 411
    :try_start_54
    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3}, LaT/a;->b(LaT/a;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 412
    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 413
    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v8, 0x0

    invoke-static {v3, v8}, LaT/a;->a(LaT/a;LaT/f;)LaT/f;

    .line 414
    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v8, 0x0

    invoke-static {v3, v8}, LaT/a;->a(LaT/a;Lo/aq;)Lo/aq;

    .line 416
    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    invoke-static {v3, v8, v9}, LaT/a;->a(LaT/a;J)J

    .line 418
    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3}, LaT/a;->f(LaT/a;)J
    :try_end_81
    .catchall {:try_start_54 .. :try_end_81} :catchall_16d

    move-result-wide v8

    cmp-long v3, v8, v0

    if-eqz v3, :cond_11f

    move v3, v5

    .line 420
    :goto_87
    :try_start_87
    iget-object v8, p0, LaT/d;->a:LaT/a;

    const/4 v9, 0x2

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v9

    invoke-static {v8, v9}, LaT/a;->a(LaT/a;I)I

    .line 421
    iget-object v8, p0, LaT/d;->a:LaT/a;

    invoke-static {v8}, LaT/a;->d(LaT/a;)I

    move-result v8

    if-eq v8, v5, :cond_a1

    iget-object v8, p0, LaT/d;->a:LaT/a;

    invoke-static {v8}, LaT/a;->d(LaT/a;)I

    move-result v8

    if-ne v8, v10, :cond_a7

    .line 422
    :cond_a1
    iget-object v8, p0, LaT/d;->a:LaT/a;

    const/4 v9, 0x0

    invoke-static {v8, v9}, LaT/a;->a(LaT/a;I)I

    .line 427
    :cond_a7
    iget-object v8, p0, LaT/d;->a:LaT/a;

    if-nez v3, :cond_bb

    const/4 v9, 0x3

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_bb

    const/4 v2, 0x3

    invoke-virtual {v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lo/aq;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/aq;

    move-result-object v2

    :cond_bb
    invoke-static {v8, v2}, LaT/a;->a(LaT/a;Lo/aq;)Lo/aq;

    .line 432
    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 436
    iget-object v8, p0, LaT/d;->a:LaT/a;

    const/4 v9, 0x0

    invoke-static {v8, v9}, LaT/a;->b(LaT/a;I)I

    .line 437
    :goto_c9
    if-ge v4, v2, :cond_153

    .line 438
    const/4 v8, 0x1

    invoke-virtual {v6, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-static {v8}, LaT/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;

    move-result-object v8

    .line 440
    iget-object v9, p0, LaT/d;->a:LaT/a;

    iget-object v10, p0, LaT/d;->a:LaT/a;

    invoke-static {v10}, LaT/a;->g(LaT/a;)I

    move-result v10

    new-instance v11, Ljava/lang/Integer;

    invoke-virtual {v8}, LaT/f;->f()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-static {v9, v10}, LaT/a;->b(LaT/a;I)I

    .line 442
    invoke-static {v8}, LaT/f;->b(LaT/f;)I

    move-result v9

    if-eqz v9, :cond_fc

    invoke-static {v8}, LaT/f;->b(LaT/f;)I

    move-result v9

    if-ne v9, v13, :cond_122

    .line 446
    :cond_fc
    if-eqz v3, :cond_102

    .line 447
    const/4 v9, 0x4

    invoke-static {v8, v9}, LaT/f;->a(LaT/f;I)I

    .line 449
    :cond_102
    iget-object v9, p0, LaT/d;->a:LaT/a;

    invoke-static {v9}, LaT/a;->b(LaT/a;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_10b
    .catchall {:try_start_87 .. :try_end_10b} :catchall_12e

    .line 437
    :goto_10b
    add-int/lit8 v4, v4, 0x1

    goto :goto_c9

    .line 404
    :catch_10e
    move-exception v2

    .line 458
    iget-object v2, p0, LaT/d;->a:LaT/a;

    invoke-static {v2, v0, v1}, LaT/a;->a(LaT/a;J)J

    .line 459
    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_47

    :cond_11f
    move v3, v4

    .line 418
    goto/16 :goto_87

    .line 450
    :cond_122
    :try_start_122
    invoke-static {v8}, LaT/f;->b(LaT/f;)I

    move-result v9

    if-ne v9, v5, :cond_149

    .line 451
    iget-object v9, p0, LaT/d;->a:LaT/a;

    invoke-static {v9, v8}, LaT/a;->a(LaT/a;LaT/f;)LaT/f;

    goto :goto_10b

    .line 456
    :catchall_12e
    move-exception v2

    :goto_12f
    monitor-exit v7
    :try_end_130
    .catchall {:try_start_122 .. :try_end_130} :catchall_12e

    :try_start_130
    throw v2
    :try_end_131
    .catchall {:try_start_130 .. :try_end_131} :catchall_131

    .line 458
    :catchall_131
    move-exception v2

    move v4, v3

    :goto_133
    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3, v0, v1}, LaT/a;->a(LaT/a;J)J

    .line 459
    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 461
    if-eqz v4, :cond_148

    .line 462
    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-virtual {v0}, LaT/a;->m()V

    :cond_148
    throw v2

    .line 453
    :cond_149
    :try_start_149
    iget-object v9, p0, LaT/d;->a:LaT/a;

    invoke-static {v9}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_10b

    .line 456
    :cond_153
    monitor-exit v7
    :try_end_154
    .catchall {:try_start_149 .. :try_end_154} :catchall_12e

    .line 458
    iget-object v2, p0, LaT/d;->a:LaT/a;

    invoke-static {v2, v0, v1}, LaT/a;->a(LaT/a;J)J

    .line 459
    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 461
    if-eqz v3, :cond_47

    .line 462
    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-virtual {v0}, LaT/a;->m()V

    goto/16 :goto_47

    .line 458
    :catchall_16b
    move-exception v2

    goto :goto_133

    .line 456
    :catchall_16d
    move-exception v2

    move v3, v4

    goto :goto_12f
.end method
