.class public Lau/h;
.super Lau/u;
.source "SourceFile"


# instance fields
.field private final c:Z

.field private final d:Lau/p;

.field private final e:Lcom/google/googlenav/ui/bN;

.field private f:Lau/s;


# direct methods
.method public constructor <init>(Lau/p;Lcom/google/googlenav/ui/bN;Lcom/google/googlenav/ui/s;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Lau/u;-><init>()V

    .line 42
    iput-object p1, p0, Lau/h;->d:Lau/p;

    .line 43
    iput-object p2, p0, Lau/h;->e:Lcom/google/googlenav/ui/bN;

    .line 44
    iput-boolean p4, p0, Lau/h;->c:Z

    .line 45
    if-eqz p2, :cond_13

    .line 46
    new-instance v0, Lau/i;

    invoke-direct {v0, p0}, Lau/i;-><init>(Lau/h;)V

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ui/bN;->a(Lcom/google/googlenav/ui/bO;)V

    .line 60
    :cond_13
    if-eqz p3, :cond_1d

    .line 61
    new-instance v0, Lau/j;

    invoke-direct {v0, p0}, Lau/j;-><init>(Lau/h;)V

    invoke-virtual {p3, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/t;)V

    .line 75
    :cond_1d
    invoke-virtual {p1}, Lau/p;->t()I

    move-result v0

    invoke-virtual {p1}, Lau/p;->s()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lau/h;->c(II)V

    .line 76
    return-void
.end method

.method private g(Lau/B;)Z
    .registers 5
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1}, Lau/p;->c(Lau/B;)Landroid/graphics/Point;

    move-result-object v0

    .line 203
    iget-boolean v1, p0, Lau/h;->c:Z

    if-eqz v1, :cond_2c

    iget v1, v0, Landroid/graphics/Point;->x:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget-object v2, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v2}, Lau/p;->t()I

    move-result v2

    mul-int/lit8 v2, v2, 0x7

    if-ge v1, v2, :cond_2c

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v1}, Lau/p;->s()I

    move-result v1

    mul-int/lit8 v1, v1, 0x7

    if-ge v0, v1, :cond_2c

    const/4 v0, 0x1

    :goto_2b
    return v0

    :cond_2c
    const/4 v0, 0x0

    goto :goto_2b
.end method

.method private r()V
    .registers 2

    .prologue
    .line 226
    iget-object v0, p0, Lau/h;->e:Lcom/google/googlenav/ui/bN;

    if-eqz v0, :cond_9

    .line 227
    iget-object v0, p0, Lau/h;->e:Lcom/google/googlenav/ui/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bN;->c()V

    .line 229
    :cond_9
    return-void
.end method


# virtual methods
.method public a(ILau/B;)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1}, Lau/p;->c(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public a()I
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->m()I

    move-result v0

    return v0
.end method

.method public a(Lau/B;)I
    .registers 3
    .parameter

    .prologue
    .line 302
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1}, Lau/p;->e(Lau/B;)I

    move-result v0

    return v0
.end method

.method public a(Lau/H;)I
    .registers 3
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1}, Lau/p;->b(Lau/H;)I

    move-result v0

    return v0
.end method

.method protected a(Lau/B;Lau/Y;II)Lau/B;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 342
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1, p2, p3, p4}, Lau/p;->a(Lau/B;Lau/Y;II)Lau/B;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 148
    invoke-direct {p0}, Lau/h;->r()V

    .line 149
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1}, Lau/p;->b(I)V

    .line 150
    return-void
.end method

.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 254
    invoke-virtual {p0}, Lau/h;->h()V

    .line 255
    invoke-direct {p0}, Lau/h;->r()V

    .line 259
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, v1}, Lau/h;->a(ZZZ)V

    .line 260
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->d()Lau/B;

    move-result-object v0

    iget-object v1, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v1}, Lau/p;->c()Lau/Y;

    move-result-object v1

    invoke-static {v0, p1, p2, v1}, Lau/s;->a(Lau/B;IILau/Y;)Lau/s;

    move-result-object v0

    iput-object v0, p0, Lau/h;->f:Lau/s;

    .line 261
    return-void
.end method

.method protected a(Lau/B;Landroid/graphics/Point;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1, p2}, Lau/p;->b(Lau/B;Landroid/graphics/Point;)V

    .line 338
    return-void
.end method

.method public a(Lau/B;Lau/Y;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 158
    invoke-virtual {p0, p1, p2}, Lau/h;->d(Lau/B;Lau/Y;)V

    .line 159
    return-void
.end method

.method protected a(Lau/B;Lau/Y;Lau/B;Landroid/graphics/Point;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 332
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1, p2, p3, p4}, Lau/p;->a(Lau/B;Lau/Y;Lau/B;Landroid/graphics/Point;)V

    .line 333
    return-void
.end method

.method protected a(Lau/Y;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lau/h;->e:Lcom/google/googlenav/ui/bN;

    if-nez v0, :cond_15

    .line 280
    invoke-static {}, Lcom/google/googlenav/common/util/t;->a()V

    .line 281
    invoke-virtual {p0, p2, p3}, Lau/h;->b(II)Lau/B;

    move-result-object v0

    neg-int v1, p2

    neg-int v2, p3

    invoke-virtual {v0, v1, v2, p1}, Lau/B;->a(IILau/Y;)Lau/B;

    move-result-object v0

    .line 282
    invoke-virtual {p0, v0, p1}, Lau/h;->e(Lau/B;Lau/Y;)V

    .line 286
    :goto_14
    return-void

    .line 285
    :cond_15
    iget-object v0, p0, Lau/h;->e:Lcom/google/googlenav/ui/bN;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/bN;->a(Lau/Y;II)V

    goto :goto_14
.end method

.method public a(Lo/B;)V
    .registers 2
    .parameter

    .prologue
    .line 348
    return-void
.end method

.method public a([Lau/B;IIILau/Y;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 233
    if-nez p1, :cond_5

    .line 250
    :goto_4
    return-void

    .line 236
    :cond_5
    invoke-virtual {p0, p5}, Lau/h;->a(Lau/Y;)V

    .line 237
    invoke-direct {p0}, Lau/h;->r()V

    .line 238
    aget-object v0, p1, p3

    .line 239
    iget-object v2, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v2, v0}, Lau/p;->d(Lau/B;)V

    .line 240
    invoke-direct {p0, v0}, Lau/h;->g(Lau/B;)Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 243
    invoke-virtual {p0}, Lau/h;->h()V

    .line 244
    invoke-virtual {p0}, Lau/h;->c()Lau/B;

    move-result-object v2

    invoke-virtual {v0, v2}, Lau/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_39

    move v0, v1

    :goto_26
    invoke-virtual {p0, v0, v6, v1}, Lau/h;->a(ZZZ)V

    .line 245
    new-instance v0, Lau/s;

    int-to-long v1, p4

    sget-wide v3, Lau/s;->a:J

    mul-long v4, v1, v3

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lau/s;-><init>([Lau/B;IIJI)V

    iput-object v0, p0, Lau/h;->f:Lau/s;

    goto :goto_4

    :cond_39
    move v0, v6

    .line 244
    goto :goto_26

    .line 248
    :cond_3b
    invoke-virtual {p0, v0}, Lau/h;->c(Lau/B;)V

    goto :goto_4
.end method

.method public b()I
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->n()I

    move-result v0

    return v0
.end method

.method public b(Lau/H;)I
    .registers 3
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1}, Lau/p;->c(Lau/H;)I

    move-result v0

    return v0
.end method

.method public b(II)Lau/B;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1, p2}, Lau/p;->b(II)Lau/B;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lau/B;Lau/Y;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 169
    invoke-virtual {p0}, Lau/h;->h()V

    .line 170
    invoke-direct {p0}, Lau/h;->r()V

    .line 172
    if-nez p2, :cond_e

    .line 173
    invoke-virtual {p0}, Lau/h;->d()Lau/Y;

    move-result-object p2

    .line 175
    :cond_e
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p2}, Lau/p;->a(Lau/Y;)V

    .line 178
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->c()Lau/Y;

    move-result-object v0

    invoke-virtual {p1, v2, v2, v0}, Lau/B;->a(IILau/Y;)Lau/B;

    move-result-object v4

    .line 181
    invoke-direct {p0, v4}, Lau/h;->g(Lau/B;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 183
    invoke-virtual {p0}, Lau/h;->c()Lau/B;

    move-result-object v0

    invoke-virtual {v4, v0}, Lau/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_68

    move v0, v1

    :goto_2e
    invoke-virtual {p0}, Lau/h;->d()Lau/Y;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6a

    move v3, v1

    :goto_39
    invoke-virtual {p0, v0, v3, v1}, Lau/h;->a(ZZZ)V

    .line 185
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, v4}, Lau/p;->d(Lau/B;)V

    .line 187
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->d()Lau/B;

    move-result-object v0

    iget-object v1, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v1}, Lau/p;->c()Lau/Y;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lau/B;->a(Lau/B;Lau/Y;)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/j;->b(I)I

    move-result v0

    .line 190
    new-instance v1, Lau/s;

    iget-object v3, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v3}, Lau/p;->d()Lau/B;

    move-result-object v3

    invoke-static {v0}, Lau/h;->b(I)I

    move-result v0

    invoke-direct {v1, v3, v4, v0, v2}, Lau/s;-><init>(Lau/B;Lau/B;II)V

    iput-object v1, p0, Lau/h;->f:Lau/s;

    .line 195
    :goto_67
    return-void

    :cond_68
    move v0, v2

    .line 183
    goto :goto_2e

    :cond_6a
    move v3, v2

    goto :goto_39

    .line 193
    :cond_6c
    invoke-virtual {p0, v4}, Lau/h;->c(Lau/B;)V

    goto :goto_67
.end method

.method public c()Lau/B;
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->d()Lau/B;

    move-result-object v0

    return-object v0
.end method

.method protected c(Lau/B;Lau/Y;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 290
    .line 291
    invoke-virtual {p0}, Lau/h;->h()V

    .line 292
    invoke-direct {p0}, Lau/h;->r()V

    .line 293
    invoke-virtual {p0}, Lau/h;->c()Lau/B;

    move-result-object v0

    invoke-virtual {p1, v0}, Lau/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2e

    move v0, v1

    :goto_13
    invoke-virtual {p0}, Lau/h;->d()Lau/Y;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_30

    :goto_1d
    invoke-virtual {p0, v0, v1, v2}, Lau/h;->a(ZZZ)V

    .line 295
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p1}, Lau/p;->b(Lau/B;)V

    .line 296
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0, p2}, Lau/p;->a(Lau/Y;)V

    .line 297
    invoke-virtual {p0}, Lau/h;->m()V

    .line 298
    return-void

    :cond_2e
    move v0, v2

    .line 293
    goto :goto_13

    :cond_30
    move v1, v2

    goto :goto_1d
.end method

.method public d()Lau/Y;
    .registers 2

    .prologue
    .line 126
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->c()Lau/Y;

    move-result-object v0

    return-object v0
.end method

.method public e()F
    .registers 2

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public f()Lau/H;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lau/h;->d:Lau/p;

    invoke-virtual {v0}, Lau/p;->b()Lau/H;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .registers 3

    .prologue
    .line 209
    iget-object v0, p0, Lau/h;->f:Lau/s;

    if-eqz v0, :cond_f

    .line 210
    iget-object v0, p0, Lau/h;->f:Lau/s;

    invoke-virtual {v0}, Lau/s;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 211
    invoke-virtual {p0}, Lau/h;->h()V

    .line 216
    :cond_f
    :goto_f
    return-void

    .line 213
    :cond_10
    iget-object v0, p0, Lau/h;->d:Lau/p;

    iget-object v1, p0, Lau/h;->f:Lau/s;

    invoke-virtual {v1}, Lau/s;->a()Lau/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lau/p;->b(Lau/B;)V

    goto :goto_f
.end method

.method public h()V
    .registers 2

    .prologue
    .line 219
    iget-object v0, p0, Lau/h;->f:Lau/s;

    if-eqz v0, :cond_a

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lau/h;->f:Lau/s;

    .line 221
    invoke-virtual {p0}, Lau/h;->m()V

    .line 223
    :cond_a
    return-void
.end method

.method public i()V
    .registers 1

    .prologue
    .line 272
    return-void
.end method

.method public j()Z
    .registers 3

    .prologue
    .line 307
    invoke-virtual {p0}, Lau/h;->d()Lau/Y;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Lau/Y;->c()Lau/Y;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Lau/Y;->a()I

    move-result v0

    invoke-virtual {p0}, Lau/h;->c()Lau/B;

    move-result-object v1

    invoke-virtual {p0, v1}, Lau/h;->a(Lau/B;)I

    move-result v1

    if-lt v0, v1, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public k()Z
    .registers 3

    .prologue
    .line 314
    invoke-virtual {p0}, Lau/h;->d()Lau/Y;

    move-result-object v0

    .line 315
    invoke-virtual {v0}, Lau/Y;->d()Lau/Y;

    move-result-object v1

    if-eqz v1, :cond_14

    invoke-virtual {v0}, Lau/Y;->a()I

    move-result v0

    invoke-static {}, Lau/Y;->e()I

    move-result v1

    if-gt v0, v1, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 321
    iget-object v0, p0, Lau/h;->f:Lau/s;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method
