.class public abstract Lau/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:I

.field private final c:Ljava/util/Set;

.field private final d:Landroid/graphics/Point;

.field private final e:Landroid/graphics/Point;

.field private f:Lau/Y;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lau/u;->c:Ljava/util/Set;

    .line 61
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lau/u;->d:Landroid/graphics/Point;

    .line 67
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lau/u;->e:Landroid/graphics/Point;

    return-void
.end method

.method public static a(Lau/H;Lau/H;)I
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 300
    if-eqz p0, :cond_1c

    if-eqz p1, :cond_1c

    invoke-virtual {p0}, Lau/H;->a()Lau/B;

    move-result-object v0

    if-eqz v0, :cond_1c

    invoke-virtual {p1}, Lau/H;->a()Lau/B;

    move-result-object v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lau/H;->b()Lau/Y;

    move-result-object v0

    if-eqz v0, :cond_1c

    invoke-virtual {p1}, Lau/H;->a()Lau/B;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 303
    :cond_1c
    const/4 v0, -0x1

    .line 319
    :goto_1d
    return v0

    .line 306
    :cond_1e
    invoke-virtual {p0}, Lau/H;->a()Lau/B;

    move-result-object v0

    .line 307
    invoke-virtual {p1}, Lau/H;->a()Lau/B;

    move-result-object v1

    .line 309
    invoke-virtual {v0}, Lau/B;->e()I

    move-result v2

    invoke-virtual {p0}, Lau/H;->b()Lau/Y;

    move-result-object v3

    invoke-static {v2, v3}, Lau/B;->a(ILau/Y;)I

    move-result v2

    .line 310
    invoke-virtual {v0}, Lau/B;->c()I

    move-result v0

    invoke-virtual {p0}, Lau/H;->b()Lau/Y;

    move-result-object v3

    invoke-static {v0, v3}, Lau/B;->b(ILau/Y;)I

    move-result v0

    .line 312
    invoke-virtual {v1}, Lau/B;->e()I

    move-result v3

    invoke-virtual {p1}, Lau/H;->b()Lau/Y;

    move-result-object v4

    invoke-static {v3, v4}, Lau/B;->a(ILau/Y;)I

    move-result v3

    .line 313
    invoke-virtual {v1}, Lau/B;->c()I

    move-result v1

    invoke-virtual {p1}, Lau/H;->b()Lau/Y;

    move-result-object v4

    invoke-static {v1, v4}, Lau/B;->b(ILau/Y;)I

    move-result v1

    .line 315
    sub-int/2addr v2, v3

    .line 316
    sub-int/2addr v0, v1

    .line 318
    mul-int v1, v2, v2

    mul-int/2addr v0, v0

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 319
    goto :goto_1d
.end method

.method public static b(I)I
    .registers 3
    .parameter

    .prologue
    .line 291
    const/16 v0, 0x50

    invoke-static {v0, p0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 292
    mul-int/lit8 v0, v0, 0xa

    add-int/lit16 v0, v0, 0xc8

    const/16 v1, 0x2bc

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a(ILau/B;)F
.end method

.method public abstract a()I
.end method

.method public a(ILau/H;)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 214
    invoke-virtual {p0, p2}, Lau/u;->a(Lau/H;)I

    move-result v0

    .line 215
    int-to-float v1, p1

    invoke-static {}, Lcom/google/googlenav/ui/bq;->d()Lcom/google/googlenav/ui/bq;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bq;->F()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 216
    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 217
    sub-int/2addr v0, v1

    return v0
.end method

.method public abstract a(Lau/B;)I
.end method

.method public abstract a(Lau/H;)I
.end method

.method protected abstract a(Lau/B;Lau/Y;II)Lau/B;
.end method

.method public a(Lcom/google/googlenav/E;Lau/B;IIIIIIILau/Y;)Lau/B;
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 597
    invoke-interface {p1}, Lcom/google/googlenav/E;->a()Lau/B;

    move-result-object v1

    .line 598
    if-nez v1, :cond_7

    .line 660
    :goto_6
    return-object p2

    .line 602
    :cond_7
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 603
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 604
    move-object/from16 v0, p10

    invoke-virtual {p0, p2, v0, p2, v5}, Lau/u;->a(Lau/B;Lau/Y;Lau/B;Landroid/graphics/Point;)V

    .line 605
    move-object/from16 v0, p10

    invoke-virtual {p0, p2, v0, v1, v4}, Lau/u;->a(Lau/B;Lau/Y;Lau/B;Landroid/graphics/Point;)V

    .line 608
    iget v1, v4, Landroid/graphics/Point;->x:I

    iget v2, v5, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    add-int v6, v1, p7

    .line 609
    iget v1, v4, Landroid/graphics/Point;->y:I

    iget v2, v5, Landroid/graphics/Point;->y:I

    sub-int v7, v1, v2

    .line 614
    move/from16 v0, p5

    invoke-static {p3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    div-int/lit8 v8, v1, 0x2

    .line 615
    add-int v9, p4, p6

    .line 620
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ak()Z

    move-result v1

    if-eqz v1, :cond_9d

    const/4 v1, 0x5

    .line 623
    :goto_3d
    const/4 v2, 0x0

    .line 626
    neg-int v3, v6

    add-int v3, v3, p9

    add-int/2addr v3, v8

    iget v10, p0, Lau/u;->a:I

    div-int/lit8 v10, v10, 0x2

    if-gt v3, v10, :cond_50

    add-int v3, v6, v8

    iget v10, p0, Lau/u;->a:I

    div-int/lit8 v10, v10, 0x2

    if-le v3, v10, :cond_9f

    :cond_50
    const/4 v3, 0x1

    .line 628
    :goto_51
    if-eqz v3, :cond_63

    .line 629
    iget v2, v4, Landroid/graphics/Point;->x:I

    iget v3, v5, Landroid/graphics/Point;->x:I

    div-int/lit8 v4, p9, 0x2

    add-int/2addr v3, v4

    if-le v2, v3, :cond_a1

    .line 631
    iget v2, p0, Lau/u;->a:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v2, v6

    sub-int/2addr v2, v8

    neg-int v2, v2

    .line 639
    :cond_63
    :goto_63
    const/4 v3, 0x0

    .line 641
    neg-int v4, v7

    add-int v4, v4, p8

    add-int/2addr v4, v9

    iget v6, p0, Lau/u;->b:I

    div-int/lit8 v6, v6, 0x2

    if-gt v4, v6, :cond_76

    add-int v4, v7, v1

    iget v6, p0, Lau/u;->b:I

    div-int/lit8 v6, v6, 0x2

    if-le v4, v6, :cond_aa

    :cond_76
    const/4 v4, 0x1

    .line 644
    :goto_77
    if-eqz v4, :cond_b5

    .line 645
    if-lez v7, :cond_ac

    .line 647
    iget v3, p0, Lau/u;->b:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v7, v3

    add-int/2addr v1, v3

    .line 654
    :goto_82
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 655
    iget v4, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Point;->x:I

    .line 656
    iget v2, v5, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Point;->y:I

    .line 657
    iget v1, v3, Landroid/graphics/Point;->x:I

    iget v2, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p10

    invoke-virtual {p0, p2, v0, v1, v2}, Lau/u;->a(Lau/B;Lau/Y;II)Lau/B;

    move-result-object p2

    goto/16 :goto_6

    .line 620
    :cond_9d
    const/4 v1, 0x0

    goto :goto_3d

    .line 626
    :cond_9f
    const/4 v3, 0x0

    goto :goto_51

    .line 634
    :cond_a1
    iget v2, p0, Lau/u;->a:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v6

    sub-int/2addr v2, v8

    sub-int v2, v2, p9

    goto :goto_63

    .line 641
    :cond_aa
    const/4 v4, 0x0

    goto :goto_77

    .line 650
    :cond_ac
    iget v1, p0, Lau/u;->b:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v7

    sub-int/2addr v1, v9

    sub-int v1, v1, p8

    goto :goto_82

    :cond_b5
    move v1, v3

    goto :goto_82
.end method

.method public a(IIII)Lau/Y;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 503
    invoke-virtual {p0}, Lau/u;->f()Lau/H;

    move-result-object v1

    .line 504
    invoke-virtual {v1}, Lau/H;->a()Lau/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lau/u;->a(Lau/B;)I

    move-result v0

    invoke-static {v0}, Lau/Y;->b(I)Lau/Y;

    move-result-object v0

    .line 505
    if-nez v0, :cond_33

    .line 506
    invoke-static {}, Lau/Y;->e()I

    move-result v0

    invoke-static {v0}, Lau/Y;->b(I)Lau/Y;

    move-result-object v0

    .line 524
    :cond_1a
    :goto_1a
    return-object v0

    :cond_1b
    move-object v0, v1

    .line 510
    :goto_1c
    invoke-virtual {v0}, Lau/Y;->d()Lau/Y;

    move-result-object v1

    .line 511
    if-eqz v1, :cond_1a

    .line 516
    invoke-virtual {v2, v0}, Lau/H;->a(Lau/Y;)Lau/H;

    move-result-object v2

    .line 517
    invoke-virtual {p0, p4, v2}, Lau/u;->a(ILau/H;)I

    move-result v3

    if-gt p1, v3, :cond_1b

    invoke-virtual {p0, p3, v2}, Lau/u;->b(ILau/H;)I

    move-result v3

    if-gt p2, v3, :cond_1b

    goto :goto_1a

    :cond_33
    move-object v2, v1

    goto :goto_1c
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method protected abstract a(Lau/B;Landroid/graphics/Point;)V
.end method

.method public abstract a(Lau/B;Lau/Y;)V
.end method

.method protected abstract a(Lau/B;Lau/Y;Lau/B;Landroid/graphics/Point;)V
.end method

.method public final a(Lau/Y;)V
    .registers 3
    .parameter

    .prologue
    .line 484
    invoke-virtual {p0}, Lau/u;->q()V

    .line 486
    invoke-virtual {p0}, Lau/u;->c()Lau/B;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lau/u;->e(Lau/B;Lau/Y;)V

    .line 487
    return-void
.end method

.method protected abstract a(Lau/Y;II)V
.end method

.method public a(Lau/v;)V
    .registers 4
    .parameter

    .prologue
    .line 82
    iget-object v1, p0, Lau/u;->c:Ljava/util/Set;

    monitor-enter v1

    .line 83
    :try_start_3
    iget-object v0, p0, Lau/u;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    monitor-exit v1

    .line 85
    return-void

    .line 84
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public abstract a(Lo/B;)V
.end method

.method protected a(ZZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 99
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lau/u;->a(ZZZII)V

    .line 100
    return-void
.end method

.method protected a(ZZZII)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    iget-object v1, p0, Lau/u;->c:Ljava/util/Set;

    monitor-enter v1

    .line 110
    :try_start_3
    iget-object v0, p0, Lau/u;->c:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    .line 111
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_23

    .line 113
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lau/v;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 114
    invoke-interface/range {v0 .. v5}, Lau/v;->a(ZZZII)V

    goto :goto_e

    .line 111
    :catchall_23
    move-exception v0

    :try_start_24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    throw v0

    .line 116
    :cond_26
    return-void
.end method

.method public abstract a([Lau/B;IIILau/Y;)V
.end method

.method public declared-synchronized a(Lau/B;Lau/B;I)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 693
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lau/u;->d:Landroid/graphics/Point;

    invoke-virtual {p0, p1, v0}, Lau/u;->a(Lau/B;Landroid/graphics/Point;)V

    .line 694
    iget-object v0, p0, Lau/u;->e:Landroid/graphics/Point;

    invoke-virtual {p0, p2, v0}, Lau/u;->a(Lau/B;Landroid/graphics/Point;)V

    .line 695
    iget-object v0, p0, Lau/u;->d:Landroid/graphics/Point;

    iget-object v1, p0, Lau/u;->e:Landroid/graphics/Point;

    invoke-static {v0, v1, p3}, LT/r;->a(Landroid/graphics/Point;Landroid/graphics/Point;I)Z
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_15

    move-result v0

    monitor-exit p0

    return v0

    .line 693
    :catchall_15
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)Z
    .registers 4
    .parameter

    .prologue
    .line 412
    iget v0, p0, Lau/u;->a:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lau/u;->b:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, p1, v0, v1}, Lau/u;->a(ZII)Z

    move-result v0

    return v0
.end method

.method public final a(ZII)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 425
    iget-object v0, p0, Lau/u;->f:Lau/Y;

    if-nez v0, :cond_a

    .line 426
    invoke-virtual {p0}, Lau/u;->d()Lau/Y;

    move-result-object v0

    iput-object v0, p0, Lau/u;->f:Lau/Y;

    .line 429
    :cond_a
    if-eqz p1, :cond_22

    iget-object v0, p0, Lau/u;->f:Lau/Y;

    invoke-virtual {v0}, Lau/Y;->d()Lau/Y;

    move-result-object v0

    .line 436
    :goto_12
    if-eqz v0, :cond_20

    invoke-virtual {v0}, Lau/Y;->a()I

    move-result v1

    iget-object v2, p0, Lau/u;->f:Lau/Y;

    invoke-virtual {v2}, Lau/Y;->a()I

    move-result v2

    if-ne v1, v2, :cond_29

    .line 438
    :cond_20
    const/4 v0, 0x0

    .line 446
    :goto_21
    return v0

    .line 429
    :cond_22
    iget-object v0, p0, Lau/u;->f:Lau/Y;

    invoke-virtual {v0}, Lau/Y;->c()Lau/Y;

    move-result-object v0

    goto :goto_12

    .line 440
    :cond_29
    invoke-virtual {p0, v0, p2, p3}, Lau/u;->a(Lau/Y;II)V

    .line 444
    iput-object v0, p0, Lau/u;->f:Lau/Y;

    .line 446
    const/4 v0, 0x1

    goto :goto_21
.end method

.method public abstract b()I
.end method

.method public b(ILau/H;)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 231
    invoke-virtual {p0, p2}, Lau/u;->b(Lau/H;)I

    move-result v0

    .line 232
    int-to-float v1, p1

    invoke-static {}, Lcom/google/googlenav/ui/bq;->d()Lcom/google/googlenav/ui/bq;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bq;->E()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 233
    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 234
    sub-int/2addr v0, v1

    return v0
.end method

.method public abstract b(Lau/H;)I
.end method

.method public b(II)Lau/B;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 742
    invoke-virtual {p0}, Lau/u;->c()Lau/B;

    move-result-object v0

    invoke-virtual {p0}, Lau/u;->d()Lau/Y;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1, p2}, Lau/u;->a(Lau/B;Lau/Y;II)Lau/B;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lau/B;)V
    .registers 3
    .parameter

    .prologue
    .line 335
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lau/u;->b(Lau/B;Lau/Y;)V

    .line 336
    return-void
.end method

.method protected abstract b(Lau/B;Lau/Y;)V
.end method

.method public abstract c()Lau/B;
.end method

.method public c(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 140
    iput p1, p0, Lau/u;->a:I

    .line 141
    iput p2, p0, Lau/u;->b:I

    .line 142
    return-void
.end method

.method public final c(Lau/B;)V
    .registers 3
    .parameter

    .prologue
    .line 473
    invoke-virtual {p0}, Lau/u;->d()Lau/Y;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lau/u;->e(Lau/B;Lau/Y;)V

    .line 474
    return-void
.end method

.method protected abstract c(Lau/B;Lau/Y;)V
.end method

.method public abstract d()Lau/Y;
.end method

.method public final d(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 532
    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v1

    invoke-virtual {p0, p1, p2, v0, v1}, Lau/u;->a(IIII)Lau/Y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lau/u;->a(Lau/Y;)V

    .line 534
    return-void
.end method

.method public final d(Lau/B;Lau/Y;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 347
    invoke-virtual {p0, p1, p2}, Lau/u;->b(Lau/B;Lau/Y;)V

    .line 348
    return-void
.end method

.method public d(Lau/B;)Z
    .registers 5
    .parameter

    .prologue
    .line 668
    invoke-virtual {p0, p1}, Lau/u;->f(Lau/B;)Landroid/graphics/Point;

    move-result-object v0

    .line 669
    iget v1, v0, Landroid/graphics/Point;->x:I

    if-ltz v1, :cond_1a

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, Lau/u;->a:I

    if-ge v1, v2, :cond_1a

    iget v1, v0, Landroid/graphics/Point;->y:I

    if-ltz v1, :cond_1a

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v1, p0, Lau/u;->b:I

    if-ge v0, v1, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public abstract e()F
.end method

.method public final e(Lau/B;Lau/Y;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 544
    invoke-virtual {p0, p1, p2}, Lau/u;->c(Lau/B;Lau/Y;)V

    .line 545
    return-void
.end method

.method public e(Lau/B;)Z
    .registers 5
    .parameter

    .prologue
    .line 682
    invoke-virtual {p0, p1}, Lau/u;->f(Lau/B;)Landroid/graphics/Point;

    move-result-object v0

    .line 683
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, Lau/u;->a:I

    neg-int v2, v2

    if-lt v1, v2, :cond_24

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, Lau/u;->a:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v1, v2, :cond_24

    iget v1, v0, Landroid/graphics/Point;->y:I

    iget v2, p0, Lau/u;->b:I

    neg-int v2, v2

    if-lt v1, v2, :cond_24

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v1, p0, Lau/u;->b:I

    mul-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_24

    const/4 v0, 0x1

    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x0

    goto :goto_23
.end method

.method public f(Lau/B;)Landroid/graphics/Point;
    .registers 3
    .parameter

    .prologue
    .line 729
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 730
    invoke-virtual {p0, p1, v0}, Lau/u;->a(Lau/B;Landroid/graphics/Point;)V

    .line 731
    return-object v0
.end method

.method public abstract f()Lau/H;
.end method

.method public abstract i()V
.end method

.method public abstract j()Z
.end method

.method public abstract k()Z
.end method

.method public abstract l()Z
.end method

.method protected m()V
    .registers 3

    .prologue
    .line 124
    iget-object v1, p0, Lau/u;->c:Ljava/util/Set;

    monitor-enter v1

    .line 125
    :try_start_3
    iget-object v0, p0, Lau/u;->c:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    .line 126
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_1e

    .line 128
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lau/v;

    .line 129
    invoke-interface {v0}, Lau/v;->i()V

    goto :goto_e

    .line 126
    :catchall_1e
    move-exception v0

    :try_start_1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    throw v0

    .line 131
    :cond_21
    return-void
.end method

.method public n()I
    .registers 2

    .prologue
    .line 146
    iget v0, p0, Lau/u;->a:I

    return v0
.end method

.method public o()I
    .registers 2

    .prologue
    .line 151
    iget v0, p0, Lau/u;->b:I

    return v0
.end method

.method public p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 242
    invoke-virtual {p0}, Lau/u;->c()Lau/B;

    move-result-object v0

    invoke-virtual {p0}, Lau/u;->a()I

    move-result v1

    invoke-virtual {p0}, Lau/u;->b()I

    move-result v2

    invoke-virtual {p0}, Lau/u;->d()Lau/Y;

    move-result-object v3

    invoke-virtual {v3}, Lau/Y;->a()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lau/C;->a(Lau/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public final q()V
    .registers 2

    .prologue
    .line 455
    const/4 v0, 0x0

    iput-object v0, p0, Lau/u;->f:Lau/Y;

    .line 456
    return-void
.end method
