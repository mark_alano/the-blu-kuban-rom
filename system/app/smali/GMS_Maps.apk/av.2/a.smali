.class public Lav/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private c:Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;

.field private final d:Lcom/google/googlenav/ui/s;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 26
    const/16 v0, 0xfa0

    sput v0, Lav/a;->a:I

    .line 32
    const/16 v0, 0xf

    sput v0, Lav/a;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;)V
    .registers 2
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    .line 43
    return-void
.end method

.method static synthetic a(Lav/a;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method static synthetic a(Lav/a;Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;)Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lav/a;->c:Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;

    return-object p1
.end method

.method static synthetic b(Lav/a;)Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lav/a;->c:Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;

    return-object v0
.end method


# virtual methods
.method public a(ILjava/lang/CharSequence;IIIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;I)V
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    new-instance v1, Lav/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v3

    move-object v2, p0

    move v4, p1

    move-object v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lav/b;-><init>(Lav/a;Las/c;ILjava/lang/CharSequence;IIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;I)V

    .line 81
    sget v2, Lav/a;->a:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Las/d;->c(J)V

    .line 82
    sget v2, Lav/a;->b:I

    invoke-virtual {v1, v2}, Las/d;->b(I)V

    .line 83
    move/from16 v0, p7

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Las/d;->a(J)V

    .line 84
    invoke-virtual {v1}, Las/d;->g()V

    .line 85
    return-void
.end method

.method protected a()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 149
    iget-object v1, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v1

    if-nez v1, :cond_a

    .line 159
    :cond_9
    :goto_9
    return v0

    .line 153
    :cond_a
    iget-object v1, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    .line 154
    invoke-virtual {v1}, Lbf/am;->C()Lbf/aJ;

    move-result-object v2

    if-nez v2, :cond_9

    invoke-virtual {v1}, Lbf/am;->v()Lbf/O;

    move-result-object v2

    if-nez v2, :cond_9

    invoke-virtual {v1}, Lbf/am;->w()Lbf/bK;

    move-result-object v1

    if-nez v1, :cond_9

    .line 159
    const/4 v0, 0x1

    goto :goto_9
.end method

.method a(ILjava/lang/CharSequence;IIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;I)Z
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-virtual {p0}, Lav/a;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 98
    const/4 v0, 0x0

    .line 133
    :goto_7
    return v0

    .line 100
    :cond_8
    const/4 v11, 0x1

    .line 101
    iget-object v0, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v12

    new-instance v0, Lav/c;

    move-object v1, p0

    move/from16 v2, p9

    move/from16 v3, p3

    move-object v4, p2

    move v5, p1

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lav/c;-><init>(Lav/a;IILjava/lang/CharSequence;IIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;)V

    invoke-virtual {v12, v0, v11}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 133
    const/4 v0, 0x1

    goto :goto_7
.end method
