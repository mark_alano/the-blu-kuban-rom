.class public Lav/e;
.super Lau/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/dM;


# instance fields
.field private final c:Lcom/google/android/maps/driveabout/vector/dK;

.field private final d:Lav/a;

.field private final e:Lcom/google/android/maps/driveabout/vector/k;

.field private final f:[F

.field private final g:Lo/Q;

.field private final h:Lo/Q;

.field private final i:Lcom/google/googlenav/android/Y;

.field private j:F

.field private k:Lav/i;

.field private final l:Lcom/google/android/maps/driveabout/vector/ct;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/dK;Lav/a;Lau/H;Lcom/google/googlenav/android/Y;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 123
    invoke-direct {p0}, Lau/u;-><init>()V

    .line 72
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lav/e;->f:[F

    .line 78
    new-instance v0, Lo/Q;

    invoke-direct {v0}, Lo/Q;-><init>()V

    iput-object v0, p0, Lav/e;->g:Lo/Q;

    .line 85
    new-instance v0, Lo/Q;

    invoke-direct {v0}, Lo/Q;-><init>()V

    iput-object v0, p0, Lav/e;->h:Lo/Q;

    .line 92
    iput v4, p0, Lav/e;->j:F

    .line 102
    new-instance v0, Lav/f;

    invoke-direct {v0, p0}, Lav/f;-><init>(Lav/e;)V

    iput-object v0, p0, Lav/e;->l:Lcom/google/android/maps/driveabout/vector/ct;

    .line 124
    iput-object p1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    .line 125
    invoke-virtual {p1, p0}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/dM;)V

    .line 126
    iput-object p2, p0, Lav/e;->d:Lav/a;

    .line 127
    iput-object p4, p0, Lav/e;->i:Lcom/google/googlenav/android/Y;

    .line 128
    invoke-static {p3, v4}, Lav/e;->a(Lau/H;F)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    .line 130
    new-instance v1, Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {p2}, Lav/a;->t()I

    move-result v2

    invoke-virtual {p2}, Lav/a;->s()I

    move-result v3

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/k;-><init>(Lcom/google/android/maps/driveabout/vector/l;IIF)V

    iput-object v1, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    .line 131
    iget-object v1, p0, Lav/e;->l:Lcom/google/android/maps/driveabout/vector/ct;

    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/ct;)V

    .line 132
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;I)V

    .line 133
    invoke-virtual {p2}, Lav/a;->t()I

    move-result v0

    invoke-virtual {p2}, Lav/a;->s()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lav/e;->c(II)V

    .line 135
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/dQ;)V

    .line 136
    return-void
.end method

.method public static a(Lau/Y;F)F
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 176
    invoke-virtual {p0}, Lau/Y;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    return v0
.end method

.method private declared-synchronized a(IILcom/google/android/maps/driveabout/vector/l;)Lau/B;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 613
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0, p3}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 614
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/k;->d(FF)Lo/Q;

    move-result-object v0

    invoke-static {v0}, Lu/e;->b(Lo/Q;)Lau/B;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_14

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 613
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/maps/driveabout/vector/l;)Lau/B;
    .registers 3
    .parameter

    .prologue
    .line 260
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 261
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/k;->b()Lo/Q;

    move-result-object v0

    invoke-static {v0}, Lu/e;->b(Lo/Q;)Lau/B;
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 260
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lav/e;)Lav/a;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lav/e;->d:Lav/a;

    return-object v0
.end method

.method static synthetic a(Lav/e;Lav/i;)Lav/i;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lav/e;->k:Lav/i;

    return-object p1
.end method

.method static a(Lau/H;F)Lcom/google/android/maps/driveabout/vector/l;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 161
    invoke-virtual {p0}, Lau/H;->a()Lau/B;

    move-result-object v1

    .line 162
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-static {v1}, Lu/e;->a(Lau/B;)Lo/Q;

    move-result-object v1

    invoke-virtual {p0}, Lau/H;->b()Lau/Y;

    move-result-object v2

    invoke-static {v2, p1}, Lav/e;->a(Lau/Y;F)F

    move-result v2

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 168
    return-object v0
.end method

.method private declared-synchronized a(Lau/B;Lau/Y;Z)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 336
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v4

    .line 337
    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    .line 341
    if-eqz p2, :cond_10b

    .line 342
    iget v1, p0, Lav/e;->j:F

    invoke-static {p2, v1}, Lav/e;->a(Lau/Y;F)F

    move-result v2

    .line 343
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    if-eq v1, v3, :cond_10b

    .line 348
    :goto_1d
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 349
    iget v0, p0, Lav/e;->a:I

    div-int/lit8 v0, v0, 0x2

    .line 350
    iget v1, p0, Lav/e;->b:I

    div-int/lit8 v1, v1, 0x2

    .line 351
    iget-object v3, p0, Lav/e;->g:Lo/Q;

    invoke-static {p1, v3}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 352
    iget-object v3, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget-object v5, p0, Lav/e;->g:Lo/Q;

    invoke-virtual {v3, v5}, Lcom/google/android/maps/driveabout/vector/k;->b(Lo/Q;)[I

    move-result-object v3

    .line 353
    const/4 v5, 0x0

    aget v5, v3, v5

    sub-int v7, v5, v0

    .line 354
    const/4 v0, 0x1

    aget v0, v3, v0

    sub-int v6, v0, v1

    .line 355
    int-to-double v0, v7

    int-to-double v8, v7

    mul-double/2addr v0, v8

    int-to-double v8, v6

    int-to-double v10, v6

    mul-double/2addr v8, v10

    add-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v8, v0

    .line 357
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    iget-object v1, p0, Lav/e;->g:Lo/Q;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 363
    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/l;->equals(Ljava/lang/Object;)Z
    :try_end_66
    .catchall {:try_start_1 .. :try_end_66} :catchall_cd

    move-result v1

    if-eqz v1, :cond_6b

    .line 395
    :cond_69
    :goto_69
    monitor-exit p0

    return-void

    .line 370
    :cond_6b
    const/4 v9, -0x1

    .line 371
    if-eqz p3, :cond_dc

    .line 376
    :try_start_6e
    iget-object v1, p0, Lav/e;->k:Lav/i;

    if-eqz v1, :cond_7a

    iget-object v1, p0, Lav/e;->k:Lav/i;

    invoke-virtual {v1, v0}, Lav/i;->a(Lcom/google/android/maps/driveabout/vector/l;)Z

    move-result v1

    if-nez v1, :cond_69

    :cond_7a
    invoke-direct {p0, v0, v8}, Lav/e;->a(Lcom/google/android/maps/driveabout/vector/l;I)Z

    move-result v1

    if-nez v1, :cond_69

    .line 379
    iget-object v1, p0, Lav/e;->g:Lo/Q;

    iget-object v3, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/k;->b()Lo/Q;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/Q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d0

    const/4 v1, 0x1

    move v5, v1

    :goto_90
    iget-object v1, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/k;->l()F

    move-result v1

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_d3

    const/4 v3, 0x1

    :goto_9b
    const/4 v4, 0x1

    move-object v1, p0

    move v2, v5

    move v5, v7

    invoke-virtual/range {v1 .. v6}, Lav/e;->a(ZZZII)V

    .line 381
    new-instance v1, Lav/i;

    invoke-direct {v1, v0}, Lav/i;-><init>(Lcom/google/android/maps/driveabout/vector/l;)V

    iput-object v1, p0, Lav/e;->k:Lav/i;

    .line 382
    iget-object v0, p0, Lav/e;->k:Lav/i;

    invoke-virtual {v0}, Lav/i;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_d5

    const/4 v0, 0x1

    .line 384
    :goto_c2
    if-eqz v0, :cond_d7

    const/4 v0, -0x1

    .line 386
    :goto_c5
    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    iget-object v2, p0, Lav/e;->k:Lav/i;

    invoke-virtual {v1, v2, v0, v9}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;II)V
    :try_end_cc
    .catchall {:try_start_6e .. :try_end_cc} :catchall_cd

    goto :goto_69

    .line 336
    :catchall_cd
    move-exception v0

    monitor-exit p0

    throw v0

    .line 379
    :cond_d0
    const/4 v1, 0x0

    move v5, v1

    goto :goto_90

    :cond_d3
    const/4 v3, 0x0

    goto :goto_9b

    .line 382
    :cond_d5
    const/4 v0, 0x0

    goto :goto_c2

    .line 384
    :cond_d7
    :try_start_d7
    invoke-static {v8}, Lav/e;->b(I)I

    move-result v0

    goto :goto_c5

    .line 389
    :cond_dc
    iget-object v1, p0, Lav/e;->g:Lo/Q;

    iget-object v3, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/k;->b()Lo/Q;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/Q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_106

    const/4 v1, 0x1

    move v5, v1

    :goto_ec
    iget-object v1, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/k;->l()F

    move-result v1

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_109

    const/4 v3, 0x1

    :goto_f7
    const/4 v4, 0x1

    move-object v1, p0

    move v2, v5

    move v5, v7

    invoke-virtual/range {v1 .. v6}, Lav/e;->a(ZZZII)V

    .line 391
    const/4 v1, -0x1

    .line 393
    iget-object v2, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v2, v0, v1, v9}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;II)V
    :try_end_104
    .catchall {:try_start_d7 .. :try_end_104} :catchall_cd

    goto/16 :goto_69

    .line 389
    :cond_106
    const/4 v1, 0x0

    move v5, v1

    goto :goto_ec

    :cond_109
    const/4 v3, 0x0

    goto :goto_f7

    :cond_10b
    move v2, v0

    goto/16 :goto_1d
.end method

.method private declared-synchronized a(Lo/Q;Lcom/google/android/maps/driveabout/vector/l;Landroid/graphics/Point;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 593
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 594
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget-object v1, p0, Lav/e;->f:[F

    invoke-virtual {v0, p1, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lo/Q;[F)V

    .line 595
    iget-object v0, p0, Lav/e;->f:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lav/e;->f:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Point;->set(II)V
    :try_end_22
    .catchall {:try_start_1 .. :try_end_22} :catchall_24

    .line 596
    monitor-exit p0

    return-void

    .line 593
    :catchall_24
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/l;I)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    iget-object v1, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/k;->l()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_20

    const/16 v0, 0xf

    if-ge p2, v0, :cond_20

    :cond_1e
    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private b(Lcom/google/android/maps/driveabout/vector/l;)Lau/Y;
    .registers 4
    .parameter

    .prologue
    .line 297
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    sget-object v1, Lav/j;->a:Lav/j;

    invoke-virtual {p0, v0, v1}, Lav/e;->a(FLav/j;)Lau/Y;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lav/e;)Lcom/google/googlenav/android/Y;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lav/e;->i:Lcom/google/googlenav/android/Y;

    return-object v0
.end method

.method private g()I
    .registers 6

    .prologue
    .line 226
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/k;->w()Lo/aK;

    move-result-object v0

    .line 227
    invoke-virtual {v0}, Lo/aK;->g()Lo/Q;

    move-result-object v1

    invoke-virtual {v1}, Lo/Q;->a()I

    move-result v1

    .line 228
    invoke-virtual {v0}, Lo/aK;->f()Lo/Q;

    move-result-object v2

    invoke-virtual {v2}, Lo/Q;->a()I

    move-result v2

    .line 229
    invoke-virtual {v0}, Lo/aK;->d()Lo/Q;

    move-result-object v3

    invoke-virtual {v3}, Lo/Q;->a()I

    move-result v3

    .line 230
    invoke-virtual {v0}, Lo/aK;->e()Lo/Q;

    move-result-object v0

    invoke-virtual {v0}, Lo/Q;->a()I

    move-result v0

    .line 233
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 234
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 235
    sub-int v0, v4, v0

    return v0
.end method

.method private h()I
    .registers 4

    .prologue
    .line 239
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/k;->w()Lo/aK;

    move-result-object v0

    invoke-virtual {v0}, Lo/aK;->a()Lo/aL;

    move-result-object v0

    invoke-virtual {v0}, Lo/aL;->d()I

    move-result v0

    .line 240
    iget-object v1, p0, Lav/e;->g:Lo/Q;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lo/Q;->d(II)V

    .line 241
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    invoke-virtual {v0}, Lo/Q;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized a(ILau/B;)F
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 192
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 193
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    invoke-static {p2, v0}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 194
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget-object v2, p0, Lav/e;->g:Lo/Q;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/k;->a(Lo/Q;Z)F

    move-result v0

    .line 196
    int-to-float v1, p1

    iget-object v2, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget-object v3, p0, Lav/e;->g:Lo/Q;

    invoke-virtual {v3}, Lo/Q;->e()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/maps/driveabout/vector/k;->b(FF)F
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_2b

    move-result v0

    mul-float/2addr v0, v1

    monitor-exit p0

    return v0

    .line 192
    :catchall_2b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()I
    .registers 3

    .prologue
    .line 202
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 203
    invoke-direct {p0}, Lav/e;->g()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 202
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lau/B;)I
    .registers 4
    .parameter

    .prologue
    .line 525
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    invoke-static {p1, v0}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 526
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    iget-object v1, p0, Lav/e;->g:Lo/Q;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lo/Q;)F

    move-result v0

    float-to-int v0, v0

    .line 531
    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_19

    move-result v0

    monitor-exit p0

    return v0

    .line 525
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lau/H;)I
    .registers 4
    .parameter

    .prologue
    .line 214
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget v1, p0, Lav/e;->j:F

    invoke-static {p1, v1}, Lav/e;->a(Lau/H;F)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 215
    invoke-direct {p0}, Lav/e;->g()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 214
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lau/B;Lau/Y;II)Lau/B;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 600
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v4

    .line 601
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    invoke-static {p1, v0}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 602
    const/4 v5, 0x0

    .line 603
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    iget-object v1, p0, Lav/e;->g:Lo/Q;

    iget v2, p0, Lav/e;->j:F

    invoke-static {p2, v2}, Lav/e;->a(Lau/Y;F)F

    move-result v2

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 609
    invoke-direct {p0, p3, p4, v0}, Lav/e;->a(IILcom/google/android/maps/driveabout/vector/l;)Lau/B;

    move-result-object v0

    return-object v0
.end method

.method protected a(FLav/j;)Lau/Y;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 279
    const/high16 v0, 0x3f80

    add-float/2addr v0, p1

    .line 281
    sget-object v1, Lav/h;->a:[I

    invoke-virtual {p2}, Lav/j;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2c

    .line 290
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 293
    :goto_12
    const/16 v1, 0x16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, Lau/Y;->b(I)Lau/Y;

    move-result-object v0

    return-object v0

    .line 283
    :pswitch_1d
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 284
    goto :goto_12

    .line 286
    :pswitch_24
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 287
    goto :goto_12

    .line 281
    nop

    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_24
    .end packed-switch
.end method

.method public declared-synchronized a(F)V
    .registers 5
    .parameter

    .prologue
    .line 186
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget v1, p0, Lav/e;->a:I

    iget v2, p0, Lav/e;->b:I

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/maps/driveabout/vector/k;->a(IIF)V

    .line 187
    iput p1, p0, Lav/e;->j:F
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    .line 188
    monitor-exit p0

    return-void

    .line 186
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 311
    iget-object v0, p0, Lav/e;->d:Lav/a;

    invoke-virtual {v0, p1}, Lav/a;->b(I)V

    .line 312
    return-void
.end method

.method public declared-synchronized a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 476
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dK;->b(FF)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    .line 477
    monitor-exit p0

    return-void

    .line 476
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Lau/B;Landroid/graphics/Point;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 586
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    invoke-static {p1, v0}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 587
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lav/e;->a(Lo/Q;Lcom/google/android/maps/driveabout/vector/l;Landroid/graphics/Point;)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    .line 588
    monitor-exit p0

    return-void

    .line 586
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lau/B;Lau/Y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 322
    monitor-enter p0

    const/4 v0, 0x1

    .line 323
    :try_start_2
    invoke-direct {p0, p1, p2, v0}, Lav/e;->a(Lau/B;Lau/Y;Z)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 324
    monitor-exit p0

    return-void

    .line 322
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Lau/B;Lau/Y;Lau/B;Landroid/graphics/Point;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 570
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v4

    .line 571
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    invoke-static {p1, v0}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 572
    iget-object v0, p0, Lav/e;->h:Lo/Q;

    invoke-static {p3, v0}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 573
    const/4 v5, 0x0

    .line 574
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    iget-object v1, p0, Lav/e;->g:Lo/Q;

    iget v2, p0, Lav/e;->j:F

    invoke-static {p2, v2}, Lav/e;->a(Lau/Y;F)F

    move-result v2

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 580
    iget-object v1, p0, Lav/e;->h:Lo/Q;

    invoke-direct {p0, v1, v0, p4}, Lav/e;->a(Lo/Q;Lcom/google/android/maps/driveabout/vector/l;Landroid/graphics/Point;)V
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2e

    .line 581
    monitor-exit p0

    return-void

    .line 570
    :catchall_2e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(Lau/Y;II)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 495
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lav/e;->j:F

    invoke-static {p1, v0}, Lav/e;->a(Lau/Y;F)F

    move-result v0

    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v1

    sub-float/2addr v0, v1

    .line 500
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lav/e;->a(ZZZ)V

    .line 501
    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    int-to-float v2, p2

    int-to-float v3, p3

    const/16 v4, 0x14a

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/dK;->a(FFFI)F
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    .line 502
    monitor-exit p0

    return-void

    .line 495
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lo/B;)V
    .registers 3
    .parameter

    .prologue
    .line 641
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lo/B;)V

    .line 642
    return-void
.end method

.method public a(ZZZII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-super/range {p0 .. p5}, Lau/u;->a(ZZZII)V

    .line 147
    return-void
.end method

.method public a([Lau/B;IIILau/Y;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 468
    if-nez p1, :cond_3

    .line 472
    :goto_2
    return-void

    .line 471
    :cond_3
    aget-object v0, p1, p3

    invoke-virtual {p0, v0, p5}, Lav/e;->d(Lau/B;Lau/Y;)V

    goto :goto_2
.end method

.method public declared-synchronized b()I
    .registers 3

    .prologue
    .line 208
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 209
    invoke-direct {p0}, Lav/e;->h()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 208
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lau/H;)I
    .registers 4
    .parameter

    .prologue
    .line 220
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget v1, p0, Lav/e;->j:F

    invoke-static {p1, v1}, Lav/e;->a(Lau/H;F)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 221
    invoke-direct {p0}, Lav/e;->h()I
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    move-result v0

    monitor-exit p0

    return v0

    .line 220
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(F)V
    .registers 3
    .parameter

    .prologue
    .line 555
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/dK;->c(F)V

    .line 556
    return-void
.end method

.method protected declared-synchronized b(Lau/B;Lau/Y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 316
    monitor-enter p0

    const/4 v0, 0x0

    .line 317
    :try_start_2
    invoke-direct {p0, p1, p2, v0}, Lav/e;->a(Lau/B;Lau/Y;Z)V
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_7

    .line 318
    monitor-exit p0

    return-void

    .line 316
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lau/B;
    .registers 2

    .prologue
    .line 256
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-direct {p0, v0}, Lav/e;->a(Lcom/google/android/maps/driveabout/vector/l;)Lau/B;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized c(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 181
    monitor-enter p0

    :try_start_1
    invoke-super {p0, p1, p2}, Lau/u;->c(II)V

    .line 182
    iget-object v0, p0, Lav/e;->e:Lcom/google/android/maps/driveabout/vector/k;

    iget v1, p0, Lav/e;->j:F

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(IIF)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 183
    monitor-exit p0

    return-void

    .line 181
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized c(Lau/B;Lau/Y;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 506
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v4

    .line 508
    iget-object v0, p0, Lav/e;->g:Lo/Q;

    invoke-static {p1, v0}, Lu/e;->a(Lau/B;Lo/Q;)V

    .line 509
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    iget-object v1, p0, Lav/e;->g:Lo/Q;

    iget v2, p0, Lav/e;->j:F

    invoke-static {p2, v2}, Lav/e;->a(Lau/Y;F)F

    move-result v2

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 515
    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;I)V
    :try_end_28
    .catchall {:try_start_1 .. :try_end_28} :catchall_2a

    .line 516
    monitor-exit p0

    return-void

    .line 506
    :catchall_2a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Lau/Y;
    .registers 2

    .prologue
    .line 266
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-direct {p0, v0}, Lav/e;->b(Lcom/google/android/maps/driveabout/vector/l;)Lau/Y;

    move-result-object v0

    return-object v0
.end method

.method public e()F
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v0

    return v0
.end method

.method public e(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 622
    .line 623
    const/4 v2, 0x0

    move-object v0, p0

    move v3, v1

    move v4, p1

    move v5, p2

    .line 625
    invoke-virtual/range {v0 .. v5}, Lav/e;->a(ZZZII)V

    .line 626
    return-void
.end method

.method public f()Lau/H;
    .registers 7

    .prologue
    .line 302
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v2

    .line 303
    iget-object v0, p0, Lav/e;->d:Lav/a;

    invoke-virtual {v0}, Lav/a;->b()Lau/H;

    move-result-object v5

    .line 304
    new-instance v0, Lau/H;

    invoke-direct {p0, v2}, Lav/e;->a(Lcom/google/android/maps/driveabout/vector/l;)Lau/B;

    move-result-object v1

    invoke-direct {p0, v2}, Lav/e;->b(Lcom/google/android/maps/driveabout/vector/l;)Lau/Y;

    move-result-object v2

    invoke-virtual {v5}, Lau/H;->c()I

    move-result v3

    invoke-virtual {v5}, Lau/H;->g()Z

    move-result v4

    invoke-virtual {v5}, Lau/H;->h()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lau/H;-><init>(Lau/B;Lau/Y;IZZ)V

    return-object v0
.end method

.method public f(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 633
    const/4 v1, 0x0

    move-object v0, p0

    move v3, v2

    move v4, p1

    move v5, p2

    .line 636
    invoke-virtual/range {v0 .. v5}, Lav/e;->a(ZZZII)V

    .line 637
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 486
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->h()V

    .line 487
    return-void
.end method

.method public j()Z
    .registers 4

    .prologue
    .line 536
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    .line 537
    sget-object v1, Lav/j;->b:Lav/j;

    invoke-virtual {p0, v0, v1}, Lav/e;->a(FLav/j;)Lau/Y;

    move-result-object v1

    .line 538
    invoke-virtual {v1}, Lau/Y;->c()Lau/Y;

    move-result-object v2

    if-eqz v2, :cond_2c

    invoke-virtual {v1}, Lau/Y;->a()I

    move-result v1

    invoke-virtual {p0}, Lav/e;->c()Lau/B;

    move-result-object v2

    invoke-virtual {p0, v2}, Lav/e;->a(Lau/B;)I

    move-result v2

    if-ge v1, v2, :cond_2c

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/dK;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2e

    :cond_2c
    const/4 v0, 0x1

    :goto_2d
    return v0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_2d
.end method

.method public k()Z
    .registers 3

    .prologue
    .line 545
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    .line 546
    sget-object v1, Lav/j;->c:Lav/j;

    invoke-virtual {p0, v0, v1}, Lav/e;->a(FLav/j;)Lau/Y;

    move-result-object v1

    invoke-virtual {v1}, Lau/Y;->d()Lau/Y;

    move-result-object v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/dK;->e()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_22

    :cond_20
    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 564
    iget-object v0, p0, Lav/e;->c:Lcom/google/android/maps/driveabout/vector/dK;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/dK;->i()Z

    move-result v0

    return v0
.end method

.method public m()V
    .registers 1

    .prologue
    .line 156
    invoke-super {p0}, Lau/u;->m()V

    .line 157
    return-void
.end method
