.class public final enum Lav/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lav/j;

.field public static final enum b:Lav/j;

.field public static final enum c:Lav/j;

.field private static final synthetic d:[Lav/j;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lav/j;

    const-string v1, "ROUND"

    invoke-direct {v0, v1, v2}, Lav/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lav/j;->a:Lav/j;

    new-instance v0, Lav/j;

    const-string v1, "FLOOR"

    invoke-direct {v0, v1, v3}, Lav/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lav/j;->b:Lav/j;

    new-instance v0, Lav/j;

    const-string v1, "CEILING"

    invoke-direct {v0, v1, v4}, Lav/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lav/j;->c:Lav/j;

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [Lav/j;

    sget-object v1, Lav/j;->a:Lav/j;

    aput-object v1, v0, v2

    sget-object v1, Lav/j;->b:Lav/j;

    aput-object v1, v0, v3

    sget-object v1, Lav/j;->c:Lav/j;

    aput-object v1, v0, v4

    sput-object v0, Lav/j;->d:[Lav/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lav/j;
    .registers 2
    .parameter

    .prologue
    .line 50
    const-class v0, Lav/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lav/j;

    return-object v0
.end method

.method public static values()[Lav/j;
    .registers 1

    .prologue
    .line 50
    sget-object v0, Lav/j;->d:[Lav/j;

    invoke-virtual {v0}, [Lav/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lav/j;

    return-object v0
.end method
