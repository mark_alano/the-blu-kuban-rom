.class public LaW/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/ab;
.implements LaR/o;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/ViewGroup;

.field private c:Ljava/util/Set;

.field private d:LaR/n;

.field private e:LaR/n;

.field private f:Ljava/util/List;

.field private final g:Lcom/google/googlenav/ui/wizard/gj;

.field private h:Lcom/google/googlenav/aV;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/aV;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LaW/A;->c:Ljava/util/Set;

    .line 150
    iput-object p1, p0, LaW/A;->g:Lcom/google/googlenav/ui/wizard/gj;

    .line 151
    iput-object p2, p0, LaW/A;->h:Lcom/google/googlenav/aV;

    .line 152
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaW/A;->f:Ljava/util/List;

    .line 153
    return-void
.end method

.method static synthetic a(LaW/A;)LaR/n;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaW/A;->e:LaR/n;

    return-object v0
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 263
    iget-object v1, p0, LaW/A;->f:Ljava/util/List;

    monitor-enter v1

    .line 264
    :try_start_3
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1f

    .line 265
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    .line 266
    iget-object v2, p0, LaW/A;->h:Lcom/google/googlenav/aV;

    iget-object v0, v0, LaW/D;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/aV;->a(Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 269
    :cond_1f
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_28

    .line 270
    invoke-direct {p0}, LaW/A;->f()V

    .line 271
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    .line 272
    return-void

    .line 269
    :catchall_28
    move-exception v0

    :try_start_29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    throw v0
.end method

.method static synthetic a(LaW/A;Ljava/util/Set;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, LaW/A;->a(Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/util/Set;)V
    .registers 7
    .parameter

    .prologue
    .line 412
    iget-object v1, p0, LaW/A;->c:Ljava/util/Set;

    monitor-enter v1

    .line 413
    :try_start_3
    iget-object v0, p0, LaW/A;->d:LaR/n;

    invoke-interface {v0, p0}, LaR/n;->a(LaR/o;)V

    .line 414
    iget-object v0, p0, LaW/A;->e:LaR/n;

    invoke-interface {v0, p0}, LaR/n;->a(LaR/o;)V

    .line 415
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    .line 416
    iget-object v3, p0, LaW/A;->c:Ljava/util/Set;

    iget-object v4, v0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 417
    iget-boolean v3, v0, LaW/D;->b:Z

    if-eqz v3, :cond_33

    .line 418
    iget-object v3, p0, LaW/A;->d:LaR/n;

    iget-object v0, v0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, LaR/n;->a(Ljava/lang/String;)V

    goto :goto_11

    .line 423
    :catchall_30
    move-exception v0

    monitor-exit v1
    :try_end_32
    .catchall {:try_start_3 .. :try_end_32} :catchall_30

    throw v0

    .line 420
    :cond_33
    :try_start_33
    iget-object v3, p0, LaW/A;->e:LaR/n;

    iget-object v0, v0, LaW/D;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, LaR/n;->a(Ljava/lang/String;)V

    goto :goto_11

    .line 423
    :cond_3b
    monitor-exit v1
    :try_end_3c
    .catchall {:try_start_33 .. :try_end_3c} :catchall_30

    .line 424
    return-void
.end method

.method private a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 182
    new-instance v0, LaW/E;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaW/E;-><init>(LaW/A;LaW/B;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LaW/E;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 183
    return-void
.end method

.method static synthetic b(LaW/A;)LaR/n;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaW/A;->d:LaR/n;

    return-object v0
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 275
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    iget-object v0, v0, LaW/D;->d:Lcom/google/googlenav/ai;

    .line 276
    iget-object v1, p0, LaW/A;->g:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/wizard/gj;->a(Lcom/google/googlenav/ai;)V

    .line 277
    return-void
.end method

.method static synthetic c(LaW/A;)Lcom/google/googlenav/aV;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaW/A;->h:Lcom/google/googlenav/aV;

    return-object v0
.end method

.method static synthetic d(LaW/A;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(LaW/A;)V
    .registers 1
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, LaW/A;->f()V

    return-void
.end method

.method private f()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 190
    iget-object v0, p0, LaW/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 192
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    move v1, v2

    .line 193
    :goto_d
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_29

    const/4 v0, 0x3

    if-ge v1, v0, :cond_29

    .line 194
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    .line 195
    invoke-virtual {p0, v0}, LaW/A;->a(LaW/D;)Landroid/view/View;

    move-result-object v0

    .line 196
    iget-object v4, p0, LaW/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 193
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 200
    :cond_29
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_46

    .line 201
    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 208
    :goto_34
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_45

    .line 209
    new-instance v0, LaW/C;

    invoke-direct {v0, p0}, LaW/C;-><init>(LaW/A;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaW/C;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 211
    :cond_45
    return-void

    .line 203
    :cond_46
    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_34
.end method


# virtual methods
.method public F_()V
    .registers 3

    .prologue
    .line 444
    new-instance v0, LaW/C;

    invoke-direct {v0, p0}, LaW/C;-><init>(LaW/A;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaW/C;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 445
    return-void
.end method

.method public a(LaW/D;)Landroid/view/View;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 219
    invoke-virtual {p0}, LaW/A;->c()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v4}, Lcom/google/googlenav/ui/bi;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 221
    const v0, 0x7f1000fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 222
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    const v0, 0x7f100020

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 225
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    const v0, 0x7f100198

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 228
    iget-object v2, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    const v0, 0x7f100044

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 230
    iget-object v2, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    const v0, 0x7f100334

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 233
    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    invoke-virtual {v2}, LaR/D;->o()Z

    move-result v2

    if-eqz v2, :cond_7e

    .line 234
    const/16 v2, 0x3af

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    :goto_60
    const v0, 0x7f100333

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    iget-object v2, p1, LaW/D;->d:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bo()Ljava/lang/String;

    move-result-object v2

    .line 242
    invoke-static {v2}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;)I

    move-result v3

    .line 244
    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v2

    .line 245
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 246
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    return-object v1

    .line 237
    :cond_7e
    const/16 v2, 0x3b0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_60
.end method

.method public a()V
    .registers 2

    .prologue
    .line 156
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->b(LaR/ab;)V

    .line 157
    return-void
.end method

.method public a(LaR/P;)V
    .registers 2
    .parameter

    .prologue
    .line 440
    return-void
.end method

.method public a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 167
    const v0, 0x7f100324

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/A;->a:Landroid/view/View;

    .line 168
    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    const v1, 0x7f100332

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaW/A;->b:Landroid/view/ViewGroup;

    .line 171
    iget-object v0, p0, LaW/A;->a:Landroid/view/View;

    const v1, 0x7f100331

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 172
    const/16 v1, 0x3b1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    iput-object v0, p0, LaW/A;->d:LaR/n;

    .line 175
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->h()LaR/n;

    move-result-object v0

    iput-object v0, p0, LaW/A;->e:LaR/n;

    .line 177
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->a(LaR/ab;)V

    .line 178
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    .line 179
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    new-instance v1, LaW/D;

    invoke-direct {v1, p0, p1}, LaW/D;-><init>(LaW/A;Lcom/google/googlenav/ai;)V

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 281
    invoke-direct {p0, v0}, LaW/A;->a(I)V

    .line 282
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 449
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 428
    iget-object v1, p0, LaW/A;->c:Ljava/util/Set;

    monitor-enter v1

    .line 429
    :try_start_4
    iget-object v0, p0, LaW/A;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 430
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_2a

    .line 431
    iget-object v0, p0, LaW/A;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_29

    iget-object v0, p0, LaW/A;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_29

    .line 432
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    .line 433
    iget-object v0, p0, LaW/A;->d:LaR/n;

    invoke-interface {v0, v2}, LaR/n;->a(LaR/o;)V

    .line 434
    iget-object v0, p0, LaW/A;->e:LaR/n;

    invoke-interface {v0, v2}, LaR/n;->a(LaR/o;)V

    .line 436
    :cond_29
    return-void

    .line 430
    :catchall_2a
    move-exception v0

    :try_start_2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    throw v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 186
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaW/A;->a(Z)V

    .line 187
    return-void
.end method

.method protected c()I
    .registers 2

    .prologue
    .line 214
    const v0, 0x7f04012b

    return v0
.end method

.method public e()Ljava/util/List;
    .registers 7

    .prologue
    .line 350
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 351
    iget-object v0, p0, LaW/A;->e:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 352
    new-instance v3, LaW/D;

    invoke-virtual {v0}, LaR/t;->h()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5, v0}, LaW/D;-><init>(LaW/A;Ljava/lang/String;ZLaR/t;)V

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 354
    :cond_2c
    iget-object v0, p0, LaW/A;->d:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_54

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 355
    new-instance v3, LaW/D;

    invoke-virtual {v0}, LaR/t;->h()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v3, p0, v4, v5, v0}, LaW/D;-><init>(LaW/A;Ljava/lang/String;ZLaR/t;)V

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3a

    .line 358
    :cond_54
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 359
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 360
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 362
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 253
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 254
    iget-object v1, p0, LaW/A;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f1000fb

    if-ne v1, v2, :cond_19

    .line 256
    invoke-direct {p0, v0}, LaW/A;->a(I)V

    .line 260
    :goto_18
    return-void

    .line 258
    :cond_19
    invoke-direct {p0, v0}, LaW/A;->b(I)V

    goto :goto_18
.end method
