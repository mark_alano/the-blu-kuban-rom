.class public LaW/C;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:LaW/A;

.field private b:Ljava/util/Set;


# direct methods
.method public constructor <init>(LaW/A;)V
    .registers 2
    .parameter

    .prologue
    .line 371
    iput-object p1, p0, LaW/C;->a:LaW/A;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private a(LaW/D;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 397
    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    if-nez v2, :cond_9

    .line 407
    :cond_8
    :goto_8
    return v0

    .line 399
    :cond_9
    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    invoke-virtual {v2}, LaR/D;->l()Z

    move-result v2

    if-nez v2, :cond_1d

    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v2

    invoke-virtual {v2}, LaR/D;->m()Z

    move-result v2

    if-eqz v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 401
    goto :goto_8

    .line 404
    :cond_1f
    invoke-virtual {p1}, LaW/D;->b()LaR/H;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p1}, LaW/D;->b()LaR/H;

    move-result-object v2

    invoke-virtual {v2}, LaR/H;->c()Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 407
    goto :goto_8
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .registers 6
    .parameter

    .prologue
    .line 377
    iget-object v0, p0, LaW/C;->a:LaW/A;

    invoke-virtual {v0}, LaW/A;->e()Ljava/util/List;

    move-result-object v0

    .line 378
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, LaW/C;->b:Ljava/util/Set;

    .line 379
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_10
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    .line 380
    iget-object v2, p0, LaW/C;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_36

    .line 387
    :cond_25
    iget-object v0, p0, LaW/C;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_34

    .line 388
    iget-object v0, p0, LaW/C;->a:LaW/A;

    iget-object v1, p0, LaW/C;->b:Ljava/util/Set;

    invoke-static {v0, v1}, LaW/A;->a(LaW/A;Ljava/util/Set;)V

    .line 390
    :cond_34
    const/4 v0, 0x0

    return-object v0

    .line 383
    :cond_36
    invoke-direct {p0, v0}, LaW/C;->a(LaW/D;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 384
    iget-object v2, p0, LaW/C;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_10
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 370
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaW/C;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
