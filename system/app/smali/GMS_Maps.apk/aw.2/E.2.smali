.class LaW/E;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:LaW/A;

.field private b:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(LaW/A;)V
    .registers 2
    .parameter

    .prologue
    .line 288
    iput-object p1, p0, LaW/E;->a:LaW/A;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LaW/A;LaW/B;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 288
    invoke-direct {p0, p1}, LaW/E;-><init>(LaW/A;)V

    return-void
.end method

.method private a(LaW/D;)Lcom/google/googlenav/ai;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 316
    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v1

    if-eqz v1, :cond_29

    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v1

    invoke-virtual {v1}, LaR/D;->l()Z

    move-result v1

    if-nez v1, :cond_29

    iget-object v1, p0, LaW/E;->a:LaW/A;

    invoke-static {v1}, LaW/A;->c(LaW/A;)Lcom/google/googlenav/aV;

    move-result-object v1

    iget-object v2, p1, LaW/D;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/aV;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_29

    invoke-virtual {p1}, LaW/D;->a()LaR/D;

    move-result-object v1

    invoke-virtual {v1}, LaR/D;->m()Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 333
    :cond_29
    :goto_29
    return-object v0

    .line 321
    :cond_2a
    invoke-virtual {p1}, LaW/D;->b()LaR/H;

    move-result-object v1

    if-eqz v1, :cond_29

    .line 325
    invoke-virtual {p1}, LaW/D;->b()LaR/H;

    move-result-object v1

    invoke-virtual {v1}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v1

    .line 327
    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/aC;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_29

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cu;->b()Z

    move-result v2

    if-nez v2, :cond_29

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_29

    move-object v0, v1

    .line 333
    goto :goto_29
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Boolean;)Ljava/util/List;
    .registers 7
    .parameter

    .prologue
    .line 294
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, LaW/E;->b:Ljava/lang/Boolean;

    .line 296
    iget-object v0, p0, LaW/E;->a:LaW/A;

    invoke-virtual {v0}, LaW/A;->e()Ljava/util/List;

    move-result-object v0

    .line 297
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 298
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaW/D;

    .line 299
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_27

    .line 308
    :cond_26
    return-object v1

    .line 302
    :cond_27
    invoke-direct {p0, v0}, LaW/E;->a(LaW/D;)Lcom/google/googlenav/ai;

    move-result-object v3

    .line 303
    if-eqz v3, :cond_13

    .line 304
    iput-object v3, v0, LaW/D;->d:Lcom/google/googlenav/ai;

    .line 305
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13
.end method

.method protected a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 338
    iget-object v0, p0, LaW/E;->a:LaW/A;

    invoke-static {v0}, LaW/A;->d(LaW/A;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 339
    :try_start_7
    iget-object v0, p0, LaW/E;->a:LaW/A;

    invoke-static {v0}, LaW/A;->d(LaW/A;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 340
    iget-object v0, p0, LaW/E;->a:LaW/A;

    invoke-static {v0}, LaW/A;->d(LaW/A;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 341
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_7 .. :try_end_1a} :catchall_28

    .line 342
    iget-object v0, p0, LaW/E;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 343
    iget-object v0, p0, LaW/E;->a:LaW/A;

    invoke-static {v0}, LaW/A;->e(LaW/A;)V

    .line 345
    :cond_27
    return-void

    .line 341
    :catchall_28
    move-exception v0

    :try_start_29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 288
    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LaW/E;->a([Ljava/lang/Boolean;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 288
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, LaW/E;->a(Ljava/util/List;)V

    return-void
.end method
