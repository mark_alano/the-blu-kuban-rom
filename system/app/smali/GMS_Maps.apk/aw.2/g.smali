.class public LaW/g;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaW/v;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Ljava/lang/String;

.field private final d:LaW/a;

.field private final l:LaW/A;

.field private final m:LaW/x;

.field private n:LaW/y;

.field private o:LaW/s;

.field private final p:LaW/O;

.field private q:Lcom/google/googlenav/ui/wizard/gj;

.field private r:Ljava/lang/String;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private u:LaW/e;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZZLcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/bu;Lcom/google/googlenav/aV;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 231
    const v0, 0x7f0f001a

    invoke-direct {p0, p9, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 232
    iput-boolean p7, p0, LaW/g;->a:Z

    .line 233
    iput-boolean p8, p0, LaW/g;->b:Z

    .line 234
    iput-object p3, p0, LaW/g;->c:Ljava/lang/String;

    .line 235
    iput-object p2, p0, LaW/g;->r:Ljava/lang/String;

    .line 236
    iput-object p10, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    .line 238
    new-instance v0, LaW/a;

    invoke-direct {v0, p1}, LaW/a;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LaW/g;->d:LaW/a;

    .line 239
    new-instance v0, LaW/x;

    invoke-direct {p0, p11}, LaW/g;->c(Lcom/google/googlenav/bu;)LaW/w;

    move-result-object v1

    invoke-direct {v0, p6, v1}, LaW/x;-><init>(Ljava/util/List;LaW/w;)V

    iput-object v0, p0, LaW/g;->m:LaW/x;

    .line 242
    new-instance v0, LaW/A;

    invoke-direct {v0, p10, p12}, LaW/A;-><init>(Lcom/google/googlenav/ui/wizard/gj;Lcom/google/googlenav/aV;)V

    iput-object v0, p0, LaW/g;->l:LaW/A;

    .line 246
    new-instance v0, LaW/y;

    invoke-direct {v0, p4, p5, p10}, LaW/y;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/gj;)V

    iput-object v0, p0, LaW/g;->n:LaW/y;

    .line 247
    new-instance v0, LaW/s;

    invoke-direct {v0, p11, p0}, LaW/s;-><init>(Lcom/google/googlenav/bu;LaW/v;)V

    iput-object v0, p0, LaW/g;->o:LaW/s;

    .line 248
    new-instance v0, LaW/O;

    invoke-direct {v0, p7, p3, p10}, LaW/O;-><init>(ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/gj;)V

    iput-object v0, p0, LaW/g;->p:LaW/O;

    .line 250
    new-instance v0, LaW/e;

    invoke-direct {v0, p10}, LaW/e;-><init>(Lcom/google/googlenav/ui/wizard/gj;)V

    iput-object v0, p0, LaW/g;->u:LaW/e;

    .line 251
    return-void
.end method

.method static synthetic a(LaW/g;)Lcom/google/googlenav/ui/wizard/gj;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 282
    const v0, 0x7f100039

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 283
    const/16 v1, 0x38c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    const v0, 0x7f10031d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 288
    new-instance v1, LaW/h;

    invoke-direct {v1, p0}, LaW/h;-><init>(LaW/g;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    const v0, 0x7f1001af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 297
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 298
    const v0, 0x7f1001b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f02021a

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 300
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v2, LaW/i;

    invoke-direct {v2, p0}, LaW/i;-><init>(LaW/g;)V

    invoke-virtual {v0, v1, v2}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    .line 314
    :goto_4f
    return-void

    .line 310
    :cond_50
    const v0, 0x7f1001b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 311
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 312
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4f
.end method

.method static synthetic b(LaW/g;)Lcom/google/googlenav/ui/e;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, LaW/g;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/bu;)LaW/w;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 188
    if-nez p1, :cond_4

    .line 219
    :cond_3
    :goto_3
    return-object v0

    .line 193
    :cond_4
    iget-boolean v1, p1, Lcom/google/googlenav/bu;->j:Z

    if-nez v1, :cond_3

    .line 199
    invoke-virtual {p1}, Lcom/google/googlenav/bu;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_20

    .line 201
    new-instance v0, LaW/k;

    const v1, 0x7f020360

    const/16 v2, 0x3a1

    const/16 v3, 0x3a0

    const/16 v4, 0x39f

    const v5, 0x7f020483

    invoke-direct/range {v0 .. v5}, LaW/k;-><init>(IIIII)V

    goto :goto_3

    .line 208
    :cond_20
    invoke-virtual {p1}, Lcom/google/googlenav/bu;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 210
    new-instance v0, LaW/k;

    const v1, 0x7f02035f

    const/16 v2, 0x39e

    const/16 v3, 0x39d

    const/16 v4, 0x39c

    const v5, 0x7f020476

    invoke-direct/range {v0 .. v5}, LaW/k;-><init>(IIIII)V

    goto :goto_3
.end method

.method static synthetic c(LaW/g;)Lcom/google/googlenav/ui/e;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, LaW/g;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private l()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 374
    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0}, LaW/y;->a()Ljava/util/List;

    move-result-object v0

    .line 375
    iget-object v1, p0, LaW/g;->m:LaW/x;

    invoke-virtual {v1}, LaW/x;->a()Ljava/util/List;

    move-result-object v1

    .line 378
    if-eqz v0, :cond_13

    if-nez v1, :cond_1e

    .line 379
    :cond_13
    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 380
    iget-object v0, p0, LaW/g;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 390
    :goto_1d
    return-void

    .line 381
    :cond_1e
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_35

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_35

    .line 383
    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 384
    iget-object v0, p0, LaW/g;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1d

    .line 387
    :cond_35
    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 388
    iget-object v0, p0, LaW/g;->s:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1d
.end method


# virtual methods
.method protected O_()V
    .registers 2

    .prologue
    .line 438
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_e

    .line 439
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaW/g;->requestWindowFeature(I)Z

    .line 441
    :cond_e
    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 4
    .parameter

    .prologue
    .line 395
    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->ab_()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 396
    const v0, 0x7f020319

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    .line 397
    const/16 v0, 0x38c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 398
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v1, LaW/j;

    invoke-direct {v1, p0}, LaW/j;-><init>(LaW/g;)V

    invoke-virtual {v0, p1, v1}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    .line 407
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 3
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0, p1}, LaW/A;->a(Lcom/google/googlenav/ai;)V

    .line 357
    return-void
.end method

.method public a(Lcom/google/googlenav/bu;)V
    .registers 4
    .parameter

    .prologue
    .line 370
    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-direct {p0, p1}, LaW/g;->c(Lcom/google/googlenav/bu;)LaW/w;

    move-result-object v1

    invoke-virtual {v0, v1}, LaW/x;->a(LaW/w;)V

    .line 371
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 328
    iput-object p1, p0, LaW/g;->c:Ljava/lang/String;

    .line 329
    iget-object v0, p0, LaW/g;->p:LaW/O;

    if-eqz v0, :cond_f

    .line 330
    iget-object v0, p0, LaW/g;->p:LaW/O;

    iget-boolean v1, p0, LaW/g;->a:Z

    iget-object v2, p0, LaW/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LaW/O;->a(ZLjava/lang/String;)V

    .line 334
    :cond_f
    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0, p2, p3}, LaW/y;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 336
    if-eqz p2, :cond_19

    .line 339
    invoke-direct {p0}, LaW/g;->l()V

    .line 341
    :cond_19
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 413
    iput-boolean p2, p0, LaW/g;->a:Z

    .line 415
    iget-object v0, p0, LaW/g;->u:LaW/e;

    if-eqz v0, :cond_d

    .line 416
    iget-object v0, p0, LaW/g;->u:LaW/e;

    iget-boolean v1, p0, LaW/g;->b:Z

    invoke-virtual {v0, p1, p2, v1}, LaW/e;->a(Ljava/lang/String;ZZ)V

    .line 419
    :cond_d
    iget-object v0, p0, LaW/g;->p:LaW/O;

    if-eqz v0, :cond_18

    .line 421
    iget-object v0, p0, LaW/g;->p:LaW/O;

    iget-object v1, p0, LaW/g;->c:Ljava/lang/String;

    invoke-virtual {v0, p2, v1}, LaW/O;->a(ZLjava/lang/String;)V

    .line 423
    :cond_18
    return-void
.end method

.method public a(Ljava/lang/String;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 268
    iput-boolean p2, p0, LaW/g;->b:Z

    .line 269
    iput-object p1, p0, LaW/g;->r:Ljava/lang/String;

    .line 270
    iput-boolean p3, p0, LaW/g;->a:Z

    .line 271
    iput-object v1, p0, LaW/g;->c:Ljava/lang/String;

    .line 272
    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0, v1, v1}, LaW/y;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-virtual {v0, v1, v1}, LaW/x;->a(Ljava/util/List;LaW/w;)V

    .line 275
    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0}, LaW/A;->b()V

    .line 277
    iget-object v0, p0, LaW/g;->r:Ljava/lang/String;

    invoke-virtual {p0, v0, p3}, LaW/g;->a(Ljava/lang/String;Z)V

    .line 278
    invoke-direct {p0}, LaW/g;->l()V

    .line 279
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 317
    iget-object v0, p0, LaW/g;->d:LaW/a;

    if-eqz v0, :cond_b

    .line 318
    iget-object v0, p0, LaW/g;->d:LaW/a;

    iget-object v1, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-virtual {v0, v1, p1}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/gj;Ljava/util/List;)V

    .line 321
    :cond_b
    return-void
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/bu;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 345
    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-direct {p0, p2}, LaW/g;->c(Lcom/google/googlenav/bu;)LaW/w;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LaW/x;->a(Ljava/util/List;LaW/w;)V

    .line 348
    if-eqz p1, :cond_e

    .line 349
    invoke-direct {p0}, LaW/g;->l()V

    .line 351
    :cond_e
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 473
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004b6

    if-ne v0, v1, :cond_1c

    .line 474
    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->e()Lcom/google/googlenav/android/d;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->Z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/d;->a(Ljava/lang/String;)V

    .line 488
    :cond_1a
    :goto_1a
    const/4 v0, 0x1

    return v0

    .line 476
    :cond_1c
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004c0

    if-ne v0, v1, :cond_5c

    .line 477
    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->e()Lcom/google/googlenav/android/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/d;->h()V

    .line 481
    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 483
    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->k()I

    goto :goto_1a

    .line 484
    :cond_5c
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004cf

    if-ne v0, v1, :cond_1a

    .line 485
    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->U_()V

    goto :goto_1a
.end method

.method public b(Lcom/google/googlenav/bu;)V
    .registers 3
    .parameter

    .prologue
    .line 429
    iget-object v0, p0, LaW/g;->o:LaW/s;

    invoke-virtual {v0, p1}, LaW/s;->a(Lcom/google/googlenav/bu;)V

    .line 430
    return-void
.end method

.method protected c()Landroid/view/View;
    .registers 4

    .prologue
    .line 135
    invoke-virtual {p0}, LaW/g;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04012d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 138
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_21

    .line 139
    const v0, 0x7f10031c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 142
    :cond_21
    const v0, 0x7f10033b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/g;->s:Landroid/view/View;

    .line 143
    const v0, 0x7f10033a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaW/g;->t:Landroid/view/View;

    .line 144
    iget-object v0, p0, LaW/g;->t:Landroid/view/View;

    const v2, 0x7f100181

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x3a3

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    invoke-direct {p0, v1}, LaW/g;->a(Landroid/view/View;)V

    .line 149
    iget-object v0, p0, LaW/g;->u:LaW/e;

    const v2, 0x7f100318

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, LaW/e;->a(Landroid/view/View;)V

    .line 150
    iget-object v0, p0, LaW/g;->r:Ljava/lang/String;

    iget-boolean v2, p0, LaW/g;->a:Z

    invoke-virtual {p0, v0, v2}, LaW/g;->a(Ljava/lang/String;Z)V

    .line 151
    iget-object v0, p0, LaW/g;->d:LaW/a;

    iget-object v2, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-virtual {v0, v2, v1}, LaW/a;->a(Lcom/google/googlenav/ui/wizard/gj;Landroid/view/View;)V

    .line 152
    iget-object v0, p0, LaW/g;->m:LaW/x;

    invoke-virtual {v0, v1}, LaW/x;->a(Landroid/view/View;)V

    .line 154
    const/16 v0, 0x57

    const-string v2, "rps"

    invoke-static {v0, v2}, Lbm/m;->a(ILjava/lang/String;)V

    .line 157
    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0, v1}, LaW/A;->a(Landroid/view/View;)V

    .line 159
    iget-object v0, p0, LaW/g;->n:LaW/y;

    invoke-virtual {v0, v1}, LaW/y;->a(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, LaW/g;->o:LaW/s;

    invoke-virtual {v0, v1}, LaW/s;->a(Landroid/view/View;)V

    .line 161
    iget-object v0, p0, LaW/g;->p:LaW/O;

    if-eqz v0, :cond_88

    .line 162
    iget-object v0, p0, LaW/g;->p:LaW/O;

    invoke-virtual {v0, v1}, LaW/O;->a(Landroid/view/View;)V

    .line 164
    :cond_88
    return-object v1
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method protected f()V
    .registers 2

    .prologue
    .line 256
    iget-object v0, p0, LaW/g;->l:LaW/A;

    invoke-virtual {v0}, LaW/A;->a()V

    .line 258
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 504
    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->ad_()V

    .line 505
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 512
    iget-object v0, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gj;->ac_()V

    .line 513
    return-void
.end method

.method protected i()Z
    .registers 2

    .prologue
    .line 517
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .parameter

    .prologue
    .line 445
    sget-object v0, LaW/g;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11001e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 446
    const v0, 0x7f1004cf

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 447
    const/16 v1, 0x21

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 448
    const v0, 0x7f1004c0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 449
    if-eqz v0, :cond_2e

    .line 451
    const/16 v1, 0x524

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 453
    :cond_2e
    const v0, 0x7f1004b6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_40

    .line 456
    const/16 v1, 0x1d8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 459
    :cond_40
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 461
    const v0, 0x7f10049b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 462
    const/16 v0, 0x4f5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 463
    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 464
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    iget-object v3, p0, LaW/g;->q:Lcom/google/googlenav/ui/wizard/gj;

    invoke-interface {v3}, Lcom/google/googlenav/ui/wizard/gj;->w()Lcom/google/googlenav/actionbar/b;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    .line 467
    :cond_6d
    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 169
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0x38c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_10
    return-object v0

    :cond_11
    const-string v0, ""

    goto :goto_10
.end method
