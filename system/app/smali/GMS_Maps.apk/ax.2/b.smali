.class public abstract Lax/b;
.super Law/a;
.source "SourceFile"

# interfaces
.implements Lax/k;
.implements Lcom/google/googlenav/F;


# static fields
.field public static final d:LaN/Y;

.field private static final m:Ljava/lang/Object;

.field private static n:I

.field private static final q:Lbm/i;

.field private static final r:Lbm/i;


# instance fields
.field private A:[Lax/y;

.field private B:Lax/y;

.field private C:Lax/y;

.field private D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private E:I

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Z

.field private I:Z

.field private J:I

.field private K:Z

.field private L:I

.field private M:I

.field private N:B

.field private O:I

.field private P:I

.field private Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private R:Z

.field private S:Z

.field private T:I

.field private U:Z

.field private V:Ljava/lang/String;

.field private W:I

.field private X:Lax/e;

.field protected a:Ljava/lang/String;

.field protected b:I

.field protected c:I

.field e:Lcom/google/googlenav/ui/m;

.field protected f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field protected g:I

.field protected h:[Lax/h;

.field i:[Lax/d;

.field j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field protected k:I

.field protected l:[I

.field private final o:I

.field private p:I

.field private s:Z

.field private t:Lax/l;

.field private u:Lax/y;

.field private v:Z

.field private w:[Lax/y;

.field private x:Lax/y;

.field private y:Z

.field private z:[Lax/y;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/16 v3, 0x16

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lax/b;->m:Ljava/lang/Object;

    .line 87
    const/4 v0, 0x1

    sput v0, Lax/b;->n:I

    .line 104
    new-instance v0, Lbm/i;

    const-string v1, "directions"

    const-string v2, "r"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lax/b;->q:Lbm/i;

    .line 110
    new-instance v0, Lbm/i;

    const-string v1, "directions time update"

    const-string v2, "T"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lax/b;->r:Lbm/i;

    .line 149
    const/16 v0, 0xf

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    sput-object v0, Lax/b;->d:LaN/Y;

    return-void
.end method

.method protected constructor <init>(Lax/k;Lcom/google/googlenav/ui/m;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 388
    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v1

    invoke-interface {p1}, Lax/k;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p2}, Lax/b;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V

    .line 390
    return-void
.end method

.method protected constructor <init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 372
    invoke-direct {p0, p4}, Lax/b;-><init>(Lcom/google/googlenav/ui/m;)V

    .line 373
    iput-object p1, p0, Lax/b;->u:Lax/y;

    .line 374
    iput-object p2, p0, Lax/b;->x:Lax/y;

    .line 375
    iput-object p3, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 376
    const/4 v0, 0x0

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    .line 377
    const/4 v0, -0x1

    iput v0, p0, Lax/b;->k:I

    .line 378
    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/b;->S:Z

    .line 381
    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 382
    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 383
    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/m;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 360
    invoke-direct {p0}, Law/a;-><init>()V

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lax/b;->a:Ljava/lang/String;

    .line 95
    iput v3, p0, Lax/b;->b:I

    .line 98
    iput v1, p0, Lax/b;->c:I

    .line 101
    iput v3, p0, Lax/b;->p:I

    .line 158
    iput-boolean v1, p0, Lax/b;->s:Z

    .line 163
    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v0

    iput-object v0, p0, Lax/b;->t:Lax/l;

    .line 173
    new-array v0, v1, [Lax/y;

    iput-object v0, p0, Lax/b;->A:[Lax/y;

    .line 180
    iput-object v2, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 183
    iput-object v2, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 188
    iput v1, p0, Lax/b;->E:I

    .line 194
    iput-object v2, p0, Lax/b;->G:Ljava/lang/String;

    .line 202
    iput-boolean v1, p0, Lax/b;->H:Z

    .line 213
    new-array v0, v1, [Lax/d;

    iput-object v0, p0, Lax/b;->i:[Lax/d;

    .line 215
    new-array v0, v1, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 219
    iput-boolean v4, p0, Lax/b;->I:Z

    .line 222
    iput v1, p0, Lax/b;->J:I

    .line 225
    iput-boolean v1, p0, Lax/b;->K:Z

    .line 231
    iput v1, p0, Lax/b;->L:I

    .line 254
    iput v1, p0, Lax/b;->P:I

    .line 259
    new-array v0, v1, [I

    iput-object v0, p0, Lax/b;->l:[I

    .line 266
    new-array v0, v1, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 283
    const/16 v0, 0xf

    iput v0, p0, Lax/b;->T:I

    .line 289
    iput-boolean v4, p0, Lax/b;->U:Z

    .line 295
    iput-object v2, p0, Lax/b;->V:Ljava/lang/String;

    .line 303
    iput v3, p0, Lax/b;->W:I

    .line 361
    iput-object p1, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    .line 362
    invoke-static {}, Lax/b;->aV()I

    move-result v0

    iput v0, p0, Lax/b;->o:I

    .line 365
    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 366
    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 367
    return-void
.end method

.method protected constructor <init>([Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/m;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 394
    invoke-direct {p0, p2}, Lax/b;-><init>(Lcom/google/googlenav/ui/m;)V

    .line 395
    array-length v0, p1

    .line 398
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v1}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v1

    iput-object v1, p0, Lax/b;->u:Lax/y;

    .line 399
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p1, v1

    invoke-static {v1}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v1

    iput-object v1, p0, Lax/b;->x:Lax/y;

    .line 402
    const/4 v1, 0x2

    if-le v0, v1, :cond_35

    .line 403
    add-int/lit8 v0, v0, -0x2

    new-array v0, v0, [Lax/y;

    iput-object v0, p0, Lax/b;->A:[Lax/y;

    .line 404
    const/4 v0, 0x1

    :goto_21
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_35

    .line 405
    iget-object v1, p0, Lax/b;->A:[Lax/y;

    add-int/lit8 v2, v0, -0x1

    aget-object v3, p1, v0

    invoke-static {v3}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    aput-object v3, v1, v2

    .line 404
    add-int/lit8 v0, v0, 0x1

    goto :goto_21

    .line 408
    :cond_35
    return-void
.end method

.method static synthetic a(Lax/b;)I
    .registers 2
    .parameter

    .prologue
    .line 73
    iget v0, p0, Lax/b;->O:I

    return v0
.end method

.method static synthetic a(Lax/b;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 73
    iput p1, p0, Lax/b;->O:I

    return p1
.end method

.method private static a(ILcom/google/googlenav/ui/m;)Lax/b;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1127
    packed-switch p0, :pswitch_data_1c

    .line 1135
    new-instance v0, Lax/s;

    invoke-direct {v0, p1}, Lax/s;-><init>(Lcom/google/googlenav/ui/m;)V

    :goto_8
    return-object v0

    .line 1129
    :pswitch_9
    new-instance v0, Lax/w;

    invoke-direct {v0, p1}, Lax/w;-><init>(Lcom/google/googlenav/ui/m;)V

    goto :goto_8

    .line 1131
    :pswitch_f
    new-instance v0, Lax/x;

    invoke-direct {v0, p1}, Lax/x;-><init>(Lcom/google/googlenav/ui/m;)V

    goto :goto_8

    .line 1133
    :pswitch_15
    new-instance v0, Lax/i;

    invoke-direct {v0, p1}, Lax/i;-><init>(Lcom/google/googlenav/ui/m;)V

    goto :goto_8

    .line 1127
    nop

    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_9
        :pswitch_f
        :pswitch_15
    .end packed-switch
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 1146
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1148
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 1149
    check-cast p0, Ljava/io/InputStream;

    invoke-virtual {v0, p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 1150
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 1152
    if-nez v1, :cond_1f

    .line 1153
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed directions stream. directionsResponse is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1155
    :cond_1f
    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 1156
    invoke-static {v1, p1}, Lax/b;->a(ILcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v1

    .line 1157
    invoke-virtual {v1, v0}, Lax/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-nez v0, :cond_35

    .line 1158
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed directions stream."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1160
    :cond_35
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lax/b;->b(Z)V

    .line 1161
    return-object v1
.end method

.method protected static a(Lax/y;Lax/y;)Lax/y;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 602
    if-nez p0, :cond_3

    .line 624
    :cond_2
    :goto_2
    return-object p1

    .line 607
    :cond_3
    invoke-virtual {p0}, Lax/y;->o()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lax/y;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 612
    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2b

    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2b

    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2b

    invoke-virtual {p0}, Lax/y;->d()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_38

    .line 616
    :cond_2b
    invoke-virtual {p0}, Lax/y;->h()Ljava/lang/String;

    move-result-object v0

    .line 624
    :goto_2f
    invoke-virtual {p1}, Lax/y;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lax/y;->a(Lax/y;Ljava/lang/String;Ljava/lang/String;)Lax/y;

    move-result-object p1

    goto :goto_2

    .line 617
    :cond_38
    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_47

    .line 618
    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_2f

    .line 621
    :cond_47
    invoke-virtual {p1}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_2f
.end method

.method public static a(LaN/B;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 2022
    if-ne p1, v3, :cond_8

    .line 2023
    invoke-static {p0}, LaN/C;->d(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 2031
    :goto_7
    return-object v0

    .line 2025
    :cond_8
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2027
    const/16 v1, 0xd

    invoke-static {p0}, LaN/C;->a(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2029
    const/4 v1, 0x4

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_7
.end method

.method private a(Lax/y;[Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x7

    const/4 v1, 0x0

    .line 1183
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/bA;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1187
    if-nez p2, :cond_33

    move v0, v1

    .line 1190
    :goto_11
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1192
    invoke-static {p1}, Lax/y;->a(Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1194
    if-eqz p2, :cond_35

    .line 1195
    :goto_1e
    array-length v0, p2

    if-ge v1, v0, :cond_35

    .line 1196
    aget-object v0, p2, v1

    .line 1200
    invoke-virtual {v0, p1}, Lax/y;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 1201
    invoke-static {v0}, Lax/y;->a(Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1195
    :cond_30
    add-int/lit8 v1, v1, 0x1

    goto :goto_1e

    .line 1187
    :cond_33
    const/4 v0, 0x1

    goto :goto_11

    .line 1206
    :cond_35
    return-object v2
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2043
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2044
    const/16 v1, 0x8

    invoke-virtual {v0, v1, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2045
    if-eqz p1, :cond_12

    .line 2046
    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2048
    :cond_12
    return-object v0
.end method

.method protected static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 505
    invoke-static {p1}, Lax/y;->a(Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 506
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 507
    return-void
.end method

.method private aU()V
    .registers 2

    .prologue
    .line 555
    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/b;->s:Z

    .line 556
    return-void
.end method

.method private static aV()I
    .registers 2

    .prologue
    .line 1305
    sget-object v1, Lax/b;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 1306
    :try_start_3
    sget v0, Lax/b;->n:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lax/b;->n:I

    monitor-exit v1

    return v0

    .line 1307
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method private aW()Ljava/lang/String;
    .registers 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1672
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_c

    .line 1673
    const-string v0, ""

    .line 1699
    :goto_b
    return-object v0

    .line 1675
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1676
    invoke-virtual {p0}, Lax/b;->Q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    .line 1677
    invoke-virtual {p0}, Lax/b;->R()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v2

    .line 1678
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7c

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7c

    const/16 v3, 0x5bc

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    aput-object v0, v4, v5

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1683
    :goto_3d
    invoke-virtual {p0}, Lax/b;->N()Z

    move-result v2

    if-eqz v2, :cond_64

    .line 1684
    invoke-virtual {p0}, Lax/b;->M()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 1686
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7f

    .line 1687
    const/16 v3, 0x375

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    aput-object v0, v4, v5

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1696
    :cond_64
    :goto_64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_77

    invoke-virtual {p0}, Lax/b;->O()I

    move-result v0

    if-lez v0, :cond_77

    .line 1697
    invoke-virtual {p0}, Lax/b;->am()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1699
    :cond_77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 1678
    :cond_7c
    const-string v0, ""

    goto :goto_3d

    .line 1691
    :cond_7f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_64
.end method

.method static synthetic b(Lax/b;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 73
    iput p1, p0, Lax/b;->M:I

    return p1
.end method

.method static synthetic b(Lax/b;)Lax/y;
    .registers 2
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lax/b;->x:Lax/y;

    return-object v0
.end method

.method public static b(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 3141
    shl-int v1, v0, p0

    and-int/2addr v1, p1

    if-lez v1, :cond_7

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/b;
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1168
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 1169
    invoke-static {v0, v2}, Lax/b;->a(ILcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    .line 1170
    const-string v1, ""

    invoke-static {v1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/y;)V

    .line 1171
    const-string v1, ""

    invoke-static {v1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->b(Lax/y;)V

    .line 1172
    invoke-virtual {v0, p0, v2}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    .line 1173
    return-object v0
.end method

.method public static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 2
    .parameter

    .prologue
    .line 1998
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x4

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x1

    goto :goto_9
.end method

.method public static f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I
    .registers 2
    .parameter

    .prologue
    .line 2008
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    .line 2009
    if-eqz v0, :cond_e

    .line 2010
    array-length v0, v0

    div-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x1

    .line 2012
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method private g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/h;
    .registers 4
    .parameter

    .prologue
    .line 566
    new-instance v0, Lax/h;

    invoke-direct {v0, p0, p1}, Lax/h;-><init>(Lax/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 567
    invoke-virtual {v0}, Lax/h;->b()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 570
    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 578
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 579
    if-eqz v1, :cond_d

    if-eq v1, v0, :cond_d

    const/4 v2, 0x4

    if-ne v1, v2, :cond_e

    :cond_d
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 1024
    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    if-nez v0, :cond_6

    .line 1050
    :goto_5
    return-void

    .line 1028
    :cond_6
    invoke-static {p1}, Lax/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;

    move-result-object v0

    .line 1029
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/bA;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1031
    const/16 v2, 0x11

    iget-object v3, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/m;->f()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1034
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_20
    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 1035
    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/ui/m;->d(J)[B

    move-result-object v0

    .line 1036
    if-eqz v0, :cond_20

    array-length v5, v0

    if-eqz v5, :cond_20

    .line 1040
    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 1042
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1043
    const/4 v3, 0x3

    invoke-virtual {v5, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1045
    invoke-virtual {v1, v7, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_20

    .line 1049
    :cond_4b
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_5
.end method

.method private static j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;
    .registers 12
    .parameter

    .prologue
    .line 1057
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1060
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v4

    .line 1061
    const/4 v0, 0x0

    move v2, v0

    :goto_d
    if-ge v2, v4, :cond_54

    .line 1062
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 1064
    const/16 v0, 0x9

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    .line 1065
    const/4 v0, 0x0

    move v1, v0

    :goto_1d
    if-ge v1, v6, :cond_50

    .line 1066
    const/16 v0, 0x9

    invoke-virtual {v5, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 1068
    const/16 v0, 0xa

    invoke-virtual {v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    .line 1069
    const/4 v0, 0x0

    :goto_2c
    if-ge v0, v8, :cond_4c

    .line 1070
    const/16 v9, 0xa

    invoke-virtual {v7, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    .line 1071
    const/16 v10, 0xc

    invoke-virtual {v9, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v10

    if-eqz v10, :cond_49

    .line 1072
    const/16 v10, 0xc

    invoke-virtual {v9, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1069
    :cond_49
    add-int/lit8 v0, v0, 0x1

    goto :goto_2c

    .line 1065
    :cond_4c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1d

    .line 1061
    :cond_50
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    .line 1078
    :cond_54
    return-object v3
.end method


# virtual methods
.method public A()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 697
    iget-boolean v2, p0, Lax/b;->U:Z

    if-nez v2, :cond_7

    .line 709
    :cond_6
    :goto_6
    return v0

    .line 700
    :cond_7
    iget v2, p0, Lax/b;->p:I

    packed-switch v2, :pswitch_data_1c

    goto :goto_6

    .line 707
    :pswitch_d
    invoke-virtual {p0, v0}, Lax/b;->t(I)Z

    move-result v2

    if-nez v2, :cond_19

    invoke-virtual {p0, v1}, Lax/b;->t(I)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_19
    move v0, v1

    goto :goto_6

    .line 700
    nop

    :pswitch_data_1c
    .packed-switch 0x4
        :pswitch_d
        :pswitch_d
        :pswitch_d
    .end packed-switch
.end method

.method public B()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x313

    .line 724
    .line 725
    invoke-virtual {p0}, Lax/b;->y()I

    move-result v0

    packed-switch v0, :pswitch_data_56

    .line 749
    :pswitch_a
    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 753
    :cond_e
    :goto_e
    return-object v1

    .line 728
    :pswitch_f
    invoke-virtual {p0}, Lax/b;->aD()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 729
    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    .line 733
    :goto_1d
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 734
    const/16 v1, 0x6c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_e

    .line 730
    :cond_34
    invoke-virtual {p0}, Lax/b;->aE()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 731
    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    .line 742
    :pswitch_43
    iget-boolean v0, p0, Lax/b;->U:Z

    if-eqz v0, :cond_4c

    .line 743
    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_e

    .line 745
    :cond_4c
    const/16 v0, 0x317

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_e

    :cond_53
    move-object v0, v1

    goto :goto_1d

    .line 725
    nop

    :pswitch_data_56
    .packed-switch 0x3
        :pswitch_f
        :pswitch_a
        :pswitch_43
    .end packed-switch
.end method

.method public C()[Lax/y;
    .registers 2

    .prologue
    .line 757
    iget-object v0, p0, Lax/b;->w:[Lax/y;

    return-object v0
.end method

.method public D()[Lax/y;
    .registers 2

    .prologue
    .line 761
    iget-object v0, p0, Lax/b;->z:[Lax/y;

    return-object v0
.end method

.method public E()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 765
    iget v1, p0, Lax/b;->c:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public F()Z
    .registers 2

    .prologue
    .line 770
    iget-boolean v0, p0, Lax/b;->R:Z

    return v0
.end method

.method public G()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    .line 774
    iput v0, p0, Lax/b;->c:I

    .line 775
    iput-boolean v0, p0, Lax/b;->R:Z

    .line 776
    return-void
.end method

.method public H()Lcom/google/googlenav/ui/m;
    .registers 2

    .prologue
    .line 1082
    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    return-object v0
.end method

.method public I()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 1213
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1215
    const/4 v0, 0x1

    iget v3, p0, Lax/b;->p:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1216
    const/4 v0, 0x2

    iget v3, p0, Lax/b;->b:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1218
    iget-object v0, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v0}, Lax/l;->b()Z

    move-result v0

    if-nez v0, :cond_2d

    .line 1219
    const/4 v0, 0x3

    iget-object v3, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v3}, Lax/l;->c()Ljava/util/Date;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Date;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1222
    :cond_2d
    const/4 v0, 0x4

    iget-object v3, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v3}, Lax/l;->d()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1225
    const/16 v0, 0xd

    iget v3, p0, Lax/b;->P:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1228
    iget-object v0, p0, Lax/b;->u:Lax/y;

    iget-object v3, p0, Lax/b;->w:[Lax/y;

    invoke-direct {p0, v0, v3}, Lax/b;->a(Lax/y;[Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1231
    iget-object v0, p0, Lax/b;->x:Lax/y;

    iget-object v3, p0, Lax/b;->z:[Lax/y;

    invoke-direct {p0, v0, v3}, Lax/b;->a(Lax/y;[Lax/y;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v1

    .line 1238
    :goto_55
    iget v3, p0, Lax/b;->J:I

    if-ge v0, v3, :cond_6a

    .line 1239
    invoke-virtual {v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 1240
    iget-object v4, p0, Lax/b;->h:[Lax/h;

    aget-object v4, v4, v0

    invoke-virtual {v4, v3}, Lax/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1241
    invoke-virtual {v2, v5, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1238
    add-int/lit8 v0, v0, 0x1

    goto :goto_55

    :cond_6a
    move v0, v1

    .line 1244
    :goto_6b
    iget-object v3, p0, Lax/b;->i:[Lax/d;

    array-length v3, v3

    if-ge v0, v3, :cond_80

    .line 1245
    const/16 v3, 0xc

    iget-object v4, p0, Lax/b;->i:[Lax/d;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lax/d;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1244
    add-int/lit8 v0, v0, 0x1

    goto :goto_6b

    :cond_80
    move v0, v1

    .line 1249
    :goto_81
    iget-object v3, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v3, v3

    if-ge v0, v3, :cond_92

    .line 1250
    const/16 v3, 0x10

    iget-object v4, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v4, v4, v0

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1249
    add-int/lit8 v0, v0, 0x1

    goto :goto_81

    .line 1254
    :cond_92
    invoke-direct {p0, v2}, Lax/b;->i(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1257
    :goto_95
    iget-object v0, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v0, v0

    if-ge v1, v0, :cond_a6

    .line 1258
    const/16 v0, 0xe

    iget-object v3, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v3, v3, v1

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1257
    add-int/lit8 v1, v1, 0x1

    goto :goto_95

    .line 1262
    :cond_a6
    return-object v2
.end method

.method protected J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 1276
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 1277
    const/4 v1, 0x2

    invoke-virtual {p0}, Lax/b;->I()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1279
    iget-object v1, p0, Lax/b;->u:Lax/y;

    invoke-virtual {v1}, Lax/y;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1281
    iget-object v1, p0, Lax/b;->x:Lax/y;

    invoke-virtual {v1}, Lax/y;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1283
    invoke-virtual {p0}, Lax/b;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_34

    .line 1284
    invoke-virtual {p0}, Lax/b;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1286
    :cond_34
    const/4 v1, 0x4

    invoke-virtual {p0}, Lax/b;->az()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1288
    invoke-virtual {p0}, Lax/b;->X()Z

    move-result v1

    if-eqz v1, :cond_4c

    invoke-virtual {p0}, Lax/b;->W()Z

    move-result v1

    if-nez v1, :cond_4c

    .line 1289
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1292
    :cond_4c
    return-object v0
.end method

.method public K()I
    .registers 3

    .prologue
    .line 1298
    iget v0, p0, Lax/b;->o:I

    iget v1, p0, Lax/b;->L:I

    add-int/2addr v0, v1

    return v0
.end method

.method abstract L()I
.end method

.method public M()I
    .registers 2

    .prologue
    .line 1328
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->k()I

    move-result v0

    return v0
.end method

.method public N()Z
    .registers 2

    .prologue
    .line 1332
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->l()Z

    move-result v0

    return v0
.end method

.method public O()I
    .registers 2

    .prologue
    .line 1336
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->m()I

    move-result v0

    return v0
.end method

.method public P()Z
    .registers 2

    .prologue
    .line 1340
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->n()Z

    move-result v0

    return v0
.end method

.method public Q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 1349
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->s()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public R()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 1353
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public S()Lax/l;
    .registers 2

    .prologue
    .line 1357
    iget-object v0, p0, Lax/b;->t:Lax/l;

    return-object v0
.end method

.method public T()Z
    .registers 2

    .prologue
    .line 1374
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->h()Z

    move-result v0

    return v0
.end method

.method public U()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1414
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v1

    .line 1415
    if-nez v1, :cond_8

    .line 1419
    :cond_7
    :goto_7
    return v0

    .line 1418
    :cond_8
    invoke-virtual {v1}, Lax/h;->g()[LaN/B;

    move-result-object v1

    .line 1419
    if-eqz v1, :cond_7

    array-length v1, v1

    if-lez v1, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method public V()V
    .registers 2

    .prologue
    .line 1433
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->w()V

    .line 1434
    return-void
.end method

.method public W()Z
    .registers 2

    .prologue
    .line 1437
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_8

    .line 1438
    const/4 v0, 0x1

    .line 1441
    :goto_7
    return v0

    :cond_8
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->x()Z

    move-result v0

    goto :goto_7
.end method

.method public X()Z
    .registers 2

    .prologue
    .line 1445
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    .line 1446
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lax/h;->u()Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public Y()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1473
    move v0, v1

    :goto_2
    invoke-virtual {p0}, Lax/b;->ae()I

    move-result v2

    if-ge v0, v2, :cond_13

    .line 1474
    invoke-virtual {p0, v0}, Lax/b;->n(I)Lax/t;

    move-result-object v2

    invoke-virtual {v2}, Lax/t;->F()I

    move-result v2

    if-ltz v2, :cond_14

    .line 1475
    const/4 v1, 0x1

    .line 1478
    :cond_13
    return v1

    .line 1473
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a(LaN/p;)LaN/Y;
    .registers 6
    .parameter

    .prologue
    .line 1915
    iget v0, p0, Lax/b;->L:I

    if-ltz v0, :cond_b

    iget v0, p0, Lax/b;->L:I

    iget-object v1, p0, Lax/b;->h:[Lax/h;

    array-length v1, v1

    if-lt v0, v1, :cond_d

    .line 1916
    :cond_b
    const/4 v0, 0x0

    .line 1923
    :goto_c
    return-object v0

    .line 1919
    :cond_d
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    iget v1, p0, Lax/b;->L:I

    aget-object v0, v0, v1

    .line 1920
    const/4 v1, 0x2

    new-array v1, v1, [LaN/B;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lax/h;->i()LaN/B;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Lax/h;->j()LaN/B;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1923
    invoke-static {v0}, Lax/h;->d(Lax/h;)LaN/B;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LaN/p;->a([LaN/B;LaN/B;)LaN/Y;

    move-result-object v0

    goto :goto_c
.end method

.method public a(B)V
    .registers 2
    .parameter

    .prologue
    .line 1784
    iput-byte p1, p0, Lax/b;->N:B

    .line 1785
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 1754
    invoke-virtual {p0}, Lax/b;->f()I

    move-result v0

    if-lt p1, v0, :cond_c

    .line 1755
    invoke-virtual {p0}, Lax/b;->f()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    .line 1763
    :cond_c
    if-gez p1, :cond_14

    .line 1764
    const/4 v0, 0x0

    iput-byte v0, p0, Lax/b;->N:B

    .line 1765
    iput p1, p0, Lax/b;->M:I

    .line 1772
    :goto_13
    return-void

    .line 1766
    :cond_14
    iget-byte v0, p0, Lax/b;->N:B

    if-nez v0, :cond_1e

    .line 1767
    iput p1, p0, Lax/b;->M:I

    .line 1768
    const/4 v0, 0x1

    iput-byte v0, p0, Lax/b;->N:B

    goto :goto_13

    .line 1770
    :cond_1e
    iput p1, p0, Lax/b;->M:I

    goto :goto_13
.end method

.method public a(Lax/e;)V
    .registers 2
    .parameter

    .prologue
    .line 526
    iput-object p1, p0, Lax/b;->X:Lax/e;

    .line 527
    return-void
.end method

.method public a(Lax/l;)V
    .registers 2
    .parameter

    .prologue
    .line 1788
    iput-object p1, p0, Lax/b;->t:Lax/l;

    .line 1789
    return-void
.end method

.method public a(Lax/y;)V
    .registers 2
    .parameter

    .prologue
    .line 1848
    iput-object p1, p0, Lax/b;->u:Lax/y;

    .line 1849
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 514
    iput-object p1, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 515
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 3123
    iput-object p1, p0, Lax/b;->a:Ljava/lang/String;

    .line 3124
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 3194
    iput-boolean p1, p0, Lax/b;->U:Z

    .line 3195
    return-void
.end method

.method public a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 491
    iput-object p1, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 492
    return-void
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 834
    const/4 v0, -0x1

    iput v0, p0, Lax/b;->W:I

    .line 835
    iput-boolean v1, p0, Lax/b;->H:Z

    .line 836
    invoke-virtual {p0}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 837
    iput-boolean v1, p0, Lax/b;->R:Z

    .line 841
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lax/b;->h:[Lax/h;

    array-length v0, v0

    if-nez v0, :cond_22c

    .line 842
    :cond_1b
    new-array v0, v2, [Lax/h;

    new-instance v3, Lax/h;

    invoke-direct {v3, p0}, Lax/h;-><init>(Lax/b;)V

    aput-object v3, v0, v1

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    move v0, v2

    .line 851
    :goto_27
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    if-nez v3, :cond_4c

    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-lez v3, :cond_4c

    .line 854
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 855
    iget-object v4, p0, Lax/b;->h:[Lax/h;

    aget-object v1, v4, v1

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lax/h;->a(Lax/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 860
    :goto_48
    invoke-direct {p0}, Lax/b;->aU()V

    .line 1015
    :cond_4b
    :goto_4b
    return v2

    .line 858
    :cond_4c
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    aget-object v0, v0, v1

    invoke-static {v0, v1}, Lax/h;->a(Lax/h;I)I

    goto :goto_48

    .line 864
    :cond_54
    const/4 v0, 0x0

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    .line 865
    iput v1, p0, Lax/b;->J:I

    .line 867
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lax/b;->P:I

    .line 872
    invoke-virtual {p1, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_16f

    .line 873
    invoke-virtual {p1, v9, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v0

    iput v0, p0, Lax/b;->b:I

    .line 883
    invoke-virtual {p0}, Lax/b;->L()I

    move-result v0

    iput v0, p0, Lax/b;->L:I

    .line 885
    invoke-virtual {p1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, Lax/b;->p:I

    .line 886
    iget v0, p0, Lax/b;->p:I

    if-eq v0, v10, :cond_173

    move v0, v2

    :goto_7e
    iput-boolean v0, p0, Lax/b;->v:Z

    .line 887
    iget v0, p0, Lax/b;->p:I

    if-eq v0, v10, :cond_176

    move v0, v2

    :goto_85
    iput-boolean v0, p0, Lax/b;->y:Z

    .line 889
    invoke-virtual {p1, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_99

    .line 890
    invoke-virtual {p1, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 892
    invoke-static {p1, v5}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lax/b;->g:I

    .line 896
    :cond_99
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lax/b;->F:Ljava/lang/String;

    .line 897
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lax/b;->G:Ljava/lang/String;

    .line 900
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ne v0, v9, :cond_4b

    .line 904
    iget-object v0, p0, Lax/b;->u:Lax/y;

    iput-object v0, p0, Lax/b;->B:Lax/y;

    .line 905
    iget-object v0, p0, Lax/b;->x:Lax/y;

    iput-object v0, p0, Lax/b;->C:Lax/y;

    .line 907
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 909
    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 910
    invoke-direct {p0, v0}, Lax/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    iput-boolean v4, p0, Lax/b;->v:Z

    .line 911
    iget-boolean v4, p0, Lax/b;->v:Z

    if-eqz v4, :cond_d9

    .line 912
    if-eqz p2, :cond_179

    array-length v4, p2

    if-lt v4, v2, :cond_179

    .line 913
    aget-object v4, p2, v1

    .line 914
    invoke-static {v4, v3}, Lax/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->u:Lax/y;

    .line 922
    :cond_d9
    :goto_d9
    invoke-virtual {p0, v0}, Lax/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->w:[Lax/y;

    .line 924
    iget-boolean v3, p0, Lax/b;->v:Z

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ne v0, v9, :cond_187

    move v0, v2

    :goto_e9
    or-int/2addr v0, v3

    iput-boolean v0, p0, Lax/b;->v:Z

    .line 927
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 929
    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 930
    invoke-direct {p0, v0}, Lax/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v4

    iput-boolean v4, p0, Lax/b;->y:Z

    .line 931
    iget-boolean v4, p0, Lax/b;->y:Z

    if-eqz v4, :cond_10d

    .line 932
    if-eqz p2, :cond_18a

    array-length v4, p2

    if-lt v4, v9, :cond_18a

    .line 933
    aget-object v4, p2, v2

    .line 934
    invoke-static {v4, v3}, Lax/y;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->x:Lax/y;

    .line 942
    :cond_10d
    :goto_10d
    invoke-virtual {p0, v0}, Lax/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->z:[Lax/y;

    .line 944
    iget-boolean v3, p0, Lax/b;->y:Z

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    if-ne v0, v9, :cond_198

    move v0, v2

    :goto_11d
    or-int/2addr v0, v3

    iput-boolean v0, p0, Lax/b;->y:Z

    .line 946
    const/16 v0, 0x13

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lax/b;->I:Z

    .line 948
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    iput v0, p0, Lax/b;->J:I

    .line 949
    iget v0, p0, Lax/b;->J:I

    invoke-virtual {p0}, Lax/b;->p()I

    move-result v3

    if-le v0, v3, :cond_140

    .line 950
    invoke-virtual {p0}, Lax/b;->p()I

    move-result v0

    iput v0, p0, Lax/b;->J:I

    .line 951
    iput v5, p0, Lax/b;->p:I

    .line 957
    :cond_140
    iget v0, p0, Lax/b;->J:I

    new-array v0, v0, [Lax/h;

    iput-object v0, p0, Lax/b;->h:[Lax/h;

    .line 958
    iput-boolean v2, p0, Lax/b;->K:Z

    move v0, v1

    .line 961
    :goto_149
    :try_start_149
    iget v3, p0, Lax/b;->J:I

    if-ge v0, v3, :cond_1aa

    .line 962
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 965
    iget-object v4, p0, Lax/b;->h:[Lax/h;

    invoke-direct {p0, v3}, Lax/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/h;

    move-result-object v3

    aput-object v3, v4, v0

    .line 966
    iget-object v3, p0, Lax/b;->h:[Lax/h;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lax/h;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16c

    .line 967
    const/4 v3, 0x0

    iput-boolean v3, p0, Lax/b;->K:Z
    :try_end_16c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_149 .. :try_end_16c} :catch_19a

    .line 961
    :cond_16c
    add-int/lit8 v0, v0, 0x1

    goto :goto_149

    .line 876
    :cond_16f
    iput v5, p0, Lax/b;->p:I

    goto/16 :goto_4b

    :cond_173
    move v0, v1

    .line 886
    goto/16 :goto_7e

    :cond_176
    move v0, v1

    .line 887
    goto/16 :goto_85

    .line 917
    :cond_179
    iget-object v4, p0, Lax/b;->u:Lax/y;

    invoke-static {v3, v0}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    invoke-static {v4, v3}, Lax/b;->a(Lax/y;Lax/y;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->u:Lax/y;

    goto/16 :goto_d9

    :cond_187
    move v0, v1

    .line 924
    goto/16 :goto_e9

    .line 937
    :cond_18a
    iget-object v4, p0, Lax/b;->x:Lax/y;

    invoke-static {v3, v0}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v3

    invoke-static {v4, v3}, Lax/b;->a(Lax/y;Lax/y;)Lax/y;

    move-result-object v3

    iput-object v3, p0, Lax/b;->x:Lax/y;

    goto/16 :goto_10d

    :cond_198
    move v0, v1

    .line 944
    goto :goto_11d

    .line 970
    :catch_19a
    move-exception v3

    .line 971
    iput v0, p0, Lax/b;->J:I

    .line 972
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    iget v3, p0, Lax/b;->J:I

    const/4 v4, 0x0

    aput-object v4, v0, v3

    .line 973
    iget v0, p0, Lax/b;->J:I

    if-nez v0, :cond_1aa

    .line 974
    iput v5, p0, Lax/b;->p:I

    .line 978
    :cond_1aa
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 979
    new-array v0, v3, [Lax/d;

    iput-object v0, p0, Lax/b;->i:[Lax/d;

    move v0, v1

    .line 980
    :goto_1b5
    if-ge v0, v3, :cond_1d5

    .line 981
    const/16 v4, 0xc

    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 983
    iget-object v5, p0, Lax/b;->i:[Lax/d;

    new-instance v6, Lax/d;

    invoke-virtual {v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v10}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, p0, v7, v8, v4}, Lax/d;-><init>(Lax/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v0

    .line 980
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b5

    .line 989
    :cond_1d5
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 990
    new-array v0, v3, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    .line 991
    :goto_1e0
    if-ge v0, v3, :cond_1ef

    .line 992
    iget-object v4, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v5, 0x10

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    aput-object v5, v4, v0

    .line 991
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e0

    .line 995
    :cond_1ef
    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    if-eqz v0, :cond_202

    .line 996
    iget-object v0, p0, Lax/b;->e:Lcom/google/googlenav/ui/m;

    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {p1}, Lax/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/ui/m;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;)V

    .line 1001
    :cond_202
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 1005
    new-array v3, v0, [I

    iput-object v3, p0, Lax/b;->l:[I

    .line 1006
    new-array v3, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v3, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1007
    :goto_210
    if-ge v1, v0, :cond_227

    .line 1008
    const/16 v3, 0xe

    invoke-virtual {p1, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 1010
    iget-object v4, p0, Lax/b;->l:[I

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    aput v5, v4, v1

    .line 1011
    iget-object v4, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aput-object v3, v4, v1

    .line 1007
    add-int/lit8 v1, v1, 0x1

    goto :goto_210

    .line 1014
    :cond_227
    invoke-direct {p0}, Lax/b;->aU()V

    goto/16 :goto_4b

    :cond_22c
    move v0, v1

    goto/16 :goto_27
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 5
    .parameter

    .prologue
    .line 1095
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1097
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    .line 1103
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lax/b;->t:Lax/l;

    invoke-virtual {v0}, Lax/l;->d()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_29

    .line 1106
    iget-object v0, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    iget v2, p0, Lax/b;->g:I

    invoke-static {v0, v2}, Lax/l;->a(Ljava/util/Date;I)Lax/l;

    move-result-object v0

    iput-object v0, p0, Lax/b;->t:Lax/l;

    .line 1111
    :cond_29
    invoke-virtual {p0}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1112
    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    .line 1113
    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    .line 1119
    :goto_39
    return v1

    .line 1115
    :cond_3a
    sget-object v0, Lax/b;->r:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->c()V

    .line 1116
    sget-object v0, Lax/b;->q:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    goto :goto_39
.end method

.method public a(II)[LaN/B;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1386
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v3

    .line 1387
    if-nez v3, :cond_a

    .line 1388
    new-array v0, v1, [LaN/B;

    .line 1407
    :cond_9
    :goto_9
    return-object v0

    .line 1392
    :cond_a
    if-ltz p1, :cond_e

    if-gez p2, :cond_13

    .line 1393
    :cond_e
    invoke-virtual {v3}, Lax/h;->g()[LaN/B;

    move-result-object v0

    goto :goto_9

    .line 1398
    :cond_13
    invoke-virtual {v3, p1}, Lax/h;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {v3, p2}, Lax/h;->e(I)Z

    move-result v0

    if-nez v0, :cond_22

    .line 1399
    :cond_1f
    new-array v0, v1, [LaN/B;

    goto :goto_9

    .line 1401
    :cond_22
    invoke-virtual {v3, p1}, Lax/h;->f(I)I

    move-result v2

    .line 1402
    invoke-virtual {v3, p2}, Lax/h;->f(I)I

    move-result v4

    .line 1403
    sub-int v0, v4, v2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [LaN/B;

    move v1, v2

    .line 1404
    :goto_31
    if-gt v1, v4, :cond_9

    .line 1405
    sub-int v5, v1, v2

    invoke-virtual {v3}, Lax/h;->g()[LaN/B;

    move-result-object v6

    aget-object v6, v6, v1

    aput-object v6, v0, v5

    .line 1404
    add-int/lit8 v1, v1, 0x1

    goto :goto_31
.end method

.method public aA()I
    .registers 2

    .prologue
    .line 1954
    iget v0, p0, Lax/b;->J:I

    return v0
.end method

.method public aB()LaN/B;
    .registers 2

    .prologue
    .line 1958
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1959
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->i()LaN/B;

    move-result-object v0

    .line 1961
    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, Lax/b;->u:Lax/y;

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    goto :goto_e
.end method

.method public aC()LaN/B;
    .registers 2

    .prologue
    .line 1965
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1966
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->j()LaN/B;

    move-result-object v0

    .line 1968
    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, Lax/b;->x:Lax/y;

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    goto :goto_e
.end method

.method public aD()Z
    .registers 3

    .prologue
    .line 1976
    iget v0, p0, Lax/b;->p:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_b

    iget-boolean v0, p0, Lax/b;->v:Z

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public aE()Z
    .registers 3

    .prologue
    .line 1984
    iget v0, p0, Lax/b;->p:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_b

    iget-boolean v0, p0, Lax/b;->y:Z

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public aF()[I
    .registers 2

    .prologue
    .line 2071
    iget-object v0, p0, Lax/b;->l:[I

    return-object v0
.end method

.method public aG()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 2123
    invoke-virtual {p0}, Lax/b;->k()I

    move-result v0

    if-ne v0, v2, :cond_9

    .line 2124
    const/4 v0, 0x0

    .line 2128
    :goto_8
    return-object v0

    .line 2126
    :cond_9
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    .line 2128
    if-eqz v0, :cond_19

    invoke-virtual {v0}, Lax/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    :cond_19
    const-string v0, ""

    goto :goto_8

    :cond_1c
    const/16 v1, 0x5fe

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lax/h;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method abstract aH()Lax/b;
.end method

.method public aI()Lax/b;
    .registers 3

    .prologue
    .line 3097
    invoke-virtual {p0}, Lax/b;->aH()Lax/b;

    move-result-object v0

    .line 3098
    invoke-virtual {p0}, Lax/b;->S()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/l;)V

    .line 3099
    invoke-virtual {p0}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 3100
    invoke-virtual {p0}, Lax/b;->aN()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/b;->v(I)V

    .line 3101
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lax/b;->b(Z)V

    .line 3102
    return-object v0
.end method

.method public aJ()Lax/b;
    .registers 3

    .prologue
    .line 3111
    invoke-virtual {p0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    .line 3112
    invoke-virtual {p0}, Lax/b;->J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    .line 3113
    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .registers 2

    .prologue
    .line 3118
    iget-object v0, p0, Lax/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lax/b;->a:Ljava/lang/String;

    :goto_6
    return-object v0

    :cond_7
    const-string v0, ""

    goto :goto_6
.end method

.method public aL()Z
    .registers 2

    .prologue
    .line 3130
    iget-boolean v0, p0, Lax/b;->H:Z

    return v0
.end method

.method public aM()V
    .registers 2

    .prologue
    .line 3134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/b;->H:Z

    .line 3135
    return-void
.end method

.method public aN()I
    .registers 2

    .prologue
    .line 3165
    iget v0, p0, Lax/b;->T:I

    return v0
.end method

.method public aO()Lax/b;
    .registers 4

    .prologue
    .line 3180
    const/4 v0, 0x0

    .line 3181
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lax/b;->t(I)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 3182
    new-instance v0, Lax/s;

    invoke-virtual {p0}, Lax/b;->ap()Lax/j;

    move-result-object v1

    invoke-direct {v0, v1}, Lax/s;-><init>(Lax/k;)V

    .line 3187
    :cond_11
    :goto_11
    if-eqz v0, :cond_18

    .line 3188
    iget v1, p0, Lax/b;->T:I

    invoke-virtual {v0, v1}, Lax/b;->v(I)V

    .line 3190
    :cond_18
    return-object v0

    .line 3183
    :cond_19
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lax/b;->t(I)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 3184
    new-instance v0, Lax/w;

    invoke-virtual {p0}, Lax/b;->ap()Lax/j;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    goto :goto_11
.end method

.method public aP()Ljava/lang/String;
    .registers 2

    .prologue
    .line 3202
    iget-object v0, p0, Lax/b;->V:Ljava/lang/String;

    return-object v0
.end method

.method public aQ()Ljava/lang/String;
    .registers 2

    .prologue
    .line 3209
    iget v0, p0, Lax/b;->b:I

    packed-switch v0, :pswitch_data_22

    .line 3217
    const/16 v0, 0x107

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_b
    return-object v0

    .line 3211
    :pswitch_c
    const/16 v0, 0x5c8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 3213
    :pswitch_13
    const/16 v0, 0x605

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 3215
    :pswitch_1a
    const/16 v0, 0x59

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 3209
    nop

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_c
        :pswitch_13
        :pswitch_1a
    .end packed-switch
.end method

.method public aT()Z
    .registers 2

    .prologue
    .line 3222
    iget-boolean v0, p0, Lax/b;->S:Z

    return v0
.end method

.method public a_()Z
    .registers 2

    .prologue
    .line 1818
    const/4 v0, 0x1

    return v0
.end method

.method public aa()Z
    .registers 2

    .prologue
    .line 1485
    iget-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v0, v0

    if-lez v0, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public ab()Lax/h;
    .registers 2

    .prologue
    .line 1497
    iget v0, p0, Lax/b;->L:I

    invoke-virtual {p0, v0}, Lax/b;->l(I)Lax/h;

    move-result-object v0

    return-object v0
.end method

.method public ac()Ljava/util/List;
    .registers 3

    .prologue
    .line 1515
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    .line 1517
    if-nez v0, :cond_d

    .line 1518
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1521
    :goto_c
    return-object v0

    :cond_d
    invoke-virtual {v0}, Lax/h;->c()Ljava/util/List;

    move-result-object v0

    goto :goto_c
.end method

.method public ad()[LaN/B;
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 1530
    invoke-virtual {p0, v0, v0}, Lax/b;->a(II)[LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public ae()I
    .registers 2

    .prologue
    .line 1573
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    .line 1576
    if-nez v0, :cond_8

    .line 1577
    const/4 v0, 0x0

    .line 1579
    :goto_7
    return v0

    :cond_8
    invoke-virtual {v0}, Lax/h;->e()I

    move-result v0

    goto :goto_7
.end method

.method public af()Ljava/util/List;
    .registers 2

    .prologue
    .line 1587
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lax/b;->ae()I

    move-result v0

    if-nez v0, :cond_11

    .line 1588
    :cond_c
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 1591
    :goto_10
    return-object v0

    :cond_11
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->f()Ljava/util/List;

    move-result-object v0

    goto :goto_10
.end method

.method public ag()I
    .registers 2

    .prologue
    .line 1598
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-static {v0}, Lax/h;->a(Lax/h;)I

    move-result v0

    return v0
.end method

.method public ah()I
    .registers 2

    .prologue
    .line 1605
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-static {v0}, Lax/h;->b(Lax/h;)I

    move-result v0

    return v0
.end method

.method public ai()LaN/B;
    .registers 2

    .prologue
    .line 1612
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-static {v0}, Lax/h;->c(Lax/h;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public aj()I
    .registers 2

    .prologue
    .line 1625
    iget v0, p0, Lax/b;->E:I

    return v0
.end method

.method public ak()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1633
    iget-object v0, p0, Lax/b;->F:Ljava/lang/String;

    return-object v0
.end method

.method public al()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1641
    iget-object v0, p0, Lax/b;->G:Ljava/lang/String;

    return-object v0
.end method

.method public am()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1659
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    if-nez v0, :cond_9

    .line 1660
    const-string v0, ""

    .line 1662
    :goto_8
    return-object v0

    :cond_9
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method public an()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1709
    iget v0, p0, Lax/b;->L:I

    invoke-virtual {p0, v0}, Lax/b;->p(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ao()Ljava/lang/String;
    .registers 4

    .prologue
    .line 1734
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1735
    invoke-direct {p0}, Lax/b;->aW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1737
    invoke-virtual {p0}, Lax/b;->an()Ljava/lang/String;

    move-result-object v1

    .line 1738
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_21

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_21

    .line 1739
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1741
    :cond_21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1743
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ap()Lax/j;
    .registers 5

    .prologue
    .line 1779
    new-instance v0, Lax/j;

    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-virtual {p0}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method

.method public aq()Lax/y;
    .registers 2

    .prologue
    .line 1843
    iget-object v0, p0, Lax/b;->u:Lax/y;

    return-object v0
.end method

.method public ar()Lax/y;
    .registers 2

    .prologue
    .line 1852
    iget-object v0, p0, Lax/b;->B:Lax/y;

    return-object v0
.end method

.method public as()Lax/y;
    .registers 2

    .prologue
    .line 1857
    iget-object v0, p0, Lax/b;->x:Lax/y;

    return-object v0
.end method

.method public at()Lax/y;
    .registers 2

    .prologue
    .line 1892
    iget-object v0, p0, Lax/b;->C:Lax/y;

    return-object v0
.end method

.method public au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 1897
    iget-object v0, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public av()I
    .registers 2

    .prologue
    .line 1910
    iget v0, p0, Lax/b;->P:I

    return v0
.end method

.method public aw()LaN/B;
    .registers 3

    .prologue
    .line 1928
    iget v0, p0, Lax/b;->L:I

    if-ltz v0, :cond_b

    iget v0, p0, Lax/b;->L:I

    iget-object v1, p0, Lax/b;->h:[Lax/h;

    array-length v1, v1

    if-lt v0, v1, :cond_d

    .line 1929
    :cond_b
    const/4 v0, 0x0

    .line 1931
    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    iget v1, p0, Lax/b;->L:I

    aget-object v0, v0, v1

    invoke-static {v0}, Lax/h;->d(Lax/h;)LaN/B;

    move-result-object v0

    goto :goto_c
.end method

.method public ax()Z
    .registers 2

    .prologue
    .line 1942
    iget-boolean v0, p0, Lax/b;->I:Z

    return v0
.end method

.method public ay()I
    .registers 2

    .prologue
    .line 1946
    iget v0, p0, Lax/b;->k:I

    return v0
.end method

.method public az()I
    .registers 2

    .prologue
    .line 1950
    iget v0, p0, Lax/b;->L:I

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 1838
    const/16 v0, 0x1c

    return v0
.end method

.method public b(I)Lcom/google/googlenav/E;
    .registers 3
    .parameter

    .prologue
    .line 1544
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    .line 1545
    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-virtual {v0, p1}, Lax/h;->a(I)Lax/m;

    move-result-object v0

    goto :goto_7
.end method

.method public b(Lax/y;)V
    .registers 2
    .parameter

    .prologue
    .line 1888
    iput-object p1, p0, Lax/b;->x:Lax/y;

    .line 1889
    return-void
.end method

.method public b(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 1270
    invoke-virtual {p0}, Lax/b;->J()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1271
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 1273
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 3198
    iput-object p1, p0, Lax/b;->V:Ljava/lang/String;

    .line 3199
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 3226
    iput-boolean p1, p0, Lax/b;->S:Z

    .line 3227
    return-void
.end method

.method protected b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lax/y;
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x7

    const/4 v5, 0x1

    .line 641
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 642
    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    .line 643
    const/4 v2, 0x2

    if-eq v1, v2, :cond_12

    if-eq v1, v5, :cond_12

    .line 659
    :cond_11
    :goto_11
    return-object v0

    .line 647
    :cond_12
    new-array v1, v3, [Lax/y;

    .line 648
    const/4 v2, 0x0

    :goto_15
    if-ge v2, v3, :cond_24

    .line 649
    invoke-virtual {p1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 650
    invoke-static {v4, p1}, Lax/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v4

    aput-object v4, v1, v2

    .line 648
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    .line 654
    :cond_24
    array-length v2, v1

    if-le v2, v5, :cond_11

    move-object v0, v1

    .line 659
    goto :goto_11
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1798
    iget v0, p0, Lax/b;->M:I

    return v0
.end method

.method public c(I)I
    .registers 2
    .parameter

    .prologue
    .line 1550
    return p1
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 785
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    new-array v1, v0, [Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 787
    const/4 v0, 0x0

    :goto_9
    array-length v2, v1

    if-ge v0, v2, :cond_15

    .line 788
    invoke-virtual {p1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    aput-object v2, v1, v0

    .line 787
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 792
    :cond_15
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 793
    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lax/b;->a(Ljava/lang/String;)V

    .line 796
    :cond_22
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-virtual {p0, v0}, Lax/b;->r(I)V

    .line 799
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    .line 802
    iget-object v1, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_61

    .line 803
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 804
    iget v2, p0, Lax/b;->g:I

    invoke-static {v1, v2}, Lax/l;->a(Ljava/util/Date;I)Lax/l;

    move-result-object v1

    iput-object v1, p0, Lax/b;->t:Lax/l;

    .line 811
    :goto_4b
    if-eqz v0, :cond_60

    .line 812
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->g(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p0, v1}, Lax/b;->s(I)V

    .line 815
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v1

    if-eqz v1, :cond_60

    .line 817
    invoke-virtual {p0}, Lax/b;->V()V

    .line 820
    :cond_60
    return v0

    .line 807
    :cond_61
    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v1

    iput-object v1, p0, Lax/b;->t:Lax/l;

    goto :goto_4b
.end method

.method public c_()Z
    .registers 2

    .prologue
    .line 1824
    const/4 v0, 0x1

    return v0
.end method

.method public d()B
    .registers 2

    .prologue
    .line 1793
    iget-byte v0, p0, Lax/b;->N:B

    return v0
.end method

.method public d(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 443
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 445
    invoke-virtual {v3, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 447
    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v0

    invoke-static {v3, v0}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V

    move v0, v1

    .line 448
    :goto_14
    iget-object v4, p0, Lax/b;->A:[Lax/y;

    array-length v4, v4

    if-ge v0, v4, :cond_23

    .line 449
    iget-object v4, p0, Lax/b;->A:[Lax/y;

    aget-object v4, v4, v0

    invoke-static {v3, v4}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V

    .line 448
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 451
    :cond_23
    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-static {v3, v0}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lax/y;)V

    .line 453
    const/4 v0, 0x5

    invoke-virtual {p0}, Lax/b;->p()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 454
    const/4 v0, 0x7

    iget v4, p0, Lax/b;->c:I

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 455
    const/16 v0, 0xd

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 456
    const/16 v0, 0x1e

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 457
    const/16 v0, 0x1f

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 458
    iget-object v0, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_52

    .line 459
    const/16 v0, 0x9

    iget-object v4, p0, Lax/b;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 461
    :cond_52
    const/16 v0, 0xb

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 464
    const/16 v0, 0x1c

    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v0, v1

    .line 466
    :goto_5d
    iget-object v4, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    array-length v4, v4

    if-ge v0, v4, :cond_6e

    .line 467
    const/16 v4, 0xa

    iget-object v5, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v5, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 466
    add-int/lit8 v0, v0, 0x1

    goto :goto_5d

    .line 473
    :cond_6e
    const/16 v0, 0x10

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 475
    const/16 v0, 0x15

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 480
    const/16 v0, 0x22

    invoke-virtual {p0}, Lax/b;->aT()Z

    move-result v4

    invoke-virtual {v3, v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 485
    const/16 v0, 0x1b

    invoke-virtual {p0}, Lax/b;->m()Z

    move-result v4

    if-nez v4, :cond_8a

    move v1, v2

    :cond_8a
    invoke-virtual {v3, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 486
    return-object v3
.end method

.method public final d_()V
    .registers 1

    .prologue
    .line 1834
    return-void
.end method

.method public e()Lcom/google/googlenav/E;
    .registers 3

    .prologue
    .line 1620
    invoke-virtual {p0}, Lax/b;->c()I

    move-result v0

    .line 1621
    const/4 v1, -0x1

    if-eq v0, v1, :cond_c

    invoke-virtual {p0, v0}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public e(I)V
    .registers 2
    .parameter

    .prologue
    .line 522
    iput p1, p0, Lax/b;->g:I

    .line 523
    return-void
.end method

.method public f()I
    .registers 2

    .prologue
    .line 1555
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    .line 1558
    if-nez v0, :cond_8

    .line 1559
    const/4 v0, 0x0

    .line 1561
    :goto_7
    return v0

    :cond_8
    invoke-virtual {v0}, Lax/h;->d()I

    move-result v0

    goto :goto_7
.end method

.method public f(I)J
    .registers 4
    .parameter

    .prologue
    .line 1324
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->h(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public g(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1344
    invoke-virtual {p0, p1}, Lax/b;->l(I)Lax/h;

    move-result-object v0

    .line 1345
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lax/h;->p()Ljava/lang/String;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const-string v0, ""

    goto :goto_a
.end method

.method public h(I)LaN/B;
    .registers 3
    .parameter

    .prologue
    .line 1361
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->c(I)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public i(I)Lo/D;
    .registers 3
    .parameter

    .prologue
    .line 1365
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->d(I)Lo/D;

    move-result-object v0

    return-object v0
.end method

.method public j(I)Lax/d;
    .registers 3
    .parameter

    .prologue
    .line 1463
    if-ltz p1, :cond_7

    iget-object v0, p0, Lax/b;->i:[Lax/d;

    array-length v0, v0

    if-lt p1, v0, :cond_9

    .line 1464
    :cond_7
    const/4 v0, 0x0

    .line 1466
    :goto_8
    return-object v0

    :cond_9
    iget-object v0, p0, Lax/b;->i:[Lax/d;

    aget-object v0, v0, p1

    goto :goto_8
.end method

.method public k()I
    .registers 2

    .prologue
    .line 412
    iget v0, p0, Lax/b;->b:I

    return v0
.end method

.method public k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3
    .parameter

    .prologue
    .line 1489
    iget-object v0, p0, Lax/b;->j:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public l(I)Lax/h;
    .registers 3
    .parameter

    .prologue
    .line 1507
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    if-eqz v0, :cond_b

    if-ltz p1, :cond_b

    iget-object v0, p0, Lax/b;->h:[Lax/h;

    array-length v0, v0

    if-lt p1, v0, :cond_d

    .line 1508
    :cond_b
    const/4 v0, 0x0

    .line 1511
    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lax/b;->h:[Lax/h;

    aget-object v0, v0, p1

    goto :goto_c
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 416
    iget v0, p0, Lax/b;->b:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public m(I)I
    .registers 3
    .parameter

    .prologue
    .line 1539
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->g(I)I

    move-result v0

    return v0
.end method

.method public m()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 420
    iget v1, p0, Lax/b;->b:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public n(I)Lax/t;
    .registers 3
    .parameter

    .prologue
    .line 1583
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->b(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method public n()Z
    .registers 3

    .prologue
    .line 424
    iget v0, p0, Lax/b;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public o(I)V
    .registers 2
    .parameter

    .prologue
    .line 1629
    iput p1, p0, Lax/b;->E:I

    .line 1630
    return-void
.end method

.method public o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 495
    iget-object v0, p0, Lax/b;->Q:[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public abstract p()I
.end method

.method public p(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1720
    iget-boolean v0, p0, Lax/b;->K:Z

    if-eqz v0, :cond_9

    .line 1721
    invoke-virtual {p0, p1}, Lax/b;->g(I)Ljava/lang/String;

    move-result-object v0

    .line 1723
    :goto_8
    return-object v0

    :cond_9
    const-string v0, ""

    goto :goto_8
.end method

.method public q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 510
    iget-object v0, p0, Lax/b;->f:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public q(I)Z
    .registers 3
    .parameter

    .prologue
    .line 1775
    invoke-virtual {p0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/h;->i(I)Z

    move-result v0

    return v0
.end method

.method public r()I
    .registers 2

    .prologue
    .line 518
    iget v0, p0, Lax/b;->g:I

    return v0
.end method

.method public r(I)V
    .registers 2
    .parameter

    .prologue
    .line 1935
    iput p1, p0, Lax/b;->k:I

    .line 1936
    return-void
.end method

.method public s(I)V
    .registers 3
    .parameter

    .prologue
    .line 1988
    iput p1, p0, Lax/b;->L:I

    .line 1989
    const/4 v0, 0x0

    iput v0, p0, Lax/b;->M:I

    .line 1990
    return-void
.end method

.method public s()Z
    .registers 2

    .prologue
    .line 1813
    const/4 v0, 0x0

    return v0
.end method

.method public s_()Z
    .registers 2

    .prologue
    .line 1803
    const/4 v0, 0x1

    return v0
.end method

.method public t()V
    .registers 2

    .prologue
    .line 530
    iget-object v0, p0, Lax/b;->X:Lax/e;

    if-eqz v0, :cond_9

    .line 531
    iget-object v0, p0, Lax/b;->X:Lax/e;

    invoke-interface {v0}, Lax/e;->a()V

    .line 533
    :cond_9
    return-void
.end method

.method public t(I)Z
    .registers 3
    .parameter

    .prologue
    .line 3148
    iget v0, p0, Lax/b;->T:I

    invoke-static {p1, v0}, Lax/b;->b(II)Z

    move-result v0

    return v0
.end method

.method public t_()Z
    .registers 2

    .prologue
    .line 1808
    invoke-virtual {p0}, Lax/b;->E()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public u(I)V
    .registers 4
    .parameter

    .prologue
    .line 3155
    invoke-virtual {p0, p1}, Lax/b;->t(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 3156
    iget v0, p0, Lax/b;->T:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    sub-int/2addr v0, v1

    iput v0, p0, Lax/b;->T:I

    .line 3158
    :cond_d
    return-void
.end method

.method public u()Z
    .registers 2

    .prologue
    .line 543
    const/4 v0, 0x0

    return v0
.end method

.method public v(I)V
    .registers 2
    .parameter

    .prologue
    .line 3161
    iput p1, p0, Lax/b;->T:I

    .line 3162
    return-void
.end method

.method public v()Z
    .registers 2

    .prologue
    .line 547
    iget-boolean v0, p0, Lax/b;->s:Z

    return v0
.end method

.method public w()V
    .registers 2

    .prologue
    .line 551
    const/4 v0, 0x0

    iput-boolean v0, p0, Lax/b;->s:Z

    .line 552
    return-void
.end method

.method public x()Z
    .registers 2

    .prologue
    .line 667
    invoke-virtual {p0}, Lax/b;->C()[Lax/y;

    move-result-object v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lax/b;->D()[Lax/y;

    move-result-object v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public y()I
    .registers 2

    .prologue
    .line 672
    iget v0, p0, Lax/b;->p:I

    return v0
.end method

.method public z()Z
    .registers 2

    .prologue
    .line 680
    iget v0, p0, Lax/b;->p:I

    packed-switch v0, :pswitch_data_a

    .line 688
    :pswitch_5
    const/4 v0, 0x0

    :goto_6
    return v0

    .line 686
    :pswitch_7
    const/4 v0, 0x1

    goto :goto_6

    .line 680
    nop

    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_7
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method
