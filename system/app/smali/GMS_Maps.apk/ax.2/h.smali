.class public Lax/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[Lax/t;

.field protected b:Ljava/util/List;

.field final synthetic c:Lax/b;

.field private d:[LaN/B;

.field private e:[I

.field private f:[Lax/m;

.field private g:Ljava/util/List;

.field private h:[LaN/B;

.field private i:LaN/B;

.field private j:LaN/B;

.field private k:I

.field private l:Z

.field private m:I

.field private n:Z

.field private o:I

.field private p:Lax/f;

.field private q:Ljava/lang/String;

.field private r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private t:Z

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:I

.field private x:I

.field private y:LaN/B;


# direct methods
.method constructor <init>(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2262
    iput-object p1, p0, Lax/h;->c:Lax/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219
    new-array v0, v1, [I

    iput-object v0, p0, Lax/h;->e:[I

    .line 2247
    iput-boolean v1, p0, Lax/h;->t:Z

    .line 2263
    return-void
.end method

.method constructor <init>(Lax/b;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2265
    iput-object p1, p0, Lax/h;->c:Lax/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219
    new-array v0, v1, [I

    iput-object v0, p0, Lax/h;->e:[I

    .line 2247
    iput-boolean v1, p0, Lax/h;->t:Z

    .line 2270
    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 2271
    invoke-direct {p0, p2}, Lax/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2272
    invoke-direct {p0, p2}, Lax/h;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lax/h;->b:Ljava/util/List;

    .line 2273
    invoke-direct {p0, v0}, Lax/h;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    iput-boolean v1, p0, Lax/h;->t:Z

    .line 2274
    invoke-direct {p0, v0}, Lax/h;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2275
    invoke-virtual {p0}, Lax/h;->v()V

    .line 2276
    invoke-direct {p0, v0}, Lax/h;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2277
    return-void
.end method

.method static synthetic a(Lax/h;)I
    .registers 2
    .parameter

    .prologue
    .line 2212
    iget v0, p0, Lax/h;->x:I

    return v0
.end method

.method static synthetic a(Lax/h;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2212
    iput p1, p0, Lax/h;->o:I

    return p1
.end method

.method static synthetic a(Lax/h;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2212
    invoke-direct {p0, p1, p2}, Lax/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 3009
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lax/h;->o:I

    .line 3011
    if-eqz p2, :cond_16

    .line 3012
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    iput-boolean v0, p0, Lax/h;->l:Z

    .line 3013
    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v0

    iput v0, p0, Lax/h;->k:I

    .line 3016
    :cond_16
    iget v0, p0, Lax/h;->o:I

    iget v1, p0, Lax/h;->k:I

    invoke-static {v0, v1}, Lax/f;->a(II)Lax/f;

    move-result-object v0

    iput-object v0, p0, Lax/h;->p:Lax/f;

    .line 3017
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2480
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 2482
    iget-object v2, p0, Lax/h;->c:Lax/b;

    invoke-static {v2}, Lax/b;->a(Lax/b;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_19

    .line 2483
    iget-object v2, p0, Lax/h;->c:Lax/b;

    invoke-static {v0}, Lax/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v3

    invoke-static {v2, v3}, Lax/b;->a(Lax/b;I)I

    .line 2485
    :cond_19
    iget-object v3, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v2, p2, 0x1

    invoke-static {v0}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v4

    aput-object v4, v3, p2

    .line 2487
    invoke-static {v0}, LaN/C;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/Y;

    move-result-object v3

    .line 2489
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    .line 2491
    if-nez v0, :cond_31

    .line 2492
    new-array v0, v1, [B

    .line 2494
    :cond_31
    new-instance v4, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v4, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2498
    :goto_3b
    :try_start_3b
    invoke-interface {v4}, Ljava/io/DataInput;->readShort()S

    move-result v5

    .line 2499
    invoke-interface {v4}, Ljava/io/DataInput;->readShort()S

    move-result v6

    .line 2500
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v7, v2, -0x1

    aget-object v7, v0, v7

    .line 2501
    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-static {v0}, Lax/b;->a(Lax/b;)I

    move-result v0

    const/4 v8, 0x4

    if-ne v0, v8, :cond_5e

    .line 2502
    iget-object v8, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v7, v5, v6, v3}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v5

    aput-object v5, v8, v2

    :goto_5c
    move v2, v0

    .line 2507
    goto :goto_3b

    .line 2504
    :cond_5e
    iget-object v8, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v0, v2, 0x1

    new-instance v9, LaN/B;

    invoke-virtual {v7}, LaN/B;->c()I

    move-result v10

    add-int/2addr v5, v10

    invoke-virtual {v7}, LaN/B;->e()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v9, v5, v6}, LaN/B;-><init>(II)V

    aput-object v9, v8, v2
    :try_end_73
    .catch Ljava/io/EOFException; {:try_start_3b .. :try_end_73} :catch_74
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_73} :catch_77

    goto :goto_5c

    .line 2508
    :catch_74
    move-exception v0

    .line 2514
    const/4 v0, 0x1

    :goto_76
    return v0

    .line 2510
    :catch_77
    move-exception v0

    move v0, v1

    .line 2512
    goto :goto_76
.end method

.method static synthetic b(Lax/h;)I
    .registers 2
    .parameter

    .prologue
    .line 2212
    iget v0, p0, Lax/h;->w:I

    return v0
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2435
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 2436
    const/16 v1, 0x14

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lax/h;->v:Ljava/lang/String;

    .line 2437
    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    iput-boolean v1, p0, Lax/h;->l:Z

    .line 2438
    invoke-static {v0, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    iput v1, p0, Lax/h;->k:I

    .line 2440
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    iput-boolean v1, p0, Lax/h;->n:Z

    .line 2441
    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    iput v1, p0, Lax/h;->m:I

    .line 2443
    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lax/h;->q:Ljava/lang/String;

    .line 2444
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v1

    iput v1, p0, Lax/h;->o:I

    .line 2446
    iget v1, p0, Lax/h;->o:I

    iget v2, p0, Lax/h;->k:I

    invoke-static {v1, v2}, Lax/f;->a(II)Lax/f;

    move-result-object v1

    iput-object v1, p0, Lax/h;->p:Lax/f;

    .line 2447
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2448
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lax/h;->s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2449
    return-void
.end method

.method static synthetic c(Lax/h;)LaN/B;
    .registers 2
    .parameter

    .prologue
    .line 2212
    iget-object v0, p0, Lax/h;->y:LaN/B;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 8
    .parameter

    .prologue
    const/16 v5, 0x1a

    .line 2457
    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 2459
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2460
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v1, :cond_1d

    .line 2461
    new-instance v3, Lax/g;

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lax/g;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2464
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2460
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 2467
    :cond_1d
    return-object v2
.end method

.method static synthetic d(Lax/h;)LaN/B;
    .registers 2
    .parameter

    .prologue
    .line 2212
    iget-object v0, p0, Lax/h;->i:LaN/B;

    return-object v0
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 22
    .parameter

    .prologue
    .line 2522
    move-object/from16 v0, p0

    iget-object v2, v0, Lax/h;->d:[LaN/B;

    array-length v12, v2

    .line 2523
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v13

    .line 2524
    if-nez v13, :cond_10

    .line 2573
    :cond_f
    :goto_f
    return-void

    .line 2527
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lax/h;->k:I

    move-object/from16 v0, p0

    iget v3, v0, Lax/h;->o:I

    add-int/2addr v3, v2

    .line 2528
    new-array v2, v12, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lax/h;->e:[I

    .line 2529
    move-object/from16 v0, p0

    iget-object v2, v0, Lax/h;->e:[I

    const/4 v4, 0x0

    aput v3, v2, v4

    .line 2530
    new-array v14, v12, [I

    .line 2531
    const/4 v4, 0x0

    .line 2532
    const/4 v2, 0x1

    :goto_2a
    if-ge v2, v12, :cond_4b

    .line 2533
    move-object/from16 v0, p0

    iget-object v5, v0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lax/h;->d:[LaN/B;

    add-int/lit8 v7, v2, -0x1

    aget-object v6, v6, v7

    sget-object v7, Lax/b;->d:LaN/Y;

    invoke-virtual {v5, v6, v7}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v5

    long-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    double-to-int v5, v5

    aput v5, v14, v2

    .line 2532
    add-int/lit8 v2, v2, 0x1

    goto :goto_2a

    .line 2536
    :cond_4b
    const/4 v2, 0x0

    move v8, v2

    move v10, v3

    :goto_4e
    if-ge v8, v13, :cond_f

    .line 2537
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 2539
    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v11

    .line 2541
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v9

    .line 2545
    if-lt v9, v12, :cond_6e

    .line 2546
    const/4 v2, 0x0

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lax/h;->e:[I

    goto :goto_f

    .line 2549
    :cond_6e
    if-eqz v9, :cond_b2

    .line 2551
    const-wide/16 v2, 0x0

    move v5, v4

    .line 2552
    :goto_73
    if-ge v5, v9, :cond_80

    .line 2553
    add-int/lit8 v6, v5, 0x1

    aget v6, v14, v6

    int-to-long v6, v6

    add-long/2addr v6, v2

    .line 2552
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-wide v2, v6

    goto :goto_73

    .line 2555
    :cond_80
    const-wide/16 v5, 0x0

    .line 2558
    const-wide/16 v15, 0x0

    cmp-long v7, v2, v15

    if-nez v7, :cond_8a

    .line 2559
    const-wide/16 v2, 0x1

    .line 2561
    :cond_8a
    :goto_8a
    if-ge v4, v9, :cond_b9

    .line 2562
    add-int/lit8 v7, v4, 0x1

    aget v7, v14, v7

    int-to-long v15, v7

    add-long/2addr v5, v15

    .line 2564
    move-object/from16 v0, p0

    iget-object v7, v0, Lax/h;->e:[I

    add-int/lit8 v15, v4, 0x1

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-int v18, v11, v10

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    mul-long v18, v18, v5

    div-long v18, v18, v2

    add-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    aput v16, v7, v15

    .line 2561
    add-int/lit8 v4, v4, 0x1

    goto :goto_8a

    .line 2568
    :cond_b2
    move-object/from16 v0, p0

    iget-object v2, v0, Lax/h;->e:[I

    const/4 v3, 0x0

    aput v11, v2, v3

    .line 2536
    :cond_b9
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v4, v9

    move v10, v11

    goto :goto_4e
.end method

.method private e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x7

    const/4 v0, 0x0

    .line 2673
    iget-object v1, p0, Lax/h;->c:Lax/b;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lax/b;->a(Lax/b;I)I

    .line 2674
    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v1, v0

    move v2, v0

    .line 2676
    :goto_e
    if-ge v1, v3, :cond_1c

    .line 2677
    invoke-virtual {p1, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lax/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2676
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 2680
    :cond_1c
    new-array v1, v2, [LaN/B;

    iput-object v1, p0, Lax/h;->d:[LaN/B;

    move v1, v0

    move v2, v0

    .line 2681
    :goto_22
    if-ge v2, v3, :cond_37

    .line 2682
    invoke-virtual {p1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 2684
    invoke-direct {p0, v4, v1}, Lax/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v5

    if-nez v5, :cond_2f

    .line 2691
    :goto_2e
    return v0

    .line 2687
    :cond_2f
    invoke-static {v4}, Lax/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)I

    move-result v4

    add-int/2addr v1, v4

    .line 2681
    add-int/lit8 v2, v2, 0x1

    goto :goto_22

    .line 2689
    :cond_37
    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v1, v0

    iput-object v0, p0, Lax/h;->i:LaN/B;

    .line 2690
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    iget-object v1, p0, Lax/h;->d:[LaN/B;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lax/h;->j:LaN/B;

    .line 2691
    const/4 v0, 0x1

    goto :goto_2e
.end method

.method private f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 16
    .parameter

    .prologue
    .line 2695
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 2700
    if-lez v0, :cond_57

    const/16 v1, 0xa

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_57

    .line 2703
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/aY;->k:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2705
    const/4 v2, 0x7

    iget-object v3, p0, Lax/h;->c:Lax/b;

    invoke-static {v3}, Lax/b;->b(Lax/b;)Lax/y;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2707
    const/4 v2, 0x5

    iget-object v3, p0, Lax/h;->d:[LaN/B;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2708
    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2711
    const/16 v2, 0xa

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 2713
    if-eqz v2, :cond_50

    .line 2714
    const/16 v3, 0x19

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2717
    :cond_50
    add-int/lit8 v0, v0, 0x1

    .line 2718
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2721
    :cond_57
    new-array v1, v0, [Lax/t;

    iput-object v1, p0, Lax/h;->a:[Lax/t;

    .line 2722
    new-array v1, v0, [LaN/B;

    iput-object v1, p0, Lax/h;->h:[LaN/B;

    .line 2723
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lax/h;->g:Ljava/util/List;

    .line 2727
    const/4 v1, 0x0

    iput-boolean v1, p0, Lax/h;->u:Z

    .line 2728
    const/4 v2, 0x0

    .line 2730
    const/4 v1, 0x0

    move v13, v1

    move v1, v2

    move v2, v13

    :goto_6d
    if-ge v2, v0, :cond_100

    .line 2731
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    new-instance v4, Lax/t;

    const/16 v5, 0xa

    invoke-virtual {p1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-direct {v4, v5}, Lax/t;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v4, v3, v2

    .line 2733
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lax/t;->B()I

    move-result v3

    iget-object v4, p0, Lax/h;->d:[LaN/B;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-lt v3, v4, :cond_99

    .line 2734
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    iget-object v4, p0, Lax/h;->d:[LaN/B;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lax/t;->c(I)V

    .line 2736
    :cond_99
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    iget-object v4, p0, Lax/h;->h:[LaN/B;

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    iget-object v6, p0, Lax/h;->a:[Lax/t;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lax/t;->B()I

    move-result v6

    aget-object v5, v5, v6

    aput-object v5, v4, v2

    invoke-virtual {v3, v5}, Lax/t;->a(LaN/B;)V

    .line 2742
    iget-boolean v3, p0, Lax/h;->u:Z

    if-nez v3, :cond_cd

    iget-object v3, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v3}, Lax/b;->m()Z

    move-result v3

    if-eqz v3, :cond_cd

    .line 2743
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lax/t;->E()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_dc

    .line 2744
    if-eqz v1, :cond_cc

    .line 2745
    const/4 v1, 0x1

    iput-boolean v1, p0, Lax/h;->u:Z

    .line 2747
    :cond_cc
    const/4 v1, 0x1

    .line 2754
    :cond_cd
    :goto_cd
    if-nez v2, :cond_de

    .line 2755
    iget-object v3, p0, Lax/h;->g:Ljava/util/List;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2730
    :cond_d9
    :goto_d9
    add-int/lit8 v2, v2, 0x1

    goto :goto_6d

    .line 2749
    :cond_dc
    const/4 v1, 0x0

    goto :goto_cd

    .line 2756
    :cond_de
    iget-object v3, p0, Lax/h;->a:[Lax/t;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lax/t;->h()Lo/D;

    move-result-object v3

    iget-object v4, p0, Lax/h;->a:[Lax/t;

    add-int/lit8 v5, v2, -0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lax/t;->h()Lo/D;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d9

    .line 2757
    iget-object v3, p0, Lax/h;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d9

    .line 2763
    :cond_100
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 2764
    const/4 v7, 0x0

    .line 2766
    const/4 v11, 0x0

    .line 2767
    const/4 v2, 0x0

    :goto_108
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    array-length v0, v0

    if-ge v2, v0, :cond_1cc

    .line 2768
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v1, v0, v2

    .line 2769
    invoke-virtual {v1}, Lax/t;->B()I

    move-result v4

    .line 2770
    invoke-virtual {v1}, Lax/t;->C()Z

    move-result v0

    if-nez v0, :cond_120

    move v0, v11

    .line 2767
    :goto_11c
    add-int/lit8 v2, v2, 0x1

    move v11, v0

    goto :goto_108

    .line 2773
    :cond_120
    invoke-virtual {v1}, Lax/t;->E()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_15b

    .line 2774
    new-instance v0, Lax/m;

    const/4 v3, 0x0

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v4

    invoke-virtual {v1}, Lax/t;->G()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lax/m;-><init>(Lax/t;IIILaN/B;Ljava/lang/String;)V

    .line 2777
    invoke-virtual {p0}, Lax/h;->u()Z

    move-result v3

    if-eqz v3, :cond_156

    .line 2778
    if-nez v7, :cond_1e3

    .line 2779
    new-instance v5, Lax/a;

    iget-object v3, p0, Lax/h;->d:[LaN/B;

    aget-object v9, v3, v4

    invoke-virtual {v1}, Lax/t;->G()Ljava/lang/String;

    move-result-object v10

    move-object v6, v1

    move v7, v2

    move v8, v4

    invoke-direct/range {v5 .. v10}, Lax/a;-><init>(Lax/t;IILaN/B;Ljava/lang/String;)V

    .line 2781
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2784
    :goto_150
    invoke-virtual {v5, v0}, Lax/a;->a(Lax/m;)V

    :goto_153
    move v0, v11

    move-object v7, v5

    .line 2788
    goto :goto_11c

    .line 2786
    :cond_156
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v7

    goto :goto_153

    .line 2790
    :cond_15b
    invoke-virtual {v1}, Lax/t;->E()I

    move-result v0

    packed-switch v0, :pswitch_data_1e6

    .line 2800
    :pswitch_162
    const/4 v3, 0x0

    .line 2806
    :goto_163
    if-eqz v7, :cond_1e1

    .line 2807
    const/4 v0, 0x3

    if-ne v3, v0, :cond_1c3

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    .line 2810
    :goto_170
    invoke-virtual {v7}, Lax/a;->m()Lax/t;

    move-result-object v5

    invoke-virtual {v5, v0}, Lax/t;->a(Ljava/lang/String;)V

    .line 2811
    const/4 v8, 0x0

    .line 2814
    :goto_178
    new-instance v0, Lax/m;

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v4

    invoke-virtual {v1}, Lax/t;->G()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lax/m;-><init>(Lax/t;IIILaN/B;Ljava/lang/String;)V

    .line 2817
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2819
    iget-object v4, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v4}, Lax/b;->m()Z

    move-result v4

    if-eqz v4, :cond_1dd

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1dd

    .line 2822
    if-nez v11, :cond_1db

    .line 2823
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lax/m;->a(Z)V

    .line 2824
    const/4 v7, 0x1

    .line 2827
    :goto_19a
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    add-int/lit8 v3, v2, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lax/t;->B()I

    move-result v4

    .line 2828
    new-instance v0, Lax/m;

    const/4 v3, 0x2

    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v4

    iget-object v6, p0, Lax/h;->a:[Lax/t;

    add-int/lit8 v9, v2, 0x1

    aget-object v6, v6, v9

    invoke-virtual {v6}, Lax/t;->G()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lax/m;-><init>(Lax/t;IIILaN/B;Ljava/lang/String;)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v7

    move-object v7, v8

    goto/16 :goto_11c

    .line 2792
    :pswitch_1bf
    const/4 v3, 0x3

    .line 2793
    goto :goto_163

    .line 2796
    :pswitch_1c1
    const/4 v3, 0x1

    .line 2797
    goto :goto_163

    .line 2807
    :cond_1c3
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_170

    .line 2834
    :cond_1cc
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lax/m;

    invoke-interface {v12, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lax/m;

    iput-object v0, p0, Lax/h;->f:[Lax/m;

    .line 2835
    return-void

    :cond_1db
    move v7, v11

    goto :goto_19a

    :cond_1dd
    move v0, v11

    move-object v7, v8

    goto/16 :goto_11c

    :cond_1e1
    move-object v8, v7

    goto :goto_178

    :cond_1e3
    move-object v5, v7

    goto/16 :goto_150

    .line 2790
    :pswitch_data_1e6
    .packed-switch 0x1
        :pswitch_1c1
        :pswitch_162
        :pswitch_162
        :pswitch_1bf
    .end packed-switch
.end method

.method private j(I)I
    .registers 5
    .parameter

    .prologue
    .line 2917
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    .line 2918
    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-static {v1}, Lax/b;->a(Lax/b;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1e

    .line 2919
    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v1, v1, p1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    sub-int v0, v1, v0

    .line 2922
    :goto_1d
    return v0

    .line 2921
    :cond_1e
    const/16 v1, 0x16

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    .line 2922
    iget-object v2, p0, Lax/h;->d:[LaN/B;

    aget-object v2, v2, p1

    invoke-virtual {v2, v1}, LaN/B;->a(LaN/Y;)I

    move-result v2

    invoke-virtual {v0, v1}, LaN/B;->a(LaN/Y;)I

    move-result v0

    sub-int v0, v2, v0

    goto :goto_1d
.end method

.method private k(I)I
    .registers 5
    .parameter

    .prologue
    .line 2933
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    .line 2934
    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-static {v1}, Lax/b;->a(Lax/b;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1e

    .line 2935
    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v1, v1, p1

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    sub-int v0, v1, v0

    .line 2938
    :goto_1d
    return v0

    .line 2937
    :cond_1e
    const/16 v1, 0x16

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    .line 2938
    iget-object v2, p0, Lax/h;->d:[LaN/B;

    aget-object v2, v2, p1

    invoke-virtual {v2, v1}, LaN/B;->b(LaN/Y;)I

    move-result v2

    invoke-virtual {v0, v1}, LaN/B;->b(LaN/Y;)I

    move-result v0

    sub-int v0, v2, v0

    goto :goto_1d
.end method

.method private l(I)Z
    .registers 3
    .parameter

    .prologue
    .line 2947
    const/16 v0, -0x8000

    if-lt p1, v0, :cond_a

    const/16 v0, 0x7fff

    if-gt p1, v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public a(I)Lax/m;
    .registers 3
    .parameter

    .prologue
    .line 2288
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    goto :goto_5
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 2280
    iget-object v0, p0, Lax/h;->v:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 13
    .parameter

    .prologue
    const/4 v9, 0x7

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 2951
    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->l:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2952
    const/16 v1, 0x9

    invoke-virtual {p1, v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2953
    iget-object v1, p0, Lax/h;->v:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 2954
    const/16 v1, 0x14

    iget-object v2, p0, Lax/h;->v:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2956
    :cond_1e
    invoke-virtual {p0}, Lax/h;->l()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 2957
    const/4 v1, 0x2

    invoke-virtual {p0}, Lax/h;->k()I

    move-result v2

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2959
    :cond_2c
    invoke-virtual {p0}, Lax/h;->n()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 2960
    invoke-virtual {p0}, Lax/h;->m()I

    move-result v1

    invoke-virtual {v6, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2963
    :cond_39
    iget-object v1, p0, Lax/h;->q:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_47

    .line 2964
    const/4 v1, 0x6

    iget-object v2, p0, Lax/h;->q:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2967
    :cond_47
    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->l()Z

    move-result v1

    if-eqz v1, :cond_54

    .line 2971
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2974
    :cond_54
    iget-object v1, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_64

    .line 2975
    const/4 v1, 0x4

    iget-object v2, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2976
    const/4 v1, 0x5

    iget-object v2, p0, Lax/h;->s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2979
    :cond_64
    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v3, v1, v4

    .line 2980
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2981
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move v5, v0

    move-object v0, v1

    move-object v1, v2

    .line 2983
    :goto_75
    :try_start_75
    iget-object v2, p0, Lax/h;->d:[LaN/B;

    array-length v2, v2

    if-ge v5, v2, :cond_c3

    .line 2984
    invoke-direct {p0, v5}, Lax/h;->j(I)I

    move-result v2

    .line 2985
    invoke-direct {p0, v5}, Lax/h;->k(I)I

    move-result v7

    .line 2986
    invoke-direct {p0, v2}, Lax/h;->l(I)Z

    move-result v8

    if-eqz v8, :cond_9a

    invoke-direct {p0, v7}, Lax/h;->l(I)Z

    move-result v8

    if-eqz v8, :cond_9a

    .line 2987
    invoke-interface {v0, v2}, Ljava/io/DataOutput;->writeShort(I)V

    .line 2988
    invoke-interface {v0, v7}, Ljava/io/DataOutput;->writeShort(I)V

    move-object v2, v3

    .line 2983
    :goto_95
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v2

    goto :goto_75

    .line 2990
    :cond_9a
    const/4 v0, 0x7

    iget-object v2, p0, Lax/h;->c:Lax/b;

    invoke-static {v2}, Lax/b;->a(Lax/b;)I

    move-result v2

    invoke-static {v3, v2}, Lax/b;->a(LaN/B;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-static {v2, v7}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2993
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_b5
    .catch Ljava/io/IOException; {:try_start_75 .. :try_end_b5} :catch_c2

    .line 2994
    :try_start_b5
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 2995
    iget-object v1, p0, Lax/h;->d:[LaN/B;

    aget-object v1, v1, v5
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_be} :catch_ef

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    goto :goto_95

    .line 2998
    :catch_c2
    move-exception v0

    .line 3001
    :cond_c3
    :goto_c3
    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-static {v0}, Lax/b;->a(Lax/b;)I

    move-result v0

    invoke-static {v3, v0}, Lax/b;->a(LaN/B;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lax/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v6, v9, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v4

    .line 3003
    :goto_d9
    iget-object v1, p0, Lax/h;->a:[Lax/t;

    array-length v1, v1

    if-ge v0, v1, :cond_ee

    .line 3004
    const/16 v1, 0xa

    iget-object v2, p0, Lax/h;->a:[Lax/t;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 3003
    add-int/lit8 v0, v0, 0x1

    goto :goto_d9

    .line 3006
    :cond_ee
    return-void

    .line 2998
    :catch_ef
    move-exception v0

    move-object v1, v2

    goto :goto_c3
.end method

.method a(III)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2583
    if-ge p3, p2, :cond_a

    .line 2584
    if-le p1, p3, :cond_8

    if-lt p1, p2, :cond_9

    :cond_8
    move v0, v1

    .line 2586
    :cond_9
    :goto_9
    return v0

    :cond_a
    if-gt p1, p3, :cond_10

    if-lt p1, p2, :cond_10

    :goto_e
    move v0, v1

    goto :goto_9

    :cond_10
    move v1, v0

    goto :goto_e
.end method

.method public b(I)Lax/t;
    .registers 3
    .parameter

    .prologue
    .line 2300
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 2284
    iget-boolean v0, p0, Lax/h;->t:Z

    return v0
.end method

.method public c(I)LaN/B;
    .registers 3
    .parameter

    .prologue
    .line 2308
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->g()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .registers 2

    .prologue
    .line 2292
    iget-object v0, p0, Lax/h;->b:Ljava/util/List;

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 2296
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    array-length v0, v0

    goto :goto_5
.end method

.method public d(I)Lo/D;
    .registers 3
    .parameter

    .prologue
    .line 2312
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->h()Lo/D;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 2304
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    array-length v0, v0

    return v0
.end method

.method public e(I)Z
    .registers 3
    .parameter

    .prologue
    .line 2341
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    if-eqz v0, :cond_13

    if-ltz p1, :cond_13

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    array-length v0, v0

    if-ge p1, v0, :cond_13

    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public f(I)I
    .registers 3
    .parameter

    .prologue
    .line 2346
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->B()I

    move-result v0

    return v0
.end method

.method public f()Ljava/util/List;
    .registers 2

    .prologue
    .line 2316
    iget-object v0, p0, Lax/h;->g:Ljava/util/List;

    return-object v0
.end method

.method public g(I)I
    .registers 3
    .parameter

    .prologue
    .line 2352
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    if-nez v0, :cond_c

    .line 2353
    :cond_a
    const/4 v0, 0x0

    .line 2355
    :goto_b
    return v0

    :cond_c
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/m;->l()I

    move-result v0

    goto :goto_b
.end method

.method public g()[LaN/B;
    .registers 2

    .prologue
    .line 2324
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    return-object v0
.end method

.method public h(I)J
    .registers 4
    .parameter

    .prologue
    .line 2367
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->y()J

    move-result-wide v0

    return-wide v0
.end method

.method public h()Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 2328
    iget-object v2, p0, Lax/h;->a:[Lax/t;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 2329
    invoke-virtual {v4}, Lax/t;->h()Lo/D;

    move-result-object v4

    if-eqz v4, :cond_11

    .line 2330
    const/4 v0, 0x1

    .line 2334
    :cond_10
    return v0

    .line 2328
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_5
.end method

.method public i()LaN/B;
    .registers 2

    .prologue
    .line 2359
    iget-object v0, p0, Lax/h;->i:LaN/B;

    return-object v0
.end method

.method public i(I)Z
    .registers 4
    .parameter

    .prologue
    .line 2371
    iget-object v0, p0, Lax/h;->a:[Lax/t;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public j()LaN/B;
    .registers 2

    .prologue
    .line 2363
    iget-object v0, p0, Lax/h;->j:LaN/B;

    return-object v0
.end method

.method public k()I
    .registers 2

    .prologue
    .line 2375
    iget v0, p0, Lax/h;->k:I

    return v0
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 2379
    iget-boolean v0, p0, Lax/h;->l:Z

    return v0
.end method

.method public m()I
    .registers 2

    .prologue
    .line 2383
    iget v0, p0, Lax/h;->m:I

    return v0
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 2387
    iget-boolean v0, p0, Lax/h;->n:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2391
    invoke-virtual {p0}, Lax/h;->m()I

    move-result v0

    iget-object v1, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v1}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .registers 2

    .prologue
    .line 2395
    iget-object v0, p0, Lax/h;->q:Ljava/lang/String;

    return-object v0
.end method

.method public q()I
    .registers 2

    .prologue
    .line 2399
    iget v0, p0, Lax/h;->o:I

    return v0
.end method

.method public r()Lax/f;
    .registers 2

    .prologue
    .line 2403
    iget-object v0, p0, Lax/h;->p:Lax/f;

    return-object v0
.end method

.method public s()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 2407
    iget-object v0, p0, Lax/h;->r:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 2411
    iget-object v0, p0, Lax/h;->s:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public u()Z
    .registers 2

    .prologue
    .line 2415
    iget-boolean v0, p0, Lax/h;->u:Z

    return v0
.end method

.method v()V
    .registers 11

    .prologue
    const v9, 0x15752a00

    const/4 v5, 0x0

    .line 2596
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    array-length v0, v0

    if-nez v0, :cond_a

    .line 2664
    :goto_9
    return-void

    .line 2601
    :cond_a
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v4

    .line 2604
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v3

    .line 2605
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v2

    .line 2606
    iget-object v0, p0, Lax/h;->d:[LaN/B;

    aget-object v0, v0, v5

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v1

    .line 2607
    iput v5, p0, Lax/h;->x:I

    .line 2608
    iput v5, p0, Lax/h;->w:I

    .line 2609
    new-instance v0, LaN/B;

    invoke-direct {v0, v5, v5}, LaN/B;-><init>(II)V

    iput-object v0, p0, Lax/h;->y:LaN/B;

    .line 2611
    const/4 v0, 0x1

    :goto_36
    iget-object v5, p0, Lax/h;->d:[LaN/B;

    array-length v5, v5

    if-ge v0, v5, :cond_8f

    .line 2614
    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v0

    invoke-virtual {v5}, LaN/B;->e()I

    move-result v5

    .line 2615
    iget-object v6, p0, Lax/h;->d:[LaN/B;

    add-int/lit8 v7, v0, -0x1

    aget-object v6, v6, v7

    invoke-virtual {v6}, LaN/B;->e()I

    move-result v8

    .line 2619
    sub-int v6, v5, v8

    .line 2620
    if-gez v6, :cond_b6

    .line 2621
    add-int/2addr v6, v9

    move v7, v6

    .line 2626
    :goto_53
    sub-int v6, v8, v5

    .line 2627
    if-gez v6, :cond_58

    .line 2628
    add-int/2addr v6, v9

    .line 2634
    :cond_58
    if-ge v6, v7, :cond_79

    .line 2636
    invoke-virtual {p0, v4, v5, v8}, Lax/h;->a(III)Z

    move-result v6

    if-eqz v6, :cond_6b

    if-eq v4, v5, :cond_6b

    .line 2637
    invoke-virtual {p0, v5, v4, v3}, Lax/h;->a(III)Z

    move-result v4

    if-eqz v4, :cond_6a

    .line 2638
    iput v9, p0, Lax/h;->x:I

    :cond_6a
    move v4, v5

    .line 2653
    :cond_6b
    :goto_6b
    iget-object v5, p0, Lax/h;->d:[LaN/B;

    aget-object v5, v5, v0

    invoke-virtual {v5}, LaN/B;->c()I

    move-result v5

    .line 2654
    if-le v5, v2, :cond_8b

    move v2, v5

    .line 2611
    :cond_76
    :goto_76
    add-int/lit8 v0, v0, 0x1

    goto :goto_36

    .line 2644
    :cond_79
    invoke-virtual {p0, v3, v8, v5}, Lax/h;->a(III)Z

    move-result v6

    if-eqz v6, :cond_6b

    if-eq v3, v5, :cond_6b

    .line 2645
    invoke-virtual {p0, v5, v4, v3}, Lax/h;->a(III)Z

    move-result v3

    if-eqz v3, :cond_89

    .line 2646
    iput v9, p0, Lax/h;->x:I

    :cond_89
    move v3, v5

    .line 2648
    goto :goto_6b

    .line 2656
    :cond_8b
    if-ge v5, v1, :cond_76

    move v1, v5

    .line 2657
    goto :goto_76

    .line 2660
    :cond_8f
    if-lt v3, v4, :cond_ae

    sub-int v0, v3, v4

    .line 2661
    :goto_93
    iget v3, p0, Lax/h;->x:I

    if-le v0, v3, :cond_b3

    :goto_97
    iput v0, p0, Lax/h;->x:I

    .line 2662
    sub-int v0, v2, v1

    iput v0, p0, Lax/h;->w:I

    .line 2663
    new-instance v0, LaN/B;

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lax/h;->x:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    iput-object v0, p0, Lax/h;->y:LaN/B;

    goto/16 :goto_9

    .line 2660
    :cond_ae
    sub-int v0, v4, v3

    sub-int v0, v9, v0

    goto :goto_93

    .line 2661
    :cond_b3
    iget v0, p0, Lax/h;->x:I

    goto :goto_97

    :cond_b6
    move v7, v6

    goto :goto_53
.end method

.method public w()V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 2870
    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-virtual {v0}, Lax/b;->m()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2890
    :goto_9
    return-void

    .line 2874
    :cond_a
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 2875
    :goto_10
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    array-length v0, v0

    if-ge v1, v0, :cond_3f

    .line 2876
    iget-object v0, p0, Lax/h;->f:[Lax/m;

    aget-object v0, v0, v1

    .line 2877
    invoke-virtual {v0}, Lax/m;->r()Z

    move-result v3

    if-eqz v3, :cond_38

    invoke-virtual {v0}, Lax/m;->n()Z

    move-result v3

    if-eqz v3, :cond_38

    .line 2879
    check-cast v0, Lax/a;

    move v3, v2

    .line 2880
    :goto_28
    invoke-virtual {v0}, Lax/a;->h()I

    move-result v5

    if-ge v3, v5, :cond_3b

    .line 2881
    invoke-virtual {v0, v3}, Lax/a;->a(I)Lax/m;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2880
    add-int/lit8 v3, v3, 0x1

    goto :goto_28

    .line 2885
    :cond_38
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2875
    :cond_3b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 2888
    :cond_3f
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lax/m;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lax/m;

    iput-object v0, p0, Lax/h;->f:[Lax/m;

    .line 2889
    iget-object v0, p0, Lax/h;->c:Lax/b;

    invoke-static {v0, v2}, Lax/b;->b(Lax/b;I)I

    goto :goto_9
.end method

.method public x()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 2898
    invoke-virtual {p0}, Lax/h;->u()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2907
    :cond_7
    :goto_7
    return v1

    :cond_8
    move v0, v1

    .line 2902
    :goto_9
    iget-object v2, p0, Lax/h;->f:[Lax/m;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 2903
    iget-object v2, p0, Lax/h;->f:[Lax/m;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lax/m;->r()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 2904
    const/4 v1, 0x1

    goto :goto_7

    .line 2902
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_9
.end method

.method public y()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 3024
    move v0, v1

    :goto_2
    iget-object v2, p0, Lax/h;->a:[Lax/t;

    array-length v2, v2

    if-ge v0, v2, :cond_18

    .line 3025
    iget-object v2, p0, Lax/h;->a:[Lax/t;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 3026
    const/4 v1, 0x1

    .line 3029
    :cond_18
    return v1

    .line 3024
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
