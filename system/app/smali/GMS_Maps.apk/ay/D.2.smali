.class public Lay/D;
.super Lay/u;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lay/S;Lay/H;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 27
    const/16 v3, 0x8

    const/4 v4, 0x0

    new-instance v5, Lay/E;

    invoke-direct {v5}, Lay/E;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lay/u;-><init>(Lay/S;Lay/H;IZLay/T;)V

    .line 36
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 55
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Lay/a;
    .registers 5
    .parameter

    .prologue
    .line 45
    invoke-virtual {p0}, Lay/D;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/a;

    .line 46
    invoke-virtual {v0}, Lay/a;->c()I

    move-result v2

    if-ne v2, p1, :cond_8

    .line 50
    :goto_1a
    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method protected a(Lay/a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-virtual {p1}, Lay/a;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 81
    const/4 v1, 0x6

    invoke-virtual {p2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 82
    return-void
.end method

.method protected bridge synthetic a(Lay/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 23
    check-cast p1, Lay/a;

    invoke-virtual {p0, p1, p2}, Lay/D;->a(Lay/a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 87
    return-void
.end method

.method public a(Lay/a;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x2

    .line 91
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 92
    invoke-virtual {p0, p1, v0}, Lay/D;->a(Lay/a;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lay/a;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lay/a;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 94
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/hc;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 95
    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 97
    iget-object v0, p0, Lay/D;->b:Lay/S;

    iget v2, p0, Lay/D;->a:I

    invoke-interface {v0, v2, v1}, Lay/S;->b(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Lay/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 23
    check-cast p1, Lay/a;

    invoke-virtual {p0, p1}, Lay/D;->a(Lay/a;)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/s;
    .registers 3
    .parameter

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lay/D;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/a;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/List;
    .registers 4

    .prologue
    .line 60
    invoke-virtual {p0}, Lay/D;->a()Ljava/util/List;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/cx;->c(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 62
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/a;

    .line 63
    invoke-virtual {v0}, Lay/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 65
    :cond_24
    return-object v1
.end method

.method protected c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/a;
    .registers 4
    .parameter

    .prologue
    .line 75
    new-instance v0, Lay/a;

    invoke-static {p1}, Lay/D;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-direct {v0, v1}, Lay/a;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v0
.end method
