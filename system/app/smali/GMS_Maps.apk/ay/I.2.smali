.class public Lay/I;
.super Lay/u;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lay/S;Lay/H;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 21
    const/16 v3, 0x9

    const/4 v4, 0x0

    new-instance v5, Lay/J;

    invoke-direct {v5}, Lay/J;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lay/u;-><init>(Lay/S;Lay/H;IZLay/T;)V

    .line 30
    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 17
    invoke-static {p0}, Lay/I;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 57
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/K;
    .registers 3
    .parameter

    .prologue
    .line 39
    invoke-static {p1}, Lay/I;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lay/K;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/K;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lay/K;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-virtual {p1}, Lay/K;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 45
    const/4 v1, 0x4

    invoke-virtual {p2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 46
    return-void
.end method

.method protected bridge synthetic a(Lay/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 17
    check-cast p1, Lay/K;

    invoke-virtual {p0, p1, p2}, Lay/I;->a(Lay/K;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method protected a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-static {p2}, Lay/I;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 52
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 53
    return-void
.end method

.method protected synthetic b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/s;
    .registers 3
    .parameter

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lay/I;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/K;

    move-result-object v0

    return-object v0
.end method
