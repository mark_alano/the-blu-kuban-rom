.class public Lay/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lay/B;
.implements Lcom/google/googlenav/g;


# instance fields
.field private final a:Lad/h;

.field private final b:Lay/H;

.field private final c:Ljava/util/Set;

.field private d:Z

.field private e:Z

.field private final f:Ljava/lang/Object;

.field private g:Z

.field private final h:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lad/h;Lay/H;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lay/b;->c:Ljava/util/Set;

    .line 48
    iput-boolean v1, p0, Lay/b;->d:Z

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lay/b;->e:Z

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lay/b;->f:Ljava/lang/Object;

    .line 64
    iput-boolean v1, p0, Lay/b;->g:Z

    .line 72
    invoke-static {}, Lcom/google/common/collect/dm;->a()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lay/b;->h:Ljava/util/Set;

    .line 76
    iput-object p1, p0, Lay/b;->a:Lad/h;

    .line 77
    iput-object p2, p0, Lay/b;->b:Lay/H;

    .line 78
    return-void
.end method

.method static synthetic a(Lay/b;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lay/b;->c:Ljava/util/Set;

    return-object v0
.end method

.method private a(Lay/t;Ljava/util/List;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 86
    const-string v0, "BackgroundPlaceDetailsFetcher.fetchDetailsIfNecessary"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 87
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88
    iget-object v2, p0, Lay/b;->h:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-interface {p1, v0}, Lay/t;->a(Ljava/lang/String;)Lay/s;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-interface {p1, v0}, Lay/t;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 94
    iget-object v2, p0, Lay/b;->c:Ljava/util/Set;

    monitor-enter v2

    .line 95
    :try_start_2c
    iget-object v3, p0, Lay/b;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_3c

    .line 96
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_2c .. :try_end_36} :catchall_46

    .line 106
    :cond_36
    const-string v0, "BackgroundPlaceDetailsFetcher.fetchDetailsIfNecessary"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 107
    return-void

    .line 98
    :cond_3c
    :try_start_3c
    iget-object v3, p0, Lay/b;->c:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_49

    .line 99
    monitor-exit v2

    goto :goto_9

    .line 101
    :catchall_46
    move-exception v0

    monitor-exit v2
    :try_end_48
    .catchall {:try_start_3c .. :try_end_48} :catchall_46

    throw v0

    :cond_49
    :try_start_49
    monitor-exit v2
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_46

    .line 103
    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lay/b;->a:Lad/h;

    invoke-virtual {v0, v2}, Lad/h;->c(Lad/g;)V

    goto :goto_9
.end method

.method static synthetic a(Lay/b;Lay/m;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lay/b;->a(Lay/m;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lay/b;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 29
    iput-boolean p1, p0, Lay/b;->d:Z

    return p1
.end method

.method private a(Lay/m;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 194
    invoke-interface {p1}, Lay/m;->d()Lay/t;

    move-result-object v1

    .line 195
    invoke-interface {v1, p2}, Lay/t;->a(Ljava/lang/String;)Lay/s;

    move-result-object v2

    if-nez v2, :cond_c

    .line 216
    :goto_b
    return v0

    .line 198
    :cond_c
    invoke-interface {p1, p2, p3}, Lay/m;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-nez v2, :cond_31

    .line 200
    iget-object v2, p0, Lay/b;->h:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 203
    const-string v2, "BPDF2"

    invoke-static {v2, p2}, LaU/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-interface {v1, p2}, Lay/t;->b(Ljava/lang/String;)Lay/G;

    move-result-object v2

    .line 210
    if-eqz v2, :cond_31

    .line 211
    new-instance v3, Lay/G;

    invoke-virtual {v2}, Lay/G;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-direct {v3, v2}, Lay/G;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 212
    invoke-virtual {v3, v0}, Lay/G;->a(Z)V

    .line 213
    invoke-interface {v1, v3}, Lay/t;->a(Lay/G;)V

    .line 216
    :cond_31
    const/4 v0, 0x1

    goto :goto_b
.end method

.method static synthetic b(Lay/b;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lay/b;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lay/b;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 29
    iput-boolean p1, p0, Lay/b;->e:Z

    return p1
.end method

.method private c()I
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 113
    const/4 v0, 0x5

    new-array v2, v0, [Lay/m;

    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v0

    invoke-virtual {v0}, Lay/l;->f()Lay/m;

    move-result-object v0

    aput-object v0, v2, v1

    const/4 v0, 0x1

    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v3

    invoke-virtual {v3}, Lay/l;->h()Lay/m;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v3

    invoke-virtual {v3}, Lay/l;->i()Lay/m;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v3

    invoke-virtual {v3}, Lay/l;->e()Lay/m;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x4

    iget-boolean v0, p0, Lay/b;->g:Z

    if-eqz v0, :cond_49

    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v0

    invoke-virtual {v0}, Lay/l;->g()Lay/m;

    move-result-object v0

    :goto_3c
    aput-object v0, v2, v3

    move v0, v1

    .line 124
    :goto_3f
    array-length v3, v2

    if-ge v0, v3, :cond_64

    .line 125
    aget-object v3, v2, v0

    if-nez v3, :cond_4b

    .line 124
    :goto_46
    add-int/lit8 v0, v0, 0x1

    goto :goto_3f

    .line 113
    :cond_49
    const/4 v0, 0x0

    goto :goto_3c

    .line 128
    :cond_4b
    aget-object v3, v2, v0

    invoke-interface {v3}, Lay/m;->d()Lay/t;

    move-result-object v3

    invoke-interface {v3}, Lay/t;->b()Ljava/util/List;

    move-result-object v3

    .line 129
    aget-object v4, v2, v0

    invoke-interface {v4}, Lay/m;->d()Lay/t;

    move-result-object v4

    invoke-direct {p0, v4, v3}, Lay/b;->a(Lay/t;Ljava/util/List;)V

    .line 130
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_46

    .line 132
    :cond_64
    return v1
.end method

.method static synthetic c(Lay/b;)Z
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lay/b;->d:Z

    return v0
.end method

.method static synthetic d(Lay/b;)Z
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lay/b;->e:Z

    return v0
.end method

.method static synthetic e(Lay/b;)I
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lay/b;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 142
    new-instance v0, Lay/c;

    invoke-static {}, Lcom/google/googlenav/bL;->a()LZ/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lay/c;-><init>(Lay/b;LZ/c;)V

    invoke-virtual {v0}, Lay/c;->g()V

    .line 177
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 239
    new-instance v0, Lay/d;

    invoke-static {}, Lcom/google/googlenav/bL;->a()LZ/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lay/d;-><init>(Lay/b;LZ/c;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lay/d;->g()V

    .line 260
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 224
    iput-boolean p1, p0, Lay/b;->g:Z

    .line 225
    if-eqz p1, :cond_7

    .line 226
    invoke-virtual {p0}, Lay/b;->b()V

    .line 228
    :cond_7
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 180
    iget-object v1, p0, Lay/b;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 181
    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lay/b;->d:Z

    .line 182
    monitor-exit v1

    .line 183
    return-void

    .line 182
    :catchall_8
    move-exception v0

    monitor-exit v1
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_8

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 264
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 265
    invoke-static {}, Lay/l;->a()Lay/l;

    move-result-object v0

    .line 266
    iget-object v1, p0, Lay/b;->b:Lay/H;

    if-eqz v1, :cond_19

    invoke-virtual {v0, p1}, Lay/l;->a(Ljava/lang/String;)Lay/C;

    move-result-object v1

    if-nez v1, :cond_19

    .line 268
    iget-object v1, p0, Lay/b;->b:Lay/H;

    invoke-virtual {v1, p1}, Lay/H;->d(Ljava/lang/String;)V

    .line 270
    :cond_19
    invoke-virtual {v0, p1}, Lay/l;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 275
    :goto_1f
    return-void

    .line 274
    :cond_20
    invoke-virtual {p0}, Lay/b;->b()V

    goto :goto_1f
.end method
