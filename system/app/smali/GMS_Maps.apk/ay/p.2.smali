.class public Lay/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lay/B;
.implements Lay/m;
.implements Lcom/google/googlenav/g;


# instance fields
.field private a:Lay/n;

.field private final b:I

.field private final c:Lay/Q;

.field private d:Lad/h;

.field private final e:Ljava/util/List;

.field private f:Lcom/google/googlenav/ui/wizard/jt;

.field private g:Lay/t;

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jt;Lay/Q;Lad/h;ILcom/google/googlenav/common/io/protocol/ProtoBufType;Lay/H;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Lcom/google/common/collect/cx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lay/p;->e:Ljava/util/List;

    .line 68
    iput-boolean v1, p0, Lay/p;->h:Z

    .line 71
    iput-boolean v1, p0, Lay/p;->i:Z

    .line 78
    iput-boolean v1, p0, Lay/p;->j:Z

    .line 87
    iput-object p1, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    .line 88
    iput-object p2, p0, Lay/p;->c:Lay/Q;

    .line 89
    iput-object p3, p0, Lay/p;->d:Lad/h;

    .line 90
    iput p4, p0, Lay/p;->b:I

    .line 92
    iget-object v0, p0, Lay/p;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-direct {p0, p6, p5}, Lay/p;->a(Lay/H;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 94
    return-void
.end method

.method private a(Lay/H;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 108
    iget v0, p0, Lay/p;->b:I

    iget-object v1, p0, Lay/p;->c:Lay/Q;

    iget v2, p0, Lay/p;->b:I

    invoke-interface {v1, v2}, Lay/Q;->a(I)Lay/S;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lay/u;->a(ILay/S;Lay/H;)Lay/u;

    move-result-object v0

    iput-object v0, p0, Lay/p;->g:Lay/t;

    .line 110
    iget-object v0, p0, Lay/p;->g:Lay/t;

    invoke-interface {v0, p0}, Lay/t;->a(Lay/B;)V

    .line 111
    return-void
.end method

.method static synthetic a(Lay/p;)V
    .registers 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lay/p;->g()V

    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 327
    iput-boolean v1, p0, Lay/p;->i:Z

    .line 328
    iget-boolean v0, p0, Lay/p;->h:Z

    if-eqz v0, :cond_e

    .line 329
    iput-boolean v1, p0, Lay/p;->h:Z

    .line 330
    iget-object v0, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->e()V

    .line 332
    :cond_e
    iget-object v0, p0, Lay/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lat/h;

    .line 333
    invoke-interface {v0}, Lat/h;->D_()V

    goto :goto_14

    .line 335
    :cond_24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lay/p;->j:Z

    .line 336
    return-void
.end method


# virtual methods
.method public B_()V
    .registers 4

    .prologue
    .line 294
    iget-object v0, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    if-eqz v0, :cond_18

    .line 295
    const/4 v0, 0x1

    iput-boolean v0, p0, Lay/p;->h:Z

    .line 296
    iget-object v0, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    const/16 v1, 0x571

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1ae

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/googlenav/ui/wizard/jt;->a(Ljava/lang/String;Ljava/lang/String;Lat/g;)V

    .line 299
    :cond_18
    return-void
.end method

.method public D_()V
    .registers 4

    .prologue
    .line 308
    iget-boolean v0, p0, Lay/p;->i:Z

    if-eqz v0, :cond_5

    .line 324
    :goto_4
    return-void

    .line 312
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lay/p;->i:Z

    .line 313
    iget-object v0, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->z()Lcom/google/googlenav/android/Y;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 314
    const/4 v0, 0x0

    .line 315
    iget-object v1, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jt;->z()Lcom/google/googlenav/android/Y;

    move-result-object v1

    new-instance v2, Lay/r;

    invoke-direct {v2, p0}, Lay/r;-><init>(Lay/p;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/Y;->a(Ljava/lang/Runnable;Z)V

    goto :goto_4

    .line 322
    :cond_24
    invoke-direct {p0}, Lay/p;->g()V

    goto :goto_4
.end method

.method public E_()V
    .registers 3

    .prologue
    .line 340
    iget-boolean v0, p0, Lay/p;->h:Z

    if-eqz v0, :cond_c

    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lay/p;->h:Z

    .line 342
    iget-object v0, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->e()V

    .line 344
    :cond_c
    iget-object v0, p0, Lay/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lat/h;

    .line 345
    invoke-interface {v0}, Lat/h;->E_()V

    goto :goto_12

    .line 347
    :cond_22
    return-void
.end method

.method public F_()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 383
    invoke-virtual {p0}, Lay/p;->d()Lay/t;

    move-result-object v0

    .line 384
    instance-of v1, v0, Lay/L;

    if-nez v1, :cond_a

    .line 401
    :goto_9
    return-void

    .line 391
    :cond_a
    invoke-interface {v0}, Lay/t;->a()Ljava/util/List;

    move-result-object v0

    .line 392
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/s;

    .line 393
    check-cast v0, Lay/C;

    .line 394
    invoke-virtual {v0}, Lay/C;->g()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 395
    add-int/lit8 v0, v1, 0x1

    :goto_29
    move v1, v0

    .line 397
    goto :goto_13

    .line 398
    :cond_2b
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "v="

    aput-object v2, v0, v1

    invoke-static {v0}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    const/16 v1, 0x9

    const-string v2, "s"

    invoke-static {v1, v2, v0}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    :cond_54
    move v0, v1

    goto :goto_29
.end method

.method public L_()V
    .registers 3

    .prologue
    .line 363
    iget-object v0, p0, Lay/p;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lat/h;

    .line 364
    invoke-interface {v0}, Lat/h;->L_()V

    goto :goto_6

    .line 368
    :cond_16
    iget-boolean v0, p0, Lay/p;->j:Z

    if-eqz v0, :cond_24

    .line 369
    invoke-virtual {p0}, Lay/p;->d()Lay/t;

    move-result-object v0

    invoke-interface {v0}, Lay/t;->c()V

    .line 370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lay/p;->j:Z

    .line 372
    :cond_24
    return-void
.end method

.method public M_()V
    .registers 1

    .prologue
    .line 377
    return-void
.end method

.method public a(Lay/C;Lay/G;LaN/am;Ljava/lang/String;)Lay/C;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 247
    invoke-virtual {p0}, Lay/p;->d()Lay/t;

    move-result-object v0

    check-cast v0, Lay/L;

    .line 248
    invoke-virtual {p1}, Lay/C;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lay/L;->a(Ljava/lang/String;)Lay/s;

    move-result-object v1

    check-cast v1, Lay/C;

    .line 249
    if-eqz v1, :cond_29

    invoke-virtual {v1}, Lay/C;->g()Z

    move-result v2

    if-eqz v2, :cond_29

    .line 250
    invoke-virtual {v1, v3}, Lay/C;->a(Z)V

    .line 251
    invoke-virtual {v0, v1}, Lay/L;->a(Lay/s;)Z

    .line 252
    const-string v0, "d"

    invoke-virtual {v1}, Lay/C;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2, p4}, Lay/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :goto_28
    return-object v1

    .line 255
    :cond_29
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lay/C;->a(Z)V

    .line 256
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lay/C;->a(J)V

    .line 257
    invoke-virtual {v0, p1}, Lay/L;->a(Lay/s;)Z

    .line 258
    if-eqz p2, :cond_44

    .line 259
    invoke-virtual {v0, p2}, Lay/L;->a(Lay/G;)V

    .line 261
    :cond_44
    if-eqz p3, :cond_56

    .line 263
    invoke-virtual {p3}, LaN/am;->y()LaN/bx;

    move-result-object v0

    if-nez v0, :cond_4f

    .line 264
    invoke-virtual {p3, v3}, LaN/am;->a(Z)LaN/bx;

    .line 266
    :cond_4f
    invoke-virtual {p1}, Lay/C;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LaN/am;->f(Ljava/lang/String;)V

    .line 268
    :cond_56
    const-string v0, "c"

    invoke-virtual {p1}, Lay/C;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p4}, Lay/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    goto :goto_28
.end method

.method public a(Lay/N;)V
    .registers 4
    .parameter

    .prologue
    .line 126
    iget-object v0, p0, Lay/p;->c:Lay/Q;

    iget v1, p0, Lay/p;->b:I

    invoke-interface {v0, v1, p1}, Lay/Q;->a(ILay/N;)V

    .line 127
    return-void
.end method

.method public a(Lay/n;)V
    .registers 2
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, Lay/p;->a:Lay/n;

    .line 99
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lay/o;Lay/N;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lay/p;->c:Lay/Q;

    iget v1, p0, Lay/p;->b:I

    invoke-interface {v0, v1, p1, p2, p3}, Lay/Q;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lay/o;Lay/N;)V

    .line 277
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/jt;)V
    .registers 2
    .parameter

    .prologue
    .line 405
    iput-object p1, p0, Lay/p;->f:Lcom/google/googlenav/ui/wizard/jt;

    .line 406
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 131
    new-instance v0, Lcom/google/googlenav/f;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lay/p;->d:Lad/h;

    invoke-virtual {v1, v0}, Lad/h;->c(Lad/g;)V

    .line 133
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 137
    new-instance v0, Lay/q;

    invoke-static {}, Lcom/google/googlenav/bL;->a()LZ/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lay/q;-><init>(Lay/p;LZ/c;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0}, Lay/q;->g()V

    .line 143
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 281
    const-string v0, "p"

    invoke-virtual {p0, p1, p2, v0}, Lay/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 286
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "a="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LaU/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288
    const/16 v1, 0x9

    const-string v2, "f"

    invoke-static {v1, v2, v0}, LaU/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 290
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 206
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 241
    :cond_7
    :goto_7
    return-void

    .line 210
    :cond_8
    iget-object v0, p0, Lay/p;->g:Lay/t;

    invoke-interface {v0, p1}, Lay/t;->a(Ljava/lang/String;)Lay/s;

    move-result-object v0

    .line 211
    if-eqz v0, :cond_7

    instance-of v1, v0, Lay/C;

    if-eqz v1, :cond_7

    .line 216
    check-cast v0, Lay/C;

    .line 218
    iget-object v1, p0, Lay/p;->g:Lay/t;

    invoke-interface {v1, p1}, Lay/t;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 221
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/fz;->q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 222
    const/4 v2, 0x0

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 223
    const/16 v2, 0x90

    invoke-virtual {v1, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 224
    const/4 v2, 0x2

    invoke-virtual {v0}, Lay/C;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 225
    invoke-static {v1}, Lay/G;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/G;

    move-result-object v2

    .line 228
    invoke-virtual {v2, v4}, Lay/G;->a(I)V

    .line 230
    :try_start_3d
    invoke-virtual {v0}, Lay/C;->d()Lau/B;

    move-result-object v3

    if-eqz v3, :cond_53

    .line 231
    const/4 v3, 0x3

    invoke-virtual {v0}, Lay/C;->d()Lau/B;

    move-result-object v0

    invoke-static {v0}, Lau/C;->d(Lau/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 235
    :cond_53
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lay/G;->a(Z)V

    .line 236
    iget-object v0, p0, Lay/p;->g:Lay/t;

    invoke-interface {v0, v2}, Lay/t;->a(Lay/G;)V
    :try_end_5c
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_5c} :catch_5d

    goto :goto_7

    .line 237
    :catch_5d
    move-exception v0

    goto :goto_7
.end method

.method public b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x90

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 147
    if-nez p2, :cond_8

    move v0, v1

    .line 201
    :goto_7
    return v0

    .line 152
    :cond_8
    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-nez v0, :cond_10

    move v0, v1

    .line 153
    goto :goto_7

    .line 156
    :cond_10
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 159
    invoke-virtual {p2, v3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 162
    :cond_19
    invoke-virtual {p2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 165
    iget-object v0, p0, Lay/p;->g:Lay/t;

    invoke-interface {v0, v3}, Lay/t;->a(Ljava/lang/String;)Lay/s;

    move-result-object v0

    if-nez v0, :cond_27

    move v0, v2

    .line 166
    goto :goto_7

    .line 173
    :cond_27
    iget-object v0, p0, Lay/p;->g:Lay/t;

    invoke-interface {v0, v3}, Lay/t;->b(Ljava/lang/String;)Lay/G;

    move-result-object v4

    .line 175
    if-nez v4, :cond_5a

    .line 176
    new-instance v0, Lay/G;

    invoke-direct {v0}, Lay/G;-><init>()V

    .line 178
    invoke-virtual {v0, v3}, Lay/G;->a(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v0, v2}, Lay/G;->a(I)V

    .line 189
    :goto_3a
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v5, Lbs/a;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 190
    invoke-virtual {v4, v2, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 191
    invoke-virtual {v0, v4}, Lay/G;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 192
    invoke-virtual {v0, v1}, Lay/G;->a(Z)V

    .line 195
    iget-object v1, p0, Lay/p;->g:Lay/t;

    invoke-interface {v1, v0}, Lay/t;->a(Lay/G;)V

    .line 197
    iget-object v0, p0, Lay/p;->a:Lay/n;

    if-eqz v0, :cond_58

    .line 198
    iget-object v0, p0, Lay/p;->a:Lay/n;

    invoke-interface {v0, v3}, Lay/n;->a(Ljava/lang/String;)V

    :cond_58
    move v0, v2

    .line 201
    goto :goto_7

    .line 185
    :cond_5a
    new-instance v0, Lay/G;

    invoke-virtual {v4}, Lay/G;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v0, v4}, Lay/G;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_3a
.end method

.method public d()Lay/t;
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lay/p;->g:Lay/t;

    return-object v0
.end method

.method public t()Z
    .registers 2

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method
