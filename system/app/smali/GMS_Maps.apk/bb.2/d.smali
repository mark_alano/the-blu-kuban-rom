.class public abstract Lbb/d;
.super Lbb/f;
.source "SourceFile"


# instance fields
.field private volatile a:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 21
    invoke-direct {p0}, Lbb/f;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lbb/d;->a:I

    return-void
.end method

.method static synthetic a(Lbb/d;)I
    .registers 3
    .parameter

    .prologue
    .line 21
    iget v0, p0, Lbb/d;->a:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lbb/d;->a:I

    return v0
.end method


# virtual methods
.method public declared-synchronized L_()Z
    .registers 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, Lbb/f;->L_()Z

    move-result v0

    if-eqz v0, :cond_e

    iget v0, p0, Lbb/d;->a:I
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_10

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_c
    monitor-exit p0

    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_c

    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(Lbb/s;)Lbb/z;
.end method

.method protected a_(Lbb/s;)V
    .registers 8
    .parameter

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lbb/d;->b(Lbb/s;)Z

    move-result v4

    .line 43
    invoke-virtual {p0, p1}, Lbb/d;->c(Lbb/s;)Z

    move-result v5

    .line 44
    if-nez v4, :cond_d

    if-nez v5, :cond_d

    .line 75
    :goto_c
    return-void

    .line 48
    :cond_d
    iget v0, p0, Lbb/d;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbb/d;->a:I

    .line 49
    new-instance v0, Lbb/e;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lbb/e;-><init>(Lbb/d;Las/c;Lbb/s;ZZ)V

    invoke-virtual {v0}, Lbb/e;->g()V

    goto :goto_c
.end method
