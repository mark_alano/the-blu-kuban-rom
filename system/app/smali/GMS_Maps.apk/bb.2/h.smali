.class public Lbb/h;
.super Lbb/c;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Lbb/w;

.field private final c:LaR/n;


# direct methods
.method public constructor <init>(LaR/n;Lbb/w;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Lbb/c;-><init>()V

    .line 36
    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iput-object p1, p0, Lbb/h;->c:LaR/n;

    .line 38
    iput-object p2, p0, Lbb/h;->b:Lbb/w;

    .line 39
    iput p3, p0, Lbb/h;->a:I

    .line 40
    return-void
.end method

.method private static final a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 107
    const/16 v0, 0xb

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 102
    const-string v0, "m"

    return-object v0
.end method

.method public b(Lbb/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 92
    invoke-static {p1}, Lbb/h;->f(Lbb/s;)Z

    move-result v0

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 97
    iget v0, p0, Lbb/h;->a:I

    return v0
.end method

.method protected d(Lbb/s;)Lbb/z;
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x6

    .line 50
    new-instance v1, Lbb/z;

    invoke-direct {v1, p1}, Lbb/z;-><init>(Lbb/s;)V

    .line 52
    invoke-static {}, Lbb/o;->a()Lbb/o;

    .line 54
    iget-object v0, p0, Lbb/h;->c:LaR/n;

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v2

    .line 55
    invoke-interface {v2}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    .line 56
    invoke-virtual {p1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbb/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 57
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1f
    :goto_1f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 58
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, LaR/D;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1f

    .line 62
    invoke-virtual {v0}, LaR/D;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lbb/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 63
    invoke-virtual {v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1f

    .line 67
    invoke-static {v2, v0}, Lbb/i;->b(LaR/u;LaR/D;)Ljava/lang/String;

    move-result-object v5

    .line 68
    invoke-static {v2, v0}, Lbb/i;->a(LaR/u;LaR/D;)Ljava/lang/String;

    move-result-object v6

    .line 69
    new-instance v7, Lbb/y;

    invoke-direct {v7}, Lbb/y;-><init>()V

    invoke-virtual {v7, v5}, Lbb/y;->a(Ljava/lang/String;)Lbb/y;

    move-result-object v5

    invoke-virtual {v0}, LaR/D;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lbb/y;->b(Ljava/lang/String;)Lbb/y;

    move-result-object v5

    invoke-virtual {v5, v6}, Lbb/y;->c(Ljava/lang/String;)Lbb/y;

    move-result-object v5

    invoke-virtual {v5, v8}, Lbb/y;->a(I)Lbb/y;

    move-result-object v5

    iget v6, p0, Lbb/h;->a:I

    invoke-virtual {v5, v6}, Lbb/y;->b(I)Lbb/y;

    move-result-object v5

    invoke-virtual {p0}, Lbb/h;->j()I

    move-result v6

    invoke-virtual {v5, v6}, Lbb/y;->c(I)Lbb/y;

    move-result-object v5

    invoke-virtual {v0}, LaR/D;->i()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lbb/y;->a(J)Lbb/y;

    move-result-object v5

    invoke-virtual {v0}, LaR/D;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lbb/y;->d(Ljava/lang/String;)Lbb/y;

    move-result-object v5

    invoke-virtual {v0}, LaR/D;->d()LaN/B;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbb/y;->a(LaN/B;)Lbb/y;

    move-result-object v0

    invoke-virtual {p0, v8}, Lbb/h;->a(I)I

    move-result v5

    invoke-virtual {v0, v5}, Lbb/y;->d(I)Lbb/y;

    move-result-object v0

    const-string v5, "google.recentplace:"

    invoke-virtual {v0, v5}, Lbb/y;->e(Ljava/lang/String;)Lbb/y;

    move-result-object v0

    invoke-virtual {v0}, Lbb/y;->a()Lbb/w;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbb/z;->b(Lbb/w;)V

    goto :goto_1f

    .line 84
    :cond_9f
    invoke-virtual {v1}, Lbb/z;->d()I

    move-result v0

    if-lez v0, :cond_b0

    invoke-virtual {p1}, Lbb/s;->h()Z

    move-result v0

    if-eqz v0, :cond_b0

    .line 85
    iget-object v0, p0, Lbb/h;->b:Lbb/w;

    invoke-virtual {v1, v0}, Lbb/z;->a(Lbb/w;)V

    .line 87
    :cond_b0
    return-object v1
.end method

.method public d()[I
    .registers 4

    .prologue
    .line 112
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xb

    aput v2, v0, v1

    return-object v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method
