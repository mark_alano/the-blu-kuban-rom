.class public Lbb/j;
.super Lbb/f;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/LinkedList;

.field private b:Lbb/l;

.field private final c:Lbb/k;

.field private volatile d:I

.field private e:J

.field private final f:Lbb/w;


# direct methods
.method public constructor <init>(Lbb/w;)V
    .registers 4
    .parameter

    .prologue
    .line 93
    invoke-direct {p0}, Lbb/f;-><init>()V

    .line 72
    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lbb/j;->e:J

    .line 94
    iput-object p1, p0, Lbb/j;->f:Lbb/w;

    .line 95
    new-instance v0, Lbb/k;

    invoke-direct {v0, p0}, Lbb/k;-><init>(Lbb/j;)V

    iput-object v0, p0, Lbb/j;->c:Lbb/k;

    .line 96
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbb/j;->a:Ljava/util/LinkedList;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lbb/j;->d:I

    .line 98
    return-void
.end method

.method static synthetic a(Lbb/j;)Lbb/l;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lbb/j;->b:Lbb/l;

    return-object v0
.end method

.method static synthetic a(Lbb/j;Lbb/l;)Lbb/l;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lbb/j;->b:Lbb/l;

    return-object p1
.end method

.method static synthetic b(Lbb/j;)Ljava/util/LinkedList;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lbb/j;->a:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic c(Lbb/j;)J
    .registers 3
    .parameter

    .prologue
    .line 50
    iget-wide v0, p0, Lbb/j;->e:J

    return-wide v0
.end method

.method static synthetic d(Lbb/j;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lbb/j;->d:I

    return v0
.end method

.method static synthetic e(Lbb/j;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    invoke-super {p0}, Lbb/f;->j()I

    move-result v0

    return v0
.end method

.method static synthetic f(Lbb/j;)Lbb/w;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lbb/j;->f:Lbb/w;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized L_()Z
    .registers 2

    .prologue
    .line 113
    monitor-enter p0

    :try_start_1
    invoke-super {p0}, Lbb/f;->L_()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lbb/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lbb/j;->b:Lbb/l;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_18

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_14
    monitor-exit p0

    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_14

    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 154
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    const/4 v0, 0x2

    :goto_4
    return v0

    :cond_5
    const/4 v0, 0x1

    goto :goto_4
.end method

.method protected a_(Lbb/s;)V
    .registers 9
    .parameter

    .prologue
    .line 123
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    .line 124
    monitor-enter p0

    .line 127
    :try_start_5
    new-instance v0, Lbb/l;

    invoke-virtual {v1}, Lbb/o;->e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v1}, Lbb/o;->f()LaN/B;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1}, Lbb/o;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lbb/l;-><init>(Lbb/j;Lbb/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lbb/j;->b:Lbb/l;

    .line 129
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_5 .. :try_end_1c} :catchall_30

    .line 130
    iget-object v0, p0, Lbb/j;->c:Lbb/k;

    invoke-virtual {v0}, Lbb/k;->a()V

    .line 137
    invoke-static {p1}, Lbb/j;->f(Lbb/s;)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 140
    new-instance v0, Lbb/z;

    invoke-direct {v0, p1}, Lbb/z;-><init>(Lbb/s;)V

    invoke-virtual {p0, v0}, Lbb/j;->a(Lbb/z;)V

    .line 142
    :cond_2f
    return-void

    .line 129
    :catchall_30
    move-exception v0

    :try_start_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    throw v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 149
    const-string v0, "r"

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .registers 2
    .parameter

    .prologue
    .line 105
    iput p1, p0, Lbb/j;->d:I

    .line 106
    return-void
.end method

.method public d()[I
    .registers 2

    .prologue
    .line 161
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    return-object v0

    nop

    :array_8
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method
