.class public Lbb/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbb/j;

.field private b:Z

.field private c:Las/d;


# direct methods
.method protected constructor <init>(Lbb/j;)V
    .registers 3
    .parameter

    .prologue
    .line 191
    iput-object p1, p0, Lbb/k;->a:Lbb/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbb/k;->b:Z

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 202
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lbb/k;->b:Z

    if-nez v0, :cond_b

    .line 203
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbb/k;->b:Z

    .line 206
    invoke-virtual {p0}, Lbb/k;->run()V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 208
    :cond_b
    monitor-exit p0

    return-void

    .line 202
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .registers 4

    .prologue
    .line 213
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->a(Lbb/j;)Lbb/l;

    move-result-object v0

    if-eqz v0, :cond_48

    .line 214
    iget-object v0, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v0}, Lbb/j;->b(Lbb/j;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v1}, Lbb/j;->a(Lbb/j;)Lbb/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 215
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v1}, Lbb/j;->a(Lbb/j;)Lbb/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 217
    iget-object v0, p0, Lbb/k;->a:Lbb/j;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbb/j;->a(Lbb/j;Lbb/l;)Lbb/l;

    .line 220
    new-instance v0, Las/d;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Las/d;-><init>(Las/c;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lbb/k;->c:Las/d;

    .line 221
    iget-object v0, p0, Lbb/k;->c:Las/d;

    iget-object v1, p0, Lbb/k;->a:Lbb/j;

    invoke-static {v1}, Lbb/j;->c(Lbb/j;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    .line 222
    iget-object v0, p0, Lbb/k;->c:Las/d;

    invoke-virtual {v0}, Las/d;->g()V
    :try_end_46
    .catchall {:try_start_1 .. :try_end_46} :catchall_4c

    .line 227
    :goto_46
    monitor-exit p0

    return-void

    .line 225
    :cond_48
    const/4 v0, 0x0

    :try_start_49
    iput-boolean v0, p0, Lbb/k;->b:Z
    :try_end_4b
    .catchall {:try_start_49 .. :try_end_4b} :catchall_4c

    goto :goto_46

    .line 213
    :catchall_4c
    move-exception v0

    monitor-exit p0

    throw v0
.end method
