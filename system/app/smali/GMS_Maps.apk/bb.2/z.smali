.class public Lbb/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static a:I


# instance fields
.field private b:Lbb/s;

.field private c:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 24
    const/16 v0, 0x14

    sput v0, Lbb/z;->a:I

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 37
    const-string v0, ""

    invoke-static {v0}, Lbb/s;->a(Ljava/lang/String;)Lbb/s;

    move-result-object v0

    invoke-direct {p0, v0}, Lbb/z;-><init>(Lbb/s;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Lbb/s;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbb/z;-><init>(Lbb/s;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lbb/s;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget v0, Lbb/z;->a:I

    if-le p2, v0, :cond_9

    .line 57
    sget p2, Lbb/z;->a:I

    .line 59
    :cond_9
    iput-object p1, p0, Lbb/z;->b:Lbb/s;

    .line 60
    if-nez p2, :cond_15

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_12
    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    .line 62
    return-void

    .line 60
    :cond_15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_12
.end method

.method private a(Lcom/google/common/base/K;)Ljava/util/ArrayList;
    .registers 4
    .parameter

    .prologue
    .line 223
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-static {v1, p1}, Lcom/google/common/collect/S;->a(Ljava/util/Collection;Lcom/google/common/base/K;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private static final a(Lbb/w;Lbb/w;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0}, Lbb/w;->j()J

    move-result-wide v2

    .line 84
    invoke-virtual {p1}, Lbb/w;->j()J

    move-result-wide v4

    .line 85
    cmp-long v6, v2, v4

    if-lez v6, :cond_f

    .line 91
    :cond_e
    :goto_e
    return v0

    .line 88
    :cond_f
    cmp-long v2, v2, v4

    if-gez v2, :cond_15

    move v0, v1

    .line 89
    goto :goto_e

    .line 91
    :cond_15
    invoke-virtual {p0}, Lbb/w;->f()I

    move-result v2

    invoke-virtual {p1}, Lbb/w;->f()I

    move-result v3

    if-lt v2, v3, :cond_e

    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public a(I)Lbb/w;
    .registers 3
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 227
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 228
    return-void
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 213
    new-instance v0, Lbb/C;

    invoke-direct {v0, p0, p1, p2}, Lbb/C;-><init>(Lbb/z;J)V

    invoke-direct {p0, v0}, Lbb/z;->a(Lcom/google/common/base/K;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    .line 219
    return-void
.end method

.method public a(Lbb/s;)V
    .registers 2
    .parameter

    .prologue
    .line 235
    iput-object p1, p0, Lbb/z;->b:Lbb/s;

    .line 236
    return-void
.end method

.method public a(Lbb/w;)V
    .registers 4
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, Lbb/z;->a:I

    if-lt v0, v1, :cond_b

    .line 80
    :goto_a
    return-void

    .line 79
    :cond_b
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a
.end method

.method public a(Lbb/z;)V
    .registers 4
    .parameter

    .prologue
    .line 136
    iget-object v0, p1, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    .line 137
    invoke-virtual {p0, v0}, Lbb/z;->b(Lbb/w;)V

    goto :goto_6

    .line 139
    :cond_16
    return-void
.end method

.method public a(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 163
    new-instance v0, Lbb/B;

    invoke-direct {v0, p0, p1}, Lbb/B;-><init>(Lbb/z;Ljava/util/Set;)V

    invoke-direct {p0, v0}, Lbb/z;->a(Lcom/google/common/base/K;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    .line 169
    return-void
.end method

.method public b()Lbb/s;
    .registers 2

    .prologue
    .line 231
    iget-object v0, p0, Lbb/z;->b:Lbb/s;

    return-object v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 148
    new-instance v0, Lbb/A;

    invoke-direct {v0, p0, p1}, Lbb/A;-><init>(Lbb/z;I)V

    invoke-direct {p0, v0}, Lbb/z;->a(Lcom/google/common/base/K;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    .line 154
    return-void
.end method

.method public b(Lbb/w;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 102
    move v1, v0

    move v2, v0

    .line 104
    :goto_3
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3e

    .line 105
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    .line 106
    if-nez v1, :cond_28

    invoke-static {p1, v0}, Lbb/z;->a(Lbb/w;Lbb/w;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 107
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 108
    const/4 v1, 0x1

    .line 109
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    :goto_25
    move v2, v1

    move v1, v0

    .line 121
    goto :goto_3

    .line 110
    :cond_28
    invoke-virtual {p1, v0}, Lbb/w;->a(Lbb/w;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 114
    if-nez v1, :cond_31

    .line 128
    :cond_30
    return-void

    .line 117
    :cond_31
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    move v1, v2

    goto :goto_25

    .line 119
    :cond_39
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    goto :goto_25

    .line 122
    :cond_3e
    if-nez v1, :cond_43

    .line 123
    invoke-virtual {p0, p1}, Lbb/z;->a(Lbb/w;)V

    .line 125
    :cond_43
    :goto_43
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, Lbb/z;->a:I

    if-le v0, v1, :cond_30

    .line 126
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_43
.end method

.method public c()Lbb/z;
    .registers 4

    .prologue
    .line 241
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/z;

    .line 243
    iget-object v1, p0, Lbb/z;->b:Lbb/s;

    iput-object v1, v0, Lbb/z;->b:Lbb/s;

    .line 245
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lbb/z;->c:Ljava/util/ArrayList;

    .line 246
    iget-object v1, v0, Lbb/z;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_18
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_18} :catch_19

    .line 248
    return-object v0

    .line 249
    :catch_19
    move-exception v0

    .line 250
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Superclass does not support clone"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 19
    invoke-virtual {p0}, Lbb/z;->c()Lbb/z;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 258
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 267
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hb;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 268
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    .line 269
    const/4 v3, 0x2

    invoke-virtual {v0}, Lbb/w;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_d

    .line 272
    :cond_22
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 292
    instance-of v0, p1, Lbb/z;

    if-nez v0, :cond_6

    .line 293
    const/4 v0, 0x0

    .line 298
    :goto_5
    return v0

    .line 296
    :cond_6
    check-cast p1, Lbb/z;

    .line 298
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    iget-object v1, p1, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 307
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    const-string v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    .line 280
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lbb/w;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_10

    .line 282
    :cond_37
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
