.class public Lbe/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILandroid/view/View$OnClickListener;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lbe/d;->a:Ljava/lang/String;

    .line 36
    iput p2, p0, Lbe/d;->b:I

    .line 37
    iput-object p3, p0, Lbe/d;->c:Landroid/view/View$OnClickListener;

    .line 38
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 64
    const/4 v0, 0x2

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 4
    .parameter

    .prologue
    .line 49
    new-instance v1, Lbe/f;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbe/f;-><init>(Lbe/e;)V

    .line 50
    const v0, 0x7f100256

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/f;->a(Lbe/f;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 51
    const v0, 0x7f100255

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbe/f;->a(Lbe/f;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 52
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 42
    check-cast p2, Lbe/f;

    .line 43
    invoke-static {p2}, Lbe/f;->a(Lbe/f;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-static {p2}, Lbe/f;->b(Lbe/f;)Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p0, Lbe/d;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 45
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 69
    const v0, 0x7f0400b6

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public d()Landroid/view/View$OnClickListener;
    .registers 2

    .prologue
    .line 59
    iget-object v0, p0, Lbe/d;->c:Landroid/view/View$OnClickListener;

    return-object v0
.end method
