.class public Lbf/A;
.super Lbf/h;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/i;)V
    .registers 2
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    .line 32
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;
    .registers 11
    .parameter

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 44
    sget-object v0, Lbf/A;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100042

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 45
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 46
    sget-object v1, Lbf/A;->a:Landroid/view/ViewGroup;

    const v2, 0x7f100048

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 49
    iget-object v1, p0, Lbf/A;->c:Lbf/i;

    check-cast v1, Lbf/C;

    .line 50
    invoke-virtual {v1}, Lbf/C;->bu()Lcom/google/googlenav/ai;

    move-result-object v5

    .line 53
    const v2, 0x7f100043

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 55
    invoke-virtual {v1}, Lbf/C;->bH()Z

    move-result v3

    if-eqz v3, :cond_ba

    .line 56
    invoke-virtual {v1}, Lbf/C;->bG()Lam/f;

    move-result-object v3

    check-cast v3, Lan/f;

    .line 57
    invoke-virtual {v3}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 58
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 65
    :goto_46
    invoke-virtual {v1}, Lbf/C;->c()[Ljava/lang/String;

    move-result-object v3

    .line 67
    invoke-virtual {v1}, Lbf/C;->b()Z

    move-result v2

    if-eqz v2, :cond_be

    .line 69
    const/16 v2, 0x258

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 84
    :cond_56
    :goto_56
    invoke-virtual {v1}, Lbf/C;->f()Z

    move-result v3

    if-nez v3, :cond_d9

    .line 85
    invoke-virtual {v5}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v5

    .line 86
    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_127

    .line 87
    invoke-static {v5}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 92
    :goto_6a
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_124

    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7c

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_124

    :cond_7c
    :goto_7c
    move-object v5, v3

    move-object v3, v2

    move-object v2, v4

    .line 110
    :goto_7f
    const v6, 0x7f10001e

    invoke-static {v0, v6, v5}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 111
    const v5, 0x7f100044

    invoke-static {v0, v5, v3}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 112
    const v3, 0x7f100045

    invoke-static {v0, v3, v2}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 115
    const v2, 0x7f100046

    invoke-virtual {v1}, Lbf/C;->bI()Z

    move-result v3

    if-eqz v3, :cond_a6

    invoke-virtual {v1}, Lbf/C;->bK()Z

    move-result v1

    if-eqz v1, :cond_a6

    const/16 v1, 0x246

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    :cond_a6
    invoke-static {v0, v2, v4}, Lbj/G;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 121
    const v1, 0x7f100047

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 124
    sget-object v0, Lbf/A;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, p1}, Lbf/A;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0

    .line 60
    :cond_ba
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_46

    .line 70
    :cond_be
    if-eqz v3, :cond_d1

    array-length v2, v3

    if-lez v2, :cond_d1

    .line 72
    aget-object v2, v3, v8

    .line 73
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_56

    array-length v6, v3

    if-le v6, v7, :cond_56

    .line 74
    aget-object v2, v3, v7

    goto :goto_56

    .line 78
    :cond_d1
    const/16 v2, 0x5e6

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_56

    .line 99
    :cond_d9
    invoke-virtual {v1}, Lbf/C;->d()[Ljava/lang/String;

    move-result-object v5

    .line 100
    aget-object v2, v5, v8

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 101
    array-length v2, v5

    if-le v2, v7, :cond_122

    .line 102
    aget-object v2, v5, v7

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 104
    :goto_ec
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v6

    if-eqz v6, :cond_11e

    array-length v6, v5

    const/4 v7, 0x2

    if-le v6, v7, :cond_11e

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Context menu bubble contains "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " lines: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11e
    move-object v5, v3

    move-object v3, v4

    goto/16 :goto_7f

    :cond_122
    move-object v2, v4

    goto :goto_ec

    :cond_124
    move-object v2, v4

    goto/16 :goto_7c

    :cond_127
    move-object v3, v4

    goto/16 :goto_6a
.end method


# virtual methods
.method public b()Lcom/google/googlenav/ui/view/d;
    .registers 2

    .prologue
    .line 36
    iget-object v0, p0, Lbf/A;->c:Lbf/i;

    invoke-direct {p0, v0}, Lbf/A;->a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0
.end method
