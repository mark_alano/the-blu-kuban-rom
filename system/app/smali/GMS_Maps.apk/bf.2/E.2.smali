.class public Lbf/E;
.super Lbf/h;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/O;)V
    .registers 2
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    .line 27
    return-void
.end method

.method private c()Lax/b;
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, Lbf/E;->c:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lax/b;

    return-object v0
.end method


# virtual methods
.method public b()Lcom/google/googlenav/ui/view/d;
    .registers 11

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 31
    invoke-virtual {p0}, Lbf/E;->a()Landroid/view/View;

    move-result-object v2

    .line 32
    iget-object v0, p0, Lbf/E;->c:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v3

    .line 34
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 35
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 36
    invoke-direct {p0}, Lbf/E;->c()Lax/b;

    move-result-object v1

    .line 38
    iget-object v0, p0, Lbf/E;->c:Lbf/i;

    check-cast v0, Lbf/O;

    invoke-virtual {v0}, Lbf/O;->bp()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 41
    const/16 v0, 0x106

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 91
    :goto_3a
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Landroid/view/View;)V

    .line 92
    invoke-virtual {p0, v2, v7, v7}, Lbf/E;->a(Landroid/view/View;Lam/f;Lam/f;)V

    .line 93
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    .line 95
    sget-object v0, Lbf/E;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/E;->c:Lbf/i;

    invoke-virtual {p0, v0, v1}, Lbf/E;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0

    .line 43
    :cond_4c
    invoke-virtual {v1}, Lax/b;->m()Z

    move-result v0

    if-nez v0, :cond_7d

    .line 45
    invoke-direct {p0}, Lbf/E;->c()Lax/b;

    move-result-object v0

    invoke-static {v0, v3, v8}, Lbf/G;->a(Lax/b;IZ)Lbf/H;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lbf/H;->a()Z

    move-result v1

    if-eqz v1, :cond_6d

    .line 50
    iget-object v0, v0, Lbf/H;->a:Ljava/lang/String;

    invoke-static {v8}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_6d
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Ljava/util/List;)V

    .line 56
    invoke-virtual {p0, v2, v4}, Lbf/E;->b(Landroid/view/View;Ljava/util/List;)V

    .line 57
    invoke-virtual {p0, v2, v7}, Lbf/E;->c(Landroid/view/View;Ljava/util/List;)V

    .line 58
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Lam/f;)V

    .line 59
    invoke-virtual {p0, v2, v9}, Lbf/E;->a(Landroid/view/View;Z)V

    goto :goto_3a

    :cond_7d
    move-object v0, v1

    .line 63
    check-cast v0, Lax/w;

    .line 64
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v1

    .line 66
    invoke-virtual {v0, v3, v1, v8}, Lax/w;->a(ILcom/google/googlenav/ui/m;Z)Ljava/lang/String;

    move-result-object v1

    .line 69
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/googlenav/ui/bi;->f()Lam/g;

    move-result-object v6

    .line 72
    invoke-static {v0, v4, v5, v3, v8}, Lbf/G;->a(Lax/w;Ljava/util/Vector;Ljava/util/Vector;IZ)I

    .line 76
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Ljava/util/List;)V

    .line 77
    invoke-virtual {p0, v2, v4}, Lbf/E;->b(Landroid/view/View;Ljava/util/List;)V

    .line 78
    invoke-virtual {p0, v2, v5}, Lbf/E;->c(Landroid/view/View;Ljava/util/List;)V

    .line 80
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c0

    .line 81
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v8, :cond_bb

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 82
    :goto_b0
    invoke-interface {v6, v0}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lbf/E;->a(Landroid/view/View;Lam/f;)V

    .line 83
    invoke-virtual {p0, v2, v8}, Lbf/E;->a(Landroid/view/View;Z)V

    goto :goto_3a

    .line 81
    :cond_bb
    invoke-virtual {v1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_b0

    .line 85
    :cond_c0
    invoke-virtual {p0, v2, v7}, Lbf/E;->a(Landroid/view/View;Lam/f;)V

    .line 86
    invoke-virtual {p0, v2, v9}, Lbf/E;->a(Landroid/view/View;Z)V

    goto/16 :goto_3a
.end method
