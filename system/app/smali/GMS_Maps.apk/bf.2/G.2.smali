.class public Lbf/G;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lax/w;Ljava/util/Vector;Ljava/util/Vector;IZ)I
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 430
    .line 431
    invoke-virtual {p0, p3}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    .line 433
    invoke-virtual {p0, v0}, Lax/w;->a(Lax/m;)Ljava/lang/String;

    move-result-object v4

    .line 434
    if-eqz v4, :cond_48

    .line 435
    const/4 v2, 0x1

    .line 436
    if-eqz p4, :cond_45

    sget-object v1, Lcom/google/googlenav/ui/aV;->i:Lcom/google/googlenav/ui/aV;

    .line 438
    :goto_12
    invoke-static {v4, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move v1, v2

    .line 440
    :goto_1a
    invoke-static {p0, v0, v3}, Lbf/G;->a(Lax/b;Lax/m;Z)Lbf/H;

    move-result-object v0

    .line 443
    invoke-virtual {v0}, Lbf/H;->a()Z

    move-result v2

    if-eqz v2, :cond_31

    .line 444
    iget-object v2, v0, Lbf/H;->a:Ljava/lang/String;

    invoke-static {p4}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    .line 447
    :cond_31
    invoke-virtual {v0}, Lbf/H;->b()Z

    move-result v2

    if-eqz v2, :cond_44

    .line 448
    iget-object v0, v0, Lbf/H;->c:Ljava/lang/String;

    invoke-static {p4}, Lbf/G;->c(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    .line 451
    :cond_44
    return v1

    .line 436
    :cond_45
    sget-object v1, Lcom/google/googlenav/ui/aV;->F:Lcom/google/googlenav/ui/aV;

    goto :goto_12

    :cond_48
    move v1, v3

    goto :goto_1a
.end method

.method public static a(Lax/b;IZ)Lbf/H;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 481
    new-instance v1, Lbf/H;

    invoke-direct {v1}, Lbf/H;-><init>()V

    .line 482
    invoke-virtual {p0, p1}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    .line 484
    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v2

    .line 485
    if-nez v2, :cond_13

    move-object v0, v1

    .line 501
    :goto_12
    return-object v0

    .line 489
    :cond_13
    invoke-virtual {v0}, Lax/m;->s()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 490
    invoke-virtual {v2}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->a:Ljava/lang/String;

    .line 500
    :cond_1f
    :goto_1f
    invoke-virtual {v2}, Lax/t;->H()[Lax/n;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->b:[Lax/n;

    move-object v0, v1

    .line 501
    goto :goto_12

    .line 492
    :cond_27
    invoke-virtual {v2}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->a:Ljava/lang/String;

    .line 494
    invoke-virtual {v2}, Lax/t;->v()I

    move-result v0

    if-lez v0, :cond_1f

    if-nez p2, :cond_1f

    .line 495
    invoke-virtual {p0}, Lax/b;->av()I

    move-result v0

    .line 496
    invoke-virtual {v2}, Lax/t;->v()I

    move-result v3

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbf/H;->c:Ljava/lang/String;

    goto :goto_1f
.end method

.method private static a(Lax/b;Lax/m;)Lbf/H;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x2

    const/16 v7, 0xa

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 239
    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v0

    .line 240
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v2

    if-eqz v2, :cond_83

    .line 242
    invoke-virtual {v0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    invoke-virtual {v0}, Lax/t;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3d

    .line 244
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 245
    const/16 v2, 0xef

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lax/t;->u()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_3d
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 249
    invoke-static {v0}, Lbf/G;->b(Lax/t;)Ljava/lang/String;

    move-result-object v2

    .line 250
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6b

    .line 251
    const/16 v2, 0x58e

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    :cond_5f
    :goto_5f
    new-instance v0, Lbf/H;

    invoke-direct {v0}, Lbf/H;-><init>()V

    .line 278
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->a:Ljava/lang/String;

    .line 279
    return-object v0

    .line 255
    :cond_6b
    const/16 v3, 0x58f

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    aput-object v2, v4, v5

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5f

    .line 259
    :cond_83
    invoke-virtual {p1}, Lax/m;->p()Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 260
    invoke-static {v0}, Lbf/G;->a(Lax/t;)Ljava/lang/String;

    move-result-object v2

    .line 261
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_ab

    .line 262
    const/16 v3, 0x589

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    aput-object v2, v4, v5

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5f

    .line 266
    :cond_ab
    const/16 v2, 0x588

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    invoke-virtual {v0}, Lax/t;->v()I

    move-result v2

    if-lez v2, :cond_5f

    .line 271
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 272
    invoke-virtual {v0}, Lax/t;->v()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5f
.end method

.method public static a(Lax/b;Lax/m;Z)Lbf/H;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v3

    .line 75
    new-instance v2, Lbf/H;

    invoke-direct {v2}, Lbf/H;-><init>()V

    .line 76
    if-nez v3, :cond_f

    move-object v0, v2

    .line 126
    :cond_e
    :goto_e
    return-object v0

    .line 81
    :cond_f
    invoke-virtual {p1}, Lax/m;->r()Z

    move-result v4

    if-eqz v4, :cond_58

    invoke-virtual {p1}, Lax/m;->n()Z

    move-result v4

    if-eqz v4, :cond_58

    .line 82
    check-cast p1, Lax/a;

    .line 84
    const/16 v4, 0x607

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3}, Lax/t;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v4, v0}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbf/H;->a:Ljava/lang/String;

    .line 87
    invoke-virtual {p1}, Lax/a;->j()I

    move-result v0

    if-lez v0, :cond_43

    .line 88
    invoke-virtual {p1}, Lax/a;->j()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbf/H;->c:Ljava/lang/String;

    :cond_41
    :goto_41
    move-object v0, v2

    .line 94
    goto :goto_e

    .line 90
    :cond_43
    invoke-virtual {p1}, Lax/a;->i()I

    move-result v0

    if-lez v0, :cond_41

    .line 91
    invoke-virtual {p1}, Lax/a;->i()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbf/H;->c:Ljava/lang/String;

    goto :goto_41

    .line 115
    :cond_58
    invoke-static {p0}, Lbf/G;->a(Lax/b;)Z

    move-result v2

    if-eqz v2, :cond_9d

    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v2

    if-nez v2, :cond_6a

    invoke-virtual {p1}, Lax/m;->p()Z

    move-result v2

    if-eqz v2, :cond_9d

    .line 117
    :cond_6a
    invoke-static {p0, p1}, Lbf/G;->a(Lax/b;Lax/m;)Lbf/H;

    move-result-object v0

    .line 123
    :goto_6e
    invoke-virtual {p1}, Lax/m;->x()Z

    move-result v1

    if-eqz v1, :cond_e

    if-nez p2, :cond_e

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lax/m;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lbf/H;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->a:Ljava/lang/String;

    goto/16 :goto_e

    .line 119
    :cond_9d
    if-nez p2, :cond_a4

    :goto_9f
    invoke-static {p0, p1, v0}, Lbf/G;->b(Lax/b;Lax/m;Z)Lbf/H;

    move-result-object v0

    goto :goto_6e

    :cond_a4
    move v0, v1

    goto :goto_9f
.end method

.method public static a(Lax/b;IILcom/google/googlenav/ui/e;Z)Lbj/H;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 377
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 378
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 379
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 383
    invoke-static {p0, p1, v1}, Lbf/G;->a(Lax/b;IZ)Lbf/H;

    move-result-object v5

    .line 385
    invoke-virtual {v5}, Lbf/H;->a()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 386
    iget-object v0, v5, Lbf/H;->a:Ljava/lang/String;

    invoke-static {v1}, Lbf/G;->a(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    .line 389
    :cond_27
    invoke-virtual {v5}, Lbf/H;->c()Z

    move-result v0

    if-eqz v0, :cond_4f

    move v0, v1

    .line 390
    :goto_2e
    iget-object v6, v5, Lbf/H;->b:[Lax/n;

    array-length v6, v6

    if-ge v0, v6, :cond_4f

    .line 391
    iget-object v6, v5, Lbf/H;->b:[Lax/n;

    aget-object v6, v6, v0

    .line 392
    invoke-virtual {v6}, Lax/n;->c()Z

    move-result v7

    if-eqz v7, :cond_4c

    .line 393
    invoke-virtual {v6}, Lax/n;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Lbf/G;->b(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    .line 390
    :cond_4c
    add-int/lit8 v0, v0, 0x1

    goto :goto_2e

    .line 398
    :cond_4f
    invoke-virtual {v5}, Lbf/H;->b()Z

    move-result v0

    if-eqz v0, :cond_62

    .line 399
    iget-object v0, v5, Lbf/H;->c:Ljava/lang/String;

    invoke-static {v1}, Lbf/G;->c(Z)Lcom/google/googlenav/ui/aV;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Lcom/google/googlenav/ui/aW;)V

    .line 403
    :cond_62
    invoke-virtual {v4}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6b

    .line 404
    invoke-static {v3, v4}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    .line 406
    :cond_6b
    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_74

    .line 407
    invoke-static {v3, v2}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/util/Vector;)V

    .line 410
    :cond_74
    if-eqz p4, :cond_91

    const/16 v4, 0x5ec

    .line 412
    :goto_78
    if-eqz p4, :cond_94

    const/4 v5, -0x1

    .line 413
    :goto_7b
    invoke-virtual {p0, p1}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v2

    .line 414
    new-instance v0, Lbj/s;

    invoke-static {v3}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v1

    move v3, p2

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lbj/s;-><init>(Ljava/lang/CharSequence;Lax/t;IIII)V

    return-object v0

    .line 410
    :cond_91
    const/16 v4, 0xc8

    goto :goto_78

    .line 412
    :cond_94
    const/16 v5, 0xe7

    goto :goto_7b
.end method

.method public static a(Z)Lcom/google/googlenav/ui/aV;
    .registers 2
    .parameter

    .prologue
    .line 456
    if-eqz p0, :cond_5

    sget-object v0, Lcom/google/googlenav/ui/aV;->j:Lcom/google/googlenav/ui/aV;

    :goto_4
    return-object v0

    :cond_5
    sget-object v0, Lcom/google/googlenav/ui/aV;->G:Lcom/google/googlenav/ui/aV;

    goto :goto_4
.end method

.method private static a(Lax/t;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 283
    invoke-virtual {p0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    .line 285
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2c

    invoke-virtual {p0}, Lax/t;->w()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 286
    const/16 v0, 0x3b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lax/t;->x()I

    move-result v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    :cond_2c
    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 327
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_9

    .line 328
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    :cond_9
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    return-void
.end method

.method private static a(Lax/b;)Z
    .registers 2
    .parameter

    .prologue
    .line 343
    invoke-virtual {p0}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {}, Lcom/google/googlenav/common/Config;->w()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private static b(Lax/b;Lax/m;Z)Lbf/H;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 142
    invoke-virtual {p1}, Lax/m;->m()Lax/t;

    move-result-object v1

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v0

    if-nez v0, :cond_22

    invoke-virtual {p1}, Lax/m;->q()Z

    move-result v0

    if-nez v0, :cond_22

    invoke-virtual {p1}, Lax/m;->r()Z

    move-result v0

    if-eqz v0, :cond_89

    .line 147
    :cond_22
    invoke-virtual {p1}, Lax/m;->o()Z

    move-result v0

    if-eqz v0, :cond_d9

    .line 148
    invoke-virtual {p1}, Lax/m;->z()Z

    move-result v0

    if-eqz v0, :cond_d1

    const/16 v0, 0x593

    .line 150
    :goto_30
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->D()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v1}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_68

    .line 154
    invoke-virtual {p1}, Lax/m;->z()Z

    move-result v0

    if-eqz v0, :cond_d5

    const/16 v0, 0x591

    .line 156
    :goto_55
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_68
    :goto_68
    invoke-virtual {v1}, Lax/t;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_89

    .line 164
    const-string v0, "\n"

    const/16 v4, 0xef

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->u()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lbf/G;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_89
    invoke-virtual {p1}, Lax/m;->k()I

    move-result v0

    packed-switch v0, :pswitch_data_148

    .line 200
    if-eqz p2, :cond_b3

    .line 203
    invoke-virtual {p1}, Lax/m;->v()Z

    move-result v0

    if-nez v0, :cond_9e

    invoke-virtual {v1}, Lax/t;->w()Z

    move-result v0

    if-nez v0, :cond_134

    :cond_9e
    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    if-lez v0, :cond_134

    .line 205
    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_b3
    :goto_b3
    new-instance v0, Lbf/H;

    invoke-direct {v0}, Lbf/H;-><init>()V

    .line 216
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_c4

    .line 217
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->a:Ljava/lang/String;

    .line 219
    :cond_c4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_d0

    .line 220
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbf/H;->c:Ljava/lang/String;

    .line 222
    :cond_d0
    return-object v0

    .line 148
    :cond_d1
    const/16 v0, 0x595

    goto/16 :goto_30

    .line 154
    :cond_d5
    const/16 v0, 0x594

    goto/16 :goto_55

    .line 160
    :cond_d9
    invoke-virtual {v1}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_68

    .line 171
    :pswitch_e1
    invoke-static {v1}, Lbf/G;->c(Lax/t;)Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b3

    .line 173
    const-string v1, "\n"

    invoke-static {v3, v1, v0}, Lbf/G;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b3

    .line 180
    :pswitch_f1
    const/16 v0, 0x590

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v1}, Lax/t;->i()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    invoke-static {v1}, Lbf/G;->d(Lax/t;)Ljava/lang/String;

    move-result-object v0

    .line 185
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_114

    .line 186
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b3

    .line 187
    :cond_114
    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    if-lez v0, :cond_b3

    if-eqz p2, :cond_b3

    .line 188
    invoke-virtual {v1}, Lax/t;->v()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->av()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b3

    .line 195
    :pswitch_12c
    invoke-virtual {v1}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b3

    .line 207
    :cond_134
    invoke-virtual {v1}, Lax/t;->w()Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 208
    invoke-virtual {v1}, Lax/t;->x()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b3

    .line 168
    nop

    :pswitch_data_148
    .packed-switch 0x1
        :pswitch_e1
        :pswitch_f1
        :pswitch_12c
    .end packed-switch
.end method

.method public static b(Z)Lcom/google/googlenav/ui/aV;
    .registers 2
    .parameter

    .prologue
    .line 466
    if-eqz p0, :cond_5

    sget-object v0, Lcom/google/googlenav/ui/aV;->l:Lcom/google/googlenav/ui/aV;

    :goto_4
    return-object v0

    :cond_5
    sget-object v0, Lcom/google/googlenav/ui/aV;->I:Lcom/google/googlenav/ui/aV;

    goto :goto_4
.end method

.method private static b(Lax/t;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 294
    invoke-virtual {p0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    .line 296
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2c

    invoke-virtual {p0}, Lax/t;->s()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 298
    const/16 v0, 0xe8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lax/t;->t()I

    move-result v3

    invoke-static {v3}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    :cond_2c
    return-object v0
.end method

.method private static c(Z)Lcom/google/googlenav/ui/aV;
    .registers 2
    .parameter

    .prologue
    .line 461
    if-eqz p0, :cond_5

    sget-object v0, Lcom/google/googlenav/ui/aV;->k:Lcom/google/googlenav/ui/aV;

    :goto_4
    return-object v0

    :cond_5
    sget-object v0, Lcom/google/googlenav/ui/aV;->H:Lcom/google/googlenav/ui/aV;

    goto :goto_4
.end method

.method private static c(Lax/t;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 306
    invoke-virtual {p0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    .line 308
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 309
    const/16 v1, 0x58c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 317
    :cond_1e
    :goto_1e
    return-object v0

    .line 311
    :cond_1f
    invoke-virtual {p0}, Lax/t;->s()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 312
    const/16 v0, 0x58d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lax/t;->t()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1e
.end method

.method private static d(Lax/t;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 347
    invoke-virtual {p0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    .line 349
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 350
    const/16 v1, 0x58b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 358
    :cond_1e
    :goto_1e
    return-object v0

    .line 352
    :cond_1f
    invoke-virtual {p0}, Lax/t;->w()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 353
    const/16 v0, 0x58a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lax/t;->x()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1e
.end method
