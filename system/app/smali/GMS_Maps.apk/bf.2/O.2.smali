.class public Lbf/O;
.super Lbf/F;
.source "SourceFile"


# static fields
.field private static F:Z

.field private static G:Ljava/util/Vector;

.field public static final w:I


# instance fields
.field private final A:LaH/m;

.field private B:Ln/q;

.field private C:Lcom/google/googlenav/ui/view/dialog/r;

.field private D:Z

.field private E:Z

.field private H:Lcom/google/googlenav/ui/view/d;

.field private final I:Lbf/I;

.field private J:Ljava/util/List;

.field private K:Lcom/google/googlenav/ui/android/M;

.field v:Z

.field private x:I

.field private y:Ljava/lang/String;

.field private z:Lcom/google/googlenav/ui/view/android/C;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 160
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/O;->w:I

    .line 165
    const/4 v0, 0x0

    sput-boolean v0, Lbf/O;->F:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 207
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lbf/O;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/F;)V

    .line 208
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/F;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 195
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lbf/F;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 123
    iput-object v6, p0, Lbf/O;->y:Ljava/lang/String;

    .line 128
    iput-object v6, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    .line 135
    iput-object v6, p0, Lbf/O;->B:Ln/q;

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/O;->v:Z

    .line 196
    iput-object p5, p0, Lbf/O;->A:LaH/m;

    .line 197
    new-instance v0, Lbf/I;

    invoke-direct {v0, p0}, Lbf/I;-><init>(Lbf/O;)V

    iput-object v0, p0, Lbf/O;->I:Lbf/I;

    .line 198
    return-void
.end method

.method private a(Lax/t;)I
    .registers 5
    .parameter

    .prologue
    .line 902
    const v0, -0x57f0f0f1

    .line 904
    invoke-direct {p0}, Lbf/O;->bx()Ln/q;

    move-result-object v1

    .line 905
    if-eqz v1, :cond_18

    .line 906
    invoke-virtual {p1}, Lax/t;->h()Lo/D;

    move-result-object v2

    .line 907
    if-eqz v2, :cond_18

    invoke-virtual {v1, v2}, Ln/q;->d(Lo/D;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 908
    const v0, 0x300f0f0f

    .line 912
    :cond_18
    return v0
.end method

.method public static a(Lax/b;)Lax/b;
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 224
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lax/b;->aL()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_b
    move-object v0, v2

    .line 236
    :cond_c
    :goto_c
    return-object v0

    .line 227
    :cond_d
    const/4 v0, 0x0

    move v1, v0

    :goto_f
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_49

    .line 228
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/b;

    .line 229
    invoke-virtual {v0}, Lax/b;->k()I

    move-result v3

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v4

    if-ne v3, v4, :cond_45

    .line 230
    invoke-virtual {p0}, Lax/b;->aq()Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->aq()Lax/y;

    move-result-object v4

    invoke-virtual {v3, v4}, Lax/y;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_45

    invoke-virtual {p0}, Lax/b;->as()Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v4

    invoke-virtual {v3, v4}, Lax/y;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 227
    :cond_45
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    :cond_49
    move-object v0, v2

    .line 236
    goto :goto_c
.end method

.method private a(Lax/y;)Lax/y;
    .registers 8
    .parameter

    .prologue
    .line 1577
    invoke-virtual {p1}, Lax/y;->q()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1578
    invoke-virtual {p1}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, Lax/y;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lax/y;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lax/y;->l()Lo/D;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lax/y;->a(Ljava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object p1

    .line 1582
    :cond_22
    return-object p1
.end method

.method static synthetic a(Lbf/O;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lbf/O;->J:Ljava/util/List;

    return-object p1
.end method

.method private a(Lat/b;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 620
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    .line 621
    invoke-virtual {p0, p1}, Lbf/O;->b(Lat/b;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/m;->a(LaN/B;)V

    .line 624
    invoke-virtual {p0}, Lbf/O;->R()V

    .line 628
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->af()Lcom/google/googlenav/ui/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/p;->h()V

    .line 631
    invoke-virtual {p0}, Lbf/O;->al()V

    .line 632
    invoke-virtual {p0, p2}, Lbf/O;->b(I)V

    .line 633
    invoke-virtual {p0}, Lbf/O;->an()Z

    .line 634
    return-void
.end method

.method static synthetic a(Lbf/O;)V
    .registers 1
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Lbf/O;->bO()V

    return-void
.end method

.method static synthetic a(Lbf/O;Ljava/util/List;Ljava/util/List;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lbf/O;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/F;Z)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, -0x1

    const-wide v4, 0x3ff3333333333333L

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 366
    check-cast p1, Lax/b;

    .line 370
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->E()V

    .line 371
    sget-boolean v0, Lbf/O;->F:Z

    if-eqz v0, :cond_7f

    .line 374
    sput-boolean v1, Lbf/O;->F:Z

    .line 375
    invoke-virtual {p0, v1, v6}, Lbf/O;->b(ILjava/lang/Object;)V

    .line 444
    :goto_19
    invoke-virtual {p1}, Lax/b;->aP()Ljava/lang/String;

    move-result-object v0

    .line 445
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 446
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 450
    const-string v0, ""

    invoke-virtual {p1, v0}, Lax/b;->b(Ljava/lang/String;)V

    .line 453
    :cond_2d
    invoke-static {p1}, Lbf/O;->e(Lax/b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->y:Ljava/lang/String;

    .line 458
    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbf/O;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aN()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->y:Ljava/lang/String;

    .line 464
    :cond_52
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_5f

    .line 465
    invoke-direct {p0}, Lbf/O;->bP()V

    .line 467
    :cond_5f
    invoke-direct {p0}, Lbf/O;->bN()V

    .line 468
    sget-object v0, LaE/b;->a:LaE/b;

    invoke-virtual {v0}, LaE/b;->e()Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 469
    invoke-virtual {p0}, Lbf/O;->bt()V

    .line 470
    iput-object v6, p0, Lbf/O;->J:Ljava/util/List;

    .line 471
    invoke-virtual {p0}, Lbf/O;->bi()Z

    move-result v0

    if-nez v0, :cond_7b

    invoke-virtual {p0}, Lbf/O;->bj()Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 472
    :cond_7b
    invoke-virtual {p0}, Lbf/O;->br()V

    .line 475
    :cond_7e
    return-void

    .line 379
    :cond_7f
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_f7

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v0

    if-eq v0, v3, :cond_f7

    .line 381
    invoke-virtual {p0, v1, v6}, Lbf/O;->b(ILjava/lang/Object;)V

    .line 385
    invoke-virtual {p1}, Lax/b;->ah()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 386
    invoke-virtual {p1}, Lax/b;->ag()I

    move-result v1

    int-to-double v1, v1

    mul-double/2addr v1, v4

    double-to-int v1, v1

    .line 387
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p0, v3}, Lbf/O;->c(Z)I

    move-result v4

    invoke-virtual {p0}, Lbf/O;->q()I

    move-result v5

    invoke-virtual {v2, v0, v1, v4, v5}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v1

    .line 390
    invoke-virtual {p1}, Lax/b;->ai()LaN/B;

    move-result-object v0

    .line 391
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ar()Z

    move-result v2

    if-eqz v2, :cond_ce

    .line 393
    invoke-virtual {p0, v3}, Lbf/O;->c(Z)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 394
    invoke-virtual {p0}, Lbf/O;->q()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 395
    neg-int v2, v2

    neg-int v4, v4

    invoke-virtual {v0, v2, v4, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    .line 397
    :cond_ce
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v2, v0, v1}, LaN/u;->d(LaN/B;LaN/Y;)V

    .line 398
    invoke-virtual {p0, v7}, Lbf/O;->b(I)V

    .line 403
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_e9

    .line 404
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    .line 407
    :cond_e9
    iput-boolean v3, p0, Lbf/O;->u:Z

    .line 441
    :cond_eb
    :goto_eb
    invoke-static {p1}, Lbf/O;->b(Lax/b;)V

    .line 442
    invoke-virtual {p1}, Lax/b;->k()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ca;->b(I)V

    goto/16 :goto_19

    .line 412
    :cond_f7
    if-eqz p2, :cond_135

    move v0, v1

    .line 416
    :goto_fa
    iget-object v2, p0, Lbf/O;->c:LaN/p;

    invoke-virtual {p1, v2}, Lax/b;->a(LaN/p;)LaN/Y;

    move-result-object v4

    .line 424
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    const/16 v5, 0xe

    if-ge v2, v5, :cond_140

    move v2, v3

    .line 425
    :goto_10f
    if-eqz v4, :cond_128

    iget-object v5, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v5}, LaN/u;->d()LaN/Y;

    move-result-object v5

    invoke-virtual {v4, v5}, LaN/Y;->b(LaN/Y;)Z

    move-result v5

    if-nez v5, :cond_11f

    if-eqz v2, :cond_128

    .line 429
    :cond_11f
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p1}, Lax/b;->aw()LaN/B;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, LaN/u;->e(LaN/B;LaN/Y;)V

    .line 432
    :cond_128
    invoke-virtual {p1}, Lax/b;->ay()I

    move-result v2

    .line 433
    if-eq v2, v7, :cond_142

    .line 434
    invoke-virtual {p0, v1, v6}, Lbf/O;->b(ILjava/lang/Object;)V

    .line 435
    invoke-virtual {p0, v2, p2, v1}, Lbf/O;->a(IZZ)V

    goto :goto_eb

    .line 412
    :cond_135
    iget-object v0, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/O;->b(LaN/B;)I

    move-result v0

    goto :goto_fa

    :cond_140
    move v2, v1

    .line 424
    goto :goto_10f

    .line 436
    :cond_142
    if-ltz v0, :cond_eb

    .line 437
    invoke-virtual {p0, v0, p2, v3}, Lbf/O;->a(IZZ)V

    goto :goto_eb
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;Lax/b;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-static {p0, p1}, Lbf/O;->b(Lcom/google/googlenav/ui/s;Lax/b;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2149
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 2150
    add-int/lit16 v0, v1, 0x200

    .line 2151
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_10

    .line 2152
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 2154
    :cond_10
    new-instance v2, Lay/b;

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbf/Q;

    invoke-direct {v1, p0, p2, p1}, Lbf/Q;-><init>(Lbf/O;Ljava/util/List;Ljava/util/List;)V

    invoke-direct {v2, v0, v1}, Lay/b;-><init>(Ljava/util/List;Lay/c;)V

    .line 2178
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    .line 2179
    return-void
.end method

.method public static a(ILcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/J;Lax/b;Lcom/google/googlenav/ui/wizard/ca;Lcom/google/googlenav/ui/wizard/z;Z)Z
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x1d

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1077
    invoke-virtual {p3}, Lax/b;->ap()Lax/j;

    move-result-object v0

    .line 1079
    packed-switch p0, :pswitch_data_136

    move v0, v1

    .line 1190
    :goto_c
    return v0

    .line 1081
    :pswitch_d
    const-string v0, "ti"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1083
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/ah;->h()V

    move v0, v2

    .line 1084
    goto :goto_c

    .line 1087
    :pswitch_17
    const-string v1, "sd"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1089
    new-instance v1, Lax/s;

    invoke-direct {v1, v0}, Lax/s;-><init>(Lax/k;)V

    .line 1090
    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    .line 1091
    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 1092
    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    .line 1093
    goto :goto_c

    .line 1096
    :pswitch_30
    const-string v1, "sw"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1098
    new-instance v1, Lax/x;

    invoke-direct {v1, v0}, Lax/x;-><init>(Lax/k;)V

    .line 1099
    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    .line 1100
    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 1101
    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    .line 1102
    goto :goto_c

    .line 1105
    :pswitch_49
    const-string v1, "st"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1107
    new-instance v1, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    .line 1109
    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    .line 1110
    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 1111
    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    .line 1112
    goto :goto_c

    .line 1115
    :pswitch_6a
    const-string v1, "sk"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1117
    new-instance v1, Lax/i;

    invoke-direct {v1, v0}, Lax/i;-><init>(Lax/k;)V

    .line 1118
    invoke-virtual {p3}, Lax/b;->aN()I

    move-result v0

    invoke-virtual {v1, v0}, Lax/b;->v(I)V

    .line 1119
    invoke-virtual {p5}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 1120
    invoke-interface {p2, v1}, Lcom/google/googlenav/J;->a(Lax/k;)V

    move v0, v2

    .line 1121
    goto :goto_c

    .line 1124
    :pswitch_83
    const/4 v0, 0x0

    .line 1125
    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v3

    if-eqz v3, :cond_92

    .line 1126
    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->k()Ljava/lang/String;

    move-result-object v0

    .line 1128
    :cond_92
    instance-of v3, p3, Lax/s;

    if-eqz v3, :cond_ab

    .line 1129
    const-string v3, "dn"

    invoke-static {v3, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-virtual {p3}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "d"

    invoke-interface {p2, v0, v1, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    :cond_a8
    :goto_a8
    move v0, v2

    .line 1158
    goto/16 :goto_c

    .line 1134
    :cond_ab
    instance-of v1, p3, Lax/x;

    if-eqz v1, :cond_c3

    .line 1135
    const-string v1, "wn"

    invoke-static {v1, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p3}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "d"

    invoke-interface {p2, v0, v1, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    goto :goto_a8

    .line 1140
    :cond_c3
    instance-of v1, p3, Lax/i;

    if-eqz v1, :cond_db

    .line 1141
    const-string v1, "bn"

    invoke-static {v1, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    invoke-virtual {p3}, Lax/b;->as()Lax/y;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p3}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "d"

    invoke-interface {p2, v0, v1, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    goto :goto_a8

    .line 1146
    :cond_db
    instance-of v1, p3, Lax/w;

    if-eqz v1, :cond_a8

    .line 1147
    const-string v1, "tn"

    invoke-static {v1, v0}, Lbf/O;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    const/16 v0, 0x61

    const-string v1, "n"

    const-string v3, "h"

    invoke-static {v0, v1, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1153
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0, v2}, LaS/a;->a(Z)V

    move-object v0, p2

    .line 1154
    check-cast v0, Lcom/google/googlenav/ui/s;

    move-object v1, p3

    check-cast v1, Lax/w;

    invoke-static {v0, v1}, Lbf/O;->a(Lcom/google/googlenav/ui/s;Lax/w;)Z

    move-result v0

    if-nez v0, :cond_a8

    .line 1155
    check-cast p3, Lax/w;

    invoke-interface {p2, p3}, Lcom/google/googlenav/J;->a(Lax/w;)V

    goto :goto_a8

    .line 1161
    :pswitch_106
    const-string v0, "es"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1167
    if-eqz p6, :cond_118

    .line 1168
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/ah;->dismiss()V

    .line 1169
    new-instance v0, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v0, v3, p1}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p5, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1172
    :cond_118
    invoke-virtual {p4, p3, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;Z)V

    move v0, v2

    .line 1173
    goto/16 :goto_c

    .line 1176
    :pswitch_11e
    const-string v0, "ee"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1182
    if-eqz p6, :cond_130

    .line 1183
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/android/ah;->dismiss()V

    .line 1184
    new-instance v0, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v0, v3, p1}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p5, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1187
    :cond_130
    invoke-virtual {p4, p3, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;Z)V

    move v0, v2

    .line 1188
    goto/16 :goto_c

    .line 1079
    :pswitch_data_136
    .packed-switch 0xe8
        :pswitch_17
        :pswitch_49
        :pswitch_30
        :pswitch_6a
        :pswitch_83
        :pswitch_d
        :pswitch_106
        :pswitch_11e
    .end packed-switch
.end method

.method private a(Lcom/google/googlenav/E;Lcom/google/googlenav/E;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 610
    iget-object v0, p0, Lbf/O;->c:LaN/p;

    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/l;->a(LaN/B;LaN/B;)I

    move-result v1

    invoke-virtual {v0, v1}, LaN/p;->c(I)I

    move-result v0

    sget v1, Lbf/O;->w:I

    if-ge v0, v1, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public static a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1451
    instance-of v2, p0, Lcom/google/googlenav/ui/s;

    if-eqz v2, :cond_11

    .line 1452
    check-cast p0, Lcom/google/googlenav/ui/s;

    .line 1456
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    sparse-switch v2, :sswitch_data_4c

    move v0, v1

    .line 1486
    :goto_10
    return v0

    :cond_11
    move v0, v1

    .line 1454
    goto :goto_10

    .line 1459
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/Object;)V

    goto :goto_10

    .line 1463
    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/Object;)V

    goto :goto_10

    .line 1467
    :sswitch_23
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->c(Ljava/lang/Object;)V

    goto :goto_10

    .line 1471
    :sswitch_2b
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->d(Ljava/lang/Object;)V

    goto :goto_10

    .line 1475
    :sswitch_33
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->e(Ljava/lang/Object;)V

    goto :goto_10

    .line 1479
    :sswitch_3b
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/Object;B)V

    goto :goto_10

    .line 1483
    :sswitch_44
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/Object;B)V

    goto :goto_10

    .line 1456
    :sswitch_data_4c
    .sparse-switch
        0x7 -> :sswitch_1b
        0x8 -> :sswitch_13
        0x18 -> :sswitch_23
        0x19 -> :sswitch_2b
        0x1a -> :sswitch_33
        0x1f -> :sswitch_3b
        0x20 -> :sswitch_44
    .end sparse-switch
.end method

.method private static a(Lcom/google/googlenav/ui/s;Lax/w;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2017
    invoke-virtual {p1}, Lax/w;->aX()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-virtual {p1}, Lax/w;->aV()J

    move-result-wide v0

    invoke-static {p1}, Lcom/google/googlenav/ui/bd;->a(Lax/b;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/ui/bd;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 2020
    const/16 v0, 0x61

    const-string v1, "sr"

    const-string v2, "s"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2023
    invoke-static {p0, p1}, Lbf/O;->b(Lcom/google/googlenav/ui/s;Lax/w;)V

    .line 2024
    const/4 v0, 0x1

    .line 2026
    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public static b(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    .line 245
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    if-nez v0, :cond_b

    .line 246
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lbf/O;->G:Ljava/util/Vector;

    .line 248
    :cond_b
    const/4 v0, 0x0

    move v1, v0

    :goto_d
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_31

    .line 249
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lax/b;

    .line 250
    invoke-virtual {v0}, Lax/b;->k()I

    move-result v0

    invoke-virtual {p0}, Lax/b;->k()I

    move-result v2

    if-ne v0, v2, :cond_2d

    .line 251
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, p0, v1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 256
    :goto_2c
    return-void

    .line 248
    :cond_2d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 255
    :cond_31
    sget-object v0, Lbf/O;->G:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_2c
.end method

.method private static b(Lcom/google/googlenav/ui/s;Lax/b;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1394
    new-instance v0, Lax/j;

    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-virtual {p1}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1398
    new-instance v1, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    .line 1400
    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v0

    invoke-virtual {v1, v0}, Lax/b;->a(Lax/l;)V

    .line 1401
    invoke-virtual {v1}, Lax/b;->aM()V

    .line 1402
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    .line 1403
    return-void
.end method

.method private static b(Lcom/google/googlenav/ui/s;Lax/w;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 2039
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x4c2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x4c1

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x4c0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x4bf

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v7, Lbf/P;

    invoke-direct {v7, p0, p1}, Lbf/P;-><init>(Lcom/google/googlenav/ui/s;Lax/w;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    .line 2074
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1642
    const/4 v0, 0x4

    const-string v1, "da"

    invoke-static {v0, v1, p0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1644
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1648
    const/4 v1, 0x4

    const-string v2, "da"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p0, v3, v0

    const/4 v4, 0x1

    if-eqz p1, :cond_12

    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    :cond_12
    const/4 v0, 0x0

    :goto_13
    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1654
    return-void

    .line 1648
    :cond_1d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_13
.end method

.method private bA()[Lcom/google/googlenav/ui/aH;
    .registers 5

    .prologue
    .line 956
    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v0

    .line 957
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aH;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/aF;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/aF;-><init>(Lax/b;)V

    aput-object v3, v1, v2

    return-object v1
.end method

.method private bB()I
    .registers 2

    .prologue
    .line 1013
    iget v0, p0, Lbf/O;->x:I

    return v0
.end method

.method private bC()Lax/s;
    .registers 2

    .prologue
    .line 1049
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/s;

    return-object v0
.end method

.method private bD()Lax/x;
    .registers 2

    .prologue
    .line 1058
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/x;

    return-object v0
.end method

.method private bE()Lax/i;
    .registers 2

    .prologue
    .line 1062
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/i;

    return-object v0
.end method

.method private bF()V
    .registers 3

    .prologue
    .line 1432
    const-string v0, "va"

    invoke-static {v0}, Lbf/O;->c(Ljava/lang/String;)V

    .line 1434
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->h(Lax/b;)V

    .line 1435
    return-void
.end method

.method private bG()V
    .registers 5

    .prologue
    .line 1587
    const-string v0, "r"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1589
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    .line 1594
    new-instance v1, Lax/j;

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-direct {p0, v2}, Lbf/O;->a(Lax/y;)Lax/y;

    move-result-object v2

    invoke-virtual {v0}, Lax/b;->aq()Lax/y;

    move-result-object v3

    invoke-direct {p0, v3}, Lbf/O;->a(Lax/y;)Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1598
    invoke-virtual {p0, v1}, Lbf/O;->b(Lax/j;)V

    .line 1599
    return-void
.end method

.method private bH()V
    .registers 5

    .prologue
    .line 1633
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    .line 1634
    iget-object v1, p0, Lbf/O;->A:LaH/m;

    invoke-interface {v1}, LaH/m;->s()LaH/h;

    move-result-object v1

    .line 1635
    new-instance v2, Lax/j;

    invoke-virtual {v1}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v1}, LaH/h;->b()Lo/D;

    move-result-object v1

    invoke-static {v3, v1}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v1

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v3

    invoke-virtual {v0}, Lax/b;->au()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v2, v1, v3, v0}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1638
    invoke-virtual {p0, v2}, Lbf/O;->b(Lax/j;)V

    .line 1639
    return-void
.end method

.method private bI()V
    .registers 3

    .prologue
    .line 1662
    const-string v0, "av"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1663
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->g(Lax/b;)V

    .line 1664
    return-void
.end method

.method private bJ()Z
    .registers 2

    .prologue
    .line 1926
    iget-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private bK()V
    .registers 3

    .prologue
    .line 1949
    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1950
    invoke-direct {p0}, Lbf/O;->bL()V

    .line 1953
    :cond_9
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    const/16 v1, 0xfa

    invoke-static {v0, v1, p0}, Lcom/google/googlenav/ui/f;->a(Lax/b;ILcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/C;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    .line 1955
    iget-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/C;->show()V

    .line 1956
    return-void
.end method

.method private bL()V
    .registers 2

    .prologue
    .line 1959
    iget-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/C;->dismiss()V

    .line 1960
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    .line 1961
    return-void
.end method

.method private bM()V
    .registers 3

    .prologue
    .line 1964
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->aI()Lax/b;

    move-result-object v0

    .line 1965
    invoke-virtual {v0}, Lax/b;->aM()V

    .line 1966
    iget-object v1, p0, Lbf/O;->z:Lcom/google/googlenav/ui/view/android/C;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/C;->h()Lcom/google/googlenav/ui/view/q;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/f;->a(Lax/b;Lcom/google/googlenav/ui/view/q;)V

    .line 1968
    invoke-direct {p0}, Lbf/O;->bL()V

    .line 1972
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    .line 1973
    return-void
.end method

.method private bN()V
    .registers 6

    .prologue
    const/4 v4, -0x1

    .line 2077
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 2080
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2082
    const v1, 0x7f040063

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2083
    const v1, 0x7f10017b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    const/16 v3, 0xf4

    invoke-static {v1, v2, p0, v3}, Lcom/google/googlenav/ui/view/android/x;->a(Landroid/view/View;Lax/b;Lcom/google/googlenav/ui/e;I)V

    .line 2086
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x13

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 2091
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->d:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2113
    :cond_46
    :goto_46
    return-void

    .line 2096
    :cond_47
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_46

    .line 2100
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-nez v0, :cond_46

    .line 2104
    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_7f

    .line 2107
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v0

    if-nez v0, :cond_7d

    invoke-virtual {p0}, Lbf/O;->bk()Z

    move-result v0

    if-eqz v0, :cond_7d

    const/4 v0, 0x1

    :goto_76
    invoke-virtual {v1, v2, p0, v0}, Lcom/google/googlenav/ui/view/e;->a(Lax/b;Lcom/google/googlenav/ui/e;Z)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    goto :goto_46

    :cond_7d
    const/4 v0, 0x0

    goto :goto_76

    .line 2111
    :cond_7f
    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->b()V

    goto :goto_46
.end method

.method private bO()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 2185
    iget-object v0, p0, Lbf/O;->J:Ljava/util/List;

    invoke-static {v0}, Lay/d;->e(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2186
    invoke-static {v0}, Lay/d;->f(Ljava/util/List;)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, v5}, Lcom/google/googlenav/ui/l;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 2188
    iget-object v1, p0, Lbf/O;->J:Ljava/util/List;

    invoke-static {v1}, Lay/d;->c(Ljava/util/List;)Lay/a;

    move-result-object v1

    .line 2189
    invoke-virtual {v1}, Lay/a;->b()D

    move-result-wide v1

    double-to-int v1, v1

    invoke-static {v1, v5}, Lcom/google/googlenav/ui/l;->c(II)Ljava/lang/String;

    move-result-object v1

    .line 2191
    iget-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    if-nez v2, :cond_35

    .line 2192
    new-instance v2, Lcom/google/googlenav/ui/android/M;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v3

    const v4, 0x7f100060

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/ui/android/M;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;I)V

    iput-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    .line 2197
    :cond_35
    iget-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    iget-object v3, p0, Lbf/O;->J:Ljava/util/List;

    invoke-static {v3}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/google/googlenav/ui/android/M;->a(Ljava/util/List;Z)V

    .line 2200
    iget-object v2, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    const/16 v3, 0xf2

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/android/M;->a(Ljava/lang/CharSequence;)V

    .line 2202
    return-void
.end method

.method private bP()V
    .registers 3

    .prologue
    .line 2222
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2223
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->j()V

    .line 2227
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/actionbar/ActionBarControllerSdk14;->b:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2235
    :cond_1a
    :goto_1a
    return-void

    .line 2231
    :cond_1b
    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1a

    .line 2232
    iget-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 2233
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->H:Lcom/google/googlenav/ui/view/d;

    goto :goto_1a
.end method

.method public static bq()Z
    .registers 1

    .prologue
    .line 1992
    const/4 v0, 0x1

    return v0
.end method

.method private bu()Z
    .registers 14

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 742
    invoke-direct {p0}, Lbf/O;->bv()Z

    move-result v1

    if-nez v1, :cond_a

    .line 786
    :goto_9
    return v0

    .line 747
    :cond_a
    invoke-static {}, Lba/c;->b()V

    .line 751
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->ad()[LaN/B;

    move-result-object v10

    .line 753
    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v11

    .line 754
    new-array v12, v11, [Lba/f;

    move v9, v0

    .line 755
    :goto_1e
    if-ge v9, v11, :cond_65

    .line 756
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v9}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lax/m;

    .line 765
    invoke-virtual {v2}, Lax/m;->l()I

    move-result v0

    .line 766
    add-int/lit8 v1, v0, 0x1

    array-length v3, v10

    if-ge v1, v3, :cond_74

    .line 768
    invoke-virtual {v2}, Lax/m;->l()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    aget-object v6, v10, v1

    .line 771
    :goto_3a
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_72

    .line 773
    invoke-virtual {v2}, Lax/m;->l()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    aget-object v5, v10, v0

    .line 776
    :goto_46
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-static {v0, v2, v8}, Lbf/G;->a(Lax/b;Lax/m;Z)Lbf/H;

    move-result-object v0

    iget-object v3, v0, Lbf/H;->a:Ljava/lang/String;

    .line 779
    new-instance v0, Lba/f;

    invoke-virtual {v2}, Lax/m;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v2}, Lax/m;->t()Ljava/lang/String;

    move-result-object v2

    const-string v4, ""

    invoke-direct/range {v0 .. v6}, Lba/f;-><init>(LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;LaN/B;)V

    aput-object v0, v12, v9

    .line 755
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1e

    .line 785
    :cond_65
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-virtual {v0, v1, v12}, Lcom/google/googlenav/ui/s;->a(I[Lba/f;)V

    move v0, v8

    .line 786
    goto :goto_9

    :cond_72
    move-object v5, v7

    goto :goto_46

    :cond_74
    move-object v6, v7

    goto :goto_3a
.end method

.method private bv()Z
    .registers 2

    .prologue
    .line 829
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_e

    .line 830
    :cond_c
    const/4 v0, 0x0

    .line 834
    :goto_d
    return v0

    .line 832
    :cond_e
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    .line 834
    invoke-virtual {v0}, Lax/m;->u()Z

    move-result v0

    goto :goto_d
.end method

.method private bw()[Lcom/google/googlenav/ui/aH;
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 863
    invoke-virtual {p0}, Lbf/O;->f()Lax/w;

    move-result-object v3

    .line 864
    if-nez v3, :cond_8

    .line 886
    :cond_7
    :goto_7
    return-object v0

    .line 867
    :cond_8
    invoke-virtual {v3}, Lax/w;->ae()I

    move-result v4

    .line 874
    const/4 v1, 0x1

    if-lt v4, v1, :cond_7

    .line 877
    add-int/lit8 v0, v4, -0x1

    new-array v1, v0, [Lcom/google/googlenav/ui/aH;

    .line 878
    const/4 v0, 0x0

    move v2, v0

    :goto_15
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_3d

    .line 879
    new-instance v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->b(Z)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    invoke-direct {v0, v3, v5, v2, v6}, Lcom/google/googlenav/ui/aJ;-><init>(Lax/w;III)V

    aput-object v0, v1, v2

    .line 882
    aget-object v0, v1, v2

    check-cast v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->a(Z)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/aJ;->a(I)V

    .line 878
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_15

    :cond_3d
    move-object v0, v1

    .line 886
    goto :goto_7
.end method

.method private bx()Ln/q;
    .registers 2

    .prologue
    .line 895
    iget-object v0, p0, Lbf/O;->B:Ln/q;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbf/O;->B:Ln/q;

    :goto_6
    return-object v0

    :cond_7
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v0

    goto :goto_6
.end method

.method private by()[Lcom/google/googlenav/ui/aH;
    .registers 8

    .prologue
    .line 922
    invoke-direct {p0}, Lbf/O;->bD()Lax/x;

    move-result-object v2

    .line 924
    invoke-virtual {v2}, Lax/x;->af()Ljava/util/List;

    move-result-object v3

    .line 925
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/googlenav/ui/aF;

    .line 927
    const/4 v0, 0x0

    move v1, v0

    :goto_10
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_55

    .line 928
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 931
    add-int/lit8 v0, v1, 0x1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_4e

    .line 932
    add-int/lit8 v0, v1, 0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 937
    :goto_34
    new-instance v6, Lcom/google/googlenav/ui/aF;

    invoke-direct {v6, v2, v5, v0}, Lcom/google/googlenav/ui/aF;-><init>(Lax/b;II)V

    .line 940
    invoke-virtual {v2, v5}, Lax/x;->n(I)Lax/t;

    move-result-object v0

    invoke-direct {p0, v0}, Lbf/O;->a(Lax/t;)I

    move-result v0

    .line 941
    const/4 v5, 0x1

    invoke-virtual {v6, v5}, Lcom/google/googlenav/ui/aF;->a(I)V

    .line 942
    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/aF;->b(I)V

    .line 944
    aput-object v6, v4, v1

    .line 927
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 934
    :cond_4e
    invoke-virtual {v2}, Lax/x;->ae()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_34

    .line 947
    :cond_55
    return-object v4
.end method

.method private bz()[Lcom/google/googlenav/ui/aH;
    .registers 5

    .prologue
    .line 951
    invoke-direct {p0}, Lbf/O;->bE()Lax/i;

    move-result-object v0

    .line 952
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aH;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/aF;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/aF;-><init>(Lax/b;)V

    aput-object v3, v1, v2

    return-object v1
.end method

.method public static c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1657
    const/4 v0, 0x4

    const-string v1, "al"

    invoke-static {v0, v1, p0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1659
    return-void
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 797
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    .line 800
    if-eqz v1, :cond_3a

    .line 801
    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    .line 802
    if-eqz v1, :cond_3a

    .line 803
    invoke-virtual {v1}, Lax/y;->k()Ljava/lang/String;

    move-result-object v1

    .line 808
    :goto_11
    if-eqz v1, :cond_2c

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2c

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 812
    :cond_2c
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3a
    move-object v1, v0

    goto :goto_11
.end method

.method private static e(Lax/b;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 1035
    instance-of v0, p0, Lax/w;

    if-eqz v0, :cond_2e

    .line 1036
    check-cast p0, Lax/w;

    .line 1037
    const/16 v0, 0x5ce

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x5d4

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lax/w;->az()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1045
    :goto_2d
    return-object v0

    .line 1040
    :cond_2e
    instance-of v0, p0, Lax/x;

    if-eqz v0, :cond_39

    .line 1041
    const/16 v0, 0x606

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2d

    .line 1042
    :cond_39
    instance-of v0, p0, Lax/i;

    if-eqz v0, :cond_44

    .line 1043
    const/16 v0, 0x5a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2d

    .line 1045
    :cond_44
    const/16 v0, 0x108

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2d
.end method

.method private e(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1976
    const-string v0, ""

    const-string v1, ""

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->k()I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/ui/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1977
    return-void
.end method

.method private g(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 517
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x38

    if-ne v1, v2, :cond_a

    .line 523
    :cond_9
    :goto_9
    return v0

    .line 520
    :cond_a
    invoke-virtual {p0}, Lbf/O;->ag()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_9

    .line 523
    :cond_17
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private h(I)V
    .registers 6
    .parameter

    .prologue
    .line 822
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->o()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    const-string v3, "d"

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/googlenav/ui/s;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    .line 825
    return-void
.end method

.method private i(I)V
    .registers 4
    .parameter

    .prologue
    .line 838
    const-string v0, "m"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/O;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    if-ltz p1, :cond_28

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge p1, v0, :cond_28

    .line 840
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lbf/O;->a(IZZ)V

    .line 844
    :goto_18
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_27

    .line 845
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/O;->b(ILjava/lang/Object;)V

    .line 847
    :cond_27
    return-void

    .line 842
    :cond_28
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/O;->b(I)V

    goto :goto_18
.end method

.method private j(I)V
    .registers 3
    .parameter

    .prologue
    .line 1412
    iget-object v0, p0, Lbf/O;->I:Lbf/I;

    invoke-virtual {v0, p1}, Lbf/I;->a(I)Lcom/google/googlenav/ui/view/dialog/r;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    .line 1413
    iget-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/r;->show()V

    .line 1414
    return-void
.end method


# virtual methods
.method protected A()V
    .registers 3

    .prologue
    .line 2250
    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_14

    .line 2251
    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 2252
    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/ah;

    invoke-virtual {p0}, Lbf/O;->G()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/ah;->a(I)V

    .line 2254
    :cond_14
    return-void
.end method

.method protected B()V
    .registers 3

    .prologue
    .line 2258
    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_14

    .line 2259
    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 2260
    iget-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/ah;

    invoke-virtual {p0}, Lbf/O;->F()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/ah;->a(I)V

    .line 2262
    :cond_14
    return-void
.end method

.method public I()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 316
    iget-object v1, p0, Lbf/O;->y:Ljava/lang/String;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lbf/O;->y:Ljava/lang/String;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-static {v2}, Lbf/O;->e(Lax/b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 329
    :cond_15
    :goto_15
    return v0

    .line 321
    :cond_16
    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v1, v1, Lax/s;

    if-eqz v1, :cond_32

    .line 322
    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v1

    invoke-virtual {v1}, Lax/s;->ab()Lax/h;

    move-result-object v1

    if-eqz v1, :cond_32

    .line 323
    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v1

    invoke-virtual {v1}, Lax/s;->aU()I

    move-result v1

    .line 324
    iget v2, p0, Lbf/O;->x:I

    if-ne v1, v2, :cond_15

    .line 329
    :cond_32
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public M()Z
    .registers 2

    .prologue
    .line 484
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method protected T()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1997
    const/16 v0, 0xf5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .registers 2

    .prologue
    .line 2002
    const/16 v0, 0x10

    return v0
.end method

.method public a(Lax/j;)Lax/b;
    .registers 4
    .parameter

    .prologue
    .line 1602
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    .line 1604
    instance-of v1, v0, Lax/w;

    if-eqz v1, :cond_1e

    .line 1605
    new-instance v0, Lax/w;

    iget-object v1, p0, Lbf/O;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    .line 1608
    invoke-static {}, Lax/l;->a()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/l;)V

    .line 1619
    :goto_1a
    invoke-virtual {v0}, Lax/b;->aM()V

    .line 1620
    return-object v0

    .line 1609
    :cond_1e
    instance-of v1, v0, Lax/x;

    if-eqz v1, :cond_28

    .line 1610
    new-instance v0, Lax/x;

    invoke-direct {v0, p1}, Lax/x;-><init>(Lax/k;)V

    goto :goto_1a

    .line 1611
    :cond_28
    instance-of v0, v0, Lax/i;

    if-eqz v0, :cond_32

    .line 1612
    new-instance v0, Lax/i;

    invoke-direct {v0, p1}, Lax/i;-><init>(Lax/k;)V

    goto :goto_1a

    .line 1614
    :cond_32
    new-instance v0, Lax/s;

    invoke-direct {v0, p1}, Lax/s;-><init>(Lax/k;)V

    goto :goto_1a
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 978
    iput-object p1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    .line 979
    invoke-virtual {p0, v2}, Lbf/O;->i(Z)V

    .line 983
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_2f

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->k()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2f

    .line 987
    invoke-virtual {p0, v2}, Lbf/O;->b(B)V

    .line 1002
    :cond_1e
    :goto_1e
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->aF()[I

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2e

    .line 1003
    const-string v0, "a"

    invoke-direct {p0, v0}, Lbf/O;->e(Ljava/lang/String;)V

    .line 1005
    :cond_2e
    return-void

    .line 991
    :cond_2f
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->ay()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1e

    .line 994
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 995
    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-lez v0, :cond_4d

    .line 996
    invoke-virtual {p0, v2}, Lbf/O;->b(I)V

    .line 999
    :cond_4d
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/O;->b(B)V

    goto :goto_1e
.end method

.method protected a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 1848
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/b;->b(Ljava/io/DataOutput;)V

    .line 1849
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1931
    const/4 v0, 0x0

    sput-object v0, Lbf/O;->G:Ljava/util/Vector;

    .line 1932
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v9, -0x1

    const/16 v8, 0x45

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 1202
    iget-object v1, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v1, Lcom/google/googlenav/ui/view/android/ah;

    .line 1203
    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v3

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v4

    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v5

    move v0, p1

    invoke-static/range {v0 .. v6}, Lbf/O;->a(ILcom/google/googlenav/ui/view/android/ah;Lcom/google/googlenav/J;Lax/b;Lcom/google/googlenav/ui/wizard/ca;Lcom/google/googlenav/ui/wizard/z;Z)Z

    move-result v0

    if-eqz v0, :cond_2d

    move v6, v7

    .line 1389
    :cond_2c
    :goto_2c
    return v6

    .line 1209
    :cond_2d
    sparse-switch p1, :sswitch_data_1f8

    goto :goto_2c

    .line 1214
    :sswitch_31
    const-string v0, "sb"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1216
    invoke-direct {p0, p2}, Lbf/O;->i(I)V

    move v6, v7

    .line 1217
    goto :goto_2c

    .line 1211
    :sswitch_3b
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/O;->c(ILjava/lang/Object;)V

    move v6, v7

    .line 1212
    goto :goto_2c

    .line 1220
    :sswitch_42
    invoke-direct {p0, p2}, Lbf/O;->i(I)V

    move v6, v7

    .line 1221
    goto :goto_2c

    .line 1224
    :sswitch_47
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    move v6, v7

    .line 1226
    goto :goto_2c

    .line 1229
    :sswitch_5e
    invoke-direct {p0}, Lbf/O;->bI()V

    move v6, v7

    .line 1230
    goto :goto_2c

    .line 1233
    :sswitch_63
    invoke-direct {p0}, Lbf/O;->bF()V

    move v6, v7

    .line 1234
    goto :goto_2c

    .line 1237
    :sswitch_68
    invoke-direct {p0}, Lbf/O;->bG()V

    move v6, v7

    .line 1238
    goto :goto_2c

    .line 1241
    :sswitch_6d
    invoke-direct {p0}, Lbf/O;->bH()V

    move v6, v7

    .line 1242
    goto :goto_2c

    .line 1245
    :sswitch_72
    invoke-direct {p0, p2}, Lbf/O;->j(I)V

    move v6, v7

    .line 1246
    goto :goto_2c

    .line 1251
    :sswitch_77
    invoke-virtual {p0, p2}, Lbf/O;->b(I)V

    .line 1252
    invoke-virtual {p0}, Lbf/O;->be()Z

    .line 1253
    invoke-direct {p0}, Lbf/O;->bv()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1254
    invoke-direct {p0}, Lbf/O;->bu()Z

    move v6, v7

    .line 1255
    goto :goto_2c

    .line 1263
    :sswitch_88
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/w;

    if-eqz v0, :cond_2c

    .line 1266
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v2}, Lax/b;->az()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/O;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1272
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 1273
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    if-ne v0, v9, :cond_cf

    :goto_b4
    invoke-virtual {v1, v6}, Lax/b;->r(I)V

    .line 1275
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0, v9}, Lax/b;->s(I)V

    .line 1276
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->f(Lax/b;)V

    .line 1277
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    move v6, v7

    .line 1278
    goto/16 :goto_2c

    :cond_cf
    move v6, v0

    .line 1273
    goto :goto_b4

    .line 1281
    :sswitch_d1
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_dc

    .line 1282
    const-string v0, "Navigation"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    .line 1284
    :cond_dc
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->k()I

    move-result v0

    packed-switch v0, :pswitch_data_25a

    goto/16 :goto_2c

    .line 1286
    :pswitch_e9
    const/16 v0, 0x25c

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_2c

    .line 1288
    :pswitch_f1
    const/16 v0, 0x25e

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_2c

    .line 1290
    :pswitch_f9
    const/16 v0, 0x25d

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_2c

    .line 1292
    :pswitch_101
    const/16 v0, 0x263

    invoke-virtual {p0, v0, p2, p3}, Lbf/O;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_2c

    .line 1297
    :sswitch_109
    const-string v0, "n"

    const-string v1, "d"

    invoke-direct {p0, v1}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1300
    invoke-direct {p0, v6}, Lbf/O;->h(I)V

    move v6, v7

    .line 1301
    goto/16 :goto_2c

    .line 1305
    :sswitch_11a
    const-string v0, "n"

    const-string v1, "d"

    invoke-direct {p0, v1}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1308
    invoke-direct {p0, v10}, Lbf/O;->h(I)V

    move v6, v7

    .line 1309
    goto/16 :goto_2c

    .line 1312
    :sswitch_12b
    const-string v0, "n"

    const-string v1, "d"

    invoke-direct {p0, v1}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1315
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbf/O;->h(I)V

    move v6, v7

    .line 1316
    goto/16 :goto_2c

    .line 1319
    :sswitch_13d
    const/16 v0, 0x61

    const-string v1, "n"

    const-string v2, "d"

    invoke-direct {p0, v2}, Lbf/O;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1323
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0, v7}, LaS/a;->a(Z)V

    .line 1324
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    .line 1325
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-static {v1, v0}, Lbf/O;->a(Lcom/google/googlenav/ui/s;Lax/w;)Z

    move-result v1

    if-nez v1, :cond_162

    .line 1326
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/w;)V

    :cond_162
    move v6, v7

    .line 1328
    goto/16 :goto_2c

    .line 1341
    :sswitch_165
    invoke-virtual {p0, p2}, Lbf/O;->b(I)V

    .line 1342
    invoke-virtual {p0}, Lbf/O;->be()Z

    .line 1344
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    iget-object v2, p0, Lbf/O;->I:Lbf/I;

    invoke-virtual {v2, v6}, Lbf/I;->b(I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Lax/b;Ljava/util/List;)V

    .line 1350
    new-array v0, v10, [Ljava/lang/String;

    const-string v1, "a=s"

    aput-object v1, v0, v6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "i="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1352
    const/16 v1, 0x10

    invoke-virtual {p0}, Lbf/O;->av()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 1355
    goto/16 :goto_2c

    .line 1359
    :sswitch_1a9
    const-string v0, "od"

    invoke-direct {p0, v0}, Lbf/O;->e(Ljava/lang/String;)V

    .line 1360
    invoke-direct {p0}, Lbf/O;->bK()V

    move v6, v7

    .line 1361
    goto/16 :goto_2c

    .line 1364
    :sswitch_1b4
    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v0

    if-eqz v0, :cond_1bd

    .line 1365
    invoke-direct {p0}, Lbf/O;->bM()V

    :cond_1bd
    move v6, v7

    .line 1367
    goto/16 :goto_2c

    .line 1370
    :sswitch_1c0
    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v0

    if-eqz v0, :cond_1ce

    .line 1371
    const-string v0, "nc"

    invoke-direct {p0, v0}, Lbf/O;->e(Ljava/lang/String;)V

    .line 1372
    invoke-direct {p0}, Lbf/O;->bL()V

    :cond_1ce
    move v6, v7

    .line 1374
    goto/16 :goto_2c

    .line 1378
    :sswitch_1d1
    invoke-super {p0, p1, p2, p3}, Lbf/F;->a(IILjava/lang/Object;)Z

    move-result v6

    goto/16 :goto_2c

    .line 1381
    :sswitch_1d7
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/O;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1383
    invoke-virtual {p0}, Lbf/O;->W()V

    move v6, v7

    .line 1384
    goto/16 :goto_2c

    .line 1386
    :sswitch_1ec
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-static {v0, v1}, Lbf/O;->b(Lcom/google/googlenav/ui/s;Lax/b;)V

    move v6, v7

    .line 1387
    goto/16 :goto_2c

    .line 1209
    :sswitch_data_1f8
    .sparse-switch
        0x1 -> :sswitch_31
        0x5 -> :sswitch_1d1
        0xc8 -> :sswitch_42
        0xc9 -> :sswitch_5e
        0xca -> :sswitch_63
        0xcb -> :sswitch_68
        0xcc -> :sswitch_6d
        0xdc -> :sswitch_1a9
        0xdd -> :sswitch_1b4
        0xe5 -> :sswitch_77
        0xe6 -> :sswitch_1a9
        0xe7 -> :sswitch_72
        0xf0 -> :sswitch_1ec
        0xf1 -> :sswitch_3b
        0xf2 -> :sswitch_88
        0xf4 -> :sswitch_47
        0x1f4 -> :sswitch_1c0
        0x25c -> :sswitch_109
        0x25d -> :sswitch_11a
        0x25e -> :sswitch_13d
        0x261 -> :sswitch_d1
        0x263 -> :sswitch_12b
        0x3f9 -> :sswitch_1d7
        0x5e6 -> :sswitch_165
    .end sparse-switch

    .line 1284
    :pswitch_data_25a
    .packed-switch 0x0
        :pswitch_e9
        :pswitch_f1
        :pswitch_f9
        :pswitch_101
    .end packed-switch
.end method

.method protected a(Lat/b;Z)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 654
    .line 658
    invoke-virtual {p1}, Lat/b;->c()Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-boolean v2, p0, Lbf/O;->D:Z

    if-eqz v2, :cond_1f

    .line 659
    iget-boolean v2, p0, Lbf/O;->E:Z

    if-eqz v2, :cond_14

    :goto_10
    invoke-direct {p0, p1, v0}, Lbf/O;->a(Lat/b;I)V

    .line 733
    :cond_13
    :goto_13
    return v1

    .line 659
    :cond_14
    invoke-virtual {p0}, Lbf/O;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_10

    .line 665
    :cond_1f
    invoke-virtual {p1}, Lat/b;->e()Z

    move-result v2

    if-eqz v2, :cond_57

    iget-boolean v2, p0, Lbf/O;->D:Z

    if-eqz v2, :cond_57

    .line 666
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    .line 667
    invoke-virtual {p0, p1}, Lbf/O;->b(Lat/b;)LaN/B;

    move-result-object v0

    .line 668
    invoke-virtual {p0, v0}, Lbf/O;->a(LaN/B;)Lo/D;

    move-result-object v3

    invoke-static {v0, v3}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v3

    .line 669
    iget-boolean v0, p0, Lbf/O;->E:Z

    if-eqz v0, :cond_4d

    new-instance v0, Lax/j;

    invoke-virtual {v2}, Lax/b;->as()Lax/y;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lax/j;-><init>(Lax/y;Lax/y;)V

    .line 673
    :goto_46
    invoke-virtual {p0, v0}, Lbf/O;->b(Lax/j;)V

    .line 674
    invoke-virtual {p0}, Lbf/O;->c()V

    goto :goto_13

    .line 669
    :cond_4d
    new-instance v0, Lax/j;

    invoke-virtual {v2}, Lax/b;->aq()Lax/y;

    move-result-object v2

    invoke-direct {v0, v2, v3}, Lax/j;-><init>(Lax/y;Lax/y;)V

    goto :goto_46

    .line 679
    :cond_57
    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v2

    if-nez v2, :cond_69

    invoke-virtual {p1}, Lat/b;->f()Z

    move-result v2

    if-nez v2, :cond_69

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-eqz v2, :cond_e1

    .line 684
    :cond_69
    iget-object v2, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v3

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LaN/u;->b(II)LaN/B;

    move-result-object v2

    .line 685
    invoke-virtual {p0, v2}, Lbf/O;->b(LaN/B;)I

    move-result v2

    .line 686
    if-ltz v2, :cond_c8

    .line 689
    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 690
    iget-object v3, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v3, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    .line 691
    iget-object v3, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 693
    iget-object v5, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v5, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lbf/O;->a(Lcom/google/googlenav/E;Lcom/google/googlenav/E;)Z

    move-result v5

    if-eqz v5, :cond_b7

    move v2, v0

    .line 706
    :cond_9e
    :goto_9e
    if-eqz v2, :cond_a2

    if-ne v2, v3, :cond_13

    .line 707
    :cond_a2
    iput-boolean v1, p0, Lbf/O;->D:Z

    .line 708
    if-nez v2, :cond_a7

    move v0, v1

    :cond_a7
    iput-boolean v0, p0, Lbf/O;->E:Z

    .line 709
    iget-boolean v0, p0, Lbf/O;->E:Z

    if-eqz v0, :cond_c5

    const-string v0, "ds"

    :goto_af
    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 713
    invoke-direct {p0, p1, v2}, Lbf/O;->a(Lat/b;I)V

    goto/16 :goto_13

    .line 699
    :cond_b7
    iget-object v5, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v5, v3}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lbf/O;->a(Lcom/google/googlenav/E;Lcom/google/googlenav/E;)Z

    move-result v4

    if-eqz v4, :cond_9e

    move v2, v3

    .line 704
    goto :goto_9e

    .line 709
    :cond_c5
    const-string v0, "de"

    goto :goto_af

    :cond_c8
    move v2, v1

    .line 723
    :goto_c9
    if-eqz v2, :cond_de

    .line 724
    const/4 v2, -0x1

    const-string v3, "c"

    const-string v4, "c"

    invoke-virtual {p0, v2, v3, v4}, Lbf/O;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 726
    invoke-virtual {p0}, Lbf/O;->Z()V

    .line 727
    if-eqz p2, :cond_de

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-eqz v2, :cond_13

    :cond_de
    move v1, v0

    .line 733
    goto/16 :goto_13

    :cond_e1
    move v2, v0

    goto :goto_c9
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 492
    const/4 v0, 0x4

    invoke-super {p0, p1, v0}, Lbf/F;->a(Lcom/google/googlenav/ui/view/t;I)Z

    move-result v0

    return v0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .registers 4
    .parameter

    .prologue
    .line 1853
    iget-object v0, p0, Lbf/O;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    .line 1854
    invoke-static {p1, v0}, Lax/b;->a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    .line 1856
    instance-of v1, v0, Lax/s;

    if-eqz v1, :cond_11

    .line 1857
    invoke-virtual {v0}, Lax/b;->G()V

    .line 1859
    :cond_11
    iput-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    .line 1860
    const/4 v0, 0x1

    return v0
.end method

.method public aB()Z
    .registers 2

    .prologue
    .line 1810
    const/4 v0, 0x1

    return v0
.end method

.method public aF()I
    .registers 2

    .prologue
    .line 576
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7f11000b

    :goto_d
    return v0

    :cond_e
    invoke-super {p0}, Lbf/F;->aF()I

    move-result v0

    goto :goto_d
.end method

.method public aH()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 582
    const/16 v0, 0xfb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aI()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 589
    const/4 v0, 0x0

    return-object v0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 594
    const/16 v0, 0xf7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .registers 5

    .prologue
    .line 1819
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    .line 1820
    invoke-virtual {v1}, Lax/b;->l()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1821
    const/16 v0, 0x237

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1829
    :goto_10
    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v1

    .line 1830
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1822
    :cond_23
    invoke-virtual {v1}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 1823
    const/16 v0, 0x23d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 1824
    :cond_30
    invoke-virtual {v1}, Lax/b;->n()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 1825
    const/16 v0, 0x232

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 1827
    :cond_3d
    const/16 v0, 0x23f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10
.end method

.method public aL()Lam/f;
    .registers 3

    .prologue
    .line 1835
    iget-object v0, p0, Lbf/O;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->ah:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 1843
    const/4 v0, 0x1

    return v0
.end method

.method public aT()Z
    .registers 3

    .prologue
    .line 1869
    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    .line 1871
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    .line 1873
    invoke-virtual {v0}, Lax/b;->v()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {v0}, Lax/b;->z_()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 1875
    :cond_13
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/b;)V

    .line 1876
    const/4 v0, 0x0

    .line 1887
    :goto_19
    return v0

    .line 1881
    :cond_1a
    invoke-virtual {v0}, Lax/b;->F()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 1882
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/b;)V

    .line 1885
    :cond_25
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lax/b;->n()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->c_(Z)V

    .line 1887
    const/4 v0, 0x1

    goto :goto_19
.end method

.method public aU()V
    .registers 3

    .prologue
    .line 1892
    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    .line 1894
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->az()I

    move-result v0

    .line 1896
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->aI()Lax/b;

    move-result-object v1

    iput-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    .line 1897
    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v1, v1, Lax/w;

    if-eqz v1, :cond_22

    .line 1900
    invoke-virtual {p0}, Lbf/O;->f()Lax/w;

    move-result-object v1

    invoke-virtual {v1, v0}, Lax/w;->w(I)V

    .line 1903
    :cond_22
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->n()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 1904
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->c_(Z)V

    .line 1906
    :cond_32
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->J:Ljava/util/List;

    .line 1907
    return-void
.end method

.method public aW()V
    .registers 3

    .prologue
    .line 337
    invoke-virtual {p0}, Lbf/O;->ah()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_13

    .line 338
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/O;->b(B)V

    .line 340
    :cond_13
    invoke-super {p0}, Lbf/F;->aW()V

    .line 341
    invoke-direct {p0}, Lbf/O;->bN()V

    .line 342
    sget-object v0, LaE/b;->a:LaE/b;

    invoke-virtual {v0}, LaE/b;->e()Z

    move-result v0

    if-eqz v0, :cond_28

    iget-object v0, p0, Lbf/O;->J:Ljava/util/List;

    if-eqz v0, :cond_28

    .line 343
    invoke-direct {p0}, Lbf/O;->bO()V

    .line 345
    :cond_28
    return-void
.end method

.method public aX()V
    .registers 1

    .prologue
    .line 352
    invoke-virtual {p0}, Lbf/O;->be()Z

    .line 353
    invoke-super {p0}, Lbf/F;->aX()V

    .line 354
    invoke-virtual {p0}, Lbf/O;->bt()V

    .line 355
    invoke-direct {p0}, Lbf/O;->bP()V

    .line 356
    return-void
.end method

.method public aa()Z
    .registers 2

    .prologue
    .line 571
    invoke-super {p0}, Lbf/F;->aa()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lbf/O;->D:Z

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public ab()Z
    .registers 2

    .prologue
    .line 642
    iget-boolean v0, p0, Lbf/O;->D:Z

    return v0
.end method

.method public ad()Z
    .registers 2

    .prologue
    .line 2266
    const/4 v0, 0x0

    return v0
.end method

.method public ap()V
    .registers 1

    .prologue
    .line 1937
    return-void
.end method

.method public aq()V
    .registers 2

    .prologue
    .line 1941
    iget-object v0, p0, Lbf/O;->I:Lbf/I;

    invoke-virtual {v0}, Lbf/I;->a()Lcom/google/googlenav/ui/view/android/aL;

    move-result-object v0

    iput-object v0, p0, Lbf/O;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 1942
    return-void
.end method

.method public ar()Lcom/google/googlenav/F;
    .registers 2

    .prologue
    .line 1067
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    return-object v0
.end method

.method public av()I
    .registers 2

    .prologue
    .line 1802
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lat/b;)LaN/B;
    .registers 6
    .parameter

    .prologue
    .line 602
    iget-object v0, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    sget v3, Lbf/O;->w:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LaN/u;->b(II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public b(Lax/j;)V
    .registers 4
    .parameter

    .prologue
    .line 1624
    invoke-virtual {p0, p1}, Lbf/O;->a(Lax/j;)Lax/b;

    move-result-object v0

    .line 1625
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    .line 1626
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 500
    invoke-virtual {p0}, Lbf/O;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lbf/O;->j(I)V

    .line 501
    const/4 v0, 0x1

    return v0
.end method

.method public bc()Ljava/lang/String;
    .registers 6

    .prologue
    const/16 v4, 0x26

    const/16 v3, 0x3d

    .line 2272
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2273
    const-string v0, "Directions"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    .line 2278
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2281
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    .line 2282
    if-eqz v1, :cond_32

    .line 2283
    const-string v2, "saddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2284
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2285
    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2286
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2290
    :cond_32
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->as()Lax/y;

    move-result-object v1

    .line 2291
    if-eqz v1, :cond_4e

    .line 2292
    const-string v2, "daddr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2293
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2294
    invoke-virtual {v1}, Lax/y;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2295
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2299
    :cond_4e
    const-string v1, "dirflg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2300
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2302
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->k()I

    move-result v1

    packed-switch v1, :pswitch_data_7e

    .line 2316
    :goto_61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2304
    :pswitch_66
    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_61

    .line 2307
    :pswitch_6c
    const-string v1, "r"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_61

    .line 2310
    :pswitch_72
    const-string v1, "w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_61

    .line 2313
    :pswitch_78
    const-string v1, "b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_61

    .line 2302
    :pswitch_data_7e
    .packed-switch 0x0
        :pswitch_66
        :pswitch_6c
        :pswitch_72
        :pswitch_78
    .end packed-switch
.end method

.method public be()Z
    .registers 2

    .prologue
    .line 1423
    iget-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    if-eqz v0, :cond_e

    .line 1424
    iget-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/r;->dismiss()V

    .line 1425
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->C:Lcom/google/googlenav/ui/view/dialog/r;

    .line 1426
    const/4 v0, 0x1

    .line 1428
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected bf()Z
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1501
    invoke-direct {p0}, Lbf/O;->bJ()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1502
    const-string v1, "nc"

    invoke-direct {p0, v1}, Lbf/O;->e(Ljava/lang/String;)V

    .line 1503
    invoke-direct {p0}, Lbf/O;->bL()V

    .line 1564
    :cond_12
    :goto_12
    return v0

    .line 1507
    :cond_13
    invoke-virtual {p0}, Lbf/O;->be()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1508
    invoke-virtual {p0}, Lbf/O;->ae()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1512
    invoke-virtual {p0, v4}, Lbf/O;->b(I)V

    goto :goto_12

    .line 1517
    :cond_23
    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v2

    if-eqz v2, :cond_40

    .line 1518
    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v2

    .line 1519
    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v3

    sparse-switch v3, :sswitch_data_94

    .line 1563
    :cond_40
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_12

    .line 1526
    :sswitch_46
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    .line 1527
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-static {v1, v2}, Lbf/O;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z

    goto :goto_12

    .line 1531
    :sswitch_51
    invoke-virtual {p0, v1, v5}, Lbf/O;->c(ILjava/lang/Object;)V

    goto :goto_12

    .line 1535
    :sswitch_55
    invoke-virtual {p0, v1, v5}, Lbf/O;->b(ILjava/lang/Object;)V

    goto :goto_12

    :sswitch_59
    move v0, v1

    .line 1539
    goto :goto_12

    .line 1542
    :sswitch_5b
    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v1, v1, Lax/w;

    if-eqz v1, :cond_68

    .line 1543
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1, v4}, Lax/b;->s(I)V

    .line 1545
    :cond_68
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->f(Lax/b;)V

    .line 1546
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_12

    .line 1550
    :sswitch_77
    const-string v1, "b"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    .line 1551
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(Lax/k;)V

    .line 1552
    iget-object v1, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_12

    .line 1557
    :sswitch_8b
    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    const-string v3, ""

    invoke-virtual {v2, v3, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    goto :goto_12

    .line 1519
    nop

    :sswitch_data_94
    .sparse-switch
        0x1 -> :sswitch_77
        0x2 -> :sswitch_5b
        0x3 -> :sswitch_51
        0x4 -> :sswitch_55
        0x7 -> :sswitch_46
        0x12 -> :sswitch_59
        0x18 -> :sswitch_46
        0x19 -> :sswitch_46
        0x1a -> :sswitch_46
        0x1c -> :sswitch_8b
    .end sparse-switch
.end method

.method public bg()Z
    .registers 2

    .prologue
    .line 1667
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/w;

    return v0
.end method

.method public bh()Z
    .registers 2

    .prologue
    .line 1671
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/s;

    return v0
.end method

.method public bi()Z
    .registers 2

    .prologue
    .line 1675
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/x;

    return v0
.end method

.method public bj()Z
    .registers 2

    .prologue
    .line 1679
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lax/i;

    return v0
.end method

.method public bk()Z
    .registers 2

    .prologue
    .line 1695
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/O;->d(Lax/b;)Z

    move-result v0

    return v0
.end method

.method public bl()Z
    .registers 2

    .prologue
    .line 1705
    iget-object v0, p0, Lbf/O;->A:LaH/m;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lbf/O;->A:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lbf/O;->A:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method bm()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1714
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v2

    .line 1717
    invoke-virtual {v2}, Lax/b;->P()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 1718
    invoke-virtual {v2}, Lax/b;->O()I

    move-result v0

    invoke-virtual {v2}, Lax/b;->av()I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 1724
    :goto_19
    invoke-virtual {v2}, Lax/b;->N()Z

    move-result v3

    if-eqz v3, :cond_51

    .line 1725
    const/16 v3, 0x10a

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {v2}, Lax/b;->M()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1729
    :goto_35
    if-eqz v0, :cond_4b

    .line 1730
    if-eqz v2, :cond_4a

    .line 1732
    const/16 v1, 0xfe

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v5

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1743
    :cond_4a
    :goto_4a
    return-object v0

    .line 1739
    :cond_4b
    if-eqz v2, :cond_4f

    move-object v0, v2

    .line 1741
    goto :goto_4a

    :cond_4f
    move-object v0, v1

    .line 1743
    goto :goto_4a

    :cond_51
    move-object v2, v1

    goto :goto_35

    :cond_53
    move-object v0, v1

    goto :goto_19
.end method

.method bn()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1753
    invoke-virtual {p0}, Lbf/O;->bh()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v1

    invoke-virtual {v1}, Lax/s;->N()Z

    move-result v1

    if-nez v1, :cond_12

    .line 1775
    :cond_11
    :goto_11
    return-object v0

    .line 1757
    :cond_12
    invoke-direct {p0}, Lbf/O;->bB()I

    move-result v1

    .line 1758
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2a

    .line 1759
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v1

    invoke-virtual {v1}, Lax/b;->F()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1760
    const/16 v0, 0x25d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    .line 1766
    :cond_2a
    const/16 v2, 0x3c

    if-lt v1, v2, :cond_11

    .line 1769
    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v0

    invoke-virtual {v0}, Lax/s;->M()I

    move-result v0

    add-int/2addr v0, v1

    .line 1771
    const/16 v1, 0x5c1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_11
.end method

.method bo()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1784
    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1794
    :cond_7
    :goto_7
    return-object v0

    .line 1788
    :cond_8
    invoke-virtual {p0}, Lbf/O;->f()Lax/w;

    move-result-object v1

    invoke-virtual {v1}, Lax/w;->Q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 1789
    if-eqz v1, :cond_7

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1790
    const/16 v0, 0xe2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1}, Lcom/google/googlenav/ui/bd;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method public bp()Z
    .registers 2

    .prologue
    .line 1980
    iget-boolean v0, p0, Lbf/O;->D:Z

    return v0
.end method

.method br()V
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 2128
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v3

    .line 2129
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move v0, v1

    .line 2130
    :goto_a
    invoke-virtual {v3}, Lax/b;->aA()I

    move-result v2

    if-ge v0, v2, :cond_27

    .line 2131
    invoke-virtual {v3, v0}, Lax/b;->l(I)Lax/h;

    move-result-object v2

    .line 2132
    invoke-virtual {v2}, Lax/h;->g()[LaN/B;

    move-result-object v5

    array-length v6, v5

    move v2, v1

    :goto_1a
    if-ge v2, v6, :cond_24

    aget-object v7, v5, v2

    .line 2133
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2132
    add-int/lit8 v2, v2, 0x1

    goto :goto_1a

    .line 2130
    :cond_24
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2137
    :cond_27
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v4, v0}, Lbf/O;->a(Ljava/util/List;Ljava/util/List;)V

    .line 2138
    return-void
.end method

.method public bs()V
    .registers 2

    .prologue
    .line 2209
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    if-nez v0, :cond_5

    .line 2219
    :cond_4
    :goto_4
    return-void

    .line 2212
    :cond_5
    iget-object v0, p0, Lbf/O;->J:Ljava/util/List;

    if-nez v0, :cond_19

    .line 2213
    invoke-virtual {p0}, Lbf/O;->bi()Z

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p0}, Lbf/O;->bj()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2214
    :cond_15
    invoke-virtual {p0}, Lbf/O;->br()V

    goto :goto_4

    .line 2217
    :cond_19
    invoke-direct {p0}, Lbf/O;->bO()V

    goto :goto_4
.end method

.method public bt()V
    .registers 2

    .prologue
    .line 2241
    iget-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    if-eqz v0, :cond_11

    .line 2242
    iget-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->c()V

    .line 2243
    iget-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->a()V

    .line 2244
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/O;->K:Lcom/google/googlenav/ui/android/M;

    .line 2246
    :cond_11
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 646
    const/4 v0, 0x1

    sput-boolean v0, Lbf/O;->F:Z

    .line 647
    return-void
.end method

.method public c(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 1629
    iget-object v0, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    .line 1630
    return-void
.end method

.method public c(Lcom/google/googlenav/F;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 274
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    if-ne p1, v0, :cond_29

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->l()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-virtual {p0}, Lbf/O;->ax()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-virtual {p0}, Lbf/O;->ai()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 276
    invoke-virtual {p0}, Lbf/O;->y()V

    .line 300
    :cond_28
    :goto_28
    return-void

    :cond_29
    move-object v0, p1

    .line 283
    check-cast v0, Lax/b;

    .line 287
    iget-object v2, p0, Lbf/O;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->a()V

    .line 288
    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-eqz v2, :cond_28

    .line 291
    iget-object v2, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    instance-of v2, v2, Lax/s;

    if-eqz v2, :cond_47

    .line 292
    invoke-direct {p0}, Lbf/O;->bC()Lax/s;

    move-result-object v2

    invoke-virtual {v2}, Lax/s;->E()Z

    move-result v2

    if-nez v2, :cond_4f

    .line 294
    :cond_47
    :goto_47
    invoke-direct {p0, p1, v1}, Lbf/O;->a(Lcom/google/googlenav/F;Z)V

    .line 298
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lax/b;->r(I)V

    goto :goto_28

    .line 292
    :cond_4f
    const/4 v1, 0x0

    goto :goto_47
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 1017
    invoke-virtual {p0}, Lbf/O;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->ab()Lax/h;

    move-result-object v0

    invoke-virtual {v0}, Lax/h;->y()Z

    move-result v0

    return v0
.end method

.method public d(Lax/b;)Z
    .registers 5
    .parameter

    .prologue
    .line 1688
    invoke-virtual {p1}, Lax/b;->u()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-virtual {p1}, Lax/b;->v()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-virtual {p1}, Lax/b;->z()Z

    move-result v0

    if-nez v0, :cond_2c

    invoke-virtual {p1}, Lax/b;->T()Z

    move-result v0

    if-nez v0, :cond_2c

    invoke-virtual {p1}, Lax/b;->aB()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aC()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->k()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lbf/O;->a(LaN/B;LaN/B;I)Z

    move-result v0

    if-eqz v0, :cond_2c

    const/4 v0, 0x1

    :goto_2b
    return v0

    :cond_2c
    const/4 v0, 0x0

    goto :goto_2b
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .registers 2

    .prologue
    .line 966
    invoke-virtual {p0}, Lbf/O;->bg()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 967
    invoke-direct {p0}, Lbf/O;->bw()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    .line 973
    :goto_a
    return-object v0

    .line 968
    :cond_b
    invoke-virtual {p0}, Lbf/O;->bi()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 969
    invoke-direct {p0}, Lbf/O;->by()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    goto :goto_a

    .line 970
    :cond_16
    invoke-virtual {p0}, Lbf/O;->bj()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 971
    invoke-direct {p0}, Lbf/O;->bz()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    goto :goto_a

    .line 973
    :cond_21
    invoke-direct {p0}, Lbf/O;->bA()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    goto :goto_a
.end method

.method public f()Lax/w;
    .registers 2

    .prologue
    .line 1054
    iget-object v0, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    return-object v0
.end method

.method protected f(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 532
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_2d

    .line 533
    invoke-virtual {p0}, Lbf/O;->ae()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 534
    const-string v1, "m"

    const-string v2, "#"

    invoke-virtual {p0, v1, v2}, Lbf/O;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    iget-object v1, p0, Lbf/O;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-direct {p0, v1}, Lbf/O;->i(I)V

    .line 564
    :cond_1f
    :goto_1f
    return v0

    .line 537
    :cond_20
    const-string v1, "l"

    const-string v2, "#"

    invoke-virtual {p0, v1, v2}, Lbf/O;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/O;->c(ILjava/lang/Object;)V

    goto :goto_1f

    .line 544
    :cond_2d
    invoke-virtual {p0}, Lbf/O;->ai()Z

    move-result v1

    if-nez v1, :cond_5e

    .line 545
    invoke-direct {p0, p1}, Lbf/O;->g(Lat/a;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 552
    invoke-direct {p0}, Lbf/O;->bu()Z

    move-result v0

    goto :goto_1f

    .line 553
    :cond_3e
    iget-object v1, p0, Lbf/O;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {p0, v1}, Lbf/O;->b(LaN/B;)I

    move-result v1

    if-ltz v1, :cond_51

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1f

    .line 559
    :cond_51
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5e

    .line 560
    invoke-virtual {p0}, Lbf/O;->bf()Z

    move-result v0

    goto :goto_1f

    .line 564
    :cond_5e
    invoke-virtual {p0, p1}, Lbf/O;->e(Lat/a;)Z

    move-result v0

    goto :goto_1f
.end method

.method public h()V
    .registers 1

    .prologue
    .line 1491
    invoke-virtual {p0}, Lbf/O;->bf()Z

    .line 1492
    return-void
.end method

.method protected i()Lbh/a;
    .registers 2

    .prologue
    .line 212
    new-instance v0, Lbh/d;

    invoke-direct {v0, p0}, Lbh/d;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected m()V
    .registers 1

    .prologue
    .line 1010
    return-void
.end method

.method protected n()V
    .registers 2

    .prologue
    .line 1921
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/O;->i(Z)V

    .line 1922
    invoke-super {p0}, Lbf/F;->n()V

    .line 1923
    return-void
.end method

.method protected x()I
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 851
    invoke-virtual {p0}, Lbf/O;->d()Z

    move-result v0

    if-eqz v0, :cond_19

    move v0, v1

    :goto_9
    invoke-virtual {p0}, Lbf/O;->au()Z

    move-result v3

    if-eqz v3, :cond_1b

    move v3, v1

    :goto_10
    add-int/2addr v0, v3

    invoke-virtual {p0}, Lbf/O;->bk()Z

    move-result v3

    if-eqz v3, :cond_1d

    :goto_17
    add-int/2addr v0, v1

    return v0

    :cond_19
    move v0, v2

    goto :goto_9

    :cond_1b
    move v3, v2

    goto :goto_10

    :cond_1d
    move v1, v2

    goto :goto_17
.end method
