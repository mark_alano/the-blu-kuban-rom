.class public Lbf/R;
.super Lbf/h;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/i;)V
    .registers 2
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    .line 27
    return-void
.end method

.method private a(Lcom/google/googlenav/friend/aI;)Lcom/google/googlenav/ui/aW;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 107
    .line 109
    invoke-virtual {p1, v0, v0}, Lcom/google/googlenav/friend/aI;->a(ZZ)Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_35

    .line 111
    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->C()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->be:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    :cond_2e
    sget-object v1, Lcom/google/googlenav/ui/aV;->n:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    .line 116
    :goto_34
    return-object v0

    :cond_35
    const/4 v0, 0x0

    goto :goto_34
.end method

.method private a(Lcom/google/googlenav/friend/aI;Z)Ljava/util/List;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 78
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 81
    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->f()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-virtual {p1}, Lcom/google/googlenav/friend/aI;->A()Ljava/lang/String;

    move-result-object v4

    .line 83
    iget-object v0, p0, Lbf/R;->c:Lbf/i;

    check-cast v0, Lbf/X;

    .line 84
    invoke-virtual {v0}, Lbf/X;->bJ()J

    move-result-wide v5

    invoke-static {p1, v5, v6, v2, p2}, Lcom/google/googlenav/friend/M;->a(Lcom/google/googlenav/friend/aI;JZZ)Ljava/lang/String;

    move-result-object v5

    .line 86
    if-eqz v4, :cond_5a

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_2d
    sget-object v4, Lcom/google/googlenav/ui/aV;->m:Lcom/google/googlenav/ui/aV;

    if-nez v5, :cond_58

    move v1, v2

    :goto_32
    invoke-static {v0, v4, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;Z)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    if-eqz v5, :cond_57

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->b:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_57
    return-object v3

    .line 89
    :cond_58
    const/4 v1, 0x0

    goto :goto_32

    :cond_5a
    move-object v0, v1

    goto :goto_2d
.end method


# virtual methods
.method public b()Lcom/google/googlenav/ui/view/d;
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 31
    invoke-virtual {p0}, Lbf/R;->a()Landroid/view/View;

    move-result-object v1

    .line 33
    iget-object v0, p0, Lbf/R;->c:Lbf/i;

    check-cast v0, Lbf/X;

    .line 34
    invoke-virtual {v0}, Lbf/X;->bG()Lcom/google/googlenav/friend/aI;

    move-result-object v0

    .line 37
    invoke-direct {p0, v0, v4}, Lbf/R;->a(Lcom/google/googlenav/friend/aI;Z)Ljava/util/List;

    move-result-object v2

    .line 38
    invoke-virtual {p0, v1, v2}, Lbf/R;->a(Landroid/view/View;Ljava/util/List;)V

    .line 41
    invoke-direct {p0, v0}, Lbf/R;->a(Lcom/google/googlenav/friend/aI;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_27

    .line 43
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ui/aW;

    aput-object v0, v2, v4

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lbf/R;->b(Landroid/view/View;Ljava/util/List;)V

    .line 47
    :cond_27
    invoke-virtual {p0, v1, v3}, Lbf/R;->c(Landroid/view/View;Ljava/util/List;)V

    .line 50
    invoke-virtual {p0, v1, v3, v3}, Lbf/R;->a(Landroid/view/View;Lam/f;Lam/f;)V

    .line 53
    invoke-virtual {p0, v1, v3}, Lbf/R;->a(Landroid/view/View;Lam/f;)V

    .line 55
    invoke-virtual {p0, v1, v3}, Lbf/R;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    .line 58
    invoke-virtual {p0, v1, v3}, Lbf/R;->a(Landroid/view/View;Landroid/view/View;)V

    .line 61
    invoke-virtual {p0, v1, v4}, Lbf/R;->a(Landroid/view/View;Z)V

    .line 63
    sget-object v0, Lbf/R;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/R;->c:Lbf/i;

    invoke-virtual {p0, v0, v1}, Lbf/R;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0
.end method
