.class public Lbf/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/au;


# instance fields
.field private final a:Lcom/google/googlenav/ui/s;

.field private final b:LaN/u;

.field private final c:Lcom/google/googlenav/ui/wizard/jv;

.field private final d:Lbf/am;

.field private final e:Lcom/google/googlenav/ui/ak;

.field private final f:Lcom/google/googlenav/aA;

.field private final g:Lcom/google/android/maps/MapsActivity;

.field private final h:Lcom/google/googlenav/actionbar/b;

.field private i:Lbf/ay;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;Lcom/google/android/maps/MapsActivity;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    sget-object v0, Lbf/ay;->a:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    .line 92
    iput-object p1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    .line 93
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    .line 94
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->d:Lbf/am;

    .line 95
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->b:LaN/u;

    .line 96
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->e:Lcom/google/googlenav/ui/ak;

    .line 97
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    iput-object v0, p0, Lbf/av;->f:Lcom/google/googlenav/aA;

    .line 98
    iput-object p2, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    .line 100
    new-instance v0, Lbf/aw;

    invoke-direct {v0, p0}, Lbf/aw;-><init>(Lbf/av;)V

    iput-object v0, p0, Lbf/av;->h:Lcom/google/googlenav/actionbar/b;

    .line 153
    return-void
.end method

.method static synthetic a(Lbf/av;)Lbf/ay;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lbf/av;->i:Lbf/ay;

    return-object v0
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 423
    packed-switch p1, :pswitch_data_22

    .line 445
    :pswitch_3
    sget-object v0, Lbf/ay;->a:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    .line 447
    :goto_7
    return-void

    .line 425
    :pswitch_8
    sget-object v0, Lbf/ay;->c:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_7

    .line 428
    :pswitch_d
    sget-object v0, Lbf/ay;->d:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_7

    .line 433
    :pswitch_12
    sget-object v0, Lbf/ay;->b:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_7

    .line 437
    :pswitch_17
    sget-object v0, Lbf/ay;->f:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_7

    .line 442
    :pswitch_1c
    sget-object v0, Lbf/ay;->e:Lbf/ay;

    iput-object v0, p0, Lbf/av;->i:Lbf/ay;

    goto :goto_7

    .line 423
    nop

    :pswitch_data_22
    .packed-switch 0x7f11000b
        :pswitch_8
        :pswitch_d
        :pswitch_1c
        :pswitch_1c
        :pswitch_3
        :pswitch_17
        :pswitch_17
        :pswitch_3
        :pswitch_12
        :pswitch_12
        :pswitch_12
    .end packed-switch
.end method

.method private a(Landroid/view/MenuItem;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 339
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 343
    if-nez p2, :cond_11

    .line 344
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 368
    :cond_10
    :goto_10
    return-void

    .line 349
    :cond_11
    iget-object v0, p0, Lbf/av;->i:Lbf/ay;

    sget-object v1, Lbf/ay;->b:Lbf/ay;

    if-ne v0, v1, :cond_41

    .line 350
    invoke-interface {p1}, Landroid/view/MenuItem;->expandActionView()Z

    .line 353
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    .line 354
    if-eqz v0, :cond_35

    .line 355
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    .line 363
    :cond_35
    :goto_35
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 364
    if-eqz v0, :cond_10

    .line 365
    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_10

    .line 358
    :cond_41
    invoke-interface {p1}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 359
    invoke-interface {p1}, Landroid/view/MenuItem;->collapseActionView()Z

    goto :goto_35
.end method

.method static synthetic b(Lbf/av;)Lbf/am;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    return-object v0
.end method

.method private b()Lbf/i;
    .registers 5

    .prologue
    .line 382
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 383
    if-nez v0, :cond_a

    .line 384
    const/4 v0, 0x0

    .line 414
    :cond_9
    :goto_9
    return-object v0

    .line 386
    :cond_a
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    .line 387
    sget-object v2, Lbf/ax;->a:[I

    iget-object v3, p0, Lbf/av;->i:Lbf/ay;

    invoke-virtual {v3}, Lbf/ay;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_50

    goto :goto_9

    .line 389
    :pswitch_1c
    const/4 v2, 0x1

    if-eq v1, v2, :cond_9

    .line 390
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->v()Lbf/O;

    move-result-object v0

    goto :goto_9

    .line 394
    :pswitch_26
    const/4 v2, 0x3

    if-eq v1, v2, :cond_9

    .line 395
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    goto :goto_9

    .line 399
    :pswitch_30
    const/16 v2, 0x1a

    if-eq v1, v2, :cond_9

    .line 400
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->C()Lbf/aJ;

    move-result-object v0

    goto :goto_9

    .line 404
    :pswitch_3b
    if-eqz v1, :cond_9

    .line 405
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    goto :goto_9

    .line 409
    :pswitch_44
    const/16 v2, 0x17

    if-eq v1, v2, :cond_9

    .line 410
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->x()Lbf/bU;

    move-result-object v0

    goto :goto_9

    .line 387
    nop

    :pswitch_data_50
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_26
        :pswitch_30
        :pswitch_3b
        :pswitch_44
    .end packed-switch
.end method

.method private b(Landroid/view/MenuItem;)V
    .registers 6
    .parameter

    .prologue
    .line 217
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 218
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->b()Z

    move-result v0

    if-nez v0, :cond_20

    .line 219
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    iget-object v1, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    const/4 v2, 0x0

    iget-object v3, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/actionbar/a;->a(Lcom/google/android/maps/MapsActivity;Landroid/app/Dialog;Lcom/google/googlenav/ui/s;)V

    .line 221
    :cond_20
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 222
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    iget-object v2, p0, Lbf/av;->h:Lcom/google/googlenav/actionbar/b;

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    .line 225
    :cond_2f
    return-void
.end method

.method static synthetic c(Lbf/av;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/actionbar/b;
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Lbf/av;->h:Lcom/google/googlenav/actionbar/b;

    return-object v0
.end method

.method public a(Landroid/view/Menu;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 237
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->b()Z

    move-result v1

    if-nez v1, :cond_3e

    .line 241
    if-eqz v0, :cond_116

    invoke-virtual {v0}, Lbf/i;->aG()I

    move-result v1

    move v6, v1

    .line 243
    :goto_1b
    if-eqz v0, :cond_11c

    invoke-virtual {v0}, Lbf/i;->aH()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v5, v1

    .line 245
    :goto_22
    if-eqz v0, :cond_125

    invoke-virtual {v0}, Lbf/i;->aI()Ljava/lang/CharSequence;

    move-result-object v1

    .line 251
    :goto_28
    iget-object v2, p0, Lbf/av;->i:Lbf/ay;

    sget-object v7, Lbf/ay;->a:Lbf/ay;

    if-ne v2, v7, :cond_36

    iget-object v2, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->P()Z

    move-result v2

    if-eqz v2, :cond_128

    :cond_36
    move v2, v4

    .line 253
    :goto_37
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v7

    invoke-virtual {v7, v6, v5, v1, v2}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 258
    :cond_3e
    const v1, 0x7f1004bd

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 259
    if-eqz v1, :cond_50

    .line 260
    iget-object v2, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->az()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 263
    :cond_50
    const v1, 0x7f1002ef

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 264
    if-eqz v1, :cond_62

    if-eqz v0, :cond_62

    .line 265
    invoke-virtual {v0}, Lbf/i;->N()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 268
    :cond_62
    const v1, 0x7f1004c3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 269
    if-eqz v1, :cond_74

    if-eqz v0, :cond_74

    .line 270
    invoke-virtual {v0}, Lbf/i;->M()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 274
    :cond_74
    const v1, 0x7f1004c4

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 275
    if-eqz v1, :cond_8c

    instance-of v2, v0, Lbf/O;

    if-eqz v2, :cond_8c

    check-cast v0, Lbf/O;

    invoke-virtual {v0}, Lbf/O;->bk()Z

    move-result v0

    if-nez v0, :cond_8c

    .line 277
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 282
    :cond_8c
    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v1

    .line 284
    const v0, 0x7f10049b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 285
    if-eqz v2, :cond_a6

    .line 290
    if-eqz v1, :cond_12b

    const v0, 0x7f020262

    :goto_a0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 293
    invoke-direct {p0, v2, v1}, Lbf/av;->a(Landroid/view/MenuItem;Z)V

    .line 296
    :cond_a6
    const v0, 0x7f100498

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 297
    if-eqz v2, :cond_bd

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_bd

    .line 298
    if-eqz v1, :cond_130

    const v0, 0x7f020235

    :goto_ba
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 302
    :cond_bd
    const v0, 0x7f1004c1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 303
    if-eqz v2, :cond_d4

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_d4

    .line 304
    if-eqz v1, :cond_134

    const v0, 0x7f020254

    :goto_d1
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 308
    :cond_d4
    const v0, 0x7f1004c2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 309
    if-eqz v2, :cond_eb

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_eb

    .line 310
    if-eqz v1, :cond_138

    const v0, 0x7f020245

    :goto_e8
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 314
    :cond_eb
    const v0, 0x7f1004be

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 315
    if-eqz v2, :cond_100

    .line 317
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_13c

    if-eqz v1, :cond_13c

    move v0, v4

    :goto_fd
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 320
    :cond_100
    const v0, 0x7f1004bf

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_115

    .line 323
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v2

    if-eqz v2, :cond_112

    if-nez v1, :cond_112

    move v3, v4

    :cond_112
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 325
    :cond_115
    return v4

    .line 241
    :cond_116
    const v1, 0x7f020222

    move v6, v1

    goto/16 :goto_1b

    .line 243
    :cond_11c
    const/16 v1, 0x2a7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    goto/16 :goto_22

    .line 245
    :cond_125
    const/4 v1, 0x0

    goto/16 :goto_28

    :cond_128
    move v2, v3

    .line 251
    goto/16 :goto_37

    .line 290
    :cond_12b
    const v0, 0x7f020263

    goto/16 :goto_a0

    .line 298
    :cond_130
    const v0, 0x7f020236

    goto :goto_ba

    .line 304
    :cond_134
    const v0, 0x7f020255

    goto :goto_d1

    .line 310
    :cond_138
    const v0, 0x7f020246

    goto :goto_e8

    :cond_13c
    move v0, v3

    .line 317
    goto :goto_fd
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 169
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v3

    .line 171
    if-eqz v3, :cond_7b

    invoke-virtual {v3}, Lbf/i;->aF()I

    move-result v0

    .line 173
    :goto_e
    iget-object v4, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v4}, Lbf/am;->w()Lbf/bK;

    move-result-object v4

    if-eqz v4, :cond_20

    .line 175
    iget-object v0, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    invoke-virtual {v0}, Lbf/bK;->aF()I

    move-result v0

    .line 177
    :cond_20
    invoke-direct {p0, v0}, Lbf/av;->a(I)V

    .line 178
    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 179
    invoke-static {p1}, LaP/a;->a(Landroid/view/Menu;)V

    .line 181
    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v4

    .line 183
    const v0, 0x7f1004be

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 184
    if-eqz v5, :cond_44

    .line 185
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_84

    if-eqz v4, :cond_84

    move v0, v1

    :goto_41
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 188
    :cond_44
    const v0, 0x7f1004bf

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_59

    .line 190
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v5

    if-eqz v5, :cond_56

    if-nez v4, :cond_56

    move v2, v1

    :cond_56
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 196
    :cond_59
    const v0, 0x7f100129

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_6b

    if-eqz v3, :cond_6b

    .line 198
    invoke-virtual {v3}, Lbf/i;->aJ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 201
    :cond_6b
    invoke-static {p1}, LaP/a;->b(Landroid/view/Menu;)V

    .line 203
    const v0, 0x7f10049b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_7a

    .line 205
    invoke-direct {p0, v0}, Lbf/av;->b(Landroid/view/MenuItem;)V

    .line 207
    :cond_7a
    return v1

    .line 171
    :cond_7b
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()I

    move-result v0

    goto :goto_e

    :cond_84
    move v0, v2

    .line 185
    goto :goto_41
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 9
    .parameter

    .prologue
    const/16 v6, 0x36

    const/16 v5, 0x34

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 461
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 462
    const v3, 0x102002c

    if-ne v2, v3, :cond_29

    .line 463
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ac()Lcom/google/googlenav/mylocationnotifier/k;

    move-result-object v1

    .line 464
    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->b()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 466
    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->h()V

    .line 467
    invoke-virtual {v1}, Lcom/google/googlenav/mylocationnotifier/k;->c()V

    .line 601
    :goto_22
    :sswitch_22
    return v0

    .line 471
    :cond_23
    iget-object v1, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->b(Z)V

    goto :goto_22

    .line 476
    :cond_29
    const-string v3, "m"

    invoke-static {p1, v3}, LaP/a;->a(Landroid/view/MenuItem;Ljava/lang/String;)V

    .line 479
    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v3}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v3

    if-eqz v3, :cond_38

    move v0, v1

    .line 481
    goto :goto_22

    .line 485
    :cond_38
    iget-object v3, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    if-eqz v3, :cond_65

    iget-object v3, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v3}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v3

    if-nez v3, :cond_65

    .line 486
    const v3, 0x7f10049b

    if-ne v2, v3, :cond_56

    .line 488
    :cond_49
    iget-object v0, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const/16 v2, 0x32c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    move v0, v1

    .line 489
    goto :goto_22

    .line 486
    :cond_56
    const v3, 0x7f100498

    if-eq v2, v3, :cond_49

    const v3, 0x7f1004c1

    if-eq v2, v3, :cond_49

    const v3, 0x7f1004c2

    if-eq v2, v3, :cond_49

    .line 493
    :cond_65
    invoke-direct {p0}, Lbf/av;->b()Lbf/i;

    move-result-object v3

    .line 494
    sparse-switch v2, :sswitch_data_14c

    :cond_6c
    :goto_6c
    move v0, v1

    .line 601
    goto :goto_22

    .line 496
    :sswitch_6e
    if-eqz v3, :cond_74

    .line 497
    invoke-virtual {v3}, Lbf/i;->aY()Z

    goto :goto_22

    :cond_74
    move v0, v1

    .line 500
    goto :goto_22

    .line 503
    :sswitch_76
    if-eqz v3, :cond_81

    .line 504
    new-instance v2, Lat/a;

    invoke-direct {v2, v5, v5, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {v3, v2}, Lbf/i;->e(Lat/a;)Z

    goto :goto_22

    :cond_81
    move v0, v1

    .line 508
    goto :goto_22

    .line 511
    :sswitch_83
    if-eqz v3, :cond_8e

    .line 512
    new-instance v2, Lat/a;

    invoke-direct {v2, v6, v6, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {v3, v2}, Lbf/i;->e(Lat/a;)Z

    goto :goto_22

    :cond_8e
    move v0, v1

    .line 516
    goto :goto_22

    .line 518
    :sswitch_90
    if-eqz v3, :cond_9f

    invoke-virtual {v3}, Lbf/i;->av()I

    move-result v2

    if-ne v2, v0, :cond_9f

    .line 519
    const/16 v1, 0xec

    const/4 v2, -0x1

    invoke-virtual {v3, v1, v2, v4}, Lbf/i;->a(IILjava/lang/Object;)Z

    goto :goto_22

    :cond_9f
    move v0, v1

    .line 523
    goto :goto_22

    .line 526
    :sswitch_a1
    iget-object v2, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v2, v1}, Lbf/am;->b(Z)V

    .line 527
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->au()V

    goto/16 :goto_22

    .line 532
    :sswitch_ad
    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_6c

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_6c

    .line 533
    iget-object v0, p0, Lbf/av;->g:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->onSearchRequested()Z

    goto :goto_6c

    .line 538
    :sswitch_c1
    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 539
    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v2, v0, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 541
    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v1

    iget-object v2, p0, Lbf/av;->b:LaN/u;

    invoke-virtual {v2}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_22

    .line 546
    :sswitch_ed
    iget-object v2, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const/16 v3, 0x13c

    invoke-virtual {v2, v1, v3}, Lcom/google/googlenav/ui/s;->a(ZI)V

    goto/16 :goto_22

    .line 549
    :sswitch_f6
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->b(I)V

    goto/16 :goto_22

    .line 552
    :sswitch_fe
    iget-object v1, p0, Lbf/av;->e:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    goto/16 :goto_22

    .line 555
    :sswitch_105
    iget-object v1, p0, Lbf/av;->c:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lbf/av;->d:Lbf/am;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;Z)V

    goto/16 :goto_22

    .line 558
    :sswitch_10e
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    goto/16 :goto_22

    .line 561
    :sswitch_115
    iget-object v1, p0, Lbf/av;->f:Lcom/google/googlenav/aA;

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    goto/16 :goto_22

    .line 564
    :sswitch_11c
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->J()V

    goto/16 :goto_22

    .line 567
    :sswitch_123
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->K()V

    goto/16 :goto_22

    .line 571
    :sswitch_12a
    iget-object v1, p0, Lbf/av;->f:Lcom/google/googlenav/aA;

    invoke-static {}, Lcom/google/googlenav/K;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto/16 :goto_22

    .line 574
    :sswitch_135
    const-string v2, "m"

    const-string v3, ""

    invoke-static {v2, v3}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v2, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->q(Z)V

    goto/16 :goto_22

    .line 580
    :sswitch_143
    iget-object v1, p0, Lbf/av;->a:Lcom/google/googlenav/ui/s;

    const-string v2, "offline"

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->g(Ljava/lang/String;)V

    goto/16 :goto_22

    .line 494
    :sswitch_data_14c
    .sparse-switch
        0x66 -> :sswitch_22
        0x67 -> :sswitch_22
        0x7f100129 -> :sswitch_6e
        0x7f1002ef -> :sswitch_83
        0x7f100498 -> :sswitch_c1
        0x7f10049b -> :sswitch_ad
        0x7f1004ad -> :sswitch_c1
        0x7f1004b6 -> :sswitch_12a
        0x7f1004bd -> :sswitch_a1
        0x7f1004be -> :sswitch_135
        0x7f1004bf -> :sswitch_143
        0x7f1004c0 -> :sswitch_115
        0x7f1004c1 -> :sswitch_f6
        0x7f1004c2 -> :sswitch_105
        0x7f1004c3 -> :sswitch_76
        0x7f1004c4 -> :sswitch_90
        0x7f1004c5 -> :sswitch_fe
        0x7f1004c6 -> :sswitch_ed
        0x7f1004c7 -> :sswitch_10e
        0x7f1004d1 -> :sswitch_11c
        0x7f1004d2 -> :sswitch_123
    .end sparse-switch
.end method
