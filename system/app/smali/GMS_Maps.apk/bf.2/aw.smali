.class Lbf/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/actionbar/b;


# instance fields
.field final synthetic a:Lbf/av;


# direct methods
.method constructor <init>(Lbf/av;)V
    .registers 2
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, Lbf/aw;->a:Lbf/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/googlenav/bf;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 110
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "22"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x4f2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lbb/w;)Z
    .registers 4
    .parameter

    .prologue
    .line 143
    invoke-virtual {p1}, Lbb/w;->g()Ljava/lang/String;

    move-result-object v0

    .line 144
    if-nez v0, :cond_8

    .line 146
    const/4 v0, 0x0

    .line 150
    :goto_7
    return v0

    .line 149
    :cond_8
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/maps/MapsActivity;->showStarOnMap(Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public b()V
    .registers 3

    .prologue
    .line 129
    iget-object v0, p0, Lbf/aw;->a:Lbf/av;

    invoke-static {v0}, Lbf/av;->a(Lbf/av;)Lbf/ay;

    move-result-object v0

    sget-object v1, Lbf/ay;->b:Lbf/ay;

    if-ne v0, v1, :cond_1e

    .line 130
    iget-object v0, p0, Lbf/aw;->a:Lbf/av;

    invoke-static {v0}, Lbf/av;->b(Lbf/av;)Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lbf/aw;->a:Lbf/av;

    invoke-static {v1}, Lbf/av;->b(Lbf/av;)Lbf/am;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbf/am;->h(Lbf/i;)V

    .line 139
    :cond_1d
    :goto_1d
    return-void

    .line 132
    :cond_1e
    iget-object v0, p0, Lbf/aw;->a:Lbf/av;

    invoke-static {v0}, Lbf/av;->c(Lbf/av;)Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->c()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 137
    iget-object v0, p0, Lbf/aw;->a:Lbf/av;

    invoke-static {v0}, Lbf/av;->c(Lbf/av;)Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    goto :goto_1d
.end method

.method public b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method
