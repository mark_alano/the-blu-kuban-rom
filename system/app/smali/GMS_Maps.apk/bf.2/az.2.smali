.class public Lbf/az;
.super Lbf/h;
.source "SourceFile"


# instance fields
.field d:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/dg;)V
    .registers 3
    .parameter

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbf/h;-><init>(Lbf/i;)V

    .line 30
    iput-object p1, p0, Lbf/az;->d:Lcom/google/googlenav/ui/wizard/dg;

    .line 31
    return-void
.end method


# virtual methods
.method public a(LaN/B;)Lcom/google/googlenav/ui/view/d;
    .registers 6
    .parameter

    .prologue
    .line 53
    invoke-virtual {p0}, Lbf/az;->b()Lcom/google/googlenav/ui/view/d;

    move-result-object v1

    .line 56
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 57
    sget-object v0, Lbf/az;->b:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    .line 58
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->f()Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lbf/az;->d:Lcom/google/googlenav/ui/wizard/dg;

    invoke-virtual {v0, p1, v2, v1, v3}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LaN/B;Landroid/view/View;Lcom/google/googlenav/ui/view/d;Lcom/google/googlenav/ui/view/c;)V

    .line 62
    :cond_1f
    return-object v1
.end method

.method public b()Lcom/google/googlenav/ui/view/d;
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    invoke-virtual {p0}, Lbf/az;->a()Landroid/view/View;

    move-result-object v0

    .line 36
    const/16 v1, 0x50b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->e:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    .line 38
    new-array v2, v5, [Lcom/google/googlenav/ui/aW;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/az;->b(Landroid/view/View;Ljava/util/List;)V

    .line 41
    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Ljava/util/List;)V

    .line 42
    invoke-virtual {p0, v0, v4}, Lbf/az;->c(Landroid/view/View;Ljava/util/List;)V

    .line 43
    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Landroid/view/View;)V

    .line 44
    invoke-virtual {p0, v0, v4, v4}, Lbf/az;->a(Landroid/view/View;Lam/f;Lam/f;)V

    .line 45
    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Lam/f;)V

    .line 46
    invoke-virtual {p0, v0, v4}, Lbf/az;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    .line 47
    invoke-virtual {p0, v0, v5}, Lbf/az;->a(Landroid/view/View;Z)V

    .line 49
    sget-object v0, Lbf/az;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/az;->d:Lcom/google/googlenav/ui/wizard/dg;

    invoke-virtual {p0, v0, v1}, Lbf/az;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    return-object v0
.end method
