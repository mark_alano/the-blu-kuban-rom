.class public Lbf/bF;
.super Lbf/h;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/googlenav/ui/bi;


# direct methods
.method public constructor <init>(Lbf/bH;)V
    .registers 3
    .parameter

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    .line 37
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iput-object v0, p0, Lbf/bF;->d:Lcom/google/googlenav/ui/bi;

    .line 38
    return-void
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/bZ;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 79
    if-eqz p1, :cond_25

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->l()I

    move-result v0

    if-lez v0, :cond_25

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v1, Lcom/google/googlenav/ui/bi;->bm:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 82
    :cond_25
    return-object p0
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/cm;)Ljava/lang/String;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x308

    const/16 v9, 0x307

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 106
    invoke-virtual {p1}, Lcom/google/googlenav/cm;->c()Z

    move-result v0

    if-eqz v0, :cond_77

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v0

    .line 109
    :goto_11
    invoke-virtual {p1}, Lcom/google/googlenav/cm;->a()Z

    move-result v3

    .line 110
    if-eqz v3, :cond_8a

    invoke-virtual {p1, v7}, Lcom/google/googlenav/cm;->d(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 117
    :goto_1c
    if-eqz v3, :cond_90

    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_22
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    invoke-static {v1, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    const/16 v4, 0xa

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v5, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 121
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_66

    .line 122
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "..."

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    :cond_66
    if-eqz v3, :cond_95

    .line 126
    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    :goto_76
    return-object v0

    .line 106
    :cond_77
    const/16 v0, 0x4eb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    .line 110
    :cond_8a
    invoke-virtual {p1, v6}, Lcom/google/googlenav/cm;->c(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1c

    .line 117
    :cond_90
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_22

    .line 130
    :cond_95
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_76
.end method


# virtual methods
.method a(Lcom/google/googlenav/bZ;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 91
    .line 92
    invoke-virtual {p1, v1}, Lcom/google/googlenav/bZ;->c(Z)Lcom/google/googlenav/cm;

    move-result-object v0

    .line 93
    if-nez v0, :cond_8

    .line 102
    :cond_7
    :goto_7
    return-void

    .line 96
    :cond_8
    invoke-virtual {v0, v1}, Lcom/google/googlenav/cm;->c(Z)Ljava/lang/String;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_7

    .line 100
    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbf/bF;->a(Ljava/lang/String;Lcom/google/googlenav/cm;)Ljava/lang/String;

    move-result-object v0

    .line 101
    sget-object v1, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7
.end method

.method public b()Lcom/google/googlenav/ui/view/d;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-virtual {p0}, Lbf/bF;->a()Landroid/view/View;

    move-result-object v2

    .line 44
    iget-object v0, p0, Lbf/bF;->c:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cp;

    .line 46
    invoke-virtual {v0}, Lcom/google/googlenav/cp;->c()B

    move-result v3

    if-nez v3, :cond_15

    move-object v0, v1

    .line 73
    :goto_14
    return-object v0

    .line 50
    :cond_15
    invoke-virtual {v0}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    .line 51
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 52
    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->i()Ljava/lang/String;

    move-result-object v4

    .line 55
    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_34

    .line 56
    invoke-static {v4, v0}, Lbf/bF;->a(Ljava/lang/String;Lcom/google/googlenav/bZ;)Ljava/lang/String;

    move-result-object v4

    .line 57
    sget-object v5, Lcom/google/googlenav/ui/aV;->m:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_34
    invoke-virtual {p0, v2, v3}, Lbf/bF;->a(Landroid/view/View;Ljava/util/List;)V

    .line 61
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 62
    invoke-virtual {p0, v0, v3}, Lbf/bF;->a(Lcom/google/googlenav/bZ;Ljava/util/List;)V

    .line 63
    invoke-virtual {p0, v2, v3}, Lbf/bF;->b(Landroid/view/View;Ljava/util/List;)V

    .line 66
    invoke-virtual {p0, v2, v1}, Lbf/bF;->c(Landroid/view/View;Ljava/util/List;)V

    .line 67
    invoke-virtual {p0, v2, v1}, Lbf/bF;->a(Landroid/view/View;Landroid/view/View;)V

    .line 68
    invoke-virtual {p0, v2, v1, v1}, Lbf/bF;->a(Landroid/view/View;Lam/f;Lam/f;)V

    .line 69
    invoke-virtual {p0, v2, v1}, Lbf/bF;->a(Landroid/view/View;Lam/f;)V

    .line 70
    invoke-virtual {p0, v2, v1}, Lbf/bF;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lbf/bF;->a(Landroid/view/View;Z)V

    .line 73
    sget-object v0, Lbf/bF;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/bF;->c:Lbf/i;

    invoke-virtual {p0, v0, v1}, Lbf/bF;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    goto :goto_14
.end method
