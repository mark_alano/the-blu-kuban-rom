.class public Lbf/bU;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaN/m;


# static fields
.field public static final C:Lcom/google/googlenav/ab;

.field private static final O:Lcom/google/googlenav/ab;

.field private static P:Lcom/google/googlenav/ab;


# instance fields
.field B:Lcom/google/googlenav/ui/android/ah;

.field private D:Lax/y;

.field private E:Lcom/google/googlenav/ui/view/u;

.field private F:LaN/k;

.field private G:Z

.field private H:Landroid/widget/ArrayAdapter;

.field private final I:Landroid/widget/AdapterView$OnItemClickListener;

.field private final J:Landroid/os/Handler;

.field private K:Lcom/google/googlenav/layer/s;

.field private final L:Landroid/os/HandlerThread;

.field private M:Landroid/os/Handler;

.field private final N:Ljava/lang/Runnable;

.field private final Q:Ljava/util/ArrayList;

.field private R:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 1190
    new-instance v0, Lcom/google/googlenav/ab;

    const/4 v1, -0x2

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    sput-object v0, Lbf/bU;->O:Lcom/google/googlenav/ab;

    .line 1193
    new-instance v0, Lcom/google/googlenav/ab;

    const/4 v1, -0x1

    const/16 v2, 0x5d5

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    sput-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    .line 1216
    sget-object v0, Lbf/bU;->O:Lcom/google/googlenav/ab;

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/F;Z)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 253
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 155
    new-instance v0, Lbf/bV;

    invoke-direct {v0, p0}, Lbf/bV;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    .line 183
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbf/bU;->J:Landroid/os/Handler;

    .line 189
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "VehicleRequestThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbf/bU;->L:Landroid/os/HandlerThread;

    .line 204
    new-instance v0, Lbf/bW;

    invoke-direct {v0, p0}, Lbf/bW;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->N:Ljava/lang/Runnable;

    .line 1218
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    .line 254
    iput-object p5, p0, Lbf/bU;->F:LaN/k;

    .line 255
    iput-boolean p7, p0, Lbf/bU;->G:Z

    .line 256
    return-void
.end method

.method private a(Landroid/content/Context;I)Landroid/view/View;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 424
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 426
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbf/bU;)Lax/y;
    .registers 2
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Lbf/bU;->bX()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method private a(IILjava/lang/Object;Lcom/google/googlenav/cm;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 725
    if-eqz p4, :cond_11

    .line 726
    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/cm;)V

    .line 729
    :cond_11
    invoke-direct {p0}, Lbf/bU;->bV()V

    .line 731
    invoke-virtual {p0, p2, p3}, Lbf/bU;->a(ILjava/lang/Object;)V

    .line 732
    return-void
.end method

.method private a(ILjava/lang/Object;ILjava/lang/Object;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 714
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->f()I

    move-result v0

    if-ge p1, v0, :cond_c

    if-gez p1, :cond_d

    .line 721
    :cond_c
    :goto_c
    return-void

    .line 718
    :cond_d
    invoke-virtual {p0, p1}, Lbf/bU;->b(I)V

    .line 719
    check-cast p2, Lcom/google/googlenav/cm;

    invoke-direct {p0, p1, p3, p4, p2}, Lbf/bU;->a(IILjava/lang/Object;Lcom/google/googlenav/cm;)V

    goto :goto_c
.end method

.method private a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 419
    const v0, 0x7f10045b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ah;

    iput-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    .line 420
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    iget-object v1, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0, v1}, Lbf/bU;->a(Lcom/google/googlenav/ui/android/ah;Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 421
    return-void
.end method

.method private a(Lax/y;II)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 877
    new-instance v0, Lcom/google/googlenav/aa;

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v5

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/aa;-><init>(Lcom/google/googlenav/Y;Lax/y;IILcom/google/googlenav/ui/bh;)V

    .line 880
    invoke-direct {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/aa;)V

    .line 881
    return-void
.end method

.method private a(Lbf/bG;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 759
    invoke-direct {p0}, Lbf/bU;->bW()V

    .line 760
    new-instance v0, Lbf/cf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbf/cf;-><init>(Lbf/bU;Lbf/bV;)V

    .line 761
    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/googlenav/ui/s;->a(Lbf/bG;Lcom/google/googlenav/bU;Z)Lcom/google/googlenav/bW;

    move-result-object v2

    .line 763
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x4c6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 765
    return-void
.end method

.method static synthetic a(Lbf/bU;Lcom/google/googlenav/aa;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lbf/bU;->b(Lcom/google/googlenav/aa;)V

    return-void
.end method

.method static synthetic a(Lbf/bU;Lcom/google/googlenav/ab;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lbf/bU;->b(Lcom/google/googlenav/ab;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/j;)V
    .registers 4
    .parameter

    .prologue
    .line 1269
    if-nez p0, :cond_3

    .line 1285
    :cond_2
    :goto_2
    return-void

    .line 1273
    :cond_3
    sget-object v0, Lbf/bU;->O:Lcom/google/googlenav/ab;

    sget-object v1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1277
    :try_start_d
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1278
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1279
    sget-object v2, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 1280
    sget-object v2, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1281
    const-string v1, "TRANSIT_VEHICLE_TYPE"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p0, v1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_32} :catch_33

    goto :goto_2

    .line 1282
    :catch_33
    move-exception v0

    goto :goto_2
.end method

.method static synthetic b(Lbf/bU;)Lax/y;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lbf/bU;->D:Lax/y;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/aa;)V
    .registers 6
    .parameter

    .prologue
    .line 1431
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 1432
    sget-object v1, Lbf/bU;->C:Lcom/google/googlenav/ab;

    sget-object v2, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 1433
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget-object v3, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v3}, Lcom/google/googlenav/ab;->a()I

    move-result v3

    aput v3, v1, v2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/aa;->a([I)V

    .line 1435
    :cond_1d
    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    .line 1436
    return-void
.end method

.method private b(Lcom/google/googlenav/ab;)V
    .registers 4
    .parameter

    .prologue
    .line 442
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbg/q;

    .line 444
    if-eqz v0, :cond_13

    .line 445
    invoke-virtual {v0, p1}, Lbg/q;->a(Lcom/google/googlenav/ab;)V

    .line 447
    :cond_13
    return-void
.end method

.method public static bL()Lcom/google/googlenav/ab;
    .registers 2

    .prologue
    .line 1229
    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    sget-object v1, Lbf/bU;->O:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1230
    invoke-static {}, Lbf/bU;->cc()Lcom/google/googlenav/ab;

    move-result-object v0

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    .line 1232
    :cond_10
    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    return-object v0
.end method

.method public static bM()V
    .registers 1

    .prologue
    .line 1240
    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    .line 1241
    return-void
.end method

.method private bP()V
    .registers 2

    .prologue
    .line 284
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 291
    :goto_a
    return-void

    .line 289
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bU;->b(I)V

    .line 290
    invoke-virtual {p0}, Lbf/bU;->an()Z

    goto :goto_a
.end method

.method private bQ()V
    .registers 13

    .prologue
    const v10, 0x3f99999a

    const/4 v4, 0x0

    .line 299
    const/4 v0, 0x3

    iget-object v1, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 300
    if-gez v5, :cond_12

    .line 339
    :cond_11
    :goto_11
    return-void

    .line 303
    :cond_12
    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v6

    .line 304
    invoke-virtual {v6}, LaN/B;->c()I

    move-result v7

    .line 305
    invoke-virtual {v6}, LaN/B;->e()I

    move-result v8

    move v3, v4

    move v2, v4

    move v1, v4

    .line 309
    :goto_23
    if-ge v3, v5, :cond_5d

    .line 310
    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v3}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v9

    .line 311
    if-nez v9, :cond_37

    move v0, v2

    .line 309
    :goto_32
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_23

    .line 314
    :cond_37
    invoke-virtual {v9}, LaN/B;->c()I

    move-result v0

    sub-int v0, v7, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 315
    if-le v0, v1, :cond_8e

    .line 318
    :goto_43
    invoke-virtual {v9}, LaN/B;->e()I

    move-result v1

    sub-int v1, v8, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 319
    const v9, 0xaba9500

    if-le v1, v9, :cond_57

    .line 321
    const v9, 0x15752a00

    sub-int v1, v9, v1

    .line 323
    :cond_57
    if-le v1, v2, :cond_8b

    move v11, v1

    move v1, v0

    move v0, v11

    .line 324
    goto :goto_32

    .line 327
    :cond_5d
    if-nez v1, :cond_61

    if-eqz v2, :cond_11

    .line 331
    :cond_61
    mul-int/lit8 v0, v1, 0x2

    int-to-float v0, v0

    mul-float/2addr v0, v10

    float-to-int v0, v0

    .line 332
    mul-int/lit8 v1, v2, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v10

    float-to-int v1, v1

    .line 333
    iget-object v2, p0, Lbf/bU;->d:LaN/u;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v3

    invoke-virtual {v2, v0, v1, v4, v3}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v0

    .line 336
    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/Y;->b(LaN/Y;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 337
    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1, v6, v0}, LaN/u;->d(LaN/B;LaN/Y;)V

    goto :goto_11

    :cond_8b
    move v1, v0

    move v0, v2

    goto :goto_32

    :cond_8e
    move v0, v1

    goto :goto_43
.end method

.method private bR()V
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 363
    invoke-direct {p0}, Lbf/bU;->bS()Z

    move-result v0

    if-nez v0, :cond_8

    .line 392
    :cond_7
    :goto_7
    return-void

    .line 367
    :cond_8
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 368
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    .line 369
    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->n()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0401be

    invoke-direct {p0, v1, v2}, Lbf/bU;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 371
    invoke-direct {p0, v1}, Lbf/bU;->a(Landroid/view/View;)V

    .line 372
    invoke-direct {p0}, Lbf/bU;->bT()Landroid/app/ActionBar$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/actionbar/a;->b(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_7

    .line 374
    :cond_2c
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_64

    .line 375
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    .line 376
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->o()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0401bf

    invoke-direct {p0, v0, v2}, Lbf/bU;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    .line 378
    invoke-direct {p0, v2}, Lbf/bU;->a(Landroid/view/View;)V

    .line 379
    const v0, 0x7f100009

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 380
    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 381
    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->c()Lcom/google/googlenav/actionbar/b;

    move-result-object v4

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    .line 383
    invoke-direct {p0}, Lbf/bU;->bT()Landroid/app/ActionBar$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_7

    .line 384
    :cond_64
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 385
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->g()Landroid/view/View;

    move-result-object v0

    .line 386
    if-eqz v0, :cond_7

    .line 387
    const v1, 0x7f100204

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ah;

    iput-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    .line 388
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    iget-object v1, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0, v1}, Lbf/bU;->a(Lcom/google/googlenav/ui/android/ah;Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 389
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7
.end method

.method private bS()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 395
    const/4 v0, 0x0

    .line 396
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ar()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 398
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    .line 399
    const v2, 0x7f10045b

    invoke-virtual {v0, v2}, Lcom/google/googlenav/actionbar/a;->a(I)Landroid/view/View;

    move-result-object v0

    .line 409
    :cond_17
    :goto_17
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3e

    :cond_1f
    const/4 v0, 0x1

    :goto_20
    move v1, v0

    :cond_21
    return v1

    .line 400
    :cond_22
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aq()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 401
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->g()Landroid/view/View;

    move-result-object v2

    .line 402
    if-eqz v2, :cond_17

    .line 403
    const v0, 0x7f100204

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_17

    :cond_3e
    move v0, v1

    .line 409
    goto :goto_20
.end method

.method private bT()Landroid/app/ActionBar$LayoutParams;
    .registers 4

    .prologue
    const/4 v2, -0x2

    .line 413
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    const/16 v1, 0x13

    invoke-direct {v0, v2, v2, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method private bU()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 454
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    .line 455
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 456
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->j()V

    .line 468
    :cond_12
    :goto_12
    return-void

    .line 457
    :cond_13
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 458
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->o()Landroid/content/Context;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 460
    const/high16 v2, 0x7f04

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 461
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 462
    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->c()Lcom/google/googlenav/actionbar/b;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    .line 464
    invoke-direct {p0}, Lbf/bU;->bT()Landroid/app/ActionBar$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_12

    .line 465
    :cond_44
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    if-eqz v0, :cond_12

    .line 466
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_12
.end method

.method private bV()V
    .registers 3

    .prologue
    .line 735
    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    .line 736
    if-eqz v0, :cond_16

    .line 737
    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    .line 738
    if-eqz v0, :cond_16

    .line 739
    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->R()V

    .line 740
    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    .line 743
    :cond_16
    return-void
.end method

.method private bW()V
    .registers 4

    .prologue
    .line 768
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {p0}, Lbf/bU;->af()Z

    move-result v0

    if-eqz v0, :cond_18

    const/16 v0, 0x1f

    :goto_e
    iget-object v2, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v0, v2}, Lbf/bU;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 772
    return-void

    .line 768
    :cond_18
    const/16 v0, 0x20

    goto :goto_e
.end method

.method private bX()Lax/y;
    .registers 3

    .prologue
    .line 857
    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    .line 858
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method private bY()V
    .registers 4

    .prologue
    .line 865
    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v0

    const/16 v1, 0x4e20

    const/16 v2, 0xa

    invoke-direct {p0, v0, v1, v2}, Lbf/bU;->a(Lax/y;II)V

    .line 867
    return-void
.end method

.method private bZ()V
    .registers 7

    .prologue
    .line 1046
    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    if-nez v0, :cond_5

    .line 1071
    :goto_4
    return-void

    .line 1051
    :cond_5
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 1053
    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    .line 1054
    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_24

    .line 1055
    iget-object v0, p0, Lbf/bU;->c:LaN/p;

    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/k;)V

    .line 1059
    :cond_24
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1060
    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 1061
    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, p0}, LaN/k;->a(LaN/m;)V

    .line 1062
    iget-object v1, p0, Lbf/bU;->c:LaN/p;

    iget-object v2, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, v2}, LaN/p;->a(LaN/k;)V

    .line 1063
    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    .line 1064
    invoke-virtual {p0}, Lbf/bU;->aP()V

    .line 1069
    :goto_48
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, Lbf/bU;->F:LaN/k;

    iget-object v4, p0, Lbf/bU;->c:LaN/p;

    iget-object v5, p0, Lbf/bU;->d:LaN/u;

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    goto :goto_4

    .line 1066
    :cond_5b
    invoke-direct {p0}, Lbf/bU;->cd()V

    goto :goto_48
.end method

.method static synthetic c(Lbf/bU;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/cp;)V
    .registers 5
    .parameter

    .prologue
    .line 538
    invoke-virtual {p1}, Lcom/google/googlenav/cp;->m()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 539
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->n()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;I)V

    .line 545
    :goto_13
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bU;->h(I)V

    .line 546
    const/4 v1, 0x2

    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v1, v0}, Lbf/bU;->a(ILcom/google/googlenav/ai;)V

    .line 550
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbf/bU;->f(I)V

    .line 551
    return-void

    .line 541
    :cond_27
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(LaN/B;)V

    goto :goto_13
.end method

.method private ca()V
    .registers 3

    .prologue
    .line 1176
    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    if-nez v0, :cond_5

    .line 1182
    :goto_4
    return-void

    .line 1180
    :cond_5
    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    iget-object v1, p0, Lbf/bU;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1181
    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    iget-object v1, p0, Lbf/bU;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4
.end method

.method private cb()V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 1197
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v2

    .line 1199
    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1200
    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    sget-object v3, Lbf/bU;->C:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1201
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_1a
    if-ge v1, v3, :cond_28

    .line 1202
    iget-object v4, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1201
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    .line 1204
    :cond_28
    iget-object v1, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_47

    .line 1205
    iget-object v1, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1206
    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_37
    if-ge v0, v1, :cond_47

    .line 1207
    iget-object v2, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1206
    add-int/lit8 v0, v0, 0x1

    goto :goto_37

    .line 1210
    :cond_47
    return-void
.end method

.method private static cc()Lcom/google/googlenav/ab;
    .registers 4

    .prologue
    .line 1250
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 1251
    if-eqz v0, :cond_12

    const-string v1, "TRANSIT_VEHICLE_TYPE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_15

    .line 1252
    :cond_12
    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    .line 1262
    :cond_14
    :goto_14
    return-object v0

    .line 1256
    :cond_15
    :try_start_15
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const-string v3, "TRANSIT_VEHICLE_TYPE"

    invoke-interface {v0, v3}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1258
    new-instance v0, Lcom/google/googlenav/ab;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_32} :catch_3d

    .line 1262
    :goto_32
    sget-object v1, Lbf/bU;->O:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    goto :goto_14

    .line 1259
    :catch_3d
    move-exception v0

    .line 1260
    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    goto :goto_32
.end method

.method private cd()V
    .registers 3

    .prologue
    .line 1566
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 1567
    invoke-static {p0}, Lbf/am;->m(Lbf/i;)Ljava/lang/String;

    move-result-object v1

    .line 1568
    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 1569
    return-void
.end method

.method static synthetic d(Lbf/bU;)V
    .registers 1
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Lbf/bU;->cb()V

    return-void
.end method

.method static synthetic e(Lbf/bU;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lbf/bU;->J:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lbf/bU;)V
    .registers 1
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Lbf/bU;->ca()V

    return-void
.end method

.method private j(I)V
    .registers 5
    .parameter

    .prologue
    .line 686
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_32

    .line 687
    invoke-virtual {p0}, Lbf/bU;->ae()Z

    move-result v0

    if-eqz v0, :cond_2f

    const/16 v0, 0x22

    :goto_12
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/bU;->b(ILjava/lang/Object;)V

    .line 700
    :goto_19
    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bU;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->b(LaN/B;)V

    .line 702
    return-void

    .line 687
    :cond_2f
    const/16 v0, 0x21

    goto :goto_12

    .line 694
    :cond_32
    invoke-virtual {p0}, Lbf/bU;->an()Z

    goto :goto_19
.end method

.method private l(Lcom/google/googlenav/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 936
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbg/q;

    .line 938
    if-eqz v0, :cond_16

    .line 940
    if-nez p1, :cond_17

    const/4 v1, 0x0

    :goto_13
    invoke-virtual {v0, v1}, Lbg/q;->b(Ljava/lang/String;)V

    .line 944
    :cond_16
    return-void

    .line 940
    :cond_17
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_24

    check-cast p1, Lcom/google/googlenav/W;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_13

    :cond_24
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v1

    goto :goto_13
.end method


# virtual methods
.method protected O()Z
    .registers 2

    .prologue
    .line 1075
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 1093
    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1346
    invoke-super {p0}, Lbf/m;->X()Z

    .line 1348
    iget-boolean v1, p0, Lbf/bU;->R:Z

    if-eqz v1, :cond_22

    .line 1349
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->n()V

    .line 1352
    iput-boolean v0, p0, Lbf/bU;->R:Z

    .line 1355
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 1356
    invoke-virtual {p0}, Lbf/bU;->Z()V

    .line 1358
    :cond_1e
    invoke-virtual {p0}, Lbf/bU;->R()V

    .line 1359
    const/4 v0, 0x1

    .line 1361
    :cond_22
    return v0
.end method

.method public Z()V
    .registers 2

    .prologue
    .line 931
    invoke-super {p0}, Lbf/m;->Z()V

    .line 932
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->d(Lbf/i;)V

    .line 933
    return-void
.end method

.method public a()Landroid/widget/AdapterView$OnItemClickListener;
    .registers 2

    .prologue
    .line 450
    iget-object v0, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 1302
    invoke-super {p0, p1}, Lbf/m;->a(Landroid/content/res/Configuration;)V

    .line 1303
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_14

    .line 1304
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/aX;

    if-eqz v0, :cond_14

    .line 1305
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->h()V

    .line 1308
    :cond_14
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    if-eqz v0, :cond_25

    .line 1311
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ah;->b()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1312
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ah;->a()V

    .line 1315
    :cond_25
    return-void
.end method

.method public a(Lax/y;)V
    .registers 2
    .parameter

    .prologue
    .line 814
    iput-object p1, p0, Lbf/bU;->D:Lax/y;

    .line 815
    return-void
.end method

.method public a(Lax/y;Lax/y;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1331
    invoke-virtual {p0, p2}, Lbf/bU;->a(Lax/y;)V

    .line 1333
    if-nez p3, :cond_9

    .line 1334
    invoke-static {p1}, Lcom/google/googlenav/ui/bv;->c(Lax/y;)Ljava/lang/String;

    move-result-object p3

    .line 1336
    :cond_9
    new-instance v2, Lcom/google/googlenav/aa;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v0

    invoke-direct {v2, p1, p3, v0}, Lcom/google/googlenav/aa;-><init>(Lax/y;Ljava/lang/String;Lcom/google/googlenav/ui/bh;)V

    .line 1338
    invoke-virtual {v2, p2}, Lcom/google/googlenav/aa;->a(Lax/y;)V

    .line 1339
    invoke-direct {p0, v2}, Lbf/bU;->b(Lcom/google/googlenav/aa;)V

    .line 1340
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x4c7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 1342
    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 597
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    .line 598
    iput-object p1, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    .line 599
    check-cast p1, Lcom/google/googlenav/Y;

    .line 600
    invoke-virtual {p1, v0}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    .line 601
    invoke-virtual {p1}, Lcom/google/googlenav/Y;->g()Lax/y;

    move-result-object v0

    iput-object v0, p0, Lbf/bU;->D:Lax/y;

    .line 603
    invoke-direct {p0}, Lbf/bU;->cb()V

    .line 604
    return-void
.end method

.method public a(Lcom/google/googlenav/aa;)V
    .registers 5
    .parameter

    .prologue
    .line 998
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->l()Lcom/google/googlenav/Y;

    move-result-object v0

    .line 1000
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->i()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 1001
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    .line 1002
    invoke-virtual {v1}, Lcom/google/googlenav/Y;->d()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(B)V

    .line 1003
    invoke-virtual {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/F;)V

    .line 1005
    invoke-virtual {p0}, Lbf/bU;->ae()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1006
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->l()V

    .line 1043
    :cond_25
    :goto_25
    return-void

    .line 1009
    :cond_26
    invoke-virtual {v0}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5b

    .line 1010
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5b

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->f()LaN/B;

    move-result-object v2

    invoke-virtual {v1, v2}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 1013
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(Ljava/lang/String;)V

    .line 1016
    :cond_5b
    invoke-virtual {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/F;)V

    .line 1017
    invoke-direct {p0}, Lbf/bU;->bZ()V

    .line 1018
    invoke-direct {p0}, Lbf/bU;->bQ()V

    .line 1019
    invoke-direct {p0}, Lbf/bU;->bP()V

    .line 1023
    iget-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_9c

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    if-eqz v0, :cond_9c

    .line 1024
    iget-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    sget-object v1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1025
    if-ltz v0, :cond_7e

    .line 1026
    iget-object v1, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ah;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/android/ah;->setSelection(I)V

    .line 1029
    :cond_7e
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 1030
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->g()Landroid/view/View;

    move-result-object v0

    .line 1031
    if-eqz v0, :cond_9c

    .line 1032
    const v1, 0x7f100202

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1033
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1037
    :cond_9c
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/aX;

    if-eqz v0, :cond_25

    .line 1038
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/bU;->b(B)V

    .line 1039
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->l()V

    .line 1040
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->m()V

    goto/16 :goto_25
.end method

.method public a(Lcom/google/googlenav/ab;)V
    .registers 8
    .parameter

    .prologue
    .line 430
    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 439
    :goto_8
    return-void

    .line 433
    :cond_9
    sput-object p1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    .line 434
    invoke-virtual {p0}, Lbf/bU;->bI()V

    .line 435
    const/16 v0, 0x73

    const-string v1, "f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/ab;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method public a(Lcom/google/googlenav/cp;)V
    .registers 8
    .parameter

    .prologue
    .line 494
    invoke-virtual {p1}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v2

    .line 495
    if-nez v2, :cond_7

    .line 531
    :goto_6
    return-void

    .line 500
    :cond_7
    new-instance v0, Lcom/google/googlenav/layer/p;

    const-string v1, "LayerTransit"

    invoke-virtual {v2}, Lcom/google/googlenav/bZ;->h()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-instance v5, Lbf/bY;

    invoke-direct {v5, p0, p1}, Lbf/bY;-><init>(Lbf/bU;Lcom/google/googlenav/cp;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/layer/p;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/googlenav/layer/q;)V

    .line 530
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    goto :goto_6
.end method

.method public a(Lcom/google/googlenav/ui/android/ah;Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1125
    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_1c

    .line 1127
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 1128
    const v0, 0x1090009

    .line 1132
    :goto_11
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    .line 1135
    :cond_1c
    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1136
    const/4 v0, 0x0

    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_28
    if-ge v0, v1, :cond_3c

    .line 1137
    iget-object v2, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1136
    add-int/lit8 v0, v0, 0x1

    goto :goto_28

    .line 1130
    :cond_38
    const v0, 0x7f0401cf

    goto :goto_11

    .line 1140
    :cond_3c
    new-instance v0, Lbf/cc;

    invoke-direct {v0, p0}, Lbf/cc;-><init>(Lbf/bU;)V

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ah;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1161
    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ah;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1162
    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    sget-object v1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 1163
    const/4 v1, -0x1

    if-eq v0, v1, :cond_57

    .line 1164
    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ah;->setSelection(I)V

    .line 1166
    :cond_57
    invoke-interface {p1, p2}, Lcom/google/googlenav/ui/android/ah;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1167
    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .registers 5
    .parameter

    .prologue
    .line 1550
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->x()Lbf/bU;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 1551
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1552
    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1554
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v0

    .line 1555
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1556
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    .line 1557
    invoke-virtual {v0}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1558
    invoke-virtual {v0}, Lcom/google/googlenav/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    goto :goto_2e

    .line 1561
    :cond_49
    invoke-direct {p0}, Lbf/bU;->cd()V

    .line 1563
    :cond_4c
    return-void
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1473
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1474
    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1475
    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/m;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1477
    :cond_15
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 608
    sparse-switch p1, :sswitch_data_c6

    .line 675
    :cond_6
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v1

    :goto_a
    return v1

    .line 610
    :sswitch_b
    invoke-virtual {p0}, Lbf/bU;->f()V

    goto :goto_a

    .line 613
    :sswitch_f
    iget-object v0, p0, Lbf/bU;->D:Lax/y;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lbf/bU;->D:Lax/y;

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 614
    :cond_1f
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v4, v2}, Lbf/bU;->a(Lax/y;Lax/y;Ljava/lang/String;)V

    goto :goto_a

    .line 617
    :cond_33
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    iget-object v3, p0, Lbf/bU;->D:Lax/y;

    invoke-virtual {v3}, Lax/y;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->i(Z)Lcom/google/googlenav/bg;

    move-result-object v2

    new-instance v3, Lbf/ca;

    invoke-direct {v3, p0}, Lbf/ca;-><init>(Lbf/bU;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/ba;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    goto :goto_a

    .line 633
    :sswitch_59
    iget-object v2, p0, Lbf/bU;->D:Lax/y;

    if-nez v2, :cond_5e

    move v0, v1

    .line 634
    :cond_5e
    check-cast p3, Lbf/bG;

    invoke-direct {p0, p3, v0}, Lbf/bU;->a(Lbf/bG;Z)V

    goto :goto_a

    .line 637
    :sswitch_64
    invoke-direct {p0}, Lbf/bU;->bY()V

    goto :goto_a

    .line 641
    :sswitch_68
    invoke-direct {p0}, Lbf/bU;->bW()V

    .line 642
    const-string v0, "d"

    invoke-virtual {p0, v0}, Lbf/bU;->c(Ljava/lang/String;)V

    .line 643
    invoke-virtual {p0, v4}, Lbf/bU;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_a

    .line 647
    :sswitch_74
    iget-object v2, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_83

    .line 648
    const/16 v0, 0x22

    .line 652
    :cond_7f
    :goto_7f
    invoke-direct {p0, p2, p3, v0, v4}, Lbf/bU;->a(ILjava/lang/Object;ILjava/lang/Object;)V

    goto :goto_a

    .line 649
    :cond_83
    iget-object v2, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    if-ne v2, v1, :cond_7f

    .line 650
    const/16 v0, 0x23

    goto :goto_7f

    .line 655
    :sswitch_8e
    if-ltz p2, :cond_93

    .line 656
    invoke-virtual {p0, p2}, Lbf/bU;->b(I)V

    .line 658
    :cond_93
    const-string v0, "m"

    invoke-virtual {p0, v0}, Lbf/bU;->c(Ljava/lang/String;)V

    .line 659
    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lbf/bU;->j(I)V

    goto/16 :goto_a

    .line 662
    :sswitch_a3
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 663
    const-string v0, "Street View"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    .line 665
    :cond_ae
    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 666
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v2

    if-nez v2, :cond_6

    .line 670
    const-string v2, "st"

    invoke-virtual {p0, v2}, Lbf/bU;->c(Ljava/lang/String;)V

    .line 671
    check-cast v0, Lcom/google/googlenav/cp;

    invoke-direct {p0, v0}, Lbf/bU;->c(Lcom/google/googlenav/cp;)V

    goto/16 :goto_a

    .line 608
    :sswitch_data_c6
    .sparse-switch
        0x1 -> :sswitch_8e
        0xd5 -> :sswitch_b
        0x258 -> :sswitch_a3
        0x25b -> :sswitch_68
        0x262 -> :sswitch_68
        0x264 -> :sswitch_f
        0xfa1 -> :sswitch_74
        0xfa3 -> :sswitch_64
        0xfa4 -> :sswitch_59
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 1098
    iget-object v1, p0, Lbf/bU;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_4b

    iget-object v1, p0, Lbf/bU;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v1, :cond_4b

    .line 1100
    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(I)V

    .line 1102
    const/16 v1, 0x73

    const-string v2, "m"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "a=b"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/Y;->c()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1107
    const/16 v1, 0x23

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bU;->a(ILjava/lang/Object;)V

    .line 1110
    :goto_4a
    return v0

    :cond_4b
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_4a
.end method

.method protected a(Ljava/io/DataInput;)Z
    .registers 8
    .parameter

    .prologue
    .line 1578
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1580
    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1581
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/layer/m;)V

    .line 1582
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v1

    .line 1583
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1584
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 1585
    const/4 v0, 0x0

    :goto_22
    if-ge v0, v2, :cond_37

    .line 1586
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v3

    .line 1587
    invoke-interface {p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 1588
    new-instance v5, Lcom/google/googlenav/ab;

    invoke-direct {v5, v3, v4}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1585
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 1590
    :cond_37
    const/4 v0, 0x1

    return v0
.end method

.method public aB()Z
    .registers 2

    .prologue
    .line 1542
    const/4 v0, 0x1

    return v0
.end method

.method public aG()I
    .registers 2

    .prologue
    .line 982
    const v0, 0x7f02021f

    return v0
.end method

.method public aH()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 987
    const/16 v0, 0x5d4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1080
    const/16 v0, 0x2fc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1595
    const/16 v0, 0x23e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 1536
    const/4 v0, 0x1

    return v0
.end method

.method protected aT()Z
    .registers 7

    .prologue
    .line 260
    invoke-static {}, Lbf/bU;->bL()Lcom/google/googlenav/ab;

    move-result-object v0

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    .line 261
    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-direct {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/ab;)V

    .line 263
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 266
    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, p0}, LaN/k;->a(LaN/m;)V

    .line 267
    iget-object v1, p0, Lbf/bU;->c:LaN/p;

    iget-object v2, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, v2}, LaN/p;->a(LaN/k;)V

    .line 268
    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    .line 270
    :cond_2c
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, Lbf/bU;->F:LaN/k;

    iget-object v4, p0, Lbf/bU;->c:LaN/p;

    iget-object v5, p0, Lbf/bU;->d:LaN/u;

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    .line 273
    iget-boolean v0, p0, Lbf/bU;->G:Z

    if-eqz v0, :cond_45

    .line 274
    invoke-direct {p0}, Lbf/bU;->bQ()V

    .line 277
    :cond_45
    iget-object v0, p0, Lbf/bU;->L:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 278
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lbf/bU;->L:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    .line 280
    invoke-super {p0}, Lbf/m;->aT()Z

    move-result v0

    return v0
.end method

.method public aU()V
    .registers 3

    .prologue
    .line 343
    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    if-eqz v0, :cond_12

    .line 344
    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_f

    .line 346
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 348
    :cond_f
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    .line 351
    :cond_12
    invoke-direct {p0}, Lbf/bU;->bU()V

    .line 352
    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    invoke-direct {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/ab;)V

    .line 354
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 356
    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    .line 357
    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_39

    .line 358
    iget-object v0, p0, Lbf/bU;->c:LaN/p;

    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/k;)V

    .line 360
    :cond_39
    return-void
.end method

.method public aW()V
    .registers 2

    .prologue
    .line 952
    invoke-super {p0}, Lbf/m;->aW()V

    .line 955
    invoke-direct {p0}, Lbf/bU;->bR()V

    .line 957
    iget-boolean v0, p0, Lbf/bU;->G:Z

    if-eqz v0, :cond_10

    .line 958
    invoke-direct {p0}, Lbf/bU;->bP()V

    .line 959
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/bU;->G:Z

    .line 961
    :cond_10
    return-void
.end method

.method protected am()V
    .registers 3

    .prologue
    .line 965
    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 966
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-nez v1, :cond_25

    .line 967
    check-cast v0, Lcom/google/googlenav/cp;

    .line 968
    invoke-virtual {v0}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bZ;->f()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 970
    invoke-virtual {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/cp;)V

    .line 972
    :cond_1b
    invoke-virtual {v0}, Lcom/google/googlenav/cp;->j()Z

    move-result v1

    if-nez v1, :cond_24

    .line 973
    invoke-virtual {p0, v0}, Lbf/bU;->a(Lcom/google/googlenav/cp;)V

    .line 978
    :cond_24
    :goto_24
    return-void

    .line 976
    :cond_25
    iget-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Law/g;

    goto :goto_24
.end method

.method protected aq()V
    .registers 2

    .prologue
    .line 477
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/aX;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 478
    return-void
.end method

.method public synthetic ar()Lcom/google/googlenav/F;
    .registers 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    return-object v0
.end method

.method public av()I
    .registers 2

    .prologue
    .line 806
    const/16 v0, 0x17

    return v0
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .registers 2

    .prologue
    .line 1468
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 924
    invoke-super {p0, p1}, Lbf/m;->b(I)V

    .line 925
    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-direct {p0, v0}, Lbf/bU;->l(Lcom/google/googlenav/ai;)V

    .line 926
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->d(Lbf/i;)V

    .line 927
    return-void
.end method

.method b(Lcom/google/googlenav/cp;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1501
    invoke-virtual {p1}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    .line 1502
    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->e()Z

    move-result v1

    .line 1503
    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->f()Z

    move-result v2

    .line 1505
    invoke-virtual {v0, v3}, Lcom/google/googlenav/bZ;->a(Z)V

    .line 1506
    invoke-virtual {v0, v3}, Lcom/google/googlenav/bZ;->b(Z)V

    .line 1507
    new-instance v3, Lcom/google/googlenav/ch;

    new-instance v4, Lbf/cd;

    invoke-direct {v4, p0, v0, v1, v2}, Lbf/cd;-><init>(Lbf/bU;Lcom/google/googlenav/bZ;ZZ)V

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/ch;-><init>(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ci;)V

    .line 1524
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 1525
    invoke-virtual {v0, v3}, Law/h;->c(Law/g;)V

    .line 1526
    return-void
.end method

.method public bG()Lcom/google/googlenav/ui/view/u;
    .registers 2

    .prologue
    .line 822
    iget-object v0, p0, Lbf/bU;->E:Lcom/google/googlenav/ui/view/u;

    if-nez v0, :cond_b

    .line 823
    new-instance v0, Lbf/cb;

    invoke-direct {v0, p0}, Lbf/cb;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->E:Lcom/google/googlenav/ui/view/u;

    .line 837
    :cond_b
    iget-object v0, p0, Lbf/bU;->E:Lcom/google/googlenav/ui/view/u;

    return-object v0
.end method

.method public bH()Z
    .registers 2

    .prologue
    .line 841
    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->h()Z

    move-result v0

    return v0
.end method

.method public bI()V
    .registers 4

    .prologue
    .line 851
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    .line 852
    invoke-direct {p0}, Lbf/bU;->bX()Lax/y;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->g()Lax/y;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lbf/bU;->a(Lax/y;Lax/y;Ljava/lang/String;)V

    .line 854
    return-void
.end method

.method public bJ()V
    .registers 2

    .prologue
    .line 947
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 948
    return-void
.end method

.method public bK()V
    .registers 4

    .prologue
    .line 1170
    const/16 v0, 0x73

    const-string v1, "f"

    const-string v2, "t=o"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1173
    return-void
.end method

.method public bN()Lcom/google/googlenav/T;
    .registers 2

    .prologue
    .line 1288
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    return-object v0
.end method

.method public bO()Lcom/google/googlenav/Y;
    .registers 2

    .prologue
    .line 1319
    invoke-super {p0}, Lbf/m;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/Y;

    return-object v0
.end method

.method public c()V
    .registers 2

    .prologue
    .line 1481
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/bU;->R:Z

    .line 1482
    return-void
.end method

.method public d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 587
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 588
    invoke-virtual {p0}, Lbf/bU;->Z()V

    .line 589
    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    .line 590
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2e

    .line 591
    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-static {v0, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;)Lcom/google/googlenav/aa;

    .line 593
    :cond_2e
    return-void
.end method

.method public d(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 482
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 483
    invoke-super {p0, p1}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v0

    .line 485
    :goto_a
    return v0

    :cond_b
    check-cast p1, Lcom/google/googlenav/cp;

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->k()Z

    move-result v0

    goto :goto_a
.end method

.method public f()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 776
    const/16 v0, 0xc2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 779
    invoke-static {}, Lcom/google/googlenav/ui/wizard/dg;->e()I

    move-result v1

    xor-int/lit8 v1, v1, 0x4

    xor-int/lit8 v1, v1, 0x8

    .line 784
    const/16 v2, 0x119

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 787
    const/4 v3, 0x5

    .line 789
    const/16 v4, 0x5e1

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 792
    iget-object v5, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v5

    new-instance v6, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v6}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    new-instance v1, Lbf/ce;

    const/4 v6, 0x0

    invoke-direct {v1, p0, v6}, Lbf/ce;-><init>(Lbf/bU;Lbf/bV;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dF;->d(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 802
    return-void
.end method

.method protected f(Lat/a;)Z
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x23

    const/4 v0, 0x1

    .line 558
    invoke-virtual {p1}, Lat/a;->a()I

    move-result v1

    if-ne v1, v4, :cond_25

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_25

    .line 560
    invoke-virtual {p0}, Lbf/bU;->ae()Z

    move-result v1

    if-nez v1, :cond_25

    .line 561
    const/16 v1, 0x73

    const-string v2, "m"

    const-string v3, "a=l"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 564
    const/4 v1, 0x0

    invoke-virtual {p0, v4, v1}, Lbf/bU;->c(ILjava/lang/Object;)V

    .line 576
    :goto_24
    return v0

    .line 570
    :cond_25
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_37

    invoke-virtual {p0}, Lbf/bU;->aa()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 571
    invoke-virtual {p0}, Lbf/bU;->h()V

    goto :goto_24

    .line 576
    :cond_37
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 885
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 886
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_62

    .line 913
    invoke-virtual {p0}, Lbf/bU;->ag()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 914
    invoke-virtual {p0, v2}, Lbf/bU;->a(B)V

    .line 918
    :goto_1c
    return-void

    .line 888
    :sswitch_1d
    invoke-virtual {p0, v2, v3}, Lbf/bU;->c(ILjava/lang/Object;)V

    goto :goto_1c

    .line 891
    :sswitch_21
    invoke-virtual {p0, v2, v3}, Lbf/bU;->b(ILjava/lang/Object;)V

    goto :goto_1c

    .line 894
    :sswitch_25
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    .line 895
    if-eqz v0, :cond_38

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_38

    .line 896
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bU;->b(I)V

    .line 898
    :cond_38
    invoke-virtual {p0, v2, v3}, Lbf/bU;->a(ILjava/lang/Object;)V

    goto :goto_1c

    .line 901
    :sswitch_3c
    invoke-virtual {p0}, Lbf/bU;->ag()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 902
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {p0, v1, v3}, Lbf/bU;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 904
    invoke-virtual {p0, v2}, Lbf/bU;->a(B)V

    goto :goto_1c

    .line 906
    :cond_55
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_1c

    .line 916
    :cond_5b
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_1c

    .line 886
    nop

    :sswitch_data_62
    .sparse-switch
        0x11 -> :sswitch_3c
        0x21 -> :sswitch_25
        0x22 -> :sswitch_1d
        0x23 -> :sswitch_21
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1486
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1487
    iget-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Law/g;

    move-result-object v1

    if-eqz v1, :cond_10

    const/4 v0, 0x1

    .line 1494
    :cond_10
    :goto_10
    return v0

    .line 1489
    :cond_11
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bZ;->f()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1492
    check-cast p1, Lcom/google/googlenav/cp;

    invoke-virtual {p0, p1}, Lbf/bU;->b(Lcom/google/googlenav/cp;)V

    goto :goto_10
.end method

.method protected i()Lbh/a;
    .registers 2

    .prologue
    .line 472
    new-instance v0, Lbh/l;

    invoke-direct {v0, p0}, Lbh/l;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected l()V
    .registers 2

    .prologue
    .line 1085
    invoke-super {p0}, Lbf/m;->l()V

    .line 1086
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_e

    .line 1087
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->m()V

    .line 1089
    :cond_e
    return-void
.end method

.method protected x()I
    .registers 2

    .prologue
    .line 1531
    const/4 v0, 0x0

    return v0
.end method
