.class public Lbf/be;
.super Lbf/l;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/m;)V
    .registers 2
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lbf/l;-><init>(Lbf/i;)V

    .line 41
    return-void
.end method

.method private a()Lbf/m;
    .registers 2

    .prologue
    .line 223
    iget-object v0, p0, Lbf/be;->a:Lbf/i;

    check-cast v0, Lbf/m;

    return-object v0
.end method

.method static synthetic a(Lbf/be;)Lbf/m;
    .registers 2
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbf/be;Lcom/google/googlenav/ai;)Ljava/util/List;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbf/be;->e(Lcom/google/googlenav/ai;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 94
    iget-object v0, p0, Lbf/be;->a:Lbf/i;

    invoke-virtual {v0, v2}, Lbf/i;->f(Z)V

    .line 95
    new-instance v1, Lcom/google/googlenav/ui/view/android/aV;

    iget-object v0, p0, Lbf/be;->a:Lbf/i;

    check-cast v0, Lbf/m;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/ui/view/android/aV;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    .line 96
    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bn;->b(Z)V

    .line 97
    return-object v1
.end method

.method private c(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/dialog/r;
    .registers 4
    .parameter

    .prologue
    .line 101
    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    .line 103
    new-instance v1, Lbf/bf;

    invoke-direct {v1, p0, v0, p1}, Lbf/bf;-><init>(Lbf/be;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ai;)V

    return-object v1
.end method

.method private d(Lcom/google/googlenav/ai;)Lbf/bg;
    .registers 4
    .parameter

    .prologue
    .line 140
    new-instance v0, Lbf/bg;

    iget-object v1, p0, Lbf/be;->a:Lbf/i;

    invoke-direct {v0, p0, v1, p1}, Lbf/bg;-><init>(Lbf/be;Lbf/i;Lcom/google/googlenav/ai;)V

    return-object v0
.end method

.method private e(Lcom/google/googlenav/ai;)Ljava/util/List;
    .registers 3
    .parameter

    .prologue
    .line 144
    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    .line 145
    invoke-static {p1, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Lbf/i;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private f(Lcom/google/googlenav/ai;)Lbf/bj;
    .registers 5
    .parameter

    .prologue
    .line 185
    new-instance v0, Lbf/bj;

    iget-object v1, p0, Lbf/be;->a:Lbf/i;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->A()[Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lbf/bj;-><init>(Lbf/be;Lbf/i;[Lcom/google/googlenav/ai;)V

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/googlenav/ui/view/android/S;
    .registers 5
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 48
    sparse-switch p1, :sswitch_data_3e

    .line 62
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :sswitch_24
    invoke-direct {p0, v0}, Lbf/be;->b(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    .line 60
    :goto_28
    return-object v0

    .line 54
    :sswitch_29
    invoke-virtual {p0, v0}, Lbf/be;->a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    goto :goto_28

    .line 56
    :sswitch_2e
    invoke-direct {p0, v0}, Lbf/be;->d(Lcom/google/googlenav/ai;)Lbf/bg;

    move-result-object v0

    goto :goto_28

    .line 58
    :sswitch_33
    invoke-direct {p0, v0}, Lbf/be;->f(Lcom/google/googlenav/ai;)Lbf/bj;

    move-result-object v0

    goto :goto_28

    .line 60
    :sswitch_38
    invoke-direct {p0, v0}, Lbf/be;->c(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/dialog/r;

    move-result-object v0

    goto :goto_28

    .line 48
    nop

    :sswitch_data_3e
    .sparse-switch
        0x0 -> :sswitch_29
        0x4 -> :sswitch_2e
        0x5 -> :sswitch_24
        0x7 -> :sswitch_33
        0x14 -> :sswitch_38
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/S;
    .registers 6
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lbf/be;->a()Lbf/m;

    move-result-object v1

    .line 70
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 74
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/a;

    invoke-virtual {v1, p1}, Lbf/m;->j(Lcom/google/googlenav/ai;)Z

    move-result v2

    invoke-virtual {v1, p1}, Lbf/m;->k(Lcom/google/googlenav/ai;)Z

    move-result v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/googlenav/ui/view/dialog/a;-><init>(Lcom/google/googlenav/ai;Lbf/m;ZZ)V

    .line 79
    :goto_21
    return-object v0

    .line 76
    :cond_22
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ak()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 77
    new-instance v0, Lcom/google/googlenav/ui/view/android/aV;

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/ui/view/android/aV;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    goto :goto_21

    .line 79
    :cond_2e
    new-instance v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/ui/view/android/bn;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    goto :goto_21
.end method
