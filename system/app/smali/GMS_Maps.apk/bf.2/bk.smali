.class public Lbf/bk;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaN/m;
.implements LaR/C;
.implements Lcom/google/android/maps/driveabout/vector/h;
.implements Lcom/google/googlenav/aT;
.implements Lcom/google/googlenav/bb;


# instance fields
.field protected volatile B:Z

.field protected C:I

.field protected D:I

.field private E:Z

.field private F:J

.field private G:Lcom/google/googlenav/ui/view/d;

.field private H:J

.field private I:Lcom/google/googlenav/ui/android/ap;

.field private J:J

.field private K:Lcom/google/googlenav/ui/view/d;

.field private L:LaN/H;

.field private M:Z

.field private N:Lcom/google/googlenav/ui/view/u;

.field private O:Ljava/lang/String;

.field private P:Lcom/google/googlenav/layer/m;

.field private Q:LaN/k;

.field private R:Lcom/google/googlenav/layer/s;

.field private S:Z

.field private T:Lai/b;

.field private U:Lai/b;

.field private V:Z

.field private W:Lbm/i;

.field private X:Lbf/ah;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;I)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 301
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    .line 162
    iput-boolean v0, p0, Lbf/bk;->B:Z

    .line 258
    iput-boolean v0, p0, Lbf/bk;->S:Z

    .line 273
    iput-boolean v0, p0, Lbf/bk;->V:Z

    .line 302
    invoke-direct {p0, p5, p6, p7}, Lbf/bk;->a(Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 303
    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 312
    invoke-direct/range {p0 .. p5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 162
    iput-boolean v0, p0, Lbf/bk;->B:Z

    .line 258
    iput-boolean v0, p0, Lbf/bk;->S:Z

    .line 273
    iput-boolean v0, p0, Lbf/bk;->V:Z

    .line 313
    invoke-direct {p0, p6, p7, p8}, Lbf/bk;->a(Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 314
    iget-object v0, p1, Lcom/google/googlenav/ui/s;->f:Lbm/i;

    iput-object v0, p0, Lbf/bk;->W:Lbm/i;

    .line 315
    return-void
.end method

.method static synthetic a(Lbf/bk;)Lai/b;
    .registers 2
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lbf/bk;->T:Lai/b;

    return-object v0
.end method

.method private a(LaW/R;)V
    .registers 6
    .parameter

    .prologue
    .line 1573
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1575
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {p1}, LaW/R;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    const/16 v3, 0x5f6

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v2

    const-string v3, "20"

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 1585
    return-void
.end method

.method static synthetic a(Lbf/bk;Lcom/google/googlenav/aZ;ZZ)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3}, Lbf/bk;->a(Lcom/google/googlenav/aZ;ZZ)V

    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;ZLjava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x2

    .line 618
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/c;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 620
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aM()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 621
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 622
    new-instance v1, Lai/b;

    new-instance v2, Lbf/bl;

    invoke-direct {v2, p0, p1, p2}, Lbf/bl;-><init>(Lbf/bk;Lcom/google/googlenav/aZ;Z)V

    invoke-direct {v1, v3, v0, v2}, Lai/b;-><init>(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lai/c;)V

    iput-object v1, p0, Lbf/bk;->T:Lai/b;

    .line 638
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->T:Lai/b;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 639
    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;ZZ)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 668
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 669
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    .line 672
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/aZ;)V

    .line 674
    invoke-virtual {p0, v1}, Lbf/bk;->b(I)V

    .line 676
    invoke-virtual {p0}, Lbf/bk;->R()V

    .line 678
    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 679
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/aZ;I)V

    .line 739
    :cond_2a
    :goto_2a
    invoke-virtual {p0}, Lbf/bk;->bG()V

    .line 742
    invoke-direct {p0}, Lbf/bk;->bZ()V

    .line 745
    iput-boolean v6, p0, Lbf/bk;->B:Z

    .line 746
    return-void

    .line 682
    :cond_33
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->K()Z

    move-result v0

    if-eqz v0, :cond_79

    .line 683
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x5be

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xa

    const/16 v5, 0x20

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 689
    :cond_79
    if-eqz p2, :cond_9b

    .line 690
    invoke-static {p1}, Lbf/bk;->d(Lcom/google/googlenav/F;)Z

    move-result v0

    if-eqz v0, :cond_88

    .line 692
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    .line 697
    :cond_88
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_98

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v0

    if-nez v0, :cond_9b

    .line 698
    :cond_98
    invoke-virtual {p0}, Lbf/bk;->an()Z

    .line 704
    :cond_9b
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 705
    invoke-direct {p0}, Lbf/bk;->cl()V

    .line 711
    :cond_a4
    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_ab

    .line 712
    invoke-direct {p0, p1}, Lbf/bk;->i(Lcom/google/googlenav/aZ;)V

    .line 716
    :cond_ab
    invoke-direct {p0, p1}, Lbf/bk;->j(Lcom/google/googlenav/aZ;)V

    .line 719
    if-eqz p2, :cond_be

    .line 724
    iput-boolean v7, p0, Lbf/bk;->M:Z

    .line 726
    invoke-virtual {p0, p3}, Lbf/bk;->k(Z)Z

    move-result v0

    if-eqz v0, :cond_be

    .line 728
    invoke-direct {p0}, Lbf/bk;->bY()LaN/H;

    move-result-object v0

    iput-object v0, p0, Lbf/bk;->L:LaN/H;

    .line 732
    :cond_be
    invoke-virtual {p0, p1}, Lbf/bk;->c(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 733
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/bk;->c(ILjava/lang/Object;)V

    goto/16 :goto_2a
.end method

.method private a(Lcom/google/googlenav/be;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1539
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 1540
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v0

    .line 1542
    if-nez v0, :cond_f

    .line 1543
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1546
    :cond_f
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_60

    .line 1547
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1552
    :goto_18
    if-eqz p2, :cond_5f

    .line 1554
    iget-object v2, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Set;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0x5f6

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v3, "20"

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 1565
    :cond_5f
    return-void

    .line 1549
    :cond_60
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_18
.end method

.method private a(Lcom/google/googlenav/layer/m;LaN/k;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 318
    iput-object p1, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    .line 319
    iput-object p2, p0, Lbf/bk;->Q:LaN/k;

    .line 320
    iput p3, p0, Lbf/bk;->C:I

    .line 322
    new-instance v0, Lcom/google/googlenav/layer/s;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v0, p0, Lbf/bk;->R:Lcom/google/googlenav/layer/s;

    .line 324
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_1e

    .line 325
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/u;->a(LaR/C;)V

    .line 331
    :cond_1e
    iget-object v0, p0, Lbf/bk;->t:Lbh/a;

    check-cast v0, Lbh/i;

    invoke-virtual {v0, p3}, Lbh/i;->c(I)V

    .line 333
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bk;->i(Z)V

    .line 334
    invoke-direct {p0}, Lbf/bk;->bW()V

    .line 335
    return-void
.end method

.method private b(ILcom/google/googlenav/ai;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 3137
    const/4 v5, 0x0

    .line 3139
    new-instance v2, Lbf/bu;

    invoke-direct {v2, p0, p1}, Lbf/bu;-><init>(Lbf/bk;I)V

    .line 3158
    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_f

    .line 3159
    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3161
    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbf/bk;->F:J

    .line 3162
    new-instance v0, Lcom/google/googlenav/ui/android/am;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/am;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lbf/bk;Lcom/google/googlenav/ai;Lam/f;)V

    iput-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    .line 3164
    return-void
.end method

.method static synthetic b(Lbf/bk;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Lbf/bk;->ca()V

    return-void
.end method

.method public static bV()J
    .registers 2

    .prologue
    .line 3177
    const-wide/16 v0, 0x1388

    return-wide v0
.end method

.method private bW()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 343
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-nez v0, :cond_6

    .line 374
    :cond_5
    :goto_5
    return-void

    .line 347
    :cond_6
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    .line 348
    instance-of v0, v1, Lbg/b;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 352
    check-cast v0, Lbg/b;

    invoke-virtual {v0}, Lbg/b;->ac()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 360
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aK;->o()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v2

    .line 364
    if-eqz v2, :cond_5

    .line 369
    new-instance v3, Lbf/ah;

    invoke-direct {v3, v0, v2}, Lbf/ah;-><init>(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lcom/google/android/maps/driveabout/vector/aZ;)V

    iput-object v3, p0, Lbf/bk;->X:Lbf/ah;

    .line 372
    iget-object v0, p0, Lbf/bk;->X:Lbf/ah;

    const/4 v2, 0x1

    new-array v2, v2, [I

    aput v4, v2, v4

    invoke-static {v2}, LR/a;->a([I)LR/a;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lbf/am;->a(Lbf/au;LR/a;)V

    goto :goto_5
.end method

.method private bX()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 757
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eq v1, p0, :cond_e

    .line 767
    :cond_d
    :goto_d
    return v0

    .line 763
    :cond_e
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_d

    .line 767
    const/4 v0, 0x1

    goto :goto_d
.end method

.method private bY()LaN/H;
    .registers 7

    .prologue
    .line 793
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 797
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->E()V

    .line 802
    iget-object v1, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->S()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->T()I

    move-result v3

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v4

    invoke-virtual {p0, v4}, Lbf/bk;->c(Z)I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v1

    .line 806
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    .line 807
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ar()Z

    move-result v2

    if-eqz v2, :cond_4b

    .line 809
    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v2

    invoke-virtual {p0, v2}, Lbf/bk;->c(Z)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 810
    invoke-virtual {p0}, Lbf/bk;->q()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    .line 811
    neg-int v2, v2

    neg-int v3, v3

    invoke-virtual {v0, v2, v3, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    .line 817
    :cond_4b
    invoke-virtual {p0, v0, v1}, Lbf/bk;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v0

    .line 820
    iget-object v2, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v2, v0, v1}, LaN/u;->d(LaN/B;LaN/Y;)V

    .line 822
    iget-object v2, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v2, v1}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    return-object v0
.end method

.method private bZ()V
    .registers 6

    .prologue
    .line 860
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 866
    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    :goto_9
    if-ge v0, v2, :cond_17

    .line 867
    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    const/16 v4, 0x10

    invoke-virtual {p0, v3, v4}, Lbf/bk;->a(Lcom/google/googlenav/E;I)V

    .line 866
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 871
    :cond_17
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_29

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_29

    .line 872
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    .line 876
    :cond_29
    return-void
.end method

.method static synthetic c(Lbf/bk;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Lbf/bk;->ch()V

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x4

    const/4 v2, 0x0

    .line 1496
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    .line 1497
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_74

    move v0, v1

    .line 1498
    :goto_11
    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v3

    .line 1500
    if-nez v3, :cond_1c

    .line 1501
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1506
    :cond_1c
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1507
    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_76

    if-nez v0, :cond_76

    .line 1508
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1518
    :cond_2f
    :goto_2f
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v3, 0x5f6

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const-string v3, "20"

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 1528
    return-void

    :cond_74
    move v0, v2

    .line 1497
    goto :goto_11

    .line 1509
    :cond_76
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 1510
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1511
    new-instance v6, Lcom/google/googlenav/bd;

    const-string v7, ""

    invoke-direct {v6, v2, v7, p2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1512
    new-instance v6, Lcom/google/googlenav/bc;

    const/16 v7, 0x12f

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-static {v7, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v8, v1, v0}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2f
.end method

.method private ca()V
    .registers 8

    .prologue
    const/4 v6, 0x3

    .line 1447
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 1448
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    .line 1449
    if-nez v0, :cond_10

    .line 1450
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1455
    :cond_10
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1456
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_62

    .line 1457
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1463
    :goto_21
    iget-object v2, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0x5f6

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v3, "20"

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 1472
    return-void

    .line 1459
    :cond_62
    new-instance v3, Lcom/google/googlenav/bc;

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct {v3, v6, v4, v5}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_21
.end method

.method private cb()V
    .registers 4

    .prologue
    .line 1588
    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1589
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    .line 1596
    :goto_b
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1597
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lbf/bk;->a(ILcom/google/googlenav/ai;)V

    .line 1600
    invoke-direct {p0, v0}, Lbf/bk;->p(Lcom/google/googlenav/ai;)Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1601
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lbf/am;->c(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    .line 1602
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    .line 1603
    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-nez v0, :cond_36

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 1604
    :cond_36
    invoke-virtual {p0}, Lbf/bk;->n()V

    .line 1606
    :cond_39
    return-void

    .line 1590
    :cond_3a
    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 1591
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    goto :goto_b

    .line 1593
    :cond_46
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    goto :goto_b
.end method

.method private cc()V
    .registers 2

    .prologue
    .line 1872
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    .line 1873
    invoke-virtual {p0}, Lbf/bk;->an()Z

    .line 1874
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bk;->b(Z)V

    .line 1875
    return-void
.end method

.method private cd()Ljava/lang/String;
    .registers 5

    .prologue
    .line 1885
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1886
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "n="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1888
    invoke-virtual {p0}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    .line 1889
    if-eqz v1, :cond_56

    .line 1890
    invoke-static {v1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    .line 1891
    if-eqz v1, :cond_56

    .line 1893
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&gmmsmh=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1894
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "u="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1898
    :cond_56
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private ce()Z
    .registers 3

    .prologue
    .line 2003
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_a
    if-ltz v0, :cond_29

    .line 2004
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    if-eqz v1, :cond_26

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_26

    .line 2005
    const/4 v0, 0x0

    .line 2008
    :goto_25
    return v0

    .line 2003
    :cond_26
    add-int/lit8 v0, v0, -0x1

    goto :goto_a

    .line 2008
    :cond_29
    const/4 v0, 0x1

    goto :goto_25
.end method

.method private cf()Z
    .registers 3

    .prologue
    .line 2022
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    .line 2023
    add-int/lit8 v0, v0, 0x1

    :goto_a
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    if-ge v0, v1, :cond_27

    .line 2024
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_24

    .line 2025
    const/4 v0, 0x0

    .line 2028
    :goto_23
    return v0

    .line 2023
    :cond_24
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2028
    :cond_27
    const/4 v0, 0x1

    goto :goto_23
.end method

.method private cg()Lcom/google/googlenav/n;
    .registers 2

    .prologue
    .line 2251
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/n;

    return-object v0
.end method

.method private ch()V
    .registers 2

    .prologue
    .line 3195
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbf/bk;->l(Z)V

    .line 3196
    return-void
.end method

.method private ci()V
    .registers 4

    .prologue
    .line 3199
    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    if-nez v0, :cond_5

    .line 3210
    :goto_4
    return-void

    .line 3203
    :cond_5
    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ap;->c()V

    .line 3204
    const/4 v0, 0x2

    const-string v1, "pubalerts d"

    iget-object v2, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/ap;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3208
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    .line 3209
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbf/bk;->H:J

    goto :goto_4
.end method

.method private cj()V
    .registers 7

    .prologue
    .line 3216
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aP()J

    move-result-wide v0

    .line 3217
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_25

    .line 3218
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    .line 3219
    iget-wide v4, p0, Lbf/bk;->F:J

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_25

    .line 3220
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbf/bk;->l(Z)V

    .line 3223
    :cond_25
    return-void
.end method

.method private ck()V
    .registers 7

    .prologue
    .line 3229
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 3230
    iget-wide v2, p0, Lbf/bk;->H:J

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_28

    .line 3232
    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    if-eqz v0, :cond_28

    .line 3233
    const/4 v0, 0x2

    const-string v1, "pubalerts dt"

    iget-object v2, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/ap;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3237
    invoke-direct {p0}, Lbf/bk;->ci()V

    .line 3240
    :cond_28
    return-void
.end method

.method private cl()V
    .registers 5

    .prologue
    .line 3243
    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_9

    .line 3244
    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3248
    :cond_9
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aq()I

    move-result v0

    if-nez v0, :cond_22

    .line 3249
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    .line 3301
    :goto_21
    return-void

    .line 3254
    :cond_22
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x3a98

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbf/bk;->J:J

    .line 3260
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 3261
    new-instance v1, Lbf/bv;

    invoke-direct {v1, p0}, Lbf/bv;-><init>(Lbf/bk;)V

    .line 3273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xeb

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->ap:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    .line 3296
    :goto_67
    new-instance v2, Lcom/google/googlenav/ui/android/az;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v3

    invoke-direct {v2, v3, v1, v0}, Lcom/google/googlenav/ui/android/az;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/aW;)V

    iput-object v2, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    .line 3299
    const/16 v0, 0x38

    const-string v1, "v"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    goto :goto_21

    .line 3280
    :cond_7e
    new-instance v1, Lbf/bn;

    invoke-direct {v1, p0}, Lbf/bn;-><init>(Lbf/bk;)V

    .line 3292
    const/16 v0, 0xec

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->ap:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    goto :goto_67
.end method

.method private cm()V
    .registers 3

    .prologue
    .line 3304
    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_10

    .line 3305
    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3306
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    .line 3307
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbf/bk;->J:J

    .line 3309
    :cond_10
    return-void
.end method

.method private cn()V
    .registers 5

    .prologue
    .line 3315
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lbf/bk;->J:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_28

    .line 3316
    invoke-direct {p0}, Lbf/bk;->cm()V

    .line 3320
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->q()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 3321
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 3324
    :cond_28
    return-void
.end method

.method static synthetic d(Lbf/bk;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Lbf/bk;->cb()V

    return-void
.end method

.method public static d(Lcom/google/googlenav/F;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1058
    move v1, v2

    :goto_2
    invoke-interface {p0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 1059
    invoke-interface {p0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1060
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1061
    const/4 v2, 0x1

    .line 1064
    :cond_19
    return v2

    .line 1058
    :cond_1a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method static synthetic e(Lbf/bk;)V
    .registers 1
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Lbf/bk;->cm()V

    return-void
.end method

.method private i(Lcom/google/googlenav/aZ;)V
    .registers 3
    .parameter

    .prologue
    .line 2878
    invoke-direct {p0}, Lbf/bk;->bX()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2892
    :goto_6
    return-void

    .line 2884
    :cond_7
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 2885
    if-eqz v0, :cond_11

    .line 2886
    invoke-direct {p0, v0}, Lbf/bk;->o(Lcom/google/googlenav/ai;)V

    goto :goto_6

    .line 2887
    :cond_11
    invoke-virtual {p0, p1}, Lbf/bk;->g(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2888
    invoke-direct {p0, p1}, Lbf/bk;->m(Lcom/google/googlenav/aZ;)V

    goto :goto_6

    .line 2890
    :cond_1b
    invoke-direct {p0, p1}, Lbf/bk;->l(Lcom/google/googlenav/aZ;)Z

    goto :goto_6
.end method

.method private j(Lcom/google/googlenav/aZ;)V
    .registers 5
    .parameter

    .prologue
    .line 2895
    invoke-direct {p0}, Lbf/bk;->bX()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2916
    :cond_6
    :goto_6
    return-void

    .line 2900
    :cond_7
    invoke-direct {p0}, Lbf/bk;->ci()V

    .line 2902
    invoke-direct {p0, p1}, Lbf/bk;->k(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 2903
    if-eqz v0, :cond_6

    .line 2908
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lbf/bk;->H:J

    .line 2909
    new-instance v1, Lcom/google/googlenav/ui/android/ap;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/ui/android/ap;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ai;)V

    iput-object v1, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    .line 2912
    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ap;->b()V

    .line 2913
    const/4 v0, 0x2

    const-string v1, "pubalerts s"

    iget-object v2, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/ap;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method

.method private k(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;
    .registers 5
    .parameter

    .prologue
    .line 2919
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 2920
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 2921
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ac()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2925
    :goto_14
    return-object v0

    .line 2919
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2925
    :cond_19
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private l(I)Lcom/google/googlenav/ai;
    .registers 3
    .parameter

    .prologue
    .line 1047
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private l(Z)V
    .registers 4
    .parameter

    .prologue
    .line 3186
    if-nez p1, :cond_c

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aQ()Z

    move-result v0

    if-nez v0, :cond_1c

    :cond_c
    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1c

    .line 3188
    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3189
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    .line 3190
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbf/bk;->F:J

    .line 3192
    :cond_1c
    return-void
.end method

.method private l(Lcom/google/googlenav/aZ;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 3056
    .line 3059
    invoke-virtual {p1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    check-cast v3, Lcom/google/googlenav/ai;

    .line 3060
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_10

    .line 3125
    :cond_f
    :goto_f
    return v0

    .line 3065
    :cond_10
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_51

    invoke-virtual {p0, v3, v0}, Lbf/bk;->c(Lcom/google/googlenav/ai;Z)Lam/f;

    move-result-object v9

    .line 3067
    :goto_1a
    new-instance v2, Lbf/bt;

    invoke-direct {v2, p0}, Lbf/bt;-><init>(Lbf/bk;)V

    .line 3113
    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_28

    .line 3114
    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 3116
    :cond_28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbf/bk;->F:J

    .line 3118
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 3119
    new-instance v0, Lcom/google/googlenav/ui/android/an;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    invoke-virtual {p0, v3}, Lbf/bk;->j(Lcom/google/googlenav/ai;)Z

    move-result v4

    invoke-virtual {p0, v3}, Lbf/bk;->k(Lcom/google/googlenav/ai;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/an;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ai;ZZ)V

    iput-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    .line 3125
    :goto_4f
    const/4 v0, 0x1

    goto :goto_f

    .line 3065
    :cond_51
    const/4 v9, 0x0

    goto :goto_1a

    .line 3122
    :cond_53
    new-instance v4, Lcom/google/googlenav/ui/android/am;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v5

    move-object v6, v2

    move-object v7, p0

    move-object v8, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/googlenav/ui/android/am;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lbf/bk;Lcom/google/googlenav/ai;Lam/f;)V

    iput-object v4, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    goto :goto_4f
.end method

.method private m(I)I
    .registers 5
    .parameter

    .prologue
    .line 2734
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2735
    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_1d

    .line 2736
    invoke-direct {p0, v0}, Lbf/bk;->l(I)Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 2737
    if-nez p1, :cond_18

    .line 2744
    :goto_17
    return v0

    .line 2740
    :cond_18
    add-int/lit8 p1, p1, -0x1

    .line 2735
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2744
    :cond_1d
    const/4 v0, -0x1

    goto :goto_17
.end method

.method private m(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 3167
    invoke-virtual {p0, p1}, Lbf/bk;->h(Lcom/google/googlenav/aZ;)I

    move-result v1

    .line 3168
    const/4 v0, -0x1

    if-ne v1, v0, :cond_8

    .line 3174
    :goto_7
    return-void

    .line 3172
    :cond_8
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 3173
    invoke-direct {p0, v1, v0}, Lbf/bk;->b(ILcom/google/googlenav/ai;)V

    goto :goto_7
.end method

.method private m(Lcom/google/googlenav/ai;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1845
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ai()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1867
    :goto_8
    :pswitch_8
    return v0

    .line 1850
    :cond_9
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->c()B

    move-result v2

    packed-switch v2, :pswitch_data_14

    :pswitch_10
    move v0, v1

    .line 1867
    goto :goto_8

    :pswitch_12
    move v0, v1

    .line 1852
    goto :goto_8

    .line 1850
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_12
        :pswitch_8
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_10
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method private n(Lcom/google/googlenav/ai;)I
    .registers 5
    .parameter

    .prologue
    .line 2705
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2706
    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_15

    .line 2707
    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    if-ne v2, p1, :cond_12

    .line 2711
    :goto_11
    return v0

    .line 2706
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2711
    :cond_15
    const/4 v0, -0x1

    goto :goto_11
.end method

.method private o(Lcom/google/googlenav/ai;)V
    .registers 5
    .parameter

    .prologue
    .line 2945
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    .line 2946
    new-instance v0, Lbf/bs;

    invoke-direct {v0, p0}, Lbf/bs;-><init>(Lbf/bk;)V

    .line 2959
    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_13

    .line 2960
    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 2962
    :cond_13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lbf/bk;->F:J

    .line 2963
    new-instance v1, Lcom/google/googlenav/ui/android/aB;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v2

    invoke-direct {v1, v2, v0, p0, p1}, Lcom/google/googlenav/ui/android/aB;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lbf/bk;Lcom/google/googlenav/ai;)V

    iput-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    .line 2965
    return-void
.end method

.method private p(Lcom/google/googlenav/ai;)Lcom/google/googlenav/layer/m;
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2968
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2970
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bO()Ljava/lang/String;

    move-result-object v1

    .line 2971
    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2972
    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2973
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bN()Ljava/lang/String;

    move-result-object v1

    .line 2974
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "msid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2975
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2976
    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2978
    const/16 v1, 0x11

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ap()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    .line 2980
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 2982
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2983
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2984
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2985
    const/4 v2, 0x4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2986
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 2987
    const/16 v2, 0x17

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 2989
    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method


# virtual methods
.method public M()Z
    .registers 2

    .prologue
    .line 1983
    invoke-direct {p0}, Lbf/bk;->ce()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lbf/bk;->ah()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public N()Z
    .registers 2

    .prologue
    .line 1990
    const/4 v0, 0x1

    return v0
.end method

.method protected O()Z
    .registers 2

    .prologue
    .line 1955
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 1960
    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-super {p0}, Lbf/m;->P()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected T()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1965
    const/16 v0, 0x4f7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .registers 2

    .prologue
    .line 1970
    const/16 v0, 0xf

    return v0
.end method

.method protected X()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1072
    invoke-super {p0}, Lbf/m;->X()Z

    .line 1076
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->h()[Lcom/google/googlenav/W;

    move-result-object v0

    if-eqz v0, :cond_30

    move v0, v1

    .line 1078
    :goto_10
    iget-boolean v3, p0, Lbf/bk;->S:Z

    if-eqz v3, :cond_32

    if-nez v0, :cond_32

    .line 1079
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->a()V

    .line 1082
    iput-boolean v2, p0, Lbf/bk;->S:Z

    .line 1085
    invoke-virtual {p0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_2c

    .line 1086
    invoke-virtual {p0}, Lbf/bk;->Z()V

    .line 1088
    :cond_2c
    invoke-virtual {p0}, Lbf/bk;->R()V

    .line 1091
    :goto_2f
    return v1

    :cond_30
    move v0, v2

    .line 1076
    goto :goto_10

    :cond_32
    move v1, v2

    .line 1091
    goto :goto_2f
.end method

.method public Y()V
    .registers 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1235
    iget-boolean v2, p0, Lbf/bk;->M:Z

    if-eqz v2, :cond_9

    .line 1236
    iput-boolean v0, p0, Lbf/bk;->M:Z

    .line 1267
    :goto_8
    return-void

    .line 1240
    :cond_9
    iget-object v2, p0, Lbf/bk;->L:LaN/H;

    if-eqz v2, :cond_40

    iget-object v2, p0, Lbf/bk;->L:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v3}, LaN/u;->c()LaN/B;

    move-result-object v3

    iget-object v4, p0, Lbf/bk;->L:LaN/H;

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gez v2, :cond_40

    iget-object v2, p0, Lbf/bk;->L:LaN/H;

    invoke-virtual {v2}, LaN/H;->b()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    iget-object v3, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    if-ge v2, v3, :cond_40

    move v0, v1

    .line 1248
    :cond_40
    const/4 v2, 0x0

    iput-object v2, p0, Lbf/bk;->L:LaN/H;

    .line 1253
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v2

    if-eqz v2, :cond_50

    .line 1254
    invoke-virtual {p0, v1}, Lbf/bk;->e(Z)V

    .line 1257
    :cond_50
    if-eqz v0, :cond_56

    .line 1260
    invoke-virtual {p0, v1}, Lbf/bk;->b(Z)V

    goto :goto_8

    .line 1262
    :cond_56
    invoke-direct {p0}, Lbf/bk;->cj()V

    .line 1263
    invoke-direct {p0}, Lbf/bk;->cn()V

    .line 1264
    invoke-direct {p0}, Lbf/bk;->ck()V

    .line 1265
    invoke-super {p0}, Lbf/m;->Y()V

    goto :goto_8
.end method

.method public a()V
    .registers 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 3340
    iget-boolean v0, p0, Lbf/bk;->V:Z

    if-nez v0, :cond_3e

    iget-object v0, p0, Lbf/bk;->W:Lbm/i;

    if-eqz v0, :cond_3e

    .line 3341
    iget-object v0, p0, Lbf/bk;->W:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    .line 3342
    iget-object v0, p0, Lbf/bk;->W:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->g()I

    move-result v0

    .line 3343
    if-lez v0, :cond_3c

    .line 3344
    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "s=a"

    aput-object v3, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3348
    const-string v1, "stat"

    invoke-static {v5, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3351
    :cond_3c
    iput-boolean v4, p0, Lbf/bk;->V:Z

    .line 3353
    :cond_3e
    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 410
    iput-object p1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    .line 412
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->az()Z

    move-result v0

    if-nez v0, :cond_10

    .line 413
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bk;->b(B)V

    .line 415
    :cond_10
    return-void
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 2514
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    .line 2517
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2518
    if-eqz v1, :cond_fd

    .line 2519
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 2520
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->S()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->T()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v4

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    invoke-static {v0, v2, v3, v4}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/R;

    .line 2525
    :cond_33
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 2526
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->d(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2528
    :cond_40
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4d

    .line 2529
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->e(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2531
    :cond_4d
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5a

    .line 2532
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->f(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2535
    :cond_5a
    const/4 v0, 0x4

    if-eq p3, v0, :cond_61

    const/16 v0, 0xf

    if-ne p3, v0, :cond_76

    .line 2538
    :cond_61
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aa()Lcom/google/googlenav/layer/a;

    move-result-object v0

    .line 2539
    if-eqz v0, :cond_127

    .line 2540
    invoke-virtual {v0}, Lcom/google/googlenav/layer/a;->a()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2541
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(I)Lcom/google/googlenav/R;

    .line 2548
    :cond_76
    :goto_76
    const/16 v0, 0x19

    if-eq p3, v0, :cond_7e

    const/16 v0, 0x18

    if-ne p3, v0, :cond_97

    .line 2550
    :cond_7e
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_97

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_97

    .line 2551
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/Integer;)Lcom/google/googlenav/R;

    .line 2556
    :cond_97
    const/16 v0, 0x1f

    if-ne p3, v0, :cond_fd

    if-eqz p2, :cond_fd

    move-object v0, p2

    .line 2558
    check-cast v0, Lcom/google/googlenav/ai;

    .line 2559
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->Y()I

    move-result v1

    sub-int v1, v4, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->N()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2565
    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2569
    :cond_fd
    if-eqz p2, :cond_10e

    invoke-interface {p2}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_10e

    .line 2570
    check-cast p2, Lcom/google/googlenav/ai;

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->bQ()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/util/List;)Lcom/google/googlenav/R;

    .line 2575
    :cond_10e
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_126

    .line 2576
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2577
    invoke-virtual {p1, v6}, Lcom/google/googlenav/R;->b(Z)Lcom/google/googlenav/R;

    .line 2579
    :cond_126
    return-void

    .line 2544
    :cond_127
    invoke-virtual {p1, v6}, Lcom/google/googlenav/R;->a(Z)Lcom/google/googlenav/R;

    goto/16 :goto_76
.end method

.method public a(Lcom/google/googlenav/aS;)V
    .registers 5
    .parameter

    .prologue
    .line 2776
    invoke-virtual {p1}, Lcom/google/googlenav/aS;->i()I

    move-result v0

    if-nez v0, :cond_a

    .line 2777
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->O:Ljava/lang/String;

    .line 2787
    :cond_9
    :goto_9
    return-void

    .line 2780
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aS;->a(I)Lcom/google/googlenav/aU;

    move-result-object v0

    .line 2781
    invoke-virtual {v0}, Lcom/google/googlenav/aU;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/bk;->O:Ljava/lang/String;

    .line 2783
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2784
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    iget-object v1, p0, Lbf/bk;->O:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aT;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Ljava/lang/String;)V

    goto :goto_9
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2237
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3019
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_9e

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9e

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    .line 3021
    :goto_10
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aK()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 3022
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aL()Lai/d;

    move-result-object v1

    invoke-virtual {v1}, Lai/d;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3026
    :cond_39
    sget-object v1, Lcom/google/googlenav/ui/aV;->ai:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3028
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_a4

    .line 3029
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aj:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3041
    :goto_68
    sget-object v0, Laz/j;->a:Laz/j;

    invoke-virtual {v0}, Laz/j;->d()Z

    move-result v0

    if-nez v0, :cond_9d

    .line 3042
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aj:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3044
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aj:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3047
    :cond_9d
    return-void

    .line 3019
    :cond_9e
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_10

    .line 3031
    :cond_a4
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_cf

    .line 3033
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->ah:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_68

    .line 3037
    :cond_cf
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->ah:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_68
.end method

.method protected a(Lcom/google/googlenav/ui/aW;)V
    .registers 5
    .parameter

    .prologue
    .line 423
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aW;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Ljava/util/List;)V

    .line 424
    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 1010
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1011
    if-eqz v0, :cond_9

    .line 1012
    invoke-virtual {v0, p1}, Lcom/google/googlenav/aZ;->b(Ljava/io/DataOutput;)V

    .line 1014
    :cond_9
    return-void
.end method

.method public a(Ljava/lang/String;LaN/H;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 2824
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->c(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->d(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    .line 2838
    invoke-virtual {p0}, Lbf/bk;->bU()Z

    move-result v0

    if-eqz v0, :cond_60

    .line 2839
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    .line 2842
    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 2843
    if-eqz v0, :cond_6a

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 2844
    :goto_53
    iget-object v2, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v2, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    .line 2846
    :cond_60
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 2847
    return-void

    .line 2843
    :cond_6a
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    goto :goto_53
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 389
    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 390
    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/m;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 392
    :cond_11
    return-void
.end method

.method public a(Ljava/util/Vector;LaN/B;I)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1118
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    .line 1119
    const/4 v0, 0x0

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    :goto_9
    if-ge v0, v1, :cond_29

    .line 1120
    invoke-virtual {v3, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    .line 1123
    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v4

    if-nez v4, :cond_18

    .line 1119
    :cond_15
    :goto_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1126
    :cond_18
    invoke-virtual {p0, v2, p2}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v4

    .line 1127
    int-to-long v6, p3

    cmp-long v6, v4, v6

    if-gez v6, :cond_15

    .line 1128
    invoke-static {p0, v2, v0, v4, v5}, Lbf/ai;->a(Lbf/i;Lcom/google/googlenav/E;IJ)Lbf/ai;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_15

    .line 1133
    :cond_29
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_34

    .line 1163
    :cond_33
    return-void

    .line 1139
    :cond_34
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    .line 1140
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v5

    .line 1141
    invoke-virtual {v5, p2}, Lcom/google/googlenav/T;->a(LaN/B;)Ljava/util/Enumeration;

    move-result-object v6

    .line 1142
    if-eqz v6, :cond_33

    .line 1145
    :cond_42
    :goto_42
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1146
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    .line 1147
    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/aZ;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_42

    .line 1150
    invoke-virtual {p0, v0, p2}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v1

    .line 1151
    int-to-long v7, p3

    cmp-long v7, v1, v7

    if-gez v7, :cond_42

    .line 1153
    iget-boolean v7, p0, Lbf/bk;->o:Z

    if-eqz v7, :cond_69

    .line 1154
    sget v7, Lbf/am;->j:I

    int-to-long v7, v7

    add-long/2addr v1, v7

    .line 1156
    :cond_69
    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v7, v4

    .line 1158
    invoke-static {p0, v0, v7, v1, v2}, Lbf/ai;->a(Lbf/i;Lcom/google/googlenav/E;IJ)Lbf/ai;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_42
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 2290
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lbf/m;->a(Z)V

    .line 2291
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aF()V

    .line 2292
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 2293
    iget-object v0, p0, Lbf/bk;->y:LaB/s;

    invoke-virtual {v0}, LaB/s;->a()V

    .line 2294
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x7

    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v5, 0x1

    .line 1271
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 1272
    sparse-switch p1, :sswitch_data_172

    .line 1440
    :cond_c
    :goto_c
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v5

    :cond_10
    :goto_10
    return v5

    .line 1285
    :sswitch_11
    iput-boolean v9, p0, Lbf/bk;->B:Z

    .line 1286
    iput p2, p0, Lbf/bk;->D:I

    .line 1287
    const-string v0, "d"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    invoke-virtual {p0, p2}, Lbf/bk;->j(I)V

    goto :goto_10

    .line 1293
    :sswitch_20
    iget-object v1, p0, Lbf/bk;->U:Lai/b;

    invoke-virtual {v1}, Lai/b;->i()[Lcom/google/googlenav/ai;

    move-result-object v1

    .line 1294
    if-eqz v1, :cond_10

    array-length v2, v1

    if-ge p2, v2, :cond_10

    .line 1301
    iget-object v2, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1302
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;BZ)V

    goto :goto_10

    .line 1307
    :sswitch_41
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p2}, Lcom/google/googlenav/F;->a(I)V

    .line 1308
    iput p2, p0, Lbf/bk;->D:I

    .line 1311
    invoke-virtual {p0}, Lbf/bk;->an()Z

    .line 1312
    invoke-virtual {p0, v5}, Lbf/bk;->b(Z)V

    goto :goto_10

    .line 1316
    :sswitch_4f
    invoke-virtual {p0}, Lbf/bk;->bO()V

    goto :goto_10

    .line 1320
    :sswitch_53
    invoke-direct {p0}, Lbf/bk;->cb()V

    .line 1321
    const/16 v0, 0x54

    const-string v1, "ac"

    const-string v2, "sl"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    .line 1327
    :sswitch_60
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "20"

    invoke-virtual {v0, v1, v2, v6, v9}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_10

    .line 1332
    :sswitch_70
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "b"

    invoke-static {v0, v1, p2, p0, v2}, Lcom/google/googlenav/ui/wizard/hy;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/aZ;ILcom/google/googlenav/bb;Ljava/lang/String;)V

    goto :goto_10

    .line 1337
    :sswitch_78
    const/16 v0, 0x59

    const-string v2, "m"

    const-string v3, "1"

    invoke-static {v0, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1340
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v9}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    goto :goto_10

    .line 1344
    :sswitch_8b
    const/16 v0, 0x59

    const-string v2, "m"

    const-string v3, "2"

    invoke-static {v0, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1347
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v5}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    goto/16 :goto_10

    .line 1351
    :sswitch_9f
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v9}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_10

    .line 1354
    :sswitch_aa
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v4}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_10

    .line 1357
    :sswitch_b5
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_10

    .line 1365
    :sswitch_c1
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v0, v4, :cond_cf

    .line 1366
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v5

    goto/16 :goto_10

    .line 1368
    :cond_cf
    if-eqz p3, :cond_de

    .line 1369
    check-cast p3, [Ljava/lang/String;

    check-cast p3, [Ljava/lang/String;

    .line 1370
    aget-object v0, p3, v9

    aget-object v1, p3, v5

    invoke-direct {p0, v0, v1}, Lbf/bk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 1372
    :cond_de
    invoke-direct {p0, v6, v6}, Lbf/bk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    .line 1376
    :sswitch_e3
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v8

    .line 1377
    const-string v0, "OpenNowNotification"

    invoke-interface {v8, v0}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_127

    .line 1378
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x362

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x361

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x6a

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Lbf/bq;

    invoke-direct {v7, p0}, Lbf/bq;-><init>(Lbf/bk;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    .line 1400
    const-string v0, "OpenNowNotification"

    new-array v1, v5, [B

    aput-byte v5, v1, v9

    invoke-interface {v8, v0, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 1402
    invoke-interface {v8}, Lcom/google/googlenav/common/io/j;->a()V

    goto/16 :goto_10

    .line 1404
    :cond_127
    invoke-direct {p0}, Lbf/bk;->ca()V

    goto/16 :goto_10

    .line 1409
    :sswitch_12c
    if-eqz p3, :cond_10

    .line 1410
    check-cast p3, Lcom/google/googlenav/be;

    .line 1411
    invoke-direct {p0, p3, v5}, Lbf/bk;->a(Lcom/google/googlenav/be;Z)V

    goto/16 :goto_10

    .line 1416
    :sswitch_135
    if-eqz p3, :cond_10

    .line 1417
    check-cast p3, Lcom/google/googlenav/be;

    .line 1418
    invoke-direct {p0, p3, v9}, Lbf/bk;->a(Lcom/google/googlenav/be;Z)V

    goto/16 :goto_10

    .line 1423
    :sswitch_13e
    if-eqz p3, :cond_c

    move-object v0, p3

    .line 1424
    check-cast v0, LaW/R;

    .line 1425
    invoke-direct {p0, v0}, Lbf/bk;->a(LaW/R;)V

    goto/16 :goto_c

    .line 1429
    :sswitch_148
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v2

    if-eqz v2, :cond_160

    :goto_154
    invoke-virtual {p0, v0, v6}, Lbf/bk;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1432
    invoke-virtual {p0}, Lbf/bk;->W()V

    goto/16 :goto_10

    .line 1429
    :cond_160
    const/16 v0, 0x8

    goto :goto_154

    .line 1435
    :sswitch_163
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v5

    goto/16 :goto_10

    .line 1437
    :sswitch_169
    invoke-virtual {p0}, Lbf/bk;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->f()V

    goto/16 :goto_10

    .line 1272
    :sswitch_data_172
    .sparse-switch
        0x2 -> :sswitch_41
        0x2bc -> :sswitch_11
        0x2c4 -> :sswitch_53
        0x2c5 -> :sswitch_4f
        0x2c6 -> :sswitch_70
        0x2c7 -> :sswitch_78
        0x2c8 -> :sswitch_9f
        0x2ca -> :sswitch_aa
        0x2cd -> :sswitch_e3
        0x2d0 -> :sswitch_8b
        0x2d1 -> :sswitch_c1
        0x2d3 -> :sswitch_b5
        0x2d4 -> :sswitch_20
        0x2d5 -> :sswitch_135
        0x2d6 -> :sswitch_12c
        0x2d7 -> :sswitch_13e
        0x2ef -> :sswitch_60
        0x3f9 -> :sswitch_148
        0x6a4 -> :sswitch_163
        0x713 -> :sswitch_c1
        0x76e -> :sswitch_169
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2480
    iget-object v0, p0, Lbf/bk;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lbf/bk;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_2c

    .line 2482
    sget-object v0, Lbf/bk;->z:Lcom/google/googlenav/ui/view/android/au;

    if-eqz v0, :cond_14

    .line 2483
    sget-object v0, Lbf/bk;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->dismiss()V

    .line 2484
    sput-object v1, Lbf/bk;->z:Lcom/google/googlenav/ui/view/android/au;

    .line 2488
    :cond_14
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v1}, Lbf/bk;->a(ILjava/lang/Object;)V

    .line 2489
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const-string v1, "s"

    const-string v2, "c"

    invoke-virtual {p0}, Lbf/bk;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lbf/bk;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2492
    const/4 v0, 0x1

    .line 2494
    :goto_2b
    return v0

    :cond_2c
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_2b
.end method

.method protected a(Ljava/io/DataInput;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 1024
    invoke-static {p1}, Lcom/google/googlenav/aZ;->b(Ljava/io/DataInput;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1025
    invoke-virtual {v0, v7}, Lcom/google/googlenav/aZ;->b(Z)V

    .line 1026
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v1

    iput-object v1, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    .line 1027
    new-instance v1, Lcom/google/googlenav/n;

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    iget-object v4, p0, Lbf/bk;->Q:LaN/k;

    iget-object v5, p0, Lbf/bk;->c:LaN/p;

    iget-object v6, p0, Lbf/bk;->d:LaN/u;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    iput-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    .line 1029
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1033
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lbf/bp;

    invoke-direct {v2, p0, v0}, Lbf/bp;-><init>(Lbf/bk;Lcom/google/googlenav/aZ;)V

    invoke-virtual {v1, v2, v7}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 1042
    :cond_36
    return v7
.end method

.method public aB()Z
    .registers 2

    .prologue
    .line 2362
    const/4 v0, 0x1

    return v0
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 2367
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_20

    :cond_1e
    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public aD()Z
    .registers 2

    .prologue
    .line 2375
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    return v0
.end method

.method public aF()I
    .registers 3

    .prologue
    .line 572
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    .line 574
    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v0

    if-nez v0, :cond_16

    .line 575
    const v0, 0x7f110014

    .line 582
    :goto_15
    return v0

    .line 576
    :cond_16
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 579
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2d

    const v0, 0x7f110013

    goto :goto_15

    :cond_2d
    const v0, 0x7f110015

    goto :goto_15

    .line 582
    :cond_31
    invoke-super {p0}, Lbf/m;->aF()I

    move-result v0

    goto :goto_15
.end method

.method public aJ()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 588
    const/16 v0, 0x4f9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .registers 4

    .prologue
    .line 2331
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2333
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2334
    invoke-virtual {p0, v1}, Lbf/bk;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    .line 2342
    :cond_e
    :goto_e
    return-object v0

    .line 2337
    :cond_f
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_22

    .line 2338
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 2341
    :cond_22
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    .line 2342
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {p0, v1}, Lbf/bk;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    goto :goto_e
.end method

.method public aL()Lam/f;
    .registers 3

    .prologue
    .line 2347
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2348
    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->ad:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    .line 2350
    :goto_16
    return-object v0

    :cond_17
    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->ae:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    goto :goto_16
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 2385
    const/4 v0, 0x1

    return v0
.end method

.method protected aN()Z
    .registers 2

    .prologue
    .line 2390
    const/4 v0, 0x1

    return v0
.end method

.method public aT()Z
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 2403
    iget v0, p0, Lbf/bk;->C:I

    invoke-static {v0, v2}, Lbf/am;->a(IZ)V

    .line 2404
    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    .line 2409
    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 2410
    invoke-virtual {p0}, Lbf/bk;->f()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 2411
    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->a(LaN/m;)V

    .line 2413
    :cond_1c
    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v1, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/k;)V

    .line 2416
    :cond_23
    return v2
.end method

.method public aU()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2448
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_13

    .line 2449
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/u;->b(LaR/C;)V

    .line 2451
    :cond_13
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbf/bk;->l(Z)V

    .line 2452
    invoke-direct {p0}, Lbf/bk;->cm()V

    .line 2453
    invoke-direct {p0}, Lbf/bk;->ci()V

    .line 2455
    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    .line 2456
    iget v0, p0, Lbf/bk;->C:I

    invoke-static {v0, v1}, Lbf/am;->a(IZ)V

    .line 2461
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/aZ;->a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->e(Lcom/google/googlenav/aZ;)V

    .line 2462
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    .line 2464
    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    .line 2465
    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_4b

    .line 2466
    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v1, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/k;)V

    .line 2470
    :cond_4b
    iget-object v0, p0, Lbf/bk;->X:Lbf/ah;

    if-eqz v0, :cond_5e

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_5e

    .line 2471
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->X:Lbf/ah;

    invoke-virtual {v0, v1}, Lbf/am;->a(Lbf/au;)V

    .line 2474
    :cond_5e
    iput-object v2, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 2475
    invoke-super {p0}, Lbf/m;->aU()V

    .line 2476
    return-void
.end method

.method public aW()V
    .registers 3

    .prologue
    .line 2421
    invoke-super {p0}, Lbf/m;->aW()V

    .line 2423
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-nez v0, :cond_17

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 2425
    :cond_17
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2426
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_38

    invoke-static {v0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_38

    .line 2427
    sget-object v1, Lcom/google/googlenav/ui/aV;->bG:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/aW;)V

    .line 2430
    :cond_38
    return-void
.end method

.method public aX()V
    .registers 3

    .prologue
    .line 2434
    invoke-super {p0}, Lbf/m;->aX()V

    .line 2435
    invoke-direct {p0}, Lbf/bk;->ch()V

    .line 2436
    invoke-direct {p0}, Lbf/bk;->cm()V

    .line 2437
    invoke-direct {p0}, Lbf/bk;->ci()V

    .line 2439
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 2441
    :cond_20
    const/16 v0, 0x4f6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bF:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/aW;)V

    .line 2444
    :cond_2f
    return-void
.end method

.method protected am()V
    .registers 9

    .prologue
    .line 519
    invoke-virtual {p0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 520
    if-eqz v0, :cond_5d

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_5d

    move-object v1, v0

    .line 521
    check-cast v1, Lcom/google/googlenav/W;

    .line 522
    const/16 v2, 0x43

    const-string v3, "o"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "l="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "i="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 527
    iget-object v1, p0, Lbf/bk;->R:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Law/g;

    .line 531
    :goto_5c
    return-void

    .line 529
    :cond_5d
    invoke-super {p0}, Lbf/m;->am()V

    goto :goto_5c
.end method

.method public ao()Z
    .registers 3

    .prologue
    .line 535
    invoke-virtual {p0}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lbf/bk;->aj()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_1c

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->q()Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method protected aq()V
    .registers 2

    .prologue
    .line 546
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bk;->d(Z)V

    .line 548
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bT;-><init>(Lbf/bk;)V

    iput-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 556
    invoke-virtual {p0}, Lbf/bk;->bG()V

    .line 557
    return-void
.end method

.method public au()Z
    .registers 2

    .prologue
    .line 2092
    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2093
    const/4 v0, 0x0

    .line 2095
    :goto_7
    return v0

    :cond_8
    invoke-super {p0}, Lbf/m;->au()Z

    move-result v0

    goto :goto_7
.end method

.method public av()I
    .registers 2

    .prologue
    .line 2380
    const/4 v0, 0x0

    return v0
.end method

.method public ay()Z
    .registers 2

    .prologue
    .line 2357
    iget-boolean v0, p0, Lbf/bk;->p:Z

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public b(LaN/B;)I
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v1, -0x1

    .line 1176
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 1177
    if-ltz v0, :cond_17

    iget-object v2, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v6}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1212
    :cond_16
    :goto_16
    return v0

    .line 1183
    :cond_17
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    .line 1184
    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    :goto_20
    if-ge v0, v3, :cond_2f

    .line 1185
    invoke-virtual {v2, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-virtual {p0, v4, p1, v6}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v4

    if-nez v4, :cond_16

    .line 1184
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 1191
    :cond_2f
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_3b

    move v0, v1

    .line 1192
    goto :goto_16

    .line 1196
    :cond_3b
    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    .line 1197
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v3

    .line 1198
    invoke-virtual {v3, p1}, Lcom/google/googlenav/T;->a(LaN/B;)Ljava/util/Enumeration;

    move-result-object v4

    .line 1199
    if-nez v4, :cond_4b

    move v0, v1

    .line 1200
    goto :goto_16

    .line 1202
    :cond_4b
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 1203
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    .line 1204
    invoke-virtual {p0, v0, p1, v6}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v5

    if-eqz v5, :cond_4b

    .line 1205
    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v2

    goto :goto_16

    :cond_67
    move v0, v1

    .line 1212
    goto :goto_16
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2033
    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    packed-switch v1, :pswitch_data_24

    .line 2046
    :cond_8
    :goto_8
    return v0

    .line 2035
    :pswitch_9
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-nez v1, :cond_8

    .line 2041
    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(B)I

    move-result v0

    neg-int v0, v0

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x8

    goto :goto_8

    .line 2033
    nop

    :pswitch_data_24
    .packed-switch 0xc
        :pswitch_9
    .end packed-switch
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .registers 2

    .prologue
    .line 384
    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    return-object v0
.end method

.method protected b(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2104
    invoke-super {p0, p1, p2}, Lbf/m;->b(ILjava/lang/Object;)V

    .line 2110
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->d()V

    .line 2111
    return-void
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .registers 3
    .parameter

    .prologue
    .line 2241
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/bk;->B:Z

    .line 2242
    return-void
.end method

.method public b(Lcom/google/googlenav/aZ;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 602
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_21

    .line 603
    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 604
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v0

    .line 605
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 606
    if-eqz v0, :cond_21

    sget-boolean v1, Lcom/google/googlenav/aZ;->a:Z

    if-eqz v1, :cond_21

    .line 607
    invoke-direct {p0, p1, p2, v0}, Lbf/bk;->a(Lcom/google/googlenav/aZ;ZLjava/lang/String;)V

    .line 613
    :cond_21
    invoke-direct {p0, p1, p2, v2}, Lbf/bk;->a(Lcom/google/googlenav/aZ;ZZ)V

    .line 614
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 2591
    invoke-virtual {p0, v0}, Lbf/bk;->e(Z)V

    .line 2592
    invoke-virtual {p0, v0}, Lbf/bk;->d(Z)V

    .line 2593
    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 1917
    const/4 v0, 0x5

    const-string v1, "v"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "v"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1921
    return-void
.end method

.method protected b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1221
    invoke-direct {p0}, Lbf/bk;->cj()V

    .line 1222
    invoke-direct {p0}, Lbf/bk;->cn()V

    .line 1223
    invoke-direct {p0}, Lbf/bk;->ck()V

    .line 1224
    invoke-super {p0, p1}, Lbf/m;->b(Z)V

    .line 1225
    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2691
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2700
    :cond_7
    :goto_7
    return v0

    .line 2695
    :cond_8
    invoke-virtual {p0, p1}, Lbf/bk;->l(Lcom/google/googlenav/ai;)I

    move-result v1

    .line 2696
    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 2700
    const/4 v0, 0x1

    goto :goto_7
.end method

.method protected bC()I
    .registers 2

    .prologue
    .line 2185
    iget v0, p0, Lbf/bk;->D:I

    return v0
.end method

.method protected bG()V
    .registers 4

    .prologue
    .line 832
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v0

    .line 833
    if-eqz v0, :cond_10

    .line 834
    new-instance v1, Lbf/bo;

    invoke-direct {v1, p0, v0}, Lbf/bo;-><init>(Lbf/bk;Las/c;)V

    invoke-virtual {v1}, Lbf/bo;->g()V

    .line 843
    :cond_10
    invoke-virtual {p0}, Lbf/bk;->bU()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aL()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3d

    .line 847
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    .line 848
    new-instance v1, Lcom/google/googlenav/aS;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aS;-><init>(LaN/B;I)V

    .line 849
    invoke-virtual {v1, p0}, Lcom/google/googlenav/aS;->a(Lcom/google/googlenav/aT;)V

    .line 850
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 854
    :goto_3c
    return-void

    .line 852
    :cond_3d
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->O:Ljava/lang/String;

    goto :goto_3c
.end method

.method protected bH()Lcom/google/googlenav/aZ;
    .registers 3

    .prologue
    .line 2214
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/google/googlenav/aZ;->a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public bI()Lbf/ah;
    .registers 2

    .prologue
    .line 377
    iget-object v0, p0, Lbf/bk;->X:Lbf/ah;

    return-object v0
.end method

.method public bJ()V
    .registers 5

    .prologue
    .line 883
    const/4 v0, 0x0

    .line 884
    iget-object v1, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v1, v1, LaB/p;

    if-eqz v1, :cond_b

    .line 885
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    .line 887
    :cond_b
    iget-object v1, p0, Lbf/bk;->y:LaB/s;

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lbf/bk;->u:Lcom/google/common/base/x;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    .line 889
    return-void
.end method

.method public bK()V
    .registers 7

    .prologue
    .line 957
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 958
    new-instance v2, Lbf/bw;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lbf/bw;-><init>(Lbf/bl;)V

    .line 959
    iget-object v3, p0, Lbf/bk;->y:LaB/s;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v3, v4, v2, v0}, LaB/s;->b(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    .line 962
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "p="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbf/bw;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "s="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbf/bw;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbf/bw;->c()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 970
    const/16 v1, 0x5e

    const-string v2, "r"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 972
    return-void
.end method

.method protected bL()V
    .registers 4

    .prologue
    .line 1879
    const/4 v0, 0x5

    const-string v1, "0"

    invoke-direct {p0}, Lbf/bk;->cd()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1882
    return-void
.end method

.method protected bM()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1978
    iget-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-le v1, v0, :cond_e

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public bN()V
    .registers 3

    .prologue
    .line 2189
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2190
    return-void
.end method

.method protected bO()V
    .registers 5

    .prologue
    .line 2197
    iget-boolean v0, p0, Lbf/bk;->B:Z

    if-eqz v0, :cond_5

    .line 2207
    :goto_4
    return-void

    .line 2203
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/bk;->B:Z

    .line 2205
    invoke-virtual {p0}, Lbf/bk;->bH()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 2206
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V

    goto :goto_4
.end method

.method public bP()Z
    .registers 3

    .prologue
    .line 2218
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_22

    const/16 v0, 0x12c

    .line 2220
    :goto_c
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->am()I

    move-result v1

    if-lez v1, :cond_25

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    if-ge v1, v0, :cond_25

    const/4 v0, 0x1

    :goto_21
    return v0

    .line 2218
    :cond_22
    const/16 v0, 0x64

    goto :goto_c

    .line 2220
    :cond_25
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public bQ()Lcom/google/googlenav/aZ;
    .registers 2

    .prologue
    .line 2262
    invoke-direct {p0}, Lbf/bk;->cg()Lcom/google/googlenav/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/n;->a()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aZ;

    return-object v0
.end method

.method public bR()Lcom/google/googlenav/T;
    .registers 2

    .prologue
    .line 2285
    invoke-direct {p0}, Lbf/bk;->cg()Lcom/google/googlenav/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/n;->b()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/T;

    return-object v0
.end method

.method public bS()I
    .registers 2

    .prologue
    .line 2582
    iget v0, p0, Lbf/bk;->C:I

    return v0
.end method

.method public bT()Lcom/google/googlenav/ui/view/u;
    .registers 2

    .prologue
    .line 2752
    iget-object v0, p0, Lbf/bk;->N:Lcom/google/googlenav/ui/view/u;

    if-nez v0, :cond_b

    .line 2753
    new-instance v0, Lbf/br;

    invoke-direct {v0, p0}, Lbf/br;-><init>(Lbf/bk;)V

    iput-object v0, p0, Lbf/bk;->N:Lcom/google/googlenav/ui/view/u;

    .line 2765
    :cond_b
    iget-object v0, p0, Lbf/bk;->N:Lcom/google/googlenav/ui/view/u;

    return-object v0
.end method

.method public bU()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2806
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    .line 2807
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aB()LaN/B;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v4

    invoke-virtual {v4}, LaN/H;->a()LaN/B;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/aT;->a(LaN/B;LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_36

    move v0, v1

    .line 2810
    :goto_1f
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_38

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aL()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_35

    if-eqz v0, :cond_38

    :cond_35
    :goto_35
    return v1

    :cond_36
    move v0, v2

    .line 2807
    goto :goto_1f

    :cond_38
    move v1, v2

    .line 2810
    goto :goto_35
.end method

.method public bg()Z
    .registers 3

    .prologue
    .line 2597
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public bh()Z
    .registers 2

    .prologue
    .line 2605
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->m()Z

    move-result v0

    return v0
.end method

.method public bi()Z
    .registers 2

    .prologue
    .line 2615
    const/4 v0, 0x1

    return v0
.end method

.method public bl()Z
    .registers 3

    .prologue
    .line 2627
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public bm()Z
    .registers 2

    .prologue
    .line 2633
    const/4 v0, 0x0

    return v0
.end method

.method protected bn()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1610
    const-string v0, "p"

    return-object v0
.end method

.method protected bo()V
    .registers 3

    .prologue
    .line 1646
    invoke-virtual {p0}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1647
    if-eqz v0, :cond_17

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->af()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 1648
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    .line 1649
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/ai;)V

    .line 1651
    :cond_17
    return-void
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 4
    .parameter

    .prologue
    .line 2052
    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v0

    sparse-switch v0, :sswitch_data_60

    move-object v0, p1

    .line 2079
    check-cast v0, Lcom/google/googlenav/ai;

    .line 2081
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bq()Z

    move-result v1

    if-eqz v1, :cond_5a

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->f()I

    move-result v0

    .line 2083
    :goto_18
    rsub-int/lit8 v0, v0, 0x1

    :goto_1a
    return v0

    .line 2055
    :sswitch_1b
    const/4 v0, 0x0

    goto :goto_1a

    .line 2057
    :sswitch_1d
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-nez v0, :cond_33

    .line 2059
    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(B)I

    move-result v0

    neg-int v0, v0

    goto :goto_1a

    .line 2065
    :cond_33
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 2070
    :sswitch_3a
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1a

    :sswitch_43
    move-object v0, p1

    .line 2075
    check-cast v0, Lcom/google/googlenav/ai;

    iget-object v1, p0, Lbf/bk;->t:Lbh/a;

    check-cast v1, Lbh/i;

    invoke-virtual {v1}, Lbh/i;->b()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    .line 2077
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 2081
    :cond_5a
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    goto :goto_18

    .line 2052
    nop

    :sswitch_data_60
    .sparse-switch
        0x0 -> :sswitch_1b
        0xc -> :sswitch_1d
        0xf -> :sswitch_3a
        0x10 -> :sswitch_43
        0x11 -> :sswitch_43
        0x12 -> :sswitch_43
        0x13 -> :sswitch_43
    .end sparse-switch
.end method

.method public c()V
    .registers 2

    .prologue
    .line 396
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/bk;->S:Z

    .line 397
    return-void
.end method

.method public c(Lcom/google/googlenav/F;)V
    .registers 5
    .parameter

    .prologue
    .line 437
    invoke-virtual {p0, p1}, Lbf/bk;->a(Lcom/google/googlenav/F;)V

    .line 438
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/bk;->b(B)V

    .line 439
    invoke-virtual {p0}, Lbf/bk;->R()V

    .line 441
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 442
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->W()I

    move-result v1

    .line 445
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/T;->g()V

    .line 448
    const/4 v2, 0x0

    iput-boolean v2, p0, Lbf/bk;->B:Z

    .line 451
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v0

    iput-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    .line 453
    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    .line 454
    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_36

    .line 455
    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v2, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v2}, LaN/p;->b(LaN/k;)V

    .line 463
    :cond_36
    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 464
    invoke-virtual {p0}, Lbf/bk;->f()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 465
    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->a(LaN/m;)V

    .line 467
    :cond_49
    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v2, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v2}, LaN/p;->a(LaN/k;)V

    .line 471
    :cond_50
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    .line 473
    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_80

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/bT;

    if-eqz v0, :cond_80

    .line 477
    const/16 v0, 0xa

    if-eq v1, v0, :cond_72

    .line 478
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->l()V

    .line 480
    :cond_72
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->m()V

    .line 481
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->h()V

    .line 485
    :cond_80
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-nez v0, :cond_94

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_a9

    .line 487
    :cond_94
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bG:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/aW;)V

    .line 492
    :cond_a9
    invoke-direct {p0}, Lbf/bk;->bY()LaN/H;

    .line 495
    invoke-direct {p0}, Lbf/bk;->bZ()V

    .line 496
    return-void
.end method

.method protected c(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1712
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_19

    invoke-virtual {p0}, Lbf/bk;->aa()Z

    move-result v1

    if-eqz v1, :cond_19

    iget-boolean v1, p0, Lbf/bk;->E:Z

    if-eqz v1, :cond_19

    .line 1714
    iput-boolean v0, p0, Lbf/bk;->E:Z

    .line 1715
    invoke-virtual {p0}, Lbf/bk;->h()V

    .line 1716
    const/4 v0, 0x1

    .line 1719
    :cond_19
    return v0
.end method

.method protected c(Lcom/google/googlenav/aZ;)Z
    .registers 4
    .parameter

    .prologue
    .line 660
    const-string v0, "19"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    const-string v0, "20"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v0

    if-eqz v0, :cond_30

    :cond_28
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_30

    const/4 v0, 0x1

    :goto_2f
    return v0

    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method public d()I
    .registers 2

    .prologue
    .line 2586
    invoke-virtual {p0}, Lbf/bk;->bS()I

    move-result v0

    return v0
.end method

.method public d(Lcom/google/googlenav/aZ;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 1931
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1932
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->Q()Ljava/lang/String;

    move-result-object v0

    .line 1945
    :goto_e
    return-object v0

    .line 1933
    :cond_f
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1938
    const/16 v0, 0x206

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 1939
    :cond_20
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_37

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 1943
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 1945
    :cond_37
    const/16 v0, 0x47b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_e
.end method

.method protected e(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 2274
    new-instance v0, Lcom/google/googlenav/n;

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    iput-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    .line 2275
    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .registers 3
    .parameter

    .prologue
    .line 1102
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/F;)V

    .line 1103
    return-void
.end method

.method public e(Lat/a;)Z
    .registers 10
    .parameter

    .prologue
    const/16 v7, 0x36

    const/16 v6, 0x34

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 1733
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v5

    .line 1736
    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1797
    :cond_11
    :goto_11
    return v1

    .line 1741
    :cond_12
    if-eq v5, v7, :cond_16

    if-ne v5, v6, :cond_11

    .line 1742
    :cond_16
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    .line 1747
    invoke-virtual {p0}, Lbf/bk;->ah()Z

    move-result v4

    if-eqz v4, :cond_58

    if-eq v0, v3, :cond_58

    .line 1780
    :cond_26
    if-ltz v0, :cond_32

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ne v0, v3, :cond_33

    :cond_32
    move v0, v1

    .line 1784
    :cond_33
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1787
    if-ltz v0, :cond_11

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ge v0, v3, :cond_11

    .line 1790
    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    .line 1791
    invoke-virtual {p0}, Lbf/bk;->an()Z

    .line 1792
    invoke-virtual {p0, v2}, Lbf/bk;->b(Z)V

    move v1, v2

    .line 1793
    goto :goto_11

    .line 1752
    :cond_58
    invoke-virtual {p0}, Lbf/bk;->ah()Z

    move-result v4

    if-nez v4, :cond_9b

    .line 1758
    if-ne v5, v6, :cond_9d

    invoke-direct {p0}, Lbf/bk;->ce()Z

    move-result v4

    if-eqz v4, :cond_9d

    .line 1759
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    move v4, v0

    .line 1772
    :goto_6f
    if-ne v5, v6, :cond_a7

    move v0, v3

    :goto_72
    add-int/2addr v0, v4

    .line 1774
    if-ltz v0, :cond_26

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    if-ge v0, v4, :cond_26

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v4

    if-eqz v4, :cond_9b

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/E;->c()B

    move-result v4

    if-nez v4, :cond_26

    :cond_9b
    move v4, v0

    goto :goto_6f

    .line 1760
    :cond_9d
    if-ne v5, v7, :cond_9b

    invoke-direct {p0}, Lbf/bk;->cf()Z

    move-result v4

    if-eqz v4, :cond_9b

    move v4, v3

    .line 1761
    goto :goto_6f

    :cond_a7
    move v0, v2

    .line 1772
    goto :goto_72
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .registers 2

    .prologue
    .line 2302
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ai()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    return-object v0
.end method

.method public f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;
    .registers 5
    .parameter

    .prologue
    .line 2935
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_1d

    .line 2936
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 2937
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bN()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 2941
    :goto_18
    return-object v0

    .line 2935
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2941
    :cond_1d
    const/4 v0, 0x0

    goto :goto_18
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 2398
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->at()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected f(Lat/a;)Z
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/16 v2, 0x23

    const/4 v0, 0x1

    .line 1660
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    if-ne v1, v2, :cond_4b

    .line 1661
    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1662
    const-string v1, "m"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1665
    invoke-direct {p0}, Lbf/bk;->cc()V

    .line 1666
    invoke-virtual {p0, v4, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    .line 1700
    :cond_21
    :goto_21
    return v0

    .line 1668
    :cond_22
    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1669
    const-string v1, "l"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    goto :goto_21

    .line 1673
    :cond_36
    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v1

    if-eqz v1, :cond_4b

    .line 1674
    const-string v1, "l"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677
    const/16 v1, 0x9

    invoke-virtual {p0, v1, v3}, Lbf/bk;->c(ILjava/lang/Object;)V

    goto :goto_21

    .line 1682
    :cond_4b
    invoke-virtual {p0, p1}, Lbf/bk;->g(Lat/a;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 1686
    invoke-virtual {p0, p1}, Lbf/bk;->d(Lat/a;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 1692
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    if-ne v1, v4, :cond_66

    invoke-virtual {p0}, Lbf/bk;->aa()Z

    move-result v1

    if-eqz v1, :cond_66

    .line 1696
    iput-boolean v0, p0, Lbf/bk;->E:Z

    goto :goto_21

    .line 1700
    :cond_66
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public g(I)Z
    .registers 3
    .parameter

    .prologue
    .line 2500
    packed-switch p1, :pswitch_data_16

    .line 2507
    invoke-super {p0, p1}, Lbf/m;->g(I)Z

    move-result v0

    :goto_7
    return v0

    .line 2504
    :pswitch_8
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->k()Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    goto :goto_7

    :cond_14
    const/4 v0, 0x0

    goto :goto_7

    .line 2500
    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method protected final g(Lat/a;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1809
    invoke-virtual {p0}, Lbf/bk;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1812
    if-nez v0, :cond_d

    move v0, v1

    .line 1834
    :goto_c
    return v0

    .line 1816
    :cond_d
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_20

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v3

    if-eqz v3, :cond_20

    .line 1817
    invoke-virtual {p0, v1, v5}, Lbf/bk;->b(ILjava/lang/Object;)V

    move v0, v2

    .line 1818
    goto :goto_c

    .line 1821
    :cond_20
    invoke-direct {p0, v0}, Lbf/bk;->m(Lcom/google/googlenav/ai;)Z

    move-result v3

    if-nez v3, :cond_28

    move v0, v1

    .line 1823
    goto :goto_c

    .line 1826
    :cond_28
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_4d

    invoke-virtual {p0}, Lbf/bk;->ag()Z

    move-result v3

    if-eqz v3, :cond_4d

    .line 1828
    const/16 v1, 0x9

    invoke-virtual {p0, v1, v5}, Lbf/bk;->a(ILjava/lang/Object;)V

    .line 1829
    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    .line 1830
    iget-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    const-string v3, "s"

    const-string v4, "k"

    invoke-virtual {p0, v1, v3, v4, v0}, Lbf/bk;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1832
    goto :goto_c

    :cond_4d
    move v0, v1

    .line 1834
    goto :goto_c
.end method

.method public g(Lcom/google/googlenav/aZ;)Z
    .registers 4
    .parameter

    .prologue
    .line 2993
    invoke-virtual {p0, p1}, Lbf/bk;->h(Lcom/google/googlenav/aZ;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public h(Lcom/google/googlenav/aZ;)I
    .registers 4
    .parameter

    .prologue
    .line 3004
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 3005
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 3006
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aJ()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 3010
    :goto_14
    return v1

    .line 3004
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3010
    :cond_19
    const/4 v1, -0x1

    goto :goto_14
.end method

.method public h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2115
    invoke-virtual {p0}, Lbf/bk;->bz()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2181
    :cond_8
    :goto_8
    return-void

    .line 2119
    :cond_9
    iput-boolean v2, p0, Lbf/bk;->B:Z

    .line 2121
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 2122
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_b6

    .line 2180
    invoke-virtual {p0, v0}, Lbf/bk;->b(Lcom/google/googlenav/ui/wizard/A;)Z

    goto :goto_8

    .line 2124
    :sswitch_20
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bk;->i(Z)V

    .line 2125
    invoke-virtual {p0, v2, v3}, Lbf/bk;->c(ILjava/lang/Object;)V

    .line 2126
    invoke-virtual {p0, v2}, Lbf/bk;->i(Z)V

    goto :goto_8

    .line 2130
    :sswitch_2b
    invoke-virtual {p0, v2, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    goto :goto_8

    .line 2134
    :sswitch_2f
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    .line 2135
    if-eqz v0, :cond_42

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_42

    .line 2136
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    .line 2138
    :cond_42
    invoke-virtual {p0, v2, v3}, Lbf/bk;->a(ILjava/lang/Object;)V

    goto :goto_8

    .line 2143
    :sswitch_46
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 2146
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 2147
    if-eqz v0, :cond_8

    .line 2148
    invoke-virtual {v0}, Lbf/i;->aR()V

    goto :goto_8

    .line 2153
    :sswitch_5f
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 2154
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->i(Ljava/lang/String;)V

    goto :goto_8

    .line 2158
    :sswitch_70
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 2159
    invoke-virtual {p0, v2, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    .line 2160
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->Y()V

    goto :goto_8

    .line 2164
    :sswitch_82
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    .line 2165
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    goto/16 :goto_8

    .line 2169
    :sswitch_93
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 2170
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/by;

    .line 2171
    if-eqz v0, :cond_8

    .line 2172
    invoke-virtual {v0}, Lbf/by;->bG()V

    goto/16 :goto_8

    .line 2176
    :sswitch_af
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->y()V

    goto/16 :goto_8

    .line 2122
    :sswitch_data_b6
    .sparse-switch
        0x0 -> :sswitch_46
        0x6 -> :sswitch_5f
        0x7 -> :sswitch_2f
        0x8 -> :sswitch_20
        0x9 -> :sswitch_2b
        0xf -> :sswitch_70
        0x10 -> :sswitch_82
        0x11 -> :sswitch_46
        0x1a -> :sswitch_93
        0x1c -> :sswitch_af
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 507
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbf/bk;->R:Lcom/google/googlenav/layer/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Law/g;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method protected i()Lbh/a;
    .registers 2

    .prologue
    .line 1705
    new-instance v0, Lbh/i;

    invoke-direct {v0, p0}, Lbh/i;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected j(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1615
    const-string v0, "m"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1616
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 1617
    invoke-virtual {p0, p1}, Lbf/bk;->b(I)V

    .line 1623
    const/4 v1, -0x1

    if-eq v0, v1, :cond_25

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 1624
    const/4 v1, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lbf/bk;->a(ILjava/lang/Object;)V

    .line 1637
    :cond_24
    :goto_24
    return-void

    .line 1626
    :cond_25
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v2}, Lbf/bk;->a(ILjava/lang/Object;)V

    .line 1633
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lbf/bk;->g:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_24

    .line 1634
    invoke-virtual {p0, v2}, Lbf/bk;->a(Ljava/lang/Object;)V

    goto :goto_24
.end method

.method public k(I)V
    .registers 4
    .parameter

    .prologue
    .line 2312
    iget v0, p0, Lbf/bk;->C:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbf/am;->a(IZ)V

    .line 2313
    iput p1, p0, Lbf/bk;->C:I

    .line 2314
    iget v0, p0, Lbf/bk;->C:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbf/am;->a(IZ)V

    .line 2318
    iget-object v0, p0, Lbf/bk;->t:Lbh/a;

    check-cast v0, Lbh/i;

    invoke-virtual {v0, p1}, Lbh/i;->c(I)V

    .line 2319
    return-void
.end method

.method protected k(Z)Z
    .registers 3
    .parameter

    .prologue
    .line 652
    if-nez p1, :cond_4

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public l(Lcom/google/googlenav/ai;)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2719
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 2720
    invoke-direct {p0, p1}, Lbf/bk;->n(Lcom/google/googlenav/ai;)I

    move-result v2

    .line 2721
    if-eq v2, v0, :cond_15

    .line 2723
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    .line 2724
    sub-int v0, v2, v0

    invoke-direct {p0, v0}, Lbf/bk;->m(I)I

    move-result v0

    .line 2726
    :cond_15
    return v0
.end method

.method protected m()V
    .registers 1

    .prologue
    .line 2770
    invoke-super {p0}, Lbf/m;->m()V

    .line 2771
    invoke-virtual {p0}, Lbf/bk;->bL()V

    .line 2772
    return-void
.end method

.method public q()I
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 991
    invoke-super {p0}, Lbf/m;->q()I

    move-result v0

    .line 992
    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_20

    .line 993
    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    .line 998
    :cond_12
    :goto_12
    iget-object v1, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    if-eqz v1, :cond_1f

    .line 999
    iget-object v1, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/ap;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    .line 1001
    :cond_1f
    return v0

    .line 994
    :cond_20
    iget-object v1, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_12

    .line 995
    iget-object v1, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    goto :goto_12
.end method

.method protected t()Ljava/lang/String;
    .registers 2

    .prologue
    .line 3357
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[SearchLayer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected v()V
    .registers 3

    .prologue
    .line 567
    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/T;->b(LaN/B;)V

    .line 568
    return-void
.end method
