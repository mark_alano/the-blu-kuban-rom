.class final Lbf/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/x;


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 898
    iput v0, p0, Lbf/bw;->a:I

    .line 899
    iput v0, p0, Lbf/bw;->b:I

    .line 900
    iput v0, p0, Lbf/bw;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lbf/bl;)V
    .registers 2
    .parameter

    .prologue
    .line 896
    invoke-direct {p0}, Lbf/bw;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 940
    iget v0, p0, Lbf/bw;->a:I

    return v0
.end method

.method public a(Lcom/google/googlenav/ai;)Ljava/lang/Iterable;
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 904
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v2, v1

    .line 906
    :goto_6
    const/4 v0, 0x3

    if-gt v2, v0, :cond_2c

    .line 907
    invoke-virtual {p1, v2}, Lcom/google/googlenav/ai;->g(I)[Lcom/google/googlenav/aq;

    move-result-object v4

    .line 908
    if-nez v4, :cond_13

    .line 906
    :cond_f
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_13
    move v0, v1

    .line 912
    :goto_14
    array-length v5, v4

    if-ge v0, v5, :cond_f

    .line 913
    aget-object v5, v4, v0

    .line 914
    invoke-virtual {v5}, Lcom/google/googlenav/aq;->b()Ljava/lang/String;

    move-result-object v5

    .line 915
    if-eqz v5, :cond_29

    .line 916
    new-instance v6, Lcom/google/googlenav/ui/bs;

    sget v7, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v6, v5, v7}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 912
    :cond_29
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 921
    :cond_2c
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->M()Lcom/google/googlenav/aq;

    move-result-object v0

    .line 922
    if-eqz v0, :cond_56

    .line 923
    invoke-virtual {v0}, Lcom/google/googlenav/aq;->b()Ljava/lang/String;

    move-result-object v1

    .line 924
    if-eqz v1, :cond_42

    .line 925
    new-instance v2, Lcom/google/googlenav/ui/bs;

    sget v4, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v2, v1, v4}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 928
    :cond_42
    invoke-virtual {v0}, Lcom/google/googlenav/aq;->d()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 929
    iget v0, p0, Lbf/bw;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbf/bw;->a:I

    .line 936
    :goto_4e
    return-object v3

    .line 931
    :cond_4f
    iget v0, p0, Lbf/bw;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbf/bw;->b:I

    goto :goto_4e

    .line 934
    :cond_56
    iget v0, p0, Lbf/bw;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbf/bw;->c:I

    goto :goto_4e
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 896
    check-cast p1, Lcom/google/googlenav/ai;

    invoke-virtual {p0, p1}, Lbf/bw;->a(Lcom/google/googlenav/ai;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 944
    iget v0, p0, Lbf/bw;->b:I

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 948
    iget v0, p0, Lbf/bw;->c:I

    return v0
.end method
