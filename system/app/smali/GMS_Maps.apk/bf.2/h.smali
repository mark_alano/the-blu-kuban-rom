.class public abstract Lbf/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbf/w;


# static fields
.field protected static a:Landroid/view/ViewGroup;

.field protected static b:Lcom/google/googlenav/ui/android/BaseAndroidView;


# instance fields
.field protected final c:Lbf/i;


# direct methods
.method public constructor <init>(Lbf/i;)V
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lbf/h;->c:Lbf/i;

    .line 59
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/android/BaseAndroidView;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 53
    sput-object p0, Lbf/h;->a:Landroid/view/ViewGroup;

    .line 54
    sput-object p1, Lbf/h;->b:Lcom/google/googlenav/ui/android/BaseAndroidView;

    .line 55
    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .registers 4

    .prologue
    .line 242
    sget-object v0, Lbf/h;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100048

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 243
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 244
    sget-object v1, Lbf/h;->a:Landroid/view/ViewGroup;

    const v2, 0x7f100042

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 245
    return-object v0
.end method

.method protected a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 70
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 72
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 75
    const/4 v1, 0x0

    .line 76
    new-instance v0, Lcom/google/googlenav/ui/view/d;

    sget-object v2, Lbf/h;->b:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-direct {v0, p1, v2, v1}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    .line 81
    :goto_16
    invoke-static {p1, p2, v0}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 82
    return-object v0

    .line 78
    :cond_1a
    new-instance v1, Lcom/google/googlenav/ui/view/android/K;

    sget-object v0, Lbf/h;->b:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f10004f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidBubbleView;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/ui/view/android/K;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/android/AndroidBubbleView;)V

    move-object v0, v1

    goto :goto_16
.end method

.method protected a(Landroid/view/View;Lam/f;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 197
    const v0, 0x7f10004c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 198
    if-eqz p2, :cond_19

    .line 199
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    check-cast p2, Lan/f;

    invoke-virtual {p2}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 204
    :goto_18
    return-void

    .line 202
    :cond_19
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_18
.end method

.method protected a(Landroid/view/View;Lam/f;Lam/f;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const v5, 0x7f10004d

    const v4, 0x7f100049

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 96
    const v0, 0x7f10001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    .line 97
    if-eqz p2, :cond_4a

    .line 98
    check-cast p2, Lan/f;

    invoke-virtual {p2}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/BubbleButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 99
    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/BubbleButton;->setVisibility(I)V

    .line 100
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 106
    :goto_27
    const v0, 0x7f10004e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    .line 107
    if-eqz p3, :cond_55

    .line 108
    check-cast p3, Lan/f;

    invoke-virtual {p3}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/BubbleButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/BubbleButton;->setVisibility(I)V

    .line 110
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 113
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lbf/h;->a(Landroid/view/View;Z)V

    .line 118
    :goto_49
    return-void

    .line 102
    :cond_4a
    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/android/BubbleButton;->setVisibility(I)V

    .line 103
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_27

    .line 115
    :cond_55
    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/android/BubbleButton;->setVisibility(I)V

    .line 116
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_49
.end method

.method protected a(Landroid/view/View;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 225
    const v0, 0x7f10004b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 226
    if-eqz p2, :cond_16

    .line 229
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 230
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 231
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 235
    :goto_15
    return-void

    .line 233
    :cond_16
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_15
.end method

.method protected a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 181
    const v0, 0x7f10001c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 182
    if-eqz p2, :cond_13

    .line 183
    invoke-static {v0, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 184
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    :goto_12
    return-void

    .line 186
    :cond_13
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_12
.end method

.method protected a(Landroid/view/View;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 129
    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 130
    if-eqz p2, :cond_19

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    .line 131
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    invoke-static {v0, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/util/List;)V

    .line 136
    :goto_18
    return-void

    .line 134
    :cond_19
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_18
.end method

.method protected a(Landroid/view/View;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 213
    const v0, 0x7f100047

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 214
    if-eqz p2, :cond_f

    const/16 v0, 0x8

    :goto_b
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 215
    return-void

    .line 214
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected b(Landroid/view/View;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 147
    const v0, 0x7f10004a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 148
    if-eqz p2, :cond_19

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    .line 149
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 150
    invoke-static {v0, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/util/List;)V

    .line 154
    :goto_18
    return-void

    .line 152
    :cond_19
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_18
.end method

.method protected c(Landroid/view/View;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 165
    const v0, 0x7f100045

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 166
    if-eqz p2, :cond_19

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    .line 167
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    invoke-static {v0, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Ljava/util/List;)V

    .line 172
    :goto_18
    return-void

    .line 170
    :cond_19
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_18
.end method
