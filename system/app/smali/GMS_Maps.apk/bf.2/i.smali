.class public abstract Lbf/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;
.implements Lcom/google/googlenav/ui/Y;
.implements Lcom/google/googlenav/ui/e;
.implements Lcom/google/googlenav/ui/view/c;
.implements Lo/C;


# instance fields
.field private A:Lbf/j;

.field private B:I

.field private C:B

.field private D:Lcom/google/googlenav/settings/e;

.field protected final a:Lcom/google/googlenav/ui/bi;

.field protected final b:Lcom/google/googlenav/ui/s;

.field protected final c:LaN/p;

.field protected final d:LaN/u;

.field protected final e:Landroid/graphics/Point;

.field protected f:Lcom/google/googlenav/F;

.field protected g:Lcom/google/googlenav/ui/view/d;

.field protected h:Lcom/google/googlenav/E;

.field protected final i:Lcom/google/googlenav/ui/X;

.field protected j:Lcom/google/googlenav/ui/view/d;

.field protected k:Lcom/google/googlenav/ui/view/d;

.field protected l:Lcom/google/googlenav/ui/view/d;

.field protected m:Lcom/google/googlenav/ui/view/d;

.field protected n:Lam/f;

.field protected o:Z

.field protected p:Z

.field protected q:[Ljava/lang/Boolean;

.field protected r:Lcom/google/googlenav/ui/view/android/aL;

.field protected s:Lbf/w;

.field protected t:Lbh/a;

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Lbf/k;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    .line 217
    iput v3, p0, Lbf/i;->u:I

    .line 226
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Boolean;

    iput-object v1, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    .line 232
    iput-boolean v2, p0, Lbf/i;->v:Z

    .line 238
    iput-boolean v2, p0, Lbf/i;->w:Z

    .line 248
    iput-boolean v4, p0, Lbf/i;->x:Z

    .line 256
    iput-boolean v2, p0, Lbf/i;->y:Z

    .line 327
    iput v3, p0, Lbf/i;->B:I

    .line 334
    iput-byte v4, p0, Lbf/i;->C:B

    .line 341
    iput-object v0, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    .line 350
    iput-object p1, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    .line 351
    iput-object p2, p0, Lbf/i;->c:LaN/p;

    .line 352
    iput-object p3, p0, Lbf/i;->d:LaN/u;

    .line 353
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    iput-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    .line 354
    iput-object p4, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    .line 356
    invoke-static {}, Lbf/al;->a()Lbf/al;

    move-result-object v1

    .line 358
    if-eqz p1, :cond_45

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v2

    if-eqz v2, :cond_45

    .line 359
    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    .line 361
    :cond_45
    invoke-virtual {v1, p0, v0}, Lbf/al;->a(Lbf/i;LaB/s;)Lbf/w;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->s:Lbf/w;

    .line 362
    invoke-virtual {p0}, Lbf/i;->i()Lbh/a;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->t:Lbh/a;

    .line 366
    iput-object p5, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    .line 367
    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 1326
    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_d

    .line 1327
    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    .line 1329
    :cond_d
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1a

    .line 1330
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    .line 1332
    :cond_1a
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/r;III)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1675
    if-nez p1, :cond_3

    .line 1679
    :goto_2
    return-void

    .line 1678
    :cond_3
    invoke-virtual {p0, p4}, Lbf/i;->c(I)Lcom/google/googlenav/e;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/googlenav/e;->a(Lcom/google/googlenav/e;Lcom/google/googlenav/ui/r;II)V

    goto :goto_2
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1478
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    .line 1481
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    .line 1484
    return-void
.end method

.method private b(LaN/B;I)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2447
    invoke-direct {p0, p2}, Lbf/i;->h(I)I

    move-result v2

    .line 2448
    iget-object v3, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v3

    invoke-virtual {v3, v2, p1, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v3

    if-nez v3, :cond_13

    .line 2463
    :cond_12
    :goto_12
    return v0

    .line 2452
    :cond_13
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v3

    if-eqz v3, :cond_23

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v3

    invoke-interface {v3}, LaH/m;->g()Z

    move-result v3

    if-nez v3, :cond_25

    :cond_23
    move v0, v1

    .line 2455
    goto :goto_12

    .line 2457
    :cond_25
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v3

    invoke-interface {v3}, LaH/m;->r()LaH/h;

    move-result-object v3

    .line 2458
    if-eqz v3, :cond_3f

    iget-object v4, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v4

    invoke-virtual {v3}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v4, v2, v3, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_3f
    move v0, v1

    .line 2461
    goto :goto_12
.end method

.method private b(LaN/B;LaN/B;I)Z
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2499
    if-ne p3, v0, :cond_5

    .line 2513
    :cond_4
    :goto_4
    return v0

    .line 2506
    :cond_5
    invoke-direct {p0, p3}, Lbf/i;->h(I)I

    move-result v2

    .line 2507
    iget-object v3, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v3

    invoke-virtual {v3, v2, p1, v1}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v3

    if-eqz v3, :cond_21

    iget-object v3, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v3

    invoke-virtual {v3, v2, p2, v1}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_21
    move v0, v1

    .line 2513
    goto :goto_4
.end method

.method private b(Lcom/google/googlenav/E;LaN/B;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2166
    if-eqz p1, :cond_11

    invoke-interface {p1}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->d()Lcom/google/googlenav/ui/aR;

    move-result-object v0

    if-nez v0, :cond_13

    :cond_11
    move v0, v1

    .line 2193
    :goto_12
    return v0

    .line 2172
    :cond_13
    check-cast p1, Lcom/google/googlenav/ai;

    .line 2173
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v2

    .line 2179
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    .line 2180
    if-nez v0, :cond_23

    move v0, v1

    .line 2181
    goto :goto_12

    .line 2183
    :cond_23
    invoke-interface {v0}, LaN/g;->a()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3b

    .line 2184
    iget-object v1, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    invoke-interface {v1}, Lcom/google/googlenav/ui/X;->d()Lcom/google/googlenav/ui/aR;

    move-result-object v1

    check-cast v0, LaN/M;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aI;LaN/Y;)[J

    move-result-object v0

    .line 2186
    invoke-static {v0, p2, v2}, LaN/M;->a([JLaN/B;LaN/Y;)Z

    move-result v0

    goto :goto_12

    .line 2187
    :cond_3b
    invoke-interface {v0}, LaN/g;->a()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_53

    .line 2188
    iget-object v1, p0, Lbf/i;->i:Lcom/google/googlenav/ui/X;

    invoke-interface {v1}, Lcom/google/googlenav/ui/X;->d()Lcom/google/googlenav/ui/aR;

    move-result-object v1

    check-cast v0, LaN/N;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/aR;->a(Lcom/google/googlenav/ui/aI;LaN/Y;)[J

    move-result-object v0

    .line 2190
    invoke-static {v0, p2, v2}, LaN/N;->b([JLaN/B;LaN/Y;)Z

    move-result v0

    goto :goto_12

    :cond_53
    move v0, v1

    .line 2193
    goto :goto_12
.end method

.method private be()V
    .registers 2

    .prologue
    .line 2838
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2850
    :cond_a
    :goto_a
    return-void

    .line 2842
    :cond_b
    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v0

    if-nez v0, :cond_15

    .line 2843
    invoke-virtual {p0}, Lbf/i;->aY()Z

    goto :goto_a

    .line 2844
    :cond_15
    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 2845
    invoke-virtual {p0}, Lbf/i;->W()V

    .line 2846
    invoke-virtual {p0}, Lbf/i;->aY()Z

    goto :goto_a

    .line 2847
    :cond_22
    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2848
    invoke-virtual {p0}, Lbf/i;->W()V

    goto :goto_a
.end method

.method private c()V
    .registers 4

    .prologue
    .line 1508
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1509
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    .line 1512
    :cond_12
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1515
    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_c

    .line 1516
    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 1517
    iput-object v1, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    .line 1519
    :cond_c
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_17

    .line 1520
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 1521
    iput-object v1, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    .line 1523
    :cond_17
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 1533
    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_c

    .line 1534
    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 1535
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    .line 1537
    :cond_c
    return-void
.end method

.method private f(Lcom/google/googlenav/ui/r;)V
    .registers 3
    .parameter

    .prologue
    .line 1311
    if-nez p1, :cond_3

    .line 1323
    :cond_2
    :goto_2
    return-void

    .line 1314
    :cond_3
    invoke-virtual {p0}, Lbf/i;->O()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1315
    invoke-direct {p0, p1}, Lbf/i;->i(Lcom/google/googlenav/ui/r;)V

    .line 1317
    :cond_c
    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1318
    invoke-direct {p0, p1}, Lbf/i;->g(Lcom/google/googlenav/ui/r;)V

    .line 1320
    :cond_15
    invoke-virtual {p0}, Lbf/i;->Q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1321
    invoke-direct {p0, p1}, Lbf/i;->h(Lcom/google/googlenav/ui/r;)V

    goto :goto_2
.end method

.method private g(Lcom/google/googlenav/ui/r;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1360
    iget-object v0, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 1362
    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_17

    .line 1363
    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    .line 1364
    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    .line 1366
    :cond_17
    return-void
.end method

.method private g(Lcom/google/googlenav/E;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 1658
    invoke-interface {p1}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-eqz v1, :cond_8

    .line 1663
    :cond_7
    :goto_7
    return v0

    .line 1662
    :cond_8
    check-cast p1, Lcom/google/googlenav/ai;

    .line 1663
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bq()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v0, 0x0

    goto :goto_7
.end method

.method private h(I)I
    .registers 3
    .parameter

    .prologue
    .line 2517
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    .line 2518
    const/4 v0, 0x3

    .line 2522
    :goto_4
    return v0

    :cond_5
    const/4 v0, 0x1

    goto :goto_4
.end method

.method private h(Lcom/google/googlenav/ui/r;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1369
    iget-object v0, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 1371
    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_17

    .line 1372
    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    .line 1373
    iget-object v0, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    .line 1375
    :cond_17
    return-void
.end method

.method private i(Lcom/google/googlenav/ui/r;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1378
    iget-object v0, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 1379
    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1a

    .line 1380
    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    .line 1381
    iget-object v0, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    .line 1383
    :cond_1a
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_2e

    .line 1384
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/d;->a(Z)V

    .line 1385
    iget-object v0, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    .line 1387
    :cond_2e
    return-void
.end method


# virtual methods
.method protected A()V
    .registers 2

    .prologue
    .line 1150
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_9

    .line 1151
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 1153
    :cond_9
    return-void
.end method

.method protected B()V
    .registers 2

    .prologue
    .line 1160
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_9

    .line 1161
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 1163
    :cond_9
    return-void
.end method

.method protected C()V
    .registers 4

    .prologue
    .line 1170
    iget-boolean v0, p0, Lbf/i;->o:Z

    if-nez v0, :cond_5

    .line 1214
    :cond_4
    :goto_4
    return-void

    .line 1176
    :cond_5
    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 1177
    invoke-virtual {p0}, Lbf/i;->S()V

    .line 1182
    :goto_e
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    packed-switch v0, :pswitch_data_6e

    .line 1212
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1179
    :cond_36
    invoke-virtual {p0}, Lbf/i;->V()V

    goto :goto_e

    .line 1186
    :pswitch_3a
    invoke-virtual {p0}, Lbf/i;->r()V

    .line 1187
    invoke-virtual {p0}, Lbf/i;->al()V

    goto :goto_4

    .line 1192
    :pswitch_41
    invoke-virtual {p0}, Lbf/i;->r()V

    .line 1193
    invoke-virtual {p0}, Lbf/i;->an()Z

    goto :goto_4

    .line 1196
    :pswitch_48
    invoke-virtual {p0}, Lbf/i;->an()Z

    .line 1197
    invoke-virtual {p0}, Lbf/i;->I()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1198
    invoke-virtual {p0}, Lbf/i;->r()V

    .line 1199
    invoke-virtual {p0}, Lbf/i;->ap()V

    .line 1200
    invoke-virtual {p0}, Lbf/i;->A()V

    goto :goto_4

    .line 1204
    :pswitch_5b
    invoke-virtual {p0}, Lbf/i;->an()Z

    .line 1205
    invoke-virtual {p0}, Lbf/i;->H()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1206
    invoke-virtual {p0}, Lbf/i;->r()V

    .line 1207
    invoke-virtual {p0}, Lbf/i;->aq()V

    .line 1208
    invoke-virtual {p0}, Lbf/i;->B()V

    goto :goto_4

    .line 1182
    :pswitch_data_6e
    .packed-switch 0x0
        :pswitch_3a
        :pswitch_41
        :pswitch_48
        :pswitch_5b
    .end packed-switch
.end method

.method protected D()V
    .registers 1

    .prologue
    .line 1218
    return-void
.end method

.method protected E()V
    .registers 1

    .prologue
    .line 1222
    return-void
.end method

.method public F()I
    .registers 3

    .prologue
    .line 1228
    invoke-virtual {p0}, Lbf/i;->az()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_11

    .line 1230
    :cond_f
    const/4 v0, 0x0

    .line 1232
    :goto_10
    return v0

    :cond_11
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0}, Lbf/i;->x()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_10
.end method

.method protected G()I
    .registers 2

    .prologue
    .line 1239
    const/4 v0, -0x1

    return v0
.end method

.method public H()Z
    .registers 2

    .prologue
    .line 1264
    iget-boolean v0, p0, Lbf/i;->v:Z

    return v0
.end method

.method public I()Z
    .registers 2

    .prologue
    .line 1272
    iget-boolean v0, p0, Lbf/i;->w:Z

    return v0
.end method

.method public J()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 1290
    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1291
    invoke-virtual {p0, v1}, Lbf/i;->d(Z)V

    .line 1292
    invoke-virtual {p0}, Lbf/i;->l()V

    .line 1299
    :cond_d
    :goto_d
    return-void

    .line 1293
    :cond_e
    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1294
    invoke-virtual {p0, v1}, Lbf/i;->e(Z)V

    .line 1295
    invoke-virtual {p0}, Lbf/i;->m()V

    goto :goto_d

    .line 1296
    :cond_1b
    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1297
    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_d
.end method

.method protected K()V
    .registers 4

    .prologue
    .line 1339
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1357
    :cond_a
    :goto_a
    return-void

    .line 1343
    :cond_b
    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    .line 1344
    if-eqz v0, :cond_a

    .line 1350
    invoke-interface {v0}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v0

    .line 1354
    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 1355
    iget-object v1, p0, Lbf/i;->d:LaN/u;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    invoke-virtual {v1, v0}, LaN/u;->a(Lo/D;)V

    goto :goto_a
.end method

.method public L()Z
    .registers 2

    .prologue
    .line 1393
    invoke-virtual {p0}, Lbf/i;->O()Z

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {p0}, Lbf/i;->Q()Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public M()Z
    .registers 2

    .prologue
    .line 1401
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-gtz v0, :cond_10

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public N()Z
    .registers 3

    .prologue
    .line 1410
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method protected O()Z
    .registers 2

    .prologue
    .line 1419
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {p0}, Lbf/i;->ax()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method protected P()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1429
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1431
    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v1

    if-nez v1, :cond_18

    invoke-virtual {p0}, Lbf/i;->ax()Z

    move-result v1

    if-eqz v1, :cond_18

    const/4 v0, 0x1

    .line 1438
    :cond_18
    :goto_18
    return v0

    .line 1432
    :cond_19
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-nez v1, :cond_18

    .line 1438
    invoke-virtual {p0}, Lbf/i;->ax()Z

    move-result v0

    goto :goto_18
.end method

.method protected Q()Z
    .registers 2

    .prologue
    .line 1447
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ax()Z

    move-result v0

    return v0
.end method

.method protected R()V
    .registers 2

    .prologue
    .line 1468
    iget-object v0, p0, Lbf/i;->z:Lbf/k;

    if-eqz v0, :cond_9

    .line 1469
    iget-object v0, p0, Lbf/i;->z:Lbf/k;

    invoke-interface {v0, p0}, Lbf/k;->c(Lbf/i;)V

    .line 1471
    :cond_9
    return-void
.end method

.method protected S()V
    .registers 4

    .prologue
    .line 1487
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {p0}, Lbf/i;->U()I

    move-result v1

    invoke-virtual {p0}, Lbf/i;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    .line 1489
    return-void
.end method

.method protected T()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1496
    const/4 v0, 0x0

    return-object v0
.end method

.method protected U()I
    .registers 2

    .prologue
    .line 1504
    const/4 v0, 0x5

    return v0
.end method

.method protected V()V
    .registers 2

    .prologue
    .line 1526
    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_c

    .line 1527
    iget-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 1528
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    .line 1530
    :cond_c
    return-void
.end method

.method protected W()V
    .registers 3

    .prologue
    .line 1540
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1a

    .line 1541
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    .line 1545
    :goto_d
    invoke-virtual {p0}, Lbf/i;->r()V

    .line 1548
    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1549
    invoke-virtual {p0}, Lbf/i;->S()V

    .line 1553
    :goto_19
    return-void

    .line 1543
    :cond_1a
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    goto :goto_d

    .line 1551
    :cond_1f
    invoke-virtual {p0}, Lbf/i;->V()V

    goto :goto_19
.end method

.method protected X()Z
    .registers 2

    .prologue
    .line 1564
    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0}, Lbh/a;->a()Z

    move-result v0

    return v0
.end method

.method public Y()V
    .registers 1

    .prologue
    .line 1752
    return-void
.end method

.method public Z()V
    .registers 2

    .prologue
    .line 1756
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    .line 1757
    invoke-virtual {p0}, Lbf/i;->al()V

    .line 1758
    invoke-virtual {p0}, Lbf/i;->r()V

    .line 1759
    return-void
.end method

.method protected a(Lcom/google/googlenav/E;LaN/B;)J
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2047
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v3

    .line 2048
    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    .line 2049
    if-eqz v0, :cond_50

    .line 2050
    invoke-virtual {p0, p1}, Lbf/i;->d(Lcom/google/googlenav/E;)I

    move-result v1

    .line 2051
    invoke-virtual {p0, p1}, Lbf/i;->e(Lcom/google/googlenav/E;)I

    move-result v4

    .line 2052
    invoke-virtual {p0, p1}, Lbf/i;->f(Lcom/google/googlenav/E;)I

    move-result v5

    .line 2056
    packed-switch v1, :pswitch_data_56

    :pswitch_1c
    move v1, v2

    .line 2076
    :goto_1d
    if-nez v2, :cond_21

    if-eqz v1, :cond_25

    .line 2077
    :cond_21
    invoke-virtual {v0, v2, v1, v3}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    .line 2079
    :cond_25
    invoke-virtual {p2, v0, v3}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v0

    .line 2081
    :goto_29
    return-wide v0

    .line 2058
    :pswitch_2a
    iget-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/bi;->b(B)I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v2, v1, 0x2

    .line 2059
    iget-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/googlenav/ui/bi;->a(B)I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    goto :goto_1d

    .line 2063
    :pswitch_45
    neg-int v1, v5

    div-int/lit8 v1, v1, 0x2

    goto :goto_1d

    .line 2066
    :pswitch_49
    neg-int v1, v4

    div-int/lit8 v2, v1, 0x2

    .line 2067
    neg-int v1, v5

    div-int/lit8 v1, v1, 0x2

    goto :goto_1d

    .line 2081
    :cond_50
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_29

    .line 2056
    :pswitch_data_56
    .packed-switch 0x0
        :pswitch_45
        :pswitch_1c
        :pswitch_49
        :pswitch_2a
    .end packed-switch
.end method

.method protected a(LaN/B;IILaN/Y;)LaN/B;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 670
    mul-int v0, p2, p2

    mul-int v1, p3, p3

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 671
    int-to-double v2, p3

    int-to-double v4, p2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    iget-object v4, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v4}, LaN/u;->e()F

    move-result v4

    float-to-double v4, v4

    const-wide v6, 0x400921fb54442d18L

    mul-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 672
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v4, v0

    double-to-int v4, v4

    .line 673
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 677
    if-nez v4, :cond_34

    if-eqz v0, :cond_38

    .line 678
    :cond_34
    invoke-virtual {p1, v4, v0, p4}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object p1

    .line 680
    :cond_38
    return-object p1
.end method

.method public a(LaN/B;LaN/Y;)LaN/B;
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 733
    .line 735
    invoke-virtual {p0}, Lbf/i;->aj()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 736
    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v1

    .line 737
    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v2

    .line 744
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {p0, v1}, Lbf/i;->e(Lcom/google/googlenav/E;)I

    move-result v3

    invoke-virtual {p0, v1}, Lbf/i;->f(Lcom/google/googlenav/E;)I

    move-result v4

    aget v5, v2, v9

    const/4 v6, 0x1

    aget v6, v2, v6

    invoke-virtual {p0, v1}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v7

    invoke-virtual {p0}, Lbf/i;->q()I

    move-result v8

    invoke-virtual {p0, v9}, Lbf/i;->c(Z)I

    move-result v9

    move-object v2, p1

    move-object v10, p2

    invoke-virtual/range {v0 .. v10}, LaN/u;->a(Lcom/google/googlenav/E;LaN/B;IIIIIIILaN/Y;)LaN/B;

    move-result-object p1

    .line 749
    :cond_32
    return-object p1
.end method

.method protected a(Lcom/google/googlenav/E;)LaN/B;
    .registers 5
    .parameter

    .prologue
    .line 1887
    invoke-interface {p1}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_26

    move-object v0, p1

    .line 1888
    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    .line 1889
    if-nez v0, :cond_11

    .line 1890
    const/4 v0, 0x0

    .line 1900
    :goto_10
    return-object v0

    .line 1892
    :cond_11
    invoke-interface {v0}, LaN/g;->a()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1f

    invoke-interface {v0}, LaN/g;->a()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_26

    .line 1896
    :cond_1f
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    goto :goto_10

    .line 1900
    :cond_26
    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    goto :goto_10
.end method

.method protected a(Lcom/google/googlenav/E;LaN/Y;)LaN/B;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 657
    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    .line 658
    if-eqz v0, :cond_1a

    .line 659
    invoke-virtual {p0, p1}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lbf/i;->c(Z)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 660
    invoke-virtual {p0}, Lbf/i;->p()I

    move-result v2

    .line 661
    invoke-virtual {p0, v0, v1, v2, p2}, Lbf/i;->a(LaN/B;IILaN/Y;)LaN/B;

    move-result-object v0

    .line 663
    :cond_1a
    return-object v0
.end method

.method public a(LaN/B;)Lo/D;
    .registers 3
    .parameter

    .prologue
    .line 3071
    iget-object v0, p0, Lbf/i;->c:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v0

    return-object v0
.end method

.method public final a(B)V
    .registers 2
    .parameter

    .prologue
    .line 517
    packed-switch p1, :pswitch_data_14

    .line 531
    :goto_3
    return-void

    .line 519
    :pswitch_4
    invoke-virtual {p0}, Lbf/i;->l()V

    goto :goto_3

    .line 522
    :pswitch_8
    invoke-virtual {p0}, Lbf/i;->m()V

    goto :goto_3

    .line 525
    :pswitch_c
    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_3

    .line 528
    :pswitch_10
    invoke-virtual {p0}, Lbf/i;->Z()V

    goto :goto_3

    .line 517
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_10
        :pswitch_c
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 575
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    .line 595
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lbf/i;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 596
    invoke-virtual {p0}, Lbf/i;->m()V

    .line 597
    return-void
.end method

.method protected a(ILjava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2915
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2916
    return-void
.end method

.method protected a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2920
    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v0

    sparse-switch v0, :sswitch_data_7a

    .line 2947
    :goto_7
    return-void

    .line 2922
    :sswitch_8
    const-string v0, "0"

    .line 2939
    :goto_a
    const/16 v2, 0x12

    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "a="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x3

    if-nez p4, :cond_65

    const/4 v1, 0x0

    :goto_55
    aput-object v1, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 2925
    :sswitch_5f
    const-string v0, "dd"

    goto :goto_a

    .line 2932
    :sswitch_62
    const-string v0, "ct"

    goto :goto_a

    .line 2939
    :cond_65
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_55

    .line 2920
    nop

    :sswitch_data_7a
    .sparse-switch
        0x0 -> :sswitch_8
        0x1 -> :sswitch_5f
        0x6 -> :sswitch_62
        0x7 -> :sswitch_62
        0xb -> :sswitch_62
        0x10 -> :sswitch_62
        0x1a -> :sswitch_62
    .end sparse-switch
.end method

.method public final a(Lam/f;)V
    .registers 2
    .parameter

    .prologue
    .line 2612
    iput-object p1, p0, Lbf/i;->n:Lam/f;

    .line 2613
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 1040
    return-void
.end method

.method public a(Lbf/j;)V
    .registers 2
    .parameter

    .prologue
    .line 1474
    iput-object p1, p0, Lbf/i;->A:Lbf/j;

    .line 1475
    return-void
.end method

.method public a(Lbf/k;)V
    .registers 2
    .parameter

    .prologue
    .line 1464
    iput-object p1, p0, Lbf/i;->z:Lbf/k;

    .line 1465
    return-void
.end method

.method public final a(Lcom/google/googlenav/E;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2907
    invoke-virtual {p0, p2}, Lbf/i;->g(I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2908
    new-instance v0, Lcom/google/googlenav/R;

    invoke-direct {v0}, Lcom/google/googlenav/R;-><init>()V

    .line 2909
    invoke-virtual {p0, v0, p1, p2}, Lbf/i;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    .line 2910
    invoke-virtual {v0}, Lcom/google/googlenav/R;->a()V

    .line 2912
    :cond_11
    return-void
.end method

.method protected abstract a(Lcom/google/googlenav/F;)V
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2963
    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(I)Lcom/google/googlenav/R;

    .line 2964
    invoke-virtual {p1, p3}, Lcom/google/googlenav/R;->b(I)Lcom/google/googlenav/R;

    .line 2965
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 2
    .parameter

    .prologue
    .line 2006
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .registers 3
    .parameter

    .prologue
    .line 1253
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->B()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1254
    invoke-virtual {p0}, Lbf/i;->ac()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1255
    invoke-virtual {p0, p1}, Lbf/i;->c(Lcom/google/googlenav/ui/r;)V

    .line 1257
    :cond_11
    invoke-virtual {p0}, Lbf/i;->aa()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1258
    invoke-direct {p0, p1}, Lbf/i;->f(Lcom/google/googlenav/ui/r;)V

    .line 1261
    :cond_1a
    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1720
    invoke-interface {p2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    .line 1721
    if-eqz v0, :cond_22

    .line 1722
    iget-object v1, p0, Lbf/i;->c:LaN/p;

    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v1, v0, v2}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    .line 1723
    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/googlenav/E;->c()B

    move-result v2

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/bi;->a(Lam/e;BII)V

    .line 1726
    :cond_22
    return-void
.end method

.method protected a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/F;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1699
    if-nez p1, :cond_3

    .line 1711
    :cond_2
    return-void

    .line 1704
    :cond_3
    const/4 v0, 0x0

    invoke-interface {p2}, Lcom/google/googlenav/F;->f()I

    move-result v1

    :goto_8
    if-ge v0, v1, :cond_2

    .line 1705
    invoke-interface {p2, v0}, Lcom/google/googlenav/F;->c(I)I

    move-result v2

    .line 1706
    invoke-interface {p2, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    .line 1707
    if-eqz v2, :cond_1d

    invoke-direct {p0, v2}, Lbf/i;->g(Lcom/google/googlenav/E;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1708
    invoke-virtual {p0, p1, v2}, Lbf/i;->a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V

    .line 1704
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    goto :goto_8
.end method

.method protected a(Ljava/io/DataOutput;)V
    .registers 2
    .parameter

    .prologue
    .line 2710
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0xb

    .line 2891
    invoke-virtual {p0, v2}, Lbf/i;->g(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 2892
    new-instance v0, Lcom/google/googlenav/R;

    invoke-direct {v0}, Lcom/google/googlenav/R;-><init>()V

    .line 2893
    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lbf/i;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    .line 2895
    invoke-virtual {v0, p1}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 2896
    invoke-virtual {v0}, Lcom/google/googlenav/R;->a()V

    .line 2898
    :cond_1a
    return-void
.end method

.method public a(Ljava/util/Vector;LaN/B;I)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2018
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-ge v0, v1, :cond_29

    .line 2019
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    .line 2020
    invoke-interface {v1}, Lcom/google/googlenav/E;->e()Z

    move-result v2

    if-nez v2, :cond_18

    .line 2018
    :cond_15
    :goto_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2026
    :cond_18
    invoke-virtual {p0, v1, p2}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v2

    .line 2027
    int-to-long v4, p3

    cmp-long v4, v2, v4

    if-gez v4, :cond_15

    .line 2028
    invoke-static {p0, v1, v0, v2, v3}, Lbf/ai;->a(Lbf/i;Lcom/google/googlenav/E;IJ)Lbf/ai;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_15

    .line 2031
    :cond_29
    return-void
.end method

.method protected a(ZLaN/Y;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 628
    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    .line 629
    if-eqz v0, :cond_3d

    invoke-interface {v0}, Lcom/google/googlenav/E;->e()Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 630
    invoke-virtual {p0, v0, p2}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/Y;)LaN/B;

    move-result-object v1

    .line 631
    if-eqz v1, :cond_1e

    .line 632
    iget-object v2, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->R()V

    .line 633
    if-eqz p1, :cond_3e

    .line 634
    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v2, v1, p2}, LaN/u;->d(LaN/B;LaN/Y;)V

    .line 643
    :cond_1e
    :goto_1e
    sget-object v1, LaE/d;->a:LaE/d;

    invoke-virtual {v1}, LaE/d;->e()Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 644
    invoke-interface {v0}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-nez v1, :cond_49

    .line 645
    check-cast v0, Lcom/google/googlenav/ai;

    .line 646
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(LaN/B;Ljava/lang/String;)V

    .line 654
    :cond_3d
    :goto_3d
    return-void

    .line 636
    :cond_3e
    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v2, v1}, LaN/u;->c(LaN/B;)V

    .line 637
    iget-object v1, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v1, p2}, LaN/u;->a(LaN/Y;)V

    goto :goto_1e

    .line 649
    :cond_49
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/intersectionexplorer/d;->a(LaN/B;Ljava/lang/String;)V

    goto :goto_3d
.end method

.method protected final a(CI)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2864
    new-instance v0, Lat/a;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p1, p2, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {p0, v0}, Lbf/i;->a(Lat/a;)Z

    move-result v0

    return v0
.end method

.method protected a(I)Z
    .registers 5
    .parameter

    .prologue
    .line 927
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lbf/am;->a(Lbf/i;I)V

    .line 930
    const-string v0, "o"

    const-string v1, "c"

    invoke-virtual {p0}, Lbf/i;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v1, v2}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    const/4 v0, 0x1

    return v0
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 987
    packed-switch p1, :pswitch_data_20

    .line 1002
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 989
    :pswitch_5
    check-cast p3, Ljava/lang/String;

    .line 990
    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 991
    const-string v0, "LaunchUrl"

    const-string v1, "missing url"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    :goto_14
    const/4 v0, 0x1

    goto :goto_4

    .line 994
    :cond_16
    invoke-virtual {p0, p3}, Lbf/i;->a(Ljava/lang/String;)V

    .line 997
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto :goto_14

    .line 987
    :pswitch_data_20
    .packed-switch 0x5
        :pswitch_5
    .end packed-switch
.end method

.method public a(LaN/B;I)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2418
    if-eqz p1, :cond_18

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ak()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 2420
    :cond_18
    const/4 v0, 0x0

    .line 2427
    :goto_19
    return v0

    .line 2422
    :cond_1a
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p2

    if-nez v0, :cond_2d

    .line 2423
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {p0, p1, p2}, Lbf/i;->b(LaN/B;I)Z

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Boolean;-><init>(Z)V

    aput-object v1, v0, p2

    .line 2427
    :cond_2d
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_19
.end method

.method public a(LaN/B;LaN/B;I)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2483
    if-eqz p1, :cond_1a

    if-eqz p2, :cond_1a

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ak()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 2485
    :cond_1a
    const/4 v0, 0x0

    .line 2492
    :goto_1b
    return v0

    .line 2487
    :cond_1c
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p3

    if-nez v0, :cond_2f

    .line 2488
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {p0, p1, p2, p3}, Lbf/i;->b(LaN/B;LaN/B;I)Z

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Boolean;-><init>(Z)V

    aput-object v1, v0, p3

    .line 2492
    :cond_2f
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, p3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1b
.end method

.method public a(LaN/g;)Z
    .registers 3
    .parameter

    .prologue
    .line 887
    if-eqz p1, :cond_a

    invoke-static {}, Lcom/google/googlenav/K;->x()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final a(Lat/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lbf/i;->f(Lat/a;)Z

    move-result v0

    return v0
.end method

.method public a(Lat/b;)Z
    .registers 3
    .parameter

    .prologue
    .line 838
    invoke-virtual {p0}, Lbf/i;->aj()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lbf/i;->a(Lat/b;Z)Z

    move-result v0

    return v0
.end method

.method protected a(Lat/b;Z)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 842
    .line 844
    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v2

    if-nez v2, :cond_e

    invoke-virtual {p1}, Lat/b;->f()Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 848
    :cond_e
    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v3

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LaN/u;->b(II)LaN/B;

    move-result-object v2

    .line 849
    invoke-virtual {p0, v2}, Lbf/i;->b(LaN/B;)I

    move-result v2

    .line 850
    if-ltz v2, :cond_27

    .line 851
    invoke-virtual {p0, v2}, Lbf/i;->a(I)Z

    move-result v0

    .line 880
    :cond_26
    :goto_26
    return v0

    .line 853
    :cond_27
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_35

    .line 858
    invoke-virtual {p0}, Lbf/i;->al()V

    goto :goto_26

    :cond_35
    move v2, v1

    .line 868
    :goto_36
    if-eqz v2, :cond_26

    .line 870
    const/4 v2, -0x1

    const-string v3, "c"

    const-string v4, "c"

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    invoke-virtual {p0}, Lbf/i;->Z()V

    .line 873
    if-eqz p2, :cond_26

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-nez v2, :cond_26

    move v0, v1

    .line 877
    goto :goto_26

    .line 864
    :cond_4e
    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v2

    if-eqz v2, :cond_56

    move v2, v1

    .line 865
    goto :goto_36

    :cond_56
    move v2, v0

    goto :goto_36
.end method

.method public a(Lbf/i;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2293
    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v1

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v2

    if-eq v1, v2, :cond_c

    .line 2309
    :cond_b
    :goto_b
    return v0

    .line 2299
    :cond_c
    invoke-virtual {p0}, Lbf/i;->aB()Z

    move-result v1

    if-eqz v1, :cond_38

    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v1

    if-eqz v1, :cond_38

    .line 2300
    invoke-virtual {p0}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2302
    const/4 v0, 0x1

    goto :goto_b

    .line 2309
    :cond_38
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_b
.end method

.method protected a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 2132
    if-nez p1, :cond_4

    .line 2155
    :cond_3
    :goto_3
    return v7

    .line 2138
    :cond_4
    invoke-interface {p1}, Lcom/google/googlenav/E;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2142
    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    .line 2144
    invoke-virtual {p0, p1}, Lbf/i;->d(Lcom/google/googlenav/E;)I

    move-result v3

    .line 2145
    invoke-virtual {p0, p1}, Lbf/i;->e(Lcom/google/googlenav/E;)I

    move-result v4

    .line 2146
    invoke-virtual {p0, p1}, Lbf/i;->f(Lcom/google/googlenav/E;)I

    move-result v5

    .line 2150
    const/4 v0, 0x3

    if-ne v3, v0, :cond_3e

    .line 2151
    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->b(B)I

    move-result v6

    .line 2152
    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->a(B)I

    move-result v7

    .line 2155
    :goto_31
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    move-object v1, p2

    move-object v8, p3

    invoke-virtual/range {v0 .. v8}, Lbf/am;->a(LaN/B;LaN/B;IIIIILam/e;)Z

    move-result v7

    goto :goto_3

    :cond_3e
    move v6, v7

    goto :goto_31
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x36

    const/16 v3, 0x34

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 785
    iget-object v2, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lbf/i;->k:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_18

    .line 786
    new-instance v0, Lat/a;

    invoke-direct {v0, v3, v3, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {p0, v0}, Lbf/i;->e(Lat/a;)Z

    move-result v0

    .line 807
    :goto_17
    return v0

    .line 788
    :cond_18
    iget-object v2, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lbf/i;->j:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_2a

    .line 789
    new-instance v0, Lat/a;

    invoke-direct {v0, v4, v4, v1, v1}, Lat/a;-><init>(IIIZ)V

    invoke-virtual {p0, v0}, Lbf/i;->e(Lat/a;)Z

    move-result v0

    goto :goto_17

    .line 791
    :cond_2a
    iget-object v2, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_45

    iget-object v2, p0, Lbf/i;->l:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_45

    .line 792
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_40

    .line 793
    invoke-direct {p0}, Lbf/i;->be()V

    goto :goto_17

    .line 796
    :cond_40
    invoke-virtual {p0}, Lbf/i;->aY()Z

    :cond_43
    move v0, v1

    .line 807
    goto :goto_17

    .line 798
    :cond_45
    iget-object v2, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_53

    iget-object v2, p0, Lbf/i;->m:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_53

    .line 799
    iget-object v1, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->n(Z)V

    goto :goto_17

    .line 801
    :cond_53
    iget-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v2, :cond_43

    iget-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v2, :cond_43

    .line 802
    invoke-virtual {p0}, Lbf/i;->m()V

    .line 803
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    const-string v2, "s"

    const-string v3, "c"

    invoke-virtual {p0}, Lbf/i;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lbf/i;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_17
.end method

.method protected a(Ljava/io/DataInput;)Z
    .registers 3
    .parameter

    .prologue
    .line 2724
    const/4 v0, 0x1

    return v0
.end method

.method public aA()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 2359
    move v2, v0

    move v1, v0

    .line 2360
    :goto_3
    const/4 v0, 0x4

    if-ge v2, v0, :cond_22

    .line 2361
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, v2

    if-eqz v0, :cond_23

    .line 2362
    iget-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x3

    :goto_17
    mul-int/lit8 v3, v2, 0x2

    shl-int/2addr v0, v3

    or-int/2addr v0, v1

    .line 2360
    :goto_1b
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    .line 2362
    :cond_20
    const/4 v0, 0x2

    goto :goto_17

    .line 2365
    :cond_22
    return v1

    :cond_23
    move v0, v1

    goto :goto_1b
.end method

.method public aB()Z
    .registers 2

    .prologue
    .line 2388
    const/4 v0, 0x0

    return v0
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 2397
    const/4 v0, 0x1

    return v0
.end method

.method public aD()Z
    .registers 2

    .prologue
    .line 2405
    const/4 v0, 0x0

    return v0
.end method

.method public aE()Z
    .registers 2

    .prologue
    .line 2543
    const/4 v0, 0x1

    return v0
.end method

.method public aF()I
    .registers 2

    .prologue
    .line 2551
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ap()I

    move-result v0

    return v0
.end method

.method public aG()I
    .registers 2

    .prologue
    .line 2559
    const v0, 0x7f020222

    return v0
.end method

.method public aH()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 2567
    const/16 v0, 0x2a7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aI()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 2576
    const/4 v0, 0x0

    return-object v0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 2584
    const/16 v0, 0xe4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .registers 2

    .prologue
    .line 2594
    const/4 v0, 0x0

    return-object v0
.end method

.method public aL()Lam/f;
    .registers 2

    .prologue
    .line 2604
    iget-object v0, p0, Lbf/i;->n:Lam/f;

    return-object v0
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 2636
    const/4 v0, 0x0

    return v0
.end method

.method protected aN()Z
    .registers 2

    .prologue
    .line 2647
    const/4 v0, 0x0

    return v0
.end method

.method public final aO()I
    .registers 2

    .prologue
    .line 2655
    iget v0, p0, Lbf/i;->u:I

    return v0
.end method

.method protected aP()V
    .registers 6

    .prologue
    .line 2678
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    .line 2679
    invoke-static {p0}, Lbf/am;->m(Lbf/i;)Ljava/lang/String;

    move-result-object v2

    .line 2681
    :try_start_c
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2682
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 2683
    invoke-virtual {p0, v3}, Lbf/i;->a(Ljava/io/DataOutput;)V

    .line 2684
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_20} :catch_21
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_20} :catch_52

    .line 2698
    :goto_20
    return-void

    .line 2685
    :catch_21
    move-exception v0

    .line 2688
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Layer Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UI"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Error saving"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2690
    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_20

    .line 2691
    :catch_52
    move-exception v0

    .line 2694
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Layer Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UI"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Error saving"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2696
    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_20
.end method

.method public aQ()V
    .registers 2

    .prologue
    .line 2734
    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    iput v0, p0, Lbf/i;->B:I

    .line 2735
    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    iput-byte v0, p0, Lbf/i;->C:B

    .line 2736
    return-void
.end method

.method public aR()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 2743
    invoke-virtual {p0}, Lbf/i;->ah()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2744
    iget v0, p0, Lbf/i;->B:I

    if-eq v0, v2, :cond_10

    .line 2745
    iget v0, p0, Lbf/i;->B:I

    invoke-virtual {p0, v0}, Lbf/i;->b(I)V

    .line 2747
    :cond_10
    iget-byte v0, p0, Lbf/i;->C:B

    if-eqz v0, :cond_1d

    .line 2748
    invoke-virtual {p0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    iget-byte v1, p0, Lbf/i;->C:B

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    .line 2750
    :cond_1d
    invoke-virtual {p0}, Lbf/i;->y()V

    .line 2752
    :cond_20
    iput v2, p0, Lbf/i;->B:I

    .line 2753
    const/4 v0, 0x0

    iput-byte v0, p0, Lbf/i;->C:B

    .line 2754
    return-void
.end method

.method public final aS()Z
    .registers 2

    .prologue
    .line 2763
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    if-eqz v0, :cond_9

    .line 2764
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v0}, Lbf/i;->b(Lcom/google/googlenav/F;)V

    .line 2766
    :cond_9
    invoke-virtual {p0}, Lbf/i;->aT()Z

    move-result v0

    return v0
.end method

.method protected aT()Z
    .registers 2

    .prologue
    .line 2776
    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .registers 1

    .prologue
    .line 2784
    return-void
.end method

.method public aV()V
    .registers 1

    .prologue
    .line 2792
    invoke-virtual {p0}, Lbf/i;->j()V

    .line 2793
    return-void
.end method

.method public aW()V
    .registers 2

    .prologue
    .line 2802
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/i;->o:Z

    .line 2805
    invoke-virtual {p0}, Lbf/i;->y()V

    .line 2808
    invoke-virtual {p0}, Lbf/i;->j()V

    .line 2809
    return-void
.end method

.method public aX()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 2815
    iput-boolean v0, p0, Lbf/i;->o:Z

    .line 2819
    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    .line 2820
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(I)V

    .line 2822
    invoke-virtual {p0}, Lbf/i;->r()V

    .line 2823
    invoke-virtual {p0}, Lbf/i;->al()V

    .line 2826
    invoke-direct {p0}, Lbf/i;->d()V

    .line 2827
    invoke-virtual {p0}, Lbf/i;->V()V

    .line 2828
    invoke-direct {p0}, Lbf/i;->f()V

    .line 2829
    return-void
.end method

.method public final aY()Z
    .registers 3

    .prologue
    .line 2853
    const/16 v0, 0x23

    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lbf/i;->a(CI)Z

    move-result v0

    return v0
.end method

.method public aZ()Z
    .registers 2

    .prologue
    .line 2873
    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2874
    const/4 v0, 0x0

    .line 2879
    :goto_7
    return v0

    .line 2876
    :cond_8
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/S;

    if-nez v0, :cond_10

    .line 2877
    const/4 v0, 0x1

    goto :goto_7

    .line 2879
    :cond_10
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/S;->P_()Z

    move-result v0

    goto :goto_7
.end method

.method public aa()Z
    .registers 2

    .prologue
    .line 1766
    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public ab()Z
    .registers 2

    .prologue
    .line 1773
    const/4 v0, 0x0

    return v0
.end method

.method protected ac()Z
    .registers 2

    .prologue
    .line 1780
    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ad()Z
    .registers 2

    .prologue
    .line 1794
    const/4 v0, 0x1

    return v0
.end method

.method public final ae()Z
    .registers 3

    .prologue
    .line 1801
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final af()Z
    .registers 3

    .prologue
    .line 1808
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final ag()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1816
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    if-ne v1, v0, :cond_a

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final ah()Z
    .registers 2

    .prologue
    .line 1823
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public ai()Z
    .registers 2

    .prologue
    .line 1831
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final aj()Z
    .registers 3

    .prologue
    .line 1838
    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_25

    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v0

    if-nez v0, :cond_23

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-virtual {p0}, Lbf/i;->ah()Z

    move-result v0

    if-nez v0, :cond_25

    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_25

    :cond_23
    const/4 v0, 0x1

    :goto_24
    return v0

    :cond_25
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public ak()Lcom/google/googlenav/ui/view/d;
    .registers 2

    .prologue
    .line 1847
    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    return-object v0
.end method

.method public al()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1925
    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_19

    .line 1926
    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 1927
    iput-object v1, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    .line 1928
    iput-object v1, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    .line 1929
    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    if-eqz v0, :cond_17

    .line 1930
    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    invoke-interface {v0, p0}, Lbf/j;->b(Lbf/i;)V

    .line 1932
    :cond_17
    iput-object v1, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    .line 1934
    :cond_19
    return-void
.end method

.method protected am()V
    .registers 1

    .prologue
    .line 1939
    return-void
.end method

.method public an()Z
    .registers 2

    .prologue
    .line 1943
    const/4 v0, 0x0

    .line 1944
    invoke-virtual {p0, v0}, Lbf/i;->g(Z)Z

    move-result v0

    return v0
.end method

.method public ao()Z
    .registers 2

    .prologue
    .line 1996
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract ap()V
.end method

.method protected abstract aq()V
.end method

.method public ar()Lcom/google/googlenav/F;
    .registers 2

    .prologue
    .line 2223
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    return-object v0
.end method

.method public as()Z
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2231
    .line 2232
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    if-eqz v0, :cond_2e

    .line 2233
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v5

    move v4, v3

    move v1, v3

    :goto_e
    if-ge v4, v5, :cond_2f

    .line 2234
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v4}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    .line 2235
    invoke-interface {v0}, Lcom/google/googlenav/E;->d()I

    move-result v6

    if-eqz v6, :cond_23

    .line 2236
    add-int/lit8 v0, v1, 0x1

    .line 2233
    :goto_1e
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_e

    .line 2238
    :cond_23
    check-cast v0, Lcom/google/googlenav/ai;

    .line 2239
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_35

    .line 2240
    add-int/lit8 v0, v1, 0x1

    goto :goto_1e

    :cond_2e
    move v1, v3

    .line 2245
    :cond_2f
    if-ne v1, v2, :cond_33

    move v0, v2

    :goto_32
    return v0

    :cond_33
    move v0, v3

    goto :goto_32

    :cond_35
    move v0, v1

    goto :goto_1e
.end method

.method public at()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 2250
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    if-eqz v1, :cond_1d

    .line 2251
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v2

    move v1, v0

    :goto_c
    if-ge v1, v2, :cond_1d

    .line 2252
    iget-object v3, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v3, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    .line 2253
    if-eqz v3, :cond_1e

    invoke-interface {v3}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_1e

    .line 2254
    const/4 v0, 0x1

    .line 2258
    :cond_1d
    return v0

    .line 2251
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_c
.end method

.method public au()Z
    .registers 2

    .prologue
    .line 2271
    const/4 v0, 0x1

    return v0
.end method

.method public abstract av()I
.end method

.method public aw()LaN/Y;
    .registers 2

    .prologue
    .line 2330
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public ax()Z
    .registers 2

    .prologue
    .line 2334
    iget-boolean v0, p0, Lbf/i;->o:Z

    return v0
.end method

.method public ay()Z
    .registers 2

    .prologue
    .line 2338
    iget-boolean v0, p0, Lbf/i;->p:Z

    return v0
.end method

.method protected az()Z
    .registers 2

    .prologue
    .line 2346
    iget-boolean v0, p0, Lbf/i;->y:Z

    return v0
.end method

.method public b(LaN/B;)I
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2095
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 2096
    if-ltz v0, :cond_17

    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v3}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2118
    :cond_16
    :goto_16
    return v0

    :cond_17
    move v0, v1

    .line 2103
    :goto_18
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-ge v0, v2, :cond_31

    .line 2104
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    .line 2105
    invoke-virtual {p0, v2, p1, v3}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 2103
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 2111
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    :cond_31
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v1, v0, :cond_47

    .line 2112
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    .line 2113
    invoke-direct {p0, v0, p1}, Lbf/i;->b(Lcom/google/googlenav/E;LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_2f

    move v0, v1

    .line 2114
    goto :goto_16

    .line 2118
    :cond_47
    const/4 v0, -0x1

    goto :goto_16
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 1911
    const/4 v0, 0x0

    return v0
.end method

.method public final b(B)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 540
    packed-switch p1, :pswitch_data_46

    .line 564
    :goto_4
    return-void

    .line 544
    :pswitch_5
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p0}, Lbf/i;->ae()Z

    move-result v0

    if-nez v0, :cond_18

    .line 545
    invoke-virtual {p0, v1}, Lbf/i;->d(Z)V

    .line 547
    :cond_18
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_4

    .line 552
    :pswitch_1f
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-nez v0, :cond_32

    .line 553
    invoke-virtual {p0, v1}, Lbf/i;->e(Z)V

    .line 555
    :cond_32
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_4

    .line 558
    :pswitch_39
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_4

    .line 561
    :pswitch_3f
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    goto :goto_4

    .line 540
    :pswitch_data_46
    .packed-switch 0x0
        :pswitch_3f
        :pswitch_39
        :pswitch_1f
        :pswitch_5
    .end packed-switch
.end method

.method public b(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 944
    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v0

    .line 945
    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v1

    .line 947
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, p1}, Lcom/google/googlenav/F;->a(I)V

    .line 950
    invoke-direct {p0}, Lbf/i;->a()V

    .line 952
    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v2

    .line 953
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->as()Z

    move-result v3

    if-eqz v3, :cond_48

    invoke-virtual {p0}, Lbf/i;->ag()Z

    move-result v3

    if-eqz v3, :cond_48

    const/4 v3, 0x3

    if-eq v2, v3, :cond_33

    const/4 v3, 0x1

    if-eq v2, v3, :cond_33

    const/16 v3, 0x17

    if-eq v2, v3, :cond_33

    const/16 v3, 0x18

    if-ne v2, v3, :cond_48

    :cond_33
    invoke-virtual {p0}, Lbf/i;->N()Z

    move-result v2

    if-ne v0, v2, :cond_3f

    invoke-virtual {p0}, Lbf/i;->M()Z

    move-result v0

    if-eq v1, v0, :cond_48

    .line 966
    :cond_3f
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    .line 969
    :cond_48
    if-ne p1, v4, :cond_4d

    .line 970
    invoke-virtual {p0}, Lbf/i;->al()V

    .line 974
    :cond_4d
    if-eq p1, v4, :cond_59

    invoke-virtual {p0}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_59

    .line 975
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lbf/i;->f(I)V

    .line 978
    :cond_59
    invoke-virtual {p0}, Lbf/i;->K()V

    .line 979
    return-void
.end method

.method protected b(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 607
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lbf/i;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 608
    invoke-virtual {p0}, Lbf/i;->n()V

    .line 609
    return-void
.end method

.method public b(Lcom/google/googlenav/F;)V
    .registers 2
    .parameter

    .prologue
    .line 1459
    invoke-virtual {p0, p1}, Lbf/i;->a(Lcom/google/googlenav/F;)V

    .line 1460
    invoke-virtual {p0}, Lbf/i;->R()V

    .line 1461
    return-void
.end method

.method protected b(Lcom/google/googlenav/ui/r;)V
    .registers 2
    .parameter

    .prologue
    .line 1577
    return-void
.end method

.method protected b(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1859
    iget-object v1, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    if-nez v1, :cond_6

    .line 1883
    :cond_5
    :goto_5
    return-void

    .line 1863
    :cond_6
    invoke-virtual {p0, p2}, Lbf/i;->a(Lcom/google/googlenav/E;)LaN/B;

    move-result-object v1

    .line 1864
    if-eqz v1, :cond_5

    .line 1868
    iget-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/d;->e()Z

    move-result v2

    if-eqz v2, :cond_4b

    .line 1869
    iget-object v2, p0, Lbf/i;->c:LaN/p;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    .line 1874
    :goto_1b
    if-nez p1, :cond_53

    move v1, v0

    .line 1875
    :goto_1e
    if-nez p1, :cond_58

    .line 1876
    :goto_20
    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    add-int/2addr v1, v3

    invoke-virtual {p0, p2}, Lbf/i;->b(Lcom/google/googlenav/E;)I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v0, v3

    invoke-virtual {p0, p2}, Lbf/i;->c(Lcom/google/googlenav/E;)I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Point;->set(II)V

    .line 1880
    if-nez p1, :cond_43

    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->e()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1881
    :cond_43
    iget-object v0, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    goto :goto_5

    .line 1871
    :cond_4b
    iget-object v2, p0, Lbf/i;->c:LaN/p;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->b(LaN/B;Landroid/graphics/Point;)V

    goto :goto_1b

    .line 1874
    :cond_53
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->c()I

    move-result v1

    goto :goto_1e

    .line 1875
    :cond_58
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->d()I

    move-result v0

    goto :goto_20
.end method

.method protected b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 617
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbf/i;->a(ZLaN/Y;)V

    .line 618
    return-void
.end method

.method public b(LaN/g;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 894
    if-nez p1, :cond_4

    .line 900
    :cond_3
    :goto_3
    return v0

    .line 897
    :cond_4
    invoke-virtual {p0, p1}, Lbf/i;->a(LaN/g;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 900
    invoke-interface {p1}, LaN/g;->b()LaN/B;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lbf/i;->a(LaN/B;I)Z

    move-result v0

    goto :goto_3
.end method

.method public final b(Lat/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 404
    invoke-virtual {p0, p1}, Lbf/i;->c(Lat/a;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 826
    const/4 v0, 0x0

    return v0
.end method

.method public ba()Lcom/google/googlenav/ui/s;
    .registers 2

    .prologue
    .line 2977
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method public bb()Lbh/a;
    .registers 2

    .prologue
    .line 2988
    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    return-object v0
.end method

.method public bc()Ljava/lang/String;
    .registers 2

    .prologue
    .line 2999
    const/4 v0, 0x0

    return-object v0
.end method

.method public bd()V
    .registers 2

    .prologue
    .line 3013
    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    if-eqz v0, :cond_9

    .line 3014
    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    invoke-interface {v0, p0}, Lbf/j;->a(Lbf/i;)V

    .line 3016
    :cond_9
    return-void
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 1921
    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 710
    iget-object v1, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->G()I

    move-result v2

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_1e

    sget v1, Lcom/google/googlenav/ui/bi;->c:I

    :goto_13
    add-int/2addr v1, v2

    .line 715
    if-nez p1, :cond_1c

    invoke-virtual {p0}, Lbf/i;->ai()Z

    move-result v2

    if-eqz v2, :cond_1d

    :cond_1c
    move v0, v1

    :cond_1d
    return v0

    :cond_1e
    move v1, v0

    .line 710
    goto :goto_13
.end method

.method public final c(I)Lcom/google/googlenav/e;
    .registers 3
    .parameter

    .prologue
    .line 1735
    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0, p1}, Lbh/a;->b(I)Lcom/google/googlenav/e;

    move-result-object v0

    return-object v0
.end method

.method protected c(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 759
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lbf/i;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 760
    invoke-virtual {p0}, Lbf/i;->l()V

    .line 761
    return-void
.end method

.method protected c(Lcom/google/googlenav/ui/r;)V
    .registers 6
    .parameter

    .prologue
    .line 1594
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    .line 1595
    if-eqz v0, :cond_3b

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_3b

    .line 1596
    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    .line 1597
    invoke-virtual {p0}, Lbf/i;->aj()Z

    move-result v2

    if-eqz v2, :cond_3b

    if-eqz v1, :cond_3b

    .line 1599
    invoke-interface {v0}, Lcom/google/googlenav/E;->c()B

    move-result v2

    if-eqz v2, :cond_38

    .line 1600
    iget-object v2, p0, Lbf/i;->c:LaN/p;

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    .line 1601
    iget-object v1, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v3}, Lcom/google/googlenav/F;->c()I

    move-result v3

    invoke-direct {p0, p1, v1, v2, v3}, Lbf/i;->a(Lcom/google/googlenav/ui/r;III)V

    .line 1603
    :cond_38
    invoke-virtual {p0, p1, v0}, Lbf/i;->b(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/E;)V

    .line 1606
    :cond_3b
    return-void
.end method

.method public c(LaN/B;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2531
    if-eqz p1, :cond_11

    iget-object v1, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ae()Lcom/google/googlenav/j;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v1

    if-eqz v1, :cond_11

    const/4 v0, 0x1

    :cond_11
    return v0
.end method

.method public c(LaN/g;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 907
    if-nez p1, :cond_4

    .line 913
    :cond_3
    :goto_3
    return v0

    .line 910
    :cond_4
    invoke-virtual {p0, p1}, Lbf/i;->a(LaN/g;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 913
    invoke-interface {p1}, LaN/g;->b()LaN/B;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lbf/i;->a(LaN/B;I)Z

    move-result v0

    goto :goto_3
.end method

.method protected c(Lat/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 408
    const/4 v0, 0x0

    return v0
.end method

.method protected d(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 2201
    iget-object v0, p0, Lbf/i;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/E;)I

    move-result v0

    return v0
.end method

.method protected d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 765
    new-instance v0, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    return-object v0
.end method

.method public d(I)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2373
    new-array v0, v6, [Ljava/lang/Boolean;

    iput-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    move v3, v2

    .line 2374
    :goto_8
    if-ge v3, v6, :cond_26

    .line 2375
    mul-int/lit8 v0, v3, 0x2

    .line 2376
    const/4 v4, 0x2

    shl-int/2addr v4, v0

    and-int/2addr v4, p1

    if-eqz v4, :cond_20

    .line 2377
    iget-object v4, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    new-instance v5, Ljava/lang/Boolean;

    shl-int v0, v1, v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_24

    move v0, v1

    :goto_1b
    invoke-direct {v5, v0}, Ljava/lang/Boolean;-><init>(Z)V

    aput-object v5, v4, v3

    .line 2374
    :cond_20
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    :cond_24
    move v0, v2

    .line 2377
    goto :goto_1b

    .line 2380
    :cond_26
    return-void
.end method

.method public d(Lcom/google/googlenav/ui/r;)V
    .registers 7
    .parameter

    .prologue
    .line 1615
    if-nez p1, :cond_3

    .line 1631
    :cond_2
    return-void

    .line 1620
    :cond_3
    const/4 v0, 0x0

    :goto_4
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1621
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->c(I)I

    move-result v1

    .line 1622
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    .line 1623
    if-eqz v2, :cond_38

    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v3

    if-eqz v3, :cond_38

    .line 1624
    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    .line 1625
    if-eqz v2, :cond_38

    .line 1626
    iget-object v3, p0, Lbf/i;->c:LaN/p;

    iget-object v4, p0, Lbf/i;->e:Landroid/graphics/Point;

    invoke-virtual {v3, v2, v4}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    .line 1627
    iget-object v2, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lbf/i;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-direct {p0, p1, v2, v3, v1}, Lbf/i;->a(Lcom/google/googlenav/ui/r;III)V

    .line 1620
    :cond_38
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method public d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1268
    iput-boolean p1, p0, Lbf/i;->v:Z

    .line 1269
    return-void
.end method

.method public d(Lat/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 415
    const/4 v0, 0x0

    return v0
.end method

.method protected e(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 2205
    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0, p1}, Lbh/a;->a(Lcom/google/googlenav/E;)I

    move-result v0

    return v0
.end method

.method public final e(I)V
    .registers 2
    .parameter

    .prologue
    .line 2666
    iput p1, p0, Lbf/i;->u:I

    .line 2667
    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .registers 3
    .parameter

    .prologue
    .line 1688
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, p1, v0}, Lbf/i;->a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/F;)V

    .line 1689
    return-void
.end method

.method public e(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1276
    iput-boolean p1, p0, Lbf/i;->w:Z

    .line 1277
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    .line 1278
    return-void
.end method

.method public e(Lat/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 814
    const/4 v0, 0x0

    return v0
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .registers 2

    .prologue
    .line 2326
    const/4 v0, 0x0

    return-object v0
.end method

.method protected f(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 2209
    iget-object v0, p0, Lbf/i;->t:Lbh/a;

    invoke-virtual {v0, p1}, Lbh/a;->b(Lcom/google/googlenav/E;)I

    move-result v0

    return v0
.end method

.method public final f(I)V
    .registers 3
    .parameter

    .prologue
    .line 2902
    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lbf/i;->a(Lcom/google/googlenav/E;I)V

    .line 2903
    return-void
.end method

.method public f(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1285
    iput-boolean p1, p0, Lbf/i;->x:Z

    .line 1286
    return-void
.end method

.method protected abstract f(Lat/a;)Z
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 3055
    invoke-virtual {p0}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-virtual {p0}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v0

    .line 3056
    :goto_b
    new-instance v2, Lcom/google/googlenav/common/util/l;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    move v0, v1

    move-object v1, v2

    .line 3057
    :goto_12
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->f()I

    move-result v2

    if-ge v0, v2, :cond_34

    .line 3058
    iget-object v2, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    .line 3059
    invoke-interface {v2}, Lcom/google/googlenav/E;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/common/util/l;->a(Lcom/google/googlenav/common/util/l;)Lcom/google/googlenav/common/util/l;

    move-result-object v1

    .line 3057
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 3055
    :cond_2b
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 3061
    :cond_34
    return-object v1
.end method

.method public g(I)Z
    .registers 3
    .parameter

    .prologue
    .line 2954
    const/4 v0, 0x1

    return v0
.end method

.method public g(Z)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1957
    invoke-virtual {p0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v1

    .line 1958
    if-nez v1, :cond_b

    .line 1959
    invoke-virtual {p0}, Lbf/i;->al()V

    .line 1988
    :cond_a
    :goto_a
    return v0

    .line 1963
    :cond_b
    iget-object v2, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    if-ne v1, v2, :cond_11

    if-eqz p1, :cond_3b

    .line 1964
    :cond_11
    invoke-virtual {p0}, Lbf/i;->am()V

    .line 1965
    iget-object v2, p0, Lbf/i;->s:Lbf/w;

    if-eqz v2, :cond_a

    .line 1970
    iget-object v2, p0, Lbf/i;->s:Lbf/w;

    invoke-interface {v2}, Lbf/w;->b()Lcom/google/googlenav/ui/view/d;

    move-result-object v2

    .line 1971
    if-nez v2, :cond_24

    .line 1972
    invoke-virtual {p0}, Lbf/i;->al()V

    goto :goto_a

    .line 1976
    :cond_24
    iput-object v2, p0, Lbf/i;->g:Lcom/google/googlenav/ui/view/d;

    .line 1977
    iput-object v1, p0, Lbf/i;->h:Lcom/google/googlenav/E;

    .line 1978
    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    if-eqz v0, :cond_31

    .line 1979
    iget-object v0, p0, Lbf/i;->A:Lbf/j;

    invoke-interface {v0, p0, v2}, Lbf/j;->a(Lbf/i;Lcom/google/googlenav/ui/view/d;)V

    .line 1983
    :cond_31
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v0

    iput-object v0, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    .line 1987
    :cond_3b
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lbf/i;->f(I)V

    .line 1988
    const/4 v0, 0x1

    goto :goto_a
.end method

.method public h()V
    .registers 1

    .prologue
    .line 426
    invoke-virtual {p0}, Lbf/i;->n()V

    .line 427
    return-void
.end method

.method public h(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2342
    iput-boolean p1, p0, Lbf/i;->p:Z

    .line 2343
    return-void
.end method

.method protected i()Lbh/a;
    .registers 2

    .prologue
    .line 374
    new-instance v0, Lbh/a;

    invoke-direct {v0, p0}, Lbh/a;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected i(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2350
    iput-boolean p1, p0, Lbf/i;->y:Z

    .line 2351
    return-void
.end method

.method protected j()V
    .registers 2

    .prologue
    .line 378
    invoke-virtual {p0}, Lbf/i;->O()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 379
    invoke-direct {p0}, Lbf/i;->b()V

    .line 384
    :goto_9
    invoke-virtual {p0}, Lbf/i;->P()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 385
    invoke-virtual {p0}, Lbf/i;->S()V

    .line 390
    :goto_12
    invoke-virtual {p0}, Lbf/i;->Q()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 391
    invoke-direct {p0}, Lbf/i;->c()V

    .line 395
    :goto_1b
    return-void

    .line 381
    :cond_1c
    invoke-direct {p0}, Lbf/i;->d()V

    goto :goto_9

    .line 387
    :cond_20
    invoke-virtual {p0}, Lbf/i;->V()V

    goto :goto_12

    .line 393
    :cond_24
    invoke-direct {p0}, Lbf/i;->f()V

    goto :goto_1b
.end method

.method public j(Z)V
    .registers 3
    .parameter

    .prologue
    .line 3007
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/S;

    if-eqz v0, :cond_11

    .line 3008
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/S;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/S;->a(Z)V

    .line 3010
    :cond_11
    return-void
.end method

.method public k()Lcom/google/googlenav/settings/e;
    .registers 2

    .prologue
    .line 419
    iget-object v0, p0, Lbf/i;->D:Lcom/google/googlenav/settings/e;

    return-object v0
.end method

.method protected l()V
    .registers 8

    .prologue
    .line 435
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    .line 436
    invoke-virtual {p0}, Lbf/i;->y()V

    .line 437
    const/16 v0, 0x19

    invoke-virtual {p0, v0}, Lbf/i;->f(I)V

    .line 439
    invoke-virtual {p0}, Lbf/i;->t()Ljava/lang/String;

    move-result-object v0

    .line 440
    if-eqz v0, :cond_36

    .line 441
    const/16 v1, 0x72

    const-string v2, ""

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 451
    :cond_36
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_5d

    iget-object v0, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->l()Z

    move-result v0

    if-nez v0, :cond_5d

    .line 452
    iget-object v0, p0, Lbf/i;->d:LaN/u;

    iget-object v1, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/i;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/i;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->b(LaN/B;)V

    .line 461
    :cond_5d
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 462
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/gk;->a(Lbf/i;)V

    .line 464
    :cond_7e
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    if-eqz v0, :cond_b3

    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ia;->o()Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 466
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/ia;->a(Lbf/i;)V

    .line 468
    :cond_b3
    return-void
.end method

.method protected m()V
    .registers 2

    .prologue
    .line 476
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    .line 477
    invoke-virtual {p0}, Lbf/i;->y()V

    .line 481
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 482
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(Z)V

    .line 490
    :cond_15
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 491
    iget-object v0, p0, Lbf/i;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gk;->a()V

    .line 493
    :cond_36
    return-void
.end method

.method protected n()V
    .registers 2

    .prologue
    .line 501
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/i;->b(B)V

    .line 502
    invoke-virtual {p0}, Lbf/i;->y()V

    .line 503
    return-void
.end method

.method public o()B
    .registers 2

    .prologue
    .line 507
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    return v0
.end method

.method protected p()I
    .registers 2

    .prologue
    .line 687
    invoke-virtual {p0}, Lbf/i;->q()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public q()I
    .registers 3

    .prologue
    .line 695
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->d()Lcom/google/googlenav/ui/android/ab;

    move-result-object v0

    .line 697
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ab;->a()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method protected r()V
    .registers 2

    .prologue
    .line 770
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_c

    .line 771
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 772
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 774
    :cond_c
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Boolean;

    iput-object v0, p0, Lbf/i;->q:[Ljava/lang/Boolean;

    .line 775
    return-void
.end method

.method public s()Lcom/google/googlenav/E;
    .registers 2

    .prologue
    .line 1007
    invoke-virtual {p0}, Lbf/i;->ah()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    iget-object v0, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_7
.end method

.method protected t()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1016
    const/4 v0, 0x0

    return-object v0
.end method

.method protected u()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1024
    const/4 v0, 0x0

    return-object v0
.end method

.method protected v()V
    .registers 1

    .prologue
    .line 1048
    return-void
.end method

.method protected w()V
    .registers 1

    .prologue
    .line 1057
    return-void
.end method

.method protected x()I
    .registers 2

    .prologue
    .line 1066
    invoke-virtual {p0}, Lbf/i;->au()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final y()V
    .registers 2

    .prologue
    .line 1077
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1078
    invoke-virtual {p0}, Lbf/i;->C()V

    .line 1085
    :goto_d
    invoke-virtual {p0}, Lbf/i;->D()V

    .line 1087
    invoke-virtual {p0}, Lbf/i;->E()V

    .line 1088
    return-void

    .line 1080
    :cond_14
    invoke-virtual {p0}, Lbf/i;->z()V

    goto :goto_d
.end method

.method protected z()V
    .registers 4

    .prologue
    .line 1096
    iget-boolean v0, p0, Lbf/i;->o:Z

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_f

    .line 1144
    :cond_e
    :goto_e
    return-void

    .line 1103
    :cond_f
    invoke-virtual {p0}, Lbf/i;->v()V

    .line 1105
    iget-object v0, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 1106
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_35

    .line 1107
    invoke-virtual {p0}, Lbf/i;->aq()V

    .line 1111
    iget-object v1, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v1, :cond_31

    .line 1112
    invoke-virtual {p0}, Lbf/i;->B()V

    .line 1141
    :goto_27
    if-eqz v0, :cond_e

    iget-object v1, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eq v0, v1, :cond_e

    .line 1142
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    goto :goto_e

    .line 1115
    :cond_31
    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_27

    .line 1117
    :cond_35
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4d

    .line 1118
    invoke-virtual {p0}, Lbf/i;->ap()V

    .line 1122
    iget-object v1, p0, Lbf/i;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v1, :cond_49

    .line 1123
    invoke-virtual {p0}, Lbf/i;->A()V

    goto :goto_27

    .line 1126
    :cond_49
    invoke-virtual {p0}, Lbf/i;->n()V

    goto :goto_27

    .line 1128
    :cond_4d
    iget-object v1, p0, Lbf/i;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5d

    .line 1129
    invoke-virtual {p0}, Lbf/i;->an()Z

    .line 1130
    invoke-virtual {p0}, Lbf/i;->r()V

    goto :goto_27

    .line 1133
    :cond_5d
    invoke-virtual {p0}, Lbf/i;->al()V

    .line 1134
    invoke-virtual {p0}, Lbf/i;->r()V

    goto :goto_27
.end method
