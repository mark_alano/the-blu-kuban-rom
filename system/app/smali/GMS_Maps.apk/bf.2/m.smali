.class public abstract Lbf/m;
.super Lbf/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/bb;


# static fields
.field public static final u:Lcom/google/common/base/x;

.field protected static z:Lcom/google/googlenav/ui/view/android/au;


# instance fields
.field protected A:Lbf/be;

.field private B:Lcom/google/googlenav/ui/a;

.field private final C:Ljava/util/Hashtable;

.field private D:Lcom/google/googlenav/ui/view/dialog/aq;

.field private E:Lcom/google/googlenav/ai;

.field protected v:I

.field protected w:Z

.field protected x:Lcom/google/googlenav/ui/br;

.field protected y:LaB/s;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 141
    new-instance v0, Lbf/n;

    invoke-direct {v0}, Lbf/n;-><init>()V

    sput-object v0, Lbf/m;->u:Lcom/google/common/base/x;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 221
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 222
    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 226
    invoke-direct/range {p0 .. p5}, Lbf/i;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 152
    iput v0, p0, Lbf/m;->v:I

    .line 158
    iput-boolean v0, p0, Lbf/m;->w:Z

    .line 182
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    .line 199
    iput-object v1, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    .line 204
    iput-object v1, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    .line 227
    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    iput-object v0, p0, Lbf/m;->y:LaB/s;

    .line 228
    iget-object v0, p0, Lbf/m;->y:LaB/s;

    invoke-static {v0}, Lbf/m;->a(LaB/s;)Lcom/google/googlenav/ui/br;

    move-result-object v0

    iput-object v0, p0, Lbf/m;->x:Lcom/google/googlenav/ui/br;

    .line 229
    new-instance v0, Lbf/be;

    invoke-direct {v0, p0}, Lbf/be;-><init>(Lbf/m;)V

    iput-object v0, p0, Lbf/m;->A:Lbf/be;

    .line 230
    return-void
.end method

.method static a(Lax/y;Lr/n;)Lax/y;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1479
    if-nez p1, :cond_3

    .line 1491
    :cond_2
    :goto_2
    return-object p0

    .line 1483
    :cond_3
    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v0

    invoke-virtual {v0}, Lo/D;->b()I

    move-result v0

    const/high16 v1, -0x8000

    if-ne v0, v1, :cond_2

    .line 1485
    invoke-virtual {p0}, Lax/y;->l()Lo/D;

    move-result-object v0

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p1, v0}, Lr/n;->c(Lo/r;)Lo/z;

    move-result-object v0

    .line 1486
    if-eqz v0, :cond_2

    .line 1487
    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    invoke-static {p0, v0}, Lax/y;->a(Lax/y;Lo/D;)Lax/y;

    move-result-object p0

    goto :goto_2
.end method

.method static a(Lcom/google/googlenav/ai;ZZ)Lax/y;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1504
    if-eqz p1, :cond_f

    .line 1505
    invoke-static {p0}, Lax/y;->c(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    .line 1512
    :goto_6
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v1

    invoke-static {v0, v1}, Lbf/m;->a(Lax/y;Lr/n;)Lax/y;

    move-result-object v0

    return-object v0

    .line 1506
    :cond_f
    if-eqz p2, :cond_16

    .line 1507
    invoke-static {p0}, Lax/y;->a(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    goto :goto_6

    .line 1509
    :cond_16
    invoke-static {p0}, Lax/y;->b(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    goto :goto_6
.end method

.method private static a(LaB/s;)Lcom/google/googlenav/ui/br;
    .registers 3
    .parameter

    .prologue
    .line 233
    new-instance v0, Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;
    .registers 4
    .parameter

    .prologue
    .line 237
    if-nez p0, :cond_4

    .line 238
    const/4 v0, 0x0

    .line 240
    :goto_3
    return-object v0

    :cond_4
    new-instance v0, Lcom/google/googlenav/ui/bs;

    invoke-virtual {p0}, Lcom/google/googlenav/ap;->a()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    goto :goto_3
.end method

.method private a()V
    .registers 2

    .prologue
    .line 364
    invoke-direct {p0}, Lbf/m;->bH()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 365
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->H()V

    .line 367
    :cond_d
    return-void
.end method

.method static synthetic a(Lbf/m;Lcom/google/googlenav/ai;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lbf/m;->n(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method private a(ZLjava/lang/String;ILjava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1106
    if-eqz p1, :cond_43

    const-string v0, "e"

    .line 1108
    :goto_4
    const/16 v2, 0x54

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    if-nez p4, :cond_46

    const/4 v1, 0x0

    :goto_39
    aput-object v1, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1115
    return-void

    .line 1106
    :cond_43
    const-string v0, "c"

    goto :goto_4

    .line 1108
    :cond_46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_39
.end method

.method private a(Lcom/google/googlenav/ai;IILjava/lang/Object;)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 982
    if-nez p1, :cond_18

    const/16 v1, 0x11

    if-eq p2, v1, :cond_18

    invoke-virtual {p0}, Lbf/m;->ae()Z

    move-result v1

    if-eqz v1, :cond_f

    if-eq p2, v0, :cond_18

    :cond_f
    const/16 v1, 0x578

    if-ne p2, v1, :cond_19

    const/4 v1, -0x1

    if-ne p3, v1, :cond_18

    if-eqz p4, :cond_19

    :cond_18
    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method static synthetic b(Lbf/m;Lcom/google/googlenav/ai;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lbf/m;->m(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method private static b(Lcom/google/googlenav/ai;I)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    .line 1640
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    .line 1641
    invoke-static {p0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    .line 1642
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    cmp-long v6, v0, v6

    if-eqz v6, :cond_69

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2a
    aput-object v0, v4, v5

    if-eqz v2, :cond_6c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "u="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_41
    aput-object v0, v4, v8

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is_ad="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1650
    return-void

    .line 1642
    :cond_69
    const-string v0, ""

    goto :goto_2a

    :cond_6c
    const-string v0, ""

    goto :goto_41
.end method

.method private b()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 1214
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-nez v1, :cond_11

    invoke-virtual {p0}, Lbf/m;->ae()Z

    move-result v1

    if-nez v1, :cond_17

    :cond_11
    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1220
    :cond_17
    :goto_17
    return v0

    .line 1218
    :cond_18
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    .line 1219
    invoke-virtual {v1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    .line 1220
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    const/4 v0, 0x1

    goto :goto_17
.end method

.method private bG()Z
    .registers 2

    .prologue
    .line 1943
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lbf/m;->bA()Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private bH()Z
    .registers 2

    .prologue
    .line 2273
    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private bI()V
    .registers 3

    .prologue
    .line 2353
    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    if-eqz v0, :cond_1c

    .line 2359
    invoke-virtual {p0}, Lbf/m;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    if-nez v0, :cond_14

    .line 2360
    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/aq;->a(Z)V

    .line 2363
    :cond_14
    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aq;->dismiss()V

    .line 2364
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    .line 2366
    :cond_1c
    return-void
.end method

.method private c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1308
    invoke-virtual {p0}, Lbf/m;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_a

    const-string v0, "f"

    :goto_9
    return-object v0

    :cond_a
    const-string v0, "s"

    goto :goto_9
.end method

.method private c(Lcom/google/googlenav/ui/wizard/A;)V
    .registers 16
    .parameter

    .prologue
    const/4 v13, 0x7

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, -0x1

    .line 1394
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    .line 1397
    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    .line 1398
    invoke-static {v1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    .line 1399
    const/4 v5, 0x4

    const-string v6, "sd"

    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const-wide/16 v8, -0x1

    cmp-long v0, v2, v8

    if-eqz v0, :cond_a6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cid="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2f
    aput-object v0, v7, v12

    if-eqz v4, :cond_a9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_46
    aput-object v0, v7, v11

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1408
    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    if-eqz v0, :cond_68

    .line 1409
    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lbb/a;->a(Lcom/google/googlenav/ai;J)V

    .line 1415
    :cond_68
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lcom/google/googlenav/n;

    if-eqz v0, :cond_bb

    .line 1416
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/n;

    invoke-virtual {v0}, Lcom/google/googlenav/n;->a()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aZ;

    .line 1420
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v2

    if-ne v2, v10, :cond_94

    .line 1421
    new-array v0, v11, [Lcom/google/googlenav/ai;

    aput-object v1, v0, v12

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v2

    invoke-static {v0, v2, v10, v10}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1426
    :cond_94
    if-eqz p1, :cond_ac

    .line 1427
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1447
    :cond_9f
    :goto_9f
    invoke-virtual {p0, v11, v1}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    .line 1451
    invoke-virtual {p0, v13}, Lbf/m;->f(I)V

    .line 1452
    return-void

    .line 1399
    :cond_a6
    const-string v0, ""

    goto :goto_2f

    :cond_a9
    const-string v0, ""

    goto :goto_46

    .line 1429
    :cond_ac
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v13, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_9f

    .line 1432
    :cond_bb
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lcom/google/googlenav/bz;

    if-eqz v0, :cond_d6

    .line 1433
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x1a

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_9f

    .line 1436
    :cond_d6
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    instance-of v0, v0, Lcom/google/googlenav/T;

    if-eqz v0, :cond_9f

    .line 1440
    const/16 v0, 0x12

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ai;->a(B)V

    .line 1441
    new-array v0, v11, [Lcom/google/googlenav/ai;

    aput-object v1, v0, v12

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v2

    invoke-static {v0, v2, v10, v10}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 1444
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v13, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_9f
.end method

.method public static c(Lcom/google/googlenav/ai;)Z
    .registers 5
    .parameter

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aY()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 1458
    iget-object v0, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v1

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->b()I

    move-result v2

    iget-object v3, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/google/googlenav/ai;)Z
    .registers 5
    .parameter

    .prologue
    .line 353
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private f()V
    .registers 5

    .prologue
    .line 1567
    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    if-nez v0, :cond_5

    .line 1580
    :goto_4
    return-void

    .line 1571
    :cond_5
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    sget-object v2, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/android/au;->h()Lcom/google/googlenav/as;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/as;->f()Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_28

    .line 1574
    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->dismiss()V

    .line 1575
    const/4 v0, 0x0

    sput-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    goto :goto_4

    .line 1579
    :cond_28
    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->show()V

    goto :goto_4
.end method

.method public static f(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bM()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->x()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0x90

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method static i(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    .line 1716
    invoke-static {p0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    .line 1717
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->cg()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_77

    const-string v0, "0"

    .line 1720
    :goto_10
    if-eqz v1, :cond_7a

    .line 1722
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&gmmsmh=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1723
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "is_ad="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cb="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1729
    :goto_76
    return-object v0

    .line 1717
    :cond_77
    const-string v0, "1"

    goto :goto_10

    .line 1729
    :cond_7a
    const-string v0, ""

    goto :goto_76
.end method

.method private j(I)V
    .registers 12
    .parameter

    .prologue
    .line 2051
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 2052
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    .line 2053
    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v3

    .line 2055
    const/16 v4, 0x54

    const-string v5, "p"

    const/4 v0, 0x4

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "f="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lbf/m;->v:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "t="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v7, 0x2

    const-wide/16 v8, -0x1

    cmp-long v0, v1, v8

    if-eqz v0, :cond_7d

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cid="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5b
    aput-object v0, v6, v7

    const/4 v1, 0x3

    if-eqz v3, :cond_80

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_73
    aput-object v0, v6, v1

    invoke-static {v6}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2064
    return-void

    .line 2055
    :cond_7d
    const-string v0, ""

    goto :goto_5b

    :cond_80
    const-string v0, ""

    goto :goto_73
.end method

.method private k(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2178
    if-gez p1, :cond_4

    move p1, v0

    .line 2179
    :cond_4
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->A()[Lcom/google/googlenav/ai;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    .line 2182
    return-void
.end method

.method private l(Lcom/google/googlenav/ai;)V
    .registers 5
    .parameter

    .prologue
    .line 1622
    if-eqz p1, :cond_c

    .line 1623
    const/4 v0, 0x5

    const-string v1, "0"

    invoke-static {p1}, Lbf/m;->i(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1627
    :cond_c
    return-void
.end method

.method private m(Lcom/google/googlenav/ai;)V
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1658
    if-nez p1, :cond_4

    .line 1707
    :cond_3
    :goto_3
    return-void

    .line 1664
    :cond_4
    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1667
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 1668
    if-eqz v4, :cond_3

    .line 1671
    const/16 v0, 0x90

    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1672
    if-eqz v5, :cond_3

    .line 1675
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->an()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v6

    .line 1677
    invoke-interface {v6, v5}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 1678
    if-nez v0, :cond_78

    const/4 v2, 0x1

    move v3, v2

    .line 1679
    :goto_2c
    if-eqz v3, :cond_4a

    .line 1680
    new-instance v0, LaR/D;

    invoke-direct {v0}, LaR/D;-><init>()V

    .line 1681
    invoke-virtual {v0, v5}, LaR/D;->a(Ljava/lang/String;)V

    .line 1682
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LaR/D;->b(Ljava/lang/String;)V

    .line 1683
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->x()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1684
    if-eqz v2, :cond_3

    .line 1687
    invoke-static {v2}, LaN/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v2

    invoke-virtual {v0, v2}, LaR/D;->a(LaN/B;)V

    .line 1689
    :cond_4a
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, LaR/D;->a(J)V

    .line 1690
    invoke-virtual {v0}, LaR/D;->f()I

    move-result v2

    .line 1691
    const/4 v5, -0x1

    if-ne v2, v5, :cond_7a

    .line 1694
    :goto_60
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, LaR/D;->a(I)V

    .line 1695
    invoke-interface {v6, v0}, LaR/u;->a(LaR/t;)Z

    .line 1696
    if-eqz v3, :cond_3

    .line 1697
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lbf/s;

    invoke-direct {v1, p0, v6, v4}, Lbf/s;-><init>(Lbf/m;LaR/u;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->run()V

    goto :goto_3

    :cond_78
    move v3, v1

    .line 1678
    goto :goto_2c

    :cond_7a
    move v1, v2

    goto :goto_60
.end method

.method private n(Lcom/google/googlenav/ai;)V
    .registers 5
    .parameter

    .prologue
    .line 2341
    if-eqz p1, :cond_4

    .line 2342
    iput-object p1, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    .line 2345
    :cond_4
    iget-object v0, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_18

    .line 2346
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aq;

    iget-object v1, p0, Lbf/m;->E:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/googlenav/ui/view/dialog/aq;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    .line 2348
    iget-object v0, p0, Lbf/m;->D:Lcom/google/googlenav/ui/view/dialog/aq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aq;->show()V

    .line 2350
    :cond_18
    return-void
.end method


# virtual methods
.method protected D()V
    .registers 5

    .prologue
    .line 1769
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1770
    if-nez v0, :cond_b

    .line 1777
    :cond_a
    :goto_a
    return-void

    .line 1773
    :cond_b
    iget-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v1, v1, LaB/p;

    if-eqz v1, :cond_a

    .line 1774
    iget-object v1, p0, Lbf/m;->y:LaB/s;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ai;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    sget-object v3, Lbf/m;->u:Lcom/google/common/base/x;

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    goto :goto_a
.end method

.method protected E()V
    .registers 5

    .prologue
    .line 1805
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1806
    if-nez v0, :cond_b

    .line 1826
    :cond_a
    :goto_a
    return-void

    .line 1810
    :cond_b
    iget-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v1, v1, LaB/p;

    if-eqz v1, :cond_a

    .line 1811
    iget-object v1, p0, Lbf/m;->y:LaB/s;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ai;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lbf/t;

    invoke-direct {v3, p0}, Lbf/t;-><init>(Lbf/m;)V

    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->b(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    goto :goto_a
.end method

.method public J()V
    .registers 2

    .prologue
    .line 2186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/m;->w:Z

    .line 2187
    invoke-super {p0}, Lbf/i;->J()V

    .line 2188
    return-void
.end method

.method protected final a(ILcom/google/googlenav/ai;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1553
    if-eqz p2, :cond_f

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1554
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lai/a;->a(Ljava/lang/String;I)Z

    .line 1556
    :cond_f
    return-void
.end method

.method public a(LaN/B;Lcom/google/googlenav/ui/wizard/dF;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1273
    invoke-virtual {p0}, Lbf/m;->aQ()V

    .line 1274
    iget-object v0, p0, Lbf/m;->d:LaN/u;

    const/16 v1, 0x13

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LaN/u;->d(LaN/B;LaN/Y;)V

    .line 1275
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 1276
    return-void
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1893
    invoke-super {p0, p1, p2, p3}, Lbf/i;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    .line 1895
    check-cast p2, Lcom/google/googlenav/ai;

    .line 1896
    if-eqz p2, :cond_29

    .line 1899
    invoke-static {p3}, Lcom/google/googlenav/Q;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1900
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 1903
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Z)Lcom/google/googlenav/R;

    .line 1904
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 1927
    :cond_1e
    :goto_1e
    const/16 v0, 0xa

    if-ne p3, v0, :cond_29

    .line 1928
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    .line 1931
    :cond_29
    return-void

    .line 1906
    :cond_2a
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_3b

    move-object v0, p2

    .line 1909
    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    goto :goto_1e

    .line 1911
    :cond_3b
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ai()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 1914
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->aS()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    goto :goto_1e

    .line 1917
    :cond_49
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    .line 1918
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1e

    .line 1921
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->b(Ljava/lang/String;)Lcom/google/googlenav/R;

    goto :goto_1e
.end method

.method public a(Lcom/google/googlenav/aZ;)V
    .registers 2
    .parameter

    .prologue
    .line 1041
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1037
    return-void
.end method

.method a(Lcom/google/googlenav/ai;I)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 1321
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    .line 1322
    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    .line 1323
    const/16 v3, 0x45

    const-string v5, "n"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-wide/16 v8, -0x1

    cmp-long v8, v0, v8

    if-eqz v8, :cond_a5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2b
    aput-object v0, v6, v7

    if-eqz v2, :cond_a8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "u="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_42
    aput-object v0, v6, v10

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "entry="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lbf/m;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v6}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v5, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1336
    invoke-virtual {p0, v10, p1}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    .line 1340
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    .line 1343
    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    if-eqz v0, :cond_86

    .line 1344
    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lbb/a;->a(Lcom/google/googlenav/ai;J)V

    .line 1347
    :cond_86
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->X()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lax/y;->a(Ljava/lang/String;LaN/B;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v0

    .line 1349
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-direct {p0}, Lbf/m;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v4, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    .line 1350
    return-void

    .line 1323
    :cond_a5
    const-string v0, ""

    goto :goto_2b

    :cond_a8
    const-string v0, ""

    goto :goto_42
.end method

.method public a(Lcom/google/googlenav/ai;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 955
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    .line 957
    if-eqz v0, :cond_11

    .line 958
    invoke-virtual {v0, p2}, Lbf/aQ;->d(Z)V

    .line 964
    :cond_11
    const/4 v0, 0x0

    .line 965
    new-instance v1, Lbf/r;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    iget-object v3, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3, v0}, Lbf/r;-><init>(Lbf/m;Las/c;Lcom/google/googlenav/android/aa;Z)V

    .line 973
    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Lbm/j;->a(J)V

    .line 974
    invoke-virtual {v1}, Lbm/j;->g()V

    .line 975
    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/A;)V
    .registers 7
    .parameter

    .prologue
    .line 1368
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    if-nez v0, :cond_7

    .line 1384
    :goto_6
    return-void

    .line 1373
    :cond_7
    invoke-direct {p0, p1}, Lbf/m;->c(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1374
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1376
    invoke-virtual {p0}, Lbf/m;->bv()Z

    move-result v1

    invoke-virtual {p0}, Lbf/m;->bw()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lbf/m;->a(Lcom/google/googlenav/ai;ZZ)Lax/y;

    move-result-object v1

    .line 1379
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const/4 v3, 0x0

    invoke-direct {p0}, Lbf/m;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1383
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbl/o;->c(Lbf/m;)V

    goto :goto_6
.end method

.method public a(Ljava/lang/Object;)V
    .registers 5
    .parameter

    .prologue
    .line 1059
    invoke-virtual {p0}, Lbf/m;->bo()V

    .line 1060
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_5f

    .line 1062
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 1063
    const/16 v0, 0x11

    .line 1068
    :goto_19
    invoke-virtual {p0, v0, p1}, Lbf/m;->b(ILjava/lang/Object;)V

    .line 1081
    :goto_1c
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->R()V

    .line 1082
    iget-object v0, p0, Lbf/m;->d:LaN/u;

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/m;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->b(LaN/B;)V

    .line 1087
    sget-object v0, LaE/d;->a:LaE/d;

    invoke-virtual {v0}, LaE/d;->e()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 1088
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1089
    if-eqz v0, :cond_53

    .line 1090
    invoke-static {}, Lcom/google/googlenav/intersectionexplorer/d;->c()Lcom/google/googlenav/intersectionexplorer/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/intersectionexplorer/d;->a(LaN/B;Ljava/lang/String;)V

    .line 1094
    :cond_53
    return-void

    .line 1065
    :cond_54
    invoke-virtual {p0}, Lbf/m;->ae()Z

    move-result v0

    if-eqz v0, :cond_5d

    const/16 v0, 0x8

    goto :goto_19

    :cond_5d
    const/4 v0, 0x7

    goto :goto_19

    .line 1073
    :cond_5f
    invoke-virtual {p0}, Lbf/m;->an()Z

    goto :goto_1c
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/aB;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1261
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-static {p1}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, p2, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Lcom/google/googlenav/aB;Z)V

    .line 1263
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 1005
    instance-of v0, p0, Lbf/by;

    if-eqz v0, :cond_7e

    .line 1006
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1a

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 1012
    :cond_18
    :goto_18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1013
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1014
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1015
    new-instance v3, Lcom/google/googlenav/bd;

    const-string v4, ""

    invoke-direct {v3, v6, v4, p2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1016
    new-instance v3, Lcom/google/googlenav/bc;

    const/16 v4, 0x12f

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object p1, v5, v6

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v7, v4, v2}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1021
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v2, "*"

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v2, 0x5f6

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v2, "20"

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 1030
    return-void

    .line 1008
    :cond_7e
    instance-of v0, p0, Lbf/bk;

    if-nez v0, :cond_18

    .line 1009
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x11

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_18
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2292
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x54

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 420
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v4

    .line 421
    invoke-direct {p0, v4, p1, p2, p3}, Lbf/m;->a(Lcom/google/googlenav/ai;IILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_78

    .line 425
    invoke-super {p0, p1, p2, p3}, Lbf/i;->a(IILjava/lang/Object;)Z

    move-result v6

    .line 426
    if-nez v6, :cond_77

    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "action:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actionIndex:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", layerType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/m;->av()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 434
    iget-object v1, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    if-eqz v1, :cond_72

    .line 435
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", index:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 437
    :cond_72
    const-string v1, "BPL-onAction"

    invoke-static {v1, v0}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    :cond_77
    :goto_77
    return v6

    .line 443
    :cond_78
    invoke-static {v4}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    .line 445
    if-eqz v1, :cond_5ba

    .line 446
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 450
    :goto_92
    sparse-switch p1, :sswitch_data_5be

    .line 951
    :cond_95
    :goto_95
    invoke-super {p0, p1, p2, p3}, Lbf/i;->a(IILjava/lang/Object;)Z

    move-result v6

    goto :goto_77

    .line 454
    :sswitch_9a
    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    if-eqz v0, :cond_b3

    .line 455
    invoke-static {}, Lbb/a;->a()Lbb/a;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v4, v1, v2}, Lbb/a;->a(Lcom/google/googlenav/ai;J)V

    .line 458
    :cond_b3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_b7

    move v5, v6

    :cond_b7
    invoke-virtual {p0, v4, v5}, Lbf/m;->b(Lcom/google/googlenav/ai;Z)Z

    move-result v0

    if-eqz v0, :cond_95

    .line 462
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbl/o;->b(Lbf/m;)V

    goto :goto_77

    .line 465
    :sswitch_c5
    const-string v0, "m"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    .line 466
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_db

    .line 467
    invoke-virtual {p0}, Lbf/m;->an()Z

    .line 468
    invoke-virtual {p0, v6}, Lbf/m;->b(Z)V

    goto :goto_77

    .line 470
    :cond_db
    invoke-virtual {p0, v8}, Lbf/m;->a(Ljava/lang/Object;)V

    goto :goto_77

    .line 474
    :sswitch_df
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490
    invoke-virtual {p0}, Lbf/m;->n()V

    .line 491
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "21"

    invoke-virtual {v1, v8, v2, v0, v6}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_77

    .line 496
    :sswitch_f2
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    .line 499
    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "ct"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 509
    :sswitch_11f
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;)V

    .line 510
    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "rp"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 517
    :sswitch_137
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 518
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v0

    .line 519
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_199

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    move-object v2, v0

    .line 520
    :goto_154
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bw()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 524
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "a=s"

    aput-object v1, v0, v5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "i="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    aput-object v7, v0, v9

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 527
    const/16 v1, 0x10

    invoke-virtual {p0}, Lbf/m;->av()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 529
    const-string v0, "ac"

    const-string v1, "rp"

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    :cond_199
    move-object v2, v8

    .line 519
    goto :goto_154

    .line 534
    :sswitch_19b
    invoke-direct {p0, p2}, Lbf/m;->j(I)V

    .line 535
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {v0, p3, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto/16 :goto_77

    .line 538
    :sswitch_1a7
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aX()Z

    move-result v0

    if-eqz v0, :cond_1bc

    .line 539
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aV()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto/16 :goto_77

    .line 543
    :cond_1bc
    const-string v0, "kml"

    invoke-static {v9, v0}, Lbm/m;->a(ILjava/lang/String;)V

    .line 547
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    goto/16 :goto_77

    .line 554
    :sswitch_1de
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_1e9

    .line 555
    const-string v0, "Street View"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    .line 561
    :cond_1e9
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    .line 563
    if-eqz p3, :cond_21c

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v1, v0

    .line 564
    :goto_1f5
    const/4 v0, 0x3

    if-ne v1, v0, :cond_1f9

    move v5, v6

    .line 565
    :cond_1f9
    if-eqz v5, :cond_21e

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bt()Ljava/lang/String;

    move-result-object v0

    .line 566
    :goto_1ff
    if-nez v0, :cond_223

    .line 567
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->b(LaN/B;)V

    .line 578
    :goto_20a
    invoke-virtual {p0, v1}, Lbf/m;->h(I)V

    .line 579
    invoke-virtual {p0, v9, v4}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    .line 583
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    .line 585
    const-string v0, "st"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    goto/16 :goto_77

    :cond_21c
    move v1, v6

    .line 563
    goto :goto_1f5

    .line 565
    :cond_21e
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v0

    goto :goto_1ff

    .line 569
    :cond_223
    if-eqz v5, :cond_235

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bu()Z

    move-result v2

    if-eqz v2, :cond_235

    .line 571
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bv()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;I)V

    goto :goto_20a

    .line 573
    :cond_235
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;LaN/B;)V

    goto :goto_20a

    .line 591
    :sswitch_23f
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    .line 592
    const-string v0, "d"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    .line 593
    invoke-virtual {p0}, Lbf/m;->n()V

    .line 596
    check-cast p3, Lcom/google/googlenav/ui/wizard/A;

    invoke-virtual {p0, p3}, Lbf/m;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto/16 :goto_77

    .line 602
    :sswitch_251
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    .line 603
    invoke-virtual {p0, v4, v5}, Lbf/m;->a(Lcom/google/googlenav/ai;I)V

    goto/16 :goto_77

    .line 609
    :sswitch_259
    invoke-virtual {p0, v5}, Lbf/m;->i(I)V

    .line 610
    invoke-virtual {p0, v4, v9}, Lbf/m;->a(Lcom/google/googlenav/ai;I)V

    goto/16 :goto_77

    .line 613
    :sswitch_261
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 616
    :sswitch_272
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ai;)V

    .line 617
    const/16 v0, 0x4f

    const-string v1, "o"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "f=p"

    aput-object v3, v2, v5

    aput-object v7, v2, v6

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 623
    const-string v0, "sr"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    goto/16 :goto_77

    .line 627
    :sswitch_293
    invoke-virtual {p0, p2}, Lbf/m;->i(I)V

    goto/16 :goto_77

    .line 630
    :sswitch_298
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bD()Ljava/lang/String;

    move-result-object v0

    .line 631
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2ab

    .line 632
    const-string v0, "Make a reservation"

    const-string v1, "missing url"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 635
    :cond_2ab
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Lbf/m;->f(I)V

    .line 637
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto/16 :goto_77

    .line 641
    :sswitch_2b7
    if-eqz p3, :cond_31c

    .line 642
    check-cast p3, Lcom/google/googlenav/ai;

    .line 650
    :cond_2bb
    :goto_2bb
    invoke-virtual {p3}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 651
    const/16 v1, 0x90

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 652
    invoke-static {p3}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_77

    .line 655
    invoke-virtual {p3}, Lcom/google/googlenav/ai;->x()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 656
    if-eqz v4, :cond_77

    .line 661
    new-instance v0, LaR/D;

    invoke-virtual {p3}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    move-object v3, v8

    invoke-direct/range {v0 .. v5}, LaR/D;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 662
    invoke-virtual {p3}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LaR/D;->a(Ljava/util/List;)V

    .line 663
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, LaR/H;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaR/H;

    move-result-object v2

    iget-object v3, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v3

    invoke-virtual {p0}, Lbf/m;->bn()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v2, v3, v4}, LaR/n;->a(LaR/D;LaR/H;Lbf/am;Ljava/lang/String;)LaR/D;

    move-result-object v0

    .line 668
    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_32a

    .line 669
    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    .line 670
    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "sp"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 643
    :cond_31c
    const/4 v0, -0x1

    if-eq p2, v0, :cond_5b7

    .line 644
    invoke-virtual {p0, p2}, Lbf/m;->b(I)V

    .line 645
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object p3

    .line 646
    if-nez p3, :cond_2bb

    goto/16 :goto_77

    .line 676
    :cond_32a
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    .line 677
    const-string v0, "ac"

    new-array v1, v9, [Ljava/lang/String;

    const-string v2, "up"

    aput-object v2, v1, v5

    aput-object v7, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 685
    :sswitch_342
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_34d

    .line 686
    const-string v0, "Review"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    .line 689
    :cond_34d
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    const-string v2, "pp"

    invoke-virtual {v0, v4, v1, v5, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    goto/16 :goto_77

    .line 694
    :sswitch_35e
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_369

    .line 695
    const-string v0, "Photo Upload"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    .line 700
    :cond_369
    if-nez p3, :cond_37b

    const-string p3, "mo"

    .line 702
    :goto_36d
    new-instance v0, Lbf/o;

    invoke-direct {v0, p0}, Lbf/o;-><init>(Lbf/m;)V

    .line 713
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1, v4, v8, p3, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    goto/16 :goto_77

    .line 700
    :cond_37b
    check-cast p3, Ljava/lang/String;

    goto :goto_36d

    .line 716
    :sswitch_37e
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lbf/m;->j(I)V

    .line 717
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->D()Lcom/google/googlenav/ui/wizard/fk;

    move-result-object v0

    invoke-virtual {v0, v4, p2, p0}, Lcom/google/googlenav/ui/wizard/fk;->a(Lcom/google/googlenav/ai;ILbf/m;)V

    goto/16 :goto_77

    .line 720
    :sswitch_394
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    .line 722
    if-eqz v0, :cond_77

    .line 723
    invoke-virtual {v0}, Lbf/aQ;->a()Z

    move-result v2

    if-nez v2, :cond_3a9

    move v5, v6

    .line 724
    :cond_3a9
    invoke-virtual {v0, v5}, Lbf/aQ;->a(Z)V

    .line 725
    const-string v0, "u"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    .line 728
    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_77

    .line 732
    :sswitch_3b6
    new-instance v3, Lcom/google/googlenav/h;

    invoke-direct {v3, v4}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/ai;)V

    .line 733
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bT()Z

    move-result v0

    if-nez v0, :cond_3c7

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bU()Z

    move-result v0

    if-eqz v0, :cond_3d5

    :cond_3c7
    const-string v4, "cppi"

    .line 736
    :goto_3c9
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    move v7, v6

    invoke-virtual/range {v1 .. v8}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V

    goto/16 :goto_77

    .line 733
    :cond_3d5
    const-string v4, "cppn"

    goto :goto_3c9

    .line 746
    :sswitch_3d8
    new-instance v0, Lcom/google/googlenav/ui/wizard/ep;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/ep;-><init>()V

    .line 747
    iget-object v1, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->m()Lbf/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->d:Lbf/a;

    .line 748
    new-instance v1, Lcom/google/googlenav/h;

    invoke-direct {v1, v4}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/ep;->a:Lcom/google/googlenav/h;

    .line 749
    iput-boolean v6, v0, Lcom/google/googlenav/ui/wizard/ep;->c:Z

    .line 750
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    goto/16 :goto_77

    .line 754
    :sswitch_3f7
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const/16 v1, 0x13c

    invoke-virtual {v0, v1}, Lbf/am;->d(I)V

    .line 755
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/s;->b(Z)V

    goto/16 :goto_77

    .line 758
    :sswitch_409
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 759
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/bZ;)V

    .line 760
    const-string v0, "c"

    invoke-virtual {p0, v0}, Lbf/m;->c(Ljava/lang/String;)V

    goto/16 :goto_77

    .line 763
    :sswitch_41f
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    .line 765
    if-eqz v0, :cond_77

    .line 766
    invoke-virtual {v0}, Lbf/aQ;->b()Z

    move-result v2

    if-nez v2, :cond_434

    move v5, v6

    .line 767
    :cond_434
    invoke-virtual {v0, v5}, Lbf/aQ;->b(Z)V

    .line 768
    const-string v0, "d"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    .line 770
    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_77

    .line 774
    :sswitch_441
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    .line 776
    if-eqz v0, :cond_77

    .line 777
    invoke-virtual {v0}, Lbf/aQ;->c()Z

    move-result v2

    if-nez v2, :cond_456

    move v5, v6

    .line 778
    :cond_456
    invoke-virtual {v0, v5}, Lbf/aQ;->c(Z)V

    .line 779
    const-string v0, "h"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    .line 781
    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_77

    .line 785
    :sswitch_463
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    check-cast p3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v1, p3, p0, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/ui/s;)V

    goto/16 :goto_77

    .line 788
    :sswitch_474
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aQ;

    .line 790
    if-eqz v0, :cond_77

    .line 791
    invoke-virtual {v0}, Lbf/aQ;->d()Z

    move-result v2

    if-nez v2, :cond_489

    move v5, v6

    .line 792
    :cond_489
    invoke-virtual {v0, v5}, Lbf/aQ;->d(Z)V

    .line 793
    const-string v0, "g"

    invoke-direct {p0, v5, v0, p2, v1}, Lbf/m;->a(ZLjava/lang/String;ILjava/lang/String;)V

    .line 796
    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_77

    .line 800
    :sswitch_496
    check-cast p3, Ljava/lang/String;

    .line 801
    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4a3

    .line 804
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p3, v5, p2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;ZI)V

    .line 806
    :cond_4a3
    const-string v0, "ac"

    const-string v1, "mi"

    invoke-static {v10, v0, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 812
    :sswitch_4ac
    check-cast p3, [Lcom/google/googlenav/aw;

    check-cast p3, [Lcom/google/googlenav/aw;

    .line 813
    if-eqz p3, :cond_77

    .line 814
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p3, p2, v5}, Lcom/google/googlenav/ui/wizard/jv;->a([Lcom/google/googlenav/aw;IZ)V

    goto/16 :goto_77

    .line 822
    :sswitch_4bb
    check-cast p3, [Lcom/google/googlenav/aw;

    check-cast p3, [Lcom/google/googlenav/aw;

    .line 823
    if-eqz p3, :cond_77

    .line 824
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p3, p2, v6}, Lcom/google/googlenav/ui/wizard/jv;->a([Lcom/google/googlenav/aw;IZ)V

    goto/16 :goto_77

    .line 829
    :sswitch_4ca
    check-cast p3, Ljava/lang/String;

    .line 830
    if-eqz p3, :cond_77

    .line 831
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Lcom/google/googlenav/ui/wizard/jv;->b(Ljava/lang/String;I)V

    goto/16 :goto_77

    .line 838
    :sswitch_4d7
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Lbf/m;->a(Lcom/google/googlenav/E;I)V

    .line 840
    new-instance v0, Lcom/google/googlenav/ui/view/android/au;

    new-instance v1, Lbf/p;

    invoke-direct {v1, p0}, Lbf/p;-><init>(Lbf/m;)V

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/android/au;-><init>(Lcom/google/googlenav/ui/e;)V

    sput-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    .line 860
    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    check-cast p3, Lcom/google/googlenav/as;

    invoke-virtual {p0}, Lbf/m;->be()Lcom/google/googlenav/ui/br;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/as;LaB/s;)V

    goto/16 :goto_77

    .line 864
    :sswitch_4fd
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/s;->e(Z)V

    goto/16 :goto_77

    .line 869
    :sswitch_504
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    check-cast p3, Lcom/google/googlenav/ai;

    invoke-virtual {v0, p3, v9, v5}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;BZ)V

    goto/16 :goto_77

    .line 872
    :sswitch_50d
    invoke-virtual {p0}, Lbf/m;->aX()V

    .line 873
    if-eqz p3, :cond_51f

    .line 874
    check-cast p3, [Ljava/lang/String;

    check-cast p3, [Ljava/lang/String;

    .line 875
    aget-object v0, p3, v5

    aget-object v1, p3, v6

    invoke-virtual {p0, v0, v1}, Lbf/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 877
    :cond_51f
    invoke-virtual {p0, v8, v8}, Lbf/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 883
    :sswitch_524
    check-cast p3, Ljava/lang/String;

    .line 884
    invoke-static {p3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_540

    .line 885
    const-string v0, "LaunchUrl"

    const-string v1, "missing url"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    :goto_533
    const/16 v0, 0x19

    if-ne p1, v0, :cond_77

    .line 899
    invoke-virtual {v4}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbl/o;->a(Lbf/m;)V

    goto/16 :goto_77

    .line 888
    :cond_540
    invoke-virtual {p0, p3}, Lbf/m;->a(Ljava/lang/String;)V

    .line 892
    invoke-direct {p0}, Lbf/m;->a()V

    .line 895
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p3, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Lcom/google/googlenav/ai;)V

    goto :goto_533

    .line 903
    :sswitch_54c
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 904
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 908
    invoke-virtual {p0, v6, v4}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    goto/16 :goto_77

    .line 912
    :sswitch_571
    invoke-virtual {p0}, Lbf/m;->bp()V

    goto/16 :goto_95

    .line 915
    :sswitch_576
    iput-boolean v6, p0, Lbf/m;->w:Z

    .line 916
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lbf/m;->i(I)V

    goto/16 :goto_77

    .line 919
    :sswitch_57e
    invoke-direct {p0, p2}, Lbf/m;->k(I)V

    goto/16 :goto_77

    .line 922
    :sswitch_583
    invoke-virtual {p0, v8}, Lbf/m;->a(Ljava/lang/Object;)V

    .line 923
    check-cast p3, Lcom/google/googlenav/bZ;

    .line 924
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p3}, Lcom/google/googlenav/bZ;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->f(Ljava/lang/String;)V

    goto/16 :goto_77

    .line 928
    :sswitch_593
    check-cast p3, Ljava/lang/String;

    .line 929
    new-instance v0, Lbf/q;

    invoke-direct {v0, p0}, Lbf/q;-><init>(Lbf/m;)V

    .line 945
    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p3}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 946
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 948
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    goto/16 :goto_77

    :cond_5b7
    move-object p3, v4

    goto/16 :goto_2bb

    :cond_5ba
    move-object v7, v8

    goto/16 :goto_92

    .line 450
    nop

    :sswitch_data_5be
    .sparse-switch
        0x1 -> :sswitch_c5
        0x3 -> :sswitch_9a
        0x4 -> :sswitch_272
        0x5 -> :sswitch_524
        0x6 -> :sswitch_261
        0xf -> :sswitch_293
        0x10 -> :sswitch_9a
        0x11 -> :sswitch_496
        0x12 -> :sswitch_4fd
        0x18 -> :sswitch_571
        0x19 -> :sswitch_524
        0xc9 -> :sswitch_409
        0x14e -> :sswitch_3f7
        0x258 -> :sswitch_1de
        0x25a -> :sswitch_df
        0x25b -> :sswitch_23f
        0x25c -> :sswitch_251
        0x25d -> :sswitch_259
        0x262 -> :sswitch_54c
        0x2bf -> :sswitch_f2
        0x2c0 -> :sswitch_11f
        0x2c1 -> :sswitch_1a7
        0x2c3 -> :sswitch_298
        0x2ce -> :sswitch_19b
        0x3fa -> :sswitch_504
        0x4b0 -> :sswitch_576
        0x4b1 -> :sswitch_57e
        0x578 -> :sswitch_2b7
        0x5dc -> :sswitch_137
        0x6a4 -> :sswitch_342
        0x713 -> :sswitch_50d
        0x76c -> :sswitch_394
        0x76d -> :sswitch_37e
        0x834 -> :sswitch_3b6
        0x843 -> :sswitch_3d8
        0x900 -> :sswitch_41f
        0x902 -> :sswitch_441
        0x906 -> :sswitch_463
        0x907 -> :sswitch_4ca
        0x908 -> :sswitch_4ac
        0x909 -> :sswitch_35e
        0x90a -> :sswitch_4bb
        0x90b -> :sswitch_4d7
        0x90c -> :sswitch_474
        0x90d -> :sswitch_593
        0xfa5 -> :sswitch_583
    .end sparse-switch
.end method

.method public aU()V
    .registers 2

    .prologue
    .line 1871
    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    if-eqz v0, :cond_c

    .line 1872
    sget-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->dismiss()V

    .line 1873
    const/4 v0, 0x0

    sput-object v0, Lbf/m;->z:Lcom/google/googlenav/ui/view/android/au;

    .line 1878
    :cond_c
    iget-object v0, p0, Lbf/m;->y:LaB/s;

    if-eqz v0, :cond_15

    .line 1879
    iget-object v0, p0, Lbf/m;->y:LaB/s;

    invoke-virtual {v0}, LaB/s;->a()V

    .line 1885
    :cond_15
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 1887
    invoke-super {p0}, Lbf/i;->aU()V

    .line 1888
    return-void
.end method

.method public aV()V
    .registers 2

    .prologue
    .line 2278
    invoke-super {p0}, Lbf/i;->aV()V

    .line 2284
    invoke-direct {p0}, Lbf/m;->bH()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2285
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->I()V

    .line 2287
    :cond_10
    return-void
.end method

.method public aW()V
    .registers 2

    .prologue
    .line 1863
    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1864
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/m;->b(B)V

    .line 1866
    :cond_12
    invoke-super {p0}, Lbf/i;->aW()V

    .line 1867
    return-void
.end method

.method protected ap()V
    .registers 4

    .prologue
    .line 1748
    iget-object v0, p0, Lbf/m;->A:Lbf/be;

    if-nez v0, :cond_21

    .line 1749
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No DetailsDialog for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1752
    :cond_21
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1753
    if-nez v0, :cond_28

    .line 1765
    :goto_27
    return-void

    .line 1759
    :cond_28
    iget v1, p0, Lbf/m;->v:I

    if-nez v1, :cond_35

    invoke-virtual {p0, v0}, Lbf/m;->h(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 1760
    const/4 v0, 0x5

    iput v0, p0, Lbf/m;->v:I

    .line 1762
    :cond_35
    iget-object v0, p0, Lbf/m;->A:Lbf/be;

    iget v1, p0, Lbf/m;->v:I

    invoke-virtual {v0, v1}, Lbf/be;->a(I)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v0

    iput-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 1764
    invoke-virtual {p0}, Lbf/m;->by()V

    goto :goto_27
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .registers 3
    .parameter

    .prologue
    .line 1048
    invoke-virtual {p0}, Lbf/m;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 1049
    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method b(Lcom/google/googlenav/ai;Z)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1232
    invoke-direct {p0}, Lbf/m;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1233
    const/4 v0, 0x0

    .line 1253
    :goto_7
    return v0

    .line 1237
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    .line 1241
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    .line 1244
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-static {p1, v0}, Lbf/m;->b(Lcom/google/googlenav/ai;I)V

    .line 1246
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aO()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1247
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x20

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/String;)V

    .line 1250
    :cond_2b
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lbf/u;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lbf/u;-><init>(Lbf/m;Lcom/google/googlenav/ai;Lbf/n;)V

    invoke-interface {v0, v1, v2, p2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Lcom/google/googlenav/aB;Z)V

    .line 1253
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public b(Lcom/google/googlenav/ui/wizard/A;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1963
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_60

    .line 2001
    :pswitch_8
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 1965
    :pswitch_a
    invoke-virtual {p0, v1}, Lbf/m;->j(Z)V

    .line 1966
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 1967
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/m;

    .line 1968
    if-eqz v0, :cond_22

    .line 1971
    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lbf/m;->n(Lcom/google/googlenav/ai;)V

    :cond_22
    move v0, v1

    .line 1973
    goto :goto_9

    .line 1976
    :pswitch_24
    invoke-virtual {p0, v1}, Lbf/m;->j(Z)V

    .line 1977
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 1980
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/m;

    .line 1982
    if-eqz v0, :cond_41

    .line 1983
    invoke-virtual {v0}, Lbf/m;->aR()V

    :cond_41
    move v0, v1

    .line 1985
    goto :goto_9

    .line 1988
    :pswitch_43
    invoke-direct {p0}, Lbf/m;->bI()V

    .line 1989
    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v0

    if-nez v0, :cond_5b

    .line 1990
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_59

    .line 1993
    invoke-virtual {p0}, Lbf/m;->A()V

    :cond_59
    :goto_59
    move v0, v1

    .line 1998
    goto :goto_9

    .line 1996
    :cond_5b
    invoke-virtual {p0}, Lbf/m;->aR()V

    goto :goto_59

    .line 1963
    nop

    :pswitch_data_60
    .packed-switch 0x24
        :pswitch_a
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_24
        :pswitch_43
    .end packed-switch
.end method

.method public bA()Z
    .registers 3

    .prologue
    .line 2009
    iget v0, p0, Lbf/m;->v:I

    if-eqz v0, :cond_9

    iget v0, p0, Lbf/m;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_b

    :cond_9
    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public bB()LaB/o;
    .registers 2

    .prologue
    .line 2164
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->k()LaB/o;

    move-result-object v0

    return-object v0
.end method

.method protected bC()I
    .registers 2

    .prologue
    .line 2196
    const/4 v0, 0x0

    return v0
.end method

.method protected bD()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 2204
    invoke-virtual {p0}, Lbf/m;->bC()I

    move-result v0

    .line 2205
    const/4 v1, -0x1

    if-eq v0, v1, :cond_21

    .line 2206
    invoke-virtual {p0, v0}, Lbf/m;->b(I)V

    .line 2207
    const/4 v0, 0x0

    iput v0, p0, Lbf/m;->v:I

    .line 2208
    invoke-virtual {p0}, Lbf/m;->m()V

    .line 2209
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->z()I

    move-result v0

    if-le v0, v2, :cond_21

    .line 2210
    iput-boolean v2, p0, Lbf/m;->w:Z

    .line 2211
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lbf/m;->i(I)V

    .line 2214
    :cond_21
    return-void
.end method

.method bE()Lcom/google/googlenav/ui/wizard/jv;
    .registers 2

    .prologue
    .line 2263
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    return-object v0
.end method

.method protected bF()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 2306
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 2307
    if-nez v0, :cond_8

    .line 2310
    :goto_7
    return v1

    :cond_8
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v2

    if-nez v2, :cond_1a

    instance-of v2, v0, Lcom/google/googlenav/W;

    if-eqz v2, :cond_1d

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->w()Z

    move-result v0

    if-eqz v0, :cond_1d

    :cond_1a
    const/4 v0, 0x1

    :goto_1b
    move v1, v0

    goto :goto_7

    :cond_1d
    move v0, v1

    goto :goto_1b
.end method

.method public bc()Ljava/lang/String;
    .registers 8

    .prologue
    const-wide/16 v5, -0x1

    const-wide v3, 0x412e848000000000L

    .line 2219
    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 2220
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2221
    const-string v0, "Place Page"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    .line 2223
    :cond_18
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/maps/place?cid="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2224
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    .line 2225
    if-eqz v1, :cond_b6

    .line 2226
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    .line 2227
    cmp-long v3, v1, v5

    if-eqz v3, :cond_b6

    .line 2228
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2229
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2255
    :goto_38
    return-object v0

    .line 2232
    :cond_39
    invoke-virtual {p0}, Lbf/m;->ah()Z

    move-result v0

    if-nez v0, :cond_45

    invoke-virtual {p0}, Lbf/m;->ag()Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 2233
    :cond_45
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 2234
    const-string v0, "Map"

    invoke-static {v0}, Laj/a;->c(Ljava/lang/String;)V

    .line 2236
    :cond_50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://maps.google.com/?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2237
    const-string v1, "ll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2238
    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 2239
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2240
    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 2241
    const-string v1, "&z="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2242
    iget-object v1, p0, Lbf/m;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2246
    invoke-virtual {p0}, Lbf/m;->ag()Z

    move-result v1

    if-eqz v1, :cond_b1

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_b1

    .line 2247
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    .line 2248
    cmp-long v3, v1, v5

    if-eqz v3, :cond_b1

    .line 2249
    const-string v3, "&q=cid:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2250
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2253
    :cond_b1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_38

    .line 2255
    :cond_b6
    const/4 v0, 0x0

    goto :goto_38
.end method

.method public be()Lcom/google/googlenav/ui/br;
    .registers 2

    .prologue
    .line 246
    iget-object v0, p0, Lbf/m;->x:Lcom/google/googlenav/ui/br;

    return-object v0
.end method

.method public bf()Lcom/google/googlenav/ui/a;
    .registers 2

    .prologue
    .line 258
    iget-object v0, p0, Lbf/m;->B:Lcom/google/googlenav/ui/a;

    if-nez v0, :cond_b

    .line 259
    new-instance v0, Lcom/google/googlenav/ui/a;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/a;-><init>(Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lbf/m;->B:Lcom/google/googlenav/ui/a;

    .line 261
    :cond_b
    iget-object v0, p0, Lbf/m;->B:Lcom/google/googlenav/ui/a;

    return-object v0
.end method

.method public bg()Z
    .registers 2

    .prologue
    .line 286
    const/4 v0, 0x0

    return v0
.end method

.method public bh()Z
    .registers 2

    .prologue
    .line 295
    const/4 v0, 0x0

    return v0
.end method

.method public bi()Z
    .registers 2

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method

.method public bj()Z
    .registers 2

    .prologue
    .line 311
    const/4 v0, 0x1

    return v0
.end method

.method public bk()Z
    .registers 2

    .prologue
    .line 319
    const/4 v0, 0x1

    return v0
.end method

.method public bl()Z
    .registers 2

    .prologue
    .line 326
    const/4 v0, 0x0

    return v0
.end method

.method public bm()Z
    .registers 2

    .prologue
    .line 388
    const/4 v0, 0x1

    return v0
.end method

.method protected bn()Ljava/lang/String;
    .registers 2

    .prologue
    .line 994
    const-string v0, "p"

    return-object v0
.end method

.method protected bo()V
    .registers 1

    .prologue
    .line 1102
    return-void
.end method

.method public bp()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1125
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lbf/m;->bA()Z

    move-result v0

    if-nez v0, :cond_12

    .line 1161
    :cond_11
    :goto_11
    return-void

    .line 1130
    :cond_12
    iget v0, p0, Lbf/m;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_19

    .line 1131
    iput v2, p0, Lbf/m;->v:I

    .line 1135
    :cond_19
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/m;->f(Z)V

    .line 1137
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_11

    .line 1138
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    .line 1140
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->w()Z

    move-result v1

    if-eqz v1, :cond_48

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ak()Z

    move-result v1

    if-nez v1, :cond_48

    .line 1144
    iget-object v1, p0, Lbf/m;->A:Lbf/be;

    invoke-virtual {v1, v2}, Lbf/be;->a(I)Lcom/google/googlenav/ui/view/android/S;

    move-result-object v1

    iput-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 1145
    iget-object v1, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 1146
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->dismiss()V

    goto :goto_11

    .line 1150
    :cond_48
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->w()Z

    move-result v1

    if-eqz v1, :cond_54

    .line 1151
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->invalidateOptionsMenu()V

    .line 1154
    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/bn;->b(Z)V

    .line 1158
    :cond_54
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    goto :goto_11
.end method

.method public bq()V
    .registers 3

    .prologue
    .line 1169
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_11

    .line 1170
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Lcom/google/googlenav/ai;)V

    .line 1172
    :cond_11
    invoke-virtual {p0}, Lbf/m;->bp()V

    .line 1173
    invoke-virtual {p0}, Lbf/m;->D()V

    .line 1174
    invoke-virtual {p0}, Lbf/m;->E()V

    .line 1175
    return-void
.end method

.method protected br()V
    .registers 2

    .prologue
    .line 1178
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/bn;

    if-eqz v0, :cond_d

    .line 1179
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/bn;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    .line 1181
    :cond_d
    return-void
.end method

.method public bs()Ljava/util/Hashtable;
    .registers 2

    .prologue
    .line 1184
    iget-object v0, p0, Lbf/m;->C:Ljava/util/Hashtable;

    return-object v0
.end method

.method public bt()V
    .registers 3

    .prologue
    .line 1284
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->a(B)V

    .line 1285
    invoke-virtual {p0}, Lbf/m;->aR()V

    .line 1290
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eq v0, p0, :cond_1e

    .line 1291
    iget-object v0, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->f(Lbf/i;)V

    .line 1293
    :cond_1e
    invoke-virtual {p0}, Lbf/m;->A()V

    .line 1294
    return-void
.end method

.method public bu()Lcom/google/googlenav/ai;
    .registers 2

    .prologue
    .line 1517
    invoke-virtual {p0}, Lbf/m;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    return-object v0
.end method

.method protected bv()Z
    .registers 2

    .prologue
    .line 1535
    const/4 v0, 0x0

    return v0
.end method

.method protected bw()Z
    .registers 2

    .prologue
    .line 1542
    const/4 v0, 0x0

    return v0
.end method

.method protected bx()Z
    .registers 2

    .prologue
    .line 1559
    const/4 v0, 0x1

    return v0
.end method

.method protected by()V
    .registers 6

    .prologue
    .line 1784
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1785
    if-nez v0, :cond_b

    .line 1801
    :cond_a
    :goto_a
    return-void

    .line 1788
    :cond_b
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bR()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1792
    sget-object v1, Lcom/google/googlenav/av;->b:Lcom/google/googlenav/av;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/av;)V

    .line 1794
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    .line 1795
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1796
    new-instance v2, Lcom/google/googlenav/f;

    new-instance v3, Lbf/v;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v0, v4}, Lbf/v;-><init>(Lbf/m;Lcom/google/googlenav/ai;Lbf/n;)V

    invoke-direct {v2, v3, v1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 1798
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/f;->a(Z)V

    .line 1799
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    goto :goto_a
.end method

.method protected bz()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1947
    invoke-direct {p0}, Lbf/m;->bG()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1948
    invoke-virtual {p0, v0}, Lbf/m;->i(I)V

    .line 1949
    const/4 v0, 0x1

    .line 1951
    :cond_b
    return v0
.end method

.method public c(Lcom/google/googlenav/ai;Z)Lam/f;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1837
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    if-nez v1, :cond_8

    .line 1855
    :cond_7
    :goto_7
    return-object v0

    .line 1841
    :cond_8
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v1

    .line 1842
    if-eqz v1, :cond_7

    .line 1846
    if-nez p2, :cond_15

    .line 1847
    invoke-virtual {v1}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v0

    goto :goto_7

    .line 1850
    :cond_15
    iget-object v0, p0, Lbf/m;->a:Lcom/google/googlenav/ui/bi;

    const v2, 0x7f02000e

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/bi;->b(I)Lam/f;

    move-result-object v0

    invoke-static {v0}, Lam/j;->d(Lam/f;)Lam/f;

    move-result-object v0

    .line 1852
    invoke-interface {v0}, Lam/f;->c()Lam/e;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/googlenav/e;->a()Lam/f;

    move-result-object v3

    invoke-interface {v0}, Lam/f;->a()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/googlenav/e;->e()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-interface {v0}, Lam/f;->b()I

    move-result v5

    invoke-virtual {v1}, Lcom/google/googlenav/e;->f()I

    move-result v1

    sub-int v1, v5, v1

    div-int/lit8 v1, v1, 0x2

    invoke-interface {v2, v3, v4, v1}, Lam/e;->a(Lam/f;II)V

    goto :goto_7
.end method

.method protected c(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    .line 2376
    sget-object v0, Lbm/l;->d:Lbm/l;

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lbm/l;)Ljava/lang/String;

    move-result-object v0

    .line 2377
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2e

    const/4 v0, 0x0

    :goto_14
    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2381
    invoke-virtual {p0}, Lbf/m;->bF()Z

    move-result v1

    if-eqz v1, :cond_2d

    invoke-virtual {p0}, Lbf/m;->af()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 2382
    const/16 v1, 0x73

    const-string v2, "a"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2385
    :cond_2d
    return-void

    .line 2377
    :cond_2e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_14
.end method

.method public d(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    .line 1199
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_10

    .line 1201
    const/4 v0, 0x3

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    .line 1203
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public d(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 340
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public g(Lcom/google/googlenav/ai;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 397
    invoke-static {p1}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 403
    :goto_7
    return v1

    .line 400
    :cond_8
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v2, 0x90

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 401
    iget-object v2, p0, Lbf/m;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v2

    invoke-interface {v2}, LaR/n;->e()LaR/u;

    move-result-object v2

    invoke-interface {v2, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 403
    if-eqz v0, :cond_2d

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_2d

    const/4 v0, 0x1

    :goto_2b
    move v1, v0

    goto :goto_7

    :cond_2d
    move v0, v1

    goto :goto_2b
.end method

.method public h()V
    .registers 2

    .prologue
    .line 1956
    invoke-virtual {p0}, Lbf/m;->bz()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1960
    :goto_6
    return-void

    .line 1959
    :cond_7
    invoke-super {p0}, Lbf/i;->h()V

    goto :goto_6
.end method

.method protected h(I)V
    .registers 4
    .parameter

    .prologue
    .line 1739
    invoke-virtual {p0}, Lbf/m;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 1740
    invoke-virtual {p0}, Lbf/m;->u()Ljava/lang/String;

    move-result-object v1

    .line 1741
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->c()B

    move-result v0

    invoke-static {v0, v1, p1}, Lba/c;->a(ILjava/lang/String;I)V

    .line 1743
    return-void
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 415
    const/4 v0, 0x0

    return v0
.end method

.method protected i()Lbh/a;
    .registers 2

    .prologue
    .line 1189
    new-instance v0, Lbh/b;

    invoke-direct {v0, p0}, Lbh/b;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected i(I)V
    .registers 3
    .parameter

    .prologue
    .line 2019
    iget v0, p0, Lbf/m;->v:I

    if-ne v0, p1, :cond_5

    .line 2046
    :goto_4
    return-void

    .line 2023
    :cond_5
    invoke-direct {p0, p1}, Lbf/m;->j(I)V

    .line 2026
    iput p1, p0, Lbf/m;->v:I

    .line 2034
    iget v0, p0, Lbf/m;->v:I

    if-nez v0, :cond_1f

    .line 2035
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_17

    .line 2036
    iget-object v0, p0, Lbf/m;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    .line 2044
    :cond_17
    :goto_17
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/m;->e(Z)V

    .line 2045
    invoke-virtual {p0}, Lbf/m;->y()V

    goto :goto_4

    .line 2039
    :cond_1f
    invoke-virtual {p0}, Lbf/m;->r()V

    goto :goto_17
.end method

.method public j(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 2322
    invoke-virtual {p0}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public k(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 2334
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/m;->a(LaN/g;)Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method protected m()V
    .registers 3

    .prologue
    .line 1586
    invoke-virtual {p0}, Lbf/m;->bx()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 1587
    iget-object v0, p0, Lbf/m;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_14

    .line 1588
    const/16 v0, 0x1f

    invoke-virtual {p0, v0}, Lbf/m;->f(I)V

    .line 1591
    :cond_14
    invoke-super {p0}, Lbf/i;->m()V

    .line 1592
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1593
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-nez v1, :cond_29

    .line 1597
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lbf/m;->a(ILcom/google/googlenav/ai;)V

    .line 1599
    :cond_29
    invoke-direct {p0}, Lbf/m;->f()V

    .line 1602
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lbf/m;->f(I)V

    .line 1605
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->v()Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 1609
    invoke-direct {p0, v0}, Lbf/m;->l(Lcom/google/googlenav/ai;)V

    .line 1614
    :cond_3d
    if-eqz v0, :cond_48

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bS()Z

    move-result v1

    if-eqz v1, :cond_48

    .line 1615
    invoke-direct {p0, v0}, Lbf/m;->m(Lcom/google/googlenav/ai;)V

    .line 1618
    :cond_48
    return-void
.end method

.method protected r()V
    .registers 1

    .prologue
    .line 2370
    invoke-super {p0}, Lbf/i;->r()V

    .line 2371
    invoke-direct {p0}, Lbf/m;->bI()V

    .line 2372
    return-void
.end method

.method protected u()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1526
    invoke-virtual {p0}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
