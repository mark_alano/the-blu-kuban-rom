.class public Lbf/x;
.super Lbf/bk;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct/range {p0 .. p8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p5, v0}, Lcom/google/googlenav/n;->a(I)V

    .line 37
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbf/x;->b(B)V

    .line 38
    return-void
.end method


# virtual methods
.method public aB()Z
    .registers 2

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public av()I
    .registers 2

    .prologue
    .line 47
    const/16 v0, 0x12

    return v0
.end method

.method public h()V
    .registers 6

    .prologue
    const/16 v4, 0x2a

    const/16 v3, 0x1c

    .line 67
    invoke-virtual {p0}, Lbf/x;->bz()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 115
    :goto_a
    return-void

    .line 71
    :cond_b
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    .line 76
    const/16 v2, 0xd

    if-eq v1, v2, :cond_29

    const/16 v2, 0xb

    if-eq v1, v2, :cond_29

    if-eq v1, v3, :cond_29

    if-eq v1, v4, :cond_29

    .line 80
    invoke-super {p0}, Lbf/bk;->h()V

    goto :goto_a

    .line 84
    :cond_29
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 85
    if-ne v1, v3, :cond_40

    .line 89
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "recent"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    .line 91
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    goto :goto_a

    .line 93
    :cond_40
    if-ne v1, v4, :cond_52

    .line 96
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    .line 98
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    goto :goto_a

    .line 102
    :cond_52
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    .line 103
    iget-object v0, p0, Lbf/x;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    sparse-switch v1, :sswitch_data_74

    goto :goto_a

    .line 112
    :sswitch_6d
    check-cast v0, Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bD()V

    goto :goto_a

    .line 104
    nop

    :sswitch_data_74
    .sparse-switch
        0x0 -> :sswitch_6d
        0x2 -> :sswitch_6d
        0xd -> :sswitch_6d
        0x1a -> :sswitch_6d
    .end sparse-switch
.end method

.method protected l()V
    .registers 1

    .prologue
    .line 43
    return-void
.end method
