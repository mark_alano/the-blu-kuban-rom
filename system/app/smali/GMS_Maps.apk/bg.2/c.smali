.class Lbg/c;
.super LR/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbg/b;


# direct methods
.method constructor <init>(Lbg/b;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 221
    iput-object p1, p0, Lbg/c;->a:Lbg/b;

    invoke-direct {p0, p2}, LR/c;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public l()V
    .registers 9

    .prologue
    const-wide/16 v6, 0xbb8

    .line 224
    :cond_2
    :goto_2
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->a(Lbg/b;)Z

    move-result v0

    if-nez v0, :cond_7a

    .line 225
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->b(Lbg/b;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 226
    :try_start_11
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_14
    .catchall {:try_start_11 .. :try_end_14} :catchall_77

    move-result-wide v2

    .line 228
    :try_start_15
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->c(Lbg/b;)J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 229
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2c

    .line 230
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->b(Lbg/b;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_2c
    .catchall {:try_start_15 .. :try_end_2c} :catchall_77
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_2c} :catch_7d

    .line 235
    :cond_2c
    :goto_2c
    :try_start_2c
    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_77

    .line 237
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->a(Lbg/b;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 238
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 241
    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v2}, Lbg/b;->c(Lbg/b;)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_57

    .line 242
    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v2}, Lbg/b;->d(Lbg/b;)LaN/p;

    move-result-object v2

    invoke-virtual {v2}, LaN/p;->a()LaN/D;

    move-result-object v2

    invoke-virtual {v2}, LaN/D;->c()V

    .line 243
    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    add-long v3, v0, v6

    invoke-static {v2, v3, v4}, Lbg/b;->a(Lbg/b;J)J

    .line 246
    :cond_57
    iget-object v2, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v2}, Lbg/b;->e(Lbg/b;)J

    move-result-wide v2

    add-long/2addr v2, v6

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    .line 248
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->f(Lbg/b;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 250
    :try_start_69
    iget-object v0, p0, Lbg/c;->a:Lbg/b;

    invoke-static {v0}, Lbg/b;->f(Lbg/b;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_72
    .catchall {:try_start_69 .. :try_end_72} :catchall_74
    .catch Ljava/lang/InterruptedException; {:try_start_69 .. :try_end_72} :catch_7b

    .line 254
    :goto_72
    :try_start_72
    monitor-exit v1

    goto :goto_2

    :catchall_74
    move-exception v0

    monitor-exit v1
    :try_end_76
    .catchall {:try_start_72 .. :try_end_76} :catchall_74

    throw v0

    .line 235
    :catchall_77
    move-exception v0

    :try_start_78
    monitor-exit v1
    :try_end_79
    .catchall {:try_start_78 .. :try_end_79} :catchall_77

    throw v0

    .line 258
    :cond_7a
    return-void

    .line 251
    :catch_7b
    move-exception v0

    goto :goto_72

    .line 232
    :catch_7d
    move-exception v0

    goto :goto_2c
.end method
