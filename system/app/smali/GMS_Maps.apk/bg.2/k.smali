.class Lbg/k;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"


# instance fields
.field private final d:Ljava/util/List;

.field private e:Lcom/google/android/maps/driveabout/vector/VectorMapView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 1336
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    .line 1330
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbg/k;->d:Ljava/util/List;

    .line 1337
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 1351
    iget-object v0, p0, Lbg/k;->e:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_9

    .line 1352
    iget-object v0, p0, Lbg/k;->e:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->q_()V

    .line 1354
    :cond_9
    return-void
.end method

.method private declared-synchronized h()[Lcom/google/android/maps/driveabout/vector/A;
    .registers 3

    .prologue
    .line 1378
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    iget-object v1, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/maps/driveabout/vector/A;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/A;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    monitor-exit p0

    return-object v0

    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1392
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_16

    .line 1393
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    .line 1394
    array-length v2, v1

    const/4 v0, 0x0

    :goto_c
    if-ge v0, v2, :cond_16

    aget-object v3, v1, v0

    .line 1395
    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/A;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1394
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 1398
    :cond_16
    return-void
.end method

.method public declared-synchronized a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1459
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 1460
    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/A;->a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_17

    goto :goto_7

    .line 1459
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1462
    :cond_1a
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/A;)V
    .registers 3
    .parameter

    .prologue
    .line 1362
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1363
    invoke-direct {p0}, Lbg/k;->e()V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 1364
    monitor-exit p0

    return-void

    .line 1362
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V
    .registers 3
    .parameter

    .prologue
    .line 1340
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lbg/k;->e:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 1341
    if-nez p1, :cond_a

    .line 1342
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 1344
    :cond_a
    monitor-exit p0

    return-void

    .line 1340
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;FFLo/T;LC/a;I)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1488
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v8

    .line 1489
    array-length v9, v8

    const/4 v0, 0x0

    move v7, v0

    :goto_7
    if-ge v7, v9, :cond_19

    aget-object v0, v8, v7

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    .line 1490
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/A;->a(Ljava/util/List;FFLo/T;LC/a;I)V

    .line 1489
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_7

    .line 1492
    :cond_19
    return-void
.end method

.method public declared-synchronized a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1473
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 1474
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/A;->a(Z)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_17

    goto :goto_7

    .line 1473
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1476
    :cond_1a
    monitor-exit p0

    return-void
.end method

.method public a_(FFLo/T;LC/a;)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1402
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    .line 1403
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_7
    if-ltz v0, :cond_16

    .line 1404
    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/A;->a_(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1405
    const/4 v0, 0x1

    .line 1408
    :goto_12
    return v0

    .line 1403
    :cond_13
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    .line 1408
    :cond_16
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public declared-synchronized b(Lcom/google/android/maps/driveabout/vector/A;)V
    .registers 3
    .parameter

    .prologue
    .line 1367
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1368
    invoke-direct {p0}, Lbg/k;->e()V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 1369
    monitor-exit p0

    return-void

    .line 1367
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(FFLC/a;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1437
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    .line 1438
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_7
    if-ltz v0, :cond_16

    .line 1439
    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/A;->b(FFLC/a;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1440
    const/4 v0, 0x1

    .line 1443
    :goto_12
    return v0

    .line 1438
    :cond_13
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    .line 1443
    :cond_16
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public declared-synchronized b(LC/a;LD/a;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1383
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    .line 1384
    array-length v2, v1

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v2, :cond_11

    aget-object v3, v1, v0

    .line 1385
    invoke-virtual {v3, p1, p2}, Lcom/google/android/maps/driveabout/vector/A;->b(LC/a;LD/a;)Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_14

    .line 1384
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1387
    :cond_11
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 1383
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 1466
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 1467
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/A;->c(LD/a;)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_17

    goto :goto_7

    .line 1466
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1469
    :cond_1a
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized c(Lcom/google/android/maps/driveabout/vector/A;)V
    .registers 3
    .parameter

    .prologue
    .line 1372
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1373
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1374
    invoke-direct {p0}, Lbg/k;->e()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 1375
    monitor-exit p0

    return-void

    .line 1372
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(FFLo/T;LC/a;)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1413
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    .line 1414
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_7
    if-ltz v0, :cond_16

    .line 1415
    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/A;->c(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1416
    const/4 v0, 0x1

    .line 1419
    :goto_12
    return v0

    .line 1414
    :cond_13
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    .line 1419
    :cond_16
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public d(FFLo/T;LC/a;)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1448
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v1

    .line 1449
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_7
    if-ltz v0, :cond_16

    .line 1450
    aget-object v2, v1, v0

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/A;->d(FFLo/T;LC/a;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1451
    const/4 v0, 0x1

    .line 1454
    :goto_12
    return v0

    .line 1449
    :cond_13
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    .line 1454
    :cond_16
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public i_()V
    .registers 3

    .prologue
    .line 1480
    iget-object v0, p0, Lbg/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/A;

    .line 1481
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/A;->i_()V

    goto :goto_6

    .line 1483
    :cond_16
    return-void
.end method

.method public j_()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1426
    invoke-direct {p0}, Lbg/k;->h()[Lcom/google/android/maps/driveabout/vector/A;

    move-result-object v2

    move v0, v1

    .line 1427
    :goto_6
    array-length v3, v2

    if-ge v0, v3, :cond_12

    .line 1428
    aget-object v3, v2, v0

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/A;->j_()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1429
    const/4 v1, 0x1

    .line 1432
    :cond_12
    return v1

    .line 1427
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_6
.end method

.method public l()I
    .registers 2

    .prologue
    .line 1496
    const/4 v0, 0x0

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 1358
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
