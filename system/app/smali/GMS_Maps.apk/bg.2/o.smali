.class public Lbg/o;
.super Lbf/bE;
.source "SourceFile"


# instance fields
.field private final C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private D:Lcom/google/android/maps/driveabout/vector/bd;

.field private E:Lcom/google/android/maps/driveabout/vector/bb;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/android/maps/driveabout/vector/VectorMapView;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct/range {p0 .. p6}, Lbf/bE;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    .line 33
    new-instance v0, Lbg/p;

    invoke-direct {v0, p0}, Lbg/p;-><init>(Lbg/o;)V

    iput-object v0, p0, Lbg/o;->E:Lcom/google/android/maps/driveabout/vector/bb;

    .line 54
    iput-object p7, p0, Lbg/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 55
    return-void
.end method

.method static synthetic a(Lbg/o;)LaN/u;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lbg/o;->d:LaN/u;

    return-object v0
.end method

.method static synthetic b(Lbg/o;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lbg/o;->b:Lcom/google/googlenav/ui/s;

    return-object v0
.end method


# virtual methods
.method protected bK()V
    .registers 3

    .prologue
    .line 64
    iget-object v0, p0, Lbg/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-nez v0, :cond_5

    .line 77
    :goto_4
    return-void

    .line 73
    :cond_5
    iget-object v0, p0, Lbg/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, LA/c;->f:LA/c;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Landroid/content/res/Resources;LA/c;)Lcom/google/android/maps/driveabout/vector/bd;

    move-result-object v0

    iput-object v0, p0, Lbg/o;->D:Lcom/google/android/maps/driveabout/vector/bd;

    .line 75
    iget-object v0, p0, Lbg/o;->D:Lcom/google/android/maps/driveabout/vector/bd;

    iget-object v1, p0, Lbg/o;->E:Lcom/google/android/maps/driveabout/vector/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bd;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 76
    iget-object v0, p0, Lbg/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/o;->D:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_4
.end method

.method protected bL()V
    .registers 3

    .prologue
    .line 90
    iget-object v0, p0, Lbg/o;->D:Lcom/google/android/maps/driveabout/vector/bd;

    if-eqz v0, :cond_b

    .line 91
    iget-object v0, p0, Lbg/o;->D:Lcom/google/android/maps/driveabout/vector/bd;

    iget-object v1, p0, Lbg/o;->E:Lcom/google/android/maps/driveabout/vector/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bd;->b(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 93
    :cond_b
    iget-object v0, p0, Lbg/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_16

    .line 95
    iget-object v0, p0, Lbg/o;->C:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lbg/o;->D:Lcom/google/android/maps/driveabout/vector/bd;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 97
    :cond_16
    return-void
.end method
