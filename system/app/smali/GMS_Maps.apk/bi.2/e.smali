.class public Lbi/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lax/w;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lax/w;)V
    .registers 2
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lbi/e;->a:Lax/w;

    .line 56
    return-void
.end method

.method private a(Lbi/g;)F
    .registers 5
    .parameter

    .prologue
    .line 91
    const/4 v1, 0x0

    .line 92
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p1}, Lbi/g;->a()I

    move-result v2

    if-ge v0, v2, :cond_14

    .line 93
    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v2

    invoke-virtual {v2}, Lbi/h;->r()F

    move-result v2

    add-float/2addr v1, v2

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 95
    :cond_14
    return v1
.end method

.method private a(Lax/t;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v1, Lbi/g;

    new-instance v2, Lbi/p;

    iget-object v3, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v3, p1, p2}, Lbi/p;-><init>(Lax/w;Lax/t;I)V

    invoke-direct {v1, v2}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method private a(Lax/t;ILjava/util/List;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 251
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v1, Lbi/g;

    new-instance v2, Lbi/m;

    iget-object v3, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v3, p1, p2, p3}, Lbi/m;-><init>(Lax/w;Lax/t;ILjava/util/List;)V

    invoke-direct {v1, v2}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    return-void
.end method

.method private a(Lax/t;Lax/t;ILcom/google/common/collect/P;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 229
    new-instance v7, Lbi/g;

    new-instance v0, Lbi/j;

    iget-object v1, p0, Lbi/e;->a:Lax/w;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lbi/j;-><init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V

    invoke-direct {v7, v0}, Lbi/g;-><init>(Lbi/h;)V

    .line 232
    invoke-virtual {p2}, Lax/t;->Q()[Lax/u;

    move-result-object v8

    .line 233
    :goto_1c
    array-length v0, v8

    if-ge v6, v0, :cond_44

    .line 234
    aget-object v2, v8, v6

    .line 235
    add-int/lit8 v0, v6, 0x1

    array-length v1, v8

    if-ge v0, v1, :cond_42

    add-int/lit8 v0, v6, 0x1

    aget-object v3, v8, v0

    .line 236
    :goto_2a
    new-instance v0, Lbi/r;

    add-int/lit8 v1, v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move-object v1, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lbi/r;-><init>(Lax/t;Lax/u;Lax/u;ILjava/util/List;)V

    invoke-virtual {v7, v0}, Lbi/g;->a(Lbi/h;)V

    .line 233
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1c

    .line 235
    :cond_42
    const/4 v3, 0x0

    goto :goto_2a

    .line 239
    :cond_44
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    return-void
.end method

.method private a(Lax/t;Lax/t;ILjava/util/List;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 219
    iget-object v6, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v7, Lbi/g;

    new-instance v0, Lbi/l;

    iget-object v1, p0, Lbi/e;->a:Lax/w;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbi/l;-><init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V

    invoke-direct {v7, v0}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    return-void
.end method

.method private b()V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 76
    move v1, v2

    :goto_2
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_45

    .line 77
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->a(Lbi/g;)F

    move-result v5

    .line 78
    const/4 v0, 0x0

    move v3, v2

    move v4, v0

    .line 79
    :goto_19
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->a()I

    move-result v0

    if-ge v3, v0, :cond_41

    .line 80
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0, v3}, Lbi/g;->a(I)Lbi/h;

    move-result-object v0

    .line 81
    sub-float v6, v5, v4

    invoke-virtual {v0, v6}, Lbi/h;->c(F)V

    .line 82
    invoke-virtual {v0}, Lbi/h;->r()F

    move-result v0

    add-float/2addr v4, v0

    .line 79
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_19

    .line 76
    :cond_41
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 85
    :cond_45
    return-void
.end method

.method private b(Lax/t;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 263
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v1, Lbi/g;

    new-instance v2, Lbi/k;

    iget-object v3, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v3, p1, p2}, Lbi/k;-><init>(Lax/w;Lax/t;I)V

    invoke-direct {v1, v2}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    return-void
.end method

.method private b(Lax/t;Lax/t;ILjava/util/List;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 244
    iget-object v6, p0, Lbi/e;->b:Ljava/util/List;

    new-instance v7, Lbi/g;

    new-instance v0, Lbi/o;

    iget-object v1, p0, Lbi/e;->a:Lax/w;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbi/o;-><init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V

    invoke-direct {v7, v0}, Lbi/g;-><init>(Lbi/h;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    return-void
.end method

.method private b(Lbi/g;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 147
    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Lbi/h;->b()Lbi/q;

    move-result-object v2

    sget-object v3, Lbi/q;->b:Lbi/q;

    if-ne v2, v3, :cond_3d

    .line 149
    invoke-virtual {v1}, Lbi/h;->a()Lax/t;

    move-result-object v1

    .line 150
    invoke-virtual {v1}, Lax/t;->x()I

    move-result v1

    .line 151
    invoke-direct {p0, p1}, Lbi/e;->a(Lbi/g;)F

    move-result v2

    .line 152
    :goto_19
    invoke-virtual {p1}, Lbi/g;->a()I

    move-result v3

    if-ge v0, v3, :cond_3d

    .line 153
    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v3

    .line 154
    invoke-virtual {v3}, Lbi/h;->p()Z

    move-result v4

    if-nez v4, :cond_3a

    if-lez v1, :cond_3a

    .line 155
    int-to-float v4, v1

    invoke-virtual {v3}, Lbi/h;->r()F

    move-result v3

    mul-float/2addr v3, v4

    div-float/2addr v3, v2

    float-to-int v3, v3

    .line 157
    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v4

    invoke-virtual {v4, v3}, Lbi/h;->a(I)V

    .line 152
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 161
    :cond_3d
    return-void
.end method

.method private c(Lbi/g;)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 167
    move v1, v0

    .line 168
    :goto_2
    invoke-virtual {p1}, Lbi/g;->a()I

    move-result v2

    if-ge v0, v2, :cond_14

    .line 169
    invoke-virtual {p1, v0}, Lbi/g;->a(I)Lbi/h;

    move-result-object v2

    invoke-virtual {v2}, Lbi/h;->q()I

    move-result v2

    add-int/2addr v1, v2

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 171
    :cond_14
    return v1
.end method

.method private c()V
    .registers 10

    .prologue
    const/4 v2, 0x0

    .line 103
    invoke-direct {p0}, Lbi/e;->e()V

    .line 104
    invoke-direct {p0}, Lbi/e;->d()I

    move-result v7

    move v1, v2

    move v3, v2

    .line 106
    :goto_a
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_56

    .line 108
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->c(Lbi/g;)I

    move-result v8

    move v4, v2

    move v5, v2

    .line 109
    :goto_20
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->a()I

    move-result v0

    if-ge v4, v0, :cond_52

    .line 110
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0, v4}, Lbi/g;->a(I)Lbi/h;

    move-result-object v0

    .line 111
    sub-int v6, v8, v5

    invoke-virtual {v0, v6}, Lbi/h;->e(I)V

    .line 112
    sub-int v6, v7, v3

    invoke-virtual {v0, v6}, Lbi/h;->f(I)V

    .line 113
    invoke-virtual {v0}, Lbi/h;->q()I

    move-result v0

    .line 114
    add-int v6, v3, v0

    .line 115
    add-int v3, v5, v0

    .line 109
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v5, v3

    move v3, v6

    goto :goto_20

    .line 106
    :cond_52
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 118
    :cond_56
    return-void
.end method

.method private d()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 124
    move v1, v0

    move v2, v0

    .line 125
    :goto_3
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1c

    .line 126
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->c(Lbi/g;)I

    move-result v0

    add-int/2addr v2, v0

    .line 125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 128
    :cond_1c
    return v2
.end method

.method private e()V
    .registers 3

    .prologue
    .line 135
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_27

    .line 136
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->b()Z

    move-result v0

    if-nez v0, :cond_23

    .line 137
    iget-object v0, p0, Lbi/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-direct {p0, v0}, Lbi/e;->b(Lbi/g;)V

    .line 135
    :cond_23
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 140
    :cond_27
    return-void
.end method

.method private f()V
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 175
    new-instance v2, Lbi/f;

    iget-object v0, p0, Lbi/e;->a:Lax/w;

    invoke-direct {v2, v0}, Lbi/f;-><init>(Lax/b;)V

    .line 177
    invoke-virtual {v2}, Lbi/f;->i()Z

    move-result v0

    if-nez v0, :cond_45

    .line 180
    invoke-virtual {v2}, Lbi/f;->d()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_46

    .line 181
    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v0

    .line 182
    invoke-virtual {v2}, Lbi/f;->a()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->e()I

    move-result v4

    invoke-virtual {v2}, Lbi/f;->h()Lcom/google/common/collect/P;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v0, v3, v4, v5}, Lbi/e;->a(Lax/t;Lax/t;ILjava/util/List;)V

    .line 184
    invoke-virtual {v2}, Lbi/f;->g()Z

    .line 185
    invoke-virtual {v2}, Lbi/f;->i()Z

    move-result v3

    if-eqz v3, :cond_47

    .line 187
    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->e()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lbi/e;->b(Lax/t;I)V

    .line 215
    :cond_45
    :goto_45
    return-void

    :cond_46
    move-object v0, v1

    .line 196
    :cond_47
    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->e()I

    move-result v4

    invoke-virtual {v2}, Lbi/f;->h()Lcom/google/common/collect/P;

    move-result-object v5

    invoke-direct {p0, v0, v3, v4, v5}, Lbi/e;->a(Lax/t;Lax/t;ILcom/google/common/collect/P;)V

    .line 197
    invoke-virtual {v2}, Lbi/f;->g()Z

    move-result v0

    .line 199
    :goto_5a
    invoke-virtual {v2}, Lbi/f;->i()Z

    move-result v3

    if-nez v3, :cond_87

    .line 201
    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->f()I

    move-result v4

    invoke-direct {p0, v0, v3, v4, v1}, Lbi/e;->b(Lax/t;Lax/t;ILjava/util/List;)V

    .line 203
    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->b()Lax/t;

    move-result-object v3

    invoke-virtual {v2}, Lbi/f;->e()I

    move-result v4

    invoke-virtual {v2}, Lbi/f;->h()Lcom/google/common/collect/P;

    move-result-object v5

    invoke-direct {p0, v0, v3, v4, v5}, Lbi/e;->a(Lax/t;Lax/t;ILcom/google/common/collect/P;)V

    .line 205
    invoke-virtual {v2}, Lbi/f;->g()Z

    move-result v0

    goto :goto_5a

    .line 207
    :cond_87
    if-nez v0, :cond_95

    .line 208
    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->e()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lbi/e;->a(Lax/t;I)V

    goto :goto_45

    .line 210
    :cond_95
    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->e()I

    move-result v1

    invoke-virtual {v2}, Lbi/f;->h()Lcom/google/common/collect/P;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v0, v1, v3}, Lbi/e;->a(Lax/t;ILjava/util/List;)V

    .line 212
    invoke-virtual {v2}, Lbi/f;->c()Lax/t;

    move-result-object v0

    invoke-virtual {v2}, Lbi/f;->e()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lbi/e;->b(Lax/t;I)V

    goto :goto_45
.end method


# virtual methods
.method public a()Lbi/d;
    .registers 3

    .prologue
    .line 65
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbi/e;->b:Ljava/util/List;

    .line 66
    invoke-direct {p0}, Lbi/e;->f()V

    .line 67
    invoke-direct {p0}, Lbi/e;->c()V

    .line 68
    invoke-direct {p0}, Lbi/e;->b()V

    .line 69
    new-instance v0, Lbi/d;

    iget-object v1, p0, Lbi/e;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Lbi/d;-><init>(Ljava/util/List;)V

    return-object v0
.end method
