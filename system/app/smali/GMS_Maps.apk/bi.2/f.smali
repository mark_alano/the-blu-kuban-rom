.class Lbi/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/common/collect/P;

.field private b:I

.field private c:I

.field private final d:Lax/b;

.field private e:Lcom/google/googlenav/ui/view/android/rideabout/m;

.field private f:Lcom/google/googlenav/ui/view/android/rideabout/m;

.field private g:I


# direct methods
.method public constructor <init>(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    invoke-static {}, Lcom/google/common/collect/P;->g()Lcom/google/common/collect/P;

    move-result-object v0

    iput-object v0, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    .line 278
    iput v1, p0, Lbi/f;->b:I

    .line 279
    iput v1, p0, Lbi/f;->c:I

    .line 286
    iput-object p1, p0, Lbi/f;->d:Lax/b;

    .line 287
    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->a:Lcom/google/googlenav/ui/view/android/rideabout/m;

    iput-object v0, p0, Lbi/f;->f:Lcom/google/googlenav/ui/view/android/rideabout/m;

    .line 288
    invoke-direct {p0}, Lbi/f;->l()V

    .line 289
    invoke-direct {p0}, Lbi/f;->k()V

    .line 290
    return-void
.end method

.method private static a(Lax/t;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 456
    invoke-virtual {p0}, Lax/t;->q()LaN/B;

    move-result-object v1

    if-nez v1, :cond_8

    .line 466
    :cond_7
    :goto_7
    return v0

    .line 458
    :cond_8
    invoke-virtual {p0}, Lax/t;->k()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 461
    invoke-virtual {p0}, Lax/t;->Q()[Lax/u;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_14
    if-ge v1, v3, :cond_21

    aget-object v4, v2, v1

    .line 462
    invoke-virtual {v4}, Lax/u;->d()LaN/B;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 461
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 466
    :cond_21
    const/4 v0, 0x1

    goto :goto_7
.end method

.method private c(I)Lax/t;
    .registers 5
    .parameter

    .prologue
    .line 317
    if-ltz p1, :cond_a

    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->ae()I

    move-result v0

    if-lt p1, v0, :cond_29

    .line 318
    :cond_a
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Step at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_29
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0, p1}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method private d(I)I
    .registers 3
    .parameter

    .prologue
    .line 381
    invoke-direct {p0, p1}, Lbi/f;->g(I)I

    move-result v0

    .line 382
    add-int/lit8 v0, v0, 0x1

    .line 383
    invoke-direct {p0, v0}, Lbi/f;->f(I)I

    move-result v0

    .line 384
    return v0
.end method

.method private e(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 434
    iget-object v0, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-virtual {v0}, Lcom/google/common/collect/P;->e()V

    move v0, p1

    .line 436
    :goto_7
    invoke-direct {p0, v0}, Lbi/f;->h(I)Z

    move-result v1

    if-nez v1, :cond_47

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->E()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_47

    .line 437
    if-eq v0, p1, :cond_2d

    .line 438
    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->q()LaN/B;

    move-result-object v1

    .line 439
    if-eqz v1, :cond_2d

    .line 440
    iget-object v2, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 443
    :cond_2d
    iget-object v1, p0, Lbi/f;->d:Lax/b;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, v2}, Lax/b;->a(II)[LaN/B;

    move-result-object v1

    .line 444
    if-eqz v1, :cond_44

    .line 445
    iget-object v2, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/google/common/collect/P;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    .line 447
    :cond_44
    add-int/lit8 v0, v0, 0x1

    .line 448
    goto :goto_7

    .line 449
    :cond_47
    return-void
.end method

.method private f(I)I
    .registers 4
    .parameter

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lbi/f;->a(I)Z

    move-result v0

    if-nez v0, :cond_26

    invoke-direct {p0, p1}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_26

    invoke-virtual {p0, p1}, Lbi/f;->b(I)Z

    move-result v0

    if-nez v0, :cond_26

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_26

    .line 503
    add-int/lit8 p1, p1, 0x1

    .line 505
    :cond_26
    return p1
.end method

.method private g(I)I
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 517
    :goto_1
    invoke-virtual {p0, p1}, Lbi/f;->a(I)Z

    move-result v0

    if-nez v0, :cond_26

    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    if-ne v0, v1, :cond_26

    invoke-virtual {p0, p1}, Lbi/f;->b(I)Z

    move-result v0

    if-nez v0, :cond_26

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    if-ne v0, v1, :cond_26

    .line 518
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 520
    :cond_26
    return p1
.end method

.method private h(I)Z
    .registers 4
    .parameter

    .prologue
    .line 540
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->ae()I

    move-result v0

    if-ge p1, v0, :cond_13

    invoke-direct {p0, p1}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_15

    :cond_13
    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private j()I
    .registers 4

    .prologue
    .line 356
    invoke-virtual {p0}, Lbi/f;->i()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 357
    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v2

    .line 358
    iget v0, p0, Lbi/f;->c:I

    move v1, v0

    :goto_d
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->f()I

    move-result v0

    if-ge v1, v0, :cond_2c

    .line 359
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0, v1}, Lax/b;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    .line 360
    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v0

    if-ne v0, v2, :cond_28

    .line 361
    iput v1, p0, Lbi/f;->c:I

    .line 362
    iget v0, p0, Lbi/f;->c:I

    .line 371
    :goto_27
    return v0

    .line 358
    :cond_28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 366
    :cond_2c
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->f()I

    move-result v0

    if-lez v0, :cond_3d

    .line 368
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v0}, Lax/b;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_27

    .line 371
    :cond_3d
    const/4 v0, -0x1

    goto :goto_27
.end method

.method private k()V
    .registers 4

    .prologue
    const/4 v2, 0x2

    .line 406
    invoke-virtual {p0}, Lbi/f;->i()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 408
    iget v0, p0, Lbi/f;->g:I

    .line 409
    invoke-virtual {p0, v0}, Lbi/f;->a(I)Z

    move-result v1

    if-nez v1, :cond_1b

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->E()I

    move-result v1

    if-eq v1, v2, :cond_1b

    .line 410
    add-int/lit8 v0, v0, 0x1

    .line 412
    :cond_1b
    invoke-virtual {p0, v0}, Lbi/f;->a(I)Z

    move-result v1

    if-nez v1, :cond_2e

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->E()I

    move-result v1

    if-ne v1, v2, :cond_2e

    .line 413
    invoke-direct {p0, v0}, Lbi/f;->e(I)V

    .line 427
    :cond_2e
    :goto_2e
    return-void

    .line 415
    :cond_2f
    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    if-ne v0, v2, :cond_3f

    .line 416
    iget v0, p0, Lbi/f;->b:I

    invoke-direct {p0, v0}, Lbi/f;->e(I)V

    goto :goto_2e

    .line 418
    :cond_3f
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    iget v1, p0, Lbi/f;->b:I

    iget v2, p0, Lbi/f;->b:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Lax/b;->a(II)[LaN/B;

    move-result-object v0

    .line 419
    if-eqz v0, :cond_2e

    array-length v1, v0

    if-eqz v1, :cond_2e

    .line 422
    iget-object v1, p0, Lbi/f;->d:Lax/b;

    iget v2, p0, Lbi/f;->b:I

    invoke-virtual {v1, v2}, Lax/b;->n(I)Lax/t;

    move-result-object v1

    .line 423
    invoke-static {v1}, Lbi/f;->a(Lax/t;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 424
    iget-object v2, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    invoke-static {v0, v1, v2}, Lbi/x;->a([LaN/B;Lax/t;Lcom/google/common/collect/P;)V

    goto :goto_2e
.end method

.method private l()V
    .registers 3

    .prologue
    .line 484
    invoke-virtual {p0}, Lbi/f;->i()Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->a:Lcom/google/googlenav/ui/view/android/rideabout/m;

    :goto_8
    iput-object v0, p0, Lbi/f;->e:Lcom/google/googlenav/ui/view/android/rideabout/m;

    .line 489
    return-void

    .line 484
    :cond_b
    invoke-virtual {p0}, Lbi/f;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_15

    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    goto :goto_8

    :cond_15
    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    goto :goto_8
.end method


# virtual methods
.method public a()Lax/t;
    .registers 3

    .prologue
    .line 297
    iget v0, p0, Lbi/f;->b:I

    invoke-direct {p0, v0}, Lbi/f;->d(I)I

    move-result v0

    .line 298
    iget-object v1, p0, Lbi/f;->d:Lax/b;

    invoke-virtual {v1}, Lax/b;->ae()I

    move-result v1

    if-lt v0, v1, :cond_10

    .line 299
    const/4 v0, 0x0

    .line 301
    :goto_f
    return-object v0

    :cond_10
    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    goto :goto_f
.end method

.method public a(I)Z
    .registers 3
    .parameter

    .prologue
    .line 528
    iget-object v0, p0, Lbi/f;->d:Lax/b;

    if-eqz v0, :cond_a

    invoke-direct {p0, p1}, Lbi/f;->h(I)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public b()Lax/t;
    .registers 2

    .prologue
    .line 305
    iget v0, p0, Lbi/f;->b:I

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Z
    .registers 3
    .parameter

    .prologue
    .line 536
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lbi/f;->h(I)Z

    move-result v0

    return v0
.end method

.method public c()Lax/t;
    .registers 2

    .prologue
    .line 309
    iget v0, p0, Lbi/f;->g:I

    invoke-direct {p0, v0}, Lbi/f;->c(I)Lax/t;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 313
    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 344
    invoke-direct {p0}, Lbi/f;->j()I

    move-result v0

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 351
    invoke-virtual {p0}, Lbi/f;->e()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 352
    if-gez v0, :cond_9

    const/4 v0, 0x0

    :cond_9
    return v0
.end method

.method public g()Z
    .registers 4

    .prologue
    .line 392
    const/4 v0, 0x0

    .line 393
    iget v1, p0, Lbi/f;->b:I

    iput v1, p0, Lbi/f;->g:I

    .line 394
    iget v1, p0, Lbi/f;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbi/f;->b:I

    .line 395
    :goto_b
    invoke-virtual {p0}, Lbi/f;->i()Z

    move-result v1

    if-nez v1, :cond_24

    invoke-virtual {p0}, Lbi/f;->b()Lax/t;

    move-result-object v1

    invoke-virtual {v1}, Lax/t;->E()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_24

    .line 396
    iget v0, p0, Lbi/f;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbi/f;->b:I

    .line 397
    const/4 v0, 0x1

    goto :goto_b

    .line 399
    :cond_24
    iget-object v1, p0, Lbi/f;->e:Lcom/google/googlenav/ui/view/android/rideabout/m;

    iput-object v1, p0, Lbi/f;->f:Lcom/google/googlenav/ui/view/android/rideabout/m;

    .line 400
    invoke-direct {p0}, Lbi/f;->l()V

    .line 401
    invoke-direct {p0}, Lbi/f;->k()V

    .line 402
    return v0
.end method

.method public h()Lcom/google/common/collect/P;
    .registers 2

    .prologue
    .line 480
    iget-object v0, p0, Lbi/f;->a:Lcom/google/common/collect/P;

    return-object v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 524
    iget v0, p0, Lbi/f;->b:I

    invoke-virtual {p0, v0}, Lbi/f;->a(I)Z

    move-result v0

    return v0
.end method
