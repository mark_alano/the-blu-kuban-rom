.class public abstract Lbi/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lax/t;

.field protected final b:Lax/w;

.field protected final c:I

.field private final d:Lbi/q;

.field private final e:Ljava/util/List;

.field private f:Lcom/google/common/collect/ImmutableList;

.field private g:[F

.field private h:F

.field private i:I

.field private j:I

.field private k:I

.field private l:F


# direct methods
.method protected constructor <init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v1, -0x4080

    const/4 v0, -0x1

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput v1, p0, Lbi/h;->h:F

    .line 101
    iput v0, p0, Lbi/h;->i:I

    .line 106
    iput v0, p0, Lbi/h;->j:I

    .line 111
    iput v0, p0, Lbi/h;->k:I

    .line 116
    iput v1, p0, Lbi/h;->l:F

    .line 120
    iput-object p1, p0, Lbi/h;->d:Lbi/q;

    .line 121
    iput-object p2, p0, Lbi/h;->b:Lax/w;

    .line 122
    iput-object p3, p0, Lbi/h;->a:Lax/t;

    .line 123
    iput p4, p0, Lbi/h;->c:I

    .line 124
    if-eqz p5, :cond_21

    .line 125
    invoke-static {p5}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lbi/h;->e:Ljava/util/List;

    .line 129
    :goto_20
    return-void

    .line 127
    :cond_21
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lbi/h;->e:Ljava/util/List;

    goto :goto_20
.end method

.method private static C()F
    .registers 1

    .prologue
    .line 1137
    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->y:F

    return v0
.end method

.method static synthetic a(Ljava/util/Date;Ljava/util/Date;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-static {p0, p1}, Lbi/h;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method static a(LaN/B;LaN/B;Ljava/util/List;F)Lcom/google/common/collect/ImmutableList;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    invoke-static {p2}, Lcom/google/common/collect/bx;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v1

    .line 348
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 349
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/B;

    .line 350
    invoke-static {p0, v0}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v3

    cmpg-float v3, v3, p3

    if-gez v3, :cond_20

    .line 351
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_8

    :cond_20
    move-object p0, v0

    .line 353
    goto :goto_8

    .line 356
    :cond_22
    :goto_22
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3a

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/B;

    invoke-static {v0, p1}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    cmpg-float v0, v0, p3

    if-gez v0, :cond_3a

    .line 357
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_22

    .line 359
    :cond_3a
    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private a(Lax/m;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1024
    if-nez p1, :cond_5

    const-string v0, ""

    :goto_4
    return-object v0

    :cond_5
    iget-object v0, p0, Lbi/h;->b:Lax/w;

    invoke-virtual {v0, p1}, Lax/w;->a(Lax/m;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method static synthetic a(Lax/t;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 41
    invoke-static {p0}, Lbi/h;->b(Lax/t;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Lax/y;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1019
    invoke-static {p0}, Lcom/google/googlenav/ui/bv;->b(Lax/y;)Ljava/lang/String;

    move-result-object v0

    .line 1020
    if-nez v0, :cond_9

    const-string v0, ""

    :goto_8
    return-object v0

    :cond_9
    invoke-static {v0}, Lau/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;
    .registers 2
    .parameter

    .prologue
    .line 41
    invoke-static {p0}, Lbi/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method static a(Lax/w;ILbi/h;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1102
    invoke-virtual {p0, p1}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    .line 1103
    invoke-virtual {v0}, Lax/m;->y()I

    move-result v1

    .line 1104
    const/4 v3, 0x0

    .line 1108
    invoke-virtual {v0}, Lax/m;->r()Z

    move-result v4

    if-eqz v4, :cond_30

    invoke-virtual {v0}, Lax/m;->n()Z

    move-result v4

    if-eqz v4, :cond_30

    .line 1109
    check-cast v0, Lax/a;

    .line 1110
    invoke-virtual {v0}, Lax/a;->j()I

    move-result v3

    .line 1111
    invoke-virtual {v0}, Lax/a;->i()I

    move-result v0

    int-to-float v1, v0

    .line 1124
    :cond_23
    cmpl-float v0, v1, v2

    if-lez v0, :cond_2a

    .line 1125
    invoke-virtual {p2, v1}, Lbi/h;->a(F)V

    .line 1127
    :cond_2a
    if-lez v3, :cond_2f

    .line 1128
    invoke-virtual {p2, v3}, Lbi/h;->a(I)V

    .line 1130
    :cond_2f
    return-void

    :cond_30
    move v0, v1

    move v1, v2

    .line 1114
    :goto_32
    invoke-virtual {p0}, Lax/w;->ae()I

    move-result v4

    if-ge v0, v4, :cond_23

    .line 1115
    invoke-virtual {p0, v0}, Lax/w;->n(I)Lax/t;

    move-result-object v4

    .line 1116
    invoke-virtual {v4}, Lax/t;->E()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_23

    .line 1119
    invoke-virtual {v4}, Lax/t;->x()I

    move-result v5

    add-int/2addr v3, v5

    .line 1120
    invoke-virtual {v4}, Lax/t;->v()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v1, v4

    .line 1114
    add-int/lit8 v0, v0, 0x1

    goto :goto_32
.end method

.method private static b(Ljava/util/Date;Ljava/util/Date;)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 301
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static b(Lax/t;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1040
    invoke-virtual {p0}, Lax/t;->z()Ljava/lang/String;

    move-result-object v0

    .line 1041
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1042
    invoke-virtual {p0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    .line 1044
    :cond_e
    return-object v0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;
    .registers 2
    .parameter

    .prologue
    .line 1033
    if-nez p0, :cond_4

    .line 1034
    const/4 v0, 0x0

    .line 1036
    :goto_3
    return-object v0

    :cond_4
    invoke-static {p0}, Lcom/google/googlenav/ui/bd;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    goto :goto_3
.end method

.method static synthetic b(Lax/w;ILbi/h;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Lbi/h;->c(Lax/w;ILbi/h;)V

    return-void
.end method

.method private static c(Lax/w;ILbi/h;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1081
    invoke-static {p0, p1, p2}, Lbi/h;->a(Lax/w;ILbi/h;)V

    .line 1082
    invoke-virtual {p2}, Lbi/h;->r()F

    move-result v0

    const/high16 v1, -0x4080

    cmpl-float v0, v0, v1

    if-nez v0, :cond_14

    .line 1083
    invoke-virtual {p2}, Lbi/h;->y()F

    move-result v0

    invoke-virtual {p2, v0}, Lbi/h;->a(F)V

    .line 1085
    :cond_14
    iget v0, p2, Lbi/h;->k:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1c

    .line 1086
    invoke-virtual {p2}, Lbi/h;->s()V

    .line 1088
    :cond_1c
    return-void
.end method


# virtual methods
.method public A()I
    .registers 2

    .prologue
    .line 1064
    iget v0, p0, Lbi/h;->i:I

    return v0
.end method

.method public B()I
    .registers 2

    .prologue
    .line 1068
    iget v0, p0, Lbi/h;->j:I

    return v0
.end method

.method public a(Ljava/util/Date;)J
    .registers 4
    .parameter

    .prologue
    .line 226
    invoke-virtual {p0}, Lbi/h;->n()Ljava/util/Date;

    move-result-object v0

    .line 227
    if-nez v0, :cond_9

    .line 228
    const-wide/16 v0, 0x0

    .line 230
    :goto_8
    return-wide v0

    :cond_9
    invoke-static {p1, v0}, Lbi/h;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_8
.end method

.method public a()Lax/t;
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    return-object v0
.end method

.method public a(F)V
    .registers 2
    .parameter

    .prologue
    .line 249
    iput p1, p0, Lbi/h;->l:F

    .line 250
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 238
    iput p1, p0, Lbi/h;->k:I

    .line 239
    return-void
.end method

.method public b(F)I
    .registers 5
    .parameter

    .prologue
    .line 384
    invoke-virtual {p0}, Lbi/h;->y()F

    move-result v0

    mul-float v1, p1, v0

    .line 385
    const/4 v0, 0x0

    .line 386
    :goto_7
    invoke-virtual {p0, v0}, Lbi/h;->c(I)F

    move-result v2

    cmpg-float v2, v2, v1

    if-gez v2, :cond_12

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 389
    :cond_12
    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public b(I)LaN/B;
    .registers 7
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 369
    invoke-virtual {p0}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    .line 370
    if-gt v4, p1, :cond_2b

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    if-gt p1, v0, :cond_2b

    const/4 v0, 0x1

    :goto_e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index not in range:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    .line 371
    if-ne p1, v4, :cond_2d

    .line 372
    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    .line 376
    :goto_2a
    return-object v0

    .line 370
    :cond_2b
    const/4 v0, 0x0

    goto :goto_e

    .line 373
    :cond_2d
    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    if-ne p1, v0, :cond_38

    .line 374
    invoke-virtual {p0}, Lbi/h;->v()LaN/B;

    move-result-object v0

    goto :goto_2a

    .line 376
    :cond_38
    invoke-virtual {v1, p1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/B;

    goto :goto_2a
.end method

.method public b()Lbi/q;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lbi/h;->d:Lbi/q;

    return-object v0
.end method

.method public c(I)F
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 398
    invoke-virtual {p0}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v5

    .line 400
    if-gt v6, p1, :cond_4f

    if-gt p1, v5, :cond_4f

    const/4 v0, 0x1

    :goto_10
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shapePointIndex not in range [0, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    .line 404
    iget-object v0, p0, Lbi/h;->g:[F

    if-nez v0, :cond_51

    .line 406
    add-int/lit8 v0, v5, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lbi/h;->g:[F

    .line 409
    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    move v4, v2

    .line 410
    :goto_3b
    if-gt v1, v5, :cond_51

    .line 411
    invoke-virtual {p0, v1}, Lbi/h;->b(I)LaN/B;

    move-result-object v3

    .line 412
    invoke-static {v0, v3}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    .line 413
    add-float/2addr v0, v4

    .line 414
    iget-object v4, p0, Lbi/h;->g:[F

    aput v0, v4, v1

    .line 410
    add-int/lit8 v1, v1, 0x1

    move v4, v0

    move-object v0, v3

    goto :goto_3b

    :cond_4f
    move v0, v1

    .line 400
    goto :goto_10

    .line 418
    :cond_51
    if-ne p1, v6, :cond_55

    move v0, v2

    :goto_54
    return v0

    :cond_55
    iget-object v0, p0, Lbi/h;->g:[F

    aget v0, v0, p1

    goto :goto_54
.end method

.method public c(F)V
    .registers 2
    .parameter

    .prologue
    .line 1048
    iput p1, p0, Lbi/h;->h:F

    .line 1049
    return-void
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 143
    sget-object v0, Lbi/i;->a:[I

    iget-object v1, p0, Lbi/h;->d:Lbi/q;

    invoke-virtual {v1}, Lbi/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_12

    .line 148
    const/4 v0, 0x1

    :goto_e
    return v0

    .line 146
    :pswitch_f
    const/4 v0, 0x0

    goto :goto_e

    .line 143
    nop

    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method public d(I)F
    .registers 4
    .parameter

    .prologue
    .line 425
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lbi/h;->c(I)F

    move-result v0

    invoke-virtual {p0, p1}, Lbi/h;->c(I)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 153
    iget v0, p0, Lbi/h;->c:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .registers 3

    .prologue
    .line 157
    iget-object v0, p0, Lbi/h;->b:Lax/w;

    iget v1, p0, Lbi/h;->c:I

    invoke-virtual {v0, v1}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    .line 158
    invoke-direct {p0, v0}, Lbi/h;->a(Lax/m;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .registers 2
    .parameter

    .prologue
    .line 1056
    iput p1, p0, Lbi/h;->i:I

    .line 1057
    return-void
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(I)V
    .registers 2
    .parameter

    .prologue
    .line 1060
    iput p1, p0, Lbi/h;->j:I

    .line 1061
    return-void
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()J
    .registers 3

    .prologue
    .line 170
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->y()J

    move-result-wide v0

    return-wide v0
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 174
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 178
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 186
    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()I
    .registers 2

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public abstract n()Ljava/util/Date;
.end method

.method public abstract o()Ljava/util/Date;
.end method

.method public p()Z
    .registers 3

    .prologue
    .line 242
    iget v0, p0, Lbi/h;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public q()I
    .registers 2

    .prologue
    .line 263
    invoke-virtual {p0}, Lbi/h;->p()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 264
    iget v0, p0, Lbi/h;->k:I

    .line 266
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public r()F
    .registers 2

    .prologue
    .line 274
    iget v0, p0, Lbi/h;->l:F

    return v0
.end method

.method protected s()V
    .registers 3

    .prologue
    .line 285
    invoke-virtual {p0}, Lbi/h;->n()Ljava/util/Date;

    move-result-object v0

    .line 286
    invoke-virtual {p0}, Lbi/h;->o()Ljava/util/Date;

    move-result-object v1

    .line 287
    if-eqz v0, :cond_c

    if-nez v1, :cond_d

    .line 291
    :cond_c
    :goto_c
    return-void

    .line 290
    :cond_d
    invoke-virtual {p0}, Lbi/h;->n()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0}, Lbi/h;->o()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lbi/h;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    iput v0, p0, Lbi/h;->k:I

    goto :goto_c
.end method

.method protected t()Z
    .registers 2

    .prologue
    .line 294
    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lbi/h;->v()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public abstract u()LaN/B;
.end method

.method public abstract v()LaN/B;
.end method

.method public abstract w()Ljava/lang/String;
.end method

.method public x()Lcom/google/common/collect/ImmutableList;
    .registers 5

    .prologue
    .line 326
    iget-object v0, p0, Lbi/h;->f:Lcom/google/common/collect/ImmutableList;

    if-nez v0, :cond_18

    .line 327
    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, Lbi/h;->v()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbi/h;->e:Ljava/util/List;

    invoke-static {}, Lbi/h;->C()F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lbi/h;->a(LaN/B;LaN/B;Ljava/util/List;F)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lbi/h;->f:Lcom/google/common/collect/ImmutableList;

    .line 330
    :cond_18
    iget-object v0, p0, Lbi/h;->f:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public y()F
    .registers 2

    .prologue
    .line 433
    invoke-virtual {p0}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 434
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lbi/h;->c(I)F

    move-result v0

    return v0
.end method

.method public z()F
    .registers 2

    .prologue
    .line 1052
    iget v0, p0, Lbi/h;->h:F

    return v0
.end method
