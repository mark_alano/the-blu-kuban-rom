.class public final enum Lbi/q;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbi/q;

.field public static final enum b:Lbi/q;

.field public static final enum c:Lbi/q;

.field public static final enum d:Lbi/q;

.field public static final enum e:Lbi/q;

.field private static final synthetic f:[Lbi/q;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lbi/q;

    const-string v1, "FIRST_WALK"

    invoke-direct {v0, v1, v2}, Lbi/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbi/q;->a:Lbi/q;

    .line 48
    new-instance v0, Lbi/q;

    const-string v1, "BOARD"

    invoke-direct {v0, v1, v3}, Lbi/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbi/q;->b:Lbi/q;

    .line 49
    new-instance v0, Lbi/q;

    const-string v1, "STAY"

    invoke-direct {v0, v1, v4}, Lbi/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbi/q;->c:Lbi/q;

    .line 50
    new-instance v0, Lbi/q;

    const-string v1, "GET_OFF"

    invoke-direct {v0, v1, v5}, Lbi/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbi/q;->d:Lbi/q;

    .line 51
    new-instance v0, Lbi/q;

    const-string v1, "DESTINATION"

    invoke-direct {v0, v1, v6}, Lbi/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbi/q;->e:Lbi/q;

    .line 46
    const/4 v0, 0x5

    new-array v0, v0, [Lbi/q;

    sget-object v1, Lbi/q;->a:Lbi/q;

    aput-object v1, v0, v2

    sget-object v1, Lbi/q;->b:Lbi/q;

    aput-object v1, v0, v3

    sget-object v1, Lbi/q;->c:Lbi/q;

    aput-object v1, v0, v4

    sget-object v1, Lbi/q;->d:Lbi/q;

    aput-object v1, v0, v5

    sget-object v1, Lbi/q;->e:Lbi/q;

    aput-object v1, v0, v6

    sput-object v0, Lbi/q;->f:[Lbi/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbi/q;
    .registers 2
    .parameter

    .prologue
    .line 46
    const-class v0, Lbi/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbi/q;

    return-object v0
.end method

.method public static values()[Lbi/q;
    .registers 1

    .prologue
    .line 46
    sget-object v0, Lbi/q;->f:[Lbi/q;

    invoke-virtual {v0}, [Lbi/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbi/q;

    return-object v0
.end method
