.class public abstract Lbi/s;
.super Lbi/h;
.source "SourceFile"


# instance fields
.field protected d:Lax/v;

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 451
    invoke-direct/range {p0 .. p5}, Lbi/h;-><init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V

    .line 453
    if-eqz p3, :cond_b

    .line 454
    invoke-direct {p0}, Lbi/s;->C()V

    .line 455
    invoke-direct {p0}, Lbi/s;->E()V

    .line 457
    :cond_b
    return-void
.end method

.method private C()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 519
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    .line 521
    iget-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 522
    const/16 v0, 0x58b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->g:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    .line 524
    iput-boolean v3, p0, Lbi/s;->f:Z

    .line 530
    :cond_2a
    :goto_2a
    return-void

    .line 525
    :cond_2b
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->w()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 526
    const/16 v0, 0x58a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v2}, Lax/t;->x()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    goto :goto_2a
.end method

.method private E()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 533
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bd;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    .line 534
    iget-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 535
    const/16 v0, 0x58c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->h:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    .line 537
    iput-boolean v3, p0, Lbi/s;->e:Z

    .line 542
    :cond_2a
    :goto_2a
    return-void

    .line 538
    :cond_2b
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->s()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 539
    const/16 v0, 0x58d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v2}, Lax/t;->t()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    goto :goto_2a
.end method


# virtual methods
.method public D()Ljava/lang/String;
    .registers 2

    .prologue
    .line 492
    invoke-virtual {p0}, Lbi/s;->N()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public F()Z
    .registers 2

    .prologue
    .line 460
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->R()Z

    move-result v0

    return v0
.end method

.method public G()Ljava/lang/String;
    .registers 2

    .prologue
    .line 464
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-static {v0}, Lbi/h;->a(Lax/t;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .registers 3

    .prologue
    .line 478
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    iget-object v1, p0, Lbi/s;->b:Lax/w;

    invoke-virtual {v0, v1}, Lax/t;->a(Lax/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 482
    iget-object v1, p0, Lbi/s;->d:Lax/v;

    iget v1, v1, Lax/v;->b:I

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget v0, v0, Lax/v;->b:I

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/rideabout/r;->c(I)I

    move-result v0

    goto :goto_7
.end method

.method public J()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 487
    iget-object v1, p0, Lbi/s;->d:Lax/v;

    iget v1, v1, Lax/v;->c:I

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget v0, v0, Lax/v;->c:I

    invoke-static {v0}, Lcom/google/googlenav/ui/view/android/rideabout/r;->c(I)I

    move-result v0

    goto :goto_7
.end method

.method public K()Ljava/lang/String;
    .registers 2

    .prologue
    .line 496
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-object v0, v0, Lax/v;->f:Ljava/lang/String;

    return-object v0
.end method

.method public L()Ljava/lang/String;
    .registers 2

    .prologue
    .line 510
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-object v0, v0, Lax/v;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected M()Ljava/lang/String;
    .registers 2

    .prologue
    .line 545
    iget-object v0, p0, Lbi/s;->g:Ljava/lang/String;

    return-object v0
.end method

.method protected N()Ljava/lang/String;
    .registers 2

    .prologue
    .line 549
    iget-object v0, p0, Lbi/s;->h:Ljava/lang/String;

    return-object v0
.end method

.method public O()Z
    .registers 2

    .prologue
    .line 557
    iget-boolean v0, p0, Lbi/s;->e:Z

    return v0
.end method

.method public P()Z
    .registers 2

    .prologue
    .line 565
    iget-boolean v0, p0, Lbi/s;->f:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 501
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-object v0, v0, Lax/v;->e:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .registers 3

    .prologue
    .line 506
    iget-object v0, p0, Lbi/s;->d:Lax/v;

    iget-wide v0, v0, Lax/v;->h:J

    return-wide v0
.end method

.method public w()Ljava/lang/String;
    .registers 3

    .prologue
    .line 469
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->Q()[Lax/u;

    move-result-object v0

    .line 470
    array-length v1, v0

    if-lez v1, :cond_11

    .line 471
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lax/u;->a()Ljava/lang/String;

    move-result-object v0

    .line 473
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, Lbi/s;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_10
.end method
