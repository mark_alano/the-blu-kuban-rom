.class public Lbj/P;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:I

.field final b:Lcom/google/googlenav/ai;

.field final c:Lbf/aQ;

.field final d:Lbf/i;


# direct methods
.method public constructor <init>(ILcom/google/googlenav/ai;Lbf/aQ;Lbf/i;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput p1, p0, Lbj/P;->a:I

    .line 66
    iput-object p2, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    .line 67
    iput-object p3, p0, Lbj/P;->c:Lbf/aQ;

    .line 68
    iput-object p4, p0, Lbj/P;->d:Lbf/i;

    .line 70
    invoke-virtual {p0}, Lbj/P;->e()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lbj/P;->d()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 71
    :cond_17
    const/16 v0, 0x20

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ai;->o(I)V

    .line 75
    :cond_1c
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 76
    const/16 v0, 0x26

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ai;->o(I)V

    .line 80
    :cond_28
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->ci()Lcom/google/googlenav/ar;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 81
    const/16 v0, 0x30

    invoke-virtual {p2, v0}, Lcom/google/googlenav/ai;->o(I)V

    .line 84
    :cond_33
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 121
    iget v0, p0, Lbj/P;->a:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 4
    .parameter

    .prologue
    .line 98
    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    .line 99
    new-instance v1, Lbj/S;

    invoke-direct {v1}, Lbj/S;-><init>()V

    .line 100
    iput-object p1, v1, Lbj/S;->a:Landroid/view/View;

    .line 101
    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->b:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f100233

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/S;->c:Landroid/view/View;

    .line 103
    const v0, 0x7f100040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbj/S;->d:Landroid/view/View;

    .line 104
    const v0, 0x7f1002ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->e:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f1002ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->f:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f1002ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->g:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->h:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f1002b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->i:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f1002b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->j:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f1002ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->k:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f1002af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->l:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f1002b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->m:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f1002b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->n:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f1002b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbj/S;->o:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f1002b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbj/S;->p:Landroid/widget/ImageView;

    .line 116
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x8

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 126
    check-cast p2, Lbj/S;

    .line 129
    iget-object v0, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    move-object v0, v1

    .line 134
    :cond_13
    iget-object v2, p2, Lbj/S;->f:Landroid/widget/TextView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 135
    if-nez v0, :cond_107

    move-object v2, v1

    .line 138
    :goto_1b
    iget-object v3, p2, Lbj/S;->e:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 141
    iget-object v2, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-static {v2, v6}, Lbf/aS;->a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v7

    .line 142
    iget-object v2, p2, Lbj/S;->h:Landroid/widget/TextView;

    invoke-static {v2, v7}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 143
    if-nez v7, :cond_115

    move-object v2, v1

    .line 146
    :goto_2e
    iget-object v3, p2, Lbj/S;->g:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 149
    iget-object v2, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ci()Lcom/google/googlenav/ar;

    move-result-object v8

    .line 152
    if-eqz v8, :cond_175

    .line 153
    invoke-virtual {v8}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aX:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 155
    const/16 v2, 0x1de

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 158
    :goto_51
    iget-object v4, p2, Lbj/S;->j:Landroid/widget/TextView;

    invoke-static {v4, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 159
    iget-object v3, p2, Lbj/S;->i:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 161
    if-eqz v8, :cond_6b

    .line 162
    iget-object v2, p2, Lbj/S;->j:Landroid/widget/TextView;

    new-instance v3, Lbj/R;

    invoke-virtual {v8}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, p1, v4}, Lbj/R;-><init>(Lbj/P;Lcom/google/googlenav/ui/e;Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 169
    :cond_6b
    invoke-virtual {p0}, Lbj/P;->e()Ljava/lang/CharSequence;

    move-result-object v3

    .line 170
    invoke-virtual {p0}, Lbj/P;->d()Ljava/lang/CharSequence;

    move-result-object v2

    .line 171
    if-nez v3, :cond_123

    if-eqz v2, :cond_123

    .line 172
    const/16 v3, 0x121

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 176
    :cond_7d
    :goto_7d
    iget-object v4, p0, Lbj/P;->c:Lbf/aQ;

    invoke-virtual {v4}, Lbf/aQ;->b()Z

    move-result v4

    if-eqz v4, :cond_12f

    .line 177
    :goto_85
    iget-object v3, p2, Lbj/S;->l:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 178
    if-nez v2, :cond_132

    move-object v3, v1

    .line 180
    :goto_8d
    iget-object v4, p2, Lbj/S;->k:Landroid/widget/TextView;

    invoke-static {v4, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 183
    iget-object v4, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->by()Z

    move-result v4

    if-eqz v4, :cond_140

    const/16 v4, 0x5f2

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aV;->ba:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 187
    :goto_a6
    iget-object v5, p2, Lbj/S;->n:Landroid/widget/TextView;

    invoke-static {v5, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 188
    if-nez v4, :cond_143

    move-object v5, v1

    .line 190
    :goto_ae
    iget-object v9, p2, Lbj/S;->m:Landroid/widget/TextView;

    invoke-static {v9, v5}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 192
    if-nez v0, :cond_bd

    if-nez v7, :cond_bd

    if-nez v2, :cond_bd

    if-nez v4, :cond_bd

    if-eqz v8, :cond_151

    .line 195
    :cond_bd
    iget-object v0, p2, Lbj/S;->b:Landroid/widget/TextView;

    const/16 v2, 0xea

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/ui/aV;->bY:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, p2, Lbj/S;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 208
    :goto_d7
    if-eqz v3, :cond_168

    .line 209
    iget-object v0, p2, Lbj/S;->d:Landroid/view/View;

    new-instance v1, Lbj/Q;

    invoke-direct {v1, p0, p1}, Lbj/Q;-><init>(Lbj/P;Lcom/google/googlenav/ui/e;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    iget-object v0, p0, Lbj/P;->c:Lbf/aQ;

    invoke-virtual {v0}, Lbf/aQ;->b()Z

    move-result v0

    if-eqz v0, :cond_15d

    const/16 v0, 0x3c0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 218
    :goto_f1
    iget-object v1, p0, Lbj/P;->c:Lbf/aQ;

    invoke-virtual {v1}, Lbf/aQ;->b()Z

    move-result v1

    if-eqz v1, :cond_164

    const v1, 0x7f020223

    .line 226
    :goto_fc
    iget-object v2, p2, Lbj/S;->o:Landroid/widget/TextView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 227
    iget-object v0, p2, Lbj/S;->p:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/ImageView;I)V

    .line 228
    return-void

    .line 135
    :cond_107
    const/16 v2, 0x37a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_1b

    .line 143
    :cond_115
    const/16 v2, 0x40

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_2e

    .line 173
    :cond_123
    if-nez v2, :cond_7d

    if-eqz v3, :cond_7d

    .line 174
    const/16 v2, 0x30c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7d

    :cond_12f
    move-object v2, v3

    .line 176
    goto/16 :goto_85

    .line 178
    :cond_132
    const/16 v3, 0x1e1

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    goto/16 :goto_8d

    :cond_140
    move-object v4, v1

    .line 183
    goto/16 :goto_a6

    .line 188
    :cond_143
    const/16 v5, 0x309

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    sget-object v9, Lcom/google/googlenav/ui/aV;->aU:Lcom/google/googlenav/ui/aV;

    invoke-static {v5, v9}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    goto/16 :goto_ae

    .line 200
    :cond_151
    iget-object v0, p2, Lbj/S;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    iget-object v0, p2, Lbj/S;->c:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_d7

    .line 216
    :cond_15d
    const/16 v0, 0x3c1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_f1

    .line 218
    :cond_164
    const v1, 0x7f020224

    goto :goto_fc

    .line 221
    :cond_168
    iget-object v0, p2, Lbj/S;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget-object v0, p2, Lbj/S;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundResource(I)V

    move-object v0, v1

    move v1, v6

    goto :goto_fc

    :cond_175
    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_51
.end method

.method public b()I
    .registers 2

    .prologue
    .line 88
    const v0, 0x7f0400e4

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 93
    const/4 v0, 0x0

    return v0
.end method

.method protected d()Ljava/lang/CharSequence;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 231
    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 249
    :cond_9
    :goto_9
    return-object v0

    .line 234
    :cond_a
    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lbf/aS;->a(Lcom/google/googlenav/ai;ZZ)Ljava/util/Vector;

    move-result-object v1

    .line 236
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 237
    iget-object v0, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_28
    :goto_28
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    .line 242
    iget-object v3, v0, Lcom/google/googlenav/ui/aW;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    iget-boolean v0, v0, Lcom/google/googlenav/ui/aW;->d:Z

    if-eqz v0, :cond_28

    .line 244
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_28

    .line 247
    :cond_43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_9
.end method

.method protected e()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 253
    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 260
    :cond_9
    :goto_9
    return-object v0

    .line 256
    :cond_a
    iget-object v1, p0, Lbj/P;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bc()Lcom/google/googlenav/ac;

    move-result-object v1

    .line 257
    if-eqz v1, :cond_9

    .line 258
    invoke-static {v1}, Lbf/aS;->a(Lcom/google/googlenav/ac;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_9
.end method
