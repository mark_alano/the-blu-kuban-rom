.class Lbj/aA;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lbj/ao;

.field private final b:Lbj/aD;

.field private final c:Lbl/h;

.field private final d:Lbj/aC;


# direct methods
.method public constructor <init>(Lbj/ao;Lbj/aD;Lbl/h;Lbj/aC;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, Lbj/aA;->a:Lbj/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p2, p0, Lbj/aA;->b:Lbj/aD;

    .line 84
    iput-object p3, p0, Lbj/aA;->c:Lbl/h;

    .line 85
    iput-object p4, p0, Lbj/aA;->d:Lbj/aC;

    .line 86
    return-void
.end method

.method private a()Lbj/aB;
    .registers 4

    .prologue
    .line 89
    sget-object v0, Lbj/aB;->a:Lbj/aB;

    .line 92
    iget-object v1, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v1, v1, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v1}, Lbl/c;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v1, v1, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v1}, Lbl/c;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1c

    .line 94
    sget-object v0, Lbj/aB;->b:Lbj/aB;

    .line 97
    :cond_1c
    iget-object v1, p0, Lbj/aA;->c:Lbl/h;

    sget-object v2, Lbl/e;->c:Lbl/e;

    invoke-virtual {v1, v2}, Lbl/h;->a(Lbl/e;)Z

    move-result v1

    if-eqz v1, :cond_40

    iget-object v1, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v1, v1, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v1}, Lbl/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "incorrect"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_40

    iget-object v1, p0, Lbj/aA;->c:Lbl/h;

    invoke-virtual {v1}, Lbl/h;->j()Z

    move-result v1

    if-eqz v1, :cond_40

    .line 100
    sget-object v0, Lbj/aB;->c:Lbj/aB;

    .line 103
    :cond_40
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 108
    iget-object v0, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v0, v0, Lbj/aD;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 110
    iget-object v0, p0, Lbj/aA;->c:Lbl/h;

    iget-object v1, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v1, v1, Lbj/aD;->b:Lbl/c;

    iget-object v2, p0, Lbj/aA;->a:Lbj/ao;

    invoke-static {v2}, Lbj/ao;->a(Lbj/ao;)Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbl/a;->a(Lbl/h;Lbl/c;Lcom/google/googlenav/ai;)V

    .line 113
    invoke-static {}, Lbl/j;->a()Lbl/j;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 114
    iget-object v0, p0, Lbj/aA;->a:Lbj/ao;

    invoke-static {v0}, Lbj/ao;->a(Lbj/ao;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-static {}, Lbl/j;->a()Lbl/j;

    move-result-object v1

    iget-object v2, p0, Lbj/aA;->c:Lbl/h;

    invoke-virtual {v2}, Lbl/h;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbj/aA;->c:Lbl/h;

    invoke-virtual {v3}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v4, v4, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v4}, Lbl/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 122
    :cond_42
    invoke-direct {p0}, Lbj/aA;->a()Lbj/aB;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v1, v1, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v1}, Lbl/c;->e()Z

    move-result v1

    if-eqz v1, :cond_68

    .line 126
    iget-object v1, p0, Lbj/aA;->a:Lbj/ao;

    invoke-static {v1}, Lbj/ao;->b(Lbj/ao;)Lcom/google/googlenav/bh;

    move-result-object v1

    iget-object v2, p0, Lbj/aA;->a:Lbj/ao;

    invoke-static {v2}, Lbj/ao;->a(Lbj/ao;)Lcom/google/googlenav/ai;

    move-result-object v2

    iget-object v3, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v3, v3, Lbj/aD;->b:Lbl/c;

    invoke-virtual {v3}, Lbl/c;->f()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/bh;->b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    .line 130
    :cond_68
    iget-object v1, p0, Lbj/aA;->a:Lbj/ao;

    invoke-static {v1}, Lbj/ao;->a(Lbj/ao;)Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v1

    invoke-virtual {v1}, Lbl/o;->a()V

    .line 133
    iget-object v1, p0, Lbj/aA;->a:Lbj/ao;

    invoke-static {v1}, Lbj/ao;->c(Lbj/ao;)Lbf/aQ;

    move-result-object v1

    invoke-virtual {v1, v5}, Lbf/aQ;->d(Z)V

    .line 135
    sget-object v1, Lbj/az;->a:[I

    invoke-virtual {v0}, Lbj/aB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_c4

    .line 146
    new-instance v0, Lcom/google/googlenav/ui/android/c;

    iget-object v1, p0, Lbj/aA;->d:Lbj/aC;

    iget-object v1, v1, Lbj/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/android/c;-><init>(Landroid/content/Context;)V

    .line 148
    iget-object v1, p0, Lbj/aA;->c:Lbl/h;

    invoke-virtual {v1}, Lbl/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/c;->b(Ljava/lang/String;)V

    .line 151
    :goto_9f
    return-void

    .line 137
    :pswitch_a0
    iget-object v0, p0, Lbj/aA;->a:Lbj/ao;

    iget-object v1, p0, Lbj/aA;->d:Lbj/aC;

    iget-object v1, v1, Lbj/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbj/aA;->c:Lbl/h;

    invoke-static {v0, v1, v2}, Lbj/ao;->a(Lbj/ao;Landroid/content/Context;Lbl/h;)V

    goto :goto_9f

    .line 140
    :pswitch_b0
    iget-object v0, p0, Lbj/aA;->a:Lbj/ao;

    iget-object v1, p0, Lbj/aA;->d:Lbj/aC;

    iget-object v1, v1, Lbj/aC;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbj/aA;->c:Lbl/h;

    iget-object v3, p0, Lbj/aA;->b:Lbj/aD;

    iget-object v3, v3, Lbj/aD;->b:Lbl/c;

    invoke-static {v0, v1, v2, v3}, Lbj/ao;->a(Lbj/ao;Landroid/content/Context;Lbl/h;Lbl/c;)V

    goto :goto_9f

    .line 135
    :pswitch_data_c4
    .packed-switch 0x1
        :pswitch_a0
        :pswitch_b0
    .end packed-switch
.end method
