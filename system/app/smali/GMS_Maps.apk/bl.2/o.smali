.class public Lbl/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lbl/b;

.field b:Lbl/h;

.field private final c:Ljava/util/Set;

.field private d:Z

.field private final e:Ljava/util/Queue;

.field private final f:Ljava/lang/Object;

.field private final g:Lcom/google/googlenav/ai;

.field private final h:Lbl/j;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ai;Lbl/j;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbl/o;->d:Z

    .line 75
    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->e:Ljava/util/Queue;

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbl/o;->f:Ljava/lang/Object;

    .line 92
    iput-object p2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    .line 93
    iput-object p3, p0, Lbl/o;->h:Lbl/j;

    .line 94
    invoke-virtual {p0, p1}, Lbl/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 95
    return-void
.end method

.method static synthetic a(Lbl/o;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 488
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->Y()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 489
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v0

    .line 491
    :goto_14
    return-object v0

    :cond_15
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    goto :goto_14
.end method

.method private a(Lbl/h;Lbl/f;ILbl/e;)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 170
    if-nez p2, :cond_4

    .line 179
    :goto_3
    return v1

    .line 174
    :cond_4
    invoke-virtual {p1}, Lbl/h;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/d;

    .line 175
    iget-object v3, v0, Lbl/d;->a:Lbl/e;

    if-ne v3, p4, :cond_c

    iget v3, v0, Lbl/d;->b:I

    if-lez v3, :cond_c

    .line 176
    iget p3, v0, Lbl/d;->b:I

    goto :goto_c

    .line 179
    :cond_23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lbl/f;->a(J)I

    move-result v0

    if-ge v0, p3, :cond_30

    const/4 v0, 0x1

    :goto_2e
    move v1, v0

    goto :goto_3

    :cond_30
    move v0, v1

    goto :goto_2e
.end method

.method private m()V
    .registers 4

    .prologue
    .line 413
    iget-object v1, p0, Lbl/o;->c:Ljava/util/Set;

    monitor-enter v1

    .line 414
    :try_start_3
    iget-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 415
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_9

    .line 417
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0

    :cond_1c
    :try_start_1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_19

    .line 418
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lbl/o;->b:Lbl/h;

    if-eqz v0, :cond_a

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    .line 127
    invoke-direct {p0}, Lbl/o;->m()V

    .line 129
    :cond_a
    return-void
.end method

.method public a(Lbf/m;)V
    .registers 3
    .parameter

    .prologue
    .line 438
    new-instance v0, Lbl/q;

    invoke-direct {v0, p0, p1}, Lbl/q;-><init>(Lbl/o;Lbf/m;)V

    invoke-virtual {p0, v0}, Lbl/o;->a(Ljava/lang/Runnable;)V

    .line 449
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 103
    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_3
    new-instance v0, Lbl/b;

    invoke-direct {v0, p1}, Lbl/b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    iput-object v0, p0, Lbl/o;->a:Lbl/b;

    .line 105
    invoke-virtual {p0}, Lbl/o;->a()V

    .line 106
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_1d

    .line 111
    if-eqz p1, :cond_1c

    .line 112
    new-instance v0, Lbl/p;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbl/p;-><init>(Lbl/o;Las/c;)V

    invoke-virtual {v0}, Lbl/p;->g()V

    .line 119
    :cond_1c
    return-void

    .line 106
    :catchall_1d
    move-exception v0

    :try_start_1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    throw v0
.end method

.method a(Ljava/lang/Runnable;)V
    .registers 5
    .parameter

    .prologue
    .line 403
    iget-object v1, p0, Lbl/o;->e:Ljava/util/Queue;

    monitor-enter v1

    .line 404
    :try_start_3
    iget-boolean v0, p0, Lbl/o;->d:Z

    if-nez v0, :cond_e

    .line 405
    iget-object v0, p0, Lbl/o;->e:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 409
    :goto_c
    monitor-exit v1

    .line 410
    return-void

    .line 407
    :cond_e
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    goto :goto_c

    .line 409
    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method public b()V
    .registers 5

    .prologue
    .line 138
    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_3
    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 140
    monitor-exit v1

    .line 163
    :goto_a
    return-void

    .line 144
    :cond_b
    invoke-virtual {p0}, Lbl/o;->c()V

    .line 147
    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-nez v0, :cond_17

    .line 148
    invoke-virtual {p0}, Lbl/o;->f()V

    .line 152
    :cond_17
    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-nez v0, :cond_20

    .line 153
    invoke-virtual {p0}, Lbl/o;->i()V

    .line 157
    :cond_20
    invoke-virtual {p0}, Lbl/o;->j()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 158
    invoke-virtual {p0}, Lbl/o;->k()Lbl/h;

    move-result-object v0

    .line 159
    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Lbl/a;->a(Lcom/google/googlenav/ai;Lbl/h;Z)V

    .line 160
    invoke-direct {p0}, Lbl/o;->m()V

    .line 162
    :cond_33
    monitor-exit v1

    goto :goto_a

    :catchall_35
    move-exception v0

    monitor-exit v1
    :try_end_37
    .catchall {:try_start_3 .. :try_end_37} :catchall_35

    throw v0
.end method

.method public b(Lbf/m;)V
    .registers 5
    .parameter

    .prologue
    .line 452
    new-instance v0, Lbl/r;

    invoke-direct {v0, p0, p1}, Lbl/r;-><init>(Lbl/o;Lbf/m;)V

    invoke-virtual {p0, v0}, Lbl/o;->a(Ljava/lang/Runnable;)V

    .line 463
    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    if-eqz v0, :cond_1d

    .line 464
    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    iget-object v1, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbl/j;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_1d
    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .registers 4
    .parameter

    .prologue
    .line 424
    iget-object v1, p0, Lbl/o;->c:Ljava/util/Set;

    monitor-enter v1

    .line 425
    :try_start_3
    iget-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 426
    monitor-exit v1

    .line 427
    return-void

    .line 426
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method c()V
    .registers 7

    .prologue
    .line 189
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v1, Lbl/e;->c:Lbl/e;

    invoke-virtual {v0, v1}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_45

    .line 191
    iget-object v1, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    .line 192
    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v2, v1}, Lbl/j;->a(Ljava/lang/String;)Lbl/g;

    move-result-object v2

    .line 193
    const/16 v3, 0xe

    sget-object v4, Lbl/e;->c:Lbl/e;

    invoke-direct {p0, v0, v2, v3, v4}, Lbl/o;->a(Lbl/h;Lbl/f;ILbl/e;)Z

    move-result v3

    if-eqz v3, :cond_45

    .line 195
    invoke-virtual {v2}, Lbl/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 197
    iget-object v4, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->J()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_46

    iget-object v4, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_46

    .line 199
    invoke-virtual {v0, v2}, Lbl/h;->a(Lbl/g;)V

    .line 200
    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    .line 212
    :cond_45
    :goto_45
    return-void

    .line 201
    :cond_46
    invoke-virtual {v0}, Lbl/h;->j()Z

    move-result v2

    if-eqz v2, :cond_45

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_45

    invoke-virtual {v0}, Lbl/h;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_45

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v2

    if-eqz v2, :cond_45

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v3

    invoke-virtual {v3}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_45

    .line 208
    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    goto :goto_45
.end method

.method public c(Lbf/m;)V
    .registers 4
    .parameter

    .prologue
    .line 470
    new-instance v0, Lbl/s;

    invoke-direct {v0, p0, p1}, Lbl/s;-><init>(Lbl/o;Lbf/m;)V

    invoke-virtual {p0, v0}, Lbl/o;->a(Ljava/lang/Runnable;)V

    .line 481
    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    if-eqz v0, :cond_17

    .line 482
    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    iget-object v1, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbl/j;->c(Ljava/lang/String;)V

    .line 484
    :cond_17
    return-void
.end method

.method public c(Ljava/lang/Runnable;)V
    .registers 4
    .parameter

    .prologue
    .line 430
    iget-object v1, p0, Lbl/o;->c:Ljava/util/Set;

    monitor-enter v1

    .line 431
    :try_start_3
    iget-object v0, p0, Lbl/o;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 432
    monitor-exit v1

    .line 433
    return-void

    .line 432
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public d()Lbl/h;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 220
    iget-object v2, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 221
    :try_start_4
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v3, Lbl/e;->c:Lbl/e;

    invoke-virtual {v0, v3}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_28

    iget-object v3, p0, Lbl/o;->h:Lbl/j;

    iget-object v4, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v4}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_28

    .line 224
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbl/h;->a(Lbl/g;)V

    .line 225
    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    .line 226
    monitor-exit v2

    .line 228
    :goto_27
    return-object v0

    :cond_28
    monitor-exit v2

    move-object v0, v1

    goto :goto_27

    .line 230
    :catchall_2b
    move-exception v0

    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_4 .. :try_end_2d} :catchall_2b

    throw v0
.end method

.method public e()V
    .registers 6

    .prologue
    .line 238
    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_3
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v2, Lbl/e;->c:Lbl/e;

    invoke-virtual {v0, v2}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    .line 240
    if-eqz v0, :cond_2f

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v2

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    iget-object v3, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v3}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v4

    invoke-virtual {v4}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2f

    .line 244
    invoke-virtual {v0}, Lbl/h;->n()Lbl/h;

    move-result-object v0

    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    .line 248
    :cond_2f
    monitor-exit v1

    .line 249
    return-void

    .line 248
    :catchall_31
    move-exception v0

    monitor-exit v1
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_31

    throw v0
.end method

.method f()V
    .registers 5

    .prologue
    .line 257
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v1, Lbl/e;->f:Lbl/e;

    invoke-virtual {v0, v1}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_37

    iget-object v1, p0, Lbl/o;->h:Lbl/j;

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_37

    .line 260
    iget-object v1, p0, Lbl/o;->h:Lbl/j;

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbl/j;->b(Ljava/lang/String;)Lbl/f;

    move-result-object v1

    .line 261
    const/16 v2, 0xe

    sget-object v3, Lbl/e;->f:Lbl/e;

    invoke-direct {p0, v0, v1, v2, v3}, Lbl/o;->a(Lbl/h;Lbl/f;ILbl/e;)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 263
    invoke-virtual {v0, v1}, Lbl/h;->a(Lbl/f;)V

    .line 264
    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    .line 267
    :cond_37
    return-void
.end method

.method public g()Lbl/h;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-object v2, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 276
    :try_start_4
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v3, Lbl/e;->f:Lbl/e;

    invoke-virtual {v0, v3}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_28

    iget-object v3, p0, Lbl/o;->h:Lbl/j;

    iget-object v4, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v4}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_28

    .line 279
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbl/h;->a(Lbl/f;)V

    .line 280
    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    .line 281
    monitor-exit v2

    .line 283
    :goto_27
    return-object v0

    :cond_28
    monitor-exit v2

    move-object v0, v1

    goto :goto_27

    .line 285
    :catchall_2b
    move-exception v0

    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_4 .. :try_end_2d} :catchall_2b

    throw v0
.end method

.method h()Lbl/h;
    .registers 6

    .prologue
    .line 294
    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 295
    :try_start_3
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v2, Lbl/e;->e:Lbl/e;

    invoke-virtual {v0, v2}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_23

    iget-object v2, p0, Lbl/o;->h:Lbl/j;

    iget-object v3, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v3}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 298
    iput-object v0, p0, Lbl/o;->b:Lbl/h;

    .line 299
    monitor-exit v1

    .line 301
    :goto_22
    return-object v0

    :cond_23
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_22

    .line 303
    :catchall_26
    move-exception v0

    monitor-exit v1
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    throw v0
.end method

.method i()V
    .registers 7

    .prologue
    .line 310
    iget-object v0, p0, Lbl/o;->a:Lbl/b;

    sget-object v1, Lbl/e;->d:Lbl/e;

    invoke-virtual {v0, v1}, Lbl/b;->a(Lbl/e;)Lbl/h;

    move-result-object v1

    .line 311
    if-eqz v1, :cond_4e

    iget-object v0, p0, Lbl/o;->h:Lbl/j;

    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-static {v2}, Lbl/o;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lbl/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbl/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4e

    .line 313
    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v0

    .line 314
    iget-object v2, p0, Lbl/o;->g:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v2

    invoke-static {v0, v2}, LaV/g;->a(LaH/m;LaN/B;)I

    move-result v2

    .line 316
    const/4 v0, -0x1

    if-eq v2, v0, :cond_4e

    .line 317
    invoke-virtual {v1}, Lbl/h;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_35
    :goto_35
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/d;

    .line 318
    iget-object v4, v0, Lbl/d;->a:Lbl/e;

    sget-object v5, Lbl/e;->d:Lbl/e;

    if-ne v4, v5, :cond_35

    iget v0, v0, Lbl/d;->b:I

    if-lt v0, v2, :cond_35

    .line 320
    iput-object v1, p0, Lbl/o;->b:Lbl/h;

    goto :goto_35

    .line 325
    :cond_4e
    return-void
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 361
    invoke-virtual {p0}, Lbl/o;->k()Lbl/h;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public k()Lbl/h;
    .registers 3

    .prologue
    .line 365
    iget-object v1, p0, Lbl/o;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 366
    :try_start_3
    iget-object v0, p0, Lbl/o;->b:Lbl/h;

    monitor-exit v1

    return-object v0

    .line 367
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method l()V
    .registers 3

    .prologue
    .line 381
    :goto_0
    iget-object v1, p0, Lbl/o;->e:Ljava/util/Queue;

    monitor-enter v1

    .line 382
    :try_start_3
    iget-object v0, p0, Lbl/o;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 383
    if-nez v0, :cond_15

    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbl/o;->d:Z

    .line 385
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_1a

    .line 393
    invoke-virtual {p0}, Lbl/o;->b()V

    .line 394
    return-void

    .line 387
    :cond_15
    :try_start_15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_1a

    .line 388
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 387
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    throw v0
.end method
