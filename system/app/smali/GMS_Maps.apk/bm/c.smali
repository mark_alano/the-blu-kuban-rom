.class public Lbm/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:J

.field private static volatile c:Ljava/lang/String;

.field private static final e:[C


# instance fields
.field private a:[B

.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 34
    const-wide v0, 0x4c32d9f7ce10f5adL

    sput-wide v0, Lbm/c;->b:J

    .line 40
    const/4 v0, 0x0

    sput-object v0, Lbm/c;->c:Ljava/lang/String;

    .line 361
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lbm/c;->e:[C

    return-void
.end method

.method constructor <init>(J)V
    .registers 3
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-wide p1, p0, Lbm/c;->d:J

    .line 50
    return-void
.end method

.method private static a(C)I
    .registers 3
    .parameter

    .prologue
    .line 363
    const/16 v0, 0x30

    if-lt p0, v0, :cond_b

    const/16 v0, 0x39

    if-gt p0, v0, :cond_b

    .line 364
    add-int/lit8 v0, p0, -0x30

    .line 368
    :goto_a
    return v0

    .line 365
    :cond_b
    const/16 v0, 0x61

    if-lt p0, v0, :cond_18

    const/16 v0, 0x66

    if-gt p0, v0, :cond_18

    .line 366
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_a

    .line 367
    :cond_18
    const/16 v0, 0x41

    if-lt p0, v0, :cond_25

    const/16 v0, 0x46

    if-gt p0, v0, :cond_25

    .line 368
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_a

    .line 370
    :cond_25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "char is not a hex char"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lbm/c;
    .registers 3
    .parameter

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/googlenav/common/Config;->getOrCreateInstance(Landroid/content/Context;)Lcom/google/googlenav/common/Config;

    .line 64
    sget-object v0, Lbm/c;->c:Ljava/lang/String;

    .line 65
    if-eqz v0, :cond_c

    .line 66
    invoke-static {v0}, Lbm/c;->a(Ljava/lang/String;)Lbm/c;

    move-result-object v0

    .line 90
    :goto_b
    return-object v0

    .line 68
    :cond_c
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    const-string v1, "deskdroid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 70
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbm/c;->a(Ljava/lang/String;)Lbm/c;

    move-result-object v0

    goto :goto_b

    .line 84
    :cond_1e
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "logging_id2"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    if-nez v0, :cond_34

    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "logging_id2"

    invoke-static {v0, v1}, Lcom/google/android/gsf/c;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_34
    sput-object v0, Lbm/c;->c:Ljava/lang/String;

    .line 90
    invoke-static {v0}, Lbm/c;->a(Ljava/lang/String;)Lbm/c;

    move-result-object v0

    goto :goto_b
.end method

.method static a(Ljava/lang/String;)Lbm/c;
    .registers 4
    .parameter

    .prologue
    .line 99
    if-nez p0, :cond_a

    .line 100
    new-instance v0, Lbm/c;

    sget-wide v1, Lbm/c;->b:J

    invoke-direct {v0, v1, v2}, Lbm/c;-><init>(J)V

    .line 103
    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lbm/c;

    const/16 v1, 0x10

    invoke-static {p0, v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lbm/c;-><init>(J)V

    goto :goto_9
.end method

.method private a()Ljavax/crypto/spec/SecretKeySpec;
    .registers 6

    .prologue
    .line 245
    iget-object v0, p0, Lbm/c;->a:[B

    if-nez v0, :cond_36

    .line 246
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    .line 247
    const-string v0, "K_PREF"

    invoke-interface {v1, v0}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    .line 248
    if-nez v0, :cond_34

    .line 249
    new-instance v2, Ljava/util/Random;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Random;-><init>(J)V

    .line 250
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 251
    invoke-virtual {v2, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 252
    const-string v2, "K_PREF"

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 253
    invoke-interface {v1}, Lcom/google/googlenav/common/io/j;->a()V

    .line 255
    :cond_34
    iput-object v0, p0, Lbm/c;->a:[B

    .line 258
    :cond_36
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v1, p0, Lbm/c;->a:[B

    const-string v2, "AES"

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method private static a(Ljava/lang/CharSequence;)[B
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 340
    if-nez p0, :cond_5

    .line 341
    const/4 v0, 0x0

    .line 358
    :goto_4
    return-object v0

    .line 343
    :cond_5
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    new-array v2, v1, [B

    .line 344
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_17

    move-object v0, v2

    .line 345
    goto :goto_4

    .line 347
    :cond_17
    aput-byte v0, v2, v0

    .line 348
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    .line 349
    :goto_1f
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v0, v3, :cond_4b

    .line 350
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 351
    rem-int/lit8 v4, v1, 0x2

    if-nez v4, :cond_3d

    .line 352
    shr-int/lit8 v4, v1, 0x1

    invoke-static {v3}, Lbm/c;->a(C)I

    move-result v3

    shl-int/lit8 v3, v3, 0x4

    int-to-byte v3, v3

    aput-byte v3, v2, v4

    .line 356
    :goto_38
    add-int/lit8 v1, v1, 0x1

    .line 349
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 354
    :cond_3d
    shr-int/lit8 v4, v1, 0x1

    aget-byte v5, v2, v4

    invoke-static {v3}, Lbm/c;->a(C)I

    move-result v3

    int-to-byte v3, v3

    add-int/2addr v3, v5

    int-to-byte v3, v3

    aput-byte v3, v2, v4

    goto :goto_38

    :cond_4b
    move-object v0, v2

    .line 358
    goto :goto_4
.end method

.method private b()Ljavax/crypto/spec/SecretKeySpec;
    .registers 12

    .prologue
    const/16 v10, 0x20

    const/16 v9, 0x18

    const/16 v8, 0x10

    const/16 v7, 0x8

    const-wide/16 v5, 0xff

    .line 269
    new-array v0, v10, [B

    const/4 v1, 0x0

    iget-wide v2, p0, Lbm/c;->d:J

    const/4 v4, 0x0

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v7

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v8

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v9

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v10

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x28

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x30

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x38

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x1b

    aput-byte v1, v0, v7

    const/16 v1, 0x9

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x30

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xa

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x28

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xb

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v10

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xc

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v9

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xd

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v8

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xe

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v7

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x2c

    aput-byte v2, v0, v1

    iget-wide v1, p0, Lbm/c;->d:J

    const/16 v3, 0x38

    ushr-long/2addr v1, v3

    and-long/2addr v1, v5

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, v0, v8

    const/16 v1, 0x11

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x30

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x12

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x28

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x13

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v10

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x14

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v9

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x15

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v8

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x16

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v7

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x17

    iget-wide v2, p0, Lbm/c;->d:J

    const/4 v4, 0x0

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-wide v1, p0, Lbm/c;->d:J

    const/4 v3, 0x0

    ushr-long/2addr v1, v3

    and-long/2addr v1, v5

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, v0, v9

    const/16 v1, 0x19

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v7

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x1a

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v8

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x1b

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v9

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x1c

    iget-wide v2, p0, Lbm/c;->d:J

    ushr-long/2addr v2, v10

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x1d

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x28

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x1e

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/16 v1, 0x1f

    iget-wide v2, p0, Lbm/c;->d:J

    const/16 v4, 0x38

    ushr-long/2addr v2, v4

    and-long/2addr v2, v5

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 302
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, v0, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method private static d([B)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 313
    if-nez p0, :cond_4

    .line 314
    const/4 v0, 0x0

    .line 324
    :goto_3
    return-object v0

    .line 316
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 318
    const/4 v0, 0x0

    :goto_d
    array-length v2, p0

    if-ge v0, v2, :cond_2b

    .line 319
    aget-byte v2, p0, v0

    ushr-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, 0xf

    .line 320
    aget-byte v3, p0, v0

    and-int/lit8 v3, v3, 0xf

    .line 321
    sget-object v4, Lbm/c;->e:[C

    aget-char v2, v4, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 322
    sget-object v2, Lbm/c;->e:[C

    aget-char v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 318
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 324
    :cond_2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method public a([B)[B
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 120
    if-eqz p1, :cond_6

    array-length v1, p1

    if-nez v1, :cond_7

    .line 130
    :cond_6
    :goto_6
    return-object v0

    .line 125
    :cond_7
    :try_start_7
    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 126
    const/4 v2, 0x1

    invoke-direct {p0}, Lbm/c;->a()Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 127
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_18
    .catch Ljava/security/GeneralSecurityException; {:try_start_7 .. :try_end_18} :catch_1a

    move-result-object v0

    goto :goto_6

    .line 128
    :catch_1a
    move-exception v1

    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "encrypt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6
.end method

.method public b(Ljava/lang/String;)[B
    .registers 3
    .parameter

    .prologue
    .line 140
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 141
    :cond_8
    const/4 v0, 0x0

    .line 143
    :goto_9
    return-object v0

    :cond_a
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lbm/c;->a([B)[B

    move-result-object v0

    goto :goto_9
.end method

.method public b([B)[B
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 176
    if-eqz p1, :cond_6

    array-length v1, p1

    if-nez v1, :cond_7

    .line 194
    :cond_6
    :goto_6
    return-object v0

    .line 181
    :cond_7
    :try_start_7
    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 182
    const/4 v2, 0x2

    invoke-direct {p0}, Lbm/c;->a()Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 183
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_18
    .catch Ljava/security/GeneralSecurityException; {:try_start_7 .. :try_end_18} :catch_1a

    move-result-object v0

    goto :goto_6

    .line 184
    :catch_1a
    move-exception v1

    .line 189
    :try_start_1b
    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 190
    const/4 v2, 0x2

    invoke-direct {p0}, Lbm/c;->b()Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 191
    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_2c
    .catch Ljava/security/GeneralSecurityException; {:try_start_1b .. :try_end_2c} :catch_2e

    move-result-object v0

    goto :goto_6

    .line 192
    :catch_2e
    move-exception v1

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decrypt migration"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lbm/c;->b(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbm/c;->d([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c([B)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 237
    invoke-virtual {p0, p1}, Lbm/c;->b([B)[B

    move-result-object v1

    .line 238
    if-nez v1, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_7
.end method

.method public d(Ljava/lang/String;)[B
    .registers 3
    .parameter

    .prologue
    .line 209
    :try_start_0
    invoke-static {p1}, Lbm/c;->a(Ljava/lang/CharSequence;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lbm/c;->b([B)[B
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    .line 212
    :goto_8
    return-object v0

    .line 210
    :catch_9
    move-exception v0

    .line 212
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lbm/c;->d(Ljava/lang/String;)[B

    move-result-object v1

    .line 226
    if-nez v1, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_7
.end method
