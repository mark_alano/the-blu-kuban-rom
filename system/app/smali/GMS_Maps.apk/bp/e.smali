.class public final enum Lbp/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbp/e;

.field public static final enum b:Lbp/e;

.field public static final enum c:Lbp/e;

.field public static final enum d:Lbp/e;

.field private static final synthetic g:[Lbp/e;


# instance fields
.field private final e:F

.field private final f:F


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x3f4ccccd

    .line 12
    new-instance v0, Lbp/e;

    const-string v1, "LIGHTNING_FREQUENT"

    const v2, 0x3f7d70a4

    invoke-direct {v0, v1, v4, v2, v3}, Lbp/e;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lbp/e;->a:Lbp/e;

    .line 13
    new-instance v0, Lbp/e;

    const-string v1, "LIGHTNING_NORMAL"

    const v2, 0x3f7fbe77

    invoke-direct {v0, v1, v5, v2, v3}, Lbp/e;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lbp/e;->b:Lbp/e;

    .line 14
    new-instance v0, Lbp/e;

    const-string v1, "LIGHTNING_RARE"

    const v2, 0x3f7ff972

    invoke-direct {v0, v1, v6, v2, v3}, Lbp/e;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lbp/e;->c:Lbp/e;

    .line 17
    new-instance v0, Lbp/e;

    const-string v1, "LIGHTNING_NONE"

    const/high16 v2, 0x3f80

    const/4 v3, 0x0

    invoke-direct {v0, v1, v7, v2, v3}, Lbp/e;-><init>(Ljava/lang/String;IFF)V

    sput-object v0, Lbp/e;->d:Lbp/e;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lbp/e;

    sget-object v1, Lbp/e;->a:Lbp/e;

    aput-object v1, v0, v4

    sget-object v1, Lbp/e;->b:Lbp/e;

    aput-object v1, v0, v5

    sget-object v1, Lbp/e;->c:Lbp/e;

    aput-object v1, v0, v6

    sget-object v1, Lbp/e;->d:Lbp/e;

    aput-object v1, v0, v7

    sput-object v0, Lbp/e;->g:[Lbp/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFF)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p4, p0, Lbp/e;->f:F

    .line 30
    iput p3, p0, Lbp/e;->e:F

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbp/e;
    .registers 2
    .parameter

    .prologue
    .line 10
    const-class v0, Lbp/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbp/e;

    return-object v0
.end method

.method public static values()[Lbp/e;
    .registers 1

    .prologue
    .line 10
    sget-object v0, Lbp/e;->g:[Lbp/e;

    invoke-virtual {v0}, [Lbp/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbp/e;

    return-object v0
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Lbp/e;->f:F

    return v0
.end method
