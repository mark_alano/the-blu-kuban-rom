.class public final enum Lbs/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbs/c;

.field public static final enum b:Lbs/c;

.field public static final enum c:Lbs/c;

.field private static final synthetic e:[Lbs/c;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lbs/c;

    const-string v1, "RAIN_LIGHT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v3, v2}, Lbs/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbs/c;->a:Lbs/c;

    .line 14
    new-instance v0, Lbs/c;

    const-string v1, "RAIN_NORMAL"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v4, v2}, Lbs/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbs/c;->b:Lbs/c;

    .line 15
    new-instance v0, Lbs/c;

    const-string v1, "RAIN_HEAVY"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v5, v2}, Lbs/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbs/c;->c:Lbs/c;

    .line 11
    const/4 v0, 0x3

    new-array v0, v0, [Lbs/c;

    sget-object v1, Lbs/c;->a:Lbs/c;

    aput-object v1, v0, v3

    sget-object v1, Lbs/c;->b:Lbs/c;

    aput-object v1, v0, v4

    sget-object v1, Lbs/c;->c:Lbs/c;

    aput-object v1, v0, v5

    sput-object v0, Lbs/c;->e:[Lbs/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lbs/c;->d:I

    .line 21
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbs/c;
    .registers 2
    .parameter

    .prologue
    .line 11
    const-class v0, Lbs/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbs/c;

    return-object v0
.end method

.method public static values()[Lbs/c;
    .registers 1

    .prologue
    .line 11
    sget-object v0, Lbs/c;->e:[Lbs/c;

    invoke-virtual {v0}, [Lbs/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbs/c;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 24
    iget v0, p0, Lbs/c;->d:I

    return v0
.end method
