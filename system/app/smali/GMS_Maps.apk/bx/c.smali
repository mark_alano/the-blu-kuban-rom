.class public Lbx/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbx/g;


# instance fields
.field private A:I

.field private B:I

.field private C:F

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private H:I

.field private I:I

.field private J:F

.field private K:Z

.field private L:I

.field private M:J

.field private N:J

.field public a:Lbx/a;

.field protected b:I

.field protected c:I

.field protected d:Lbw/b;

.field protected e:Lbw/b;

.field private f:[F

.field private g:[F

.field private h:J

.field private i:J

.field private j:Lbx/f;

.field private k:J

.field private l:J

.field private m:J

.field private n:F

.field private o:I

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:Lbx/b;

.field private v:J

.field private w:Lbw/b;

.field private x:[Lbx/b;

.field private y:I

.field private z:F


# direct methods
.method public constructor <init>()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-array v0, v1, [F

    iput-object v0, p0, Lbx/c;->f:[F

    .line 37
    new-array v0, v1, [F

    fill-array-data v0, :array_90

    iput-object v0, p0, Lbx/c;->g:[F

    .line 38
    iput-wide v4, p0, Lbx/c;->h:J

    .line 39
    iput-wide v4, p0, Lbx/c;->i:J

    .line 48
    const-wide/32 v0, 0x3b9aca00

    iput-wide v0, p0, Lbx/c;->k:J

    .line 51
    const-wide/32 v0, 0xf4240

    iput-wide v0, p0, Lbx/c;->l:J

    .line 54
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lbx/c;->m:J

    .line 60
    iput v3, p0, Lbx/c;->b:I

    .line 61
    iput v3, p0, Lbx/c;->c:I

    .line 72
    iput v2, p0, Lbx/c;->n:F

    .line 75
    iput v3, p0, Lbx/c;->o:I

    .line 76
    iput v2, p0, Lbx/c;->p:F

    .line 77
    iput v2, p0, Lbx/c;->q:F

    .line 78
    iput v2, p0, Lbx/c;->r:F

    .line 79
    iput v2, p0, Lbx/c;->s:F

    .line 80
    iput v2, p0, Lbx/c;->t:F

    .line 85
    iput-object v6, p0, Lbx/c;->u:Lbx/b;

    .line 87
    iput-object v6, p0, Lbx/c;->w:Lbw/b;

    .line 91
    const/16 v0, 0x12c

    iput v0, p0, Lbx/c;->y:I

    .line 103
    const v0, 0x3dcccccd

    iput v0, p0, Lbx/c;->z:F

    .line 109
    const/16 v0, 0x1770

    iput v0, p0, Lbx/c;->A:I

    .line 110
    const/16 v0, 0x3a98

    iput v0, p0, Lbx/c;->B:I

    .line 116
    const/high16 v0, 0x3f40

    iput v0, p0, Lbx/c;->C:F

    .line 117
    const v0, 0x3ee66666

    iput v0, p0, Lbx/c;->D:F

    .line 118
    const v0, 0x3e99999a

    iput v0, p0, Lbx/c;->E:F

    .line 124
    const v0, 0x45bb8000

    iput v0, p0, Lbx/c;->F:F

    .line 125
    const v0, 0x466a6000

    iput v0, p0, Lbx/c;->G:F

    .line 130
    const v0, 0x7a120

    iput v0, p0, Lbx/c;->H:I

    .line 131
    const v0, 0x927c0

    iput v0, p0, Lbx/c;->I:I

    .line 137
    const v0, 0x3f19999a

    iput v0, p0, Lbx/c;->J:F

    .line 143
    iput-boolean v3, p0, Lbx/c;->K:Z

    .line 151
    const/16 v0, 0x3e8

    iput v0, p0, Lbx/c;->L:I

    .line 157
    const-wide/16 v0, 0x78

    iget-wide v2, p0, Lbx/c;->m:J

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbx/c;->M:J

    .line 163
    iput-wide v4, p0, Lbx/c;->N:J

    .line 166
    new-instance v0, Lbx/e;

    invoke-direct {v0}, Lbx/e;-><init>()V

    iput-object v0, p0, Lbx/c;->j:Lbx/f;

    .line 167
    iget-object v0, p0, Lbx/c;->j:Lbx/f;

    invoke-interface {v0, p0}, Lbx/f;->a(Lbx/g;)V

    .line 168
    return-void

    .line 37
    :array_90
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public constructor <init>(FIFIZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    invoke-direct {p0}, Lbx/c;-><init>()V

    .line 173
    iput p1, p0, Lbx/c;->z:F

    .line 174
    iput p2, p0, Lbx/c;->A:I

    .line 175
    iput p3, p0, Lbx/c;->C:F

    .line 176
    iput p4, p0, Lbx/c;->y:I

    .line 177
    iput-boolean p5, p0, Lbx/c;->K:Z

    .line 178
    return-void
.end method

.method private a(Lbw/b;)V
    .registers 11
    .parameter

    .prologue
    const v3, 0x4b189680

    const/high16 v5, 0x4040

    .line 581
    iget v0, p1, Lbw/b;->b:I

    iput v0, p0, Lbx/c;->b:I

    .line 582
    iget v0, p1, Lbw/b;->c:I

    iput v0, p0, Lbx/c;->c:I

    .line 583
    iget v0, p1, Lbw/b;->b:I

    iget v1, p0, Lbx/c;->b:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const v1, 0x47d90a8f

    mul-float/2addr v0, v1

    div-float v4, v0, v3

    .line 586
    iget v0, p1, Lbw/b;->c:I

    iget v1, p0, Lbx/c;->c:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lbx/c;->b:I

    int-to-double v1, v1

    invoke-static {v1, v2}, Lbw/a;->b(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    div-float v2, v0, v3

    .line 589
    new-instance v0, Lbx/a;

    sub-float v1, v2, v5

    add-float/2addr v2, v5

    sub-float v3, v4, v5

    add-float/2addr v4, v5

    const-wide/high16 v5, 0x3ff0

    iget v7, p0, Lbx/c;->y:I

    invoke-direct/range {v0 .. v7}, Lbx/a;-><init>(FFFFDI)V

    iput-object v0, p0, Lbx/c;->a:Lbx/a;

    .line 591
    new-instance v0, Lbw/b;

    iget v1, p1, Lbw/b;->b:I

    iget v2, p1, Lbw/b;->c:I

    iget v3, p1, Lbw/b;->d:I

    iget-object v4, p1, Lbw/b;->f:Ljava/lang/String;

    iget-object v5, p1, Lbw/b;->g:Ljava/lang/String;

    iget v6, p1, Lbw/b;->h:I

    iget v7, p1, Lbw/b;->e:F

    iget-object v8, p1, Lbw/b;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lbw/b;-><init>(IIILjava/lang/String;Ljava/lang/String;IFLjava/lang/String;)V

    iput-object v0, p0, Lbx/c;->d:Lbw/b;

    .line 594
    return-void
.end method

.method private a(Lbw/b;F)V
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    iget-object v0, v0, Lbx/a;->i:[Lbx/b;

    array-length v1, v0

    .line 222
    const/4 v0, 0x0

    :goto_6
    if-ge v0, v1, :cond_5e

    .line 223
    iget-object v2, p0, Lbx/c;->a:Lbx/a;

    iget-object v2, v2, Lbx/a;->i:[Lbx/b;

    aget-object v2, v2, v0

    .line 224
    invoke-virtual {p0, v2}, Lbx/c;->a(Lbx/b;)Lbw/b;

    move-result-object v3

    .line 225
    iget v4, v3, Lbw/b;->b:I

    iget v5, p1, Lbw/b;->b:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    .line 226
    iget v3, v3, Lbw/b;->c:I

    iget v6, p1, Lbw/b;->c:I

    sub-int/2addr v3, v6

    int-to-double v6, v3

    .line 227
    mul-double v3, v4, v4

    mul-double v5, v6, v6

    add-double/2addr v3, v5

    .line 228
    iget v5, v2, Lbx/b;->d:F

    iget v6, p0, Lbx/c;->n:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-double v5, v5

    .line 229
    const-wide v7, 0x400921fb54442d18L

    cmpl-double v7, v5, v7

    if-lez v7, :cond_3d

    .line 230
    const-wide v7, 0x401921fb54442d18L

    sub-double v5, v7, v5

    .line 232
    :cond_3d
    iget v5, v2, Lbx/b;->e:F

    float-to-double v5, v5

    float-to-double v7, p2

    const-wide/high16 v9, -0x4010

    mul-double/2addr v3, v9

    iget v9, p0, Lbx/c;->H:I

    int-to-float v9, v9

    iget-object v10, p0, Lbx/c;->d:Lbw/b;

    iget v10, v10, Lbw/b;->d:I

    int-to-float v10, v10

    iget v11, p0, Lbx/c;->F:F

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    float-to-double v9, v9

    div-double/2addr v3, v9

    invoke-static {v3, v4}, Ljava/lang/Math;->exp(D)D

    move-result-wide v3

    mul-double/2addr v3, v7

    add-double/2addr v3, v5

    double-to-float v3, v3

    iput v3, v2, Lbx/b;->e:F

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 237
    :cond_5e
    return-void
.end method

.method private b(Lbw/b;)V
    .registers 11
    .parameter

    .prologue
    .line 602
    iget-object v0, p0, Lbx/c;->d:Lbw/b;

    if-eqz v0, :cond_10

    .line 603
    iget v0, p0, Lbx/c;->r:F

    iget-object v1, p0, Lbx/c;->d:Lbw/b;

    invoke-virtual {p1, v1}, Lbw/b;->a(Lbw/b;)D

    move-result-wide v1

    double-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lbx/c;->r:F

    .line 605
    :cond_10
    new-instance v0, Lbw/b;

    iget v1, p1, Lbw/b;->b:I

    iget v2, p1, Lbw/b;->c:I

    iget v3, p1, Lbw/b;->d:I

    iget-object v4, p1, Lbw/b;->f:Ljava/lang/String;

    iget-object v5, p1, Lbw/b;->g:Ljava/lang/String;

    iget v6, p1, Lbw/b;->h:I

    iget v7, p1, Lbw/b;->e:F

    iget-object v8, p1, Lbw/b;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lbw/b;-><init>(IIILjava/lang/String;Ljava/lang/String;IFLjava/lang/String;)V

    iput-object v0, p0, Lbx/c;->d:Lbw/b;

    .line 608
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    invoke-virtual {v0}, Lbx/a;->a()Lbx/b;

    move-result-object v0

    .line 609
    invoke-virtual {p0, v0}, Lbx/c;->a(Lbx/b;)Lbw/b;

    move-result-object v1

    iput-object v1, p0, Lbx/c;->e:Lbw/b;

    .line 611
    iget-object v1, p0, Lbx/c;->u:Lbx/b;

    if-eqz v1, :cond_4b

    .line 612
    iget-object v1, p0, Lbx/c;->u:Lbx/b;

    invoke-virtual {p0, v1}, Lbx/c;->a(Lbx/b;)Lbw/b;

    move-result-object v1

    .line 613
    iget v2, p0, Lbx/c;->s:F

    float-to-double v2, v2

    iget-object v4, p0, Lbx/c;->e:Lbw/b;

    invoke-virtual {v1, v4}, Lbw/b;->a(Lbw/b;)D

    move-result-wide v4

    add-double v1, v2, v4

    double-to-float v1, v1

    iput v1, p0, Lbx/c;->s:F

    .line 615
    :cond_4b
    iput-object v0, p0, Lbx/c;->u:Lbx/b;

    .line 617
    iget-object v1, p0, Lbx/c;->a:Lbx/a;

    iget v0, v0, Lbx/b;->d:F

    iput v0, v1, Lbx/a;->h:F

    .line 618
    return-void
.end method

.method private b(Lbw/b;F)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 624
    const/4 v0, 0x0

    :goto_1
    int-to-float v1, v0

    iget-object v2, p0, Lbx/c;->a:Lbx/a;

    iget v2, v2, Lbx/a;->a:I

    int-to-float v2, v2

    mul-float/2addr v2, p2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_43

    .line 625
    iget-object v1, p0, Lbx/c;->a:Lbx/a;

    iget-object v1, v1, Lbx/a;->i:[Lbx/b;

    aget-object v1, v1, v0

    iget v2, p1, Lbw/b;->c:I

    iget v3, p0, Lbx/c;->c:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    iget v4, p0, Lbx/c;->b:I

    int-to-double v4, v4

    invoke-static {v4, v5}, Lbw/a;->b(D)D

    move-result-wide v4

    double-to-float v4, v4

    float-to-double v4, v4

    const-wide v6, 0x416312d000000000L

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, v1, Lbx/b;->a:F

    .line 628
    iget-object v1, p0, Lbx/c;->a:Lbx/a;

    iget-object v1, v1, Lbx/a;->i:[Lbx/b;

    aget-object v1, v1, v0

    iget v2, p1, Lbw/b;->b:I

    iget v3, p0, Lbx/c;->b:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    const-wide v4, 0x3f86c22813448063L

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, v1, Lbx/b;->b:F

    .line 624
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 632
    :cond_43
    return-void
.end method


# virtual methods
.method a(Lbw/b;JJJJ)F
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 744
    sub-long v2, p8, p2

    long-to-double v2, v2

    const-wide v4, 0x41cdcd6500000000L

    div-double v10, v2, v4

    .line 745
    const-wide/16 v2, 0x0

    cmpl-double v2, v10, v2

    if-nez v2, :cond_12

    .line 746
    const/4 v2, 0x0

    .line 750
    :goto_11
    return v2

    .line 748
    :cond_12
    iget v2, p1, Lbw/b;->b:I

    int-to-double v2, v2

    iget v4, p1, Lbw/b;->c:I

    int-to-double v4, v4

    move-wide/from16 v0, p4

    long-to-double v6, v0

    move-wide/from16 v0, p6

    long-to-double v8, v0

    invoke-static/range {v2 .. v9}, Lbw/a;->a(DDDD)D

    move-result-wide v2

    .line 750
    div-double/2addr v2, v10

    double-to-float v2, v2

    goto :goto_11
.end method

.method public a(Lbx/b;)Lbw/b;
    .registers 8
    .parameter

    .prologue
    .line 302
    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbx/c;->a(Lbx/b;Lbw/b;JZ)Lbw/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbx/b;Lbw/b;JZ)Lbw/b;
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 319
    if-eqz p5, :cond_44

    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    invoke-virtual {v0, p1}, Lbx/a;->a(Lbx/b;)I

    move-result v0

    move v10, v0

    .line 321
    :goto_9
    iget v0, p0, Lbx/c;->b:I

    iget v1, p1, Lbx/b;->b:F

    float-to-double v1, v1

    const-wide v3, 0x40567f46328ec073L

    mul-double/2addr v1, v3

    double-to-int v1, v1

    add-int v11, v0, v1

    .line 322
    iget v0, p0, Lbx/c;->c:I

    iget v1, p1, Lbx/b;->a:F

    float-to-double v1, v1

    const-wide v3, 0x416312d000000000L

    iget v5, p0, Lbx/c;->b:I

    int-to-double v5, v5

    invoke-static {v5, v6}, Lbw/a;->b(D)D

    move-result-wide v5

    div-double/2addr v3, v5

    mul-double/2addr v1, v3

    double-to-int v1, v1

    add-int v12, v0, v1

    .line 325
    if-nez p2, :cond_47

    const/4 v7, 0x0

    .line 327
    :goto_30
    iget-object v0, p0, Lbx/c;->d:Lbw/b;

    if-nez v0, :cond_54

    const-string v8, "wifi"

    .line 335
    :goto_36
    new-instance v0, Lbw/b;

    const-string v4, ""

    const-string v5, ""

    const/4 v6, -0x1

    move v1, v11

    move v2, v12

    move v3, v10

    invoke-direct/range {v0 .. v8}, Lbw/b;-><init>(IIILjava/lang/String;Ljava/lang/String;IFLjava/lang/String;)V

    return-object v0

    .line 319
    :cond_44
    const/4 v0, -0x1

    move v10, v0

    goto :goto_9

    .line 325
    :cond_47
    iget-wide v2, p0, Lbx/c;->v:J

    int-to-long v4, v11

    int-to-long v6, v12

    move-object v0, p0

    move-object v1, p2

    move-wide/from16 v8, p3

    invoke-virtual/range {v0 .. v9}, Lbx/c;->a(Lbw/b;JJJJ)F

    move-result v7

    goto :goto_30

    .line 327
    :cond_54
    iget-object v0, p0, Lbx/c;->d:Lbw/b;

    iget-object v8, v0, Lbw/b;->a:Ljava/lang/String;

    goto :goto_36
.end method

.method public a(F)V
    .registers 2
    .parameter

    .prologue
    .line 293
    return-void
.end method

.method public a(J)V
    .registers 9
    .parameter

    .prologue
    .line 206
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    const v1, 0x3f19999a

    invoke-virtual {v0, v1}, Lbx/a;->b(F)V

    .line 209
    const-wide/high16 v0, 0x3fe0

    iget-wide v2, p0, Lbx/c;->i:J

    sub-long v2, p1, v2

    iget-wide v4, p0, Lbx/c;->k:J

    div-long/2addr v2, v4

    long-to-int v2, v2

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 211
    iget-object v1, p0, Lbx/c;->d:Lbw/b;

    invoke-direct {p0, v1, v0}, Lbx/c;->a(Lbw/b;F)V

    .line 212
    iget v0, p0, Lbx/c;->n:F

    invoke-virtual {p0, v0}, Lbx/c;->a(F)V

    .line 213
    return-void
.end method

.method public a(JFFF)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    if-eqz v0, :cond_7e

    .line 186
    iget-object v0, p0, Lbx/c;->j:Lbx/f;

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lbx/f;->a(JFFF)V

    .line 189
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 190
    const/4 v1, 0x0

    aput p3, v0, v1

    .line 191
    const/4 v1, 0x1

    aput p4, v0, v1

    .line 192
    const/4 v1, 0x2

    aput p5, v0, v1

    .line 193
    invoke-static {v0}, Lbx/d;->c([F)[F

    move-result-object v0

    iput-object v0, p0, Lbx/c;->f:[F

    .line 194
    iget-object v0, p0, Lbx/c;->g:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    const/high16 v1, 0x3f80

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2e

    .line 195
    iget-object v0, p0, Lbx/c;->f:[F

    iput-object v0, p0, Lbx/c;->g:[F

    .line 197
    :cond_2e
    iget-object v0, p0, Lbx/c;->g:[F

    const/4 v1, 0x0

    const v2, 0x3f4ccccd

    iget-object v3, p0, Lbx/c;->g:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd

    iget-object v4, p0, Lbx/c;->f:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 198
    iget-object v0, p0, Lbx/c;->g:[F

    const/4 v1, 0x1

    const v2, 0x3f4ccccd

    iget-object v3, p0, Lbx/c;->g:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd

    iget-object v4, p0, Lbx/c;->f:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 199
    iget-object v0, p0, Lbx/c;->g:[F

    const/4 v1, 0x2

    const v2, 0x3f4ccccd

    iget-object v3, p0, Lbx/c;->g:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v2, v3

    const v3, 0x3e4ccccd

    iget-object v4, p0, Lbx/c;->f:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 200
    iget-object v0, p0, Lbx/c;->g:[F

    invoke-static {v0}, Lbx/d;->c([F)[F

    move-result-object v0

    iput-object v0, p0, Lbx/c;->g:[F

    .line 202
    :cond_7e
    return-void
.end method

.method public a(JLbw/b;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 404
    iget-wide v0, p0, Lbx/c;->N:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_16

    iget-wide v0, p0, Lbx/c;->l:J

    div-long v0, p1, v0

    iget-wide v2, p0, Lbx/c;->N:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lbx/c;->M:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_16

    .line 439
    :cond_15
    :goto_15
    return-void

    .line 408
    :cond_16
    iput-wide p1, p0, Lbx/c;->i:J

    .line 412
    if-eqz p3, :cond_15

    iget v0, p3, Lbw/b;->d:I

    if-lez v0, :cond_15

    .line 413
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    if-nez v0, :cond_26

    .line 414
    invoke-direct {p0, p3}, Lbx/c;->a(Lbw/b;)V

    goto :goto_15

    .line 416
    :cond_26
    const/high16 v0, 0x3f80

    invoke-direct {p0, p3, v0}, Lbx/c;->a(Lbw/b;F)V

    .line 418
    invoke-direct {p0, p3}, Lbx/c;->b(Lbw/b;)V

    .line 420
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    invoke-virtual {v0}, Lbx/a;->b()V

    .line 422
    iget v0, p0, Lbx/c;->n:F

    invoke-virtual {p0, v0}, Lbx/c;->a(F)V

    .line 425
    iget v0, p0, Lbx/c;->z:F

    .line 433
    iget v1, p3, Lbw/b;->d:I

    iget v2, p0, Lbx/c;->A:I

    if-ge v1, v2, :cond_42

    .line 434
    iget v0, p0, Lbx/c;->C:F

    .line 436
    :cond_42
    invoke-direct {p0, p3, v0}, Lbx/c;->b(Lbw/b;F)V

    goto :goto_15
.end method

.method public b(J)Lbw/b;
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 666
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    if-eqz v0, :cond_36

    .line 667
    iget-object v0, p0, Lbx/c;->a:Lbx/a;

    invoke-virtual {v0}, Lbx/a;->a()Lbx/b;

    move-result-object v1

    .line 668
    iget-object v2, p0, Lbx/c;->w:Lbw/b;

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v3, p1

    invoke-virtual/range {v0 .. v5}, Lbx/c;->a(Lbx/b;Lbw/b;JZ)Lbw/b;

    move-result-object v0

    .line 669
    iput-object v0, p0, Lbx/c;->w:Lbw/b;

    .line 670
    iput-wide p1, p0, Lbx/c;->v:J

    .line 671
    iget-boolean v1, p0, Lbx/c;->K:Z

    if-eqz v1, :cond_35

    .line 672
    iget-object v1, p0, Lbx/c;->x:[Lbx/b;

    if-nez v1, :cond_28

    .line 673
    iget-object v1, p0, Lbx/c;->a:Lbx/a;

    iget v1, v1, Lbx/a;->a:I

    new-array v1, v1, [Lbx/b;

    iput-object v1, p0, Lbx/c;->x:[Lbx/b;

    .line 675
    :cond_28
    iget-object v1, p0, Lbx/c;->a:Lbx/a;

    iget-object v1, v1, Lbx/a;->i:[Lbx/b;

    iget-object v2, p0, Lbx/c;->x:[Lbx/b;

    iget-object v3, p0, Lbx/c;->a:Lbx/a;

    iget v3, v3, Lbx/a;->a:I

    invoke-static {v1, v6, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 679
    :cond_35
    :goto_35
    return-object v0

    :cond_36
    const/4 v0, 0x0

    goto :goto_35
.end method
