.class public Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;
.super Landroid/support/v4/app/f;
.source "SourceFile"


# static fields
.field private static P:Lcom/google/commerce/wireless/topiary/D;

.field private static final ac:Lcom/google/commerce/wireless/topiary/T;

.field private static final ad:Lcom/google/commerce/wireless/topiary/T;

.field private static final ae:Lcom/google/commerce/wireless/topiary/T;

.field private static final af:Lcom/google/commerce/wireless/topiary/T;


# instance fields
.field protected N:Lcom/google/android/apps/common/offerslib/d;

.field protected O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

.field private final Q:Lcom/google/android/apps/common/offerslib/i;

.field private R:Lcom/google/commerce/wireless/topiary/HybridWebView;

.field private S:Lcom/google/android/apps/common/offerslib/c;

.field private T:Ljava/lang/String;

.field private U:Lcom/google/android/apps/common/offerslib/a;

.field private V:Landroid/accounts/Account;

.field private W:Lcom/google/android/apps/common/offerslib/v;

.field private X:Ljava/lang/String;

.field private Y:Landroid/widget/RelativeLayout;

.field private Z:Landroid/view/View;

.field private aa:Landroid/os/Handler;

.field private ab:I

.field private ag:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 107
    invoke-static {v2, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ac:Lcom/google/commerce/wireless/topiary/T;

    .line 109
    invoke-static {v2, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ad:Lcom/google/commerce/wireless/topiary/T;

    .line 111
    invoke-static {v1, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ae:Lcom/google/commerce/wireless/topiary/T;

    .line 113
    invoke-static {v1, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ZZ)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->af:Lcom/google/commerce/wireless/topiary/T;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v4/app/f;-><init>()V

    .line 70
    new-instance v0, Lcom/google/android/apps/common/offerslib/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/common/offerslib/i;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Lcom/google/android/apps/common/offerslib/f;)V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Q:Lcom/google/android/apps/common/offerslib/i;

    .line 103
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->aa:Landroid/os/Handler;

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    .line 1060
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->aa:Landroid/os/Handler;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 265
    if-nez p0, :cond_4

    .line 266
    const/4 v0, 0x0

    .line 272
    :goto_3
    return-object v0

    .line 268
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/a;->g()Z

    move-result v0

    .line 269
    if-eqz v0, :cond_12

    .line 270
    if-eqz p1, :cond_f

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ad:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_3

    :cond_f
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->af:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_3

    .line 272
    :cond_12
    if-eqz p1, :cond_17

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ac:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_3

    :cond_17
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ae:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_3
.end method

.method private static a(ZZ)Lcom/google/commerce/wireless/topiary/T;
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 281
    new-instance v0, Lcom/google/commerce/wireless/topiary/T;

    if-eqz p1, :cond_33

    const-string v1, "weblogin:service=sierrasandbox"

    :goto_7
    if-eqz p1, :cond_36

    const-string v2, "https://sandbox.google.com/checkout"

    :goto_b
    sget-object v5, Lcom/google/commerce/wireless/topiary/V;->b:Lcom/google/commerce/wireless/topiary/V;

    new-instance v6, Ljava/util/ArrayList;

    new-array v9, v3, [Lcom/google/commerce/wireless/topiary/U;

    const/4 v10, 0x0

    new-instance v11, Lcom/google/commerce/wireless/topiary/U;

    if-eqz p1, :cond_39

    const-string v4, "https://sandbox.google.com/checkout"

    move-object v8, v4

    :goto_19
    if-eqz p0, :cond_3d

    const-string v4, "FSS"

    move-object v7, v4

    :goto_1e
    if-eqz p0, :cond_41

    const/16 v4, 0x708

    :goto_22
    invoke-direct {v11, v8, v7, v4}, Lcom/google/commerce/wireless/topiary/U;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v11, v9, v10

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/commerce/wireless/topiary/T;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/commerce/wireless/topiary/V;Ljava/util/List;)V

    return-object v0

    :cond_33
    const-string v1, "Google Checkout"

    goto :goto_7

    :cond_36
    const-string v2, "https://checkout.google.com"

    goto :goto_b

    :cond_39
    const-string v4, "https://checkout.google.com"

    move-object v8, v4

    goto :goto_19

    :cond_3d
    const-string v4, "SSID"

    move-object v7, v4

    goto :goto_1e

    :cond_41
    const v4, 0x127500

    goto :goto_22
.end method

.method private a(ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1140
    packed-switch p1, :pswitch_data_32

    .line 1156
    :goto_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1157
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->c:Ljava/lang/String;

    .line 1159
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/apps/common/offerslib/d;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;ILjava/lang/String;)V

    .line 1160
    return-void

    .line 1142
    :pswitch_17
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->b:Ljava/lang/String;

    goto :goto_3

    .line 1145
    :pswitch_20
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->a:Ljava/lang/String;

    goto :goto_3

    .line 1148
    :pswitch_29
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->d()Lcom/google/android/apps/common/offerslib/b;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/apps/common/offerslib/b;->c:Ljava/lang/String;

    goto :goto_3

    .line 1140
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_17
        :pswitch_20
        :pswitch_29
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/common/offerslib/a;Landroid/accounts/Account;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 169
    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->a()V

    .line 172
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v0, :cond_d

    .line 173
    invoke-static {p0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    .line 177
    :cond_d
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    .line 179
    invoke-static {p0, p2, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v1

    if-nez v1, :cond_36

    .line 180
    const-string v1, "OfferDetailsFragment"

    const-string v2, "Clearing cookies and preloading offer details."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    sget-object v1, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v1}, Lcom/google/commerce/wireless/topiary/D;->c()V

    .line 182
    invoke-virtual {p1}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v1

    .line 183
    sget-object v2, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-static {v1}, Lcom/google/android/apps/common/offerslib/z;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/common/offerslib/f;

    invoke-direct {v3}, Lcom/google/android/apps/common/offerslib/f;-><init>()V

    invoke-virtual {v2, v1, v0, p2, v3}, Lcom/google/commerce/wireless/topiary/D;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/E;)V

    .line 192
    :cond_36
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;ILjava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c(Z)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 553
    if-nez p0, :cond_8

    .line 554
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556
    :cond_8
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->clearHistory()V

    .line 955
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->requestFocus(I)Z

    .line 958
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    .line 959
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 969
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dispatchEvent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    .line 970
    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->x()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 971
    const-string v0, "dispatchEvent"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/common/offerslib/A;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 973
    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v1, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    .line 975
    :cond_3a
    return-void
.end method

.method private a(Landroid/accounts/Account;Landroid/content/Context;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 302
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 303
    const-string v0, "preferences_last_account_used_to_display_offer_details"

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 304
    const-string v1, "OfferDetailsFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Comparing current account :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to last used account :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    if-nez v0, :cond_31

    if-nez p1, :cond_41

    :cond_31
    if-eqz v0, :cond_35

    if-eqz p1, :cond_41

    :cond_35
    if-eqz v0, :cond_59

    if-eqz p1, :cond_59

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_59

    :cond_41
    const/4 v0, 0x1

    move v1, v0

    .line 311
    :goto_43
    if-eqz v1, :cond_58

    .line 313
    if-eqz p1, :cond_5c

    .line 314
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "preferences_last_account_used_to_display_offer_details"

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 318
    :goto_53
    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->W:Lcom/google/android/apps/common/offerslib/v;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/common/offerslib/v;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 321
    :cond_58
    return v1

    .line 306
    :cond_59
    const/4 v0, 0x0

    move v1, v0

    goto :goto_43

    .line 316
    :cond_5c
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "preferences_last_account_used_to_display_offer_details"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_53
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v0, :cond_a

    .line 245
    invoke-static {p0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    .line 248
    :cond_a
    if-eqz p1, :cond_20

    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    if-eq v0, v1, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method static synthetic b(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 924
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 925
    const-string v1, "http"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_35

    .line 926
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 928
    :cond_35
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    .line 929
    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/util/HashMap;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ag:Ljava/util/HashMap;

    return-object v0
.end method

.method private c(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 573
    iput v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    .line 574
    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Z:Landroid/view/View;

    if-eqz p1, :cond_b

    :goto_7
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 575
    return-void

    .line 574
    :cond_b
    const/16 v0, 0x8

    goto :goto_7
.end method

.method static synthetic d(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Lcom/google/android/apps/common/offerslib/c;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Lcom/google/commerce/wireless/topiary/HybridWebView;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Lcom/google/android/apps/common/offerslib/i;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Q:Lcom/google/android/apps/common/offerslib/i;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Z
    .registers 2
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->y()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic v()Lcom/google/commerce/wireless/topiary/D;
    .registers 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    return-object v0
.end method

.method private w()V
    .registers 5

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_7

    .line 224
    :cond_6
    :goto_6
    return-void

    .line 204
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/accounts/Account;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 213
    :cond_17
    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 217
    sget-object v1, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v1, :cond_30

    .line 218
    invoke-static {v0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    .line 221
    :cond_30
    const-string v0, "OfferDetailsFragment"

    const-string v1, "Account not authenticated, clearing all cookies."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/D;->c()V

    goto :goto_6
.end method

.method private x()Z
    .registers 3

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method private y()Z
    .registers 2

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Z:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private z()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 939
    new-instance v0, Lcom/google/android/apps/common/offerslib/u;

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->s()Landroid/accounts/Account;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    new-instance v5, Lcom/google/android/apps/common/offerslib/g;

    invoke-direct {v5, p0}, Lcom/google/android/apps/common/offerslib/g;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/common/offerslib/u;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;Lcom/google/commerce/wireless/topiary/z;)V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    .line 941
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/HybridWebView;

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    .line 943
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 944
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 945
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0, v6}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->setVisibility(I)V

    .line 946
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 947
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x2

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 328
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 329
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 332
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 334
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 335
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 338
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 339
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 340
    iput-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Y:Landroid/widget/RelativeLayout;

    .line 342
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-direct {v2, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 343
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 346
    const/16 v3, 0x11

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 347
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 348
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 349
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 350
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 351
    iput-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Z:Landroid/view/View;

    .line 353
    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1000
    const/16 v0, 0x4d

    if-ne p1, v0, :cond_1a

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1a

    .line 1002
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ag:Ljava/util/HashMap;

    const-string v1, "scan_qr"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "SCAN_RESULT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    :cond_1a
    return-void
.end method

.method public a(Landroid/accounts/Account;)V
    .registers 3
    .parameter

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    if-eqz v0, :cond_e

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a()V

    .line 156
    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->z()V

    .line 158
    :cond_e
    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/a;)V
    .registers 2
    .parameter

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    .line 143
    return-void
.end method

.method public a(Lcom/google/android/apps/common/offerslib/d;)V
    .registers 2
    .parameter

    .prologue
    .line 434
    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    .line 435
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->g()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 560
    const-string v0, "OfferDetailsFragment"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    :cond_13
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->a()V

    .line 472
    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->w()V

    .line 473
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 474
    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/a;->f()Ljava/lang/String;

    move-result-object v1

    .line 476
    const-string v2, "com.google.android.apps.offers.VIEW_OFFER_DETAILS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_61

    .line 477
    const-string v0, "processIntent ACTION_SHOW_OFFER"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    .line 478
    const-string v0, "offer_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    .line 479
    const-string v0, "offer_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "offer_id"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 481
    const-string v0, "offer_namespace"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "offer_namespace"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 483
    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->x()Z

    move-result v0

    .line 485
    iget-object v2, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/common/offerslib/d;->i(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Landroid/location/Location;

    move-result-object v2

    .line 486
    if-eqz v2, :cond_4e

    .line 487
    const-string v3, "user_location"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 490
    :cond_4e
    invoke-static {p1, v1, v0}, Lcom/google/android/apps/common/offerslib/z;->a(Landroid/content/Intent;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    .line 510
    :goto_54
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/a;Z)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    .line 511
    return-void

    .line 492
    :cond_61
    const-string v2, "com.google.android.apps.offers.VIEW_MY_OFFERS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_75

    .line 493
    const-string v0, "processIntent ACTION_SHOW_MY_OFFERS"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    .line 494
    invoke-static {v1}, Lcom/google/android/apps/common/offerslib/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    goto :goto_54

    .line 495
    :cond_75
    const-string v2, "com.google.android.apps.offers.VIEW_CUSTOM"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_94

    .line 496
    const-string v0, "processIntent ACTION_SHOW_CUSTOM_VIEW"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    .line 497
    const-string v0, "page_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "page_name"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    invoke-static {p1, v1}, Lcom/google/android/apps/common/offerslib/z;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    goto :goto_54

    .line 500
    :cond_94
    const-string v2, "com.google.android.apps.offers.PROCESS_QR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ca

    .line 501
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processIntent ACTION_PROCESS_QR - got qr: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "SCAN_RESULT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    .line 503
    const-string v0, "SCAN_RESULT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "SCAN_RESULT"

    invoke-static {v0, v2}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 505
    invoke-static {p1, v1}, Lcom/google/android/apps/common/offerslib/z;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    goto :goto_54

    .line 507
    :cond_ca
    const-string v0, "processIntent action not recognized"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Ljava/lang/String;)V

    .line 508
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "action not recognized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_1b

    const/4 v1, 0x0

    :goto_17
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 583
    :cond_1a
    return-void

    .line 581
    :cond_1b
    const/16 v1, 0x8

    goto :goto_17
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 358
    invoke-super {p0, p1}, Landroid/support/v4/app/f;->c(Landroid/os/Bundle;)V

    .line 361
    if-eqz p1, :cond_49

    .line 362
    const-string v0, "param_app_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 363
    const-string v0, "param_app_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/offerslib/a;

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    .line 365
    :cond_17
    const-string v0, "param_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 366
    const-string v0, "param_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    .line 368
    :cond_29
    const-string v0, "param_requested_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 369
    const-string v0, "param_requested_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    .line 371
    :cond_39
    const-string v0, "param_offer_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 372
    const-string v0, "param_offer_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    .line 376
    :cond_49
    sget-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    if-nez v0, :cond_5b

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->d()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/content/Context;)Lcom/google/commerce/wireless/topiary/D;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->P:Lcom/google/commerce/wireless/topiary/D;

    .line 379
    :cond_5b
    new-instance v0, Lcom/google/android/apps/common/offerslib/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/common/offerslib/v;-><init>(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->W:Lcom/google/android/apps/common/offerslib/v;

    .line 381
    invoke-direct {p0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->z()V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->N:Lcom/google/android/apps/common/offerslib/d;

    if-nez v0, :cond_71

    .line 384
    new-instance v0, Lcom/google/android/apps/common/offerslib/d;

    invoke-direct {v0}, Lcom/google/android/apps/common/offerslib/d;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/d;)V

    .line 386
    :cond_71
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    if-nez v0, :cond_89

    .line 387
    new-instance v0, Lcom/google/android/apps/common/offerslib/c;

    invoke-direct {v0}, Lcom/google/android/apps/common/offerslib/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    .line 388
    if-eqz p1, :cond_89

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    const-string v1, "param_internal_url_whitelist"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/offerslib/c;->a(Ljava/lang/String;)V

    .line 394
    :cond_89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ag:Ljava/util/HashMap;

    .line 396
    if-eqz p1, :cond_a0

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->Q:Lcom/google/android/apps/common/offerslib/i;

    const-string v2, "Android"

    invoke-virtual {v0, v1, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 403
    :cond_a0
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    if-eqz v0, :cond_9

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {v0}, Lcom/google/android/apps/common/offerslib/a;->a()V

    .line 415
    :cond_9
    invoke-super {p0, p1}, Landroid/support/v4/app/f;->d(Landroid/os/Bundle;)V

    .line 416
    const-string v0, "param_app_settings"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->U:Lcom/google/android/apps/common/offerslib/a;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 417
    const-string v0, "param_account"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 418
    const-string v0, "param_requested_url"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string v0, "param_internal_url_whitelist"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->S:Lcom/google/android/apps/common/offerslib/c;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v0, "param_offer_type"

    iget-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->X:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->R:Lcom/google/commerce/wireless/topiary/HybridWebView;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 423
    return-void
.end method

.method public f()V
    .registers 1

    .prologue
    .line 407
    invoke-super {p0}, Landroid/support/v4/app/f;->f()V

    .line 408
    return-void
.end method

.method public j()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 427
    iput-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->aa:Landroid/os/Handler;

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->a()V

    .line 429
    iput-object v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    .line 430
    invoke-super {p0}, Landroid/support/v4/app/f;->j()V

    .line 431
    return-void
.end method

.method public r()Z
    .registers 2

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->O:Lcom/google/commerce/wireless/topiary/HybridWebViewControl;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/HybridWebViewControl;->d()V

    .line 543
    const/4 v0, 0x1

    .line 545
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method protected s()Landroid/accounts/Account;
    .registers 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->V:Landroid/accounts/Account;

    return-object v0
.end method

.method t()V
    .registers 4

    .prologue
    .line 981
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.zxing.client.android.SCAN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 984
    const-string v1, "SCAN_MODE"

    const-string v2, "QR_CODE_MODE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 985
    const/16 v1, 0x4d

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Landroid/content/Intent;I)V

    .line 986
    return-void
.end method

.method u()V
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1166
    iget v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    if-eqz v1, :cond_c

    .line 1167
    iget v1, p0, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->ab:I

    if-ne v1, v0, :cond_d

    :goto_9
    invoke-direct {p0, v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c(Z)V

    .line 1169
    :cond_c
    return-void

    .line 1167
    :cond_d
    const/4 v0, 0x0

    goto :goto_9
.end method
