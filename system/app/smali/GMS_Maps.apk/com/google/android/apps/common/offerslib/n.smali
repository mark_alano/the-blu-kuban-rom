.class Lcom/google/android/apps/common/offerslib/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/common/offerslib/t;


# instance fields
.field final synthetic a:Lcom/google/android/apps/common/offerslib/m;


# direct methods
.method constructor <init>(Lcom/google/android/apps/common/offerslib/m;)V
    .registers 2
    .parameter

    .prologue
    .line 690
    iput-object p1, p0, Lcom/google/android/apps/common/offerslib/n;->a:Lcom/google/android/apps/common/offerslib/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)V
    .registers 6
    .parameter

    .prologue
    .line 694
    const-string v0, ""

    .line 695
    if-eqz p1, :cond_5c

    .line 696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 705
    :goto_40
    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/n;->a:Lcom/google/android/apps/common/offerslib/m;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v2, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    iget-object v0, p0, Lcom/google/android/apps/common/offerslib/n;->a:Lcom/google/android/apps/common/offerslib/m;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/m;->c:Lcom/google/android/apps/common/offerslib/i;

    iget-object v0, v0, Lcom/google/android/apps/common/offerslib/i;->a:Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;

    invoke-static {v0}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->c(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;)Ljava/util/HashMap;

    move-result-object v0

    const-string v3, "acquire_location"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;->a(Lcom/google/android/apps/common/offerslib/OfferDetailsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    return-void

    :cond_5c
    move-object v1, v0

    goto :goto_40
.end method
