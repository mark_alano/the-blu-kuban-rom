.class public abstract Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field public static final a:J

.field private static d:I


# instance fields
.field protected b:Lr/I;

.field protected c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

.field private volatile e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

.field private final f:Landroid/os/IBinder;

.field private volatile g:J

.field private volatile h:J

.field private i:J

.field private j:Landroid/os/Looper;

.field private k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

.field private l:Lcom/google/googlenav/android/F;

.field private m:Landroid/net/wifi/WifiManager$WifiLock;

.field private n:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 70
    const/16 v0, 0x20

    sput v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    .line 87
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_f

    const-wide/16 v0, 0x14

    :goto_c
    sput-wide v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a:J

    return-void

    :cond_f
    const-wide/16 v0, 0xa

    goto :goto_c
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 160
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 163
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/j;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->f:Landroid/os/IBinder;

    .line 1153
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    return-object p1
.end method

.method private a(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 432
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendMessage(Landroid/os/Message;)Z

    .line 433
    return-void
.end method

.method private a(ILjava/lang/Object;J)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    invoke-virtual {v1, v0, p3, p4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 446
    return-void
.end method

.method private a(J)V
    .registers 7
    .parameter

    .prologue
    .line 1047
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    .line 1048
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFECHED_FINISHED"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    .line 1050
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    .line 1051
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x3

    .line 751
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->c()Z

    move-result v2

    if-nez v2, :cond_2b

    .line 753
    if-eqz p3, :cond_17

    .line 755
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v2}, Lr/I;->d()Z

    move-result v2

    if-nez v2, :cond_1b

    .line 762
    :goto_14
    invoke-interface {p3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    .line 765
    :cond_17
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    .line 797
    :goto_1a
    return-void

    .line 757
    :cond_1b
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v0

    if-nez v0, :cond_23

    move v0, v1

    .line 758
    goto :goto_14

    .line 759
    :cond_23
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i()Z

    move-result v0

    if-eqz v0, :cond_c6

    move v0, v7

    .line 760
    goto :goto_14

    .line 769
    :cond_2b
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h:J

    .line 770
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    .line 771
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->b()V

    .line 778
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 782
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    if-ne v2, v4, :cond_bd

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v2

    if-eqz v2, :cond_bd

    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/e;->c()I

    move-result v2

    .line 789
    :goto_5a
    new-array v4, v7, [Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "l="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->f()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "x="

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->e()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "m="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 793
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v1

    const-string v3, "s"

    invoke-virtual {p0, v1, v3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;ILcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;J)V

    invoke-direct {p0, v7, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    goto/16 :goto_1a

    .line 782
    :cond_bd
    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/e;->b()I

    move-result v2

    goto :goto_5a

    :cond_c6
    move v0, v3

    goto/16 :goto_14
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->p()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILjava/lang/Object;J)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Li/f;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Li/f;)V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;)V
    .registers 6
    .parameter

    .prologue
    .line 1113
    const-string v0, ""

    .line 1114
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/g;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3c

    .line 1127
    :goto_d
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "r="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1131
    const/16 v1, 0x60

    const-string v2, "c"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1133
    return-void

    .line 1116
    :pswitch_32
    const-string v0, "u"

    goto :goto_d

    .line 1119
    :pswitch_35
    const-string v0, "n"

    goto :goto_d

    .line 1122
    :pswitch_38
    const-string v0, "o"

    goto :goto_d

    .line 1114
    nop

    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_32
        :pswitch_35
        :pswitch_38
    .end packed-switch
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 856
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->a()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v2

    .line 859
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 860
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a()V

    .line 865
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v0}, Lr/I;->j()J

    move-result-wide v3

    const-wide/16 v5, 0x190

    cmp-long v0, v3, v5

    if-gez v0, :cond_2d

    .line 867
    const-wide/16 v3, 0x3e8

    :try_start_20
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_23
    .catch Ljava/lang/InterruptedException; {:try_start_20 .. :try_end_23} :catch_11c

    .line 874
    :goto_23
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->q()Z

    move-result v0

    if-nez v0, :cond_12

    .line 880
    :cond_2d
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->e()Z

    move-result v0

    if-nez v0, :cond_64

    .line 881
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->y()V

    .line 883
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    if-eqz v0, :cond_5b

    .line 884
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->b()Z

    move-result v0

    if-nez v0, :cond_5b

    .line 885
    const/4 v0, 0x0

    .line 886
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v2}, Lr/I;->d()Z

    move-result v2

    if-nez v2, :cond_5c

    move v0, v1

    .line 891
    :cond_54
    :goto_54
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    .line 950
    :cond_5b
    :goto_5b
    return-void

    .line 888
    :cond_5c
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-nez v1, :cond_54

    .line 889
    const/4 v0, 0x2

    goto :goto_54

    .line 897
    :cond_64
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 899
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->b()I

    move-result v0

    if-lez v0, :cond_a3

    .line 900
    :cond_6e
    :goto_6e
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    sget v4, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    if-ge v0, v4, :cond_a3

    .line 901
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->c()Lo/aq;

    move-result-object v4

    .line 902
    if-eqz v4, :cond_a3

    .line 903
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a0

    sget-object v0, Lr/c;->c:Lr/c;

    .line 907
    :goto_8e
    invoke-static {v0}, Lr/u;->a(Lr/c;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v0, v4}, Lr/I;->a(Lo/aq;)Z

    move-result v0

    if-nez v0, :cond_6e

    .line 909
    :cond_9c
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6e

    .line 903
    :cond_a0
    sget-object v0, Lr/c;->e:Lr/c;

    goto :goto_8e

    .line 918
    :cond_a3
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v4

    .line 922
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/m;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    .line 926
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e4

    sget-object v0, Lr/c;->c:Lr/c;

    move-object v2, v0

    .line 930
    :goto_bf
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f()V

    .line 931
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_ca
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 936
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    invoke-virtual {v8, v0, v5, v2, v1}, Lr/I;->a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->a(Lr/v;)V

    goto :goto_ca

    .line 926
    :cond_e4
    sget-object v0, Lr/c;->e:Lr/c;

    move-object v2, v0

    goto :goto_bf

    .line 941
    :cond_e8
    const-string v0, "b"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5b

    .line 945
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    if-eqz v0, :cond_117

    .line 946
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->f()Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->b()V

    .line 948
    :cond_117
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V

    goto/16 :goto_5b

    .line 868
    :catch_11c
    move-exception v0

    goto/16 :goto_23
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->r()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;)V
    .registers 8
    .parameter

    .prologue
    .line 969
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 971
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 972
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v3

    if-ne v2, v3, :cond_1f

    .line 973
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(J)V

    .line 977
    :cond_1f
    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h:J

    .line 980
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->b()V

    .line 983
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->g()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 984
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/l;->d()Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-result-object v2

    const-string v3, "f"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    invoke-static {}, Lbm/m;->d()V

    .line 991
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    .line 992
    return-void
.end method

.method private b(Li/f;)V
    .registers 2
    .parameter

    .prologue
    .line 700
    invoke-interface {p1}, Li/f;->a()V

    .line 701
    return-void
.end method

.method private c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 548
    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/e;->i()Z

    move-result v0

    if-nez v0, :cond_1d

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 550
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/b;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(Li/f;)V

    .line 561
    const/4 v0, 0x7

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    .line 575
    :goto_1c
    return-void

    .line 564
    :cond_1d
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    new-instance v6, Lcom/google/android/apps/gmm/map/internal/store/prefetch/c;

    invoke-direct {v6, p0, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/c;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V

    .line 573
    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(Li/f;)V

    .line 574
    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    goto :goto_1c
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->s()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    return-object v0
.end method

.method private d(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 595
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 596
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;

    invoke-direct {v4, p0, p2, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)V

    .line 630
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/e;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/e;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    invoke-direct {v3, p1, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Li/f;)V

    .line 648
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->h()I

    move-result v5

    .line 649
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/p;->e()I

    move-result v2

    .line 651
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/f;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;ILcom/google/android/apps/gmm/map/internal/store/prefetch/p;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;I)V

    .line 688
    invoke-interface {v4, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(Li/f;)V

    .line 691
    if-nez v2, :cond_2c

    .line 692
    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->b()V

    .line 697
    :goto_2b
    return-void

    .line 694
    :cond_2c
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    sget-object v2, Lr/c;->c:Lr/c;

    invoke-virtual {v1, v3, v2, v0}, Lr/I;->a(Lo/ar;Lr/c;Ls/e;)V

    goto :goto_2b
.end method

.method static synthetic e(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->u()V

    return-void
.end method

.method public static l()I
    .registers 1

    .prologue
    .line 1136
    sget v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    return v0
.end method

.method static synthetic m()I
    .registers 1

    .prologue
    .line 62
    sget v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->d:I

    return v0
.end method

.method private n()Landroid/os/PowerManager$WakeLock;
    .registers 4

    .prologue
    .line 295
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 296
    const/4 v1, 0x1

    const-string v2, "PrefetcherService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    return-object v0
.end method

.method private o()Landroid/net/wifi/WifiManager$WifiLock;
    .registers 4

    .prologue
    .line 307
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 308
    const/4 v1, 0x1

    const-string v2, "PrefetcherService"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .registers 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->removeMessages(I)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->removeMessages(I)V

    .line 331
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->q()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    .line 340
    :goto_1d
    return-void

    .line 338
    :cond_1e
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    goto :goto_1d
.end method

.method private q()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 348
    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v1

    if-nez v1, :cond_8

    .line 366
    :cond_7
    :goto_7
    return v0

    .line 354
    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->t()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 358
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    invoke-virtual {v1}, Lcom/google/googlenav/android/F;->a()Z

    move-result v1

    if-nez v1, :cond_26

    .line 359
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 360
    const/16 v1, 0x60

    const-string v2, "n"

    const-string v3, "p"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 366
    :cond_26
    const/4 v0, 0x1

    goto :goto_7
.end method

.method private r()V
    .registers 7

    .prologue
    .line 529
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/a;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;)V

    .line 531
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 532
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->c()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    .line 533
    new-instance v2, Lcom/google/googlenav/prefetch/android/l;

    invoke-direct {v2, v1}, Lcom/google/googlenav/prefetch/android/l;-><init>(Ljava/util/List;)V

    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    .line 534
    return-void
.end method

.method private s()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 537
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/q;-><init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V

    .line 539
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 540
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->c()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    .line 541
    new-instance v2, Lcom/google/googlenav/prefetch/android/l;

    invoke-direct {v2, v1}, Lcom/google/googlenav/prefetch/android/l;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v2, v0, v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    .line 542
    return-void
.end method

.method private t()Z
    .registers 7

    .prologue
    .line 741
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 742
    invoke-static {}, Lcom/google/googlenav/clientparam/e;->k()Lcom/google/googlenav/clientparam/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/e;->d()J

    move-result-wide v2

    .line 743
    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    sub-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1d

    const/4 v0, 0x1

    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method private u()V
    .registers 2

    .prologue
    .line 957
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 960
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    .line 962
    return-void
.end method

.method private v()V
    .registers 3

    .prologue
    .line 999
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_9

    .line 1000
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 1003
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_12

    .line 1004
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1019
    :cond_12
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1020
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    .line 1022
    :cond_1e
    return-void
.end method

.method private w()J
    .registers 7

    .prologue
    const-wide/16 v2, 0x0

    .line 1032
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFECHED_FINISHED"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1036
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_21

    .line 1039
    :goto_20
    return-wide v0

    :cond_21
    move-wide v0, v2

    goto :goto_20
.end method

.method private x()J
    .registers 5

    .prologue
    .line 1054
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFETCH_NOT_START_CONDITION_LOG_TIME"

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private y()V
    .registers 2

    .prologue
    .line 1085
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/v;->b()V

    .line 1086
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 1087
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->v()V

    .line 1088
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    .line 322
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 525
    const/4 v0, 0x5

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    .line 526
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1105
    const/16 v0, 0x60

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;->a()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1107
    return-void
.end method

.method public a(Li/f;)V
    .registers 3
    .parameter

    .prologue
    .line 704
    const/16 v0, 0x9

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    .line 705
    return-void
.end method

.method protected abstract b()V
.end method

.method public b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 584
    const/16 v0, 0x8

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(ILjava/lang/Object;)V

    .line 585
    return-void
.end method

.method protected abstract c()V
.end method

.method protected abstract d()V
.end method

.method protected e()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 244
    new-instance v0, Lcom/google/googlenav/android/F;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/F;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->l:Lcom/google/googlenav/android/F;

    .line 246
    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/I;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b:Lr/I;

    .line 249
    invoke-static {}, Lcom/google/googlenav/prefetch/android/h;->d()Lcom/google/googlenav/prefetch/android/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a()V

    .line 262
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->w()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    .line 266
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->x()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    .line 271
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->o()Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->m:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 275
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->n:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 279
    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v0

    if-nez v0, :cond_49

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->b()V

    .line 286
    :goto_48
    return-void

    .line 283
    :cond_49
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a()V

    goto :goto_48
.end method

.method public f()V
    .registers 3

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    .line 517
    return-void
.end method

.method public g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;
    .registers 7

    .prologue
    .line 715
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->g()J

    .line 717
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;

    iget-wide v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    iget-wide v3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->h:J

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v5

    if-eqz v5, :cond_1d

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    if-eqz v5, :cond_1d

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_19
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;-><init>(JJLjava/lang/String;)V

    return-object v0

    :cond_1d
    const/4 v5, 0x0

    goto :goto_19
.end method

.method public h()V
    .registers 4

    .prologue
    const-wide/16 v1, 0x0

    .line 724
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 726
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(J)V

    .line 727
    iput-wide v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->g:J

    .line 729
    :cond_d
    return-void
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 732
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    if-ne v0, v1, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public j()V
    .registers 5

    .prologue
    .line 1063
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    .line 1064
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LAST_PREFETCH_NOT_START_CONDITION_LOG_TIME"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    .line 1067
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    .line 1068
    return-void
.end method

.method public k()Z
    .registers 7

    .prologue
    .line 1075
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_1c

    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->i:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1e

    :cond_1c
    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->f:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .registers 3

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->c()V

    .line 230
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PrefetcherService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 231
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 232
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 236
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->k:Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/n;->sendEmptyMessage(I)Z

    .line 240
    return-void
.end method

.method public onDestroy()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 398
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->j:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 404
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/i;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/i;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/b;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    check-cast v0, Ljava/lang/Void;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 405
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 375
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Landroid/content/Intent;)V

    .line 376
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 380
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Landroid/content/Intent;)V

    .line 383
    const/4 v0, 0x1

    return v0
.end method
