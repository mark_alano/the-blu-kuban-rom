.class Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

.field final synthetic b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 596
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public H_()V
    .registers 3

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->H_()V

    .line 622
    return-void
.end method

.method public a()V
    .registers 2

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a()V

    .line 610
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(I)V

    .line 628
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 604
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(II)V

    .line 605
    return-void
.end method

.method public a(Li/f;)V
    .registers 3
    .parameter

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->a(Li/f;)V

    .line 600
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/d;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;->b()V

    .line 616
    return-void
.end method
