.class public final enum Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    const-string v1, "USER_INITIATED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    const-string v1, "NO_NETWORK_CONNECTION_IN_A_LONG_TIME"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    .line 154
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    const-string v1, "OFFLINE_TURNED_OFF"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    .line 153
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->d:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;
    .registers 2
    .parameter

    .prologue
    .line 153
    const-class v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;
    .registers 1

    .prologue
    .line 153
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->d:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/h;

    return-object v0
.end method
