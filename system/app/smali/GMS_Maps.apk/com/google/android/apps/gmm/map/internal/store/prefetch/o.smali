.class final enum Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

.field public static final enum d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 158
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    const-string v1, "PREFETCHING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    const-string v1, "REMOVING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    const-string v1, "SUSPENDED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->b:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->c:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->e:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 158
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;
    .registers 2
    .parameter

    .prologue
    .line 158
    const-class v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;
    .registers 1

    .prologue
    .line 158
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->e:[Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/store/prefetch/o;

    return-object v0
.end method
