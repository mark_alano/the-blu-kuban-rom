.class public abstract Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Li/f;


# instance fields
.field protected final a:Lcom/google/googlenav/android/F;

.field protected final b:Lr/I;

.field protected final c:Landroid/net/wifi/WifiManager$WifiLock;

.field protected final d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

.field protected final e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

.field private volatile f:Z

.field private final g:Li/g;

.field private final h:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/android/F;Lr/I;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;Li/g;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f:Z

    .line 48
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->a:Lcom/google/googlenav/android/F;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->b:Lr/I;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->c:Landroid/net/wifi/WifiManager$WifiLock;

    .line 56
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->d:Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;

    .line 57
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    .line 58
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g:Li/g;

    .line 59
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f:Z

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr/v;

    .line 110
    invoke-interface {v0}, Lr/v;->a()V

    goto :goto_9

    .line 112
    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g:Li/g;

    if-eqz v0, :cond_22

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->g:Li/g;

    invoke-interface {v0}, Li/g;->a()V

    .line 115
    :cond_22
    return-void
.end method

.method public a(Lr/v;)V
    .registers 3
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->f:Z

    return v0
.end method

.method public abstract c()Z
.end method

.method protected abstract d()Z
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 84
    const/4 v0, 0x0

    .line 86
    :goto_7
    return v0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->d()Z

    move-result v0

    goto :goto_7
.end method

.method public f()V
    .registers 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 91
    return-void
.end method

.method public g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/t;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/k;

    return-object v0
.end method
