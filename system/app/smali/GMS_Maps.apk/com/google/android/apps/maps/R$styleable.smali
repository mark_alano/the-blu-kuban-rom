.class public final Lcom/google/android/apps/maps/R$styleable;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final GridFlowLayout:[I = null

.field public static final GridFlowLayout_horizontalGridSize:I = 0x0

.field public static final GridFlowLayout_horizontalGridSpacing:I = 0x2

.field public static final GridFlowLayout_verticalGridSize:I = 0x1

.field public static final GridFlowLayout_verticalGridSpacing:I = 0x3

.field public static final ListPopupSpinnerSdk5:[I = null

.field public static final ListPopupSpinnerSdk5_arrowOnLeft:I = 0x1

.field public static final ListPopupSpinnerSdk5_dialogResource:I = 0x0

.field public static final MaxWidthForSpinner:[I = null

.field public static final MaxWidthForSpinner_spinner_max_width:I = 0x0

.field public static final SqueezedLabelView:[I = null

.field public static final SqueezedLabelView_desiredTextSize:I = 0x0

.field public static final SqueezedLabelView_minTextSize:I = 0x1

.field public static final da_SqueezedLabelView:[I = null

.field public static final da_SqueezedLabelView_da_desiredTextSize:I = 0x0

.field public static final da_SqueezedLabelView_da_minTextSize:I = 0x1

.field public static final da_TileButton:[I = null

.field public static final da_TileButton_icon:I = 0x1

.field public static final da_TileButton_title:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x2

    .line 5755
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_32

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->GridFlowLayout:[I

    .line 5834
    new-array v0, v3, [I

    fill-array-data v0, :array_3e

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->ListPopupSpinnerSdk5:[I

    .line 5871
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010009

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->MaxWidthForSpinner:[I

    .line 5902
    new-array v0, v3, [I

    fill-array-data v0, :array_46

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->SqueezedLabelView:[I

    .line 5949
    new-array v0, v3, [I

    fill-array-data v0, :array_4e

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->da_SqueezedLabelView:[I

    .line 5996
    new-array v0, v3, [I

    fill-array-data v0, :array_56

    sput-object v0, Lcom/google/android/apps/maps/R$styleable;->da_TileButton:[I

    return-void

    .line 5755
    nop

    :array_32
    .array-data 0x4
        0x2t 0x0t 0x1t 0x7ft
        0x3t 0x0t 0x1t 0x7ft
        0x4t 0x0t 0x1t 0x7ft
        0x5t 0x0t 0x1t 0x7ft
    .end array-data

    .line 5834
    :array_3e
    .array-data 0x4
        0x7t 0x0t 0x1t 0x7ft
        0x8t 0x0t 0x1t 0x7ft
    .end array-data

    .line 5902
    :array_46
    .array-data 0x4
        0x0t 0x0t 0x1t 0x7ft
        0x1t 0x0t 0x1t 0x7ft
    .end array-data

    .line 5949
    :array_4e
    .array-data 0x4
        0xat 0x0t 0x1t 0x7ft
        0xbt 0x0t 0x1t 0x7ft
    .end array-data

    .line 5996
    :array_56
    .array-data 0x4
        0xct 0x0t 0x1t 0x7ft
        0xdt 0x0t 0x1t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 5738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
