.class Lcom/google/android/location/a$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:[Z

.field private final c:[D

.field private final d:[D

.field private e:I


# direct methods
.method private constructor <init>()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x5

    .line 496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/location/a$a;->a:[Ljava/lang/String;

    .line 491
    new-array v0, v3, [Z

    iput-object v0, p0, Lcom/google/android/location/a$a;->b:[Z

    .line 492
    new-array v0, v3, [D

    iput-object v0, p0, Lcom/google/android/location/a$a;->c:[D

    .line 493
    new-array v0, v3, [D

    iput-object v0, p0, Lcom/google/android/location/a$a;->d:[D

    .line 494
    iput v1, p0, Lcom/google/android/location/a$a;->e:I

    move v0, v1

    .line 497
    :goto_18
    if-ge v0, v3, :cond_21

    .line 498
    iget-object v2, p0, Lcom/google/android/location/a$a;->b:[Z

    aput-boolean v1, v2, v0

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 500
    :cond_21
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a$1;)V
    .registers 2
    .parameter

    .prologue
    .line 485
    invoke-direct {p0}, Lcom/google/android/location/a$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 485
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a$a;->b(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V

    return-void
.end method

.method private a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 507
    if-eqz p1, :cond_5

    if-nez p2, :cond_7

    :cond_5
    move v0, v8

    .line 523
    :goto_6
    return v0

    .line 510
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->a()Ljava/lang/String;

    move-result-object v10

    .line 511
    invoke-interface {p2}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v4

    .line 512
    invoke-interface {p2}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v6

    move v9, v8

    .line 514
    :goto_14
    const/4 v0, 0x5

    if-ge v9, v0, :cond_3f

    .line 515
    iget-object v0, p0, Lcom/google/android/location/a$a;->b:[Z

    aget-boolean v0, v0, v9

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/google/android/location/a$a;->a:[Ljava/lang/String;

    aget-object v0, v0, v9

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 516
    iget-object v0, p0, Lcom/google/android/location/a$a;->c:[D

    aget-wide v0, v0, v9

    iget-object v2, p0, Lcom/google/android/location/a$a;->d:[D

    aget-wide v2, v2, v9

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    .line 517
    const-wide/high16 v2, 0x4049

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3b

    .line 518
    const/4 v0, 0x1

    goto :goto_6

    .line 514
    :cond_3b
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_14

    .line 522
    :cond_3f
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a$a;->b(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V

    move v0, v8

    .line 523
    goto :goto_6
.end method

.method private b(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 527
    if-eqz p1, :cond_4

    if-nez p2, :cond_5

    .line 536
    :cond_4
    :goto_4
    return-void

    .line 530
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/location/e/e;->a()Ljava/lang/String;

    move-result-object v0

    .line 531
    iget-object v1, p0, Lcom/google/android/location/a$a;->a:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/location/a$a;->e:I

    aput-object v0, v1, v2

    .line 532
    iget-object v0, p0, Lcom/google/android/location/a$a;->c:[D

    iget v1, p0, Lcom/google/android/location/a$a;->e:I

    invoke-interface {p2}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 533
    iget-object v0, p0, Lcom/google/android/location/a$a;->d:[D

    iget v1, p0, Lcom/google/android/location/a$a;->e:I

    invoke-interface {p2}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 534
    iget-object v0, p0, Lcom/google/android/location/a$a;->b:[Z

    iget v1, p0, Lcom/google/android/location/a$a;->e:I

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 535
    iget v0, p0, Lcom/google/android/location/a$a;->e:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/google/android/location/a$a;->e:I

    goto :goto_4
.end method

.method static synthetic b(Lcom/google/android/location/a$a;Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 485
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a$a;->a(Lcom/google/android/location/e/e;Lcom/google/android/location/os/g;)Z

    move-result v0

    return v0
.end method
