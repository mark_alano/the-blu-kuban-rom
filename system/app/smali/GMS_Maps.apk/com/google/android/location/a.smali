.class abstract Lcom/google/android/location/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a$1;,
        Lcom/google/android/location/a$a;,
        Lcom/google/android/location/a$b;,
        Lcom/google/android/location/a$c;
    }
.end annotation


# instance fields
.field protected final a:Ljava/lang/String;

.field final b:Lcom/google/android/location/os/i;

.field final c:Lcom/google/android/location/b/f;

.field final d:Lcom/google/android/location/x;

.field final e:Lcom/google/android/location/a$b;

.field protected f:Lcom/google/android/location/a$c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/os/i;Lcom/google/android/location/b/f;Lcom/google/android/location/x;Lcom/google/android/location/a$b;Lcom/google/android/location/a$c;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p2, p0, Lcom/google/android/location/a;->b:Lcom/google/android/location/os/i;

    .line 91
    iput-object p3, p0, Lcom/google/android/location/a;->c:Lcom/google/android/location/b/f;

    .line 92
    iput-object p4, p0, Lcom/google/android/location/a;->d:Lcom/google/android/location/x;

    .line 93
    iput-object p5, p0, Lcom/google/android/location/a;->e:Lcom/google/android/location/a$b;

    .line 94
    iput-object p6, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    .line 95
    iput-object p1, p0, Lcom/google/android/location/a;->a:Ljava/lang/String;

    .line 98
    return-void
.end method

.method protected static a(JLcom/google/android/location/os/g;Lcom/google/android/location/e/e;Lcom/google/android/location/e/E;ZI)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 109
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 110
    if-eqz p2, :cond_26

    .line 113
    if-nez p3, :cond_e

    if-eqz p4, :cond_49

    :cond_e
    move v0, v1

    .line 114
    :goto_f
    const/4 v3, 0x3

    invoke-static {p2, p0, p1, p5, v0}, Lcom/google/android/location/a;->a(Lcom/google/android/location/os/g;JZZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 116
    invoke-interface {p2}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v3

    add-long/2addr v3, p0

    .line 117
    invoke-interface {p2}, Lcom/google/android/location/os/g;->g()J

    move-result-wide v5

    sub-long/2addr v3, v5

    .line 118
    const/16 v0, 0xe

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 120
    :cond_26
    if-eqz p3, :cond_2f

    .line 121
    invoke-static {p3, p0, p1}, Lcom/google/android/location/e/f;->a(Lcom/google/android/location/e/e;J)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 124
    :cond_2f
    if-eqz p4, :cond_39

    .line 125
    invoke-virtual {p4, p0, p1, v1}, Lcom/google/android/location/e/E;->a(JZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 126
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 128
    :cond_39
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/j/a;->U:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 129
    invoke-virtual {v0, v1, p6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 130
    const/16 v1, 0x63

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 131
    return-object v2

    .line 113
    :cond_49
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private static a(Lcom/google/android/location/os/g;JZZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const-wide v4, 0x416312d000000000L

    .line 136
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->x:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 137
    invoke-interface {p0}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v1

    mul-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 138
    const/4 v1, 0x2

    invoke-interface {p0}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 140
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/j/a;->Q:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 141
    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 142
    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 143
    const/4 v0, 0x6

    invoke-interface {p0}, Lcom/google/android/location/os/g;->g()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 144
    const/16 v0, 0x11

    invoke-virtual {v1, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 145
    const/4 v0, 0x3

    invoke-interface {p0}, Lcom/google/android/location/os/g;->a()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 146
    invoke-interface {p0}, Lcom/google/android/location/os/g;->i()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 147
    const/16 v0, 0x10

    invoke-interface {p0}, Lcom/google/android/location/os/g;->e()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 149
    :cond_55
    invoke-interface {p0}, Lcom/google/android/location/os/g;->j()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 150
    const/16 v0, 0xd

    invoke-interface {p0}, Lcom/google/android/location/os/g;->k()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 152
    :cond_65
    if-eqz p4, :cond_77

    invoke-interface {p0}, Lcom/google/android/location/os/g;->l()Z

    move-result v0

    if-eqz v0, :cond_77

    .line 153
    const/16 v0, 0xa

    invoke-interface {p0}, Lcom/google/android/location/os/g;->m()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 155
    :cond_77
    return-object v1
.end method

.method protected static a(Lcom/google/android/location/e/E;Lcom/google/android/location/os/g;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 165
    if-eqz p0, :cond_5

    if-nez p1, :cond_6

    .line 169
    :cond_5
    :goto_5
    return v0

    .line 168
    :cond_6
    iget-wide v1, p0, Lcom/google/android/location/e/E;->a:J

    invoke-interface {p1}, Lcom/google/android/location/os/g;->f()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 169
    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    const-wide/32 v3, 0xafc80

    cmp-long v1, v1, v3

    if-gtz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method protected static a(Lcom/google/android/location/os/g;Lcom/google/android/location/os/g;I)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-interface {p0}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v0

    invoke-interface {p0}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/google/android/location/os/g;->b()D

    move-result-wide v4

    invoke-interface {p1}, Lcom/google/android/location/os/g;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/g/c;->c(DDDD)D

    move-result-wide v0

    .line 104
    int-to-double v2, p2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1b

    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private final d()Ljava/lang/String;
    .registers 3

    .prologue
    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fault from instance of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()V
    .registers 7

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 182
    :cond_1
    iget-object v1, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    .line 183
    iget-object v2, p0, Lcom/google/android/location/a;->b:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    .line 184
    sget-object v4, Lcom/google/android/location/a$1;->a:[I

    iget-object v5, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    invoke-virtual {v5}, Lcom/google/android/location/a$c;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_4a

    .line 213
    :goto_16
    iget-object v2, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    if-eq v1, v2, :cond_1a

    .line 216
    :cond_1a
    if-nez v0, :cond_1

    .line 217
    return-void

    .line 186
    :pswitch_1d
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->a(J)Z

    move-result v0

    goto :goto_16

    .line 189
    :pswitch_22
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->b(J)Z

    move-result v0

    goto :goto_16

    .line 192
    :pswitch_27
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->c(J)Z

    move-result v0

    goto :goto_16

    .line 195
    :pswitch_2c
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->d(J)Z

    move-result v0

    goto :goto_16

    .line 198
    :pswitch_31
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->e(J)Z

    move-result v0

    goto :goto_16

    .line 201
    :pswitch_36
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->f(J)Z

    move-result v0

    goto :goto_16

    .line 204
    :pswitch_3b
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->g(J)Z

    move-result v0

    goto :goto_16

    .line 207
    :pswitch_40
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->h(J)Z

    move-result v0

    goto :goto_16

    .line 210
    :pswitch_45
    invoke-virtual {p0, v2, v3}, Lcom/google/android/location/a;->i(J)Z

    move-result v0

    goto :goto_16

    .line 184
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_22
        :pswitch_27
        :pswitch_2c
        :pswitch_31
        :pswitch_36
        :pswitch_3b
        :pswitch_40
        :pswitch_45
    .end packed-switch
.end method

.method a(I)V
    .registers 2
    .parameter

    .prologue
    .line 275
    return-void
.end method

.method a(IIZ)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 316
    return-void
.end method

.method a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 361
    return-void
.end method

.method a(Lcom/google/android/location/e/E;)V
    .registers 2
    .parameter

    .prologue
    .line 298
    return-void
.end method

.method a(Lcom/google/android/location/e/e;)V
    .registers 2
    .parameter

    .prologue
    .line 332
    return-void
.end method

.method a(Lcom/google/android/location/os/g;)V
    .registers 2
    .parameter

    .prologue
    .line 290
    return-void
.end method

.method a(Lcom/google/android/location/os/h;)V
    .registers 2
    .parameter

    .prologue
    .line 348
    return-void
.end method

.method a(Lcom/google/android/location/os/i$b;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 368
    return-void
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 282
    return-void
.end method

.method a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 306
    return-void
.end method

.method protected a(J)Z
    .registers 5
    .parameter

    .prologue
    .line 224
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 324
    return-void
.end method

.method b()Z
    .registers 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->a:Lcom/google/android/location/a$c;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/google/android/location/a;->f:Lcom/google/android/location/a$c;

    sget-object v1, Lcom/google/android/location/a$c;->b:Lcom/google/android/location/a$c;

    if-ne v0, v1, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected b(J)Z
    .registers 5
    .parameter

    .prologue
    .line 228
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()V
    .registers 1

    .prologue
    .line 355
    return-void
.end method

.method c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 339
    return-void
.end method

.method protected c(J)Z
    .registers 5
    .parameter

    .prologue
    .line 232
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected d(J)Z
    .registers 5
    .parameter

    .prologue
    .line 236
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected e(J)Z
    .registers 5
    .parameter

    .prologue
    .line 240
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected f(J)Z
    .registers 5
    .parameter

    .prologue
    .line 244
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected g(J)Z
    .registers 5
    .parameter

    .prologue
    .line 248
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {p0}, Lcom/google/android/location/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected h(J)Z
    .registers 4
    .parameter

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method

.method protected i(J)Z
    .registers 4
    .parameter

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method
