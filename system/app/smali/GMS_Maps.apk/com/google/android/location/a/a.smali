.class Lcom/google/android/location/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(Lcom/google/android/location/a/e;J)Lcom/google/android/location/clientlib/NlpActivity;
    .registers 14
    .parameter
    .parameter

    .prologue
    const/16 v9, 0x3a

    const/4 v8, 0x1

    const/16 v7, 0xa

    const-wide v5, 0x3fc3333333333333L

    const/16 v4, 0x64

    .line 18
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fbe4bfd2e946801L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21f

    .line 19
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3f9a84be40420f6fL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    .line 20
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3f924d8fd5cb7910L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    .line 21
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    .line 347
    :goto_36
    return-object v0

    .line 23
    :cond_37
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b866e43aa79bcL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    .line 24
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f23879c4113c686L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    .line 25
    const/16 v0, 0x18

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f201f31f46ed246L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    .line 26
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f121682f944241cL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    .line 27
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f14b599aa60913aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    .line 28
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_36

    .line 30
    :cond_8a
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd64bf8fcd67fd4L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    .line 31
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_36

    .line 33
    :cond_9f
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_36

    .line 37
    :cond_a9
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto :goto_36

    .line 40
    :cond_b3
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe901c92ddbdb5eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    .line 41
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 43
    :cond_cc
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 47
    :cond_d7
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 50
    :cond_e2
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 54
    :cond_eb
    const/16 v0, 0x3c

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4ce6c093d96638L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e5

    .line 55
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fa13be22e5de15dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    .line 56
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2c92ddbdb5d895L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    .line 57
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f58e757928e0c9eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    .line 58
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 60
    :cond_12c
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f22599ed7c6fbd2L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    .line 61
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 63
    :cond_144
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4024a2fd976ff3aeL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    .line 64
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 66
    :cond_161
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 71
    :cond_16c
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 74
    :cond_177
    const-wide v0, 0x3fefae147ae147aeL

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402a5d3bf2f55582L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    .line 75
    invoke-virtual {p1, v9}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f46a4873365881aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    .line 76
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 78
    :cond_1a1
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fb954003254e6e2L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    .line 79
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 81
    :cond_1b5
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 85
    :cond_1c0
    const/16 v0, 0x33

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2e03f705857affL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1da

    .line 86
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 88
    :cond_1da
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 93
    :cond_1e5
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd16af038e29f9dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_214

    .line 94
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002dd40ee06d938L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_209

    .line 95
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 97
    :cond_209
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 100
    :cond_214
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 105
    :cond_21f
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa5171e29b6b2afL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69a

    .line 106
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa8f3eccc469510L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2ee

    .line 107
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd12bb6672fba02L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e3

    .line 108
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40005a5ebb7739f3L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c0

    .line 109
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002bf2cb641700dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b7

    .line 110
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f593b3a68b19a41L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2ac

    .line 111
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021cde3fbbd7b20L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a1

    .line 112
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2f969e3c968944L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_296

    .line 113
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 115
    :cond_296
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 118
    :cond_2a1
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 121
    :cond_2ac
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 124
    :cond_2b7
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v9, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 127
    :cond_2c0
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f97af640639d5e5L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d8

    .line 128
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 130
    :cond_2d8
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 134
    :cond_2e3
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 137
    :cond_2ee
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f91737542a23c00L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e4

    .line 138
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd44dbdf8f47304L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d9

    .line 139
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fcec8e68e3ef284L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4bd

    .line 140
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020623bbc6eb0b8L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c2

    .line 141
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3feb905186db50f4L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b7

    .line 142
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f67aeddce7cd035L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_349

    .line 143
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 145
    :cond_349
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40287909aed56b01L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_366

    .line 146
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 148
    :cond_366
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa4422036006d0dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37e

    .line 149
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 151
    :cond_37e
    iget-wide v0, p1, Lcom/google/android/location/a/e;->c:D

    const-wide v2, 0x4016e28134480629L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_394

    .line 152
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 154
    :cond_394
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f616872b020c49cL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3ac

    .line 155
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 157
    :cond_3ac
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 164
    :cond_3b7
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 167
    :cond_3c2
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022fd1e53a81dc1L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b4

    .line 168
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5d4738a3b57c4eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_419

    .line 169
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f45f45e0b4e11dcL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f8

    .line 170
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 172
    :cond_3f8
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f71b60ae9680e06L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40e

    .line 173
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 175
    :cond_40e
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 179
    :cond_419
    invoke-virtual {p1, v7}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6e060fe47991bcL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a9

    .line 180
    iget-wide v0, p1, Lcom/google/android/location/a/e;->c:D

    const-wide v2, 0x4019e81beb18116fL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49e

    .line 181
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4fd9ba1b1960faL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44b

    .line 182
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 184
    :cond_44b
    invoke-virtual {p1, v9}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3ffb480a5accd5L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_463

    .line 185
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 187
    :cond_463
    const/16 v0, 0x25

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3a79fec99f1ae3L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47b

    .line 188
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 190
    :cond_47b
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021b79acffa7eb7L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_493

    .line 191
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 193
    :cond_493
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 200
    :cond_49e
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 203
    :cond_4a9
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 207
    :cond_4b4
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 211
    :cond_4bd
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb82a23bff8a8f4L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_536

    .line 212
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd55553ef6b5d46L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4fb

    .line 213
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4028a45069a4df48L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f0

    .line 214
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v9, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 216
    :cond_4f0
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 219
    :cond_4fb
    const/16 v0, 0x3c

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f253bd1676640a7L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_515

    .line 220
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 222
    :cond_515
    iget-wide v0, p1, Lcom/google/android/location/a/e;->a:D

    const-wide v2, 0x3fc7eb313be22e5eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52b

    .line 223
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 225
    :cond_52b
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 230
    :cond_536
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402189d6f112fd33L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59a

    .line 231
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f500a393ee5eeddL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58f

    .line 232
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f680d3cff64cf8dL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_586

    .line 233
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4908e581cf7879L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57b

    .line 234
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->a:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 236
    :cond_57b
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 239
    :cond_586
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 242
    :cond_58f
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 245
    :cond_59a
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f548ba83f4eca68L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b4

    .line 246
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 248
    :cond_5b4
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5e42e126202539L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5ce

    .line 249
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 251
    :cond_5ce
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 258
    :cond_5d9
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 261
    :cond_5e4
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide v2, 0x3fd054bcf0b6b6e1L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_662

    .line 262
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc1b645a1cac083L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63c

    .line 263
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f804ab606b7aa26L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_631

    .line 264
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400560a2877ee4e2L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_626

    .line 265
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 267
    :cond_626
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 270
    :cond_631
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 273
    :cond_63c
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4035922281344806L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_657

    .line 274
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 276
    :cond_657
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 280
    :cond_662
    iget-wide v0, p1, Lcom/google/android/location/a/e;->b:D

    const-wide/high16 v2, 0x3fda

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68f

    .line 281
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f61d9b1b79d909fL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_684

    .line 282
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 284
    :cond_684
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 287
    :cond_68f
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 293
    :cond_69a
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff68a9cdc443915L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e3

    .line 294
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f805e1c15097c81L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c2

    .line 295
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 297
    :cond_6c2
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fe2cba94bbe4474L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d8

    .line 298
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v9, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 300
    :cond_6d8
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 304
    :cond_6e3
    invoke-virtual {p1, v8}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3feea88ce703afb8L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7ee

    .line 305
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff677fae3608d09L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_721

    .line 306
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9cf5f4e4430b18L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_718

    .line 307
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 309
    :cond_718
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 312
    :cond_721
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91b25f633ce63aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73b

    .line 313
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 315
    :cond_73b
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40135cbc48f10a9aL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_753

    .line 316
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 318
    :cond_753
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020f01d5c31593eL

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e3

    .line 319
    invoke-virtual {p1, v9}, Lcom/google/android/location/a/e;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7a5119ce075f70L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7da

    .line 320
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/location/a/e;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400044cc6822ff09L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7cf

    .line 321
    const-wide v0, 0x3fee666666666666L

    invoke-virtual {p1, v0, v1}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40316eba9d1f6018L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c4

    .line 322
    iget-wide v0, p1, Lcom/google/android/location/a/e;->c:D

    const-wide v2, 0x4017d312b1b36bd3L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7bb

    .line 323
    invoke-virtual {p1, v5, v6}, Lcom/google/android/location/a/e;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401a6ce2089e3433L

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b0

    .line 324
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 326
    :cond_7b0
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 329
    :cond_7bb
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 332
    :cond_7c4
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 335
    :cond_7cf
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->b:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 338
    :cond_7da
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    invoke-direct {v0, v1, v4, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 341
    :cond_7e3
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36

    .line 347
    :cond_7ee
    new-instance v0, Lcom/google/android/location/clientlib/NlpActivity;

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->c:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/google/android/location/clientlib/NlpActivity;-><init>(Lcom/google/android/location/clientlib/NlpActivity$ActivityType;IJ)V

    goto/16 :goto_36
.end method
