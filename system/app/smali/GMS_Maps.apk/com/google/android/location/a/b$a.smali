.class Lcom/google/android/location/a/b$a;
.super Lcom/google/android/location/a/b$d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/a/b;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/clientlib/NlpActivity;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Z

.field private f:Lcom/google/android/location/a/c;


# direct methods
.method private constructor <init>(Lcom/google/android/location/a/b;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 196
    iput-object p1, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/b$d;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    .line 216
    iput v2, p0, Lcom/google/android/location/a/b$a;->d:I

    .line 217
    iput-boolean v2, p0, Lcom/google/android/location/a/b$a;->e:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 196
    invoke-direct {p0, p1}, Lcom/google/android/location/a/b$a;-><init>(Lcom/google/android/location/a/b;)V

    return-void
.end method

.method private b(Lcom/google/android/location/clientlib/NlpActivity;)I
    .registers 5
    .parameter

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_2b

    .line 351
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/clientlib/NlpActivity;

    .line 352
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    if-eq v2, v0, :cond_27

    .line 353
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    .line 356
    :goto_26
    return v0

    .line 350
    :cond_27
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 356
    :cond_2b
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_26
.end method

.method private g()V
    .registers 3

    .prologue
    .line 239
    iget v0, p0, Lcom/google/android/location/a/b$a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/a/b$a;->d:I

    .line 241
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->f:Lcom/google/android/location/a/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/a/c;->a(I)V

    .line 242
    return-void
.end method

.method private h()V
    .registers 5

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/location/a/b$a;->f()Z

    move-result v0

    if-nez v0, :cond_13

    .line 258
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    .line 260
    :cond_13
    return-void
.end method

.method private i()Z
    .registers 5

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    if-eqz v0, :cond_35

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v2}, Lcom/google/android/location/a/b;->i(Lcom/google/android/location/a/b;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x15f90

    cmp-long v0, v0, v2

    if-gez v0, :cond_35

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->b()D

    move-result-wide v0

    const-wide v2, 0x3fe6666666666666L

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_35

    const/4 v0, 0x1

    :goto_34
    return v0

    :cond_35
    const/4 v0, 0x0

    goto :goto_34
.end method


# virtual methods
.method protected a(Lcom/google/android/location/a/c$a;)Lcom/google/android/location/a/c;
    .registers 5
    .parameter

    .prologue
    .line 328
    new-instance v0, Lcom/google/android/location/a/c;

    iget-object v1, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/a/k;

    invoke-direct {v2}, Lcom/google/android/location/a/k;-><init>()V

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/location/a/c;-><init>(Lcom/google/android/location/os/i;Lcom/google/android/location/a/c$a;Lcom/google/android/location/a/k;)V

    return-object v0
.end method

.method protected a()V
    .registers 5

    .prologue
    .line 222
    invoke-virtual {p0, p0}, Lcom/google/android/location/a/b$a;->a(Lcom/google/android/location/a/c$a;)Lcom/google/android/location/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/a/b$a;->f:Lcom/google/android/location/a/c;

    .line 223
    invoke-virtual {p0}, Lcom/google/android/location/a/b$a;->f()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 225
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    .line 229
    :goto_19
    return-void

    .line 227
    :cond_1a
    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->g()V

    goto :goto_19
.end method

.method public a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 268
    iget-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    if-eqz v0, :cond_8

    .line 318
    :goto_7
    return-void

    .line 272
    :cond_8
    if-nez p1, :cond_17

    .line 274
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_7

    .line 277
    :cond_17
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->h(Lcom/google/android/location/a/b;)Lcom/google/android/location/clientlib/NlpActivity;

    move-result-object v0

    if-eqz v0, :cond_49

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_49

    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v2}, Lcom/google/android/location/a/b;->h(Lcom/google/android/location/a/b;)Lcom/google/android/location/clientlib/NlpActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v2

    if-ne v0, v2, :cond_49

    .line 281
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 282
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_7

    .line 287
    :cond_49
    invoke-direct {p0, p1}, Lcom/google/android/location/a/b$a;->b(Lcom/google/android/location/clientlib/NlpActivity;)I

    move-result v0

    add-int/lit8 v2, v0, 0x1

    .line 288
    if-lt v2, v6, :cond_85

    const/4 v0, 0x1

    .line 289
    :goto_52
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v3

    sget-object v4, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->e:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-eq v3, v4, :cond_8d

    if-eqz v0, :cond_8d

    .line 291
    invoke-virtual {p1}, Lcom/google/android/location/clientlib/NlpActivity;->getActivity()Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/clientlib/NlpActivity$ActivityType;->d:Lcom/google/android/location/clientlib/NlpActivity$ActivityType;

    if-eq v0, v1, :cond_87

    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->i()Z

    move-result v0

    if-eqz v0, :cond_87

    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0}, Lcom/google/android/location/a/b;->f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/e/B;->a:Lcom/google/android/location/e/B;

    if-ne v0, v1, :cond_87

    .line 299
    :goto_78
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_7

    :cond_85
    move v0, v1

    .line 288
    goto :goto_52

    .line 297
    :cond_87
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/clientlib/NlpActivity;)V

    goto :goto_78

    .line 300
    :cond_8d
    iget v0, p0, Lcom/google/android/location/a/b$a;->d:I

    rsub-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    const/16 v2, 0x8

    if-le v0, v2, :cond_a4

    .line 304
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-direct {v1, v2, v5}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto/16 :goto_7

    .line 306
    :cond_a4
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_ad

    .line 311
    :cond_ad
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    :goto_b2
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v6, :cond_c0

    .line 313
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_b2

    .line 316
    :cond_c0
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    iget-object v1, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    invoke-static {v1}, Lcom/google/android/location/a/b;->b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x1388

    add-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;J)V

    goto/16 :goto_7
.end method

.method public a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    if-eqz v0, :cond_5

    .line 365
    :goto_4
    return-void

    .line 364
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_4
.end method

.method protected a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 246
    invoke-super {p0, p1}, Lcom/google/android/location/a/b$d;->a(Z)V

    .line 247
    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->h()V

    .line 248
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 252
    invoke-super {p0, p1, p2}, Lcom/google/android/location/a/b$d;->a(ZLjava/lang/String;)V

    .line 253
    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->h()V

    .line 254
    return-void
.end method

.method public a_()V
    .registers 5

    .prologue
    .line 369
    iget-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    if-eqz v0, :cond_5

    .line 373
    :goto_4
    return-void

    .line 372
    :cond_5
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    new-instance v1, Lcom/google/android/location/a/b$c;

    iget-object v2, p0, Lcom/google/android/location/a/b$a;->a:Lcom/google/android/location/a/b;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/location/a/b$c;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    invoke-static {v0, v1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V

    goto :goto_4
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 233
    invoke-super {p0}, Lcom/google/android/location/a/b$d;->b()V

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/a/b$a;->e:Z

    .line 235
    iget-object v0, p0, Lcom/google/android/location/a/b$a;->f:Lcom/google/android/location/a/c;

    invoke-virtual {v0}, Lcom/google/android/location/a/c;->a()V

    .line 236
    return-void
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/google/android/location/a/b$a;->g()V

    .line 334
    return-void
.end method
