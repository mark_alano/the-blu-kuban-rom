.class public Lcom/google/android/location/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/b$1;,
        Lcom/google/android/location/a/b$d;,
        Lcom/google/android/location/a/b$a;,
        Lcom/google/android/location/a/b$c;,
        Lcom/google/android/location/a/b$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/os/i;

.field private final b:Z

.field private c:Lcom/google/android/location/a/b$d;

.field private d:J

.field private e:Lcom/google/android/location/clientlib/NlpActivity;

.field private f:J

.field private g:Lcom/google/android/location/a/n$b;

.field private h:J

.field private i:Z

.field private j:Z

.field private k:I

.field private l:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m:I

.field private n:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/location/a/b$b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/a/b$b;-><init>(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$1;)V

    iput-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    .line 36
    iput-wide v2, p0, Lcom/google/android/location/a/b;->d:J

    .line 37
    iput-object v1, p0, Lcom/google/android/location/a/b;->e:Lcom/google/android/location/clientlib/NlpActivity;

    .line 38
    iput-wide v2, p0, Lcom/google/android/location/a/b;->f:J

    .line 39
    iput-object v1, p0, Lcom/google/android/location/a/b;->g:Lcom/google/android/location/a/n$b;

    .line 40
    iput-wide v2, p0, Lcom/google/android/location/a/b;->h:J

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/a/b;->i:Z

    .line 42
    iput-boolean v4, p0, Lcom/google/android/location/a/b;->j:Z

    .line 47
    iput v4, p0, Lcom/google/android/location/a/b;->k:I

    .line 51
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    .line 53
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/a/b;->m:I

    .line 54
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    .line 58
    iput-object p1, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    .line 59
    invoke-interface {p1}, Lcom/google/android/location/os/i;->A()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/a/b;->b:Z

    .line 60
    iget-boolean v0, p0, Lcom/google/android/location/a/b;->b:Z

    if-nez v0, :cond_3e

    .line 66
    :cond_3e
    return-void
.end method

.method private a()V
    .registers 5

    .prologue
    const-wide/16 v2, -0x1

    .line 396
    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_11

    .line 398
    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/location/os/i;->a(I)V

    .line 399
    iput-wide v2, p0, Lcom/google/android/location/a/b;->d:J

    .line 401
    :cond_11
    return-void
.end method

.method private a(J)V
    .registers 5
    .parameter

    .prologue
    .line 387
    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_f

    .line 388
    iput-wide p1, p0, Lcom/google/android/location/a/b;->d:J

    .line 391
    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    const/16 v1, 0x8

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 393
    :cond_f
    return-void
.end method

.method private a(Lcom/google/android/location/a/b$d;)V
    .registers 3
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->b()V

    .line 71
    iput-object p1, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    .line 73
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->a()V

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;J)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a/b;->a(J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;Lcom/google/android/location/a/b$d;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/a/b$d;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/location/a/b;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    return-void
.end method

.method private a(Lcom/google/android/location/clientlib/NlpActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 377
    iput-object p1, p0, Lcom/google/android/location/a/b;->e:Lcom/google/android/location/clientlib/NlpActivity;

    .line 378
    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0, p1}, Lcom/google/android/location/os/i;->a(Lcom/google/android/location/clientlib/NlpActivity;)V

    .line 379
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/a/b;)Z
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/a/b;->b:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/a/b;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/android/location/a/b;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/location/a/b;)Lcom/google/android/location/os/i;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    return-object v0
.end method

.method private b(Z)V
    .registers 8
    .parameter

    .prologue
    const-wide/16 v2, 0x3e8

    .line 549
    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_2f

    .line 550
    iget v0, p0, Lcom/google/android/location/a/b;->m:I

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    .line 556
    :goto_10
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/google/android/location/a/b;->n:J

    const-wide/16 v4, 0xe10

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    .line 557
    iget v0, p0, Lcom/google/android/location/a/b;->k:I

    if-nez v0, :cond_46

    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_46

    .line 558
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->e()V

    .line 562
    :cond_2e
    :goto_2e
    return-void

    .line 552
    :cond_2f
    iget v1, p0, Lcom/google/android/location/a/b;->m:I

    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    goto :goto_10

    .line 559
    :cond_46
    if-eqz p1, :cond_2e

    .line 560
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->d()V

    goto :goto_2e
.end method

.method static synthetic b(Lcom/google/android/location/a/b;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/android/location/a/b;->j:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/location/a/b;)J
    .registers 3
    .parameter

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/android/location/a/b;->n:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/location/a/b;)V
    .registers 1
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/location/a/b;->a()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/location/a/b;)J
    .registers 3
    .parameter

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/location/a/b;)Lcom/google/android/location/a/n$b;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/a/b;->g:Lcom/google/android/location/a/n$b;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/a/b;)J
    .registers 3
    .parameter

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/android/location/a/b;->f:J

    return-wide v0
.end method

.method static synthetic h(Lcom/google/android/location/a/b;)Lcom/google/android/location/clientlib/NlpActivity;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/location/a/b;->e:Lcom/google/android/location/clientlib/NlpActivity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/location/a/b;)J
    .registers 3
    .parameter

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/android/location/a/b;->h:J

    return-wide v0
.end method

.method static synthetic j(Lcom/google/android/location/a/b;)Z
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/a/b;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/location/a/b;)Z
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/location/a/b;->j:Z

    return v0
.end method


# virtual methods
.method public a(I)V
    .registers 8
    .parameter

    .prologue
    const-wide/16 v4, -0x1

    .line 484
    const/16 v0, 0x8

    if-ne p1, v0, :cond_c

    .line 485
    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_d

    .line 499
    :cond_c
    :goto_c
    return-void

    .line 488
    :cond_d
    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/a/b;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_21

    .line 491
    iget-wide v0, p0, Lcom/google/android/location/a/b;->d:J

    .line 492
    iput-wide v4, p0, Lcom/google/android/location/a/b;->d:J

    .line 493
    invoke-direct {p0, v0, v1}, Lcom/google/android/location/a/b;->a(J)V

    goto :goto_c

    .line 495
    :cond_21
    iput-wide v4, p0, Lcom/google/android/location/a/b;->d:J

    .line 496
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0}, Lcom/google/android/location/a/b$d;->c()V

    goto :goto_c
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 530
    iget v0, p0, Lcom/google/android/location/a/b;->k:I

    if-le p1, v0, :cond_d

    const/4 v0, 0x1

    .line 531
    :goto_5
    iput p1, p0, Lcom/google/android/location/a/b;->k:I

    .line 532
    iput p2, p0, Lcom/google/android/location/a/b;->m:I

    .line 533
    invoke-direct {p0, v0}, Lcom/google/android/location/a/b;->b(Z)V

    .line 534
    return-void

    .line 530
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(Lcom/google/android/location/a/n$b;)V
    .registers 4
    .parameter

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b$d;->a(Lcom/google/android/location/a/n$b;)V

    .line 524
    iput-object p1, p0, Lcom/google/android/location/a/b;->g:Lcom/google/android/location/a/n$b;

    .line 525
    iget-object v0, p0, Lcom/google/android/location/a/b;->a:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/a/b;->h:J

    .line 526
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0, p1}, Lcom/google/android/location/a/b$d;->a(Z)V

    .line 506
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/location/a/b;->c:Lcom/google/android/location/a/b$d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/a/b$d;->a(ZLjava/lang/String;)V

    .line 516
    return-void
.end method

.method public b(I)V
    .registers 4
    .parameter

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 539
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/a/b;->b(Z)V

    .line 540
    return-void
.end method

.method public c(I)V
    .registers 4
    .parameter

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/location/a/b;->l:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 545
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/b;->b(Z)V

    .line 546
    return-void
.end method
