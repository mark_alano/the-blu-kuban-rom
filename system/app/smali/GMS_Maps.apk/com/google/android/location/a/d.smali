.class Lcom/google/android/location/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/d$1;,
        Lcom/google/android/location/a/d$b;,
        Lcom/google/android/location/a/d$c;,
        Lcom/google/android/location/a/d$a;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    return-void
.end method

.method private a([DD)D
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 175
    .line 176
    aget-wide v3, p1, v2

    cmpl-double v0, v3, p2

    if-ltz v0, :cond_1d

    move v0, v1

    .line 177
    :goto_9
    array-length v5, p1

    move v4, v2

    move v3, v2

    :goto_c
    if-ge v4, v5, :cond_2a

    aget-wide v6, p1, v4

    .line 178
    if-eqz v0, :cond_1f

    cmpg-double v8, v6, p2

    if-gez v8, :cond_1f

    .line 180
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    .line 177
    :cond_1a
    :goto_1a
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    :cond_1d
    move v0, v2

    .line 176
    goto :goto_9

    .line 181
    :cond_1f
    if-nez v0, :cond_1a

    cmpl-double v6, v6, p2

    if-ltz v6, :cond_1a

    .line 183
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v1

    goto :goto_1a

    .line 186
    :cond_2a
    int-to-double v0, v3

    array-length v2, p1

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private a([D)Lcom/google/android/location/a/d$c;
    .registers 10
    .parameter

    .prologue
    const-wide/16 v1, 0x0

    .line 156
    .line 158
    array-length v5, p1

    const/4 v0, 0x0

    move-wide v3, v1

    :goto_5
    if-ge v0, v5, :cond_f

    aget-wide v6, p1, v0

    .line 159
    add-double/2addr v3, v6

    .line 160
    mul-double/2addr v6, v6

    add-double/2addr v1, v6

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 162
    :cond_f
    array-length v0, p1

    int-to-double v5, v0

    div-double v5, v3, v5

    .line 164
    array-length v0, p1

    .line 165
    if-nez v0, :cond_1e

    const-wide/high16 v0, 0x7ff8

    .line 166
    :goto_18
    new-instance v2, Lcom/google/android/location/a/d$c;

    invoke-direct {v2, v5, v6, v0, v1}, Lcom/google/android/location/a/d$c;-><init>(DD)V

    return-object v2

    .line 165
    :cond_1e
    mul-double/2addr v3, v5

    sub-double/2addr v1, v3

    int-to-double v3, v0

    div-double v0, v1, v3

    goto :goto_18
.end method

.method private a(Ljava/util/List;JZ)[D
    .registers 15
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;JZ)[D"
        }
    .end annotation

    .prologue
    .line 131
    const/4 v0, 0x3

    new-array v4, v0, [D

    .line 132
    const/4 v1, 0x0

    .line 133
    if-eqz p4, :cond_47

    const/4 v0, -0x1

    move v3, v0

    .line 134
    :goto_8
    if-eqz p4, :cond_4a

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_10
    move v2, v1

    move v1, v0

    :goto_12
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_30

    if-ltz v1, :cond_30

    .line 136
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/j;

    .line 137
    if-nez p4, :cond_28

    iget-wide v5, v0, Lcom/google/android/location/a/j;->a:J

    cmp-long v5, v5, p2

    if-gtz v5, :cond_30

    :cond_28
    if-eqz p4, :cond_4c

    iget-wide v5, v0, Lcom/google/android/location/a/j;->a:J

    cmp-long v5, v5, p2

    if-gez v5, :cond_4c

    .line 146
    :cond_30
    const/4 v0, 0x0

    aget-wide v5, v4, v0

    int-to-double v7, v2

    div-double/2addr v5, v7

    aput-wide v5, v4, v0

    .line 147
    const/4 v0, 0x1

    aget-wide v5, v4, v0

    int-to-double v7, v2

    div-double/2addr v5, v7

    aput-wide v5, v4, v0

    .line 148
    const/4 v0, 0x2

    aget-wide v5, v4, v0

    int-to-double v1, v2

    div-double v1, v5, v1

    aput-wide v1, v4, v0

    .line 149
    return-object v4

    .line 133
    :cond_47
    const/4 v0, 0x1

    move v3, v0

    goto :goto_8

    .line 134
    :cond_4a
    const/4 v0, 0x0

    goto :goto_10

    .line 141
    :cond_4c
    const/4 v5, 0x0

    aget-wide v6, v4, v5

    iget-object v8, v0, Lcom/google/android/location/a/j;->b:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    float-to-double v8, v8

    add-double/2addr v6, v8

    aput-wide v6, v4, v5

    .line 142
    const/4 v5, 0x1

    aget-wide v6, v4, v5

    iget-object v8, v0, Lcom/google/android/location/a/j;->b:[F

    const/4 v9, 0x1

    aget v8, v8, v9

    float-to-double v8, v8

    add-double/2addr v6, v8

    aput-wide v6, v4, v5

    .line 143
    const/4 v5, 0x2

    aget-wide v6, v4, v5

    iget-object v0, v0, Lcom/google/android/location/a/j;->b:[F

    const/4 v8, 0x2

    aget v0, v0, v8

    float-to-double v8, v0

    add-double/2addr v6, v8

    aput-wide v6, v4, v5

    .line 144
    add-int/lit8 v2, v2, 0x1

    .line 135
    add-int v0, v1, v3

    move v1, v0

    goto :goto_12
.end method

.method private a([DJ)[D
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x5

    .line 106
    new-instance v2, Ljava/util/PriorityQueue;

    invoke-direct {v2, v6}, Ljava/util/PriorityQueue;-><init>(I)V

    .line 108
    const/4 v0, 0x0

    :goto_7
    array-length v1, p1

    if-ge v0, v1, :cond_21

    .line 109
    aget-wide v3, p1, v0

    .line 110
    new-instance v1, Lcom/google/android/location/a/d$a;

    const/4 v5, 0x0

    invoke-direct {v1, v0, v3, v4, v5}, Lcom/google/android/location/a/d$a;-><init>(IDLcom/google/android/location/a/d$1;)V

    invoke-virtual {v2, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->size()I

    move-result v1

    if-le v1, v6, :cond_1e

    .line 112
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    .line 108
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 115
    :cond_21
    new-array v3, v6, [D

    .line 116
    long-to-double v0, p2

    const-wide v4, 0x41cdcd6500000000L

    div-double/2addr v0, v4

    .line 117
    array-length v4, p1

    add-int/lit8 v4, v4, 0x1

    .line 118
    int-to-double v5, v4

    div-double v0, v5, v0

    .line 119
    mul-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    div-double v4, v0, v4

    .line 120
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    .line 121
    :goto_3c
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_56

    .line 122
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/d$a;

    .line 123
    invoke-static {v0}, Lcom/google/android/location/a/d$a;->b(Lcom/google/android/location/a/d$a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-double v6, v0

    mul-double/2addr v6, v4

    aput-wide v6, v3, v1

    .line 124
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    .line 125
    goto :goto_3c

    .line 126
    :cond_56
    return-object v3
.end method

.method private b(Ljava/util/List;)Lcom/google/android/location/a/d$b;
    .registers 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;)",
            "Lcom/google/android/location/a/d$b;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/16 v5, 0x40

    const/4 v7, 0x0

    .line 70
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    filled-new-array {v0, v10}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[D

    .line 71
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v7

    .line 72
    :goto_1e
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_46

    .line 73
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/j;

    move v4, v7

    .line 74
    :goto_2b
    if-ge v4, v10, :cond_39

    .line 75
    aget-object v6, v1, v3

    iget-object v8, v0, Lcom/google/android/location/a/j;->b:[F

    aget v8, v8, v4

    float-to-double v8, v8

    aput-wide v8, v6, v4

    .line 74
    add-int/lit8 v4, v4, 0x1

    goto :goto_2b

    .line 77
    :cond_39
    iget-wide v8, v0, Lcom/google/android/location/a/j;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1e

    .line 80
    :cond_46
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v3, v8

    .line 82
    new-instance v0, Lcom/google/android/location/a/i;

    invoke-direct {v0}, Lcom/google/android/location/a/i;-><init>()V

    const/4 v6, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/location/a/i;->a([[DLjava/util/List;JIZ)[D

    move-result-object v0

    .line 86
    new-array v1, v5, [D

    .line 87
    invoke-static {v0, v7, v1, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    array-length v2, v0

    add-int/lit8 v2, v2, -0x40

    .line 90
    new-array v6, v2, [D

    .line 91
    invoke-static {v0, v5, v6, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    new-instance v0, Lcom/google/android/location/a/d$b;

    invoke-direct {v0, v1, v6, v3, v4}, Lcom/google/android/location/a/d$b;-><init>([D[DJ)V

    return-object v0
.end method

.method private c(Ljava/util/List;)[D
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;)[D"
        }
    .end annotation

    .prologue
    .line 196
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [D

    .line 197
    const/4 v0, 0x0

    .line 198
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/j;

    .line 199
    invoke-virtual {v0}, Lcom/google/android/location/a/j;->a()D

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 200
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 202
    :cond_22
    return-object v2
.end method


# virtual methods
.method a(Ljava/util/List;)Lcom/google/android/location/a/e;
    .registers 20
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/j;",
            ">;)",
            "Lcom/google/android/location/a/e;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct/range {p0 .. p1}, Lcom/google/android/location/a/d;->c(Ljava/util/List;)[D

    move-result-object v9

    .line 30
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/location/a/d;->a([D)Lcom/google/android/location/a/d$c;

    move-result-object v2

    .line 31
    invoke-static {v2}, Lcom/google/android/location/a/d$c;->a(Lcom/google/android/location/a/d$c;)D

    move-result-wide v3

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v3, v4}, Lcom/google/android/location/a/d;->a([DD)D

    move-result-wide v5

    .line 32
    invoke-static {v2}, Lcom/google/android/location/a/d$c;->b(Lcom/google/android/location/a/d$c;)D

    move-result-wide v3

    invoke-static {v2}, Lcom/google/android/location/a/d$c;->a(Lcom/google/android/location/a/d$c;)D

    move-result-wide v7

    div-double/2addr v3, v7

    .line 34
    invoke-direct/range {p0 .. p1}, Lcom/google/android/location/a/d;->b(Ljava/util/List;)Lcom/google/android/location/a/d$b;

    move-result-object v11

    .line 35
    invoke-static {v11}, Lcom/google/android/location/a/d$b;->a(Lcom/google/android/location/a/d$b;)[D

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/location/a/d;->a([D)Lcom/google/android/location/a/d$c;

    move-result-object v2

    .line 36
    invoke-static {v2}, Lcom/google/android/location/a/d$c;->b(Lcom/google/android/location/a/d$c;)D

    move-result-wide v7

    invoke-static {v2}, Lcom/google/android/location/a/d$c;->a(Lcom/google/android/location/a/d$c;)D

    move-result-wide v12

    div-double/2addr v7, v12

    .line 39
    invoke-static {v11}, Lcom/google/android/location/a/d$b;->a(Lcom/google/android/location/a/d$b;)[D

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    new-array v2, v2, [D

    .line 40
    invoke-static {v11}, Lcom/google/android/location/a/d$b;->a(Lcom/google/android/location/a/d$b;)[D

    move-result-object v10

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-static {v11}, Lcom/google/android/location/a/d$b;->a(Lcom/google/android/location/a/d$b;)[D

    move-result-object v14

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    invoke-static {v10, v12, v2, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 42
    invoke-static {v11}, Lcom/google/android/location/a/d$b;->b(Lcom/google/android/location/a/d$b;)J

    move-result-wide v12

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v12, v13}, Lcom/google/android/location/a/d;->a([DJ)[D

    move-result-object v12

    .line 45
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/a/j;

    iget-wide v13, v2, Lcom/google/android/location/a/j;->a:J

    const-wide/32 v15, 0x3b9aca00

    add-long/2addr v13, v15

    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v14, v2}, Lcom/google/android/location/a/d;->a(Ljava/util/List;JZ)[D

    move-result-object v13

    .line 47
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/a/j;

    iget-wide v14, v2, Lcom/google/android/location/a/j;->a:J

    const-wide/32 v16, 0x3b9aca00

    sub-long v14, v14, v16

    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/google/android/location/a/d;->a(Ljava/util/List;JZ)[D

    move-result-object v14

    .line 53
    invoke-static {v9}, Ljava/util/Arrays;->sort([D)V

    .line 55
    new-instance v2, Lcom/google/android/location/a/e;

    invoke-static {v11}, Lcom/google/android/location/a/d$b;->a(Lcom/google/android/location/a/d$b;)[D

    move-result-object v10

    invoke-static {v11}, Lcom/google/android/location/a/d$b;->c(Lcom/google/android/location/a/d$b;)[D

    move-result-object v11

    invoke-direct/range {v2 .. v14}, Lcom/google/android/location/a/e;-><init>(DDD[D[D[D[D[D[D)V

    return-object v2
.end method
