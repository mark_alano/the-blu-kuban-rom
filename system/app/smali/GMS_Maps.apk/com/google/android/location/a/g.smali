.class Lcom/google/android/location/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/g$a;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    return-void
.end method

.method private a(Ljava/util/List;II)Ljava/util/List;
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/g$a;",
            ">;II)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/g$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 74
    :goto_9
    if-ge p2, v0, :cond_14

    .line 75
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    add-int/2addr p2, p3

    goto :goto_9

    .line 77
    :cond_14
    return-object v1
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/g$a;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/g$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 39
    if-ne v3, v1, :cond_16

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :goto_15
    return-object v0

    .line 45
    :cond_16
    invoke-direct {p0, p1, v2, v5}, Lcom/google/android/location/a/g;->a(Ljava/util/List;II)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/g;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 46
    invoke-direct {p0, p1, v1, v5}, Lcom/google/android/location/a/g;->a(Ljava/util/List;II)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/g;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 47
    div-int/lit8 v6, v3, 0x2

    .line 49
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 50
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 51
    :goto_32
    if-ge v2, v6, :cond_6c

    .line 52
    const-wide v8, -0x3fe6de04abbbd2e8L

    int-to-double v10, v2

    mul-double/2addr v8, v10

    int-to-double v10, v3

    div-double/2addr v8, v10

    .line 53
    new-instance v10, Lcom/google/android/location/a/g$a;

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-direct {v10, v11, v12, v8, v9}, Lcom/google/android/location/a/g$a;-><init>(DD)V

    .line 54
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    invoke-virtual {v10, v0}, Lcom/google/android/location/a/g$a;->c(Lcom/google/android/location/a/g$a;)Lcom/google/android/location/a/g$a;

    move-result-object v8

    .line 55
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    .line 56
    invoke-virtual {v0, v8}, Lcom/google/android/location/a/g$a;->a(Lcom/google/android/location/a/g$a;)Lcom/google/android/location/a/g$a;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-virtual {v0, v8}, Lcom/google/android/location/a/g$a;->b(Lcom/google/android/location/a/g$a;)Lcom/google/android/location/a/g$a;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_32

    .line 59
    :cond_6c
    invoke-interface {v1, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object v0, v1

    .line 60
    goto :goto_15
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/List;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/g$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_10

    .line 28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must be a power of 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_10
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 31
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    .line 32
    new-instance v0, Lcom/google/android/location/a/g$a;

    const-wide/16 v5, 0x0

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/google/android/location/a/g$a;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 34
    :cond_34
    invoke-direct {p0, v1}, Lcom/google/android/location/a/g;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
