.class Lcom/google/android/location/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/b/i;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/location/a/h;->a:Lcom/google/android/location/b/i;

    .line 40
    return-void
.end method

.method private a(Lcom/google/android/location/a/n$a;Z)Lcom/google/android/location/e/w;
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-virtual {p1}, Lcom/google/android/location/a/n$a;->b()Lcom/google/android/location/e/t;

    move-result-object v1

    .line 87
    if-nez v1, :cond_8

    .line 94
    :cond_7
    :goto_7
    return-object v0

    .line 90
    :cond_8
    if-eqz p2, :cond_11

    iget-object v1, v1, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    .line 91
    :goto_c
    if-eqz v1, :cond_7

    .line 94
    iget-object v0, v1, Lcom/google/android/location/e/o;->c:Lcom/google/android/location/e/w;

    goto :goto_7

    .line 90
    :cond_11
    iget-object v1, v1, Lcom/google/android/location/e/t;->b:Lcom/google/android/location/e/D;

    goto :goto_c
.end method

.method private a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1, p3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Z)Lcom/google/android/location/e/w;

    move-result-object v0

    .line 76
    invoke-direct {p0, p2, p3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Z)Lcom/google/android/location/e/w;

    move-result-object v1

    .line 77
    if-eqz v0, :cond_c

    if-nez v1, :cond_e

    .line 78
    :cond_c
    const/4 v0, 0x0

    .line 82
    :goto_d
    return-object v0

    .line 80
    :cond_e
    invoke-static {v0, v1}, Lcom/google/android/location/g/c;->a(Lcom/google/android/location/e/w;Lcom/google/android/location/e/w;)I

    move-result v1

    .line 81
    if-eqz p3, :cond_20

    const/16 v0, 0x1388

    .line 82
    :goto_16
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_d

    .line 81
    :cond_20
    const/16 v0, 0x3e8

    goto :goto_16
.end method

.method private a(Ljava/util/Set;)Ljava/lang/Double;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Double;"
        }
    .end annotation

    .prologue
    .line 125
    const/4 v1, 0x0

    .line 126
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 127
    iget-object v3, p0, Lcom/google/android/location/a/h;->a:Lcom/google/android/location/b/i;

    invoke-virtual {v3, v0}, Lcom/google/android/location/b/i;->a(Ljava/lang/Object;)Lcom/google/android/location/b/a;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_36

    .line 129
    invoke-virtual {v0}, Lcom/google/android/location/b/a;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/C;

    iget v0, v0, Lcom/google/android/location/e/C;->c:I

    div-int/lit16 v0, v0, 0x3e8

    .line 130
    if-eqz v1, :cond_2e

    int-to-double v3, v0

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    cmpg-double v3, v3, v5

    if-gez v3, :cond_36

    .line 131
    :cond_2e
    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_33
    move-object v1, v0

    .line 134
    goto :goto_5

    .line 135
    :cond_35
    return-object v1

    :cond_36
    move-object v0, v1

    goto :goto_33
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;
    .registers 9
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Double;"
        }
    .end annotation

    .prologue
    .line 144
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 145
    if-nez v0, :cond_12

    .line 146
    const/4 v0, 0x0

    .line 148
    :goto_11
    return-object v0

    :cond_12
    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v1

    int-to-double v1, v1

    int-to-double v3, v0

    div-double v0, v1, v3

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_11
.end method

.method private a(Lcom/google/android/location/e/E;)Ljava/util/Set;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/e/E;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 153
    const/4 v0, 0x0

    :goto_6
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v2

    if-ge v0, v2, :cond_18

    .line 154
    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 156
    :cond_18
    return-object v1
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 161
    invoke-interface {v0, p2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 162
    return-object v0
.end method

.method private a(Ljava/util/List;Ljava/util/Map;)V
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 63
    sget-object v2, Lcom/google/android/location/a/m$a;->e:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    .line 65
    sget-object v2, Lcom/google/android/location/a/m$a;->f:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    .line 67
    sget-object v2, Lcom/google/android/location/a/m$a;->g:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    .line 69
    sget-object v2, Lcom/google/android/location/a/m$a;->h:Lcom/google/android/location/a/m$a;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/a/n$a;

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/a/n$a;Lcom/google/android/location/a/n$a;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    .line 71
    return-void
.end method

.method private a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    if-eqz p3, :cond_5

    .line 116
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    :cond_5
    return-void
.end method

.method private b(Ljava/util/List;Ljava/util/Map;)V
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/e/E;)Ljava/util/Set;

    move-result-object v1

    .line 100
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/e/E;)Ljava/util/Set;

    move-result-object v2

    .line 101
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/a/h;->a(Lcom/google/android/location/e/E;)Ljava/util/Set;

    move-result-object v0

    .line 102
    invoke-direct {p0, v1, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    .line 103
    sget-object v4, Lcom/google/android/location/a/m$a;->a:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, v1, v0, v3}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {p0, p2, v4, v1}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    .line 105
    sget-object v1, Lcom/google/android/location/a/m$a;->b:Lcom/google/android/location/a/m$a;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-interface {p2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v1, Lcom/google/android/location/a/m$a;->c:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, v3}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v3

    invoke-direct {p0, p2, v1, v3}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    .line 108
    invoke-direct {p0, v2, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 109
    sget-object v3, Lcom/google/android/location/a/m$a;->d:Lcom/google/android/location/a/m$a;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/location/a/h;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v3, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/Map;Lcom/google/android/location/a/m$a;Ljava/lang/Double;)V

    .line 111
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/Map;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/a/m$a;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_24

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t compute features for history of size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 56
    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/h;->b(Ljava/util/List;Ljava/util/Map;)V

    .line 57
    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/List;Ljava/util/Map;)V

    .line 58
    return-object v0
.end method
