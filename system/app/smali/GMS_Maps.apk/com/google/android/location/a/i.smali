.class Lcom/google/android/location/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a([D[D)D
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 138
    const-wide/16 v1, 0x0

    .line 139
    const/4 v0, 0x0

    :goto_3
    array-length v3, p1

    if-ge v0, v3, :cond_f

    .line 140
    aget-wide v3, p1, v0

    aget-wide v5, p2, v0

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 142
    :cond_f
    return-wide v1
.end method

.method a([D)Ljava/util/List;
    .registers 16
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([D)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 55
    array-length v2, p1

    .line 56
    mul-int/lit8 v3, v2, 0x2

    .line 57
    new-array v4, v3, [D

    .line 58
    invoke-virtual {p0, v2}, Lcom/google/android/location/a/i;->a(I)[D

    move-result-object v5

    move v0, v1

    .line 59
    :goto_b
    if-ge v0, v2, :cond_17

    .line 60
    aget-wide v6, p1, v0

    aget-wide v8, v5, v0

    mul-double/2addr v6, v8

    aput-wide v6, v4, v0

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 63
    :goto_18
    if-ge v0, v3, :cond_21

    .line 64
    const-wide/16 v5, 0x0

    aput-wide v5, v4, v0

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 66
    :cond_21
    new-instance v5, Ljava/util/ArrayList;

    array-length v0, v4

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 67
    array-length v6, v4

    move v0, v1

    :goto_29
    if-ge v0, v6, :cond_37

    aget-wide v7, v4, v0

    .line 68
    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_29

    .line 70
    :cond_37
    new-instance v0, Lcom/google/android/location/a/g;

    invoke-direct {v0}, Lcom/google/android/location/a/g;-><init>()V

    invoke-virtual {v0, v5}, Lcom/google/android/location/a/g;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 72
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 73
    const-wide/high16 v6, 0x3ff0

    mul-int v0, v3, v3

    int-to-double v8, v0

    div-double/2addr v6, v8

    .line 74
    new-instance v3, Ljava/lang/Double;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    iget-wide v8, v0, Lcom/google/android/location/a/g$a;->a:D

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    iget-wide v0, v0, Lcom/google/android/location/a/g$a;->a:D

    mul-double/2addr v0, v8

    invoke-direct {v3, v0, v1}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    const/4 v0, 0x1

    move v1, v0

    :goto_6f
    if-ge v1, v2, :cond_8f

    .line 76
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/g$a;

    .line 77
    new-instance v3, Ljava/lang/Double;

    iget-wide v8, v0, Lcom/google/android/location/a/g$a;->a:D

    iget-wide v10, v0, Lcom/google/android/location/a/g$a;->a:D

    mul-double/2addr v8, v10

    iget-wide v10, v0, Lcom/google/android/location/a/g$a;->b:D

    iget-wide v12, v0, Lcom/google/android/location/a/g$a;->b:D

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    mul-double/2addr v8, v6

    invoke-direct {v3, v8, v9}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6f

    .line 81
    :cond_8f
    return-object v5
.end method

.method a(I)[D
    .registers 14
    .parameter

    .prologue
    const-wide/high16 v10, 0x4010

    const-wide/high16 v8, 0x4000

    .line 39
    new-array v1, p1, [D

    .line 40
    const/4 v0, 0x0

    :goto_7
    if-ge v0, p1, :cond_2b

    .line 41
    int-to-double v2, v0

    add-int/lit8 v4, p1, -0x1

    int-to-double v4, v4

    div-double/2addr v4, v8

    sub-double/2addr v2, v4

    neg-double v2, v2

    int-to-double v4, v0

    add-int/lit8 v6, p1, -0x1

    int-to-double v6, v6

    div-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    add-int/lit8 v4, p1, -0x1

    int-to-double v4, v4

    div-double/2addr v4, v10

    add-int/lit8 v6, p1, -0x1

    int-to-double v6, v6

    div-double/2addr v6, v10

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 44
    :cond_2b
    return-object v1
.end method

.method a([[DLjava/util/List;JIZ)[D
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[D",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;JIZ)[D"
        }
    .end annotation

    .prologue
    .line 192
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p5

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/a/i;->a([[DLjava/util/List;IJ)[[D

    move-result-object v3

    .line 194
    new-array v4, p5, [D

    .line 195
    new-array v5, p5, [D

    .line 197
    const/4 v0, 0x3

    new-array v1, v0, [D

    fill-array-data v1, :array_84

    .line 198
    const/4 v0, 0x0

    :goto_14
    if-ge v0, p5, :cond_3a

    .line 199
    aget-object v2, v3, v0

    .line 200
    invoke-virtual {p0, v2, v2}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    aput-wide v6, v4, v0

    .line 201
    if-eqz p6, :cond_36

    if-lez v0, :cond_36

    .line 202
    invoke-virtual {p0, v2, v1}, Lcom/google/android/location/a/i;->b([D[D)[D

    move-result-object v1

    .line 203
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v1, v1}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    aput-wide v7, v5, v6

    .line 198
    :cond_36
    add-int/lit8 v0, v0, 0x1

    move-object v1, v2

    goto :goto_14

    .line 207
    :cond_3a
    if-eqz p6, :cond_5e

    mul-int/lit8 v0, p5, 0x2

    add-int/lit8 v0, v0, -0x1

    :goto_40
    new-array v2, v0, [D

    .line 209
    invoke-virtual {p0, v4}, Lcom/google/android/location/a/i;->a([D)Ljava/util/List;

    move-result-object v3

    .line 210
    const/4 v0, 0x0

    move v1, v0

    :goto_48
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_60

    .line 211
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    aput-wide v6, v2, v1

    .line 210
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_48

    :cond_5e
    move v0, p5

    .line 207
    goto :goto_40

    .line 213
    :cond_60
    if-eqz p6, :cond_82

    .line 214
    invoke-virtual {p0, v5}, Lcom/google/android/location/a/i;->a([D)Ljava/util/List;

    move-result-object v3

    .line 215
    const/4 v0, 0x0

    move v1, v0

    :goto_68
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_82

    .line 216
    add-int v4, p5, v1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    aput-wide v5, v2, v4

    .line 215
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_68

    .line 219
    :cond_82
    return-object v2

    .line 197
    nop

    :array_84
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method a([[DLjava/util/List;IJ)[[D
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[D",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;IJ)[[D"
        }
    .end annotation

    .prologue
    .line 102
    const/4 v2, 0x0

    aget-object v2, p1, v2

    array-length v2, v2

    move/from16 v0, p3

    filled-new-array {v0, v2}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    .line 103
    move-wide/from16 v0, p4

    long-to-double v3, v0

    move/from16 v0, p3

    int-to-double v5, v0

    div-double v6, v3, v5

    .line 104
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    new-array v8, v3, [D

    .line 105
    const/4 v3, 0x0

    move v4, v3

    :goto_22
    array-length v3, v8

    if-ge v4, v3, :cond_38

    .line 106
    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    long-to-double v9, v9

    aput-wide v9, v8, v4

    .line 105
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_22

    .line 108
    :cond_38
    const/4 v4, 0x0

    .line 112
    const/4 v3, 0x0

    move v5, v3

    move v3, v4

    :goto_3c
    move/from16 v0, p3

    if-ge v5, v0, :cond_bc

    .line 113
    const/4 v4, 0x0

    aget-wide v9, v8, v4

    int-to-double v11, v5

    mul-double/2addr v11, v6

    add-double/2addr v9, v11

    .line 114
    :goto_46
    aget-wide v11, v8, v3

    const-wide v13, 0x3eb0c6f7a0b5ed8dL

    add-double/2addr v11, v13

    cmpg-double v4, v11, v9

    if-gez v4, :cond_58

    array-length v4, v8

    if-ge v3, v4, :cond_58

    .line 115
    add-int/lit8 v3, v3, 0x1

    goto :goto_46

    .line 117
    :cond_58
    array-length v4, v8

    if-ne v3, v4, :cond_66

    .line 118
    array-length v4, v8

    add-int/lit8 v4, v4, -0x1

    aget-object v4, p1, v4

    aput-object v4, v2, v5

    .line 112
    :cond_62
    :goto_62
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3c

    .line 119
    :cond_66
    aget-wide v11, v8, v3

    sub-double v11, v9, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->abs(D)D

    move-result-wide v11

    const-wide v13, 0x3ec0c6f7a0b5ed8dL

    cmpg-double v4, v11, v13

    if-ltz v4, :cond_79

    if-nez v3, :cond_7e

    .line 120
    :cond_79
    aget-object v4, p1, v3

    aput-object v4, v2, v5

    goto :goto_62

    .line 122
    :cond_7e
    add-int/lit8 v4, v3, -0x1

    aget-wide v11, v8, v4

    sub-double v11, v9, v11

    .line 123
    aget-wide v13, v8, v3

    sub-double v9, v13, v9

    .line 124
    add-double v13, v11, v9

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v13

    const-wide v15, 0x3eb0c6f7a0b5ed8dL

    cmpg-double v4, v13, v15

    if-gez v4, :cond_9c

    .line 125
    aget-object v4, p1, v3

    aput-object v4, v2, v5

    goto :goto_62

    .line 127
    :cond_9c
    const/4 v4, 0x0

    :goto_9d
    aget-object v13, p1, v3

    array-length v13, v13

    if-ge v4, v13, :cond_62

    .line 128
    aget-object v13, v2, v5

    add-int/lit8 v14, v3, -0x1

    aget-object v14, p1, v14

    aget-wide v14, v14, v4

    mul-double/2addr v14, v9

    aget-object v16, p1, v3

    aget-wide v16, v16, v4

    mul-double v16, v16, v11

    add-double v14, v14, v16

    add-double v16, v11, v9

    div-double v14, v14, v16

    aput-wide v14, v13, v4

    .line 127
    add-int/lit8 v4, v4, 0x1

    goto :goto_9d

    .line 134
    :cond_bc
    return-object v2
.end method

.method b([D[D)[D
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 154
    invoke-virtual {p0, p2, p2}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v1

    .line 155
    array-length v3, p1

    new-array v3, v3, [D

    .line 156
    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3eb0c6f7a0b5ed8dL

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1f

    .line 157
    :goto_15
    array-length v1, p1

    if-ge v0, v1, :cond_33

    .line 158
    aget-wide v1, p1, v0

    aput-wide v1, v3, v0

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 161
    :cond_1f
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/a/i;->a([D[D)D

    move-result-wide v4

    div-double v1, v4, v1

    .line 162
    :goto_25
    array-length v4, p1

    if-ge v0, v4, :cond_33

    .line 163
    aget-wide v4, p1, v0

    aget-wide v6, p2, v0

    mul-double/2addr v6, v1

    sub-double/2addr v4, v6

    aput-wide v4, v3, v0

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 166
    :cond_33
    return-object v3
.end method
