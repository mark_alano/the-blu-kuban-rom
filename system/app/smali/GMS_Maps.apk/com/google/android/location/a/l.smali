.class public Lcom/google/android/location/a/l;
.super Lcom/google/android/location/c/J;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/l$a;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/l$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/location/os/i;

.field private e:I

.field private f:Lcom/google/android/location/c/F;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 46
    const-string v0, "rate"

    sput-object v0, Lcom/google/android/location/a/l;->a:Ljava/lang/String;

    return-void
.end method

.method public static a(I)I
    .registers 2
    .parameter

    .prologue
    const/4 v0, 0x3

    .line 144
    packed-switch p0, :pswitch_data_a

    .line 152
    :goto_4
    :pswitch_4
    return v0

    .line 146
    :pswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 148
    :pswitch_7
    const/4 v0, 0x2

    goto :goto_4

    .line 144
    nop

    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_5
        :pswitch_7
        :pswitch_4
    .end packed-switch
.end method

.method public static b(I)I
    .registers 2
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 161
    packed-switch p0, :pswitch_data_a

    .line 169
    :goto_4
    :pswitch_4
    return v0

    .line 165
    :pswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 167
    :pswitch_7
    const/4 v0, 0x2

    goto :goto_4

    .line 161
    nop

    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private j()V
    .registers 8

    .prologue
    .line 97
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 98
    iget-object v0, p0, Lcom/google/android/location/a/l;->f:Lcom/google/android/location/c/F;

    iget v1, p0, Lcom/google/android/location/a/l;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/google/android/location/a/l;->d:Lcom/google/android/location/os/i;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/location/c/F;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/location/a/l;->f:Lcom/google/android/location/c/F;

    aput-object v4, v1, v3

    invoke-static {v1}, Lcom/google/android/location/c/F;->a([Lcom/google/android/location/c/F;)Ljava/util/Set;

    move-result-object v1

    const-wide/16 v3, 0x3e8

    const-string v6, "SensorRateUtil"

    move-object v5, p0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/location/os/i;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/c/l;Ljava/lang/String;)Lcom/google/android/location/c/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/location/c/r;->a()V

    .line 104
    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 213
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/a/l;->e:I

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/a/l;->f:Lcom/google/android/location/c/F;

    .line 215
    return-void
.end method

.method private l()V
    .registers 8

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/location/a/l;->d:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->v()Ljava/io/File;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_13

    .line 229
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 230
    if-nez v1, :cond_13

    .line 253
    :goto_12
    return-void

    .line 235
    :cond_13
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/google/android/location/a/l;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 237
    :try_start_1a
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    .line 238
    const-string v0, "version,1"

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/location/a/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_85

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 240
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/c/F;

    invoke-virtual {v1}, Lcom/google/android/location/c/F;->a()I

    move-result v1

    .line 241
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/l$a;

    .line 242
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/google/android/location/a/l$a;->a:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v0, v0, Lcom/google/android/location/a/l$a;->b:I

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_4e

    .line 250
    :catch_83
    move-exception v0

    goto :goto_12

    .line 246
    :cond_85
    iget-object v0, p0, Lcom/google/android/location/a/l;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 247
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/c/F;

    invoke-virtual {v1}, Lcom/google/android/location/c/F;->a()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_8f

    .line 249
    :cond_c4
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_c7
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_c7} :catch_83

    goto/16 :goto_12
.end method


# virtual methods
.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 175
    new-instance v0, Lcom/google/android/location/a/k;

    invoke-direct {v0}, Lcom/google/android/location/a/k;-><init>()V

    iget-object v2, p0, Lcom/google/android/location/a/l;->f:Lcom/google/android/location/c/F;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/location/a/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/F;)Ljava/util/List;

    move-result-object v2

    .line 178
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_3b

    .line 179
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/j;

    iget-wide v3, v0, Lcom/google/android/location/a/j;->a:J

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/j;

    iget-wide v0, v0, Lcom/google/android/location/a/j;->a:J

    sub-long v0, v3, v0

    .line 181
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-double v2, v2

    long-to-double v0, v0

    const-wide v4, 0x41cdcd6500000000L

    div-double/2addr v0, v4

    div-double v0, v2, v0

    double-to-int v0, v0

    move v1, v0

    .line 187
    :cond_3b
    iget-object v0, p0, Lcom/google/android/location/a/l;->b:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/location/a/l;->f:Lcom/google/android/location/c/F;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 188
    if-nez v0, :cond_53

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 190
    iget-object v2, p0, Lcom/google/android/location/a/l;->b:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/location/a/l;->f:Lcom/google/android/location/c/F;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    :cond_53
    new-instance v2, Lcom/google/android/location/a/l$a;

    iget v3, p0, Lcom/google/android/location/a/l;->e:I

    invoke-direct {v2, v3, v1}, Lcom/google/android/location/a/l$a;-><init>(II)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    iget v0, p0, Lcom/google/android/location/a/l;->e:I

    invoke-static {v0}, Lcom/google/android/location/a/l;->b(I)I

    move-result v0

    .line 194
    iget v1, p0, Lcom/google/android/location/a/l;->e:I

    if-eq v0, v1, :cond_6d

    .line 196
    iput v0, p0, Lcom/google/android/location/a/l;->e:I

    .line 197
    invoke-direct {p0}, Lcom/google/android/location/a/l;->j()V

    .line 204
    :goto_6c
    return-void

    .line 200
    :cond_6d
    iget-object v0, p0, Lcom/google/android/location/a/l;->c:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/location/a/l;->f:Lcom/google/android/location/c/F;

    iget-object v2, p0, Lcom/google/android/location/a/l;->d:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    invoke-direct {p0}, Lcom/google/android/location/a/l;->l()V

    .line 202
    invoke-direct {p0}, Lcom/google/android/location/a/l;->k()V

    goto :goto_6c
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/android/location/a/l;->k()V

    .line 210
    return-void
.end method
