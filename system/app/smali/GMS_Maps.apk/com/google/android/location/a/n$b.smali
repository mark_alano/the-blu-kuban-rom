.class public Lcom/google/android/location/a/n$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/a/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/e/B;

.field private final b:D


# direct methods
.method constructor <init>(Lcom/google/android/location/e/B;D)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput-object p1, p0, Lcom/google/android/location/a/n$b;->a:Lcom/google/android/location/e/B;

    .line 242
    iput-wide p2, p0, Lcom/google/android/location/a/n$b;->b:D

    .line 243
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/B;
    .registers 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/location/a/n$b;->a:Lcom/google/android/location/e/B;

    return-object v0
.end method

.method public b()D
    .registers 3

    .prologue
    .line 259
    iget-wide v0, p0, Lcom/google/android/location/a/n$b;->b:D

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TravelDetectionResult [type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/a/n$b;->a:Lcom/google/android/location/e/B;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", confidence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/a/n$b;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
