.class public Lcom/google/android/location/a/n;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/a/n$a;,
        Lcom/google/android/location/a/n$b;
    }
.end annotation


# static fields
.field static final a:Lcom/google/android/location/a/n$b;


# instance fields
.field private final b:Lcom/google/android/location/a/m;

.field private final c:Lcom/google/android/location/a/f;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/location/a/h;

.field private final f:Lcom/google/android/location/os/c;

.field private g:Lcom/google/android/location/a/n$b;

.field private h:J

.field private i:Z

.field private j:Lcom/google/android/location/e/B;

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/e/B;->c:Lcom/google/android/location/e/B;

    const-wide/high16 v2, -0x4010

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/a/n$b;-><init>(Lcom/google/android/location/e/B;D)V

    sput-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/b/i;Lcom/google/android/location/os/c;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/b/i",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/e/C;",
            ">;",
            "Lcom/google/android/location/os/c;",
            ")V"
        }
    .end annotation

    .prologue
    const-wide/16 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/google/android/location/a/m;

    invoke-direct {v0}, Lcom/google/android/location/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/n;->b:Lcom/google/android/location/a/m;

    .line 50
    new-instance v0, Lcom/google/android/location/a/f;

    invoke-direct {v0}, Lcom/google/android/location/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/n;->c:Lcom/google/android/location/a/f;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    .line 55
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    iput-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    .line 56
    iput-wide v1, p0, Lcom/google/android/location/a/n;->h:J

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/a/n;->i:Z

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    .line 59
    iput-wide v1, p0, Lcom/google/android/location/a/n;->k:J

    .line 68
    iput-object p2, p0, Lcom/google/android/location/a/n;->f:Lcom/google/android/location/os/c;

    .line 69
    new-instance v0, Lcom/google/android/location/a/h;

    invoke-direct {v0, p1}, Lcom/google/android/location/a/h;-><init>(Lcom/google/android/location/b/i;)V

    iput-object v0, p0, Lcom/google/android/location/a/n;->e:Lcom/google/android/location/a/h;

    .line 70
    return-void
.end method

.method private a(Lcom/google/android/location/a/n$a;)Ljava/util/List;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/a/n$a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 198
    const/4 v0, 0x0

    :goto_6
    iget-object v2, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1c

    .line 199
    iget-object v2, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 201
    :cond_1c
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    return-object v1
.end method

.method private a()Z
    .registers 3

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/android/location/a/n;->b()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    sget-object v1, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private a(Lcom/google/android/location/a/n$a;Ljava/util/List;)Z
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/a/n$a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/a/n$a;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 227
    invoke-virtual {p1}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    iget-wide v1, v0, Lcom/google/android/location/e/E;->a:J

    .line 228
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/a/n$a;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$a;->a()Lcom/google/android/location/e/E;

    move-result-object v0

    iget-wide v3, v0, Lcom/google/android/location/e/E;->a:J

    .line 230
    sub-long v0, v1, v3

    const-wide/32 v2, 0xd6d8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_23

    const/4 v0, 0x1

    :goto_22
    return v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method

.method private b(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 154
    if-nez p2, :cond_5

    .line 157
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    .line 182
    :goto_4
    return-object v0

    .line 158
    :cond_5
    invoke-virtual {p2}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-gtz v0, :cond_e

    .line 162
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_4

    .line 165
    :cond_e
    new-instance v0, Lcom/google/android/location/a/n$a;

    invoke-direct {v0, p2, p1}, Lcom/google/android/location/a/n$a;-><init>(Lcom/google/android/location/e/E;Lcom/google/android/location/e/t;)V

    .line 166
    invoke-direct {p0, v0}, Lcom/google/android/location/a/n;->b(Lcom/google/android/location/a/n$a;)V

    .line 167
    invoke-direct {p0, v0}, Lcom/google/android/location/a/n;->a(Lcom/google/android/location/a/n$a;)Ljava/util/List;

    move-result-object v0

    .line 168
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_24

    .line 170
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_4

    .line 172
    :cond_24
    iget-object v1, p0, Lcom/google/android/location/a/n;->e:Lcom/google/android/location/a/h;

    invoke-virtual {v1, v0}, Lcom/google/android/location/a/h;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcom/google/android/location/a/n;->b:Lcom/google/android/location/a/m;

    invoke-virtual {v1, v0}, Lcom/google/android/location/a/m;->a(Ljava/util/Map;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    goto :goto_4
.end method

.method private b(Lcom/google/android/location/a/n$a;)V
    .registers 4
    .parameter

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/a/n;->a(Lcom/google/android/location/a/n$a;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 218
    :cond_10
    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_24

    .line 220
    iget-object v0, p0, Lcom/google/android/location/a/n;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 223
    :cond_24
    return-void
.end method

.method private b()Z
    .registers 5

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/location/a/n;->f:Lcom/google/android/location/os/c;

    invoke-interface {v0}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/a/n;->h:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x57e40

    cmp-long v0, v0, v2

    if-lez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method


# virtual methods
.method public a(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/a/n;->b(Lcom/google/android/location/e/t;Lcom/google/android/location/e/E;)Lcom/google/android/location/a/n$b;

    move-result-object v1

    .line 89
    iget-object v2, p0, Lcom/google/android/location/a/n;->c:Lcom/google/android/location/a/f;

    if-nez p1, :cond_4f

    const/4 v0, 0x0

    :goto_9
    invoke-virtual {v2, v0}, Lcom/google/android/location/a/f;->a(Lcom/google/android/location/e/c;)Lcom/google/android/location/a/n$b;

    move-result-object v0

    .line 92
    sget-object v2, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-ne v1, v2, :cond_52

    sget-object v2, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v2, :cond_52

    .line 94
    :goto_15
    iget-object v2, p0, Lcom/google/android/location/a/n;->f:Lcom/google/android/location/os/c;

    invoke-interface {v2}, Lcom/google/android/location/os/c;->a()J

    move-result-wide v2

    .line 95
    sget-object v4, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-ne v0, v4, :cond_54

    invoke-direct {p0}, Lcom/google/android/location/a/n;->a()Z

    move-result v4

    if-eqz v4, :cond_54

    .line 100
    iget-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    .line 115
    :goto_27
    iget-object v1, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    if-eqz v1, :cond_44

    sget-object v1, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v1, :cond_44

    iget-object v1, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v4

    if-eq v1, v4, :cond_44

    .line 118
    iget-wide v4, p0, Lcom/google/android/location/a/n;->k:J

    sub-long v4, v2, v4

    const-wide/32 v6, 0xc350

    cmp-long v1, v4, v6

    if-lez v1, :cond_71

    .line 120
    iput-wide v2, p0, Lcom/google/android/location/a/n;->k:J

    .line 134
    :cond_44
    :goto_44
    sget-object v1, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-eq v0, v1, :cond_4e

    .line 135
    invoke-virtual {v0}, Lcom/google/android/location/a/n$b;->a()Lcom/google/android/location/e/B;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/a/n;->j:Lcom/google/android/location/e/B;

    .line 137
    :cond_4e
    return-object v0

    .line 89
    :cond_4f
    iget-object v0, p1, Lcom/google/android/location/e/t;->c:Lcom/google/android/location/e/c;

    goto :goto_9

    :cond_52
    move-object v0, v1

    .line 92
    goto :goto_15

    .line 101
    :cond_54
    sget-object v4, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    if-ne v1, v4, :cond_65

    iget-boolean v4, p0, Lcom/google/android/location/a/n;->i:Z

    if-eqz v4, :cond_65

    invoke-direct {p0}, Lcom/google/android/location/a/n;->a()Z

    move-result v4

    if-eqz v4, :cond_65

    .line 107
    iget-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    goto :goto_27

    .line 109
    :cond_65
    if-ne v0, v1, :cond_6f

    const/4 v1, 0x1

    :goto_68
    iput-boolean v1, p0, Lcom/google/android/location/a/n;->i:Z

    .line 110
    iput-object v0, p0, Lcom/google/android/location/a/n;->g:Lcom/google/android/location/a/n$b;

    .line 111
    iput-wide v2, p0, Lcom/google/android/location/a/n;->h:J

    goto :goto_27

    .line 109
    :cond_6f
    const/4 v1, 0x0

    goto :goto_68

    .line 131
    :cond_71
    sget-object v0, Lcom/google/android/location/a/n;->a:Lcom/google/android/location/a/n$b;

    goto :goto_44
.end method
