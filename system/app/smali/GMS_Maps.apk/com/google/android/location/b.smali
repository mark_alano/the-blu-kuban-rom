.class public abstract Lcom/google/android/location/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b$a;
    }
.end annotation


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Lcom/google/android/location/os/i;

.field protected final c:Ljava/util/Random;

.field d:J

.field e:J

.field private final f:Lcom/google/android/location/t;

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/location/os/i;ILcom/google/android/location/t;Ljava/util/Random;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v0, -0x1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-wide v0, p0, Lcom/google/android/location/b;->d:J

    .line 30
    iput-wide v0, p0, Lcom/google/android/location/b;->e:J

    .line 51
    iput-object p1, p0, Lcom/google/android/location/b;->a:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    .line 53
    iput p3, p0, Lcom/google/android/location/b;->g:I

    .line 54
    iput-object p4, p0, Lcom/google/android/location/b;->f:Lcom/google/android/location/t;

    .line 55
    iput-object p5, p0, Lcom/google/android/location/b;->c:Ljava/util/Random;

    .line 56
    return-void
.end method

.method private b(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .registers 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/location/b;->b()Lcom/google/android/location/e/u;

    move-result-object v1

    .line 88
    iget-object v0, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 89
    iget-object v0, v1, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/b$a;

    .line 90
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x493e0

    sub-long/2addr v4, v6

    .line 93
    cmp-long v1, v2, v10

    if-eqz v1, :cond_3a

    const-wide/32 v6, 0x5265c00

    sub-long v6, v2, v6

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-gez v1, :cond_3a

    cmp-long v1, v4, v2

    if-gez v1, :cond_3a

    .line 96
    iget-wide v4, p0, Lcom/google/android/location/b;->d:J

    cmp-long v1, v4, v2

    if-eqz v1, :cond_35

    .line 99
    :cond_35
    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/location/b;->a(JLcom/google/android/location/b$a;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 101
    :goto_39
    return-object v0

    :cond_3a
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_39
.end method


# virtual methods
.method public a()Lcom/google/android/location/e/u;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/location/b;->f:Lcom/google/android/location/t;

    invoke-virtual {v0}, Lcom/google/android/location/t;->b()Z

    move-result v0

    if-nez v0, :cond_18

    .line 69
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/google/android/location/b$a;->a:Lcom/google/android/location/b$a;

    invoke-static {v0, v1}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 77
    :goto_17
    return-object v0

    .line 71
    :cond_18
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 72
    iget-object v0, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 73
    invoke-direct {p0, v2}, Lcom/google/android/location/b;->b(Ljava/util/Calendar;)Lcom/google/android/location/e/u;

    move-result-object v1

    .line 74
    iget-object v0, v1, Lcom/google/android/location/e/u;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-eqz v0, :cond_39

    move-object v0, v1

    .line 75
    goto :goto_17

    .line 77
    :cond_39
    invoke-virtual {p0, v2}, Lcom/google/android/location/b;->a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_17
.end method

.method protected a(JLcom/google/android/location/b$a;)Lcom/google/android/location/e/u;
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/location/b$a;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/google/android/location/b;->d:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_25

    .line 116
    iget-object v0, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v0

    sub-long v0, p1, v0

    .line 117
    iget-object v2, p0, Lcom/google/android/location/b;->b:Lcom/google/android/location/os/i;

    iget v3, p0, Lcom/google/android/location/b;->g:I

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/location/os/i;->a(IJ)V

    .line 118
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/location/b;->b(JLcom/google/android/location/b$a;)V

    .line 119
    iput-wide p1, p0, Lcom/google/android/location/b;->d:J

    .line 120
    iput-wide v0, p0, Lcom/google/android/location/b;->e:J

    .line 123
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 125
    :goto_24
    return-object v0

    :cond_25
    iget-wide v0, p0, Lcom/google/android/location/b;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;

    move-result-object v0

    goto :goto_24
.end method

.method protected abstract a(Ljava/util/Calendar;)Lcom/google/android/location/e/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract b()Lcom/google/android/location/e/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b$a;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract b(JLcom/google/android/location/b$a;)V
.end method
