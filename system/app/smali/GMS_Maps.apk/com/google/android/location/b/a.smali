.class public final Lcom/google/android/location/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:I

.field private d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ILjava/lang/Object;J)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;J)V"
        }
    .end annotation

    .prologue
    .line 44
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/b/a;-><init>(ILjava/lang/Object;JJ)V

    .line 45
    return-void
.end method

.method constructor <init>(ILjava/lang/Object;JJ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;JJ)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/b/a;->c:I

    .line 52
    if-nez p2, :cond_10

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Position many not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_10
    iput-object p2, p0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    .line 56
    iput p1, p0, Lcom/google/android/location/b/a;->c:I

    .line 57
    iput-wide p3, p0, Lcom/google/android/location/b/a;->a:J

    .line 58
    iput-wide p5, p0, Lcom/google/android/location/b/a;->b:J

    .line 59
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/location/e/v;)Lcom/google/android/location/b/a;
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/location/e/v",
            "<TP;>;)",
            "Lcom/google/android/location/b/a",
            "<TP;>;"
        }
    .end annotation

    .prologue
    .line 126
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v5

    .line 127
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v3

    .line 128
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 129
    invoke-interface {p1, p0}, Lcom/google/android/location/e/v;->b(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v2

    .line 130
    new-instance v0, Lcom/google/android/location/b/a;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/b/a;-><init>(ILjava/lang/Object;JJ)V

    .line 131
    iput-wide v5, v0, Lcom/google/android/location/b/a;->b:J

    .line 132
    return-object v0
.end method

.method public static a(Lcom/google/android/location/b/a;Ljava/io/DataOutput;Lcom/google/android/location/e/v;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/location/b/a",
            "<TP;>;",
            "Ljava/io/DataOutput;",
            "Lcom/google/android/location/e/v",
            "<TP;>;)V"
        }
    .end annotation

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/google/android/location/b/a;->b:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 138
    iget-wide v0, p0, Lcom/google/android/location/b/a;->a:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 139
    iget v0, p0, Lcom/google/android/location/b/a;->c:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 140
    iget-object v0, p0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    invoke-interface {p2, v0, p1}, Lcom/google/android/location/e/v;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    .line 141
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/google/android/location/b/a;->a:J

    return-wide v0
.end method

.method a(ILjava/lang/Object;J)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;J)V"
        }
    .end annotation

    .prologue
    .line 97
    if-nez p2, :cond_a

    .line 98
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Position may not be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_a
    iput-wide p3, p0, Lcom/google/android/location/b/a;->a:J

    .line 101
    iput-object p2, p0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    .line 102
    iput p1, p0, Lcom/google/android/location/b/a;->c:I

    .line 103
    return-void
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 80
    iput-wide p1, p0, Lcom/google/android/location/b/a;->b:J

    .line 81
    return-void
.end method

.method public b()J
    .registers 3

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/google/android/location/b/a;->b:J

    return-wide v0
.end method

.method public b(J)V
    .registers 3
    .parameter

    .prologue
    .line 87
    iput-wide p1, p0, Lcom/google/android/location/b/a;->a:J

    .line 88
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/location/b/a;->c:I

    return v0
.end method

.method public d()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheResult ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/b/a;->d:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " databaseVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/b/a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " readingTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/b/a;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lastSeenTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/b/a;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
