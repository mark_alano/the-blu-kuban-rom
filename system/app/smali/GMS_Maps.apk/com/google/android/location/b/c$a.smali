.class Lcom/google/android/location/b/c$a;
.super Lcom/google/android/location/b/j;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/location/b/j",
        "<TK;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/io/File;


# direct methods
.method constructor <init>(ILjava/util/concurrent/ExecutorService;Ljava/io/File;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lcom/google/android/location/b/j;-><init>(I)V

    .line 195
    iput-object p2, p0, Lcom/google/android/location/b/c$a;->a:Ljava/util/concurrent/ExecutorService;

    .line 196
    iput-object p3, p0, Lcom/google/android/location/b/c$a;->b:Ljava/io/File;

    .line 197
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/b/c$a;)Ljava/io/File;
    .registers 2
    .parameter

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/location/b/c$a;->b:Ljava/io/File;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/util/Map$Entry;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/e/A",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 205
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/A;

    .line 206
    iget-object v1, p0, Lcom/google/android/location/b/c$a;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/location/b/c$a$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/location/b/c$a$1;-><init>(Lcom/google/android/location/b/c$a;Lcom/google/android/location/e/A;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 222
    return-void
.end method
