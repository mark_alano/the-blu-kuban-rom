.class public Lcom/google/android/location/b/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/b/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/j",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/location/b/k$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/k$b",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/io/File;

.field private final d:Lcom/google/android/location/c/a;

.field private final e:Lcom/google/android/location/os/f;


# direct methods
.method public constructor <init>(ILcom/google/android/location/b/k$b;Ljava/io/File;[BLcom/google/android/location/os/f;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/location/b/k$b",
            "<TK;TV;>;",
            "Ljava/io/File;",
            "[B",
            "Lcom/google/android/location/os/f;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/google/android/location/b/j;

    invoke-direct {v0, p1}, Lcom/google/android/location/b/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/b/d;->a:Lcom/google/android/location/b/j;

    .line 64
    iput-object p2, p0, Lcom/google/android/location/b/d;->b:Lcom/google/android/location/b/k$b;

    .line 66
    iput-object p3, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    .line 67
    if-nez p4, :cond_16

    .line 68
    iput-object v1, p0, Lcom/google/android/location/b/d;->d:Lcom/google/android/location/c/a;

    .line 72
    :goto_13
    iput-object p5, p0, Lcom/google/android/location/b/d;->e:Lcom/google/android/location/os/f;

    .line 73
    return-void

    .line 70
    :cond_16
    invoke-static {p4, v1}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/b/d;->d:Lcom/google/android/location/c/a;

    goto :goto_13
.end method

.method private static a(Ljava/io/Closeable;)V
    .registers 2
    .parameter

    .prologue
    .line 209
    if-eqz p0, :cond_5

    .line 211
    :try_start_2
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    .line 217
    :cond_5
    :goto_5
    return-void

    .line 212
    :catch_6
    move-exception v0

    goto :goto_5
.end method

.method private d()V
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/location/b/d;->a:Lcom/google/android/location/b/j;

    invoke-virtual {v0}, Lcom/google/android/location/b/j;->clear()V

    .line 169
    iget-object v0, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 170
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/Object;J)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    .prologue
    .line 97
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/d;->a:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/j;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/e/A;

    .line 98
    if-eqz v0, :cond_11

    .line 99
    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/e/A;->a(J)Ljava/lang/Object;
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_13

    move-result-object v0

    .line 101
    :goto_f
    monitor-exit p0

    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_f

    .line 97
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .registers 7

    .prologue
    .line 134
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/d;->d:Lcom/google/android/location/c/a;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_22

    if-nez v0, :cond_7

    .line 162
    :goto_5
    monitor-exit p0

    return-void

    .line 138
    :cond_7
    const/4 v0, 0x0

    .line 141
    :try_start_8
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_14
    .catchall {:try_start_8 .. :try_end_14} :catchall_4d
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_14} :catch_3f
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_14} :catch_44

    .line 142
    :try_start_14
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 143
    const/4 v2, 0x1

    if-eq v0, v2, :cond_25

    .line 145
    invoke-direct {p0}, Lcom/google/android/location/b/d;->d()V
    :try_end_1e
    .catchall {:try_start_14 .. :try_end_1e} :catchall_55
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_1e} :catch_59
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_1e} :catch_57

    .line 160
    :try_start_1e
    invoke-static {v1}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V
    :try_end_21
    .catchall {:try_start_1e .. :try_end_21} :catchall_22

    goto :goto_5

    .line 134
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0

    .line 148
    :cond_25
    :try_start_25
    iget-object v0, p0, Lcom/google/android/location/b/d;->d:Lcom/google/android/location/c/a;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/a;->a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;

    move-result-object v0

    .line 149
    iget-object v2, p0, Lcom/google/android/location/b/d;->b:Lcom/google/android/location/b/k$b;

    iget-object v3, p0, Lcom/google/android/location/b/d;->a:Lcom/google/android/location/b/j;

    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lcom/google/android/location/e/u;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v4, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/location/b/k$b;->a(Lcom/google/android/location/b/j;Ljava/io/InputStream;)V
    :try_end_3b
    .catchall {:try_start_25 .. :try_end_3b} :catchall_55
    .catch Ljava/io/FileNotFoundException; {:try_start_25 .. :try_end_3b} :catch_59
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_3b} :catch_57

    .line 160
    :try_start_3b
    invoke-static {v1}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V

    goto :goto_5

    .line 152
    :catch_3f
    move-exception v1

    .line 160
    :goto_40
    invoke-static {v0}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V
    :try_end_43
    .catchall {:try_start_3b .. :try_end_43} :catchall_22

    goto :goto_5

    .line 155
    :catch_44
    move-exception v1

    move-object v1, v0

    .line 158
    :goto_46
    :try_start_46
    invoke-direct {p0}, Lcom/google/android/location/b/d;->d()V
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_55

    .line 160
    :try_start_49
    invoke-static {v1}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V

    goto :goto_5

    :catchall_4d
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_51
    invoke-static {v1}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_55
    .catchall {:try_start_49 .. :try_end_55} :catchall_22

    :catchall_55
    move-exception v0

    goto :goto_51

    .line 155
    :catch_57
    move-exception v0

    goto :goto_46

    .line 152
    :catch_59
    move-exception v0

    move-object v0, v1

    goto :goto_40
.end method

.method public declared-synchronized a(JJ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 127
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/b/d;->a:Lcom/google/android/location/b/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/b/j;->a(JJ)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 128
    monitor-exit p0

    return-void

    .line 127
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;J)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;J)V"
        }
    .end annotation

    .prologue
    .line 113
    monitor-enter p0

    :try_start_1
    new-instance v0, Lcom/google/android/location/e/A;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/android/location/e/A;-><init>(Ljava/lang/Object;J)V

    .line 114
    iget-object v1, p0, Lcom/google/android/location/b/d;->a:Lcom/google/android/location/b/j;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/b/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 115
    monitor-exit p0

    return-void

    .line 113
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .registers 6

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/location/b/d;->d:Lcom/google/android/location/c/a;

    if-nez v0, :cond_5

    .line 203
    :goto_4
    return-void

    .line 180
    :cond_5
    const/4 v0, 0x0

    .line 183
    :try_start_6
    iget-object v1, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 184
    iget-object v1, p0, Lcom/google/android/location/b/d;->e:Lcom/google/android/location/os/f;

    iget-object v2, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    invoke-interface {v1, v2}, Lcom/google/android/location/os/f;->a(Ljava/io/File;)V

    .line 186
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1e
    .catchall {:try_start_6 .. :try_end_1e} :catchall_53
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_1e} :catch_65
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_1e} :catch_49

    .line 187
    const/4 v0, 0x1

    :try_start_1f
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 188
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 189
    monitor-enter p0
    :try_end_28
    .catchall {:try_start_1f .. :try_end_28} :catchall_5b
    .catch Ljava/io/FileNotFoundException; {:try_start_1f .. :try_end_28} :catch_43
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_28} :catch_62

    .line 190
    :try_start_28
    iget-object v2, p0, Lcom/google/android/location/b/d;->b:Lcom/google/android/location/b/k$b;

    iget-object v3, p0, Lcom/google/android/location/b/d;->a:Lcom/google/android/location/b/j;

    invoke-interface {v2, v3, v0}, Lcom/google/android/location/b/k$b;->a(Lcom/google/android/location/b/j;Ljava/io/OutputStream;)V

    .line 191
    monitor-exit p0
    :try_end_30
    .catchall {:try_start_28 .. :try_end_30} :catchall_40

    .line 192
    :try_start_30
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 193
    iget-object v2, p0, Lcom/google/android/location/b/d;->d:Lcom/google/android/location/c/a;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/android/location/c/a;->a(Ljava/io/DataOutputStream;[B)V
    :try_end_3c
    .catchall {:try_start_30 .. :try_end_3c} :catchall_5b
    .catch Ljava/io/FileNotFoundException; {:try_start_30 .. :try_end_3c} :catch_43
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_3c} :catch_62

    .line 201
    invoke-static {v1}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V

    goto :goto_4

    .line 191
    :catchall_40
    move-exception v0

    :try_start_41
    monitor-exit p0
    :try_end_42
    .catchall {:try_start_41 .. :try_end_42} :catchall_40

    :try_start_42
    throw v0
    :try_end_43
    .catchall {:try_start_42 .. :try_end_43} :catchall_5b
    .catch Ljava/io/FileNotFoundException; {:try_start_42 .. :try_end_43} :catch_43
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_43} :catch_62

    .line 194
    :catch_43
    move-exception v0

    move-object v0, v1

    .line 201
    :goto_45
    invoke-static {v0}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V

    goto :goto_4

    .line 196
    :catch_49
    move-exception v1

    .line 199
    :goto_4a
    :try_start_4a
    iget-object v1, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_4f
    .catchall {:try_start_4a .. :try_end_4f} :catchall_5d

    .line 201
    invoke-static {v0}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V

    goto :goto_4

    :catchall_53
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_57
    invoke-static {v1}, Lcom/google/android/location/b/d;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_5b
    move-exception v0

    goto :goto_57

    :catchall_5d
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_57

    .line 196
    :catch_62
    move-exception v0

    move-object v0, v1

    goto :goto_4a

    .line 194
    :catch_65
    move-exception v1

    goto :goto_45
.end method

.method public c()V
    .registers 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/location/b/d;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 224
    return-void
.end method
