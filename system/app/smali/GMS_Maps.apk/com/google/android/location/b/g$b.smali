.class public Lcom/google/android/location/b/g$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/b/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private b:J


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/b/g$b;->b:J

    .line 505
    iput-object p1, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 506
    return-void
.end method

.method private a(JJJJ)J
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 675
    add-long v0, p3, p1

    sub-long/2addr v0, p5

    invoke-static {p7, p8, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/location/b/g$b;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method private a(JJJ)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 656
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long v3, v0, v2

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p3

    move-wide v7, p5

    .line 658
    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/b/g$b;->a(JJJJ)J

    move-result-wide v0

    .line 660
    iget-object v2, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 663
    :cond_28
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 664
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long v3, v0, v2

    move-object v0, p0

    move-wide v1, p1

    move-wide v5, p3

    move-wide v7, p5

    .line 666
    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/b/g$b;->a(JJJJ)J

    move-result-wide v0

    .line 668
    iget-object v2, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 671
    :cond_50
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/b/g$b;J)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 485
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->c(J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/b/g$b;JJJ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 485
    invoke-direct/range {p0 .. p6}, Lcom/google/android/location/b/g$b;->a(JJJ)V

    return-void
.end method

.method private b(J)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 627
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 628
    iget-object v1, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 629
    invoke-virtual {p0}, Lcom/google/android/location/b/g$b;->d()F

    move-result v0

    .line 634
    cmpl-float v1, v0, v2

    if-lez v1, :cond_22

    .line 635
    const v1, 0x3dcccccd

    sub-float/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/location/b/g$b;->a(F)V

    .line 638
    :cond_22
    iput-wide p1, p0, Lcom/google/android/location/b/g$b;->b:J

    .line 639
    return-void
.end method

.method static synthetic b(Lcom/google/android/location/b/g$b;)V
    .registers 1
    .parameter

    .prologue
    .line 485
    invoke-direct {p0}, Lcom/google/android/location/b/g$b;->i()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/location/b/g$b;J)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 485
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->b(J)V

    return-void
.end method

.method private c(J)V
    .registers 6
    .parameter

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 644
    return-void
.end method

.method private d(J)I
    .registers 11
    .parameter

    .prologue
    const-wide/32 v6, 0x5265c00

    .line 683
    const-wide/32 v2, 0x927c0

    .line 684
    long-to-double v0, p1

    const-wide/high16 v4, 0x3ff0

    mul-double/2addr v0, v4

    const-wide v4, 0x4194997000000000L

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    .line 686
    mul-long v4, v0, v6

    sub-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    .line 687
    sub-long v2, v6, v2

    cmp-long v2, v4, v2

    if-lez v2, :cond_25

    .line 688
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 690
    :cond_25
    long-to-int v0, v0

    return v0
.end method

.method private i()V
    .registers 3

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 606
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 607
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 608
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 609
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 515
    iget-wide v0, p0, Lcom/google/android/location/b/g$b;->b:J

    return-wide v0
.end method

.method a(F)V
    .registers 4
    .parameter

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 570
    return-void
.end method

.method a(J)V
    .registers 6
    .parameter

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/b/g$b;->d(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 616
    return-void
.end method

.method a(JI)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 622
    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/b/g$b;->a(J)V

    .line 623
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 624
    return-void
.end method

.method public b()I
    .registers 3

    .prologue
    const/4 v1, 0x5

    .line 532
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    :goto_f
    return v0

    :cond_10
    const/4 v0, -0x1

    goto :goto_f
.end method

.method b(F)V
    .registers 4
    .parameter

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 587
    return-void
.end method

.method public c()I
    .registers 3

    .prologue
    const/4 v1, 0x3

    .line 548
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 549
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 551
    :goto_f
    return v0

    :cond_10
    const v0, 0x7fffffff

    goto :goto_f
.end method

.method public d()F
    .registers 3

    .prologue
    const/4 v1, 0x4

    .line 560
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    :goto_f
    return v0

    :cond_10
    const/high16 v0, -0x4080

    goto :goto_f
.end method

.method e()V
    .registers 3

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 566
    return-void
.end method

.method public f()F
    .registers 3

    .prologue
    const/4 v1, 0x7

    .line 577
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    :goto_f
    return v0

    :cond_10
    const/high16 v0, -0x4080

    goto :goto_f
.end method

.method g()V
    .registers 3

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 583
    return-void
.end method

.method public h()I
    .registers 3

    .prologue
    const/4 v1, 0x2

    .line 594
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 595
    iget-object v0, p0, Lcom/google/android/location/b/g$b;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 597
    :goto_f
    return v0

    :cond_10
    const v0, 0x7fffffff

    goto :goto_f
.end method
