.class public Lcom/google/android/location/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/base/K;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/b/g$a;,
        Lcom/google/android/location/b/g$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/K",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/location/b/h;

.field private final b:Lcom/google/android/location/b/g$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/b/g$a",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b/g$b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/location/os/i;

.field private final d:Lcom/google/android/location/os/h;

.field private e:J

.field private f:Z

.field private final g:Lcom/google/android/location/os/j;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/i;Lcom/google/android/location/os/h;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-static {p1}, Lcom/google/android/location/b/g;->a(Lcom/google/android/location/os/f;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/location/b/g;-><init>(Lcom/google/android/location/os/i;Ljava/io/File;Lcom/google/android/location/os/h;)V

    .line 115
    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/i;Ljava/io/File;Lcom/google/android/location/os/h;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Lcom/google/android/location/b/h;

    invoke-direct {v0}, Lcom/google/android/location/b/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    .line 95
    new-instance v0, Lcom/google/android/location/b/g$a;

    const/16 v1, 0x3e8

    iget-object v2, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/b/g$a;-><init>(ILcom/google/android/location/b/h;)V

    iput-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    .line 119
    iput-object p1, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    .line 120
    iput-object p3, p0, Lcom/google/android/location/b/g;->d:Lcom/google/android/location/os/h;

    .line 121
    new-instance v0, Lcom/google/android/location/os/j;

    const/4 v1, 0x2

    invoke-interface {p1}, Lcom/google/android/location/os/i;->k()Ljavax/crypto/SecretKey;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p1}, Lcom/google/android/location/os/i;->l()[B

    move-result-object v4

    sget-object v5, Lcom/google/android/location/j/a;->N:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-object v6, p2

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/os/j;-><init>(ILjavax/crypto/SecretKey;I[BLcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/File;Lcom/google/common/base/K;Lcom/google/android/location/os/f;)V

    iput-object v0, p0, Lcom/google/android/location/b/g;->g:Lcom/google/android/location/os/j;

    .line 129
    invoke-direct {p0}, Lcom/google/android/location/b/g;->i()V

    .line 130
    return-void
.end method

.method private static a(Lcom/google/android/location/os/f;)Ljava/io/File;
    .registers 4
    .parameter

    .prologue
    .line 436
    new-instance v0, Ljava/io/File;

    invoke-interface {p0}, Lcom/google/android/location/os/f;->f()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 437
    return-object v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 15
    .parameter

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x1

    .line 375
    invoke-virtual {p1, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v1

    .line 376
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    add-long v9, v3, v1

    .line 378
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->b()J

    move-result-wide v7

    .line 379
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v5

    .line 382
    sub-long v3, v7, v5

    .line 385
    cmp-long v0, v9, v7

    if-lez v0, :cond_53

    .line 387
    :goto_1f
    sub-long/2addr v7, v3

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-virtual {p0, v7, v8, v0}, Lcom/google/android/location/b/g;->a(JZ)V

    .line 389
    const/4 v0, 0x0

    move v7, v0

    :goto_2a
    invoke-virtual {p1, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v7, v0, :cond_55

    .line 392
    invoke-virtual {p1, v12, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    .line 393
    invoke-direct {p0, v8}, Lcom/google/android/location/b/g;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 397
    new-instance v0, Lcom/google/android/location/b/g$b;

    invoke-direct {v0, v8}, Lcom/google/android/location/b/g$b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 401
    invoke-static/range {v0 .. v6}, Lcom/google/android/location/b/g$b;->a(Lcom/google/android/location/b/g$b;JJJ)V

    .line 402
    invoke-virtual {v8, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    .line 403
    iget-object v10, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v10, v8, v0}, Lcom/google/android/location/b/g$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    :cond_4f
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2a

    :cond_53
    move-wide v7, v9

    .line 385
    goto :goto_1f

    .line 406
    :cond_55
    return-void
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 422
    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_10

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private h()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 409
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/j/a;->N:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 410
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 412
    const/4 v3, 0x3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    invoke-static {v0}, Lcom/google/android/location/b/g$b;->a(Lcom/google/android/location/b/g$b;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_11

    .line 414
    :cond_2c
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->c()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 415
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/location/b/g;->e:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 417
    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/google/android/location/b/g;->f:Z

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 418
    return-object v1
.end method

.method private i()V
    .registers 5

    .prologue
    .line 428
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x4194997000000000L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    .line 429
    iget-object v2, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v2}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/location/b/g;->a(JZ)V

    .line 430
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->clear()V

    .line 433
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)F
    .registers 5
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    .line 298
    iget-object v2, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    if-eqz v0, :cond_15

    const/4 v1, 0x1

    :goto_d
    invoke-virtual {v2, v1}, Lcom/google/android/location/b/h;->a(Z)V

    .line 300
    if-nez v0, :cond_17

    .line 301
    const/high16 v0, -0x4080

    .line 303
    :goto_14
    return v0

    .line 298
    :cond_15
    const/4 v1, 0x0

    goto :goto_d

    .line 303
    :cond_17
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->d()F

    move-result v0

    goto :goto_14
.end method

.method a(JJ)Lcom/google/android/location/b/g$b;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    .line 268
    if-nez v0, :cond_32

    .line 269
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->r:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 270
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 271
    new-instance v1, Lcom/google/android/location/b/g$b;

    invoke-direct {v1, v0}, Lcom/google/android/location/b/g$b;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/location/b/g$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    .line 273
    if-nez v0, :cond_31

    .line 275
    iget-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->c()V

    :cond_31
    move-object v0, v1

    .line 278
    :cond_32
    invoke-static {v0, p3, p4}, Lcom/google/android/location/b/g$b;->a(Lcom/google/android/location/b/g$b;J)V

    .line 279
    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/b/g;->g:Lcom/google/android/location/os/j;

    invoke-virtual {v0}, Lcom/google/android/location/os/j;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 197
    invoke-direct {p0, v0}, Lcom/google/android/location/b/g;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 198
    invoke-virtual {p0}, Lcom/google/android/location/b/g;->g()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_c} :catch_d

    .line 205
    :goto_c
    return-void

    .line 201
    :catch_d
    move-exception v0

    .line 202
    invoke-direct {p0}, Lcom/google/android/location/b/g;->i()V

    goto :goto_c
.end method

.method public a(JZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 258
    iput-wide p1, p0, Lcom/google/android/location/b/g;->e:J

    .line 259
    iput-boolean p3, p0, Lcom/google/android/location/b/g;->f:Z

    .line 260
    return-void
.end method

.method public a(Lcom/google/android/location/e/E;)V
    .registers 7
    .parameter

    .prologue
    .line 287
    if-nez p1, :cond_3

    .line 294
    :cond_2
    return-void

    .line 290
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    .line 291
    const/4 v0, 0x0

    :goto_a
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 292
    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/google/android/location/b/g;->a(JJ)Lcom/google/android/location/b/g$b;

    .line 291
    add-int/lit8 v0, v0, 0x1

    goto :goto_a
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 13
    .parameter

    .prologue
    .line 147
    invoke-static {p1}, Lcom/google/android/location/k/c;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 188
    :cond_6
    return-void

    .line 150
    :cond_7
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v4

    .line 151
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    .line 152
    iget-object v0, p0, Lcom/google/android/location/b/g;->d:Lcom/google/android/location/os/h;

    invoke-virtual {v0}, Lcom/google/android/location/os/h;->e()I

    move-result v7

    .line 153
    const/4 v0, 0x0

    move v1, v0

    :goto_1a
    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 154
    const/4 v0, 0x3

    invoke-virtual {v6, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    .line 155
    const/4 v0, 0x3

    invoke-virtual {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 156
    const/4 v0, 0x3

    invoke-virtual {v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 159
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_79

    .line 160
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v2

    .line 164
    :goto_40
    const-wide/16 v9, -0x1

    cmp-long v0, v2, v9

    if-eqz v0, :cond_75

    .line 165
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    .line 167
    if-eqz v0, :cond_75

    .line 168
    invoke-virtual {v0, v4, v5, v7}, Lcom/google/android/location/b/g$b;->a(JI)V

    .line 170
    const/4 v2, 0x4

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_83

    .line 171
    const/4 v2, 0x4

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$b;->a(F)V

    .line 178
    :goto_66
    const/4 v2, 0x5

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_87

    .line 179
    const/4 v2, 0x5

    invoke-virtual {v8, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$b;->b(F)V

    .line 153
    :cond_75
    :goto_75
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1a

    .line 162
    :cond_79
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/e/F;->a(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_40

    .line 174
    :cond_83
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->e()V

    goto :goto_66

    .line 182
    :cond_87
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->g()V

    goto :goto_75
.end method

.method public synthetic a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 62
    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/b/g;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Long;)F
    .registers 3
    .parameter

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    .line 309
    if-nez v0, :cond_d

    .line 310
    const/high16 v0, -0x4080

    .line 312
    :goto_c
    return v0

    :cond_d
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->f()F

    move-result v0

    goto :goto_c
.end method

.method public b()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/b/g$b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/location/e/E;)V
    .registers 5
    .parameter

    .prologue
    .line 321
    if-nez p1, :cond_3

    .line 332
    :cond_2
    return-void

    .line 325
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 326
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {p1, v1}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    .line 328
    if-eqz v0, :cond_20

    .line 329
    const/high16 v2, 0x3f80

    invoke-virtual {v0, v2}, Lcom/google/android/location/b/g$b;->a(F)V

    .line 325
    :cond_20
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 212
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_f

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public c(Ljava/lang/Long;)J
    .registers 4
    .parameter

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/g$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    .line 342
    if-nez v0, :cond_d

    .line 343
    const-wide/16 v0, -0x1

    .line 345
    :goto_c
    return-wide v0

    :cond_d
    invoke-virtual {v0}, Lcom/google/android/location/b/g$b;->a()J

    move-result-wide v0

    goto :goto_c
.end method

.method public c()V
    .registers 3

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/location/b/g;->g()V

    .line 229
    :try_start_3
    iget-object v0, p0, Lcom/google/android/location/b/g;->g:Lcom/google/android/location/os/j;

    invoke-direct {p0}, Lcom/google/android/location/b/g;->h()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/j;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_c} :catch_d

    .line 233
    :goto_c
    return-void

    .line 230
    :catch_d
    move-exception v0

    goto :goto_c
.end method

.method public c(Lcom/google/android/location/e/E;)V
    .registers 7
    .parameter

    .prologue
    .line 354
    if-nez p1, :cond_3

    .line 363
    :cond_2
    return-void

    .line 357
    :cond_3
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v1

    .line 359
    const/4 v0, 0x0

    :goto_a
    invoke-virtual {p1}, Lcom/google/android/location/e/E;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 360
    invoke-virtual {p1, v0}, Lcom/google/android/location/e/E;->a(I)Lcom/google/android/location/e/E$a;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/location/e/E$a;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4, v1, v2}, Lcom/google/android/location/b/g;->a(JJ)Lcom/google/android/location/b/g$b;

    move-result-object v3

    .line 361
    invoke-static {v3, v1, v2}, Lcom/google/android/location/b/g$b;->b(Lcom/google/android/location/b/g$b;J)V

    .line 359
    add-int/lit8 v0, v0, 0x1

    goto :goto_a
.end method

.method public d()J
    .registers 3

    .prologue
    .line 241
    iget-wide v0, p0, Lcom/google/android/location/b/g;->e:J

    return-wide v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/google/android/location/b/g;->f:Z

    return v0
.end method

.method public f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    iget-object v1, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-static {v1}, Lcom/google/android/location/b/g$a;->a(Lcom/google/android/location/b/g$a;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v2}, Lcom/google/android/location/b/g$a;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/b/h;->a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .registers 13

    .prologue
    const-wide/32 v10, 0x5265c00

    const v9, 0x7fffffff

    .line 446
    iget-object v0, p0, Lcom/google/android/location/b/g;->c:Lcom/google/android/location/os/i;

    invoke-interface {v0}, Lcom/google/android/location/os/i;->a()J

    move-result-wide v0

    .line 447
    const-wide/32 v2, 0xa4cb800

    sub-long v2, v0, v2

    .line 448
    const-wide/32 v4, 0x240c8400

    sub-long v4, v0, v4

    .line 449
    iget-object v0, p0, Lcom/google/android/location/b/g;->b:Lcom/google/android/location/b/g$a;

    invoke-virtual {v0}, Lcom/google/android/location/b/g$a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 450
    :cond_20
    :goto_20
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 451
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 452
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/g$b;

    invoke-virtual {v1}, Lcom/google/android/location/b/g$b;->c()I

    move-result v7

    .line 453
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/b/g$b;

    invoke-virtual {v1}, Lcom/google/android/location/b/g$b;->h()I

    move-result v1

    .line 454
    if-ne v7, v9, :cond_46

    .line 455
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_20

    .line 458
    :cond_46
    int-to-long v7, v7

    mul-long/2addr v7, v10

    cmp-long v7, v7, v4

    if-gez v7, :cond_55

    .line 459
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 460
    iget-object v0, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-virtual {v0}, Lcom/google/android/location/b/h;->b()V

    goto :goto_20

    .line 465
    :cond_55
    if-eq v1, v9, :cond_20

    int-to-long v7, v1

    mul-long/2addr v7, v10

    cmp-long v1, v7, v2

    if-gez v1, :cond_20

    .line 469
    iget-object v1, p0, Lcom/google/android/location/b/g;->a:Lcom/google/android/location/b/h;

    invoke-virtual {v1}, Lcom/google/android/location/b/h;->b()V

    .line 470
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/b/g$b;

    invoke-static {v0}, Lcom/google/android/location/b/g$b;->b(Lcom/google/android/location/b/g$b;)V

    goto :goto_20

    .line 475
    :cond_6c
    return-void
.end method
