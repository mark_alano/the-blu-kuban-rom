.class Lcom/google/android/location/b/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/google/android/location/b/h;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/b/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private a(I)V
    .registers 4
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/location/b/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/android/location/b/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 59
    return-void
.end method

.method private static d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/j/a;->P:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 63
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 64
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 65
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 66
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 67
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 68
    return-object v0
.end method


# virtual methods
.method public a(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/location/b/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 51
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 52
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 53
    invoke-static {}, Lcom/google/android/location/b/h;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/b/h;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 54
    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 20
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/location/b/h;->a(I)V

    .line 21
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 37
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/location/b/h;->a(I)V

    .line 38
    if-eqz p1, :cond_a

    .line 39
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/location/b/h;->a(I)V

    .line 41
    :cond_a
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 25
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/location/b/h;->a(I)V

    .line 26
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 30
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/location/b/h;->a(I)V

    .line 31
    return-void
.end method
