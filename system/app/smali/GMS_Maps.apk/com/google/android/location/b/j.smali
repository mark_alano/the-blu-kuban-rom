.class Lcom/google/android/location/b/j;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;",
        "Lcom/google/android/location/e/A",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 22
    const/high16 v0, 0x3f40

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 24
    iput p1, p0, Lcom/google/android/location/b/j;->a:I

    .line 25
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/location/b/j;->a:I

    return v0
.end method

.method public a(JJ)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/android/location/b/j;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 58
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 59
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/e/A;

    .line 60
    invoke-virtual {v1}, Lcom/google/android/location/e/A;->b()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gez v3, :cond_29

    .line 61
    invoke-virtual {p0, v0}, Lcom/google/android/location/b/j;->a(Ljava/util/Map$Entry;)V

    .line 62
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_8

    .line 63
    :cond_29
    invoke-virtual {v1}, Lcom/google/android/location/e/A;->c()J

    move-result-wide v3

    cmp-long v1, v3, p3

    if-gez v1, :cond_8

    .line 64
    invoke-virtual {p0, v0}, Lcom/google/android/location/b/j;->a(Ljava/util/Map$Entry;)V

    .line 65
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_8

    .line 68
    :cond_38
    return-void
.end method

.method protected a(Ljava/util/Map$Entry;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/e/A",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    return-void
.end method

.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/e/A",
            "<TV;>;>;)Z"
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/location/b/j;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/location/b/j;->a:I

    if-le v0, v1, :cond_f

    const/4 v0, 0x1

    .line 30
    :goto_9
    if-eqz v0, :cond_e

    .line 31
    invoke-virtual {p0, p1}, Lcom/google/android/location/b/j;->a(Ljava/util/Map$Entry;)V

    .line 33
    :cond_e
    return v0

    .line 29
    :cond_f
    const/4 v0, 0x0

    goto :goto_9
.end method
