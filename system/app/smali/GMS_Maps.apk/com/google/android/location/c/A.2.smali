.class public final Lcom/google/android/location/c/A;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/location/c/j;


# static fields
.field static a:Z

.field static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/location/c/A;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final c:Ljava/lang/String;

.field final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/location/c/j$a;

.field private final h:Ljava/lang/String;

.field private final i:[B

.field private final j:J

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Z

.field private final n:Z

.field private volatile o:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/c/A;->a:Z

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    .line 38
    sget-object v0, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/android/location/c/F;->c:Lcom/google/android/location/c/F;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/android/location/c/F;->d:Lcom/google/android/location/c/F;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/android/location/c/F;->e:Lcom/google/android/location/c/F;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/android/location/c/F;->f:Lcom/google/android/location/c/F;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/android/location/c/F;->i:Lcom/google/android/location/c/F;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    new-instance v0, Lcom/google/android/location/c/A$1;

    invoke-direct {v0}, Lcom/google/android/location/c/A$1;-><init>()V

    sput-object v0, Lcom/google/android/location/c/A;->e:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/location/c/A;->k:I

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    .line 82
    iput-boolean v1, p0, Lcom/google/android/location/c/A;->m:Z

    .line 97
    iput-boolean v1, p0, Lcom/google/android/location/c/A;->o:Z

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 185
    invoke-static {v0}, Lcom/google/android/location/c/F;->a(I)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/A;->f:Ljava/util/Set;

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/c/j$a;->valueOf(Ljava/lang/String;)Lcom/google/android/location/c/j$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/A;->g:Lcom/google/android/location/c/j$a;

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/A;->h:Ljava/lang/String;

    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/location/c/A;->j:J

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/c/A;->k:I

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 191
    iget-object v3, p0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    invoke-direct {p0, v0, v3}, Lcom/google/android/location/c/A;->a(Landroid/os/Bundle;Ljava/util/Map;)V

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/A;->l:Ljava/lang/String;

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/A;->c:Ljava/lang/String;

    .line 194
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 195
    if-lez v0, :cond_7c

    .line 196
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/location/c/A;->i:[B

    .line 197
    iget-object v0, p0, Lcom/google/android/location/c/A;->i:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 201
    :goto_61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_80

    move v0, v1

    :goto_68
    iput-boolean v0, p0, Lcom/google/android/location/c/A;->m:Z

    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_82

    move v0, v1

    :goto_71
    iput-boolean v0, p0, Lcom/google/android/location/c/A;->n:Z

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_84

    :goto_79
    iput-boolean v1, p0, Lcom/google/android/location/c/A;->o:Z

    .line 204
    return-void

    .line 199
    :cond_7c
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/c/A;->i:[B

    goto :goto_61

    :cond_80
    move v0, v2

    .line 201
    goto :goto_68

    :cond_82
    move v0, v2

    .line 202
    goto :goto_71

    :cond_84
    move v1, v2

    .line 203
    goto :goto_79
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/location/c/A$1;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/location/c/A;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Ljava/lang/String;Ljava/lang/String;[BZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;J",
            "Lcom/google/android/location/c/j$a;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[BZ)V"
        }
    .end annotation

    .prologue
    const-wide/16 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/location/c/A;->k:I

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    .line 82
    iput-boolean v2, p0, Lcom/google/android/location/c/A;->m:Z

    .line 97
    iput-boolean v2, p0, Lcom/google/android/location/c/A;->o:Z

    .line 138
    sget-object v0, Lcom/google/android/location/c/j$a;->b:Lcom/google/android/location/c/j$a;

    if-ne p4, v0, :cond_22

    .line 139
    const-string v0, "dataPath could not be null if you want to write data to local storage"

    invoke-static {p5, v0}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    :cond_22
    sget-object v0, Lcom/google/android/location/c/j$a;->a:Lcom/google/android/location/c/j$a;

    if-ne p4, v0, :cond_96

    .line 143
    cmp-long v0, p2, v3

    if-lez v0, :cond_94

    const-wide/32 v3, 0x493e0

    cmp-long v0, p2, v3

    if-gtz v0, :cond_94

    move v0, v1

    :goto_32
    const-string v3, "Invalid scan duration for MEMORY collection destination."

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 149
    :goto_37
    invoke-static {p5}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_85

    .line 150
    invoke-static {p6}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    if-eqz p7, :cond_a3

    array-length v0, p7

    const/16 v3, 0x20

    if-ne v0, v3, :cond_a3

    :cond_4a
    move v0, v1

    :goto_4b
    const-string v3, "You must specify a valid key for encryption when writing data to persistent storage."

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 152
    sget-boolean v0, Lcom/google/android/location/c/A;->a:Z

    if-nez v0, :cond_85

    .line 153
    if-nez p7, :cond_85

    .line 154
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    :try_start_5b
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    .line 157
    const-string v4, "/data/data"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "The key should be in the /data/data partition."

    invoke-static {v3, v4}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 159
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_a5

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_a5

    :goto_76
    const-string v0, "%s does not exist."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p6, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V
    :try_end_85
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_85} :catch_a7

    .line 167
    :cond_85
    iput-object p6, p0, Lcom/google/android/location/c/A;->c:Ljava/lang/String;

    .line 168
    iput-object p7, p0, Lcom/google/android/location/c/A;->i:[B

    .line 169
    iput-object p1, p0, Lcom/google/android/location/c/A;->f:Ljava/util/Set;

    .line 170
    iput-wide p2, p0, Lcom/google/android/location/c/A;->j:J

    .line 171
    iput-object p4, p0, Lcom/google/android/location/c/A;->g:Lcom/google/android/location/c/j$a;

    .line 172
    iput-object p5, p0, Lcom/google/android/location/c/A;->h:Ljava/lang/String;

    .line 173
    iput-boolean p8, p0, Lcom/google/android/location/c/A;->n:Z

    .line 174
    return-void

    :cond_94
    move v0, v2

    .line 143
    goto :goto_32

    .line 147
    :cond_96
    cmp-long v0, p2, v3

    if-ltz v0, :cond_a1

    move v0, v1

    :goto_9b
    const-string v3, "Scan duration should be >= 0"

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    goto :goto_37

    :cond_a1
    move v0, v2

    goto :goto_9b

    :cond_a3
    move v0, v2

    .line 150
    goto :goto_4b

    :cond_a5
    move v1, v2

    .line 159
    goto :goto_76

    .line 161
    :catch_a7
    move-exception v0

    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to parse the key path."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Z)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;J",
            "Lcom/google/android/location/c/j$a;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 178
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v6, v5

    move-object v7, v5

    move v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/c/A;-><init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Ljava/lang/String;Ljava/lang/String;[BZ)V

    .line 179
    return-void
.end method

.method private a(Ljava/util/Map;)Landroid/os/Bundle;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 312
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 313
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 314
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/c/F;

    invoke-virtual {v1}, Lcom/google/android/location/c/F;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_d

    .line 316
    :cond_35
    return-object v2
.end method

.method private a(Landroid/os/Bundle;Ljava/util/Map;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/c/F;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 320
    if-eqz p1, :cond_2e

    .line 321
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 323
    :try_start_16
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/location/c/F;->b(I)Lcom/google/android/location/c/F;

    move-result-object v2

    .line 324
    if-eqz v2, :cond_a

    .line 325
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2b
    .catch Ljava/lang/NumberFormatException; {:try_start_16 .. :try_end_2b} :catch_2c

    goto :goto_a

    .line 327
    :catch_2c
    move-exception v0

    goto :goto_a

    .line 332
    :cond_2e
    return-void
.end method

.method public static a(Ljava/lang/String;)[B
    .registers 6
    .parameter

    .prologue
    .line 370
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 371
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 372
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 373
    const/16 v0, 0x80

    new-array v0, v0, [B

    .line 375
    :goto_13
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1f

    .line 376
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_13

    .line 378
    :cond_1f
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/location/c/A;->l:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/google/android/location/c/F;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/google/android/location/c/A;->o:Z

    .line 264
    return-void
.end method

.method public b()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/location/c/A;->f:Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/google/android/location/c/j$a;
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/location/c/A;->g:Lcom/google/android/location/c/j$a;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/location/c/A;->h:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .registers 2

    .prologue
    .line 286
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .registers 3

    .prologue
    .line 243
    iget-wide v0, p0, Lcom/google/android/location/c/A;->j:J

    return-wide v0
.end method

.method public f()[B
    .registers 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/location/c/A;->i:[B

    return-object v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/android/location/c/A;->m:Z

    return v0
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/google/android/location/c/A;->n:Z

    return v0
.end method

.method public i()Lcom/google/android/location/c/j;
    .registers 10

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/location/c/A;->c:Ljava/lang/String;

    if-eqz v0, :cond_32

    .line 348
    new-instance v0, Lcom/google/android/location/c/A;

    iget-object v1, p0, Lcom/google/android/location/c/A;->f:Ljava/util/Set;

    iget-wide v2, p0, Lcom/google/android/location/c/A;->j:J

    iget-object v4, p0, Lcom/google/android/location/c/A;->g:Lcom/google/android/location/c/j$a;

    iget-object v5, p0, Lcom/google/android/location/c/A;->h:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/location/c/A;->c:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/location/c/A;->a(Ljava/lang/String;)[B

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/location/c/A;->n:Z

    invoke-direct/range {v0 .. v8}, Lcom/google/android/location/c/A;-><init>(Ljava/util/Set;JLcom/google/android/location/c/j$a;Ljava/lang/String;Ljava/lang/String;[BZ)V

    .line 351
    iget v1, p0, Lcom/google/android/location/c/A;->k:I

    iput v1, v0, Lcom/google/android/location/c/A;->k:I

    .line 352
    iget-object v1, v0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 353
    iget-object v1, p0, Lcom/google/android/location/c/A;->l:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/c/A;->l:Ljava/lang/String;

    .line 354
    iget-boolean v1, p0, Lcom/google/android/location/c/A;->m:Z

    iput-boolean v1, v0, Lcom/google/android/location/c/A;->m:Z

    .line 355
    iget-boolean v1, p0, Lcom/google/android/location/c/A;->o:Z

    iput-boolean v1, v0, Lcom/google/android/location/c/A;->o:Z

    .line 358
    :goto_31
    return-object v0

    :cond_32
    move-object v0, p0

    goto :goto_31
.end method

.method public j()Ljava/util/Map;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 389
    iget-object v0, p0, Lcom/google/android/location/c/A;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/F;

    .line 390
    sget-object v1, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 391
    sget-object v1, Lcom/google/android/location/c/A;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 392
    iget-object v4, p0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 393
    if-nez v0, :cond_3b

    iget v0, p0, Lcom/google/android/location/c/A;->k:I

    .line 394
    :goto_33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 393
    :cond_3b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_33

    .line 397
    :cond_40
    return-object v2
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/google/android/location/c/A;->o:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 336
    const-string v0, "Scanner types: %s; Dest: %s; ScanDuration: %d, SensorDelay: %s; Data path: %s; Key path: %s; View opted out WIFI APs: %s; AutomaticShutDown: %s; ForceUpload: %s"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/c/A;->f:Ljava/util/Set;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/c/A;->g:Lcom/google/android/location/c/j$a;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcom/google/android/location/c/A;->j:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/location/c/A;->j()Ljava/util/Map;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/location/c/A;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/location/c/A;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/location/c/A;->m:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/android/location/c/A;->n:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/android/location/c/A;->o:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 293
    iget-object v0, p0, Lcom/google/android/location/c/A;->f:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/android/location/c/F;->a(Ljava/util/Set;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 294
    iget-object v0, p0, Lcom/google/android/location/c/A;->g:Lcom/google/android/location/c/j$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/j$a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/google/android/location/c/A;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 296
    iget-wide v3, p0, Lcom/google/android/location/c/A;->j:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 297
    iget v0, p0, Lcom/google/android/location/c/A;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 298
    iget-object v0, p0, Lcom/google/android/location/c/A;->d:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/google/android/location/c/A;->a(Ljava/util/Map;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 299
    iget-object v0, p0, Lcom/google/android/location/c/A;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/location/c/A;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/google/android/location/c/A;->i:[B

    if-nez v0, :cond_5d

    move v0, v1

    .line 302
    :goto_3b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 303
    if-lez v0, :cond_45

    .line 304
    iget-object v0, p0, Lcom/google/android/location/c/A;->i:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 306
    :cond_45
    iget-boolean v0, p0, Lcom/google/android/location/c/A;->m:Z

    if-eqz v0, :cond_61

    move v0, v2

    :goto_4a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 307
    iget-boolean v0, p0, Lcom/google/android/location/c/A;->n:Z

    if-eqz v0, :cond_63

    move v0, v2

    :goto_52
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 308
    iget-boolean v0, p0, Lcom/google/android/location/c/A;->o:Z

    if-eqz v0, :cond_65

    :goto_59
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 309
    return-void

    .line 301
    :cond_5d
    iget-object v0, p0, Lcom/google/android/location/c/A;->i:[B

    array-length v0, v0

    goto :goto_3b

    :cond_61
    move v0, v1

    .line 306
    goto :goto_4a

    :cond_63
    move v0, v1

    .line 307
    goto :goto_52

    :cond_65
    move v2, v1

    .line 308
    goto :goto_59
.end method
