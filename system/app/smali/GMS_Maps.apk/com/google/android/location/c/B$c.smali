.class final enum Lcom/google/android/location/c/B$c;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/B;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/c/B$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/c/B$c;

.field public static final enum b:Lcom/google/android/location/c/B$c;

.field private static final synthetic c:[Lcom/google/android/location/c/B$c;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    new-instance v0, Lcom/google/android/location/c/B$c;

    const-string v1, "ABORT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/c/B$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/c/B$c;->a:Lcom/google/android/location/c/B$c;

    .line 71
    new-instance v0, Lcom/google/android/location/c/B$c;

    const-string v1, "BLOCK"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/c/B$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/c/B$c;->b:Lcom/google/android/location/c/B$c;

    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/location/c/B$c;

    sget-object v1, Lcom/google/android/location/c/B$c;->a:Lcom/google/android/location/c/B$c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/c/B$c;->b:Lcom/google/android/location/c/B$c;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/location/c/B$c;->c:[Lcom/google/android/location/c/B$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/c/B$c;
    .registers 2
    .parameter

    .prologue
    .line 69
    const-class v0, Lcom/google/android/location/c/B$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/B$c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/c/B$c;
    .registers 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/location/c/B$c;->c:[Lcom/google/android/location/c/B$c;

    invoke-virtual {v0}, [Lcom/google/android/location/c/B$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/c/B$c;

    return-object v0
.end method
