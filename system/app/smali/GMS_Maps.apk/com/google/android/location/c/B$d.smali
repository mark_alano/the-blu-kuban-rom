.class Lcom/google/android/location/c/B$d;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/B;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/Handler;",
        "Lcom/google/android/location/c/h",
        "<",
        "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
        "Lcom/google/android/location/c/D$a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/B;

.field private final b:Ljava/util/concurrent/locks/ReentrantLock;

.field private final c:Ljava/util/concurrent/locks/Condition;

.field private volatile d:I

.field private volatile e:Z

.field private final f:Lcom/google/android/location/c/B$a;

.field private final g:Lcom/google/android/location/c/n;

.field private volatile h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/location/c/B;Landroid/os/Looper;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 205
    iput-object p1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    .line 206
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 191
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    .line 192
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    .line 193
    iput v3, p0, Lcom/google/android/location/c/B$d;->d:I

    .line 196
    iput-boolean v3, p0, Lcom/google/android/location/c/B$d;->e:Z

    .line 198
    new-instance v0, Lcom/google/android/location/c/B$a;

    const/high16 v1, 0x4396

    invoke-direct {v0, v1}, Lcom/google/android/location/c/B$a;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->f:Lcom/google/android/location/c/B$a;

    .line 200
    new-instance v0, Lcom/google/android/location/c/n;

    const-wide/32 v1, 0xea60

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/c/n;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/location/c/B$d;->g:Lcom/google/android/location/c/n;

    .line 203
    iput-boolean v3, p0, Lcom/google/android/location/c/B$d;->h:Z

    .line 207
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 335
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, p2, v2}, Lcom/google/android/location/c/B;->a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 336
    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z

    if-nez v1, :cond_1a

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v1}, Lcom/google/android/location/c/B;->d(Lcom/google/android/location/c/B;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 337
    :cond_1a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Will not send to MASF: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z

    if-eqz v1, :cond_3e

    const-string v1, "Too many server errors."

    :goto_2b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 339
    new-instance v3, Lcom/google/android/location/c/D$a;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/c/B$d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    .line 349
    :cond_3d
    :goto_3d
    return-void

    .line 337
    :cond_3e
    const-string v1, "Interrupted by client."

    goto :goto_2b

    .line 344
    :cond_41
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v1}, Lcom/google/android/location/c/B;->e(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/t;

    move-result-object v1

    invoke-virtual {v1, v2, p0}, Lcom/google/android/location/c/t;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/h;)Z

    move-result v1

    .line 345
    if-nez v1, :cond_3d

    .line 346
    new-instance v1, Lcom/google/android/location/c/D$a;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v3, "Can not send to MASF."

    invoke-direct {v1, v4, v0, v3}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    invoke-direct {p0, v2, v1}, Lcom/google/android/location/c/B$d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    goto :goto_3d
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 274
    const/4 v0, 0x2

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p0, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 278
    invoke-virtual {p0, v0}, Lcom/google/android/location/c/B$d;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    move-result v0

    .line 279
    const-string v1, "There is pending result before handler thread exits."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 280
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 299
    iget v0, p0, Lcom/google/android/location/c/B$d;->d:I

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/location/c/B$d;->e:Z

    if-eqz v0, :cond_e

    .line 300
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/location/c/B$d;->sendEmptyMessage(I)Z

    .line 301
    const/4 v0, 0x1

    .line 303
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private c()V
    .registers 3

    .prologue
    .line 353
    iget v0, p0, Lcom/google/android/location/c/B$d;->d:I

    if-nez v0, :cond_3d

    const/4 v0, 0x1

    :goto_5
    const-string v1, "pending requests are not 0 before quiting."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 355
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v1, v1, Lcom/google/android/location/c/B;->d:Lcom/google/android/location/c/H;

    invoke-virtual {v0, v1}, Lcom/google/android/location/c/s;->a(Lcom/google/android/location/c/H;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/c/s;->d()V

    .line 358
    :cond_28
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v0, v0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_35

    .line 359
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v0, v0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    invoke-interface {v0}, Lcom/google/android/location/c/l;->i()V

    .line 361
    :cond_35
    invoke-virtual {p0}, Lcom/google/android/location/c/B$d;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 363
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 364
    return-void

    .line 353
    :cond_3d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v0}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_9
    const-string v1, "session ID should not be null in asynchronized mode."

    invoke-static {v0, v1}, Lcom/google/android/location/c/L;->b(ZLjava/lang/Object;)V

    .line 369
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/B$d;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    .line 370
    return-void

    .line 368
    :cond_12
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x3

    .line 387
    :try_start_1
    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 388
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    .line 390
    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v1

    if-eqz v1, :cond_44

    .line 393
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v1, v1, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    if-eqz v1, :cond_2b

    .line 394
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v1, v1, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/location/c/l;->a(Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_2b
    .catchall {:try_start_1 .. :try_end_2b} :catchall_60

    .line 407
    :cond_2b
    :goto_2b
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 408
    iget v0, p0, Lcom/google/android/location/c/B$d;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/location/c/B$d;->d:I

    .line 409
    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->b()Z

    .line 410
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 411
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 413
    return-void

    .line 397
    :cond_44
    :try_start_44
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->g:Lcom/google/android/location/c/n;

    invoke-virtual {v1}, Lcom/google/android/location/c/n;->a()V

    .line 398
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->g:Lcom/google/android/location/c/n;

    invoke-virtual {v1}, Lcom/google/android/location/c/n;->b()I

    move-result v1

    if-le v1, v2, :cond_54

    .line 400
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z

    .line 404
    :cond_54
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->c(Lcom/google/android/location/c/B;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, p2, v0, v2}, Lcom/google/android/location/c/B;->a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V
    :try_end_5f
    .catchall {:try_start_44 .. :try_end_5f} :catchall_60

    goto :goto_2b

    .line 407
    :catchall_60
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 408
    iget v1, p0, Lcom/google/android/location/c/B$d;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/location/c/B$d;->d:I

    .line 409
    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->b()Z

    .line 410
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 411
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 286
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 287
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/B$d;->e:Z

    .line 288
    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->b()Z

    move-result v0

    if-nez v0, :cond_f

    .line 291
    :cond_f
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    .line 292
    monitor-exit p0

    return-void

    .line 286
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 375
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/B$d;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    .line 376
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 164
    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p2, Lcom/google/android/location/c/D$a;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    return-void
.end method

.method public declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 225
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_1c

    .line 228
    :goto_7
    :try_start_7
    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->e:Z
    :try_end_9
    .catchall {:try_start_7 .. :try_end_9} :catchall_68
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_9} :catch_61

    if-eqz v1, :cond_12

    .line 268
    :try_start_b
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_1c

    :goto_10
    monitor-exit p0

    return v0

    .line 232
    :cond_12
    :try_start_12
    iget-boolean v1, p0, Lcom/google/android/location/c/B$d;->h:Z
    :try_end_14
    .catchall {:try_start_12 .. :try_end_14} :catchall_68
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_14} :catch_61

    if-eqz v1, :cond_1f

    .line 268
    :try_start_16
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_1b
    .catchall {:try_start_16 .. :try_end_1b} :catchall_1c

    goto :goto_10

    .line 225
    :catchall_1c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236
    :cond_1f
    :try_start_1f
    iget v1, p0, Lcom/google/android/location/c/B$d;->d:I

    const/16 v2, 0x14

    if-ge v1, v2, :cond_53

    .line 240
    const/4 v1, 0x1

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 243
    iget-object v2, p0, Lcom/google/android/location/c/B$d;->a:Lcom/google/android/location/c/B;

    invoke-static {v2}, Lcom/google/android/location/c/B;->b(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/B$d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/c/B$d;->f:Lcom/google/android/location/c/B$a;

    invoke-virtual {v3}, Lcom/google/android/location/c/B$a;->a()J

    move-result-wide v3

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/location/c/B$d;->sendMessageAtTime(Landroid/os/Message;J)Z

    move-result v1

    .line 245
    if-eqz v1, :cond_4c

    .line 246
    iget v2, p0, Lcom/google/android/location/c/B$d;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/location/c/B$d;->d:I

    .line 247
    iget-object v2, p0, Lcom/google/android/location/c/B$d;->f:Lcom/google/android/location/c/B$a;

    invoke-virtual {v2}, Lcom/google/android/location/c/B$a;->b()V
    :try_end_4c
    .catchall {:try_start_1f .. :try_end_4c} :catchall_68
    .catch Ljava/lang/InterruptedException; {:try_start_1f .. :try_end_4c} :catch_61

    .line 268
    :cond_4c
    :try_start_4c
    iget-object v0, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v0, v1

    goto :goto_10

    .line 252
    :cond_53
    if-nez p3, :cond_5b

    .line 268
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_5a
    .catchall {:try_start_4c .. :try_end_5a} :catchall_1c

    goto :goto_10

    .line 262
    :cond_5b
    :try_start_5b
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_60
    .catchall {:try_start_5b .. :try_end_60} :catchall_68
    .catch Ljava/lang/InterruptedException; {:try_start_5b .. :try_end_60} :catch_61

    goto :goto_7

    .line 265
    :catch_61
    move-exception v1

    .line 268
    :try_start_62
    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_10

    :catchall_68
    move-exception v0

    iget-object v1, p0, Lcom/google/android/location/c/B$d;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_6f
    .catchall {:try_start_62 .. :try_end_6f} :catchall_1c
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter

    .prologue
    .line 309
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_2a

    .line 325
    :goto_5
    return-void

    .line 312
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 314
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_5

    .line 318
    :pswitch_16
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 319
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/location/c/D$a;

    invoke-direct {p0, v1, v0}, Lcom/google/android/location/c/B$d;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;)V

    goto :goto_5

    .line 322
    :pswitch_26
    invoke-direct {p0}, Lcom/google/android/location/c/B$d;->c()V

    goto :goto_5

    .line 309
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_6
        :pswitch_16
        :pswitch_26
    .end packed-switch
.end method
