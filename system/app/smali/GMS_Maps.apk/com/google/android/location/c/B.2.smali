.class Lcom/google/android/location/c/B;
.super Lcom/google/android/location/c/D;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/B$a;,
        Lcom/google/android/location/c/B$d;,
        Lcom/google/android/location/c/B$b;,
        Lcom/google/android/location/c/B$c;
    }
.end annotation


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[B

.field private final h:Landroid/os/PowerManager;

.field private final i:Landroid/content/Context;

.field private volatile j:Lcom/google/android/location/c/t;

.field private volatile k:Z

.field private final l:Ljava/lang/String;

.field private volatile m:Lcom/google/android/location/c/s;

.field private n:Ljava/lang/Object;

.field private final o:Lcom/google/android/location/c/B$c;

.field private p:Lcom/google/android/location/c/B$d;

.field private volatile q:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-static {}, Lcom/google/android/location/c/B;->b()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/c/B;-><init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Ljava/lang/String;Lcom/google/android/location/k/a/c;)V

    .line 91
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Ljava/lang/String;Lcom/google/android/location/k/a/c;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0, p7, p9, p3}, Lcom/google/android/location/c/D;-><init>(Lcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    .line 56
    iput-boolean v1, p0, Lcom/google/android/location/c/B;->k:Z

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/c/B;->n:Ljava/lang/Object;

    .line 64
    iput-boolean v1, p0, Lcom/google/android/location/c/B;->q:Z

    .line 103
    const-string v0, "Session id should not be null. Please make sure you called the correct constructor."

    invoke-static {p8, v0}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iput-object p1, p0, Lcom/google/android/location/c/B;->i:Landroid/content/Context;

    .line 106
    iput-object p4, p0, Lcom/google/android/location/c/B;->f:Ljava/lang/String;

    .line 107
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/location/c/B;->h:Landroid/os/PowerManager;

    .line 108
    iput-object p5, p0, Lcom/google/android/location/c/B;->e:Ljava/lang/String;

    .line 109
    iput-object p6, p0, Lcom/google/android/location/c/B;->g:[B

    .line 110
    iput-object p8, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Lcom/google/android/location/c/B;->o:Lcom/google/android/location/c/B$c;

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/B;)Landroid/os/PowerManager;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/c/B;->h:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 462
    if-eqz p3, :cond_6

    .line 463
    const/4 v0, 0x2

    invoke-virtual {p2, v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 465
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p1, v0, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 466
    return-object p1
.end method

.method static synthetic a(Lcom/google/android/location/c/B;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    if-eqz v0, :cond_d

    .line 476
    iget-object v0, p0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    invoke-virtual {p2}, Lcom/google/android/location/c/D$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p4, p3, v1}, Lcom/google/android/location/c/l;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 478
    :cond_d
    invoke-direct {p0, p1, p4}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    .line 479
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/location/c/B;->e:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 511
    :goto_4
    return-void

    .line 490
    :cond_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    .line 494
    iget-object v7, p0, Lcom/google/android/location/c/B;->n:Ljava/lang/Object;

    monitor-enter v7

    .line 495
    :try_start_f
    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    if-nez v0, :cond_25

    .line 498
    new-instance v0, Lcom/google/android/location/c/s;

    iget-object v1, p0, Lcom/google/android/location/c/B;->h:Landroid/os/PowerManager;

    iget-object v2, p0, Lcom/google/android/location/c/B;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/location/c/B;->g:[B

    iget-object v4, p0, Lcom/google/android/location/c/B;->a:Lcom/google/android/location/c/l;

    iget-object v5, p0, Lcom/google/android/location/c/B;->b:Lcom/google/android/location/k/a/c;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/c/s;-><init>(Landroid/os/PowerManager;Ljava/lang/String;[BLcom/google/android/location/c/l;Lcom/google/android/location/k/a/c;Lcom/google/android/location/c/H;)V

    iput-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    .line 502
    :cond_25
    monitor-exit v7
    :try_end_26
    .catchall {:try_start_f .. :try_end_26} :catchall_3a

    .line 503
    if-eqz p2, :cond_34

    .line 504
    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/s;->a(Ljava/lang/String;)Lcom/google/android/location/c/D$a;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Lcom/google/android/location/c/D$a;->a()Z

    move-result v0

    if-nez v0, :cond_34

    .line 509
    :cond_34
    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/s;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    goto :goto_4

    .line 502
    :catchall_3a
    move-exception v0

    :try_start_3b
    monitor-exit v7
    :try_end_3c
    .catchall {:try_start_3b .. :try_end_3c} :catchall_3a

    throw v0
.end method

.method static synthetic b(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/B$d;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    return-object v0
.end method

.method static b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/location/c/B;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 545
    new-instance v2, Lcom/google/android/location/c/D$a;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const-string v4, "To many data in upload queue."

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/location/c/D$a;-><init>(ZLcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/location/c/B;->l:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/location/c/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/android/location/c/D$a;ILjava/lang/String;)V

    .line 549
    return-void
.end method

.method static synthetic d(Lcom/google/android/location/c/B;)Z
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/location/c/B;->k:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/t;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/c/B;->j:Lcom/google/android/location/c/t;

    return-object v0
.end method

.method private e()V
    .registers 4

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/location/c/t;

    iget-object v1, p0, Lcom/google/android/location/c/B;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/location/c/B;->b:Lcom/google/android/location/k/a/c;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/c/t;-><init>(Landroid/content/Context;Lcom/google/android/location/k/a/c;)V

    iput-object v0, p0, Lcom/google/android/location/c/B;->j:Lcom/google/android/location/c/t;

    .line 116
    new-instance v0, Lcom/google/android/location/c/B$b;

    const-string v1, "RemoteScanResultWriter.workerThread"

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/c/B$b;-><init>(Lcom/google/android/location/c/B;Ljava/lang/String;)V

    .line 117
    invoke-virtual {v0}, Lcom/google/android/location/c/B$b;->start()V

    .line 121
    invoke-virtual {v0}, Lcom/google/android/location/c/B$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/google/android/location/c/B$d;

    invoke-direct {v1, p0, v0}, Lcom/google/android/location/c/B$d;-><init>(Lcom/google/android/location/c/B;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    .line 124
    return-void
.end method

.method static synthetic f(Lcom/google/android/location/c/B;)Lcom/google/android/location/c/s;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/location/c/B;->m:Lcom/google/android/location/c/s;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .registers 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    if-eqz v0, :cond_9

    .line 555
    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    invoke-virtual {v0}, Lcom/google/android/location/c/B$d;->a()V

    .line 557
    :cond_9
    return-void
.end method

.method protected declared-synchronized a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 520
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/B;->q:Z

    if-nez v0, :cond_b

    .line 521
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/B;->q:Z

    .line 522
    invoke-direct {p0}, Lcom/google/android/location/c/B;->e()V

    .line 526
    :cond_b
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/google/android/location/c/B;->f:Ljava/lang/String;

    if-eqz v0, :cond_21

    .line 527
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 528
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/location/c/B;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 530
    :cond_21
    iget-object v0, p0, Lcom/google/android/location/c/B;->o:Lcom/google/android/location/c/B$c;

    sget-object v1, Lcom/google/android/location/c/B$c;->a:Lcom/google/android/location/c/B$c;

    if-ne v0, v1, :cond_35

    .line 531
    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Z

    move-result v0

    .line 532
    if-nez v0, :cond_33

    .line 534
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/B;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_33
    .catchall {:try_start_1 .. :try_end_33} :catchall_3d

    .line 538
    :cond_33
    :goto_33
    monitor-exit p0

    return v0

    :cond_35
    :try_start_35
    iget-object v0, p0, Lcom/google/android/location/c/B;->p:Lcom/google/android/location/c/B$d;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/location/c/B$d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)Z
    :try_end_3b
    .catchall {:try_start_35 .. :try_end_3b} :catchall_3d

    move-result v0

    goto :goto_33

    .line 520
    :catchall_3d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .registers 2

    .prologue
    .line 562
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/B;->k:Z

    .line 563
    return-void
.end method
