.class Lcom/google/android/location/c/G$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/G;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/G;


# direct methods
.method constructor <init>(Lcom/google/android/location/c/G;)V
    .registers 2
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([FI)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 58
    array-length v0, p1

    if-ge p2, v0, :cond_5

    if-gez p2, :cond_9

    .line 59
    :cond_5
    const v0, 0x7f7fffff

    .line 61
    :goto_8
    return v0

    :cond_9
    aget v0, p1, p2

    goto :goto_8
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 51
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 12
    .parameter

    .prologue
    .line 35
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 36
    iget-object v0, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-virtual {v0}, Lcom/google/android/location/c/G;->h()V

    .line 37
    iget-object v0, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-virtual {v0}, Lcom/google/android/location/c/G;->g()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 47
    :goto_11
    return-void

    .line 42
    :cond_12
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    .line 43
    iget-object v0, p0, Lcom/google/android/location/c/G$1;->a:Lcom/google/android/location/c/G;

    invoke-virtual {v0}, Lcom/google/android/location/c/G;->f()Lcom/google/android/location/c/k;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v4, v2}, Lcom/google/android/location/c/G$1;->a([FI)F

    move-result v2

    const/4 v3, 0x1

    invoke-direct {p0, v4, v3}, Lcom/google/android/location/c/G$1;->a([FI)F

    move-result v3

    const/4 v5, 0x2

    invoke-direct {p0, v4, v5}, Lcom/google/android/location/c/G$1;->a([FI)F

    move-result v4

    iget v5, p1, Landroid/hardware/SensorEvent;->accuracy:I

    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/location/c/k;->a(IFFFIJJ)V

    goto :goto_11
.end method
