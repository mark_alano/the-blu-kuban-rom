.class public Lcom/google/android/location/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/location/k/a/c;

.field private final b:Ljavax/crypto/spec/SecretKeySpec;

.field private final c:Z

.field private final d:Ljavax/crypto/Mac;

.field private final e:Ljavax/crypto/Cipher;


# direct methods
.method private constructor <init>([B[BLcom/google/android/location/k/a/c;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v0, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/c/a;->b:Ljavax/crypto/spec/SecretKeySpec;

    .line 97
    invoke-static {p3}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/a;->a:Lcom/google/android/location/k/a/c;

    .line 101
    if-eqz p2, :cond_27

    .line 102
    const-string v0, "AES/CBC/PKCS5Padding"

    .line 103
    invoke-direct {p0, p2}, Lcom/google/android/location/c/a;->a([B)Ljavax/crypto/Mac;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    .line 104
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/location/c/a;->c:Z

    .line 112
    :goto_20
    :try_start_20
    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_23
    .catch Ljava/security/GeneralSecurityException; {:try_start_20 .. :try_end_23} :catch_2f

    move-result-object v0

    .line 116
    :goto_24
    iput-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    .line 117
    return-void

    .line 106
    :cond_27
    const-string v0, "AES"

    .line 107
    iput-object v1, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    .line 108
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/c/a;->c:Z

    goto :goto_20

    .line 113
    :catch_2f
    move-exception v0

    move-object v0, v1

    goto :goto_24
.end method

.method public static a([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/location/c/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/location/c/a;-><init>([B[BLcom/google/android/location/k/a/c;)V

    return-object v0
.end method

.method private a([B)Ljavax/crypto/Mac;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 127
    .line 129
    :try_start_1
    const-string v0, "HmacSHA1"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 130
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "HmacSHA1"

    invoke-direct {v2, p1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_11
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_11} :catch_12
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_11} :catch_15

    .line 140
    :goto_11
    return-object v0

    .line 131
    :catch_12
    move-exception v0

    move-object v0, v1

    .line 139
    goto :goto_11

    .line 135
    :catch_15
    move-exception v0

    move-object v0, v1

    .line 137
    goto :goto_11
.end method

.method private a(Ljava/io/InputStream;I)[B
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 254
    .line 257
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 258
    new-array v1, p2, [B

    .line 260
    :goto_7
    if-lez p2, :cond_16

    const/4 v2, 0x0

    :try_start_a
    invoke-virtual {p1, v1, v2, p2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-ltz v2, :cond_16

    .line 261
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 262
    sub-int/2addr p2, v2

    goto :goto_7

    .line 264
    :cond_16
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_19} :catch_1b

    move-result-object v0

    .line 267
    :goto_1a
    return-object v0

    .line 265
    :catch_1b
    move-exception v0

    .line 267
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private a([BLjavax/crypto/spec/IvParameterSpec;I)[B
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    if-nez v0, :cond_c

    .line 281
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create cipher."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_c
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1f

    const-string v0, "encrypt"

    .line 285
    :goto_11
    :try_start_11
    iget-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    iget-object v1, p0, Lcom/google/android/location/c/a;->b:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v0, p3, v1, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/location/c/a;->e:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1d
    .catch Ljava/security/GeneralSecurityException; {:try_start_11 .. :try_end_1d} :catch_22

    move-result-object v0

    return-object v0

    .line 283
    :cond_1f
    const-string v0, "decrypt"

    goto :goto_11

    .line 287
    :catch_22
    move-exception v0

    .line 288
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to encrypt or decrypt."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x20

    const/16 v3, 0x10

    const/4 v1, 0x0

    .line 82
    if-eqz p0, :cond_1e

    array-length v0, p0

    if-ne v0, v4, :cond_1e

    const/4 v0, 0x1

    :goto_b
    const-string v2, "Key must be 32 bytes."

    invoke-static {v0, v2}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    .line 84
    new-instance v0, Lcom/google/android/location/c/a;

    invoke-static {p0, v1, v3}, Lcom/google/android/location/k/c;->a([BII)[B

    move-result-object v1

    invoke-static {p0, v3, v4}, Lcom/google/android/location/k/c;->a([BII)[B

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/location/c/a;-><init>([B[BLcom/google/android/location/k/a/c;)V

    return-object v0

    :cond_1e
    move v0, v1

    .line 82
    goto :goto_b
.end method

.method private b()Ljavax/crypto/spec/IvParameterSpec;
    .registers 3

    .prologue
    .line 181
    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 182
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 183
    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 184
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v1, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    return-object v1
.end method


# virtual methods
.method public declared-synchronized a(Ljava/io/DataInputStream;)Lcom/google/android/location/e/u;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInputStream;",
            ")",
            "Lcom/google/android/location/e/u",
            "<",
            "Ljava/lang/Integer;",
            "[B>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/16 v4, 0x14

    const/16 v5, 0x10

    .line 197
    monitor-enter p0

    :try_start_6
    iget-boolean v1, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    if-nez v1, :cond_19

    .line 198
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create HMAC generator."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_16
    .catchall {:try_start_6 .. :try_end_16} :catchall_16

    .line 197
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0

    .line 203
    :cond_19
    :try_start_19
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 204
    const/4 v1, 0x4

    .line 206
    const/high16 v2, 0x20

    if-gt v3, v2, :cond_24

    if-gez v3, :cond_3a

    .line 207
    :cond_24
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid data length or too long: %d bytes."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_3a
    iget-boolean v2, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v2, :cond_9f

    .line 212
    const/16 v0, 0x14

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/c/a;->a(Ljava/io/InputStream;I)[B

    move-result-object v1

    .line 213
    array-length v0, v1

    add-int/lit8 v0, v0, 0x4

    .line 214
    if-eqz v1, :cond_4c

    array-length v2, v1

    if-eq v2, v4, :cond_54

    .line 215
    :cond_4c
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to read HMAC."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_54
    const/16 v2, 0x10

    invoke-direct {p0, p1, v2}, Lcom/google/android/location/c/a;->a(Ljava/io/InputStream;I)[B

    move-result-object v4

    .line 220
    array-length v2, v4

    add-int/2addr v2, v0

    .line 221
    if-eqz v4, :cond_61

    array-length v0, v4

    if-eq v0, v5, :cond_69

    .line 222
    :cond_61
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to read IV."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_69
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v0, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 228
    :goto_6e
    invoke-direct {p0, p1, v3}, Lcom/google/android/location/c/a;->a(Ljava/io/InputStream;I)[B

    move-result-object v4

    .line 229
    add-int/2addr v2, v3

    .line 230
    if-eqz v4, :cond_78

    array-length v5, v4

    if-eq v5, v3, :cond_78

    .line 235
    :cond_78
    iget-boolean v3, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v3, :cond_90

    .line 236
    iget-object v3, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    invoke-virtual {v3, v4}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v3

    .line 237
    invoke-static {v3, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_90

    .line 238
    new-instance v0, Ljava/io/IOException;

    const-string v1, "HMAC does not match."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_90
    const/4 v1, 0x2

    invoke-direct {p0, v4, v0, v1}, Lcom/google/android/location/c/a;->a([BLjavax/crypto/spec/IvParameterSpec;I)[B

    move-result-object v0

    .line 242
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/location/e/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/location/e/u;
    :try_end_9c
    .catchall {:try_start_19 .. :try_end_9c} :catchall_16

    move-result-object v0

    monitor-exit p0

    return-object v0

    :cond_9f
    move v2, v1

    move-object v1, v0

    goto :goto_6e
.end method

.method public declared-synchronized a(Ljava/io/DataOutputStream;[B)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 151
    monitor-enter p0

    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    if-nez v1, :cond_15

    .line 152
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create HMAC, data can not be signed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_12

    .line 151
    :catchall_12
    move-exception v0

    monitor-exit p0

    throw v0

    .line 155
    :cond_15
    if-eqz p2, :cond_1c

    :try_start_17
    array-length v1, p2

    const/high16 v2, 0x20

    if-le v1, v2, :cond_24

    .line 156
    :cond_1c
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid data: data is empty or too long."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_24
    iget-boolean v1, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v1, :cond_52

    .line 162
    invoke-direct {p0}, Lcom/google/android/location/c/a;->b()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v1

    .line 164
    :goto_2c
    const/4 v2, 0x1

    invoke-direct {p0, p2, v1, v2}, Lcom/google/android/location/c/a;->a([BLjavax/crypto/spec/IvParameterSpec;I)[B

    move-result-object v2

    .line 166
    iget-boolean v3, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v3, :cond_3b

    .line 167
    iget-object v0, p0, Lcom/google/android/location/c/a;->d:Ljavax/crypto/Mac;

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    .line 172
    :cond_3b
    array-length v3, v2

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 173
    iget-boolean v3, p0, Lcom/google/android/location/c/a;->c:Z

    if-eqz v3, :cond_4d

    .line 174
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 175
    invoke-virtual {v1}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 177
    :cond_4d
    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_50
    .catchall {:try_start_17 .. :try_end_50} :catchall_12

    .line 178
    monitor-exit p0

    return-void

    :cond_52
    move-object v1, v0

    goto :goto_2c
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/location/c/a;->c:Z

    return v0
.end method
