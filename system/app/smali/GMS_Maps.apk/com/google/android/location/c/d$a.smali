.class Lcom/google/android/location/c/d$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:Z

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZLjava/util/Set;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/c/F;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput-object p1, p0, Lcom/google/android/location/c/d$a;->a:Ljava/lang/String;

    .line 168
    iput-boolean p2, p0, Lcom/google/android/location/c/d$a;->b:Z

    .line 169
    iput-boolean p3, p0, Lcom/google/android/location/c/d$a;->c:Z

    .line 170
    iput-object p4, p0, Lcom/google/android/location/c/d$a;->d:Ljava/util/Set;

    .line 171
    iput-boolean p5, p0, Lcom/google/android/location/c/d$a;->e:Z

    .line 172
    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/d$a;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/location/c/d$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 208
    :try_start_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 209
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 210
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1d} :catch_23

    move-result v1

    if-eqz v1, :cond_21

    .line 218
    :goto_20
    return v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_20

    .line 214
    :catch_23
    move-exception v1

    goto :goto_20
.end method


# virtual methods
.method public a(Lcom/google/android/location/c/j;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 188
    invoke-interface {p1}, Lcom/google/android/location/c/j;->c()Lcom/google/android/location/c/j$a;

    move-result-object v0

    sget-object v3, Lcom/google/android/location/c/j$a;->c:Lcom/google/android/location/c/j$a;

    if-ne v0, v3, :cond_3d

    move v0, v1

    .line 190
    :goto_b
    invoke-interface {p1}, Lcom/google/android/location/c/j;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3f

    .line 191
    invoke-interface {p1}, Lcom/google/android/location/c/j;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/location/c/d$a;->a(Ljava/lang/String;)Z

    move-result v3

    .line 193
    :goto_19
    if-eqz v3, :cond_1f

    iget-boolean v3, p0, Lcom/google/android/location/c/d$a;->b:Z

    if-eqz v3, :cond_3c

    :cond_1f
    if-eqz v0, :cond_25

    iget-boolean v0, p0, Lcom/google/android/location/c/d$a;->c:Z

    if-eqz v0, :cond_3c

    :cond_25
    invoke-interface {p1}, Lcom/google/android/location/c/j;->g()Z

    move-result v0

    if-eqz v0, :cond_2f

    iget-boolean v0, p0, Lcom/google/android/location/c/d$a;->e:Z

    if-eqz v0, :cond_3c

    :cond_2f
    iget-object v0, p0, Lcom/google/android/location/c/d$a;->d:Ljava/util/Set;

    invoke-interface {p1}, Lcom/google/android/location/c/j;->b()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3c

    move v2, v1

    :cond_3c
    return v2

    :cond_3d
    move v0, v2

    .line 188
    goto :goto_b

    :cond_3f
    move v3, v2

    goto :goto_19
.end method
