.class final enum Lcom/google/android/location/c/f$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/c/f$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/location/c/f$b;

.field public static final enum b:Lcom/google/android/location/c/f$b;

.field public static final enum c:Lcom/google/android/location/c/f$b;

.field public static final enum d:Lcom/google/android/location/c/f$b;

.field public static final enum e:Lcom/google/android/location/c/f$b;

.field private static final synthetic f:[Lcom/google/android/location/c/f$b;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 616
    new-instance v0, Lcom/google/android/location/c/f$b;

    const-string v1, "NOT_READ_YET"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/c/f$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/c/f$b;->a:Lcom/google/android/location/c/f$b;

    .line 620
    new-instance v0, Lcom/google/android/location/c/f$b;

    const-string v1, "READ_INTERRUPTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/c/f$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/c/f$b;->b:Lcom/google/android/location/c/f$b;

    .line 624
    new-instance v0, Lcom/google/android/location/c/f$b;

    const-string v1, "READ_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/location/c/f$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/c/f$b;->c:Lcom/google/android/location/c/f$b;

    .line 629
    new-instance v0, Lcom/google/android/location/c/f$b;

    const-string v1, "READ_FAILURE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/location/c/f$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/c/f$b;->d:Lcom/google/android/location/c/f$b;

    .line 633
    new-instance v0, Lcom/google/android/location/c/f$b;

    const-string v1, "INVALID_FORMAT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/location/c/f$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/c/f$b;->e:Lcom/google/android/location/c/f$b;

    .line 612
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/location/c/f$b;

    sget-object v1, Lcom/google/android/location/c/f$b;->a:Lcom/google/android/location/c/f$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/c/f$b;->b:Lcom/google/android/location/c/f$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/location/c/f$b;->c:Lcom/google/android/location/c/f$b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/location/c/f$b;->d:Lcom/google/android/location/c/f$b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/location/c/f$b;->e:Lcom/google/android/location/c/f$b;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/location/c/f$b;->f:[Lcom/google/android/location/c/f$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 612
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/c/f$b;
    .registers 2
    .parameter

    .prologue
    .line 612
    const-class v0, Lcom/google/android/location/c/f$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/c/f$b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/c/f$b;
    .registers 1

    .prologue
    .line 612
    sget-object v0, Lcom/google/android/location/c/f$b;->f:[Lcom/google/android/location/c/f$b;

    invoke-virtual {v0}, [Lcom/google/android/location/c/f$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/c/f$b;

    return-object v0
.end method
