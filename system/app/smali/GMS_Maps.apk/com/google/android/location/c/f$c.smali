.class Lcom/google/android/location/c/f$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/l;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/location/c/f;

.field private final b:Lcom/google/android/location/c/f$a;

.field private final c:Lcom/google/android/location/c/g;

.field private volatile d:Z

.field private volatile e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 922
    iput-object p1, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/c/f$c;->d:Z

    .line 920
    invoke-static {}, Lcom/google/android/location/c/L;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f$c;->e:Ljava/util/List;

    .line 923
    iput-object p2, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    .line 924
    iput-object p3, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    .line 925
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;Lcom/google/android/location/c/f$1;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 914
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/location/c/f$c;-><init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/f$c;)Lcom/google/android/location/c/g;
    .registers 2
    .parameter

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1075
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0, p2}, Lcom/google/android/location/c/f$a;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 1076
    monitor-exit p0

    return-void

    .line 1075
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1084
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/location/c/f$c;->d:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_2e

    if-eqz v0, :cond_7

    .line 1101
    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    .line 1087
    :cond_7
    :try_start_7
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/f$a;->d()Lcom/google/android/location/c/K;

    move-result-object v0

    .line 1088
    if-eqz v0, :cond_5

    .line 1090
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/c/f$c;->d:Z

    .line 1091
    iget-object v1, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->h(Lcom/google/android/location/c/f;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1092
    iget-object v1, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v1, :cond_5

    .line 1093
    iget-object v1, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v1}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/c/f$c$4;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/location/c/f$c$4;-><init>(Lcom/google/android/location/c/f$c;Ljava/lang/String;Lcom/google/android/location/c/K;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2d
    .catchall {:try_start_7 .. :try_end_2d} :catchall_2e

    goto :goto_5

    .line 1084
    :catchall_2e
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 929
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 953
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 957
    return-void
.end method

.method public a(Lcom/google/android/location/c/H;)V
    .registers 2
    .parameter

    .prologue
    .line 961
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 965
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 949
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1020
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;IZ)V

    .line 1021
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v0, :cond_19

    .line 1022
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/f$c$2;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/location/c/f$c$2;-><init>(Lcom/google/android/location/c/f$c;ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1029
    :cond_19
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->g(Lcom/google/android/location/c/f;)Z

    move-result v0

    if-nez v0, :cond_3a

    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 1030
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0, p1}, Lcom/google/android/location/c/f$a;->b(Ljava/lang/String;)V

    .line 1031
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0, p1}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1032
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f;Z)Z

    .line 1035
    :cond_3a
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;I)V
    :try_end_3d
    .catchall {:try_start_1 .. :try_end_3d} :catchall_3f

    .line 1036
    monitor-exit p0

    return-void

    .line 1020
    :catchall_3f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;ILjava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1006
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    const/4 v1, 0x0

    invoke-static {v0, p2, v1}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;IZ)V

    .line 1007
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v0, :cond_19

    .line 1008
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v0}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/location/c/f$c$1;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/location/c/f$c$1;-><init>(Lcom/google/android/location/c/f$c;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1015
    :cond_19
    invoke-direct {p0, p1, p2}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;I)V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1e

    .line 1016
    monitor-exit p0

    return-void

    .line 1006
    :catchall_1e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1048
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_38

    .line 1049
    iget-object v0, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v0}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1050
    iget-object v1, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-static {v1, p2}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;Ljava/lang/String;)Lcom/google/android/location/c/K;

    move-result-object v1

    .line 1051
    if-eqz v1, :cond_38

    .line 1052
    iget-object v2, p0, Lcom/google/android/location/c/f$c;->b:Lcom/google/android/location/c/f$a;

    invoke-virtual {v2, p2}, Lcom/google/android/location/c/f$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1054
    iget-object v3, p0, Lcom/google/android/location/c/f$c;->e:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1055
    iget-object v3, p0, Lcom/google/android/location/c/f$c;->c:Lcom/google/android/location/c/g;

    if-eqz v3, :cond_38

    .line 1056
    iget-object v3, p0, Lcom/google/android/location/c/f$c;->a:Lcom/google/android/location/c/f;

    invoke-static {v3}, Lcom/google/android/location/c/f;->d(Lcom/google/android/location/c/f;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/google/android/location/c/f$c$3;

    invoke-direct {v4, p0, v0, v2, v1}, Lcom/google/android/location/c/f$c$3;-><init>(Lcom/google/android/location/c/f$c;ILjava/lang/String;Lcom/google/android/location/c/K;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1065
    :cond_38
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f$c;->b(Ljava/lang/String;)V
    :try_end_3b
    .catchall {:try_start_1 .. :try_end_3b} :catchall_3d

    .line 1066
    monitor-exit p0

    return-void

    .line 1048
    :catchall_3d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(ZZ)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1001
    return-void
.end method

.method public a_(I)V
    .registers 2
    .parameter

    .prologue
    .line 989
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 941
    return-void
.end method

.method public c()V
    .registers 1

    .prologue
    .line 945
    return-void
.end method

.method public d()V
    .registers 1

    .prologue
    .line 977
    return-void
.end method

.method public e()V
    .registers 1

    .prologue
    .line 981
    return-void
.end method

.method public f()V
    .registers 1

    .prologue
    .line 985
    return-void
.end method

.method public g()V
    .registers 1

    .prologue
    .line 993
    return-void
.end method

.method public h()V
    .registers 1

    .prologue
    .line 997
    return-void
.end method

.method public declared-synchronized i()V
    .registers 1

    .prologue
    .line 1040
    monitor-enter p0

    monitor-exit p0

    return-void
.end method
