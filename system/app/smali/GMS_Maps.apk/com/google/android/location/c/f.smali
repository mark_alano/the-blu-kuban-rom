.class public Lcom/google/android/location/c/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/c/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/c/f$3;,
        Lcom/google/android/location/c/f$c;,
        Lcom/google/android/location/c/f$a;,
        Lcom/google/android/location/c/f$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Landroid/os/Handler;

.field private final g:Lcom/google/android/location/c/g;

.field private final h:Lcom/google/android/location/k/a/c;

.field private i:[B

.field private j:[B

.field private volatile k:Z

.field private volatile l:Z

.field private m:Ljava/util/concurrent/CountDownLatch;

.field private n:Ljava/lang/Runnable;

.field private o:Lcom/google/android/location/c/f$c;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 66
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    .line 67
    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, ".lck"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, "sessionId"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 69
    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, "uploadedSeq"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    sget-object v0, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    const-string v1, "sessionSummary"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B[BZLcom/google/android/location/c/g;Landroid/os/Looper;Lcom/google/android/location/k/a/c;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-boolean v1, p0, Lcom/google/android/location/c/f;->k:Z

    .line 89
    iput-boolean v1, p0, Lcom/google/android/location/c/f;->l:Z

    .line 92
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->m:Ljava/util/concurrent/CountDownLatch;

    .line 165
    new-instance v0, Lcom/google/android/location/c/f$1;

    invoke-direct {v0, p0}, Lcom/google/android/location/c/f$1;-><init>(Lcom/google/android/location/c/f;)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->n:Ljava/lang/Runnable;

    .line 126
    if-eqz p7, :cond_1b

    if-eqz p8, :cond_75

    :cond_1b
    move v0, v2

    :goto_1c
    const-string v3, "looper could not be null when listener is not null."

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 128
    invoke-static {p1}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    invoke-static {p2}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    if-nez p4, :cond_2b

    if-eqz p5, :cond_77

    :cond_2b
    move v0, v2

    :goto_2c
    const-string v3, "Need non empty key or keyPath."

    invoke-static {v0, v3}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 132
    if-eqz p5, :cond_38

    array-length v0, p5

    const/16 v3, 0x20

    if-ne v0, v3, :cond_39

    :cond_38
    move v1, v2

    :cond_39
    const-string v0, "signingKey needs to be 32 byte long."

    invoke-static {v1, v0}, Lcom/google/android/location/c/L;->a(ZLjava/lang/Object;)V

    .line 134
    invoke-static {p9}, Lcom/google/android/location/c/L;->a(Lcom/google/android/location/k/a/c;)Lcom/google/android/location/k/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f;->h:Lcom/google/android/location/k/a/c;

    .line 135
    iput-object p1, p0, Lcom/google/android/location/c/f;->b:Landroid/content/Context;

    .line 136
    iput-object p3, p0, Lcom/google/android/location/c/f;->c:Ljava/lang/String;

    .line 137
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_79

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    .line 142
    :goto_65
    iput-object p4, p0, Lcom/google/android/location/c/f;->i:[B

    .line 143
    iput-object p5, p0, Lcom/google/android/location/c/f;->j:[B

    .line 144
    iput-boolean p6, p0, Lcom/google/android/location/c/f;->e:Z

    .line 145
    iput-object p7, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    .line 146
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->f:Landroid/os/Handler;

    .line 147
    return-void

    :cond_75
    move v0, v1

    .line 126
    goto :goto_1c

    :cond_77
    move v0, v1

    .line 130
    goto :goto_2c

    .line 140
    :cond_79
    iput-object p2, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    goto :goto_65
.end method

.method static synthetic a(Lcom/google/android/location/c/f;[Ljava/lang/String;)Lcom/google/android/location/c/f$a;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a([Ljava/lang/String;)Lcom/google/android/location/c/f$a;

    move-result-object v0

    return-object v0
.end method

.method private a([Ljava/lang/String;)Lcom/google/android/location/c/f$a;
    .registers 11
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 523
    invoke-static {}, Lcom/google/android/location/c/L;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 526
    array-length v6, p1

    move v4, v3

    move-object v0, v2

    move-object v1, v2

    :goto_a
    if-ge v4, v6, :cond_3d

    aget-object v7, p1, v4

    .line 527
    const-string v8, "sessionId"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_24

    .line 529
    invoke-direct {p0, v7}, Lcom/google/android/location/c/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 530
    invoke-static {v1}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_21

    move-object v1, v2

    .line 526
    :cond_21
    :goto_21
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 533
    :cond_24
    const-string v8, "uploadedSeq"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_31

    .line 534
    invoke-direct {p0, v7}, Lcom/google/android/location/c/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_21

    .line 535
    :cond_31
    const-string v8, "sessionSummary"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_21

    .line 539
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 542
    :cond_3d
    if-nez v1, :cond_46

    .line 543
    invoke-static {}, Lcom/google/android/location/c/B;->b()Ljava/lang/String;

    move-result-object v1

    .line 544
    invoke-direct {p0, v1}, Lcom/google/android/location/c/f;->c(Ljava/lang/String;)Z

    .line 546
    :cond_46
    new-instance v2, Lcom/google/android/location/c/f$a;

    invoke-direct {v2, p0, v5, v1}, Lcom/google/android/location/c/f$a;-><init>(Lcom/google/android/location/c/f;Ljava/util/List;Ljava/lang/String;)V

    .line 547
    if-eqz v0, :cond_71

    .line 548
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 549
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 550
    array-length v4, v1

    move v0, v3

    :goto_59
    if-ge v0, v4, :cond_71

    aget-object v3, v1, v0

    .line 551
    invoke-static {v3}, Lcom/google/android/location/c/L;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6e

    .line 553
    :try_start_63
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 554
    invoke-virtual {v2, v3}, Lcom/google/android/location/c/f$a;->a(I)V
    :try_end_6e
    .catch Ljava/lang/NumberFormatException; {:try_start_63 .. :try_end_6e} :catch_72

    .line 550
    :cond_6e
    :goto_6e
    add-int/lit8 v0, v0, 0x1

    goto :goto_59

    .line 562
    :cond_71
    return-object v2

    .line 555
    :catch_72
    move-exception v3

    goto :goto_6e
.end method

.method static synthetic a(Lcom/google/android/location/c/f;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/google/android/location/c/f$a;)V
    .registers 14
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 262
    new-instance v0, Lcom/google/android/location/c/f$c;

    iget-object v1, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    invoke-direct {v0, p0, p1, v1, v3}, Lcom/google/android/location/c/f$c;-><init>(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;Lcom/google/android/location/c/g;Lcom/google/android/location/c/f$1;)V

    iput-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    .line 263
    new-instance v0, Lcom/google/android/location/c/B;

    iget-object v1, p0, Lcom/google/android/location/c/f;->b:Landroid/content/Context;

    sget-object v2, Lcom/google/android/location/c/B$c;->b:Lcom/google/android/location/c/B$c;

    iget-object v4, p0, Lcom/google/android/location/c/f;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v8

    move-object v5, v3

    move-object v6, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/c/B;-><init>(Landroid/content/Context;Lcom/google/android/location/c/B$c;Lcom/google/android/location/c/H;Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/location/c/l;Ljava/lang/String;Lcom/google/android/location/k/a/c;)V

    .line 272
    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_41

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 273
    iget-boolean v3, p0, Lcom/google/android/location/c/f;->l:Z

    if-nez v3, :cond_3e

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/B;Lcom/google/android/location/c/f$a;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_28

    .line 276
    :cond_3e
    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->a()V

    .line 280
    :cond_41
    invoke-virtual {v0}, Lcom/google/android/location/c/B;->d()V

    .line 285
    :goto_44
    :try_start_44
    iget-object v0, p0, Lcom/google/android/location/c/f;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 286
    iget-boolean v0, p0, Lcom/google/android/location/c/f;->e:Z

    if-nez v0, :cond_5f

    move v0, v10

    .line 287
    :goto_4e
    iget-boolean v1, p0, Lcom/google/android/location/c/f;->e:Z

    if-eqz v1, :cond_59

    .line 288
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->c(Lcom/google/android/location/c/f$a;)Z

    move-result v0

    if-nez v0, :cond_61

    move v0, v10

    .line 290
    :cond_59
    :goto_59
    if-eqz v0, :cond_5e

    .line 291
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->b(Lcom/google/android/location/c/f$a;)V
    :try_end_5e
    .catch Ljava/lang/InterruptedException; {:try_start_44 .. :try_end_5e} :catch_63

    .line 299
    :cond_5e
    return-void

    :cond_5f
    move v0, v11

    .line 286
    goto :goto_4e

    :cond_61
    move v0, v11

    .line 288
    goto :goto_59

    .line 294
    :catch_63
    move-exception v0

    goto :goto_44
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Lcom/google/android/location/c/f$a;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a(Lcom/google/android/location/c/f$a;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    if-eqz v0, :cond_e

    .line 250
    iget-object v0, p0, Lcom/google/android/location/c/f;->f:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/location/c/f$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/location/c/f$2;-><init>(Lcom/google/android/location/c/f;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 257
    :cond_e
    return-void
.end method

.method private a(Lcom/google/android/location/c/B;Lcom/google/android/location/c/f$a;Ljava/lang/String;)Z
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 442
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 444
    iget-object v0, p0, Lcom/google/android/location/c/f;->i:[B

    if-eqz v0, :cond_d1

    .line 445
    iget-object v0, p0, Lcom/google/android/location/c/f;->i:[B

    iget-object v5, p0, Lcom/google/android/location/c/f;->h:Lcom/google/android/location/k/a/c;

    invoke-static {v0, v5}, Lcom/google/android/location/c/a;->a([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v0

    .line 447
    :goto_22
    iget-object v5, p0, Lcom/google/android/location/c/f;->j:[B

    if-eqz v5, :cond_2e

    .line 448
    iget-object v1, p0, Lcom/google/android/location/c/f;->j:[B

    iget-object v5, p0, Lcom/google/android/location/c/f;->h:Lcom/google/android/location/k/a/c;

    invoke-static {v1, v5}, Lcom/google/android/location/c/a;->b([BLcom/google/android/location/k/a/c;)Lcom/google/android/location/c/a;

    move-result-object v1

    .line 450
    :cond_2e
    new-instance v5, Lcom/google/android/location/c/b;

    invoke-direct {v5, v4, v0, v1}, Lcom/google/android/location/c/b;-><init>(Ljava/lang/String;Lcom/google/android/location/c/a;Lcom/google/android/location/c/a;)V

    .line 453
    sget-object v0, Lcom/google/android/location/c/f$b;->c:Lcom/google/android/location/c/f$b;

    .line 457
    :cond_35
    :goto_35
    :try_start_35
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->a()[B

    move-result-object v1

    if-eqz v1, :cond_44

    .line 458
    iget-boolean v4, p0, Lcom/google/android/location/c/f;->l:Z

    if-eqz v4, :cond_55

    .line 460
    invoke-virtual {p1}, Lcom/google/android/location/c/B;->c()V

    .line 461
    sget-object v0, Lcom/google/android/location/c/f$b;->b:Lcom/google/android/location/c/f$b;
    :try_end_44
    .catchall {:try_start_35 .. :try_end_44} :catchall_c5
    .catch Ljava/io/FileNotFoundException; {:try_start_35 .. :try_end_44} :catch_6d
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_44} :catch_99
    .catch Lcom/google/android/location/c/b$a; {:try_start_35 .. :try_end_44} :catch_ba

    .line 505
    :cond_44
    :try_start_44
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_cc

    .line 510
    :goto_47
    invoke-virtual {p2, p3, v0}, Lcom/google/android/location/c/f$a;->a(Ljava/lang/String;Lcom/google/android/location/c/f$b;)V

    .line 511
    iget-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    invoke-virtual {p2}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 513
    :goto_54
    return v0

    .line 464
    :cond_55
    :try_start_55
    new-instance v4, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/j/a;->aw:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 465
    invoke-virtual {v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 466
    invoke-virtual {v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v1

    if-nez v1, :cond_78

    .line 467
    new-instance v0, Ljava/io/IOException;

    const-string v1, "isValid returned after parsing GLocRequest"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6d
    .catchall {:try_start_55 .. :try_end_6d} :catchall_c5
    .catch Ljava/io/FileNotFoundException; {:try_start_55 .. :try_end_6d} :catch_6d
    .catch Ljava/io/IOException; {:try_start_55 .. :try_end_6d} :catch_99
    .catch Lcom/google/android/location/c/b$a; {:try_start_55 .. :try_end_6d} :catch_ba

    .line 491
    :catch_6d
    move-exception v0

    .line 492
    :try_start_6e
    sget-object v0, Lcom/google/android/location/c/f$b;->d:Lcom/google/android/location/c/f$b;

    .line 493
    const-string v1, "File not found."
    :try_end_72
    .catchall {:try_start_6e .. :try_end_72} :catchall_c5

    .line 505
    :try_start_72
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_75} :catch_76

    goto :goto_47

    .line 506
    :catch_76
    move-exception v1

    goto :goto_47

    .line 469
    :cond_78
    const/4 v1, 0x6

    :try_start_79
    invoke-virtual {v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/c/L;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 472
    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    .line 476
    const/4 v6, 0x3

    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v6

    .line 477
    invoke-virtual {p2, p3, v6}, Lcom/google/android/location/c/f$a;->a(Ljava/lang/String;I)V

    .line 478
    invoke-virtual {p2, v6}, Lcom/google/android/location/c/f$a;->b(I)Z

    move-result v7

    if-eqz v7, :cond_a4

    .line 480
    const/4 v1, 0x1

    invoke-static {p2, v6, v1}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;IZ)V
    :try_end_98
    .catchall {:try_start_79 .. :try_end_98} :catchall_c5
    .catch Ljava/io/FileNotFoundException; {:try_start_79 .. :try_end_98} :catch_6d
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_98} :catch_99
    .catch Lcom/google/android/location/c/b$a; {:try_start_79 .. :try_end_98} :catch_ba

    goto :goto_35

    .line 495
    :catch_99
    move-exception v0

    .line 496
    :try_start_9a
    sget-object v0, Lcom/google/android/location/c/f$b;->d:Lcom/google/android/location/c/f$b;

    .line 497
    const-string v1, "Error reading file."
    :try_end_9e
    .catchall {:try_start_9a .. :try_end_9e} :catchall_c5

    .line 505
    :try_start_9e
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_a1
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_a1} :catch_a2

    goto :goto_47

    .line 506
    :catch_a2
    move-exception v1

    goto :goto_47

    .line 483
    :cond_a4
    :try_start_a4
    invoke-virtual {p1, v4, v1}, Lcom/google/android/location/c/B;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v1

    .line 484
    if-nez v1, :cond_35

    .line 485
    iget-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    invoke-virtual {p2}, Lcom/google/android/location/c/f$a;->c()Ljava/lang/String;

    move-result-object v1

    const-string v4, "Fatal: can not submit task."

    invoke-virtual {v0, v1, v6, v4}, Lcom/google/android/location/c/f$c;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_b5
    .catchall {:try_start_a4 .. :try_end_b5} :catchall_c5
    .catch Ljava/io/FileNotFoundException; {:try_start_a4 .. :try_end_b5} :catch_6d
    .catch Ljava/io/IOException; {:try_start_a4 .. :try_end_b5} :catch_99
    .catch Lcom/google/android/location/c/b$a; {:try_start_a4 .. :try_end_b5} :catch_ba

    .line 505
    :try_start_b5
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_b8
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_b8} :catch_ca

    :goto_b8
    move v0, v2

    .line 508
    goto :goto_54

    .line 499
    :catch_ba
    move-exception v0

    .line 500
    :try_start_bb
    sget-object v0, Lcom/google/android/location/c/f$b;->e:Lcom/google/android/location/c/f$b;

    .line 501
    const-string v1, "Invalid file format."
    :try_end_bf
    .catchall {:try_start_bb .. :try_end_bf} :catchall_c5

    .line 505
    :try_start_bf
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_c2
    .catch Ljava/io/IOException; {:try_start_bf .. :try_end_c2} :catch_c3

    goto :goto_47

    .line 506
    :catch_c3
    move-exception v1

    goto :goto_47

    .line 504
    :catchall_c5
    move-exception v0

    .line 505
    :try_start_c6
    invoke-virtual {v5}, Lcom/google/android/location/c/b;->b()V
    :try_end_c9
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_c9} :catch_cf

    .line 508
    :goto_c9
    throw v0

    .line 506
    :catch_ca
    move-exception v0

    goto :goto_b8

    :catch_cc
    move-exception v1

    goto/16 :goto_47

    :catch_cf
    move-exception v1

    goto :goto_c9

    :cond_d1
    move-object v0, v1

    goto/16 :goto_22
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Ljava/io/File;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->a(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/location/c/f;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/google/android/location/c/f;->k:Z

    return p1
.end method

.method private a(Ljava/io/File;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 412
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 413
    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_1b

    aget-object v4, v2, v1

    .line 414
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 415
    invoke-direct {p0, v4}, Lcom/google/android/location/c/f;->a(Ljava/io/File;)Z

    .line 413
    :goto_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 417
    :cond_17
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_14

    .line 420
    :cond_1b
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 422
    const/4 v0, 0x1

    .line 426
    :cond_22
    return v0
.end method

.method private a([Ljava/io/File;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 372
    .line 373
    array-length v2, p1

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_1d

    aget-object v3, p1, v1

    .line 374
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 381
    :cond_d
    :goto_d
    return v0

    .line 377
    :cond_e
    sget-object v4, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 373
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 381
    :cond_1d
    array-length v1, p1

    sget-object v2, Lcom/google/android/location/c/f;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-gt v1, v2, :cond_d

    const/4 v0, 0x1

    goto :goto_d
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)[B
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 581
    const/4 v1, 0x0

    .line 583
    :try_start_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z
    :try_end_10
    .catchall {:try_start_2 .. :try_end_10} :catchall_47
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_10} :catch_57

    move-result v3

    if-eqz v3, :cond_19

    .line 599
    :cond_13
    if-eqz v0, :cond_18

    .line 601
    :try_start_15
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_18} :catch_51

    .line 604
    :cond_18
    :goto_18
    return-object v0

    .line 587
    :cond_19
    :try_start_19
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1e
    .catchall {:try_start_19 .. :try_end_1e} :catchall_47
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_1e} :catch_57

    .line 588
    :try_start_1e
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 589
    const/16 v3, 0x400

    new-array v3, v3, [B

    .line 591
    :goto_27
    invoke-virtual {v1, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_3b

    .line 592
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_31
    .catchall {:try_start_1e .. :try_end_31} :catchall_55
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_31} :catch_32

    goto :goto_27

    .line 595
    :catch_32
    move-exception v2

    .line 599
    :goto_33
    if-eqz v1, :cond_18

    .line 601
    :try_start_35
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_39

    goto :goto_18

    .line 602
    :catch_39
    move-exception v1

    goto :goto_18

    .line 594
    :cond_3b
    :try_start_3b
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_55
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3e} :catch_32

    move-result-object v0

    .line 599
    if-eqz v1, :cond_18

    .line 601
    :try_start_41
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_44} :catch_45

    goto :goto_18

    .line 602
    :catch_45
    move-exception v1

    goto :goto_18

    .line 599
    :catchall_47
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_4b
    if-eqz v1, :cond_50

    .line 601
    :try_start_4d
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_50} :catch_53

    .line 604
    :cond_50
    :goto_50
    throw v0

    .line 602
    :catch_51
    move-exception v1

    goto :goto_18

    :catch_53
    move-exception v1

    goto :goto_50

    .line 599
    :catchall_55
    move-exception v0

    goto :goto_4b

    .line 595
    :catch_57
    move-exception v1

    move-object v1, v0

    goto :goto_33
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/c/f;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    .line 571
    if-nez v1, :cond_a

    .line 572
    const/4 v0, 0x0

    .line 574
    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_9
.end method

.method private b(Lcom/google/android/location/c/f$a;)V
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 306
    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->e()Ljava/util/List;

    move-result-object v1

    .line 307
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    :goto_a
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_21

    .line 309
    if-eqz v0, :cond_17

    .line 310
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :cond_17
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 314
    :cond_21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_47

    .line 316
    const/4 v1, 0x0

    .line 318
    :try_start_28
    new-instance v0, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    const-string v5, "uploadedSeq"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_37
    .catchall {:try_start_28 .. :try_end_37} :catchall_52
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_37} :catch_48

    .line 319
    :try_start_37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_42
    .catchall {:try_start_37 .. :try_end_42} :catchall_5d
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_42} :catch_62

    .line 323
    if-eqz v0, :cond_47

    .line 325
    :try_start_44
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_59

    .line 331
    :cond_47
    :goto_47
    return-void

    .line 320
    :catch_48
    move-exception v0

    move-object v0, v1

    .line 323
    :goto_4a
    if-eqz v0, :cond_47

    .line 325
    :try_start_4c
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4f} :catch_50

    goto :goto_47

    .line 326
    :catch_50
    move-exception v0

    goto :goto_47

    .line 323
    :catchall_52
    move-exception v0

    :goto_53
    if-eqz v1, :cond_58

    .line 325
    :try_start_55
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_58
    .catch Ljava/io/IOException; {:try_start_55 .. :try_end_58} :catch_5b

    .line 327
    :cond_58
    :goto_58
    throw v0

    .line 326
    :catch_59
    move-exception v0

    goto :goto_47

    :catch_5b
    move-exception v1

    goto :goto_58

    .line 323
    :catchall_5d
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_53

    .line 320
    :catch_62
    move-exception v1

    goto :goto_4a
.end method

.method static synthetic b(Lcom/google/android/location/c/f;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/location/c/f;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/location/c/f;Ljava/lang/String;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/location/c/f;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/location/c/f;)Lcom/google/android/location/c/g;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/c/f;->g:Lcom/google/android/location/c/g;

    return-object v0
.end method

.method private c()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 390
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 392
    :try_start_8
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 393
    if-nez v2, :cond_f

    .line 405
    :cond_e
    :goto_e
    return v0

    .line 398
    :cond_f
    invoke-direct {p0, v2}, Lcom/google/android/location/c/f;->a([Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 400
    invoke-direct {p0, v1}, Lcom/google/android/location/c/f;->a(Ljava/io/File;)Z
    :try_end_18
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_18} :catch_1a

    move-result v0

    goto :goto_e

    .line 402
    :catch_1a
    move-exception v1

    goto :goto_e
.end method

.method private c(Lcom/google/android/location/c/f$a;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 340
    invoke-virtual {p1}, Lcom/google/android/location/c/f$a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 341
    invoke-static {p1, v0}, Lcom/google/android/location/c/f$a;->a(Lcom/google/android/location/c/f$a;Ljava/lang/String;)Lcom/google/android/location/c/K;

    move-result-object v2

    .line 342
    const-string v3, "Summary should not be null after all complete."

    invoke-static {v2, v3}, Lcom/google/android/location/c/L;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    iget v3, v2, Lcom/google/android/location/c/K;->a:I

    if-ne v3, v4, :cond_2e

    iget v3, v2, Lcom/google/android/location/c/K;->b:I

    if-nez v3, :cond_2e

    iget v3, v2, Lcom/google/android/location/c/K;->c:I

    if-nez v3, :cond_2e

    iget v3, v2, Lcom/google/android/location/c/K;->f:I

    if-eqz v3, :cond_32

    :cond_2e
    iget v2, v2, Lcom/google/android/location/c/K;->d:I

    if-ne v2, v4, :cond_9

    .line 349
    :cond_32
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :try_start_39
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_3c
    .catch Ljava/lang/SecurityException; {:try_start_39 .. :try_end_3c} :catch_45

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_9

    .line 364
    :cond_40
    invoke-direct {p0}, Lcom/google/android/location/c/f;->c()Z

    move-result v0

    return v0

    .line 356
    :catch_45
    move-exception v0

    goto :goto_9
.end method

.method private c(Ljava/lang/String;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 895
    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/c/f;->d:Ljava/lang/String;

    const-string v2, "sessionId"

    invoke-direct {v3, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    const/4 v2, 0x0

    .line 898
    :try_start_b
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_29
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_11} :catch_1f

    .line 899
    :try_start_11
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_18
    .catchall {:try_start_11 .. :try_end_18} :catchall_35
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_18} :catch_37

    .line 900
    const/4 v0, 0x1

    .line 904
    if-eqz v1, :cond_1e

    .line 906
    :try_start_1b
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1e} :catch_31

    .line 911
    :cond_1e
    :goto_1e
    return v0

    .line 901
    :catch_1f
    move-exception v1

    move-object v1, v2

    .line 904
    :goto_21
    if-eqz v1, :cond_1e

    .line 906
    :try_start_23
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_26} :catch_27

    goto :goto_1e

    .line 907
    :catch_27
    move-exception v1

    goto :goto_1e

    .line 904
    :catchall_29
    move-exception v0

    move-object v1, v2

    :goto_2b
    if-eqz v1, :cond_30

    .line 906
    :try_start_2d
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_30} :catch_33

    .line 908
    :cond_30
    :goto_30
    throw v0

    .line 907
    :catch_31
    move-exception v1

    goto :goto_1e

    :catch_33
    move-exception v1

    goto :goto_30

    .line 904
    :catchall_35
    move-exception v0

    goto :goto_2b

    .line 901
    :catch_37
    move-exception v2

    goto :goto_21
.end method

.method static synthetic d(Lcom/google/android/location/c/f;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/c/f;->f:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/location/c/f;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/location/c/f;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/location/c/f;)Lcom/google/android/location/c/f$c;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/c/f;->o:Lcom/google/android/location/c/f$c;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/location/c/f;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/location/c/f;->k:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/location/c/f;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/location/c/f;->m:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 151
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/location/c/f;->n:Ljava/lang/Runnable;

    const-string v2, "BatchScanResultUploader.Thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 152
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 153
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/c/f;->l:Z

    .line 163
    return-void
.end method
